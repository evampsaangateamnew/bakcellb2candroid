# Ease Volley Wrapper

> This clone is modified to work with `Network Responses` which are specific 
> to Evamp.

## Configuration

`Ease` is not smart enough to know every thing in advance. To help it out, you need to pass a configuration 
object *(implementation of `EaseConfig` interface)* to `Ease` for making it work according to your need.

You can either create your own configuration by implementing `EaseConfig` interface or `extending` from
`EaseDefaultConfig` which provides default functionality e.g. default loader etc.

Call `EaseUtils.init` at least once with `EaseConfig` object for configuring `Ease`.

    EaseUtils.init(EaseConfig)
    

## Permission

`Ease` required `android.permission.ACCESS_NETWORK_STATE` & `android.permission.INTERNET` permission for accessing
network and checking network state.


## Building a Request

Creating a request with *Ease* is literally easy. `RequestBuilder` offers numerous methods for 
tweaking a request. e.g. running in background, enable caching etc.

  1. Creating Request Headers *[Optional]*
  
      ```java
      RequestHeaders headers = RequestHeaders.newInstance().put("some", "value").put("key", "va");
      ```
  2. Creating Request Body *[Optional]*. For `Bakcell`  it is a `JsonObject`
  
      ```java
       JsonObject body = new JsonObject();
               body.addProperty("subject", subject);
               body.addProperty("text", text);
          
     // for setting it as body, use body method of EaseRequest
      ```
  
  3. Creating Request
  
  
   ```java
        EaseRequest.type(new TypeToken<EaseResponse<List<UserModel>>>(){}) // type token discussed below
                        .method().post() // request type
                        .body(body) // body created in step 2
                        .headers(headers) // headers created in step 1
                        .requestId(100) // request ID for this request
                        .endPoint("users") 
                        .responseCallbacks(this) // callbacks
                        .build().execute(this); // this = context
   ```

## Creating type tokens

In order to parse a network response to arbitrary objects using *Gson* you need to pass `type information` of the `Object`
to `Ease`. This can be done with `TypeToken` class which accepts required type as parametrized type. e.g.

    new TypeToken<EaseResponse<List<UserModel>>>(){}

It will convert the *data* key from network response into a list of `UserModels`.

> This syntax is a little ugly but sadly, we have no other choice.

## Getting response as String

Typically `Ease` will convert network response to required type but sometimes you want it without conversion e.g. in case 
you want to receive many requests with same callback, taking the responsibility of converting *JSON* response
to model yourself. For this purpose, `Ease` provides `asString(Class<T>)` method for getting response as String.

      EaseRequest.asString(String.class)...

## Receiving Response

Ease offers a clean way for handling network response by using `EaseCallbacks` interface. For 
above example response can be handheld as following.

```java

   @Override
    public void onSuccess(@NonNull EaseRequest<List<UserModel>> request, @NonNull String description, @Nullable List<UserModel> data) {
            // on success, use request.id() for getting request id.
    }


    @Override
    public void onFailure(@NonNull EaseRequest<List<UserModel>> request, @NonNull String description) {
            // request failed with description
    }


    @Override
    public void onError(@NonNull EaseRequest<List<UserModel>> request, @NonNull EaseException e) {
            // error occured while connecting to server
    }
    
```

## Request IDs

Each request can have **optional** request ID, for identifying responses in callbacks. You 
can always match the request ID in response callback for parsing response of specific 
request.

> Although, request IDs are optional, I strongly recommend to attach ID with each request.

## Running network call in background

For running network calls in background, you can call `runInBackground()` while building network request. If 
network call is running in background **no progress dialog will be shown to user**.


## Default Headers

For setting default headers (which will be sent with each request), use `defaultHeaders` method
of `EaseConfig`.

```java
@Nullable
    @Override
    public RequestHeaders defaultHeaders(Context context) {
        RequestHeaders headers = RequestHeaders.newInstance()
                .put("Content-Type", "application/json")
                .put("userAgent", "Android")
                .put("token", "some token")
  
        return headers;
    }
```

## Default Body

`Evamp` specific `Ease` allows setting default body. It is set as a `JsonObject` with a key. For setting 
default body, return required key from `defaultBodyKey` method of `EaseConfig` e.g.

```java
 @Override
    public String defaultBodyKey() {
        return "defaultBody"; // the key against which default body is set, see below for setting default body object
    }
```

For setting default body object against key, return `JsonObject` from `defaultBody` method of 
`EaseConfig` class. e.g.

```java
   @Override
    public JsonObject defaultBody(Context context) {
        JsonObject body = new JsonObject();
        body.addProperty("userAgent", "Android");
        body.addProperty("token", "some token");
        body.addProperty("lang", LanguageSource.getLanguageHeader(context));
        return body;
    }
```

> For skipping default body, return **null** from `defaultBodyKey` method of `EaseConfig`.

```
Made with LOVE by Allaudin
```
