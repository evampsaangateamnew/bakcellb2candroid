package allaudin.github.io.ease;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Parsed response for {@link EaseRequest}
 *
 * @param <T> type of this response
 * @author M.Allaudin
 * @see EaseRequest
 * @since 1.0
 */


public final class EaseResponse<T> {


    /**
     * Call status for this request
     */
    private boolean callStatus;


    /**
     * Result code for this string
     */
    private String resultCode;


    /**
     * Data parsed from network response.
     *
     * @see EaseBaseRequest
     */
    private T data;

    @SerializedName("resultDesc")
    private String description;

    /**
     * Status code returned from server
     */
    private int statusCode;


    /**
     * True if this response is fetched from preference cache.
     * <p>
     * This is ease specific caching model.
     */
    private boolean isCached = false;

    /**
     * Get status code for this request
     *
     * @return status code
     */
    public int getServerStatusCode() {
        return statusCode;
    }

    /**
     * Raw network response
     */
    private String raw;

    public boolean isSuspended() {
        return resultCode != null && resultCode.equalsIgnoreCase("-5");
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getRaw() {
        return raw;
    }

    public boolean isCached() {
        return isCached;
    }

    public void setCached(boolean cached) {
        isCached = cached;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Check if request is successful or not
     * <p>
     * For Ojo, result code "00" represents success.
     *
     * @return true if {@code resultCode equals to "00"}, false otherwise.
     */
    public boolean isSuccessful() {
        return resultCode != null && !resultCode.equalsIgnoreCase("null") && resultCode.equalsIgnoreCase("00");
    }


    /**
     * Get description for this request
     *
     * @param description for this request
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public void setData(T data) {
        this.data = data;
    }


    public String getDescription() {
        return description == null ? "" : description;
    }

    public String getDescription(Context context) {
        return TextUtils.isEmpty(description) ? context.getString(R.string.technical_error) : description;
    }

    public T getData() {
        return data;
    }

    public void setCallStatus(boolean callStatus) {
        this.callStatus = callStatus;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }


    public boolean callStatus() {
        return callStatus;
    }

    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
} // Korek
