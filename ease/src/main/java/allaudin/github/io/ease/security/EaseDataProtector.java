package allaudin.github.io.ease.security;

import android.util.Log;

import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import allaudin.github.io.ease.EaseUtils;

/**
 * Encrypt or decrypt data using AES 128-bit encryption algorithm
 *
 * @author M.Allaudin
 *         <p>Created on 7/28/2017.</p>
 */

public class EaseDataProtector {

    private static final String ENCRYPTION_TYPE = "AES/CBC/PKCS5Padding";


    /**
     * Encrypt data
     *
     * @param key  key for encryption
     * @param iv   initialization vector for encryption
     * @param data data to be encrypted
     * @return string - encrypted data
     */
    public static String encrypt(SecretKey key, IvParameterSpec iv, String data) {
        if (data != null) {
            try {
                Cipher cipher = Cipher.getInstance(ENCRYPTION_TYPE);
                cipher.init(Cipher.ENCRYPT_MODE, key, iv);
                return EaseUtils.bytesToHex(cipher.doFinal(data.getBytes()));
            } catch (GeneralSecurityException e) {
                Log.d("Tag", String.format("%s", e.getMessage()));
            }
        }
        return "";
    } // encrypt


    /**
     * Decrypt input data
     *
     * @param key  key used for encryption
     * @param iv   initialization vector used in encryption
     * @param data encrypted data
     * @return decrypted data
     */
    public static String decrypt(SecretKey key, IvParameterSpec iv, String data) {
        if (data != null) {
            try {
                Cipher cipher = Cipher.getInstance(ENCRYPTION_TYPE);
                cipher.init(Cipher.DECRYPT_MODE, key, iv);
                return new String(cipher.doFinal(EaseUtils.hexToByte(data)));
            } catch (GeneralSecurityException e) {
                Log.d("Tag", String.format("%s", e.getMessage()));
            }
        }
        return "";
    } // decrypt

} // EaseDataProtector
