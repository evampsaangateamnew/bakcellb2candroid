package allaudin.github.io.ease;

import java.util.ArrayList;

/**
 * @author Junaid Hassan on 31, December, 2019
 * Senior Android Engineer. Islamabad Pakistan
 */
public class EaseRootValues {

    private ArrayList<String> ndkValues = new ArrayList<>();

    public ArrayList<String> getNdkValues() {
        return ndkValues;
    }//getNdkValues ends

    //singleton object
    private static EaseRootValues instance = null;

    public static EaseRootValues getInstance() {
        if (instance == null) {
            instance = new EaseRootValues();
        }
        return instance;
    }
}
