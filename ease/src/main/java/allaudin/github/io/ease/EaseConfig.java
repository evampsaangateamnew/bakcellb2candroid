package allaudin.github.io.ease;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.JsonObject;

import allaudin.github.io.ease.security.EaseCryptoConfig;
import allaudin.github.io.ease.ssl.EaseSslPinner;

/**
 * Ease configuration interface for initializing
 * ease wrapper.
 *
 * @author M.Allaudin
 * @since 1.0
 */

public interface EaseConfig {

    /**
     * Base URL for requests.
     * <p>
     * Note: Base url must end with "/"
     *
     * @return baseUrl base url for all requests
     */
    @NonNull
    String getBaseUrl();

    /**
     * Dialog for showing before each network call when applicable.
     *
     * @return EaseDialog progress dialog for network calls
     */
    @NonNull
    EaseDialog getDialog(Context context);

    /**
     * Default headers to be added in each network call.
     *
     * @return RequestHeaders headers for adding in every network call.
     */
    @Nullable
    RequestHeaders defaultHeaders(Context context);

    /**
     * Enable response caching.
     * <p>
     * <p>
     * <b>Note:</b> This flag has <em>less priority</em> than flag set through {@link EaseRequest.RequestBuilder}
     *
     * @return boolean - true for caching response, false otherwise
     */
    boolean shouldCacheResponse();

    /**
     * Enable request logs.
     *
     * @return true for enable logging, false for disabling.
     */
    boolean enableLogging();

    /**
     * Enable SSL.
     *
     * @return true for enable SSL, false for disabling.
     */
    boolean enableSSLPinning();


    /**
     * Socket timeout for all requests
     * <p>If 0 is returned form this method, default socket timout of {@code 10 sec} will be used.</p>
     * <p>
     * <b>Note:</b> This timeout has <em>less priority</em> than timeout set through {@link EaseRequest.RequestBuilder}
     *
     * @return int timeout in millis
     * @see com.android.volley.RetryPolicy
     */
    int socketTimeOut();

    /**
     * Number for tries in case of failure
     *
     * @return int number of tries
     * @see com.android.volley.RetryPolicy
     */
    int numOfRetries();

    /**
     * Default Json body to be sent with every request.
     * Note: Added for OJO app specifically.
     *
     * @return jsonObject  body
     */

    JsonObject defaultBody(Context context);

    /**
     * Key against which default body json will be added.
     *
     * @return String key for default json body
     */
    String defaultBodyKey();

    /**
     * SSL pinning configuration for Ease request
     *
     * @return EaseSslPinner - ease pinning configuration
     */
    @Nullable
    EaseSslPinner sslPinner();

    /**
     * Return easeCipher which provides keys for data encryption
     *
     * @return easeCipher - an implementation of easeCipher interface
     */
    EaseCryptoConfig easeCipher();

    /**
     * Enable or disable cache encryption
     *
     * @return true - for enabling cache encryption, false otherwise
     */
    boolean enableCacheEncryption();

}
