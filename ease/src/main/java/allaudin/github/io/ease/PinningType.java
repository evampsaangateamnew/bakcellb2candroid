package allaudin.github.io.ease;

import allaudin.github.io.ease.ssl.EaseSslPinner;

/**
 * Types of SSL pinning
 *
 * @author M.Allaudin
 *         <p>Created on 7/28/2017.</p>
 */

public enum PinningType {

    /**
     * Matches server's certificate with the one in Keystore.
     *
     * @see EaseSslPinner
     */
    CERTIFICATE,

    /**
     * Matches only public key of certificate
     *
     * @see EaseSslPinner
     */
    PUBLIC_KEY
}
