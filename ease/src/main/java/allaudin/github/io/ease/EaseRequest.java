package allaudin.github.io.ease;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import allaudin.github.io.ease.ssl.PublicKeyTrustManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import allaudin.github.io.ease.ssl.EaseSslPinner;
import allaudin.github.io.ease.ssl.OpenTrustManager;

/**
 * A request which converts {@link Response.Listener} to {@link EaseCallbacks} and
 * does heavy lifting of initializing request, setting proper types, request body
 * and headers etc.
 *
 * @param <T> type of request
 * @author M.Allaudin
 * @see RequestHeaders
 * @see RequestBody
 * @see EaseCallbacks
 * @since 1.0
 */


public class EaseRequest<T> implements Response.Listener<EaseResponse<T>>, Response.ErrorListener, NetworkCall<T> {

    private static final boolean LOG = true;

    /**
     * Default headers to be send in body.
     */
    private final Map<String, String> mDefaultBodyParams;

    /**
     * Either run service call in background or foreground.
     * <p>
     * When run in background, progress dialog is not shown and vice versa.
     */
    private boolean mRunInBackground;


    /**
     * Dialog listener for showing and hiding dialog before network calls.
     */
    private EaseDialog mEaseDialog;

    /**
     * Type of request method
     */
    private int mMethod;

    /**
     * Id for this request. This will be passed back to caller
     * via {@link EaseCallbacks}
     */
    private int mRequestId;

    /**
     * End point for this request
     */
    private String mEndPoint;

    /**
     * Headers for this request, default headers are merged if set.
     *
     * @see EaseConfig
     */
    private RequestHeaders mHeaders;

    /**
     * Body for this request
     */
    private JsonObject mBody;

    /**
     * Type token for parsing network response to specific asString
     */
    private TypeToken<EaseResponse<T>> mTypeToken;

    /**
     * Callbacks for sending back parsed network response
     */
    private EaseCallbacks<T> mResponseCallbacks;

    /**
     * Base request for handling real network call and response parsing
     */
    private EaseBaseRequest<T> mEaseBaseRequest;

    /**
     * Should cache response. If set to -1, fetch flag from configuration.
     *
     * @see EaseConfig
     */

    private int mShouldCache;

    /**
     * Socket timeout for this request. If set to -1, fetch from configuration.
     */
    private int mSocketTimeout;

    /**
     * Log request if enabled.
     */
    private boolean mLoggingEnabled;


    /**
     * Get response from cache if it is in the cache.
     */
    private boolean mCacheHit;

    /**
     * If response is cached already, refresh it.
     */
    private boolean mRefreshCache;

    /**
     * Cache key for this request. If cache key is defined in the request,
     * response will be cached and can be fetched from the cache in next request
     * by passing true to {@code mCacheHit } variable in request builder.
     */
    private String mCacheKey;


    /**
     * Complete URL for this request.
     */
    private String mRequestUrl;

    /**
     * Application mContext for managing cache.
     */
    private Context mContext;

    /**
     * Ease configuration object
     */
    private EaseConfig mConfig;

    /**
     * Hit cache if data is cached already and update cache in background.
     */
    private boolean mHitCacheAndRefresh;

    private EaseRequest(int requestId, TypeToken<EaseResponse<T>> typeToken, int method, String endpoint, RequestHeaders headers,
                        JsonObject requestBody,
                        EaseCallbacks<T> responseCallbacks, EaseDialog easeDialog, boolean runInBackground,
                        int shouldCache, int socketTimeout, boolean cacheHit,
                        boolean refreshCache, String cacheKey, Map<String, String> defaultBodyParams, boolean hitAndRefresh) {
        mRequestId = requestId;
        mEndPoint = endpoint;
        mHeaders = headers;
        mMethod = method;
        mShouldCache = shouldCache;
        mBody = requestBody;
        mTypeToken = typeToken;
        mEaseDialog = easeDialog;
        mRunInBackground = runInBackground;
        mResponseCallbacks = responseCallbacks;
        mSocketTimeout = socketTimeout;
        mCacheHit = cacheHit;
        mRefreshCache = refreshCache;
        mCacheKey = cacheKey;
        mDefaultBodyParams = defaultBodyParams;
        mHitCacheAndRefresh = hitAndRefresh;
    }


    private static <T> EaseRequest<T> newInstance(int requestId, TypeToken<EaseResponse<T>> typeToken, int method, String endpoint, RequestHeaders headers, JsonObject requestBody,
                                                  EaseCallbacks<T> responseCallbacks,
                                                  EaseDialog dialog, boolean runInBackground,
                                                  int shouldCache, int socketTimeout,
                                                  boolean cacheHit, boolean refreshCache, String cacheKey,
                                                  Map<String, String> defaultBodyParams, boolean hitAndRefresh) {
        return new EaseRequest<>(requestId, typeToken, method, endpoint, headers, requestBody, responseCallbacks,
                dialog, runInBackground, shouldCache, socketTimeout, cacheHit,
                refreshCache,
                cacheKey, defaultBodyParams, hitAndRefresh);
    }

    /**
     * Get builder for Parametrized type token
     *
     * @param type type token for specific asString
     * @param <T>  type of response
     * @return RequestBuilder for this type
     */
    public static <T> RequestBuilder<T> type(TypeToken<EaseResponse<T>> type) {
        return new RequestBuilder<>(type);
    }

    /**
     * Get builder for non-parametrized type token
     *
     * @param token    non-parametrized string token
     * @param <String> type of response
     * @return RequestBuilder for this type
     */
    public static <String> RequestBuilder<String> asString(Class<String> token) {
        return new RequestBuilder<>(EaseStringType.getType(token));
    }

    private void log(String message, Object args) {
        EaseLogger.debug(String.format(Locale.US, "[id=%s] - [endpoint=%s] - %s", mRequestId, mEndPoint, message), args);
    }

    @Override
    public EaseRequest<T> execute(@NonNull Context context) {

        this.mContext = context;


        mConfig = EaseUtils.getConfig();

        // check cached response
        if ((mCacheHit || mHitCacheAndRefresh) && !TextUtils.isEmpty(mCacheKey) && !mRefreshCache) {
            String json;

            if (mConfig.enableCacheEncryption()) {
                if (LOG) {
                    log("%s", "cache encryption was enabled. fetching encrypted response from cache.");
                }
                json = EaseCacheManager.get(context, mCacheKey, "", mConfig.easeCipher());
            } else {
                if (LOG) {
                    log("%s", "cache encryption was disabled. fetching simple response from cache.");
                }
                json = EaseCacheManager.get(context, mCacheKey, "");
            }
            if (!TextUtils.isEmpty(json)) {
                EaseResponse<T> response = new EaseBaseRequest.ResponseParser<T>().parseResponse(mTypeToken, 200, json);
                response.setRaw(json);
                response.setCached(true);
                if (LOG) {
                    log("%s", "cache hit");
                }
                onResponse(response);
                if (mHitCacheAndRefresh) {
                    mRunInBackground = true;
                } else {
                    return this;
                }
            }
        }


        // check if internet connection is available or not
        if (!isDeviceOnline(this.mContext) && mResponseCallbacks != null) {
            if (LOG) {
                log("%s", "device is offline.");
            }
            mResponseCallbacks.onError(this, new EaseException("No internet connection."));
            return this;
        }

        RequestHeaders defaultHeaders = mConfig.defaultHeaders(context);

        if (mHeaders != null && defaultHeaders != null) {
            mHeaders.mergeDefaultHeader(defaultHeaders);
        } else {
            mHeaders = RequestHeaders.newInstance(defaultHeaders);
        }

        // Menus API set Lang English for now
//        if (mRequestId == 1008) {
//            mHeaders.put("lang", "3");
//        }

//        mHeaders = defaultHeaders != null ? RequestHeaders.newInstance(defaultHeaders) : mHeaders;
        mRequestUrl = mConfig.getBaseUrl() + mEndPoint;
        JsonObject defaultBody = mConfig.defaultBody(context);

        if (mConfig.defaultBodyKey() != null && mConfig.defaultBodyKey().trim().length() > 0 && defaultBody != null) {


            for (String key : mDefaultBodyParams.keySet()) {
                defaultBody.addProperty(key, mDefaultBodyParams.get(key));
            }

            mBody.add(mConfig.defaultBodyKey(), defaultBody);
        }

        mEaseBaseRequest = EaseBaseRequest.getInstance(mTypeToken, mMethod, mRequestUrl,
                mConfig.enableLogging(),
                mHeaders, mBody.toString(),
                this, this);
        mEaseBaseRequest.setShouldCache(mShouldCache == -1 ? mConfig.shouldCacheResponse() : mShouldCache == 1);
        showDialog(context);


        int timeout = mSocketTimeout == -1 ? mConfig.socketTimeOut() : mSocketTimeout;

        mEaseBaseRequest.setRetryPolicy(new DefaultRetryPolicy(timeout, mConfig.numOfRetries(), DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        EaseSslPinner pinner;
        SSLSocketFactory socketFactory;

        if ((pinner = mConfig.sslPinner()) != null && (socketFactory = createSocketFactory(context, pinner)) != null) {
            EaseVolleyWrapper.addRequest(context, mEaseBaseRequest, socketFactory);
        } else {
            EaseVolleyWrapper.addRequest(context, mEaseBaseRequest);
        }

        mLoggingEnabled = mConfig.enableLogging();

        return this;
    }


    @Nullable
    private SSLSocketFactory createSocketFactory(Context context, EaseSslPinner pinner) {
        try {
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            KeyStore trustStore = pinner.trustStore(context);
            if (mConfig.enableSSLPinning()) {//pinner.byPassSsl()
                Log.d("ease", "mConfig.enableSSLPinning()" );
                //sslContext.init(null, new TrustManager[]{new OpenTrustManager()}, null);//older one
                sslContext.init(null, new TrustManager[]{new PublicKeyTrustManager(EaseRootValues.getInstance().getNdkValues())}, null);
            } else if (pinner.type().equals(PinningType.CERTIFICATE) && trustStore != null) {
                Log.d("ease", "PinningType.CERTIFICATE" );
                tmf.init(trustStore);
                sslContext.init(null, tmf.getTrustManagers(), null);
            } else if (pinner.type().equals(PinningType.PUBLIC_KEY)) {
                Log.d("ease", "PinningType.PUBLIC_KEY" );
//                sslContext.init(null, pinner.publicKeysTrusManager(context), null);
                sslContext.init(null, new TrustManager[]{new PublicKeyTrustManager(EaseRootValues.getInstance().getNdkValues())}, null);
            }
            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            EaseLogger.debug(EaseLogger.TAG_CATCH_LOGS, e.getMessage());
        }
        return null;
    }// createSocketFactory


    private void logRequest(String raw, EaseException exception) {
        if (mLoggingEnabled) {
            EaseLogger.debug("Request[%s]\n%s\n%s", mRequestUrl, mHeaders == null ? "" : mHeaders.toString(),
                    EaseUtils.prettyString(mBody.toString()));
            if (raw != null) {
                EaseLogger.debug("%s", EaseUtils.prettyString(raw));
            } else {
                EaseLogger.debug("%s", exception.toString());
            }
        }

    }

    /**
     * Either cache was returned before hitting server.
     *
     * @return true - if cached data was returned, false otherwise.
     */
    public boolean wasHitAndCache() {
        return mHitCacheAndRefresh;
    }

    /**
     * Returns status, if service was running in background or not.
     *
     * @return true - if service was running in background, false otherwise.
     */
    public boolean wasRunningInBackground() {
        return mRunInBackground;
    }

    /**
     * Check internet connectivity.
     *
     * @return true if device is connect to <b>any</b> network, false otherwise.
     */
    private boolean isDeviceOnline(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }


    /**
     * Initialize and show dialog if call is not running in background.
     *
     * @param context activity mContext for initializing dialog.
     */
    private void showDialog(Context context) {
        if (!mRunInBackground) {
            mEaseDialog = mEaseDialog == null ? mEaseDialog = EaseUtils.getConfig().getDialog(context).init(context) : mEaseDialog.init(context);
            mEaseDialog.show();
        }
    } // showDialog

    private void hideDialog() {
        if (!mRunInBackground && mEaseDialog != null) {
            mEaseDialog.hide();
        }
    } // hideDialog

    /**
     * Get original base request offer.
     *
     * @return EaseBaseRequest - underlying base request
     */
    @Nullable
    public EaseBaseRequest<T> getBaseRequest() {
        return mEaseBaseRequest;
    }

    @Override
    public void cancel() {
        if (mEaseBaseRequest != null) {
            mEaseBaseRequest.cancel();
        }
    }

    @Override
    public void onResponse(EaseResponse<T> response) {
        logRequest(response.getRaw(), null);
        hideDialog();
        if (mResponseCallbacks == null) {
            EaseLogger.warning("response callbacks are not specified for request [%s]", mEndPoint);
            return;
        }

        if (response.isSuccessful()) {
            if ((mCacheHit || mRefreshCache || mHitCacheAndRefresh) && mContext != null && !TextUtils.isEmpty(mCacheKey) && !response.isCached()) {
                EaseLogger.debug("%s [%s]", "caching response for endpoint", mEndPoint);
                if (mConfig.enableCacheEncryption()) {
                    EaseCacheManager.add(mContext, mCacheKey, response.getRaw(), mConfig.easeCipher());
                } else {
                    EaseCacheManager.add(mContext, mCacheKey, response.getRaw());
                }
            }
            mResponseCallbacks.onSuccess(this, response);
        } else {
            mResponseCallbacks.onFailure(this, response);
        }

    } // onResponse

    public int method() {
        return mMethod;
    }

    public int id() {
        return mRequestId;
    }

    public boolean idEqualsTo(int id) {
        return mRequestId == id;
    }

    public String endPoint() {
        return mEndPoint;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        hideDialog();

        logRequest(null, new EaseException(error));
        if (mResponseCallbacks != null) {
            mResponseCallbacks.onError(this, new EaseException(error));
        } else {
            EaseLogger.warning("response callbacks are not specified for request %s", mEndPoint);
        }
    } // onErrorResponse


    /**
     * A builder for building requests of asString {@link EaseRequest}
     *
     * @param <K>
     */
    public static class RequestBuilder<K> {

        private int method;
        private int requestId;
        private String endpoint;
        private JsonObject requestBody;
        private RequestHeaders headers;
        private EaseDialog dialog;
        private int shouldCache;
        private int socketTimeout;
        private boolean runInBackground;
        private TypeToken<EaseResponse<K>> typeToken;
        private EaseCallbacks<K> responseCallbacks;
        private Map<String, String> defaultBodyParams = new HashMap<>();
        private boolean hitCache;
        private boolean refreshCache;
        private String cacheKey;
        private boolean hitAndRefresh;

        RequestBuilder(TypeToken<EaseResponse<K>> typeToken) {
            method = Request.Method.GET;
            runInBackground = false;
            this.typeToken = typeToken;
            this.shouldCache = -1; // fall back to default
            this.socketTimeout = -1; // fall back to default
            requestBody = new JsonObject();
        }

        public RequestBuilder<K> requestId(int requestId) {
            this.requestId = requestId;
            return this;
        }

        public RequestBuilder<K> socketTimeOut(int timeout) {
            this.socketTimeout = timeout;
            return this;
        }

        public RequestBuilder<K> defaultBodyParams(Map<String, String> bodyParams) {
            this.defaultBodyParams = bodyParams;
            return this;
        }


        public RequestBuilder<K> dialog(@Nullable EaseDialog dialog) {
            this.dialog = dialog;
            return this;
        }

        public RequestBuilder<K> runInBackground() {
            this.runInBackground = true;
            return this;
        }

        public RequestBuilder<K> runInBackground(boolean runInBackground) {
            this.runInBackground = runInBackground;
            return this;
        }

        public RequestBuilder<K> cache() {
            this.shouldCache = 1;
            return this;
        }

        public RequestBuilder<K> noCache() {
            this.shouldCache = 0;
            return this;
        }

        public RequestBuilder<K> body(JsonObject requestBody) {
            this.requestBody = requestBody;
            return this;
        }


        public RequestBuilder<K> headers(RequestHeaders headers) {
            this.headers = headers;
            return this;
        }


        public RequestBuilder<K> endPoint(String endPoint) {
            this.endpoint = endPoint;
            return this;
        }

        public RequestBuilder<K> method(int method) {
            this.method = method;
            return this;
        }

        /**
         * Custom pref based cache for Ease. This is not default volley cache.
         *
         * @param cacheKey     cache key for saving cache response with this key
         * @param hitCache     return response from cache if true.
         * @param refreshCache get response from network and update cache
         * @return RequestBuilder this builder
         */
        public RequestBuilder<K> easeCache(String cacheKey, boolean hitCache, boolean refreshCache) {
            this.hitCache = hitCache;
            this.cacheKey = cacheKey;
            this.refreshCache = refreshCache;
            return this;
        }

        public RequestBuilder<K> hitEaseCacheAndRefresh(String cacheKey) {
            this.hitAndRefresh = true;
            this.refreshCache = false;
            this.cacheKey = cacheKey;
            return this;
        }

        public RequestMethod<K> method() {
            return new RequestMethod<K>() {
                @Override
                public RequestBuilder<K> get() {
                    method = Request.Method.GET;
                    return RequestBuilder.this;
                }

                @Override
                public RequestBuilder<K> post() {
                    method = Request.Method.POST;
                    return RequestBuilder.this;
                }

                @Override
                public RequestBuilder<K> put() {
                    method = Request.Method.PUT;
                    return RequestBuilder.this;
                }

                @Override
                public RequestBuilder<K> patch() {
                    method = Request.Method.PATCH;
                    return RequestBuilder.this;
                }

                @Override
                public RequestBuilder<K> delete() {
                    method = Request.Method.DELETE;
                    return RequestBuilder.this;
                }

                @Override
                public RequestBuilder<K> head() {
                    method = Request.Method.HEAD;
                    return RequestBuilder.this;
                }

            };
        }


        public RequestBuilder<K> responseCallbacks(EaseCallbacks<K> responseCallbacks) {
            this.responseCallbacks = responseCallbacks;
            return this;
        }


        public NetworkCall<K> build() {
            return EaseRequest.newInstance(requestId, typeToken, method, endpoint, headers,
                    requestBody, responseCallbacks, dialog, runInBackground, shouldCache, socketTimeout,
                    hitCache, refreshCache, cacheKey, defaultBodyParams, hitAndRefresh);
        }


        public interface RequestMethod<T> {
            RequestBuilder<T> get();

            RequestBuilder<T> post();

            RequestBuilder<T> put();

            RequestBuilder<T> patch();

            RequestBuilder<T> delete();

            RequestBuilder<T> head();
        } // RequestMethod

    } // RequestBuilder


    @Override
    public String toString() {
        return "";
    }
} // EaseRequest
