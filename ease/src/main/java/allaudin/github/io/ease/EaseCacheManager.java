package allaudin.github.io.ease;
import android.content.Context;

import allaudin.github.io.ease.security.EaseDataProtector;
import allaudin.github.io.ease.security.EaseCryptoConfig;

/**
 * A custom cache manager for saving responses in preferences.
 * <p>
 *
 *
 * @author M.Allaudin
 *         <p>Created on 7/3/2017.</p>
 */
public final class EaseCacheManager {

    private static final String EASE_PREF_NAME = "ease.cache.prefs";

    private EaseCacheManager() {
        throw new AssertionError("Can't instantiate EaseCacheManager");
    }


    /**
     * Destroy ease cache
     *
     * @param context context for preferences
     */
    public static void destroyCache(Context context) {
        context.getSharedPreferences(EASE_PREF_NAME, Context.MODE_PRIVATE)
                .edit().clear().apply();
    } // destroyCache

    /**
     * Add value to shared prefs
     *
     * @param context context for shared prefs
     * @param key     key
     * @param value   value for given key
     */
    public static void add(Context context, String key, String value) {
        context.getSharedPreferences(EASE_PREF_NAME, Context.MODE_PRIVATE)
                .edit().putString(String.valueOf(key.hashCode()), value)
                .apply();
    } // add


    /**
     * Encrypt and add value to shared prefs
     *
     * @param context context for shared prefs
     * @param key     key
     * @param value   value for given key
     */
    public static void add(Context context, String key, String value, EaseCryptoConfig puzzler) {
        context.getSharedPreferences(EASE_PREF_NAME, Context.MODE_PRIVATE)
                .edit().putString(String.valueOf(key.hashCode()),
                EaseDataProtector.encrypt(puzzler.getKey(), puzzler.ivParamSpec(), value))
                .apply();
    } // add


    /**
     * Get string from prefs with given default value
     *
     * @param context context for shared prefs
     * @param key     key for this field
     * @param def     default value
     * @return value as String
     */
    public static String get(Context context, String key, String def) {
        return context.getSharedPreferences(EASE_PREF_NAME, Context.MODE_PRIVATE)
                .getString(String.valueOf(key.hashCode()), def);
    }

    /**
     * Decrypt and get string from prefs with given default value
     *
     * @param context context for shared prefs
     * @param key     key for this field
     * @param def     default value
     * @return value as String
     */
    public static String get(Context context, String key, String def, EaseCryptoConfig puzzler) {
        String encryptedData = context.getSharedPreferences(EASE_PREF_NAME, Context.MODE_PRIVATE)
                .getString(String.valueOf(key.hashCode()), def);
        return EaseDataProtector.decrypt(puzzler.getKey(), puzzler.ivParamSpec(), encryptedData);
    }

    public static void clear(Context context, String key) {
        context.getSharedPreferences(EASE_PREF_NAME, Context.MODE_PRIVATE).edit()
                .remove(String.valueOf(key.hashCode())).apply();
    }

} // EaseCacheManager
