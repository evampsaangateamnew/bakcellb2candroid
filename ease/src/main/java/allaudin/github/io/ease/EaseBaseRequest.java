package allaudin.github.io.ease;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Base request for lifting heavy work for parsing response
 * into give asString.
 *
 * @author M.Allaudin
 * @see EaseRequest
 * @since 1.0
 */

class EaseBaseRequest<T> extends JsonRequest<EaseResponse<T>> {

    /**
     * Response listener for propagating parsed response
     */
    private Response.Listener<EaseResponse<T>> mResponseListener;

    /**
     * Type token for converting response to specific asString
     */
    private TypeToken<EaseResponse<T>> mTypeToken;

    /**
     * Request headers
     */
    private RequestHeaders mHeaders;

    /**
     * Log response or not
     */
    private boolean mLog;

    // make visible for test cases
    public EaseBaseRequest(TypeToken<EaseResponse<T>> token, int method, String url,
                           RequestHeaders headers, String body, boolean log, Response.Listener<EaseResponse<T>> responseListener,
                           Response.ErrorListener errorListener) {
        super(method, url, body, responseListener, errorListener);
        mResponseListener = responseListener;
        mHeaders = headers;
        mTypeToken = token;
        mLog = log;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders == null ? super.getHeaders() : mHeaders.get();
    }


    static <T> EaseBaseRequest<T> getInstance(TypeToken<EaseResponse<T>> token, int method, String url, boolean log, RequestHeaders headers, String body, Response.Listener<EaseResponse<T>> responseListener, Response.ErrorListener errorListener) {
        return new EaseBaseRequest<>(token, method, url, headers, body, log, responseListener, errorListener);
    }


    @Override
    protected Response<EaseResponse<T>> parseNetworkResponse(NetworkResponse response) {
        try {

            String json = new String(response.data, "UTF-8");
            EaseResponse<T> allayResponse = new ResponseParser<T>().parseResponse( mTypeToken, response.statusCode, json);
            allayResponse.setRaw(json);
            return Response.success(allayResponse, HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }


    @Override
    protected void deliverResponse(EaseResponse<T> response) {
        mResponseListener.onResponse(response);
    }


    public static class ResponseParser<T> {

        ResponseParser() {
        }

        @NonNull
        public EaseResponse<T> parseResponse(TypeToken<EaseResponse<T>> typeToken, int statusCode, String json) {

            EaseResponse<T> allayResponse;

            if (typeToken instanceof EaseStringType && ((EaseStringType) typeToken).getActualType().equals(String.class)) {
                allayResponse = new EaseResponse<>();
                JsonObject object = new JsonParser().parse(json).getAsJsonObject();
                allayResponse.setCallStatus(Boolean.parseBoolean(parseJsonKey(object, "callStatus", "false")));


                if (object.has("resultDesc") && !object.get("resultDesc").isJsonNull()) {
                    allayResponse.setDescription(object.get("resultDesc").getAsString());

                }

                if (object.has("resultCode") && !object.get("resultCode").isJsonNull()) {
                    allayResponse.setResultCode(object.get("resultCode").getAsString());
                }
                allayResponse.setStatusCode(statusCode);
                // Safe cast to type <T> as both T and data are of type String.
                //noinspection unchecked
                allayResponse.setData((T) parseJsonKey(object, "data", null));
            } else {
                allayResponse = new Gson().fromJson(json, typeToken.getType());
                allayResponse.setStatusCode(statusCode);
            }
            return allayResponse;
        }

        /**
         * Return a String representation of key
         *
         * @param json       json from which key is extracted
         * @param key        key for json field
         * @param defaultVal default value to be return when json key has null value or key is not found
         * @return String representation of data against key or empty if key is not present or value is {@code null}
         */
        private String parseJsonKey(JsonObject json, String key, String defaultVal) {
            boolean hasValue = json.has(key) && !json.get(key).isJsonNull();

            if (!hasValue) {
                return defaultVal;
            }

            return json.get(key).isJsonPrimitive() ? json.get(key).toString() :
                    json.get(key).isJsonObject()? json.get(key).toString(): json.get(key).getAsString();
        }

    }
} // EaseBaseRequest
