package allaudin.github.io.ease.security;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 * Configuration of data encryption algorithm for cached response
 *
 * @author M.Allaudin
 *         <p>Created on 7/28/2017.</p>
 */

public interface EaseCryptoConfig {

    /**
     * Secret key for data encryption/decryption.
     * <p>
     * <b>Note:</b> Every invocation of this method should return same key.
     *
     * @return secret key
     */
    SecretKey getKey();

    /**
     * Parameter spec for AES algorithm
     * <p>
     * <b>Note:</b> Every invocation of this method should return same initialization vector (IV)
     *
     * @return iv - initialization vector
     */
    IvParameterSpec ivParamSpec();

} // EaseCryptoConfig
