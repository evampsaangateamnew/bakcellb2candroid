package allaudin.github.io.ease.ssl;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.security.KeyStore;

import javax.net.ssl.TrustManager;

import allaudin.github.io.ease.PinningType;

/**
 * @author M.Allaudin
 *         <p>Created on 7/28/2017.</p>
 */

public interface EaseSslPinner {


    /**
     * Type of SSL pinning.
     *
     * @return {@link PinningType}
     */
    @NonNull
    PinningType type();

    ;

    /**
     * Trust store which contains required SSL certificates.
     * <p>
     * <b>Note:</b> This is only required for Certificate pinning type i.e. when
     * {@link PinningType#CERTIFICATE} is returned from {@link EaseSslPinner#type()}
     *
     * @return keystore
     */
    @Nullable
    KeyStore trustStore(@NonNull Context context);

    /**
     * Public keys which are matched against a certificate in case of public key pinning i.e.
     * when{@link PinningType#PUBLIC_KEY} is returned from {@link EaseSslPinner#type()}
     *
     * @return public keys - keys which are matched with certificate public keys
     */
    @NonNull
    TrustManager[] publicKeysTrusManager(@NonNull Context context);

    /**
     * By pass SSL for Ease requests.
     * <p>
     * <b>Warning:</b> This should only be used for testing purposes.
     *
     * @return true - for bypassing SSL, false otherwise.
     */
    boolean byPassSsl();
}
