package allaudin.github.io.ease;

import android.util.Log;

import com.android.volley.VolleyLog;

/**
 * @author M.Allaudin
 *         <p>Created on 7/3/2017.</p>
 */

class EaseLogger {

    public static final String TAG_CATCH_LOGS = "%s";

    private static final String TAG = VolleyLog.TAG;


    public static void debug(String format, Object... args) {
        try {
            Log.d(TAG, String.format(format, args));
        } catch (Exception ignored) {
        }
    }


    public static void warning(String format, Object... args) {
        try {
            Log.d(TAG, String.format(format, args));
        } catch (Exception ignored) {
        }
    }

}
