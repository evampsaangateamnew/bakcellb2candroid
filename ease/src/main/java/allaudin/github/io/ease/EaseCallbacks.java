package allaudin.github.io.ease;

import androidx.annotation.NonNull;

/**
 * Presents <em>parsed</em> network response in a clean manner.
 * Each different output type is handled separately.
 * <p>
 * <p>
 * <b>Note:</b> This callback is preferred over {@link com.android.volley.Response.Listener}
 *
 * @param <T> type of this response
 * @author M.Allaudin
 * @see <a href="https://www.w3.org/Protocols/HTTP/HTRESP.html">HTTP Response Codes</a>
 * @since 1.0
 * <p>
 */

public interface EaseCallbacks<T> {


    /**
     * Called when response is successful i.e. when {@code resultCode == 00} holds true.
     * <b>Customized for Ojo</b>
     *
     * @param request  request form which response is parsed
     * @param response service response
     */
    void onSuccess(@NonNull EaseRequest<T> request, EaseResponse<T> response);

    /**
     * Called when server returns status code in range of failure i.e. {@code statusCode >= 400}
     *
     * @param request  request form which response is parsed
     * @param response server response
     */
    void onFailure(@NonNull EaseRequest<T> request, EaseResponse<T> response);

    /**
     * Called when an exception occurs while connecting to server.
     *
     * @param request request form which response is parsed
     * @param e       exception wrapping the cause of failure (if available)
     */
    void onError(@NonNull EaseRequest<T> request, @NonNull EaseException e);

} // EaseCallbacks
