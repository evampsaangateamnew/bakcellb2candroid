package allaudin.github.io.ease;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.JsonObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import allaudin.github.io.ease.security.EaseCryptoConfig;
import allaudin.github.io.ease.ssl.EaseSslPinner;

/**
 * Default ease configuration
 *
 * @author M.Allaudin
 */

public abstract class EaseDefaultConfig implements EaseConfig {

    @NonNull
    @Override
    public EaseDialog getDialog(Context context) {
        return DefaultEaseDialog.newDialog("Loadinnnnnnnnnnnnnnnng...");
    }

    @Nullable
    @Override
    public RequestHeaders defaultHeaders(Context context) {
        return null;
    }

    @Override
    public boolean shouldCacheResponse() {
        return false;
    }

    @Override
    public boolean enableLogging() {
        return false;
    }

    @Override
    public int socketTimeOut() {
        return (int) TimeUnit.SECONDS.toMillis(10);
    }

    @Override
    public int numOfRetries() {
        return 0;
    }

    @Override
    public String defaultBodyKey() {
        return null;
    }

    @Override
    public JsonObject defaultBody(Context context) {
        return new JsonObject();
    }

    @Override
    public EaseSslPinner sslPinner() {

        return new EaseSslPinner() {
            @NonNull
            @Override
            public PinningType type() {
                return PinningType.PUBLIC_KEY;
            }

            @Nullable
            @Override
            public KeyStore trustStore(@NonNull Context context) {
                return null;
            }


            @NonNull
            public TrustManager[] publicKeysTrusManager(Context context) {
                return buildTrustManagerFromCertificate(context);
            }

            @Override
            public boolean byPassSsl() {
                return false;
            }
        };

    }

    @Override
    public EaseCryptoConfig easeCipher() {
        return null;
    }

    @Override
    public boolean enableCacheEncryption() {
        return false;
    }

    public static TrustManager[] buildTrustManagerFromCertificate(Context context) {
        if (context == null) return null;
        try {
            // Load CAs from an InputStream
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            // From https://www.washington.edu/itconnect/security/ca/load-der.crt////
            InputStream is = context.getResources().getAssets().open("certificates/bakcell_certificate.crt");
            InputStream caInput = new BufferedInputStream(is);
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            return tmf.getTrustManagers();

        } catch (NoSuchAlgorithmException e) {
            EaseLogger.debug(EaseLogger.TAG_CATCH_LOGS, e.getMessage());
        } catch (KeyStoreException e) {
            EaseLogger.debug(EaseLogger.TAG_CATCH_LOGS, e.getMessage());
        } catch (CertificateException e) {
            EaseLogger.debug(EaseLogger.TAG_CATCH_LOGS, e.getMessage());
        } catch (IOException e) {
            EaseLogger.debug(EaseLogger.TAG_CATCH_LOGS, e.getMessage());
        }
        return null;
    }


}
