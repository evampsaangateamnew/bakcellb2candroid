package allaudin.github.io.ease.ssl;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.X509TrustManager;

/**
 * @author M.Allaudin
 * <p>Created on 7/28/2017.</p>
 */

public class PublicKeyTrustManager implements X509TrustManager {

    private final ArrayList<String> keys;

    public PublicKeyTrustManager(ArrayList<String> keys) {
        this.keys = keys;
    }//PublicKeyTrustManager ends

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        boolean isMatched = false;

        for (X509Certificate x509Certificate : chain) {
            if (x509Certificate.getPublicKey() != null) {
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-256");
                    md.update(x509Certificate.getPublicKey().getEncoded());
                    byte[] dig = md.digest();
                    String certForMatch = Base64.encodeToString(dig, Base64.DEFAULT);
                    String serverPublicKey = certForMatch.trim();
                    for (String localKey : keys) {
//                        Log.d("ease", "public:::" + serverPublicKey + " local:::" + localKey);
                        if (localKey.equalsIgnoreCase(serverPublicKey)) {
//                            Log.d("ease", "matchedWith:::" + serverPublicKey);
                            isMatched = true;
                            break;
                        }
                    }//inner loop ends
                } catch (NoSuchAlgorithmException e) {
//                    Log.d("ease", String.format("%s", e.getMessage()));
                }
            }

            if(isMatched){
                //if local and server public keys are match
                //simply break the loop
                break;
            }
        }//outer for ends

        if (!isMatched) {
            throw new CertificateException("Client is not trusted.");
        }
    }//checkServerTrusted ends

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }//getAcceptedIssuers ends
}//PublicKeyTrustManager ends
