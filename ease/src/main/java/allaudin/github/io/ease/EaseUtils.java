package allaudin.github.io.ease;

import androidx.annotation.NonNull;

import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Locale;

/**
 * Static utility methods for Ease wrapper.
 *
 * @author M. Allaudin
 * @since 1.0
 */

public class EaseUtils {


    private static EaseConfig mInstance;
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static final JsonParser jParser = new JsonParser();


    public static String prettyString(String string) {
        JsonObject json = jParser.parse(string).getAsJsonObject();
        return gson.toJson(json);
    }

    private EaseUtils() {
        throw new AssertionError(String.format(Locale.US, "Can't instantiate %s.", EaseUtils.class.getSimpleName()));
    }

    public static void init(EaseConfig easeConfig) {
        mInstance = mInstance == null ? mInstance = easeConfig : mInstance;

        if (mInstance.enableLogging()) {
            VolleyLog.DEBUG = false;
            VolleyLog.TAG = "ease";
        }
    }

    public static EaseConfig getConfig() {
        return mInstance == null ? new EaseDefaultConfig() {
            @NonNull
            @Override
            public String getBaseUrl() {
                return "";
            }

            @Override
            public boolean enableSSLPinning() {
                return true;
            }
        } : mInstance;
    }

    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5',
                '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexToByte(String string) {
        int len = string.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(string.charAt(i), 16) << 4)
                    + Character.digit(string.charAt(i + 1), 16));
        }
        return data;
    }
} // EaseUtils
