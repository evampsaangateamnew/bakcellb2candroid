#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring

JNICALL
Java_com_bakcell_activities_SplashActivity_getNeededValue(
        JNIEnv *env,
        jobject) {
    std::string var = "s@@ng@tibe*&~~~jkm@@st@r:::hbnnV6jRcxEUEcdKHs4jMuTbpQyTfEdt+7+Zloen5KU=";
    return env->NewStringUTF(var.c_str());
}

/**method for getting the ssl pining key
 * in case of multiple ssl pining keys, should append using the separator
 * append it to the @param sslPiningKey
 * so it should be contain any spliting delimeter
 * the delimeter should be ::::::: (seven collons)
 * add any new ssl pining key to the bottom of this file with any new info!*/
extern "C" JNIEXPORT jstring

JNICALL
Java_com_bakcell_activities_SplashActivity_getSSLPinningKey(
        JNIEnv *env,
        jobject) {

    // A Separator is used for separation for the keys
    std::string separator = ":::::::"; //7 chars

    // Server Public Keys
    std::string sslPiningKey = "hbnnV6jRcxEUEcdKHs4jMuTbpQyTfEdt+7+Zloen5KU=" +
                               separator +
                               "TuT5rhwcDOo93J/khqvsTlhc8i1OTP9z9XDhjl095Kk=" +
                               separator +
                               "izsvtfVqStqVAx/fDo90FqAngqx85vfa+oDJj9n0bQQ=";

    return env->NewStringUTF(sslPiningKey.c_str());

}//Java_com_bakcell_activities_SplashActivity_getSSLPinningKey ends

//"hbnnV6jRcxEUEcdKHs4jMuTbpQyTfEdt+7+Zloen5KU=";//older value, used in 2019

//"TuT5rhwcDOo93J/khqvsTlhc8i1OTP9z9XDhjl095Kk=";//currently getting this public key as on 26-december-2019

// "izsvtfVqStqVAx/fDo90FqAngqx85vfa+oDJj9n0bQQ=" //provided as on 31 december 2019