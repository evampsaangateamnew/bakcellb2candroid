package com.bakcell.adapters;

import android.app.Activity;
import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

/**
 * @author Junaid Hassan on 19, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class MultiAccountsPopupRecyclerAdapter extends RecyclerView.Adapter<MultiAccountsPopupRecyclerAdapter.ViewHolder> {

    private final String fromClass = "MultiAccountsPopupRecyclerAdapter";
    private Activity activity;
    private Context context;
    private ArrayList<UserModel> userModelArrayList;

    public MultiAccountsPopupRecyclerAdapter(Activity activity, Context context, ArrayList<UserModel> userModelArrayList) {
        this.activity = activity;
        this.context = context;
        this.userModelArrayList = userModelArrayList;
    }//constructor ends

    @Override
    public long getItemId(int position) {
        return String.valueOf(position).hashCode();
    }//getItemId ends

    @Override
    public int getItemViewType(int position) {
        return 0;
    }//getItemViewType ends

    @Override
    public int getItemCount() {
        return userModelArrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_multi_accounts_recycler, parent, false);
        return new ViewHolder(v);
    }//onCreateViewHolder ends

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (Tools.hasValue(userModelArrayList.get(position).getMsisdn())) {
            holder.number.setText(userModelArrayList.get(position).getMsisdn());
            holder.number.setSelected(true);
        }

        String firstName = "";
        if (Tools.hasValue(userModelArrayList.get(position).getFirstName())) {
            firstName = userModelArrayList.get(position).getFirstName();
        }

        String lastName = "";
        if (Tools.hasValue(userModelArrayList.get(position).getLastName())) {
            lastName = userModelArrayList.get(position).getLastName();
        }

        /**check if first name has the values,
         * if not then show only the last name
         * will omit the space in this case!*/
        if (Tools.hasValue(firstName)) {
            holder.nameTV.setText(firstName + " " + lastName);
            holder.nameTV.setSelected(true);
        } else {
            holder.nameTV.setText(lastName);
            holder.nameTV.setSelected(true);
        }

        if (Tools.hasValue(userModelArrayList.get(position).getMsisdn())
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())
                && userModelArrayList.get(position).getMsisdn().equalsIgnoreCase(DataManager.getInstance().getCurrentUser().getMsisdn())) {
            holder.ovalIV.setVisibility(View.VISIBLE);
        } else {
            holder.ovalIV.setVisibility(View.INVISIBLE);
        }

        /**hide the very last bottom view below the content holder*/
        if (position == userModelArrayList.size() - 1) {
            holder.bottomView.setVisibility(View.GONE);
        } else {
            holder.bottomView.setVisibility(View.VISIBLE);
        }

        holder.contentContainerHolder.setOnClickListener(view -> {
            /**check if the clicked item is currently logged in,
             * if, then do nothing
             * else, switch the account*/
            if (DataManager.getInstance().getCurrentUser().getMsisdn().equalsIgnoreCase(userModelArrayList.get(position).getMsisdn())) {
                //do nothing, item is the currently logged in user
                BakcellPopUpDialog.showMessageDialog(activity, getLocalizedNoteTitle(),
                        getLocalizedUserAlreadySelectedMessage());
            } else {
                RootValues.getInstance().getManageAccountSwitchingEvents().
                        onAccountSwitchFromManageAccountsRecycler(userModelArrayList.get(position));
            }
        });
    }//onBindViewHolder ends

    private String getLocalizedNoteTitle() {
        String title;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            title = "Note";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            title = "Qeyd";
        } else {
            title = "К сведению";
        }
        return title;
    }

    private String getLocalizedUserAlreadySelectedMessage() {
        String title;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            title = "User already selected.";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            title = "İstifadəçi artıq seçilib.";
        } else {
            title = "Пользователь уже выбран.";
        }
        return title;
    }//getLocalizedUserAlreadySelectedMessage ends

    class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout contentContainerHolder;
        ImageView ovalIV;
        BakcellTextViewNormal nameTV;
        BakcellTextViewNormal number;
        View bottomView;

        public ViewHolder(View itemView) {
            super(itemView);
            contentContainerHolder = itemView.findViewById(R.id.contentContainerHolder);
            bottomView = itemView.findViewById(R.id.bottomView);
            ovalIV = itemView.findViewById(R.id.ovalIV);
            nameTV = itemView.findViewById(R.id.nameTV);
            number = itemView.findViewById(R.id.number);
        }
    }
}//class ends
