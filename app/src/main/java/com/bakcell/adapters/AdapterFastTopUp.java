package com.bakcell.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.FastTopUpActivity;
import com.bakcell.activities.landing.TopUpActivity;
import com.bakcell.models.fastpayments.FastTopUpDetail;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;
import java.util.List;

public class AdapterFastTopUp extends RecyclerView.Adapter<AdapterFastTopUp.ViewHolder> {
    List<FastTopUpDetail> fastTopUpDetails = new ArrayList<>();
    Context context;
    private SetOnItemSelected setOnItemSelected;

    public AdapterFastTopUp(List<FastTopUpDetail> fastTopUpDetails, Context context, SetOnItemSelected setOnItemSelected) {
        this.fastTopUpDetails = fastTopUpDetails;
        this.context = context;
        this.setOnItemSelected = setOnItemSelected;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_fast_topup,
                parent, false);
        return new ViewHolder(view);
    } // onCreateViewHolder Ends

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FastTopUpDetail fastTopUpDetail = fastTopUpDetails.get(position);
        if (fastTopUpDetail == null) return; //safe passage

        holder.itemView.setOnClickListener(view -> {
            //Check if user clicked last item then Go to TopUp Screen
            if ((position == fastTopUpDetails.size() - 1) && context != null && context instanceof FastTopUpActivity) {
                BaseActivity.startNewActivity((FastTopUpActivity) context, TopUpActivity.class);
            }
        });
        //check if last item then set properties to Do More Payments item.
        //else continue with normal fow
        if (position == fastTopUpDetails.size() - 1) {
            holder.llAmount.setVisibility(View.GONE);
            holder.btnPay.setVisibility(View.GONE);
            holder.cardIcon.setImageResource(R.drawable.ic_add_card);
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.dark_gray));
            holder.cardNumber.setText(context.getString(R.string.more_payments));
        } else {
            //else show simple card according to its type
            holder.cardNumber.setText(fastTopUpDetail.getTopupNumber());
            holder.textPayment.setText(fastTopUpDetail.getAmount());
            holder.llAmount.setVisibility(View.VISIBLE);
            holder.btnPay.setVisibility(View.VISIBLE);
            if (fastTopUpDetail.getCardType().contains("v")) {
                holder.cardIcon.setImageResource(R.drawable.ic_visa_card);
            } else if (fastTopUpDetail.getCardType().contains("m")) {
                holder.cardIcon.setImageResource(R.drawable.ic_master_card);
            }
        }
        //set ClickListener to Button
        holder.btnPay.setOnClickListener(view -> {
            if (setOnItemSelected != null) {
                setOnItemSelected.onSelected(fastTopUpDetail);
            }
        });

    } //onBindViewHolder Ends

    @Override
    public int getItemCount() {
        return fastTopUpDetails.size();
    }

    public void deleteItem(int position) {
        fastTopUpDetails.remove(position);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        BakcellTextViewNormal cardNumber;
        BakcellTextViewBold textPayment;
        LinearLayout llAmount;
        ImageView cardIcon;
        BakcellButtonNormal btnPay;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardIcon = itemView.findViewById(R.id.card_icon);
            cardNumber = itemView.findViewById(R.id.card_text);
            llAmount = itemView.findViewById(R.id.llAmount);
            textPayment = itemView.findViewById(R.id.text_payment);
            btnPay = itemView.findViewById(R.id.btn_pay);
        }
    }

    public interface SetOnItemSelected  {
        public void onSelected(FastTopUpDetail fastTopUpDetail);
    }
}
