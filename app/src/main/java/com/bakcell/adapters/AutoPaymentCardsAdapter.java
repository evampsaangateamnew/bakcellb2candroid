package com.bakcell.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.SaveCardsItemClickListener;
import com.bakcell.models.topup.GetSavedCardsItems;

import java.util.ArrayList;
import java.util.List;

public class AutoPaymentCardsAdapter extends RecyclerView.Adapter<AutoPaymentCardsAdapter.ViewHolder> {
    private List<GetSavedCardsItems> savedCardsItems = new ArrayList<>();
    private Context context;
    private int selectedItem = 0;
    private SaveCardsItemClickListener saveCardsItemClickListener;

    public AutoPaymentCardsAdapter(List<GetSavedCardsItems> dataSet, Context context, SaveCardsItemClickListener saveCardsItemClickListener) {
        if (dataSet != null)
            this.savedCardsItems = dataSet;
        this.context = context;
        this.saveCardsItemClickListener = saveCardsItemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_topup_visa,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GetSavedCardsItems getSavedCardsItems = savedCardsItems.get(position);
//        if(!isValidCardData(getSavedCardsItems))
//        {
//            return;
//        }
        holder.cardNumber.setText(getSavedCardsItems.getTopupNumber());
        //check if position is selected Item then update selected row color
        //else set properties to normal.
        if (position == selectedItem) {
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.radioButton.setChecked(true);
            holder.cardNumber.setTypeface(RootValues.getInstance().getFontArialBold(), Typeface.BOLD);
        } else {
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.text_gray));
            holder.radioButton.setChecked(false);
            holder.cardNumber.setTypeface(RootValues.getInstance().getFontArialRegular(), Typeface.NORMAL);
        }
        //set Click listener to radioButton
        holder.radioButton.setOnClickListener(v -> {
            updateItemCheckState(position);

        });
        //set Click listener to RecyclerView Row
        holder.itemView.setOnClickListener(view -> {
            updateItemCheckState(position);
        });

        //else show simple card according to its type
        if (getSavedCardsItems.getCardType().contains("v")) {
            holder.cardIcon.setImageResource(R.drawable.ic_visa_card);
        } else if (getSavedCardsItems.getCardType().contains("m")) {
            holder.cardIcon.setImageResource(R.drawable.ic_master_card);
        }

    } //end onBindViewHolder

    private boolean isValidCardData(GetSavedCardsItems getSavedCardsItems) {
        if (getSavedCardsItems != null && getSavedCardsItems.getId() != null
                && !getSavedCardsItems.getId().isEmpty()
                && getSavedCardsItems.getCardType() != null
                && !getSavedCardsItems.getCardType().isEmpty()
                && getSavedCardsItems.getTopupNumber() != null
                && !getSavedCardsItems.getTopupNumber().isEmpty()) {
            return true;
        } else
            return false;

    }

    private void updateItemCheckState(int position) {

        int copyOfLastCheckedPosition = selectedItem;
        selectedItem = position;
        notifyItemChanged(copyOfLastCheckedPosition);
        notifyItemChanged(selectedItem);

        if (saveCardsItemClickListener != null) {
            saveCardsItemClickListener.onItemClick(savedCardsItems.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return savedCardsItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView cardNumber;
        ImageView cardIcon;
        RadioButton radioButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardIcon = itemView.findViewById(R.id.card_icon);
            cardNumber = itemView.findViewById(R.id.card_text);
            radioButton = itemView.findViewById(R.id.radio_btn);
        }
    }
}
