package com.bakcell.adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bakcell.fragments.pager.tutorials.TutorialPaggerFragment;
import com.bakcell.models.tutorials.Tutorial;

import java.util.List;

/**
 * Created by Zeeshan Shabbir on 8/3/2017.
 */

public class AdapterTutorials extends FragmentPagerAdapter {

    private List<Tutorial> tutorialList;
    private Context context;

    public AdapterTutorials(FragmentManager fm, List<Tutorial> list, Context context) {
        super(fm);
        this.tutorialList = list;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return new TutorialPaggerFragment().newInstance(context,
                tutorialList.get(position).getRes(),
                tutorialList.get(position).getTitle(),
                tutorialList.get(position).getMessage(),
                position);
    }

    @Override
    public int getCount() {
        return tutorialList.size();
    }
}
