package com.bakcell.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.models.usagehistory.summary.UsageSummary;
import com.bakcell.models.usagehistory.summary.UsageSummaryRecords;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

public class ExpandableAdapterUsageHistorySummary extends BaseExpandableListAdapter {

    private Context context;
    ArrayList<UsageSummary> usageSummaryMainArrayList;

    public ExpandableAdapterUsageHistorySummary(Context context, ArrayList<UsageSummary> arrayList) {
        this.context = context;
        this.usageSummaryMainArrayList = arrayList;
    }

    public void updateList(ArrayList<UsageSummary> arrayList) {
        this.usageSummaryMainArrayList = new ArrayList<>();
        this.usageSummaryMainArrayList.addAll(arrayList);
        notifyDataSetChanged();
    }

    public class ViewHolderGroup {
        ImageView plus_icon_img;
        BakcellTextViewNormal usage_type_txt, usage_txt, charged_txt, tv_azn;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        ViewHolderGroup viewHolderGroup = null;

        if (convertView == null) {
            viewHolderGroup = new ViewHolderGroup();
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_exp_adapter_header_usage_summary, null);
            viewHolderGroup.plus_icon_img = (ImageView) convertView.findViewById(R.id.plus_icon_img);
            viewHolderGroup.usage_type_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.usage_type_txt);
            viewHolderGroup.usage_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.usage_txt);
            viewHolderGroup.charged_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.charged_txt);
            viewHolderGroup.tv_azn = (BakcellTextViewNormal) convertView.findViewById(R.id.tv_azn);
            convertView.setTag(viewHolderGroup);
        } else {
            viewHolderGroup = (ViewHolderGroup) convertView.getTag();
        }

        if (isExpanded) {
            viewHolderGroup.plus_icon_img.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.minus_gray_normal));
        } else {
            viewHolderGroup.plus_icon_img.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.plus_gray_normal));
        }


        if (Tools.hasValue(usageSummaryMainArrayList.get(groupPosition).getName())) {
            viewHolderGroup.usage_type_txt.setText(usageSummaryMainArrayList.get(groupPosition).getName());
        } else {
            viewHolderGroup.usage_type_txt.setText("");
        }

        if (Tools.hasValue(usageSummaryMainArrayList.get(groupPosition).getTotalUsage())) {
            viewHolderGroup.usage_txt.setText(usageSummaryMainArrayList.get(groupPosition).getTotalUsage().trim());
        } else {
            viewHolderGroup.usage_txt.setText("");
        }

        if (Tools.hasValue(usageSummaryMainArrayList.get(groupPosition).getTotalCharge())) {
            viewHolderGroup.charged_txt.setText(usageSummaryMainArrayList.get(groupPosition).getTotalCharge());
            viewHolderGroup.tv_azn.setVisibility(View.VISIBLE);
        } else {
            viewHolderGroup.charged_txt.setText("");
            viewHolderGroup.tv_azn.setVisibility(View.GONE);
        }

        return convertView;
    }


    public class ViewHolderChild {
        BakcellTextViewNormal usage_type_txt, usage_txt, charged_txt, tv_azn;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolderChild viewHolderChild = null;

        if (convertView == null) {
            viewHolderChild = new ViewHolderChild();
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_exp_adapter_sub_usage_summary, null);

            viewHolderChild.usage_type_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.usage_type_txt);
            viewHolderChild.usage_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.usage_txt);
            viewHolderChild.charged_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.charged_txt);
            viewHolderChild.tv_azn = (BakcellTextViewNormal) convertView.findViewById(R.id.tv_azn);

            convertView.setTag(viewHolderChild);
        } else {
            viewHolderChild = (ViewHolderChild) convertView.getTag();
        }

        if (Tools.hasValue(usageSummaryMainArrayList.get(groupPosition).getRecords().get(childPosition).getService_type())) {
            viewHolderChild.usage_type_txt.setText(usageSummaryMainArrayList.get(groupPosition).getRecords().get(childPosition).getService_type());
        } else {
            viewHolderChild.usage_type_txt.setText("");
        }

        if (Tools.hasValue(usageSummaryMainArrayList.get(groupPosition).getRecords().get(childPosition).getTotal_usage())) {
            viewHolderChild.usage_txt.setText(usageSummaryMainArrayList.get(groupPosition).getRecords().get(childPosition).getTotal_usage());
        } else {
            viewHolderChild.usage_txt.setText("");
        }


        if (Tools.hasValue(usageSummaryMainArrayList.get(groupPosition).getRecords().get(childPosition).getChargeable_amount())) {
            viewHolderChild.charged_txt.setText(usageSummaryMainArrayList.get(groupPosition).getRecords().get(childPosition).getChargeable_amount());
            viewHolderChild.tv_azn.setVisibility(View.VISIBLE);
        } else {
            viewHolderChild.charged_txt.setText("");
            viewHolderChild.tv_azn.setVisibility(View.GONE);
        }


        return convertView;
    }


    @Override
    public UsageSummaryRecords getChild(int groupPosition, int childPosititon) {
        return this.usageSummaryMainArrayList.get(groupPosition).getRecords().get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.usageSummaryMainArrayList.get(groupPosition).getRecords().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.usageSummaryMainArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.usageSummaryMainArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}