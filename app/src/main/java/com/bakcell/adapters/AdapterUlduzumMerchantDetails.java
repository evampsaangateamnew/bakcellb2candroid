package com.bakcell.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.models.ulduzumdetails.BranchList;

import java.util.ArrayList;

public class AdapterUlduzumMerchantDetails extends RecyclerView.Adapter<AdapterUlduzumMerchantDetails.ViewHolder> {
    ArrayList<BranchList> arrayList;

    public AdapterUlduzumMerchantDetails(ArrayList<BranchList> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_itemt_merchant_branches,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(arrayList.get(position)!=null&&arrayList.get(position).getAddress()!=null&&!arrayList.get(position).getAddress().isEmpty())
        holder.txt_branchAddress.setText(arrayList.get(position).getAddress());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_branchAddress;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_branchAddress = itemView.findViewById(R.id.txt_branchAddress);
        }
    }
}
