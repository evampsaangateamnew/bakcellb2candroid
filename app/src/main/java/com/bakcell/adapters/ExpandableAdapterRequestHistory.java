package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.models.loan.RequestHistory;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;

/**
 * Created by Noman on 6/9/2017.
 */

public class ExpandableAdapterRequestHistory extends BaseExpandableListAdapter {

    private ArrayList<RequestHistory> requestHistoryArrayList = new ArrayList<>();

    private Context context;

    public ExpandableAdapterRequestHistory(ArrayList<RequestHistory> requestHistoryArrayList, Context context) {
        this.requestHistoryArrayList = requestHistoryArrayList;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return requestHistoryArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_item_request_history, null);
            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tv_status);
            viewHolder.tvAmount = (TextView) convertView.findViewById(R.id.tv_amount);
            viewHolder.tvPaid = (TextView) convertView.findViewById(R.id.tv_paid);
            viewHolder.tvRemaining = (TextView) convertView.findViewById(R.id.tv_remaining);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (groupPosition > -1 && groupPosition < requestHistoryArrayList.size()) {
            if (Tools.hasValue(requestHistoryArrayList.get(groupPosition).getDateTime())) {
                viewHolder.tvDate.setText(requestHistoryArrayList.get(groupPosition).getDateTime());
            } else {
                viewHolder.tvDate.setText("");
            }
            if (Tools.hasValue(requestHistoryArrayList.get(groupPosition).getPaid())) {
                viewHolder.tvPaid.setText(requestHistoryArrayList.get(groupPosition).getPaid());
            } else {
                viewHolder.tvPaid.setText("");
            }
            if (Tools.hasValue(requestHistoryArrayList.get(groupPosition).getStatus())) {
                viewHolder.tvStatus.setSelected(true);
                viewHolder.tvStatus.setText(requestHistoryArrayList.get(groupPosition).getStatus());
            } else {
                viewHolder.tvStatus.setText("");
            }
            if (Tools.hasValue(requestHistoryArrayList.get(groupPosition).getRemaining())) {
                viewHolder.tvRemaining.setText(requestHistoryArrayList.get(groupPosition).getRemaining());
            } else {
                viewHolder.tvRemaining.setText("");
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_child_item_request_history, null);
            viewHolder.tvPeak = (TextView) convertView.findViewById(R.id.tv_peak);
            viewHolder.tvAmount = (TextView) convertView.findViewById(R.id.tv_amount);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (groupPosition > -1 && groupPosition < requestHistoryArrayList.size()) {
            if (Tools.hasValue(requestHistoryArrayList.get(groupPosition).getLoanID())) {
                viewHolder.tvPeak.setText(requestHistoryArrayList.get(groupPosition).getLoanID());
            } else {
                viewHolder.tvPeak.setText("");
            }
        }
        if (Tools.hasValue(requestHistoryArrayList.get(groupPosition).getAmount())) {
            viewHolder.tvAmount.setText(requestHistoryArrayList.get(groupPosition).getAmount());
        } else {
            viewHolder.tvAmount.setText("");
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void updateData(ArrayList<RequestHistory> data) {
        requestHistoryArrayList = new ArrayList<>();
        requestHistoryArrayList.addAll(data);
        notifyDataSetChanged();
    }

    class ViewHolder {
        TextView tvDate, tvStatus, tvAmount, tvPaid, tvRemaining, tvPeak;
    }
}
