package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.interfaces.OnCardTypeClicked;
import com.bakcell.models.user.userpredefinedobjects.CardType;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;
import java.util.List;

public class TopUpCardTypeDialogAdapter extends RecyclerView.Adapter<TopUpCardTypeDialogAdapter.ViewHolder> {
    private List<CardType> cardTypes = new ArrayList<>();
    private Context context;
    private int selectedItem = 0;
    private OnCardTypeClicked onCardTypeClicked;

    public TopUpCardTypeDialogAdapter(Context context, List<CardType> dataSet, OnCardTypeClicked onCardTypeClicked) {
        if (dataSet != null)
            this.cardTypes = dataSet;
        this.context = context;
        this.onCardTypeClicked = onCardTypeClicked;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_dialog_card_type_item,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CardType cardType = cardTypes.get(position);
        if (cardType != null && Tools.hasValue(cardType.getValue())) {
            String card = cardType.getValue();
            holder.radioButton.setChecked(position == 0);
            String str1 = card.substring(0, 1);
            String str2 = card.substring(1, card.length());
            holder.radioButton.setText(str1.toUpperCase() + str2);

            holder.radioButton.setChecked(position == selectedItem);
            //set Click listener to radioButton
            holder.radioButton.setOnClickListener(v -> {
                updateItemCheckState(position);

            });
        }


    } //end onBindViewHolder

    private void updateItemCheckState(int position) {
        selectedItem = position;
        notifyDataSetChanged();
        if (onCardTypeClicked != null) {
            onCardTypeClicked.onCardClicked(cardTypes.get(position).getKey());
        }
    }

    @Override
    public int getItemCount() {
        return cardTypes.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        RadioButton radioButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.rb_card_type);
        }
    }
}
