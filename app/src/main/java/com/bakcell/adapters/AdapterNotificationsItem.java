package com.bakcell.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.notifications.Notification;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.text.ParseException;
import java.util.ArrayList;


public class AdapterNotificationsItem extends RecyclerView.Adapter<AdapterNotificationsItem.ViewHolder> implements Filterable {

    public static final String NOTIFICATION_ACTION_TYPE_TARIF = "tariff";
    public static final String NOTIFICATION_ACTION_TYPE_SUPPLEMENTARY = "supplementary";

    private NotificationFilter notificationFilter;

    private ArrayList<Notification> notificationItemsArrayList;
    private ArrayList<Notification> notificationItemsArrayListTemp;
    private Context context;

    public AdapterNotificationsItem(Context context, ArrayList<Notification> arrayList) {
        this.notificationItemsArrayList = arrayList;
        this.notificationItemsArrayListTemp = arrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_item_notifications, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        Notification notificationItems = notificationItemsArrayList.get(i);

        viewHolder.prepareListItem(notificationItems);

        if (i == (notificationItemsArrayList.size() - 1)) {
            viewHolder.line_below.setVisibility(View.INVISIBLE);
            viewHolder.line.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.line_below.setVisibility(View.VISIBLE);
            viewHolder.line.setVisibility(View.VISIBLE);
        }

        final int i1 = i;
        viewHolder.btn_renew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context == null) return;
                if (NOTIFICATION_ACTION_TYPE_SUPPLEMENTARY.equalsIgnoreCase(notificationItemsArrayList.get(i1).getActionType())) {
                    Bundle bundle = new Bundle();
                    bundle.putString(SupplementaryOffersActivity.TAB_NAVIGATION_KEY, "");
                    bundle.putString(SupplementaryOffersActivity.OFFER_NAVIGATION_ID_KEY, notificationItemsArrayList.get(i1).getActionId());
                    BakcellLogger.logE("actionIdNotify", "actionId:::" + notificationItemsArrayList.get(i1).getActionId(), "AdpaterNotificationsItem", "OnClick");
                    BaseActivity.startNewActivity((Activity) context, SupplementaryOffersActivity.class, bundle);
                } else if (NOTIFICATION_ACTION_TYPE_TARIF.equalsIgnoreCase(notificationItemsArrayList.get(i1).getActionType())) {
                    RootValues.getInstance().setIsTariffFromDashBoard(true);
                    RootValues.getInstance().setOfferingIdRedirectFromNotifications(notificationItemsArrayList.get(i1).getActionId());

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(TariffsFragment.KEY_DATA_FROM_NOTIFICATION, null);
                    ((HomeActivity) context).selectMenu(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS, bundle);

                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return notificationItemsArrayList.size();
    }

    public void updateAdapter(ArrayList<Notification> arrayList) {
        notificationItemsArrayList = new ArrayList<>();
        notificationItemsArrayList.addAll(arrayList);

        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        if (notificationFilter == null)
            notificationFilter = new NotificationFilter();
        return notificationFilter;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        BakcellTextViewNormal notification_text;
        BakcellTextViewNormal notification_time;
        BakcellButtonNormal btn_renew;
        ImageView notification_icon;

        View line_below;
        View line;

        public ViewHolder(View itemView) {
            super(itemView);
            notification_text = (BakcellTextViewNormal) itemView.findViewById(R.id.notification_text);
            notification_time = (BakcellTextViewNormal) itemView.findViewById(R.id.notification_time);
            btn_renew = (BakcellButtonNormal) itemView.findViewById(R.id.btn_renew);
            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
            line_below = itemView.findViewById(R.id.line_below);
            line = itemView.findViewById(R.id.line);
        }

        public void prepareListItem(Notification notification) {
            if (notification == null) return;

            if (Tools.hasValue(notification.getTextMessage())) {
                notification_text.setText(notification.getTextMessage());
                notification_text.setVisibility(View.VISIBLE);
            } else {
                notification_text.setVisibility(View.INVISIBLE);
            }
            if (Tools.hasValue(notification.getDateTime())) {
                try {
                    notification_time.setText(Tools.getTimeSecMinutesHoursDays(notification.getDateTime(), context));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                notification_time.setVisibility(View.VISIBLE);
            } else {
                notification_time.setVisibility(View.INVISIBLE);
            }

            if (Tools.hasValue(notification.getIcon())) {
                notification_icon.setVisibility(View.VISIBLE);
            } else {
                notification_icon.setVisibility(View.INVISIBLE);
            }

            if (Tools.hasValue(notification.getButtonText())) {
                btn_renew.setVisibility(View.VISIBLE);
                btn_renew.setEnabled(true);
                btn_renew.setText(notification.getButtonText());
                btn_renew.setSelected(true);
            } else {
                btn_renew.setVisibility(View.INVISIBLE);
                btn_renew.setEnabled(false);
            }


        }
    }

    class NotificationFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                if (constraint.toString().trim().length() == 0) {
                    results.values = notificationItemsArrayListTemp;
                    results.count = notificationItemsArrayListTemp.size();
                } else {
                    ArrayList<Notification> filterList = new ArrayList<>();
                    for (Notification notification : notificationItemsArrayListTemp) {
                        if (notification.getTextMessage().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            filterList.add(notification);
                        }
                    }
                    results.count = filterList.size();
                    results.values = filterList;
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notificationItemsArrayList = (ArrayList<Notification>) results.values;
            notifyDataSetChanged();
        }
    }
}
