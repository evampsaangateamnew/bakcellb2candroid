package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;

import com.bakcell.R;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.globals.RootValues;
import com.bakcell.models.supplementaryoffers.OfferFilter;
import com.bakcell.models.supplementaryoffers.OfferFiltersMain;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 8/17/2017.
 */

public class AdapterPackageType extends BaseAdapter {
    private Context context;
    private PackageItemListener packageItemListener;
    private String type;
    private ArrayList<OfferFilter> offerFilters;

    private CheckedTextView checkedTextViewAll;

    public AdapterPackageType(Context context, OfferFiltersMain offerMain,
                              PackageItemListener packageItemListener, String selectedLayout) {
        this.context = context;
        this.packageItemListener = packageItemListener;
        this.type = selectedLayout;
        if (type.equalsIgnoreCase(SupplementaryOffersActivity.LAYOUT_PHONE)) {
            if (offerMain.getApp() != null) {
                if (offerFilters != null) offerFilters.clear();
                addAllToDataSet(offerMain.getApp());
            }
        } else if (type.equalsIgnoreCase(SupplementaryOffersActivity.LAYOUT_TAB)) {
            if (offerMain.getTab() != null) {
                if (offerFilters != null) offerFilters.clear();
                addAllToDataSet(offerMain.getTab());
            }
        } else if (type.equalsIgnoreCase(SupplementaryOffersActivity.LAYOUT_DESKTOP)) {
            if (offerMain.getDesktop() != null) {
                if (offerFilters != null) offerFilters.clear();
                addAllToDataSet(offerMain.getDesktop());
            }
        }
    }

    private void addAllToDataSet(ArrayList<OfferFilter> offerFilters) {
        this.offerFilters = new ArrayList<>(offerFilters);
    }

    @Override
    public int getCount() {
        int size;
        if (offerFilters == null) {
            size = 0;
        } else {
            size = offerFilters.size();
        }
        return size;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_item_package_type, null);
            viewHolder.packageName = (CheckedTextView) convertView.findViewById(R.id.tv_name);
            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (Tools.hasValue(offerFilters.get(position).getValue())) {
            viewHolder.packageName.setText(offerFilters.get(position).getValue());
        }
        final ViewHolder finalViewHolder = viewHolder;

        if (position == 0) {
            checkedTextViewAll = viewHolder.packageName;
        }

        // Set is Checked
        boolean isSelected = false;

        if (RootValues.getInstance().getOfferFiltersCheckedList() != null) {
            for (int i = 0; i < RootValues.getInstance().getOfferFiltersCheckedList().size(); i++) {
                if (RootValues.getInstance().getOfferFiltersCheckedList().get(i).getKey()
                        .equalsIgnoreCase(offerFilters.get(position).getKey())) {
                    isSelected = true;
                    break;
                }
            }
        }


        if (isSelected) {
            finalViewHolder.packageName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.filter_checked, 0,
                    0, 0);
        } else {
            finalViewHolder.packageName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.filter_unchecked, 0,
                    0, 0);
        }


        ////////

        viewHolder.packageName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (offerFilters.get(position).isCheck()) {
                    finalViewHolder.packageName.setChecked(false);
                    offerFilters.get(position).setCheck(false);
                    finalViewHolder.packageName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.filter_unchecked, 0,
                            0, 0);
                    if (offerFilters.get(position).getKey().equalsIgnoreCase("0")) {
                        setAllUnchecked(false);
                        ArrayList<OfferFilter> tempList = new ArrayList<OfferFilter>(offerFilters);

                        RootValues.getInstance().getOfferFiltersCheckedList().removeAll(tempList);
                        int i = 0;
                        i++;
                    } else {
                        RootValues.getInstance().getOfferFiltersCheckedList().remove(offerFilters.get(position));
                        int i = 0;
                        i++;
                    }
                } else {
                    finalViewHolder.packageName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.filter_checked, 0,
                            0, 0);
                    finalViewHolder.packageName.setChecked(true);
                    offerFilters.get(position).setCheck(true);
                    if (offerFilters.get(position).getKey().equalsIgnoreCase("0")) {
                        setAllChecked();

                        ArrayList<OfferFilter> tempList = new ArrayList<OfferFilter>(offerFilters);

                        if (tempList != null) {
                            for (int i = 0; i < tempList.size(); i++) {

                                boolean isExist = false;
                                for (int j = 0; j < RootValues.getInstance().getOfferFiltersCheckedList().size(); j++) {
                                    if (tempList.get(i).getKey().equalsIgnoreCase(RootValues.getInstance().getOfferFiltersCheckedList().get(j).getKey())) {
                                        isExist = true;
                                        break;
                                    }
                                }
                                if (!isExist) {
                                    RootValues.getInstance().getOfferFiltersCheckedList().add(tempList.get(i));
                                }

                            }
                        }


                    } else {
                        boolean isExist = false;
                        for (int i = 0; i < RootValues.getInstance().getOfferFiltersCheckedList().size(); i++) {
                            if (offerFilters.get(position).getKey().equalsIgnoreCase(RootValues.getInstance().getOfferFiltersCheckedList().get(i).getKey())) {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist) {
                            RootValues.getInstance().getOfferFiltersCheckedList().add(offerFilters.get(position));
                        }
                    }

                }
                packageItemListener.onCheckedItemChange(RootValues.getInstance().getOfferFiltersCheckedList(), offerFilters, checkedTextViewAll);

            }
        });
        return convertView;
    }


    public void setAllUnchecked(boolean isFromActivity) {
        if (isFromActivity) {
            packageItemListener.onCheckedItemChange(RootValues.getInstance().getOfferFiltersCheckedList(), offerFilters, checkedTextViewAll);
        }
        notifyDataSetChanged();
    }

    private void setAllChecked() {
        notifyDataSetChanged();
    }

    class ViewHolder {
        private CheckedTextView packageName;
        private CheckBox checkbox;
    }

    public interface PackageItemListener {
        void onCheckedItemChange(ArrayList<OfferFilter> filteredIds, ArrayList<OfferFilter> offerFilters, CheckedTextView checkedTextViewAll);
    }
}
