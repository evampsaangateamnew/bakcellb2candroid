package com.bakcell.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.bakcell.R;
import com.bakcell.models.ulduzum.UlduzumUnusedCodesModel;
import com.bakcell.models.ulduzum.Unusedcodeslist;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

public class UlduzumUnusedCodesAdapter extends ArrayAdapter<Unusedcodeslist> {
    private Context context;
    private UlduzumUnusedCodesModel ulduzumUnusedCodesModel;

    public UlduzumUnusedCodesAdapter(Context context, UlduzumUnusedCodesModel ulduzumUnusedCodesModel) {
        super(context, R.layout.ulduzum_unused_codes_list_item, ulduzumUnusedCodesModel.getUnusedcodeslist());
        this.context = context;
        this.ulduzumUnusedCodesModel = ulduzumUnusedCodesModel;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.ulduzum_unused_codes_list_item, parent, false);
            viewHolder.codeLabel = convertView.findViewById(R.id.codeLabel);
            viewHolder.codeDateLabel = convertView.findViewById(R.id.codeDateLabel);
            viewHolder.codeTimeLabel = convertView.findViewById(R.id.codeTimeLabel);
            //set the tag
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.codeLabel.setSelected(true);
        viewHolder.codeDateLabel.setSelected(true);
        viewHolder.codeTimeLabel.setSelected(true);
        //set the ui
        //set code
        if (Tools.hasValue(ulduzumUnusedCodesModel.getUnusedcodeslist().get(position).getIdentical_code())) {
            viewHolder.codeLabel.setText(ulduzumUnusedCodesModel.getUnusedcodeslist().get(position).getIdentical_code());
        }

        //set code date
        if (Tools.hasValue(ulduzumUnusedCodesModel.getUnusedcodeslist().get(position).getInsert_date())) {
            try {
                String[] parts = ulduzumUnusedCodesModel.getUnusedcodeslist().get(position).getInsert_date().trim().split(" ");
                String date = parts[0];
                String time = parts[1];
                viewHolder.codeDateLabel.setText(date.trim());
                viewHolder.codeTimeLabel.setText(time.trim());
            } catch (Exception exp) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, exp.getMessage());
            }
        }
        return convertView;
    }

    public class ViewHolder {
        BakcellTextViewNormal codeLabel;
        BakcellTextViewNormal codeDateLabel;
        BakcellTextViewNormal codeTimeLabel;
    }//ViewHolder ends
}//class ends
