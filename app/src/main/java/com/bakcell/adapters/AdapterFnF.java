package com.bakcell.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.models.friendfamily.FriendFamily;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeeshan Shabbir on 6/21/2017.
 */

public class AdapterFnF extends BaseAdapter {

    private ArrayList<FriendFamily> fnfList;
    private Context context;
    private UpdateNumberCounter updateNumberCounter;


    public AdapterFnF(ArrayList<FriendFamily> fnfList, Context context, UpdateNumberCounter updateNumberCounter) {
        this.fnfList = fnfList;
        this.context = context;
        this.updateNumberCounter = updateNumberCounter;
    }

    @Override
    public int getCount() {
        return fnfList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_item_fnf, null);
            viewHolder.tvNumber = (TextView) convertView.findViewById(R.id.tv_number);
            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.ivRemove = (ImageView) convertView.findViewById(R.id.iv_remove);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (Tools.hasValue(fnfList.get(position).getMsisdn())) {
            viewHolder.tvNumber.setText(fnfList.get(position).getMsisdn());
        } else {
            viewHolder.tvNumber.setText("");
        }

        if (Tools.hasValue(fnfList.get(position).getCreatedDate())) {
            viewHolder.tvDate.setText(fnfList.get(position).getCreatedDate());
        } else {
            viewHolder.tvDate.setText("");
        }

        viewHolder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(R.layout.diaog_logout_alert, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                BakcellButtonNormal cancelBtn = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_cancel);
                BakcellButtonNormal yesBtn = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_logout);
                BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
                BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
                tvTitle.setText(R.string.dialog_confirmation_lbl);
                yesBtn.setText(R.string.lbl_okay);
                tvMessage.setText(context.getString(R.string.msg_are_you_sure_you_want_to_delete_number,
                        fnfList.get(position).getMsisdn()));

                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            deleteNumber(fnfList.get(position)); //Fixed IOB crash
                        } catch (Exception e) {
                            Logger.debugLog(getClass().getSimpleName(), e.getMessage());
                        }
                        alertDialog.dismiss();
                    }
                });
                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

            }
        });
        return convertView;
    }

    private void deleteNumber(FriendFamily friendFamily) {
        updateNumberCounter.onUpdateCounter(friendFamily);
    }

    public void updateFnf(List<FriendFamily> newFnf) {
        fnfList = new ArrayList<>();
        fnfList.addAll(newFnf);
        notifyDataSetChanged();
    }

    class ViewHolder {
        protected TextView tvNumber, tvDate;
        protected ImageView ivRemove;
    }

    public interface UpdateNumberCounter {
        void onUpdateCounter(FriendFamily friendFamily);
    }
}
