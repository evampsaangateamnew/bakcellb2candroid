package com.bakcell.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Zeeshan Shabbir on 9/21/2017.
 */

public class SelectAmountAdapter extends BaseAdapter {
    Context context;
    String items[] = {};

    private int lastSelectedPos = -1;
    private boolean isOpened = false;

    public SelectAmountAdapter(Context context, String[] item) {
        this.context = context;
        this.items = item;
    }

    public int getLastSelectedPos() {
        return lastSelectedPos;
    }

    public void setLastSelectedPos(int lastSelectedPos) {
        this.lastSelectedPos = lastSelectedPos;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    @Override
    public int getCount() {
        if (items == null) return 0;
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_item_select_amount, null);
            viewHolder.filterTitle = (BakcellTextViewNormal) convertView
                    .findViewById(R.id.tv_filter_title);
            viewHolder.filterTitle.setSelected(true);
            viewHolder.tv_azn = (BakcellTextViewNormal) convertView.findViewById(R.id.tv_azn);
            viewHolder.ivFilterIndicator = (ImageView) convertView.findViewById(R.id.iv_indicator);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (items[position].contentEquals(context.getString(R.string.spinner_label_select_all))) {
            viewHolder.tv_azn.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.tv_azn.setVisibility(View.VISIBLE);
        }


        if (isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_red));
        } else if (!isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else if (!isOpened && position == lastSelectedPos) {
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else {
            viewHolder.ivFilterIndicator.setVisibility(View.INVISIBLE);
        }
        viewHolder.filterTitle.setText(items[position]);
        return convertView;
    }

    class ViewHolder {
        protected BakcellTextViewNormal filterTitle, tv_azn;
        protected ImageView ivFilterIndicator;
    }
}
