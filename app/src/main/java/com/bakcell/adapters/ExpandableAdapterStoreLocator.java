package com.bakcell.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.fragments.menus.StoreLocatorFragment;
import com.bakcell.globals.RootValues;
import com.bakcell.models.storelocator.StoreLocator;
import com.bakcell.models.storelocator.StoreTime;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.StoreLocatorTimingItem;

import java.util.ArrayList;

/**
 * Created by Noman on 6/7/2017.
 */

public class ExpandableAdapterStoreLocator extends BaseExpandableListAdapter {
    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private ArrayList<StoreLocator> storeLocatorList;

    public ExpandableAdapterStoreLocator(Context mContext, ArrayList<StoreLocator> storeLocatorList) {
        this.mContext = mContext;
        this.storeLocatorList = storeLocatorList;
    }


    public void updataData(ArrayList<StoreLocator> newStoreLocatorList) {
        storeLocatorList = new ArrayList<>();
        storeLocatorList.addAll(newStoreLocatorList);
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return storeLocatorList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.store_locator_list_item, null);
            viewHolder.groupIconInducator = (ImageView) convertView.findViewById(R.id.groupIconInducator);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            viewHolder.storeName = (TextView) convertView.findViewById(R.id.tv_store_name);
            viewHolder.tv_address = (TextView) convertView.findViewById(R.id.tv_address);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (Tools.hasValue(storeLocatorList.get(groupPosition).getStore_name())) {
            viewHolder.storeName.setText(storeLocatorList.get(groupPosition).getStore_name());
        } else {
            viewHolder.storeName.setText("");
        }

        if (Tools.hasValue(storeLocatorList.get(groupPosition).getAddress())) {
            viewHolder.tv_address.setText(storeLocatorList.get(groupPosition).getAddress());
        } else {
            viewHolder.tv_address.setText("");
        }

        if (!isExpanded) {
            viewHolder.groupIconInducator.setImageDrawable(ContextCompat.getDrawable(mContext,
                    R.drawable.ic_plus_sign1));
        } else {
            viewHolder.groupIconInducator.setImageDrawable(ContextCompat.getDrawable(mContext,
                    R.drawable.ic_minu_sign1));
        }

        final int pos = groupPosition;
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RootValues.getInstance().getRedirectMapFromStoreListListener().onRedirectMapFromStoreListListen(storeLocatorList.get(pos));
            }
        });

        // Set store locator marker icon
        viewHolder.imageView.setImageResource(StoreLocatorFragment.getMarkerIcon(storeLocatorList.get(groupPosition), false));

        return convertView;
    }


    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.store_locator_list_child_item, null);
            viewHolder.llContactLayout = (LinearLayout) convertView.findViewById(R.id.ll_phone_layout);
            viewHolder.llTimingsLayout = (LinearLayout) convertView.findViewById(R.id.ll_timings);
            viewHolder.contactNumberOne = (TextView) convertView.findViewById(R.id.tv_contact_1);
            viewHolder.contactNumberTwo = (TextView) convertView.findViewById(R.id.tv_contact_2);
            viewHolder.contactNumberThree = (TextView) convertView.findViewById(R.id.tv_contact_3);
            viewHolder.phoneIcon = (ImageView) convertView.findViewById(R.id.phoneIcon);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.contactNumberOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    openDailer(Uri.parse("tel:" + storeLocatorList.get(groupPosition).getContactNumbers().get(0)
                            /*.replaceFirst(".$", "")*/));
                } catch (Exception e) {
                    Logger.debugLog(TAG, e.getMessage());
                }
            }
        });
        viewHolder.contactNumberTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    openDailer(Uri.parse("tel:" + storeLocatorList.get(groupPosition).getContactNumbers().get(1)
                            /*.replaceFirst(".$", "")*/));
                } catch (Exception e) {
                    Logger.debugLog(TAG, e.getMessage());
                }
            }
        });
        viewHolder.contactNumberThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    openDailer(Uri.parse("tel:" + storeLocatorList.get(groupPosition).getContactNumbers().get(2)
                            /*.replaceFirst(".$", "")*/));
                } catch (Exception e) {
                    Logger.debugLog(TAG, e.getMessage());
                }

            }
        });

        ArrayList<StoreTime> storeTimingsList = storeLocatorList.get(groupPosition).getTimings();

        ArrayList<String> contactList = storeLocatorList.get(groupPosition).getContactNumbers();

        if (storeTimingsList != null && storeTimingsList.size() > 0) {
            viewHolder.llTimingsLayout.setVisibility(View.VISIBLE);
            viewHolder.llTimingsLayout.removeAllViews();
            for (int i = 0; i < storeTimingsList.size(); i++) {
                StoreLocatorTimingItem item = new StoreLocatorTimingItem(mContext);
                item.getTvTimings().setText(storeTimingsList.get(i).getTimings());
                item.getTvday().setText(storeTimingsList.get(i).getDay());
                viewHolder.llTimingsLayout.addView(item);
            }

        } else {
            viewHolder.llTimingsLayout.setVisibility(View.GONE);
        }


        if (contactList != null && contactList.size() > 0) {
            viewHolder.llContactLayout.setVisibility(View.VISIBLE);
            viewHolder.phoneIcon.setVisibility(View.VISIBLE);
            try {
                viewHolder.contactNumberOne.setText(contactList.get(0));
            } catch (Exception e) {
                Logger.debugLog(TAG, e.getMessage());
                viewHolder.contactNumberOne.setText("");
            }

            try {
                viewHolder.contactNumberTwo.setText(contactList.get(1));
            } catch (Exception e) {
                Logger.debugLog(TAG, e.getMessage());
                viewHolder.contactNumberTwo.setText("");
            }

            try {
                viewHolder.contactNumberThree.setText(contactList.get(2));
            } catch (Exception e) {
                Logger.debugLog(TAG, e.getMessage());
                viewHolder.contactNumberThree.setText("");
            }
        } else {
            viewHolder.llContactLayout.setVisibility(View.GONE);
            viewHolder.phoneIcon.setVisibility(View.GONE);
        }


        return convertView;
    }

    private void openDailer(Uri parse) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(parse);
            mContext.startActivity(intent);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class ViewHolder {
        TextView storeName, tv_address, contactNumberOne, contactNumberTwo, contactNumberThree;
        LinearLayout llContactLayout, llTimingsLayout;
        ImageView groupIconInducator, phoneIcon, imageView;
    }

}
