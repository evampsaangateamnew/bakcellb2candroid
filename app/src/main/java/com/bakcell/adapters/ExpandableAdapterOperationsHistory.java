package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.operationhistory.OperationHistoryRecord;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

public class ExpandableAdapterOperationsHistory extends BaseExpandableListAdapter {

    private Context context;
    ArrayList<OperationHistoryRecord> operationHistoryArrayList;

    public ExpandableAdapterOperationsHistory(Context context, ArrayList<OperationHistoryRecord> arrayList) {
        this.context = context;
        this.operationHistoryArrayList = arrayList;
    }

    public void updateList(ArrayList<OperationHistoryRecord> arrayList) {
        this.operationHistoryArrayList = new ArrayList<>();
        this.operationHistoryArrayList.addAll(arrayList);
        notifyDataSetChanged();
    }

    public class ViewHolderGroup {
        BakcellTextViewNormal date_time_txt, service_txt, amount_txt, ending_balance_txt, tv_azn, tv_azn_2;
        RelativeLayout endingBalance;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        ViewHolderGroup viewHolderGroup = null;

        if (convertView == null) {
            viewHolderGroup = new ViewHolderGroup();
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_item_operation_history, null);
            viewHolderGroup.date_time_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.date_time_txt);
            viewHolderGroup.service_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.service_txt);
            viewHolderGroup.amount_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.amount_txt);
            viewHolderGroup.endingBalance = (RelativeLayout) convertView.findViewById(R.id.ending_balance);
            viewHolderGroup.ending_balance_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.ending_balance_txt);
            viewHolderGroup.tv_azn = (BakcellTextViewNormal) convertView.findViewById(R.id.tv_azn);
            viewHolderGroup.tv_azn_2 = (BakcellTextViewNormal) convertView.findViewById(R.id.tv_azn_2);
            convertView.setTag(viewHolderGroup);
        } else {
            viewHolderGroup = (ViewHolderGroup) convertView.getTag();
        }

        if (DataManager.getInstance().getCurrentUser() != null && Tools.hasValue(DataManager
                .getInstance().getCurrentUser().getSubscriberType())) {
            if (DataManager.getInstance().getCurrentUser().getSubscriberType()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_POSTPAID)) {
                viewHolderGroup.endingBalance.setVisibility(View.GONE);
            } else {
                viewHolderGroup.endingBalance.setVisibility(View.VISIBLE);
            }
        }


        if (Tools.hasValue(operationHistoryArrayList.get(groupPosition).getDate())) {
            viewHolderGroup.date_time_txt.setText(operationHistoryArrayList.get(groupPosition).getDate());
        } else {
            viewHolderGroup.date_time_txt.setText("");
        }

        if (Tools.hasValue(operationHistoryArrayList.get(groupPosition).getTransactionType())) {
            viewHolderGroup.service_txt.setText(operationHistoryArrayList.get(groupPosition).getTransactionType());
        } else {
            viewHolderGroup.service_txt.setText("");
        }

        if (Tools.hasValue(operationHistoryArrayList.get(groupPosition).getAmount())) {
            viewHolderGroup.amount_txt.setText(operationHistoryArrayList.get(groupPosition).getAmount());

            if (Tools.hasValue(operationHistoryArrayList.get(groupPosition).getCurrency())) {
                if (operationHistoryArrayList.get(groupPosition).getCurrency().equalsIgnoreCase("azn")) {
                    viewHolderGroup.tv_azn.setVisibility(View.VISIBLE);
                } else {
                    viewHolderGroup.amount_txt.append(" " + operationHistoryArrayList.get(groupPosition).getCurrency());
                    viewHolderGroup.tv_azn.setVisibility(View.GONE);
                }
            } else {
                viewHolderGroup.tv_azn.setVisibility(View.VISIBLE);
            }

        } else {
            viewHolderGroup.amount_txt.setText("");
            viewHolderGroup.tv_azn.setVisibility(View.GONE);
        }

        if (Tools.hasValue(operationHistoryArrayList.get(groupPosition).getEndingBalance())) {
            viewHolderGroup.ending_balance_txt.setText(operationHistoryArrayList.get(groupPosition).getEndingBalance());
            viewHolderGroup.tv_azn_2.setVisibility(View.VISIBLE);
        } else {
            viewHolderGroup.ending_balance_txt.setText("");
            viewHolderGroup.tv_azn_2.setVisibility(View.GONE);
        }

        return convertView;
    }


    public class ViewHolderChild {
        BakcellTextViewNormal description_txt, clarification_txt;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolderChild viewHolderChild = null;

        if (convertView == null) {
            viewHolderChild = new ViewHolderChild();
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_exp_adapter_sub_operation_history, null);

            viewHolderChild.description_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.description_txt);
            viewHolderChild.clarification_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.clarification_txt);

            convertView.setTag(viewHolderChild);
        } else {
            viewHolderChild = (ViewHolderChild) convertView.getTag();
        }

        if (Tools.hasValue(operationHistoryArrayList.get(groupPosition).getDescription())) {
            viewHolderChild.description_txt.setText(operationHistoryArrayList.get(groupPosition).getDescription());
        } else {
            viewHolderChild.description_txt.setText("");
        }
        if (Tools.hasValue(operationHistoryArrayList.get(groupPosition).getClarification())) {
            viewHolderChild.clarification_txt.setText(operationHistoryArrayList.get(groupPosition).getClarification());
        } else {
            viewHolderChild.clarification_txt.setText("");
        }

        return convertView;
    }


    @Override
    public String getChild(int groupPosition, int childPosititon) {
        return this.operationHistoryArrayList.get(groupPosition).getTransactionType();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.operationHistoryArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.operationHistoryArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}