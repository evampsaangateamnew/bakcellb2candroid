package com.bakcell.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.TopUpActivity;
import com.bakcell.activities.landing.TopUpAddCardActivity;
import com.bakcell.globals.RootValues;

import java.util.ArrayList;

public class AdapterTopUpVisa extends RecyclerView.Adapter<AdapterTopUpVisa.ViewHolder> {
    ArrayList<String> dataSet = new ArrayList<>();
    Context context;
    // showAddCardOption if set true then Add new Card option should be visible else don't show Add new card option.
    boolean showAddCardOption;
    int selectedItem = 0;

    public AdapterTopUpVisa(ArrayList<String> dataSet, Context context, boolean showAddCardOption) {
        if (dataSet != null)
            this.dataSet = dataSet;
        this.context = context;
        this.showAddCardOption = showAddCardOption;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_topup_visa,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.cardNumber.setText(dataSet.get(position));
        //check if position is selected Item then update selected row color
        //else set properties to normal.
        if (position == selectedItem) {
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.radioButton.setChecked(true);
            holder.cardNumber.setTypeface(RootValues.getInstance().getFontArialBold(), Typeface.BOLD);
        } else {
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.text_gray));
            holder.radioButton.setChecked(false);
            holder.cardNumber.setTypeface(RootValues.getInstance().getFontArialRegular(), Typeface.NORMAL);
        }
        //set Click listener to radioButton
        holder.radioButton.setOnClickListener(v -> {
            int copyOfLastCheckedPosition = selectedItem;
            selectedItem = holder.getAdapterPosition();
            notifyItemChanged(copyOfLastCheckedPosition);
            notifyItemChanged(selectedItem);
        });
        //set Click listener to RecyclerView Row
        holder.itemView.setOnClickListener(view -> {
            //check if showAddCardOption is true & User clicked on last item then go To Add card Activity
            //else update the selected position
            if (showAddCardOption && (position == dataSet.size() - 1) && context instanceof TopUpActivity) {
                BaseActivity.startNewActivity((TopUpActivity) context, TopUpAddCardActivity.class);
            } else {
                int copyOfLastCheckedPosition = selectedItem;
                selectedItem = holder.getAdapterPosition();
                notifyItemChanged(copyOfLastCheckedPosition);
                notifyItemChanged(selectedItem);
            }
        });
        //check if true, then show Add card option & hide radio button for Add card Option in the recyclerview.
        //else show simple card list
        if (showAddCardOption) {
            if (position == dataSet.size() - 1) {
                holder.cardIcon.setImageResource(R.drawable.ic_add_card);
                holder.cardNumber.setTextColor(context.getResources().getColor(R.color.dark_gray));
                holder.radioButton.setVisibility(View.GONE);
            } else {
                if (position == 1) {
                    holder.cardIcon.setImageResource(R.drawable.ic_master_card);
                } else {
                    holder.cardIcon.setImageResource(R.drawable.ic_visa_card);
                }
                holder.radioButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (position == 1) {
                holder.cardIcon.setImageResource(R.drawable.ic_master_card);
            } else {
                holder.cardIcon.setImageResource(R.drawable.ic_visa_card);
            }
            holder.radioButton.setVisibility(View.VISIBLE);

        }
    } //end onBindViewHolder

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView cardNumber;
        ImageView cardIcon;
        RadioButton radioButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardIcon = itemView.findViewById(R.id.card_icon);
            cardNumber = itemView.findViewById(R.id.card_text);
            radioButton = itemView.findViewById(R.id.radio_btn);
        }
    }
}
