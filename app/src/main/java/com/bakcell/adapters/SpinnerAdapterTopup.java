package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import com.bakcell.R;
import com.bakcell.models.user.userpredefinedobjects.CardType;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.List;

public class SpinnerAdapterTopup extends BaseAdapter {

    private List<CardType> cardTypes;
    Context context;
    private int lastSelectedPos = -1;
    private boolean isOpened = false;


    public SpinnerAdapterTopup(List<CardType> cardTypes, Context context) {
        this.cardTypes = cardTypes;
        this.context = context;
    }

    public int getLastSelectedPos() {
        return lastSelectedPos;
    }

    public void setLastSelectedPos(int lastSelectedPos) {
        this.lastSelectedPos = lastSelectedPos;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    @Override
    public int getCount() {
        return cardTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        SpinnerAdapterTopup.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new SpinnerAdapterTopup.ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.money_card_type_spinner_item, null);
            viewHolder.filterTitle = convertView
                    .findViewById(R.id.card_title);
            viewHolder.ivFilterIndicator = convertView.findViewById(R.id.iv_indicator);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (SpinnerAdapterTopup.ViewHolder) convertView.getTag();
        }
        if (isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_red));
        } else if (!isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else if (!isOpened && position == lastSelectedPos) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else {
            viewHolder.ivFilterIndicator.setVisibility(View.INVISIBLE);
        }

        if (Tools.hasValue(cardTypes.get(position).getValue())) {
            viewHolder.filterTitle.setText(cardTypes.get(position).getValue());
        } else {
            viewHolder.filterTitle.setText("---");
        }

        return convertView;

    }

    class ViewHolder {
        protected BakcellTextViewNormal filterTitle;
        protected ImageView ivFilterIndicator;
    }

}
