package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bakcell.R;


public class SpinnerAdapterOperationHistoryFilter extends BaseAdapter {

    Context context;
    String[] spinnerItems;
    private LayoutInflater inflater = null;


    public SpinnerAdapterOperationHistoryFilter(Context contxt, String[] spinnerValues) {
        context = contxt;
        spinnerItems = spinnerValues;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return spinnerItems.length;
    }

    @Override
    public String getItem(int position) {
        return spinnerItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_item_operation_history_filters, null);
            viewHolder = new ViewHolder();
            viewHolder.filter_name = (TextView) convertView.findViewById(R.id.filter_name);
            viewHolder.separator = convertView.findViewById(R.id.view_seperator);
            viewHolder.filter_name.setSelected(true);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.filter_name.setText(spinnerItems[position]);

        return convertView;
    }


    public class ViewHolder {
        TextView filter_name;
        View separator;
    }


}

