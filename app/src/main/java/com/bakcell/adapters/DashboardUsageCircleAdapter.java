package com.bakcell.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.MySubscriptionsActivity;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.fragments.menus.DashboardFragment;
import com.bakcell.globals.RootValues;
import com.bakcell.models.dashboard.freeresources.FreeResoures;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import java.util.ArrayList;

/**
 * Created by Noman on 7/27/2017.
 */

public class DashboardUsageCircleAdapter extends RecyclerView.Adapter<DashboardUsageCircleAdapter.ViewHolder> {
    private Context context;
    private ArrayList<FreeResoures> freeResouresUsageList;
    private boolean isRoaming;
    private boolean isAnim;

    public DashboardUsageCircleAdapter(Context context, ArrayList<FreeResoures>
            freeResouresUsageList, boolean isRoaming, boolean isAnim) {
        this.context = context;
        this.freeResouresUsageList = freeResouresUsageList;
        this.isRoaming = isRoaming;
        this.isAnim = isAnim;
    }

    @Override
    public int getItemCount() {
        //juni1289
        return  freeResouresUsageList == null ? 0 : freeResouresUsageList.size();
//        return freeResouresUsageList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboard_usagecircle_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int pos = position;
        float animValue = 0;

        if (Tools.hasValue(freeResouresUsageList.get(position).getResourcesTitleLabel())) {
            holder.itemTitle.setText(freeResouresUsageList.get(position).getResourcesTitleLabel());
        } else {
            holder.itemTitle.setText("");
        }
        if (Tools.hasValue(freeResouresUsageList.get(position).getResourcesTitleLabel())) {
            if (freeResouresUsageList.get(position).getResourcesTitleLabel().equalsIgnoreCase("sms")) {
                holder.value_name.setVisibility(View.GONE);
            } else {
                holder.value_name.setVisibility(View.VISIBLE);
            }
        }
        if (Tools.hasValue(freeResouresUsageList.get(position).getResourceUnitName())) {
            holder.value_name.setText(freeResouresUsageList.get(position).getResourceUnitName());
        } else {
            holder.value_name.setText("");
        }
        if (Tools.hasValue(freeResouresUsageList.get(position).getRemainingFormatted())) {
            holder.item_value.setText(freeResouresUsageList.get(position).getRemainingFormatted());
            animValue = Tools.getFloatFromString(freeResouresUsageList.get(position).getRemainingFormatted());
        } else {
            holder.item_value.setText("0");
        }

        //discounted value will show
        if (Tools.hasValue(freeResouresUsageList.get(position).getResourceDiscountedText())) {
            holder.valuesLayout.setVisibility(View.GONE);
            holder.discounted_value.setVisibility(View.VISIBLE);
            holder.discounted_value.setText(freeResouresUsageList.get(position).getResourceDiscountedText());
            holder.circlePlusButton.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.selector_plus_button));
            holder.itemTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.item_value.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.value_name.setTextColor(ContextCompat.getColor(context, R.color.white));
            setCircularProgress(holder.fullArcView, 100, 100, holder.item_value,
                    R.color.text_gray, R.color.white, this.isAnim, false, animValue);
        } else if (Tools.isNumeric(freeResouresUsageList.get(position).getResourceInitialUnits())
                && Tools.isNumeric(freeResouresUsageList.get(position).getResourceRemainingUnits())) { // progress value wil show
            holder.valuesLayout.setVisibility(View.VISIBLE);
            holder.discounted_value.setVisibility(View.GONE);
            holder.circlePlusButton.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.selector_plus_button));
            holder.itemTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.item_value.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.value_name.setTextColor(ContextCompat.getColor(context, R.color.white));
            setCircularProgress(holder.fullArcView,
                    Tools.getFloatFromString(freeResouresUsageList.get(position)
                            .getResourceInitialUnits()),
                    Tools.getFloatFromString(freeResouresUsageList.get(position)
                            .getResourceRemainingUnits()), holder.item_value,
                    R.color.text_gray, R.color.white, this.isAnim, false, animValue);
        } else { // Disable circle will show
            holder.valuesLayout.setVisibility(View.VISIBLE);
            holder.discounted_value.setVisibility(View.GONE);
            holder.circlePlusButton.setBackground(ContextCompat.getDrawable(context, R.drawable.plus_normal));
            holder.itemTitle.setTextColor(ContextCompat.getColor(context,
                    R.color.dashboard_circles_text_disabled_color_red));

            holder.item_value.setTextColor(ContextCompat.getColor(context,
                    R.color.white));
            holder.value_name.setTextColor(ContextCompat.getColor(context,
                    R.color.white));

            setCircularProgress(holder.fullArcView, 100, 0, holder.item_value,
                    R.color.text_gray,
                    R.color.text_gray, false, true, animValue);

            //            setCxircularProgress(holder.fullArcView, 100, 100, holder.item_value,
//                    R.color.dashboard_circles_disabled_color_red,
//                    R.color.dashboard_circles_disabled_color_red, false, true);
        }


        holder.circlePlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                if (!isRoaming) {
                    bundle.putString(SupplementaryOffersActivity.TAB_NAVIGATION_KEY,
                            freeResouresUsageList.get(pos).getResourceType());
                } else {
                    bundle.putString(SupplementaryOffersActivity.TAB_NAVIGATION_KEY, DashboardFragment.CIRCLE_RESOURCE_TYPE_ROAMING);
                }
                BaseActivity.startNewActivity((Activity) context, SupplementaryOffersActivity.class, bundle);
            }
        });

        holder.fullArcView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                if (!isRoaming) {
                    bundle.putString(SupplementaryOffersActivity.TAB_NAVIGATION_KEY,
                            freeResouresUsageList.get(pos).getResourceType());
                } else {
                    bundle.putString(SupplementaryOffersActivity.TAB_NAVIGATION_KEY,
                            DashboardFragment.CIRCLE_RESOURCE_TYPE_ROAMING);
                }
                BaseActivity.startNewActivity((Activity) context, MySubscriptionsActivity.class, bundle);
            }
        });

    }

    public void updateUsageCircles(ArrayList<FreeResoures> freeResouresUsageList) {
        if (freeResouresUsageList != null && freeResouresUsageList.size() > 0) {
            this.freeResouresUsageList.addAll(freeResouresUsageList);
            notifyDataSetChanged();
        }
    }

    int coutAnim = 0;

    private void setCircularProgress(DecoView fullArcView, float max, float progress,
                                     final BakcellTextViewNormal item_value,
                                     int maxBarColor, int progressBarColor,
                                     boolean isCircleAnim, final boolean showZero,
                                     float animValue) {

        if (max < progress) {
            progress = max;
        }

        try {
            // Create background track
            fullArcView.addSeries(new SeriesItem.Builder(ContextCompat.getColor(context, maxBarColor))
                    .setRange(0, max, max)
                    .setInitialVisibility(false)
                    .setLineWidth(RootValues.getInstance().getBACK_CIRCULAR_WIDTH())
                    .build());
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        fullArcView.configureAngles(360, 0);
        fullArcView.setRotation(0);

        fullArcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(0000)
                .setDuration(0000)
                .build());

        if (showZero || progress <= 0) return;

//Create data series track


        if (isCircleAnim) {

            final SeriesItem seriesItem1 = new SeriesItem.Builder(ContextCompat.getColor(context, progressBarColor))
                    .setRange(0, max, 0)
                    .setLineWidth(RootValues.getInstance().getCIRCULAR_WIDTH()).setSpinDuration(DashboardFragment.ANIMATION_DURATION)
                    .build();

            int series1Index = fullArcView.addSeries(seriesItem1);

            fullArcView.addEvent(new DecoEvent.Builder(progress).setIndex(series1Index).setDelay(0000).build());

            coutAnim = 0;
            seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
                @Override
                public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
//                    item_value.setText(++coutAnim + "");
                }

                @Override
                public void onSeriesItemDisplayProgress(float percentComplete) {

                }
            });

        } else {

            fullArcView.addSeries(new SeriesItem.Builder(ContextCompat.getColor(context, progressBarColor))
                    .setRange(0, max, progress)
                    .setLineWidth(RootValues.getInstance().getCIRCULAR_WIDTH())
                    .build());
        }

    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public final DecoView fullArcView;

        public final BakcellTextViewNormal itemTitle;
        public final ImageView circlePlusButton;
        public final BakcellTextViewNormal item_value;
        public final BakcellTextViewNormal value_name;
        public final BakcellTextViewNormal discounted_value;
        public final RelativeLayout valuesLayout;
        public final View dummy_view;

        public ViewHolder(View itemView) {
            super(itemView);

            int width = (int) Tools.convertDpToPixel(RootValues.getInstance().getDASHBOARD_CIRCLE_WIDTH(), itemView.getContext());
            itemView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));

            fullArcView = (DecoView) itemView.findViewById(R.id.fullArcView);

            itemTitle = (BakcellTextViewNormal) itemView.findViewById(R.id.itemTitle);
            itemTitle.setSelected(true);
            circlePlusButton = (ImageView) itemView.findViewById(R.id.circlePlusButton);
            item_value = (BakcellTextViewNormal) itemView.findViewById(R.id.item_value);
            value_name = (BakcellTextViewNormal) itemView.findViewById(R.id.value_name);
            discounted_value = (BakcellTextViewNormal) itemView.findViewById(R.id.discounted_value);
            valuesLayout = (RelativeLayout) itemView.findViewById(R.id.valuesLayout);
            dummy_view = itemView.findViewById(R.id.dummy_view);
        }

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
