package com.bakcell.adapters;

import android.content.Context;
import android.os.Handler;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.LeadingMarginSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.ChangeTariffListener;
import com.bakcell.interfaces.UpdateListOnSuccessfullySurvey;
import com.bakcell.models.inappsurvey.Answers;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Questions;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.inappsurvey.UserSurveys;
import com.bakcell.models.tariffs.TariffCinNew;
import com.bakcell.models.tariffs.tariffcinobject.TariffCin;
import com.bakcell.models.tariffs.tariffcinobject.description.Description;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.Attributes;
import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.TariffDetailLayout;
import com.bakcell.viewsitem.ViewItemTariffsAttribute;
import com.bakcell.webservices.changetariff.ChangeTariff;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Zeeshan Shabbir on 7/27/2017.
 */

public class AdapterTraiffsCin extends RecyclerView.Adapter<AdapterTraiffsCin.ViewHolder> {
    Context context;
    private static boolean isDetailExpanded;
    private static boolean isDescriptionExpanded;
    private String CIN = "cin";
    ArrayList<TariffCin> cinArrayList;
    private int rateStars = 0;

    public AdapterTraiffsCin(Context context, ArrayList<TariffCin> cinArrayList, boolean detailExp, boolean descExp) {
        this.context = context;
        isDetailExpanded = detailExp;
        isDescriptionExpanded = descExp;
        this.cinArrayList = cinArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_tariffs_cin, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.prepareListItem(context, position);

        if (cinArrayList.get(position).getHeader() != null) {
            holder.prepareHeaderItem(cinArrayList.get(position).getHeader(), holder);
        } else {
            holder.prepareHeaderItem(null, holder);
        }

        if (cinArrayList.get(position).getDetails() != null) {
            holder.prepareDetailItem(cinArrayList.get(position).getDetails(), holder);
        } else {
            holder.prepareDetailItem(null, holder);
        }

        if (cinArrayList.get(position).getDescription() != null) {
            holder.prepareDescriptionItem(cinArrayList.get(position).getDescription(), holder);
        } else {
            holder.prepareDescriptionItem(null, holder);
        }

        final int pos1 = position;
        // Subscribe Button process

        holder.button_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (context != null && cinArrayList != null && cinArrayList.get(pos1) != null && cinArrayList.get(pos1).getHeader() != null) {
                    processOnSubscribeButton(context, cinArrayList.get(pos1).getHeader().getOfferingId(), cinArrayList.get(pos1).getHeader().getName(), cinArrayList.get(pos1).getHeader().getSubscribable());
                }
            }
        });
        Data data = PrefUtils.getInAppSurvey(context).getData();
        if (data != null && data.getUserSurveys() != null) {
            List<UserSurveys> userSurveys = data.getUserSurveys();
            for (int i = 0; i < userSurveys.size(); i++) {
                if (userSurveys.get(i).getOfferingId().equals(cinArrayList.get(pos1).getHeader().getOfferingId())) {
                    if (userSurveys.get(i).getAnswer() != null) {
                        String surveyId = userSurveys.get(i).getSurveyId();
                        if (surveyId != null) {
                            Surveys survey = data.findSurveyById(Integer.parseInt(surveyId));
                            if (Tools.isNumeric(userSurveys.get(i).getAnswerId())) {
                                if (Tools.isNumeric(userSurveys.get(i).getQuestionId())) {
                                    rateStars = setStarsCinHolder(Integer.parseInt(userSurveys.get(i).getAnswerId()),
                                            Integer.parseInt(userSurveys.get(i).getQuestionId()), survey, holder);
                                }
                            }
                        }
                    } else {
                      rateStars =   setStarsCinHolder(0,0,null, holder);
                    }
                }
            }
        }
        final int finalRateStars = rateStars;
        holder.ratingStarsLayout.setOnClickListener(
                v -> {
                    String tariffId = cinArrayList.get(position).getHeader().getOfferingId();
                    inAppFeedback(context,tariffId,finalRateStars , position);
                }
        );
    }

    private int setStarsCinHolder(int answerId, int questionId, Surveys survey, ViewHolder viewHolder) {
        if (survey != null) {
            Questions question = survey.findQuestion(questionId);
            if (question != null) {
                List<Answers> answers = question.getAnswers();
                if (answers != null) {
                    if (answers.size() > 0 && answers.get(0).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(false);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 1;
                    } else if (answers.size() > 1 && answers.get(1).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 2;
                    } else if (answers.size() > 2 && answers.get(2).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 3;
                    } else if (answers.size() > 3 && answers.get(3).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(false);
                        return 4;
                    } else if (answers.size() > 4 && answers.get(4).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(true);
                        return 5;
                    }

                } else {
                    viewHolder.starOne.setChecked(false);
                    viewHolder.starTwo.setChecked(false);
                    viewHolder.starThree.setChecked(false);
                    viewHolder.starFour.setChecked(false);
                    viewHolder.starFive.setChecked(false);
                    return 0;
                }
            } else {
                viewHolder.starOne.setChecked(false);
                viewHolder.starTwo.setChecked(false);
                viewHolder.starThree.setChecked(false);
                viewHolder.starFour.setChecked(false);
                viewHolder.starFive.setChecked(false);
                return 0;
            }
        } else {
            viewHolder.starOne.setChecked(false);
            viewHolder.starTwo.setChecked(false);
            viewHolder.starThree.setChecked(false);
            viewHolder.starFour.setChecked(false);
            viewHolder.starFive.setChecked(false);
            return 0;
        }
        return 0;
    }

    private  void inAppFeedback(Context context, String tariffId,int rate, int position) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TARIFF_PAGE);
            if (surveys != null) {
                InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                if (surveys.getQuestions() != null) {
                    currentVisit = 0;
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(new UpdateListOnSuccessfullySurvey() {
                        @Override
                        public void updateListOnSuccessfullySurvey() {
                            notifyItemChanged(position);
                        }
                    });
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "1", tariffId, rate);

                }
            }
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE, currentVisit);
    }

    private void processOnSubscribeButton(Context context, String offeringId, String tariffName, String subscribable) {
        if (context == null) return;

        ChangeTariff.requestForChangeTariffDialog(context, offeringId, tariffName, subscribable, CIN, new ChangeTariffListener() {
            @Override
            public void onChangeTariffListenerSuccess(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_SUCCESS,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }

            @Override
            public void onChangeTariffListenerFailure(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_FAILURE,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (cinArrayList == null)
            return 0;
        else
            return cinArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout secondLayout, thirdLayout;
        ExpandableLayout expandable_title_layout, expandable_description_layout, expandable_detail_layout;
        private ImageView detail_expand_icon, description_expand_icon;
        HeaderLayout headerLayout = new HeaderLayout();
        TariffDetailLayout detailLayout = new TariffDetailLayout();
        DescriptionLayout descriptionLayout = new DescriptionLayout();

        BakcellButtonNormal button_subscribe;
        ConstraintLayout ratingStarsLayout;
        CheckBox starOne, starTwo, starThree, starFour, starFive;

        View line_belew_expanable_title, detail_belew_line, description_belew_line;

        BakcellTextViewNormal detail_title_txt;
        BakcellTextViewNormal description_title_txt;

        class HeaderLayout {
            BakcellTextViewBold cin_title_txt;
            BakcellTextViewNormal bonus_txt, tv_bonus_text, tv_call_lbl, tv_call_value, tv_sms_lbl,
                    tv_sms_value, tv_internet_lbl, tv_internet_value, tv_InternetSubTitle,
                    tv_InternetSubTitleValue, tv_call_left_value;
            ImageView call_img, sms_img, internet_img;
            LinearLayout call_content_layout, sms_content_layout, internet_content_layout;
            View seperator;
        }

        class DescriptionLayout {
            RelativeLayout advantage_header_layout, clarification_header_layout;
            BakcellTextViewNormal advantage_txt_title, clarification_txt_title,
                    tv_advantages_description, every_subcriber_txt;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            int width = (int) Tools.convertDpToPixel(RootValues.getInstance().getTARIFFS_WIDTH(), itemView.getContext());
            itemView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
            //tv_bonus_text = (TextView) itemView.findViewById(R.id.tv_bonus_text);
            secondLayout = (LinearLayout) itemView.findViewById(R.id.second_layout);
            thirdLayout = (LinearLayout) itemView.findViewById(R.id.third_layout);
            expandable_title_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_title_layout);
            expandable_detail_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_detail_layout);
            expandable_description_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_description_layout);
            description_expand_icon = (ImageView) itemView.findViewById(R.id.description_expand_icon);
            detail_expand_icon = (ImageView) itemView.findViewById(R.id.detail_expand_icon);

            //Headers view
            headerLayout.cin_title_txt = (BakcellTextViewBold) itemView.findViewById(R.id.cin_title_txt);
            headerLayout.bonus_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.bonus_txt);
            headerLayout.tv_bonus_text = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_bonus_text);
            headerLayout.tv_bonus_text.setSelected(true);
            headerLayout.tv_call_lbl = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_call_lbl);
            headerLayout.tv_call_value = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_call_value);
            headerLayout.tv_call_value.setSelected(true);
            headerLayout.seperator = (View) itemView.findViewById(R.id.seperator);
            headerLayout.tv_sms_lbl = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_sms_lbl);
            headerLayout.tv_sms_value = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_sms_value);
            headerLayout.tv_internet_lbl = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_internet_lbl);
            headerLayout.tv_internet_value = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_internet_value);
            headerLayout.tv_InternetSubTitle = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitle);

            headerLayout.tv_InternetSubTitleValue = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitleValue);
            headerLayout.tv_call_left_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_call_left_value);
            headerLayout.tv_call_left_value.setSelected(true);

            headerLayout.call_content_layout = (LinearLayout) itemView.findViewById(R.id.call_content_layout);
            headerLayout.sms_content_layout = (LinearLayout) itemView.findViewById(R.id.sms_content_layout);
            headerLayout.internet_content_layout = (LinearLayout) itemView.findViewById(R.id.internet_content_layout);

            headerLayout.call_img = (ImageView) itemView.findViewById(R.id.call_img);
            headerLayout.sms_img = (ImageView) itemView.findViewById(R.id.call_img);
            headerLayout.internet_img = (ImageView) itemView.findViewById(R.id.call_img);

            button_subscribe = (BakcellButtonNormal) itemView.findViewById(R.id.button_subscribe);

            // Rating Stars
            ratingStarsLayout = itemView.findViewById(R.id.ratingStarsLayout);
            starOne = itemView.findViewById(R.id.star1);
            starTwo = itemView.findViewById(R.id.star2);
            starThree = itemView.findViewById(R.id.star3);
            starFour = itemView.findViewById(R.id.star4);
            starFive = itemView.findViewById(R.id.star5);

            //Description view

            descriptionLayout.advantage_header_layout = (RelativeLayout) itemView.findViewById(R.id.advantage_header_layout);
            descriptionLayout.clarification_header_layout = (RelativeLayout) itemView.findViewById(R.id.clarification_header_layout);

            descriptionLayout.every_subcriber_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.every_subcriber_txt);
            descriptionLayout.tv_advantages_description = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_advantages_description);

            descriptionLayout.advantage_txt_title = (BakcellTextViewNormal) itemView.findViewById(R.id.advantage_txt);
            descriptionLayout.clarification_txt_title = (BakcellTextViewNormal) itemView.findViewById(R.id.clarification_txt);

            line_belew_expanable_title = itemView.findViewById(R.id.line_belew_expanable_title);
            detail_belew_line = itemView.findViewById(R.id.detail_belew_line);
            description_belew_line = itemView.findViewById(R.id.description_belew_line);

            detail_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.detail_title_txt);
            detail_title_txt.setSelected(true);

            description_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.description_title_txt);
            description_title_txt.setSelected(true);

            // Init Detail View
            detailLayout = TariffsFragment.loadDetailUI_Ids(itemView);


        }

        void prepareListItem(final Context context, final int i) {


            secondLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDetailExpanded) {
                        isDetailExpanded = false;
                    } else {
                        isDetailExpanded = true;
                    }
                    isDescriptionExpanded = false;
                    notifyDataSetChanged();
                }
            });

            thirdLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDescriptionExpanded) {
                        isDescriptionExpanded = false;
                    } else {
                        isDescriptionExpanded = true;
                    }
                    isDetailExpanded = false;
                    notifyDataSetChanged();
                }
            });


            if (isDetailExpanded) {
                if (cinArrayList.get(i).getDetails() != null) {
                } else {
                    return;
                }
                expandable_detail_layout.setExpanded(true);
                expandable_title_layout.setExpanded(false);
                expandable_description_layout.setExpanded(false);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
                description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
            } else if (isDescriptionExpanded) {
                if (cinArrayList.get(i).getDescription() != null) {
                } else {
                    return;
                }
                expandable_detail_layout.setExpanded(false);
                expandable_title_layout.setExpanded(false);
                expandable_description_layout.setExpanded(true);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
            } else {
                expandable_detail_layout.setExpanded(false);
                expandable_description_layout.setExpanded(false);
                expandable_title_layout.setExpanded(true);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
            }
        }

        void prepareHeaderItem(final Header header, ViewHolder holder) {
            holder.headerLayout.cin_title_txt.setText("");
            holder.headerLayout.bonus_txt.setText("");
            holder.headerLayout.tv_bonus_text.setText("");
            holder.headerLayout.tv_call_lbl.setText("");
            holder.headerLayout.tv_call_value.setText("");
            holder.headerLayout.tv_internet_lbl.setText("");
            holder.headerLayout.tv_internet_value.setText("");
            holder.headerLayout.tv_InternetSubTitleValue.setText("");
            holder.headerLayout.tv_InternetSubTitle.setText("");
            holder.headerLayout.tv_sms_lbl.setText("");
            holder.headerLayout.tv_sms_value.setText("");
            if (Tools.hasValue(header.getName())) {
                headerLayout.cin_title_txt.setText(header.getName());
                headerLayout.cin_title_txt.setSelected(true);
            }
            if (Tools.hasValue(header.getBonusTitle())) {
                headerLayout.bonus_txt.setText(header.getBonusTitle() + ":");
                headerLayout.bonus_txt.measure(0, 0);
            } else {
                headerLayout.bonus_txt.setVisibility(View.GONE);
            }
            if (header.getBonusDescription() != null) {
                headerLayout.bonus_txt.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int width = headerLayout.bonus_txt.getWidth();
                        LeadingMarginSpan span = new LeadingMarginSpan.Standard(width, 0);
                        SpannableString spannableString = new SpannableString(header.getBonusDescription());
                        spannableString.setSpan(span, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        headerLayout.tv_bonus_text.setText(spannableString);
                    }
                }, 1);
            }
            if (header.getCall() != null) {

                String priceTemple = "";
                if (Tools.hasValue(header.getCall().getPriceTemplate())) {
                    priceTemple = header.getCall().getPriceTemplate();
                }
                boolean isDoubleVal = false;
                if (priceTemple.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)) {
                    holder.headerLayout.tv_call_left_value.setVisibility(View.VISIBLE);
                    isDoubleVal = true;
                } else {
                    holder.headerLayout.tv_call_left_value.setVisibility(View.INVISIBLE);
                }

                if (Tools.hasValue(header.getCall().getTitleValueRight())) {
                    holder.headerLayout.tv_call_value.setText(header.getCall().getTitleValueRight());
                }
                if (Tools.hasValue(header.getCall().getTitleValueLeft())) {
                    holder.headerLayout.tv_call_left_value.setText(header.getCall().getTitleValueLeft());
                }

                if (isDoubleVal && Tools.hasValue(header.getCall().getTitleValueLeft())
                        && Tools.hasValue(header.getCall().getTitleValueRight())) {
                    holder.headerLayout.seperator.setVisibility(View.VISIBLE);
                } else {
                    holder.headerLayout.seperator.setVisibility(View.GONE);
                }

                if (Tools.hasValue(header.getCall().getTitle())) {
                    holder.headerLayout.tv_call_lbl.setText(header.getCall().getTitle());
                    holder.headerLayout.tv_call_lbl.setSelected(true);
                }

//                Tools.setTariffsIcons(header.getCall().getIconName(), holder.headerLayout.call_img, context);

                if (header.getCall().getAttributes() != null) {
                    headerLayout.call_content_layout.removeAllViews();
                    Handler handler = new Handler();
                    final String finalPriceTemple = priceTemple;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < header.getCall().getAttributes().size(); i++) {
                                ViewItemTariffsAttribute viewItemTariffsAttribute =
                                        new ViewItemTariffsAttribute(context);
                                boolean isDoubleVal = false;
                                if (finalPriceTemple.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)
                                        || finalPriceTemple.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_VALUES)) {
                                    viewItemTariffsAttribute.getRl_left().setVisibility(View.VISIBLE);
                                    isDoubleVal = true;
                                } else {
                                    viewItemTariffsAttribute.getRl_left().setVisibility(View.INVISIBLE);
                                }
                                viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                                viewItemTariffsAttribute.getCredit_value_txt_2().setText("");
                                viewItemTariffsAttribute.getAttribute_label_text().setText("");
                                if (Tools.hasValue(header.getCall().getAttributes().get(i).getTitle()))
                                    viewItemTariffsAttribute.getAttribute_label_text().setText(header.getCall().getAttributes().get(i).getTitle());

                                if (Tools.hasValue(header.getCall().getAttributes().get(i).getValueLeft())) {
                                    String value = header.getCall().getAttributes().get(i).getValueLeft().trim();
                                    if (!Tools.isNumeric(value)) {
                                        viewItemTariffsAttribute.hideLeftAZN();
                                    } else {
                                        viewItemTariffsAttribute.showLeftAZN();
                                    }
                                    if (Tools.isValueRed(value)) {
                                        viewItemTariffsAttribute.setLeftValueTextColor(R.color.colorPrimary);
                                    } else {
                                        viewItemTariffsAttribute.setLeftValueTextColor(R.color.black);
                                    }
                                    viewItemTariffsAttribute.getCredit_value_txt_2().setText(value);
                                } else {
                                    viewItemTariffsAttribute.hideLeftAZN();
                                }
                                if (Tools.hasValue(header.getCall().getAttributes().get(i).getValueRight())) {
                                    String value = header.getCall().getAttributes().get(i).getValueRight().trim();
                                    if (!Tools.isNumeric(value)) {
                                        viewItemTariffsAttribute.hideRightAZN();
                                    } else {
                                        viewItemTariffsAttribute.showRightAZN();
                                    }
                                    if (Tools.isValueRed(value)) {
                                        viewItemTariffsAttribute.setRightValueTextColor(R.color.colorPrimary);
                                    } else {
                                        viewItemTariffsAttribute.setRightValueTextColor(R.color.black);
                                    }
                                    viewItemTariffsAttribute.getCredit_value_txt_1().setText(value);
                                } else {
                                    viewItemTariffsAttribute.hideRightAZN();
                                }
                                // Hide/Show Saperat
                                if (isDoubleVal && Tools.hasValue(header.getCall().getAttributes().get(i).getValueLeft())
                                        && Tools.hasValue(header.getCall().getAttributes().get(i).getValueRight())) {
                                    viewItemTariffsAttribute.getSeperator().setVisibility(View.VISIBLE);
                                    viewItemTariffsAttribute.getCredit_value_txt_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                                    viewItemTariffsAttribute.getTv_tzn_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                                } else {
                                    viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                                }
                                headerLayout.call_content_layout.addView(viewItemTariffsAttribute);
                            }
                        }
                    }, 10);

                }
            }
            if (header.getSms() != null) {
                if (header.getSms().getTitle() != null) {
                    holder.headerLayout.tv_sms_lbl.setText(header.getSms().getTitle());
                    holder.headerLayout.tv_sms_lbl.setSelected(true);
                }
                if (header.getSms().getTitleValue() != null) {
                    holder.headerLayout.tv_sms_value.setText(header.getSms().getTitleValue());
                }

                if (header.getSms().getAttributes() != null) {
                    addAttributesToHeader(header.getSms().getAttributes(), holder.headerLayout.sms_content_layout);
                }

//                Tools.setTariffsIcons(header.getSms().getIconName(), holder.headerLayout.sms_img, context);

            }
            if (header.getInternet() != null) {
                if (header.getInternet().getTitle() != null) {
                    holder.headerLayout.tv_internet_lbl.setText(header.getInternet().getTitle());
                    holder.headerLayout.tv_internet_lbl.setSelected(true);
                }
                if (header.getInternet().getTitleValue() != null) {
                    holder.headerLayout.tv_internet_value.setText(header.getInternet().getTitleValue());
                }

                if (header.getInternet().getAttributes() != null) {
                    addAttributesToHeader(header.getInternet().getAttributes(), holder.headerLayout.internet_content_layout);
                }
                if (Tools.hasValue(header.getInternet().getSubTitle())) {
                    holder.headerLayout.tv_InternetSubTitle.setText(header.getInternet().getSubTitle());
                }
                if (Tools.hasValue(header.getInternet().getSubTitleValue())) {
                    holder.headerLayout.tv_InternetSubTitleValue.setText(header.getInternet().getSubTitleValue());
                }

//                Tools.setTariffsIcons(header.getInternet().getIconName(), holder.headerLayout.internet_img, context);

            }

            //handling subscribe button.!

            if (header.getSubscribable() != null) {

                if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_NOT_SUBSCRIBE)) {

                    button_subscribe.setEnabled(false);
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                    ratingStarsLayout.setVisibility(View.GONE);

                } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.ALREADY_SUBSCRIBED)) {

                    button_subscribe.setText(R.string.subscribed_lbl);
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                    button_subscribe.setEnabled(false);
                    ratingStarsLayout.setVisibility(View.VISIBLE);

                } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.RENEWABLE)) {

                    button_subscribe.setText(R.string.renew_lbl);
                    button_subscribe.setEnabled(true);
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    ratingStarsLayout.setVisibility(View.VISIBLE);

                } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_SUBSCRIBE)) {

                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.black));
                    button_subscribe.setText(R.string.subscribe_lbl);
                    button_subscribe.setEnabled(true);
                    ratingStarsLayout.setVisibility(View.GONE);

                } else {
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                    button_subscribe.setEnabled(false);
                    ratingStarsLayout.setVisibility(View.GONE);
                }
                button_subscribe.setSelected(true);


            }


        }

        void prepareDetailItem(TariffDetails offerDetail, ViewHolder holder) {
            if (offerDetail == null) {
                holder.secondLayout.setVisibility(View.GONE);
                holder.detail_belew_line.setVisibility(View.GONE);
                return;
            }
            holder.detail_belew_line.setVisibility(View.VISIBLE);
            holder.secondLayout.setVisibility(View.VISIBLE);

            if (Tools.hasValue(offerDetail.getDetailLabel())) {
                detail_title_txt.setText(offerDetail.getDetailLabel());
            }

            TariffsFragment.populateDetailSectionContents(offerDetail, detailLayout, context);

        }

        void prepareDescriptionItem(Description description, ViewHolder holder) {
            if (description == null) {
                holder.thirdLayout.setVisibility(View.GONE);
                holder.description_belew_line.setVisibility(View.GONE);
                return;
            }

            if (Tools.hasValue(description.getDescLabel())) {
                holder.description_title_txt.setText(description.getDescLabel());
            }

            holder.description_belew_line.setVisibility(View.VISIBLE);
            holder.thirdLayout.setVisibility(View.VISIBLE);
            holder.descriptionLayout.every_subcriber_txt.setText("");
            holder.descriptionLayout.tv_advantages_description.setText("");

            holder.descriptionLayout.advantage_txt_title.setText("");
            holder.descriptionLayout.clarification_txt_title.setText("");
            holder.descriptionLayout.advantage_header_layout.setVisibility(View.GONE);
            holder.descriptionLayout.clarification_header_layout.setVisibility(View.GONE);

            if (description.getAdvantages() != null) {
                holder.descriptionLayout.advantage_header_layout.setVisibility(View.VISIBLE);
                if (Tools.hasValue(description.getAdvantages().getTitle())) {
                    holder.descriptionLayout.advantage_txt_title.setText(description.getAdvantages().getTitle());
                }
                if (description.getAdvantages().getDescription() != null) {
                    holder.descriptionLayout.tv_advantages_description.setText(description.getAdvantages().getDescription());
                }
            }

            if (description.getClassification() != null) {
                holder.descriptionLayout.clarification_header_layout.setVisibility(View.VISIBLE);

                if (Tools.hasValue(description.getClassification().getTitle())) {
                    holder.descriptionLayout.clarification_txt_title.setText(description.getClassification().getTitle());
                }

                if (description.getClassification().getDescription() != null) {
                    holder.descriptionLayout.every_subcriber_txt.setText(description.getClassification().getDescription());
                }
            }
        }

        void addAttributesToHeader(final ArrayList<Attributes> attributes, final LinearLayout linearLayout) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    linearLayout.removeAllViews();
                    for (int i = 0; i < attributes.size(); i++) {
                        ViewItemTariffsAttribute item =
                                new ViewItemTariffsAttribute(context);
                        item.getAttribute_label_text().setText("");
                        item.getCredit_value_txt_1().setText("");
                        if (attributes.get(i).getTitle() != null)
                            item.getAttribute_label_text().setText(attributes.get(i).getTitle());
                        if (attributes.get(i).getValue() != null) {
                            String value = attributes.get(i).getValue().trim();
                            item.getCredit_value_txt_1().setText(value);
                            if (Tools.isValueRed(value)) {
                                item.setRightValueTextColor(R.color.colorPrimary);
                            } else {
                                item.setRightValueTextColor(R.color.black);
                            }
                            if (!Tools.isNumeric(value)) {
                                item.hideRightAZN();
                            }
                            item.getSeperator().setVisibility(View.GONE);
                        }
                        linearLayout.addView(item);
                    }
                }
            }, 10);

        }
    }


}
