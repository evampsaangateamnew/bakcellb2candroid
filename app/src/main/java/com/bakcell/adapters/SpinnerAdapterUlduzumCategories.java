package com.bakcell.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.models.ulduzum.UlduzumCategoriesSpinnerModel;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

public class SpinnerAdapterUlduzumCategories extends ArrayAdapter<UlduzumCategoriesSpinnerModel> {

    private ArrayList<UlduzumCategoriesSpinnerModel> categoriesNamesList = null;
    private Context context = null;
    private LayoutInflater inflater = null;

    public SpinnerAdapterUlduzumCategories(Context context, ArrayList<UlduzumCategoriesSpinnerModel> categoriesNamesList) {
        super(context, R.layout.ulduzum_location_categories_spinner_item, R.id.categoryId, categoriesNamesList);
        this.categoriesNamesList = categoriesNamesList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }//SpinnerAdapterUlduzumCategories ends

    @Override
    public int getCount() {
        return categoriesNamesList.size();
    }//getCount ends

    @Nullable
    @Override
    public UlduzumCategoriesSpinnerModel getItem(int position) {
        return super.getItem(position);
    }//getItem ends

    @Override
    public long getItemId(int position) {
        return position;
    }//getItemId ends

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.ulduzum_location_categories_spinner_item, null);
            viewHolder = new ViewHolder();
            viewHolder.categoryIdLabel = convertView.findViewById(R.id.categoryId);
            viewHolder.separator = convertView.findViewById(R.id.view_seperator);
            viewHolder.categoryNameLabel = convertView.findViewById(R.id.categoryName);
            viewHolder.categoryCode = convertView.findViewById(R.id.categoryCode);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.categoryNameLabel.setSelected(true);

        //set the name
        viewHolder.categoryNameLabel.setText(categoriesNamesList.get(position).category);

        return convertView;
    }//getView ends

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getDropDownView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.ulduzum_location_categories_spinner_item, null);
            viewHolder = new ViewHolder();
            viewHolder.categoryIdLabel = convertView.findViewById(R.id.categoryId);
            viewHolder.separator = convertView.findViewById(R.id.view_seperator);
            viewHolder.categoryNameLabel = convertView.findViewById(R.id.categoryName);
            viewHolder.categoryCode = convertView.findViewById(R.id.categoryCode);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.categoryNameLabel.setSelected(true);
        //set the name
        viewHolder.categoryNameLabel.setText(categoriesNamesList.get(position).category);

        return convertView;
    }//getDropDownView ends

    public class ViewHolder {
        BakcellTextViewNormal categoryNameLabel;
        View separator;
        TextView categoryIdLabel, categoryCode;
    }//ViewHolder ends
}//class ends
