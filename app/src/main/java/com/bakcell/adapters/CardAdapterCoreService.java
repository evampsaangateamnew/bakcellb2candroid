package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.activities.landing.CoreServicesActivity;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.CoreServiceStatusChangeListener;
import com.bakcell.interfaces.SubscribeUnsubscribeListener;
import com.bakcell.models.coreservices.CoreServicesList;
import com.bakcell.models.coreservices.CoreServicesMain;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.ViewItemCoreServices;
import com.bakcell.webservices.subscribeunsubscribe.SubscribeUnsubscribe;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.text.ParseException;
import java.util.ArrayList;

import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.bakcell.webservices.subscribeunsubscribe.SubscribeUnsubscribe.OFFER_UNSUBSCRIBE;

/**
 * Created by Zeeshan Shabbir on 10/10/2017.
 */

public class CardAdapterCoreService extends RecyclerView.Adapter<CardAdapterCoreService.ViewHolder> {
    private Context context;
    private final String fromClass = "CardAdapterCoreService";
    private CoreServiceStatusChangeListener coreServiceStatusChangeListener = null;
    public static final String SWITCH_STATUS = "active";
    private static final String OFFERING_ID_CALL_FORWARD = "165811270";
    public static final String OFFERING_ID_I_AM_BACK = "102341307";
    public static final String OFFERING_ID_I_CALLED_YOU = "984855324";
    public static final String OFFERING_ID_TURNED_OFF = "984855324";
    public static final String OFFERING_ID_BUSY = "1479418507";
    public static final String SUSPENDED = "Suspended";
    private CoreServicesActivity.CoreServiceCallForwardListener callForwardListner;
    ArrayList<CoreServicesMain> coreServices;
//    private boolean isIcalledYouBack;

    boolean is_turned_off_suspended = false;
    boolean activeInActiveStatus = false;

    public CardAdapterCoreService(CoreServicesActivity.CoreServiceCallForwardListener callForwardListner, ArrayList<CoreServicesMain> coreServices, Context context, boolean is_turned_off_suspended, boolean activeInActiveStatus) {
        this.callForwardListner = callForwardListner;
        this.coreServices = coreServices;
        this.context = context;
        this.is_turned_off_suspended = is_turned_off_suspended;
        this.activeInActiveStatus = activeInActiveStatus;
        registerCoreServiceStatusChangerListener();
    }

    private void registerCoreServiceStatusChangerListener() {
        coreServiceStatusChangeListener = new CoreServiceStatusChangeListener() {
            @Override
            public void updateStatus(String offerId, boolean isActive) {
                updateSwitchStatus(offerId, isActive);
            }
        };

        RootValues.getInstance().setCoreServiceStatusChangeListener(coreServiceStatusChangeListener);
    }

    private void updateSwitchStatus(String offerId, boolean isActive) {
        for (int i = 0; i < coreServices.size(); i++) {
            for (int j = 0; j < coreServices.get(i).getCoreServicesList().size(); j++) {
                BakcellLogger.logE("manageNumber", "offerId:::" + coreServices.get(i).getCoreServicesList().get(j).getOfferingId() +
                        " to check:::" + offerId + " status:::" + isActive, "CardAdapterCoreService", "updateStatus");

                if (coreServices.get(i).getCoreServicesList().get(j).getOfferingId().equalsIgnoreCase(offerId)) {
                    if (isActive) {
                        coreServices.get(i).getCoreServicesList().get(j).setStatus(SWITCH_STATUS);
                    } else {
                        coreServices.get(i).getCoreServicesList().get(j).setStatus("inactive");
                    }// if(isActive) ends
                }//if (coreServices.get(i).getCoreServicesList().get(j).getOfferingId().equalsIgnoreCase(offerId)) ends
            }//for (int j = 0; j < coreServices.get(i).getCoreServicesList().size(); j++)  ends
        }//for (int i = 0; i < coreServices.size(); i++) ends

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_core_services_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tv_category_title.setText("");
        if (Tools.hasValue(coreServices.get(position).getCoreServiceCategory())) {
            holder.tv_category_title.setText(coreServices.get(position).getCoreServiceCategory());
        }
        if (coreServices.get(position).getCoreServicesList() != null) {
            holder.content_layout.removeAllViews();
            for (int i = 0; i < coreServices.get(position).getCoreServicesList().size(); i++) {
                String offeringId = coreServices.get(position).getCoreServicesList().get(i).getOfferingId();

                final ViewItemCoreServices viewItemCoreServices = new ViewItemCoreServices(context, offeringId);

                viewItemCoreServices.getTv_label().setText("");
                viewItemCoreServices.getTv_desc().setText("");
                viewItemCoreServices.getNextPayTitle().setText("");
                viewItemCoreServices.getNextPayDaysTitle().setText("");
                viewItemCoreServices.getNextDateTxt().setText("");
                viewItemCoreServices.getCreditValueTxt().setText("");
                viewItemCoreServices.getRenewalTypeText().setText("");

               /* if (coreServices.get(position).getCoreServicesList().get(i).getOfferingId().contentEquals(OFFERING_ID_I_AM_BACK)) {
                    if (isIcalledYouBack) {
                        viewItemCoreServices.getRl_switch_layout().setClickable(true);
                    } else {
                        viewItemCoreServices.getRl_switch_layout().setClickable(false);
                    }

                }*/

                boolean isActivve = false;
                if (Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getStatus())
                        && coreServices.get(position).getCoreServicesList().get(i)
                        .getStatus().equalsIgnoreCase(SWITCH_STATUS)) {
                    viewItemCoreServices.getSwitchCategory().setChecked(true);
                    isActivve = true;
                } else {
                    viewItemCoreServices.getSwitchCategory().setChecked(false);
                }


                if (is_turned_off_suspended) {
                    if (coreServices.get(position).getCoreServicesList().get(i).getOfferingId().contentEquals(OFFERING_ID_TURNED_OFF)) {
                        viewItemCoreServices.getSwitchCategory().setChecked(true);
                    }

                    if (coreServices.get(position).getCoreServicesList().get(i).getOfferingId().contentEquals(OFFERING_ID_I_AM_BACK)) {
                        //disable the switch
                        viewItemCoreServices.getRl_switch_layout().setClickable(false);
                        viewItemCoreServices.disableCategorySwitch();
                        viewItemCoreServices.getRl_switch_layout().setEnabled(false);
                        //check for status
                        BakcellLogger.logE("staX", "elseeee123:::" + coreServices.get(position).getCoreServicesList().get(i).getStatus(), fromClass, "onBindViewHolder");
                        if (coreServices.get(position).getCoreServicesList().get(i).getStatus().equalsIgnoreCase(SWITCH_STATUS)) {
                            viewItemCoreServices.getSwitchCategory().setChecked(true);
                            BakcellLogger.logE("staX", "iffffffff123:::" + coreServices.get(position).getCoreServicesList().get(i).getStatus(), fromClass, "onBindViewHolder");
                        } else {
                            viewItemCoreServices.getSwitchCategory().setChecked(false);
                        }
                    }

                    if (coreServices.get(position).getCoreServicesList().get(i).getOfferingId().contentEquals(OFFERING_ID_BUSY)) {
                        //disable the switch
                        viewItemCoreServices.getRl_switch_layout().setClickable(false);
                        viewItemCoreServices.disableCategorySwitch();
                        viewItemCoreServices.getRl_switch_layout().setEnabled(false);
                        //check for status
                        if (coreServices.get(position).getCoreServicesList().get(i).getStatus().equalsIgnoreCase(SWITCH_STATUS)) {
                            viewItemCoreServices.getSwitchCategory().setChecked(true);
                        } else {
                            viewItemCoreServices.getSwitchCategory().setChecked(false);
                        }
                    }
                } else {
                    BakcellLogger.logE("staX", "elseeee", fromClass, "onBindViewHolder");

                    //turned off is not suspended, so check the active or inactive status
                    //check if the current offering id is of i am back
                    //make the switch clickable, but set the status according to the active inactive status of turned off
                    if (coreServices.get(position).getCoreServicesList().get(i).getOfferingId().contentEquals(OFFERING_ID_I_AM_BACK)) {
                        if (activeInActiveStatus) {
                            BakcellLogger.logE("staX", "elseeee111", fromClass, "onBindViewHolder");
                            viewItemCoreServices.getRl_switch_layout().setClickable(true);
                            viewItemCoreServices.enableCategorySwitch();
                            viewItemCoreServices.getRl_switch_layout().setEnabled(true);
                        } else {
                            BakcellLogger.logE("staX", "elseeee2222", fromClass, "onBindViewHolder");
                            viewItemCoreServices.getRl_switch_layout().setClickable(false);
                            viewItemCoreServices.getRl_switch_layout().setEnabled(false);
                            viewItemCoreServices.disableCategorySwitch();
                        }

                        if (coreServices.get(position).getCoreServicesList().get(i).getStatus().equalsIgnoreCase(SWITCH_STATUS)) {
                            viewItemCoreServices.getSwitchCategory().setChecked(true);
                        } else {
                            viewItemCoreServices.getSwitchCategory().setChecked(false);
                        }
                    }
                }

                if (i == coreServices.get(position).getCoreServicesList().size()) {
                    viewItemCoreServices.getSeperator().setVisibility(View.GONE);
                } else {
                    viewItemCoreServices.getSeperator().setVisibility(View.VISIBLE);
                }
                if (Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getName())) {
                    viewItemCoreServices.getTv_label().setText(coreServices.get(position)
                            .getCoreServicesList().get(i).getName());
                } else {
                    viewItemCoreServices.getTv_label().setVisibility(View.GONE);
                }
                if (Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getDescription())) {
                    viewItemCoreServices.getTv_desc().setText(coreServices.get(position)
                            .getCoreServicesList().get(i).getDescription());
                } else {
                    viewItemCoreServices.getTv_desc().setVisibility(View.GONE);
                }
                if (Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getPrice())) {
                    if (coreServices.get(position).getCoreServicesList().get(i).getPrice().equalsIgnoreCase("0.0")) {
                        viewItemCoreServices.getTvTzn().setVisibility(View.GONE);
                        viewItemCoreServices.getCreditValueTxt().setText(context.getString(R.string.lbl_free));
                        viewItemCoreServices.getCreditValueTxt().setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    } else {
                        viewItemCoreServices.getTvTzn().setVisibility(View.VISIBLE);
                        viewItemCoreServices.getCreditValueTxt().setText(coreServices.get(position)
                                .getCoreServicesList().get(i).getPrice());
                        viewItemCoreServices.getCreditValueTxt().setTextColor(context.getResources().getColor(R.color.black));
                    }
                } else {
                    viewItemCoreServices.getCreditValueTxt().setVisibility(View.GONE);
                    viewItemCoreServices.getTvTzn().setVisibility(View.GONE);
                }

                // ProgressBar Work here
                if (isActivve && Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getEffectiveDate()) && Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getRenewable())) {
                    viewItemCoreServices.getNextPayProgressLayout().setVisibility(View.VISIBLE);
                    viewItemCoreServices.getRenewalTypeText().setVisibility(View.GONE);

                    long startMili = Tools.getMiliSecondsFromDate(coreServices.get(position).getCoreServicesList().get(i).getEffectiveDate());
                    long endMili = Tools.getMiliSecondsFromDate(coreServices.get(position).getCoreServicesList().get(i).getExpireDate());
                    endMili = Tools.getOneDayMinus(endMili);
                    long currentMili = Tools.getCurrentMiliSeconds();

                    int daysDiffCurrent = Tools.getDaysDifferenceBetweenMiliseconds(startMili, currentMili);
                    int daysDiffTotal = Tools.getDaysDifferenceBetweenMiliseconds(startMili, endMili);

                    if (daysDiffTotal <= 0) {
                        daysDiffTotal = 1;
                        daysDiffCurrent = 1;
                    }
                    viewItemCoreServices.getNextProgress().setMax(daysDiffTotal);
                    viewItemCoreServices.getNextProgress().setProgress(daysDiffCurrent);

                    int daysLeft = daysDiffTotal - daysDiffCurrent;

                    if (daysLeft < 0) {
                        daysLeft = 0;
                    }
                    viewItemCoreServices.getNextPayDaysTitle().setText(context.getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, daysLeft, daysLeft));

                } else {
                    viewItemCoreServices.getNextPayProgressLayout().setVisibility(View.GONE);
                    viewItemCoreServices.getRenewalTypeText().setVisibility(View.VISIBLE);

                    String validityText = "";

                    if (Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getValidityLabel())
                            && Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getValidity())) {
                        validityText = coreServices.get(position).getCoreServicesList().get(i).getValidityLabel() + ": ";
                        validityText += coreServices.get(position).getCoreServicesList().get(i).getValidity();
                        viewItemCoreServices.getRenewalTypeText().setText(validityText);
                    } else {
                        viewItemCoreServices.getRenewalTypeText().setVisibility(View.GONE);
                    }
                }

                if (Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getProgressTitle())) {
                    viewItemCoreServices.getNextPayTitle().setText(coreServices.get(position).getCoreServicesList().get(i).getProgressTitle());
                }

                if (Tools.hasValue(coreServices.get(position).getCoreServicesList().get(i).getExpireDate())) {
                    try {
                        viewItemCoreServices.getNextDateTxt().setText(coreServices.get(position).getCoreServicesList().get(i).getProgressDateLabel() + ": " +
                                Tools.getDateAccordingToClientSaid(coreServices.get(position)
                                        .getCoreServicesList().get(i).getExpireDate()));
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                }
                holder.content_layout.addView(viewItemCoreServices);
            }
        }

        checkifCallForwardingInCoreServiceList(holder, coreServices.get(position));

    }


    private void checkifCallForwardingInCoreServiceList(final ViewHolder holder, final CoreServicesMain coreServicesMain) {

        if (holder == null) return;

        int callForwardItemIndex = -1;

        if (coreServicesMain != null) {
            if (coreServicesMain.getCoreServicesList() != null) {
                for (int i = 0; i < coreServicesMain.getCoreServicesList().size(); i++) {

                    if (Tools.hasValue(coreServicesMain.getCoreServicesList().get(i).getOfferingId())) {
                        if (coreServicesMain.getCoreServicesList().get(i).getOfferingId()
                                .equalsIgnoreCase(OFFERING_ID_CALL_FORWARD)) {
                            callForwardItemIndex = i;
                            break;
                        }
                    }

                }
            }

        }


        if (callForwardItemIndex != -1) {

            holder.content_layout.setVisibility(View.GONE);
            holder.callForwardLayout.setVisibility(View.VISIBLE);

            holder.tvPhoneNumber.setText(R.string.lbl_none);

            if (coreServicesMain != null && coreServicesMain.getCoreServicesList() != null
                    && coreServicesMain.getCoreServicesList().size() > callForwardItemIndex) {

                if (coreServicesMain.getCoreServicesList().get(callForwardItemIndex) != null
                        && Tools.hasValue(coreServicesMain.getCoreServicesList().get(callForwardItemIndex).getForwardNumber())) {
                    holder.tvPhoneNumber.setText(coreServicesMain.getCoreServicesList().get(callForwardItemIndex).getForwardNumber());
                    holder.switchPhoneSettings.setChecked(true);
                    holder.switchPhoneSettings.setEnabled(true);
                } else {
                    holder.switchPhoneSettings.setChecked(false);
                    holder.switchPhoneSettings.setEnabled(false);
                }
            }

            if (coreServicesMain != null) {
                holder.tv_category_title.setText(coreServicesMain.getCoreServiceCategory());
            }

            final int finalCallForwardItemIndex = callForwardItemIndex;
//            holder.tv_phone_layout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (!holder.switchPhoneSettings.isChecked()) {
//                        callForwardListner.onCallForwardListener(coreServicesMain, finalCallForwardItemIndex);
//                    }
//                }
//            });

            holder.callForwardLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!holder.switchPhoneSettings.isChecked()) {
                        callForwardListner.onCallForwardListener(coreServicesMain, finalCallForwardItemIndex);
                    }
                }
            });
            holder.ll_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.switchPhoneSettings.isEnabled()) {
                        if (holder.switchPhoneSettings.isChecked()) {
                            deactivateNumber(holder, coreServicesMain, finalCallForwardItemIndex);
                        } else {
                        }
                    }else {
                        callForwardListner.onCallForwardListener(coreServicesMain, finalCallForwardItemIndex);
                    }
                }
            });

//            holder.switchPhoneSettings.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        holder.tv_phone_layout.setEnabled(false);
//                    } else {
//                        holder.tv_phone_layout.setEnabled(true);
//                    }
//                }
//            });

        } else {

            holder.callForwardLayout.setVisibility(View.GONE);
            holder.content_layout.setVisibility(View.VISIBLE);

        }

    }

    private void deactivateNumber(final ViewHolder holder, CoreServicesMain coreServicesMain, int finalCallForwardItemIndex) {

        if (coreServicesMain != null && coreServicesMain.getCoreServicesList() != null &&
                finalCallForwardItemIndex > -1
                && finalCallForwardItemIndex < coreServicesMain.getCoreServicesList().size()) {
            CoreServicesList forwardNumber = coreServicesMain.getCoreServicesList().get(finalCallForwardItemIndex);
            SubscribeUnsubscribe.requestSubscribeUnsubscribe(context, OFFER_UNSUBSCRIBE,
                    forwardNumber.getOfferingId(),
                    forwardNumber.getName(), new SubscribeUnsubscribeListener() {
                        @Override
                        public void onSubscribeUnsubscribeSuccess() {
                            holder.switchPhoneSettings.setChecked(false);
                            holder.switchPhoneSettings.setEnabled(false);
                        }

                        @Override
                        public void onSubscribeUnsubscribeFailure() {
                            holder.switchPhoneSettings.setChecked(true);
                            holder.switchPhoneSettings.setEnabled(true);
                        }
                    });
        }
    }


    @Override
    public int getItemCount() {
        return coreServices.size();
    }

    public void updateCoreServicesAfterCallForwardAdded(String number) {

        if (!Tools.hasValue(number)) return;

        int callForwardCatIndex = -1, callForwardItemIndex = -1;
        if (coreServices != null) {
            for (int i = 0; i < coreServices.size(); i++) {
                if (coreServices.get(i) != null) {

                    callForwardItemIndex = -1;
                    if (coreServices.get(i).getCoreServicesList() != null) {
                        for (int j = 0; j < coreServices.get(i).getCoreServicesList().size(); j++) {
                            if (Tools.hasValue(coreServices.get(i).getCoreServicesList().get(j).getOfferingId())) {

                                if (coreServices.get(i).getCoreServicesList().get(j).getOfferingId()
                                        .equalsIgnoreCase(OFFERING_ID_CALL_FORWARD)) {

                                    callForwardItemIndex = j;

                                    break;
                                }
                            }

                        }
                    }

                    if (callForwardItemIndex != -1) {
                        callForwardCatIndex = i;
                        break;
                    }

                }
            }
        }

        if (callForwardCatIndex != -1 && callForwardItemIndex != -1) {
            coreServices.get(callForwardCatIndex).getCoreServicesList()
                    .get(callForwardItemIndex).setForwardNumber(number);
        }

        notifyDataSetChanged();

    }


    class ViewHolder extends RecyclerView.ViewHolder {
        BakcellTextViewBold tv_category_title;
        LinearLayout content_layout, ll_settings;
        BakcellTextViewNormal tvPhoneNumber;
        RelativeLayout callForwardLayout, tv_phone_layout;
        SwitchCompat switchPhoneSettings;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_category_title = (BakcellTextViewBold) itemView.findViewById(R.id.tv_category_title);
            content_layout = (LinearLayout) itemView.findViewById(R.id.content_layout);
            ll_settings = (LinearLayout) itemView.findViewById(R.id.ll_settings);
            callForwardLayout = (RelativeLayout) itemView.findViewById(R.id.callForwardLayout);
            tv_phone_layout = (RelativeLayout) itemView.findViewById(R.id.tv_phone_layout);
            tvPhoneNumber = (BakcellTextViewNormal) itemView.findViewById(R.id.tvPhoneNumber);
            switchPhoneSettings = (SwitchCompat) itemView.findViewById(R.id.switchPhoneSettings);
        }
    }
}
