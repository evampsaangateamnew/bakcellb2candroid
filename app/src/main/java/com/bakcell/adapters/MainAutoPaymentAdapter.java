package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.interfaces.ScheduledPaymentItemEvent;
import com.bakcell.models.getscheduledpayments.ScheduledPaymentDataItem;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;
import java.util.List;

public class MainAutoPaymentAdapter extends RecyclerView.Adapter<MainAutoPaymentAdapter.ViewHolder> {
    private List<ScheduledPaymentDataItem> savedCardsItems = new ArrayList<>();
    private Context context;
    private int selectedItem = 0;
    private ScheduledPaymentItemEvent scheduledPaymentItemEvent;

    public MainAutoPaymentAdapter(List<ScheduledPaymentDataItem> dataSet, Context context, ScheduledPaymentItemEvent scheduledPaymentItemEvent) {
        if (dataSet != null)
            this.savedCardsItems = dataSet;
        this.context = context;
        this.scheduledPaymentItemEvent = scheduledPaymentItemEvent;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_auto_payment,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ScheduledPaymentDataItem getSavedCardsItems = savedCardsItems.get(position);

        if (getSavedCardsItems != null) {

            holder.status.setText(getSavedCardsItems.getRecurrentDay());
            holder.status.setSelected(true);
            holder.topUpNumber.setText(getSavedCardsItems.getMsisdn());

            if (getSavedCardsItems.getAmount() != null && Tools.hasValue(getSavedCardsItems.getAmount()) && Double.parseDouble(getSavedCardsItems.getAmount()) > 0) {
                holder.textPayment.setText(getSavedCardsItems.getAmount());
            }

            //check if position is selected Item then update selected row color
            //else set properties to normal.

            //set Click listener to RecyclerView Row
            holder.itemView.setOnClickListener(view -> {
                updateItemCheckState(position);
            });

            if (position == savedCardsItems.size() - 1) {
                //check if true, then show Add card option & hide radio button for Add card Option in the recyclerview.
                holder.cardIcon.setImageResource(R.drawable.ic_add_card);
                holder.llAmount.setVisibility(View.GONE);
                holder.status.setVisibility(View.GONE);
                holder.topUpNumber.setTextColor(context.getResources().getColor(R.color.dark_gray));
                holder.topUpNumber.setText(R.string.lbl_auto_payment_add_new_auto_payment);

            } else {

                if (getSavedCardsItems.getAmount() != null && Tools.hasValue(getSavedCardsItems.getAmount()) && Double.parseDouble(getSavedCardsItems.getAmount()) > 0) {
                    holder.llAmount.setVisibility(View.VISIBLE);
                } else {
                    holder.llAmount.setVisibility(View.GONE);
                }

                holder.status.setVisibility(View.VISIBLE);

                //else show simple card according to its type
                if (getSavedCardsItems.getCardType().contains("v")) {
                    holder.cardIcon.setImageResource(R.drawable.ic_visa_card);
                } else if (getSavedCardsItems.getCardType().contains("m")) {
                    holder.cardIcon.setImageResource(R.drawable.ic_master_card);
                }
            }
        }

    } //end onBindViewHolder

    private void updateItemCheckState(int position) {

        if (scheduledPaymentItemEvent != null) {
            scheduledPaymentItemEvent.onItemClick(position);
        }
    }

    @Override
    public int getItemCount() {
        return savedCardsItems.size();
    }

    public void deleteItem(int position) {
        savedCardsItems.remove(position);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView topUpNumber, textPayment, status;
        ImageView cardIcon;
        LinearLayout llAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardIcon = itemView.findViewById(R.id.card_icon);
            topUpNumber = itemView.findViewById(R.id.card_text);
            textPayment = itemView.findViewById(R.id.text_payment);
            status = itemView.findViewById(R.id.text_status);
            llAmount = itemView.findViewById(R.id.llAmount);
        }
    }
}
