package com.bakcell.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.models.faq.FaqCategory;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 6/5/2017.
 */

public class SpinnerAdapterFAQ extends BaseAdapter {

    private ArrayList<FaqCategory> faqCategoriesList;
    Context context;

    private int lastSelectedPos = -1;
    private boolean isOpened = false;

    public SpinnerAdapterFAQ(Context context, ArrayList<FaqCategory> faqCategoriesList) {
        this.context = context;
        this.faqCategoriesList = faqCategoriesList;
    }

    public int getLastSelectedPos() {
        return lastSelectedPos;
    }

    public void setLastSelectedPos(int lastSelectedPos) {
        this.lastSelectedPos = lastSelectedPos;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    @Override
    public int getCount() {
        return faqCategoriesList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.faq_spinner_item, null);
            viewHolder.filterTitle = (BakcellTextViewNormal) convertView
                    .findViewById(R.id.tv_filter_title);
            viewHolder.ivFilterIndicator = (ImageView) convertView.findViewById(R.id.iv_indicator);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_red));
        } else if (!isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else if (!isOpened && position == lastSelectedPos) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else {
            viewHolder.ivFilterIndicator.setVisibility(View.INVISIBLE);
        }

        if (Tools.hasValue(faqCategoriesList.get(position).getTitle())) {
            viewHolder.filterTitle.setText(faqCategoriesList.get(position).getTitle());
        } else {
            viewHolder.filterTitle.setText("");
        }

        return convertView;
    }

    public void refreshList(ArrayList<FaqCategory> faqCategoryArrayList) {

        this.faqCategoriesList.addAll(faqCategoryArrayList);
        notifyDataSetChanged();

    }

    class ViewHolder {
        protected BakcellTextViewNormal filterTitle;
        protected ImageView ivFilterIndicator;
    }
}
