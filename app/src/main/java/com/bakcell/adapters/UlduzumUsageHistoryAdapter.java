package com.bakcell.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.models.ulduzum.UlduzumUsageHistoryModel;
import com.bakcell.models.ulduzum.UsageHistoryList;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

public class UlduzumUsageHistoryAdapter extends ArrayAdapter<UsageHistoryList> {

    private Context context;
    private UlduzumUsageHistoryModel usageHistoryData;

    public UlduzumUsageHistoryAdapter(Context context, UlduzumUsageHistoryModel usageHistoryData) {
        super(context, R.layout.ulduzum_usage_history_list_item, usageHistoryData.getUsageHistoryList());
        this.context = context;
        this.usageHistoryData = usageHistoryData;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.ulduzum_usage_history_list_item, parent, false);
            viewHolder.merchantTitle = convertView.findViewById(R.id.merchantTitle);
            viewHolder.merchantValue = convertView.findViewById(R.id.merchantValue);
            viewHolder.amountTitle = convertView.findViewById(R.id.amountTitle);
            viewHolder.amountValue = convertView.findViewById(R.id.amountValue);
            viewHolder.discountPercentageTitle = convertView.findViewById(R.id.discountPercentageTitle);
            viewHolder.discountPercentageValue = convertView.findViewById(R.id.discountPercentageValue);
            viewHolder.paymentTypeValue = convertView.findViewById(R.id.paymentTypeValue);
            viewHolder.paymentTypeTitle = convertView.findViewById(R.id.paymentTypeTitle);
            viewHolder.discountAmountValue = convertView.findViewById(R.id.discountAmountValue);
            viewHolder.discountAmountTitle = convertView.findViewById(R.id.discountAmountTitle);
            viewHolder.redemptionDateValue = convertView.findViewById(R.id.redemptionDateValue);
            viewHolder.redemptionDateTitle = convertView.findViewById(R.id.redemptionDateTitle);
            viewHolder.mainContainer = convertView.findViewById(R.id.mainContainer);
            //set the tag
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //set the background of the main container
        if (position % 2 == 0) {
            viewHolder.mainContainer.setBackgroundColor(context.getResources().getColor(R.color.light_gray));
        } else {
            viewHolder.mainContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        //text view selections
        viewHolder.merchantTitle.setSelected(true);
        viewHolder.merchantValue.setSelected(true);
        viewHolder.amountValue.setSelected(true);
        viewHolder.amountTitle.setSelected(true);
        viewHolder.discountPercentageValue.setSelected(true);
        viewHolder.discountPercentageTitle.setSelected(true);
        viewHolder.discountAmountValue.setSelected(true);
        viewHolder.discountAmountTitle.setSelected(true);
        viewHolder.redemptionDateValue.setSelected(true);
        viewHolder.redemptionDateTitle.setSelected(true);
        viewHolder.paymentTypeValue.setSelected(true);
        viewHolder.paymentTypeTitle.setSelected(true);

        //set the ui here
        //merchant name
        if (Tools.hasValue(usageHistoryData.getUsageHistoryList().get(position).getMerchant_name())) {
            viewHolder.merchantValue.setText(usageHistoryData.getUsageHistoryList().get(position).getMerchant_name());
        }//merchant name

        //redemption date
        if (Tools.hasValue(usageHistoryData.getUsageHistoryList().get(position).getRedemption_date())) {
            viewHolder.redemptionDateValue.setText(usageHistoryData.getUsageHistoryList().get(position).getRedemption_date());
        }//redemption date

        //amount
        if (Tools.hasValue(usageHistoryData.getUsageHistoryList().get(position).getAmount())) {
            viewHolder.amountValue.setText(usageHistoryData.getUsageHistoryList().get(position).getAmount());
        }//amount

        //discount amount value
        if (Tools.hasValue(usageHistoryData.getUsageHistoryList().get(position).getDiscount_amount())) {
            viewHolder.discountAmountValue.setText(usageHistoryData.getUsageHistoryList().get(position).getDiscount_amount());
        }//discount amount value

        //discount percentage
        if (Tools.hasValue(usageHistoryData.getUsageHistoryList().get(position).getDiscount_percent())) {
            viewHolder.discountPercentageValue.setText(usageHistoryData.getUsageHistoryList().get(position).getDiscount_percent());
        } //discount percentage

        //payment type
        if (Tools.hasValue(usageHistoryData.getUsageHistoryList().get(position).getPayment_type())) {
            viewHolder.paymentTypeValue.setText(usageHistoryData.getUsageHistoryList().get(position).getPayment_type());
        }//payment type
        return convertView;
    }//getView ends

    public class ViewHolder {
        BakcellTextViewNormal merchantTitle;
        BakcellTextViewNormal merchantValue;
        BakcellTextViewNormal amountTitle;
        BakcellTextViewNormal amountValue;
        BakcellTextViewNormal discountPercentageTitle;
        BakcellTextViewNormal discountPercentageValue;
        BakcellTextViewNormal paymentTypeValue;
        BakcellTextViewNormal paymentTypeTitle;
        BakcellTextViewNormal discountAmountValue;
        BakcellTextViewNormal discountAmountTitle;
        BakcellTextViewNormal redemptionDateValue;
        BakcellTextViewNormal redemptionDateTitle;
        RelativeLayout mainContainer;
    }
}