package com.bakcell.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bakcell.R;

import java.util.ArrayList;


public class RoamingCountryAdapter extends ArrayAdapter<String> implements Filterable {

    private Context mContext;
    private ArrayList<String> countriesList;
    private ArrayList<String> countriesListTemp;
    private LayoutInflater inflater = null;
    private CountryFilter countryFilter;

    public RoamingCountryAdapter(Context context, int resource, ArrayList<String> countriesList) {
        super(context, resource, countriesList);
        this.mContext = context;
        this.countriesList = countriesList;
        this.countriesListTemp = countriesList;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return countriesList.size();
    }

    @Override
    public String getItem(int position) {
        return countriesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_item_roaming_country, null);
            viewHolder = new ViewHolder();
            viewHolder.filter_name = (TextView) convertView.findViewById(R.id.filter_name);
            viewHolder.separator = convertView.findViewById(R.id.view_seperator);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.filter_name.setText(countriesList.get(position));

        return convertView;
    }


    public class ViewHolder {
        TextView filter_name;
        View separator;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        if (countryFilter == null) {
            countryFilter = new CountryFilter();
        }
        return countryFilter;
    }

    public class CountryFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                ArrayList<String> filterList = new ArrayList<>();
                if (constraint.toString().trim().length() == 0) {
                    results.count = countriesListTemp.size();
                    results.values = countriesListTemp;
                }
                for (String country : countriesListTemp) {
                    if (country.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filterList.add(country);
                    }
                }
                if (filterList.size() > 0) {
                    results.count = filterList.size();
                    results.values = filterList;
                }
            }else{
                results.count = countriesListTemp.size();
                results.values = countriesListTemp;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                countriesList = (ArrayList<String>) results.values;
                notifyDataSetChanged();

            }
        }
    }
}

