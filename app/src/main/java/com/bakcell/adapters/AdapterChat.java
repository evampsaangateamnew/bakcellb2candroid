package com.bakcell.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.models.livechat.LiveChat;

import java.util.List;


/**
 * Created by Zeeshan Shabbir on 6/21/2017.
 */

public class AdapterChat extends RecyclerView.Adapter<AdapterChat.ViewHolder> {

    public static final String TYPE = "SENDER";

    List<LiveChat> messages;

    public AdapterChat(List<LiveChat> messages) {
        this.messages = messages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewHolder viewHolder = null;

        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_live_chat_receiver_item, parent, false);
            viewHolder = new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_live_chat_sender_item, parent, false);
            viewHolder = new ViewHolder(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvMessage.setText(messages.get(position).getMessage());
        holder.tvDate.setText(messages.get(position).getTime());
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position).getType().contains(TYPE))
            return 0;
        else
            return 1;
    }


    public void updateMessages(List<LiveChat> newChat) {
        messages.addAll(newChat);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMessage;
        TextView tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
        }
    }
}
