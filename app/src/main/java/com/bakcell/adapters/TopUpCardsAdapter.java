package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.interfaces.SaveCardsItemClickListener;
import com.bakcell.models.topup.GetSavedCardsItems;

import java.util.ArrayList;
import java.util.List;

public class TopUpCardsAdapter extends RecyclerView.Adapter<TopUpCardsAdapter.ViewHolder> {
    private List<GetSavedCardsItems> savedCardsItems = new ArrayList<>();
    private Context context;
    private int selectedItem = 0;
    private SaveCardsItemClickListener saveCardsItemClickListener;

    public TopUpCardsAdapter(List<GetSavedCardsItems> dataSet, Context context, SaveCardsItemClickListener saveCardsItemClickListener) {
        if (dataSet != null)
            this.savedCardsItems = dataSet;
        this.context = context;
        this.saveCardsItemClickListener = saveCardsItemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_topup_visa,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GetSavedCardsItems getSavedCardsItems = savedCardsItems.get(position);
        holder.cardNumber.setText(getSavedCardsItems.getTopupNumber());
        //check if position is selected Item then update selected row color
        //else set properties to normal.
        if (position == selectedItem) {
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.radioButton.setChecked(true);
        } else {
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.text_gray));
            holder.radioButton.setChecked(false);
        }
        //set Click listener to radioButton
        holder.radioButton.setOnClickListener(v -> {
            updateItemCheckState(position);

        });
        //set Click listener to RecyclerView Row
        holder.itemView.setOnClickListener(view -> {
                updateItemCheckState(position);
        });

        if (position == savedCardsItems.size() - 1) {
            //check if true, then show Add card option & hide radio button for Add card Option in the recyclerview.
            holder.cardIcon.setImageResource(R.drawable.ic_add_card);
            holder.cardNumber.setTextColor(context.getResources().getColor(R.color.dark_gray));
            holder.radioButton.setVisibility(View.GONE);


            if (savedCardsItems.size() > 1) {
                holder.cardNumber.setText(context.getString(R.string.another_card));
            } else {
                holder.cardNumber.setText(context.getString(R.string.add_card));
            }

        } else {
            holder.radioButton.setVisibility(View.VISIBLE);
            //else show simple card according to its type
            if (getSavedCardsItems.getCardType().contains("v")) {
                holder.cardIcon.setImageResource(R.drawable.ic_visa_card);
            } else if (getSavedCardsItems.getCardType().contains("m")) {
                holder.cardIcon.setImageResource(R.drawable.ic_master_card);
            }
        }

    } //end onBindViewHolder

    private void updateItemCheckState(int position) {

        if (savedCardsItems.size() > 1 && position == selectedItem) {
            selectedItem = -1;
            notifyDataSetChanged();
            setUpCardSelectedListener(null, -1);
        } else {
            int copyOfLastCheckedPosition = selectedItem;
            selectedItem = position;
            notifyItemChanged(copyOfLastCheckedPosition);
            notifyItemChanged(selectedItem);
            setUpCardSelectedListener(savedCardsItems.get(position), position);
        }

    }

    void setUpCardSelectedListener(GetSavedCardsItems getSavedCardsItems, int position) {
        if (saveCardsItemClickListener != null) {
            saveCardsItemClickListener.onItemClick(getSavedCardsItems, position);
        }
    }

    @Override
    public int getItemCount() {
        return savedCardsItems.size();
    }

    public void deleteItem(int position) {
        savedCardsItems.remove(position);
        notifyDataSetChanged();
    }

    public GetSavedCardsItems getFirstItem() {
        return savedCardsItems != null ? savedCardsItems.get(0) : null;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView cardNumber;
        ImageView cardIcon;
        RadioButton radioButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardIcon = itemView.findViewById(R.id.card_icon);
            cardNumber = itemView.findViewById(R.id.card_text);
            radioButton = itemView.findViewById(R.id.radio_btn);
        }
    }
}
