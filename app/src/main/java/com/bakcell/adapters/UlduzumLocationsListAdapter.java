package com.bakcell.adapters;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.UlduzumMerchantDetailsActivity;
import com.bakcell.models.ulduzum.UlduzumLocationsListModel;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class UlduzumLocationsListAdapter extends ArrayAdapter<UlduzumLocationsListModel> implements StickyListHeadersAdapter {

    private Context context;
    private Activity activity;
    private ArrayList<UlduzumLocationsListModel> ulduzumLocationsListModel;

    public UlduzumLocationsListAdapter(Context context, Activity activity,ArrayList<UlduzumLocationsListModel> ulduzumLocationsListModel) {
        super(context, R.layout.ulduzum_location_list_item, ulduzumLocationsListModel);
        this.context = context;
        this.activity = activity;
        this.ulduzumLocationsListModel = ulduzumLocationsListModel;
    }//UlduzumLocationsListAdapter ends

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.ulduzum_location_list_item, parent, false);
            viewHolder.categoryLabel = convertView.findViewById(R.id.categoryLabel);
            viewHolder.giftPackIcon = convertView.findViewById(R.id.giftPackIcon);
            viewHolder.nameLabel = convertView.findViewById(R.id.nameLabel);
            viewHolder.percentageLabel = convertView.findViewById(R.id.percentageLabel);
            //set the tag
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        UlduzumLocationsListModel model = getItem(position);
        if (model != null) {
            //name above category name
            if (Tools.hasValue(model.name)) {
                viewHolder.nameLabel.setText(model.name);
            }

            //category name below name
            if (Tools.hasValue(model.categoryName)) {
                viewHolder.categoryLabel.setText(model.categoryName);
            }

            //set the percentage
            if (Tools.hasValue(model.loyalitySegment)) {
                if (!model.loyalitySegment.matches("[0-9]+")) {
                    viewHolder.percentageLabel.setVisibility(View.GONE);
                    viewHolder.giftPackIcon.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.percentageLabel.setText(model.loyalitySegment + "%");
                    viewHolder.percentageLabel.setVisibility(View.VISIBLE);
                    viewHolder.giftPackIcon.setVisibility(View.GONE);
                }//if (!model.loyalitySegment.matches("[0-9]+"))  ends
            } else {
                viewHolder.percentageLabel.setVisibility(View.GONE);
                viewHolder.giftPackIcon.setVisibility(View.GONE);
            }//if (Tools.hasValue(model.loyalitySegment))  ends
        }//if(model!=null)

       convertView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Bundle bundle =new Bundle();
               bundle.putString("merchantId",model.merchantId);
                BaseActivity.startNewActivity(activity, UlduzumMerchantDetailsActivity.class,bundle);
           }
       });
        return convertView;
    }//getView ends

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.ulduzum_location_list_header_item, parent, false);
            holder.headerText = convertView.findViewById(R.id.headerText);
            holder.headerLayout = convertView.findViewById(R.id.headerLayout);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        if (Tools.hasValue(ulduzumLocationsListModel.get(position).name)) {
            holder.headerText.setText("");
            //set header text as first char in name
            String headerText = "" + ulduzumLocationsListModel.get(position).name.subSequence(0, 1).charAt(0);
            holder.headerText.setText(headerText);
            holder.headerLayout.setVisibility(View.VISIBLE);
        } else {
            holder.headerText.setText("");
            holder.headerLayout.setVisibility(View.GONE);
        }

        return convertView;
    }//getHeaderView ends

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        return ulduzumLocationsListModel.get(position).name.subSequence(0, 1).charAt(0);
    }//getHeaderId ends

    public class HeaderViewHolder {
        BakcellTextViewNormal headerText;
        RelativeLayout headerLayout;
    }//HeaderViewHolder ends

    public class ViewHolder {
        BakcellTextViewNormal categoryLabel;
        ImageView giftPackIcon;
        BakcellTextViewNormal percentageLabel;
        BakcellTextViewNormal nameLabel;
    }//ViewHolder ends

}//class ends
