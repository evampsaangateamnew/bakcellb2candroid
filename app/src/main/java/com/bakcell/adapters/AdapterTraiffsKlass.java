package com.bakcell.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.ChangeTariffListener;
import com.bakcell.interfaces.UpdateListOnSuccessfullySurvey;
import com.bakcell.models.inappsurvey.Answers;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Questions;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.inappsurvey.UserSurveys;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.Attributes;
import com.bakcell.models.tariffs.tariffklassobject.TariffKlass;
import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePrice;
import com.bakcell.models.tariffs.tariffklassobject.paygprice.PaygPrice;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.TariffDetailLayout;
import com.bakcell.viewsitem.ViewItemTariffsAttribute;
import com.bakcell.viewsitem.ViewItemTariffsKlassAttribute;
import com.bakcell.webservices.changetariff.ChangeTariff;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeeshan Shabbir on 7/26/2017.
 */

public class AdapterTraiffsKlass extends RecyclerView.Adapter<AdapterTraiffsKlass.ViewHolder> {


    private static boolean isDetailExpanded;
    private static boolean isPackagePriceExpanded;
    private static boolean isPayGPriceExpanded;
    public static String KLASS = "klass";

    ArrayList<TariffKlass> klassArrayList;

    Context context;


    public AdapterTraiffsKlass() {

    }

    public AdapterTraiffsKlass(Context context, ArrayList<TariffKlass> klassArrayList, boolean packageExp, boolean payGExp, boolean detailExp) {
        this.context = context;
        isPackagePriceExpanded = packageExp;
        isPayGPriceExpanded = payGExp;
        isDetailExpanded = detailExp;
        this.klassArrayList = klassArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .adapter_item_traiffs_klass, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.prepareListItem(context, position);
        int rateStars = 0;

        if (klassArrayList.get(position).getHeader() != null) {
            holder.prepareHeaderItem(klassArrayList.get(position).getHeader(), holder);
        } else {
            holder.prepareHeaderItem(null, holder);
        }
        if (klassArrayList.get(position).getPackagePrice() != null) {
            holder.preparePackagePriceItem(klassArrayList.get(position).getPackagePrice(), holder);
        } else {
            holder.preparePackagePriceItem(null, holder);
        }
        if (klassArrayList.get(position).getPaygPrice() != null) {
            holder.preparePayGPriceItem(klassArrayList.get(position).getPaygPrice(), holder);
        } else {
            holder.preparePayGPriceItem(null, holder);
        }
        if (klassArrayList.get(position).getDetails() != null) {
            holder.prepareDetailItem(klassArrayList.get(position).getDetails(), holder);
        } else {
            holder.prepareDetailItem(null, holder);
        }


        final int pos1 = position;
        // Subscribe Button process


        holder.button_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (context != null && klassArrayList != null && klassArrayList.get(pos1) != null && klassArrayList.get(pos1).getHeader() != null) {
                    processOnSubscribeButton(context, klassArrayList.get(pos1).getHeader().getOfferingId(), klassArrayList.get(pos1).getHeader().getName(), klassArrayList.get(pos1).getHeader().getSubscribable());
                }
            }
        });
        Data data = PrefUtils.getInAppSurvey(context).getData();
        if (data != null && data.getUserSurveys() != null) {
            List<UserSurveys> userSurveys = data.getUserSurveys();
            for (int i = 0; i < userSurveys.size(); i++) {
                if (userSurveys.get(i).getOfferingId().equals(klassArrayList.get(pos1).getHeader().getOfferingId())) {
                    if (userSurveys.get(i).getAnswer() != null) {
                        String surveyId = userSurveys.get(i).getSurveyId();
                        if (surveyId != null) {
                            Surveys survey = data.findSurveyById(Integer.parseInt(surveyId));
                            if (Tools.isNumeric(userSurveys.get(i).getAnswerId())) {
                                if (Tools.isNumeric(userSurveys.get(i).getQuestionId())) {
                                    rateStars = setStarsKlassViewHolder(Integer.parseInt(userSurveys.get(i).getAnswerId()),
                                            Integer.parseInt(userSurveys.get(i).getQuestionId()), survey, holder);
                                }
                            }
                        }
                    } else {
                        rateStars = setStarsKlassViewHolder(0, 0, null, holder);
                    }
                }
            }
        }

        int finalRateStars = rateStars;
        holder.ratingStarsLayout.setOnClickListener(
                v -> {
                    String tariffId = klassArrayList.get(pos1).getHeader().getOfferingId();
                    inAppFeedback(context, tariffId, finalRateStars, position);
                }
        );
    }

    private void inAppFeedback(Context context, String tariffId, int rate, int position) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TARIFF_PAGE);
            if (surveys != null) {
                InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                if (surveys.getQuestions() != null) {
                    currentVisit = 0;
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(new UpdateListOnSuccessfullySurvey() {
                        @Override
                        public void updateListOnSuccessfullySurvey() {
                            notifyItemChanged(position);
                        }
                    });
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "1", tariffId, rate);
                }
            }
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE, currentVisit);
    }

    private int setStarsKlassViewHolder(int answerId, int questionId, Surveys survey, ViewHolder viewHolder) {
        if (survey != null) {
            Questions question = survey.findQuestion(questionId);
            if (question != null) {
                List<Answers> answers = question.getAnswers();
                if (answers != null) {
                    if (answers.size() > 0 && answers.get(0).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(false);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 1;
                    } else if (answers.size() > 1 && answers.get(1).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 2;
                    } else if (answers.size() > 2 && answers.get(2).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 3;
                    } else if (answers.size() > 3 && answers.get(3).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(false);
                        return 4;
                    } else if (answers.size() > 4 && answers.get(4).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(true);
                        return 5;
                    }

                } else {
                    viewHolder.starOne.setChecked(false);
                    viewHolder.starTwo.setChecked(false);
                    viewHolder.starThree.setChecked(false);
                    viewHolder.starFour.setChecked(false);
                    viewHolder.starFive.setChecked(false);
                    return 0;
                }
            } else {
                viewHolder.starOne.setChecked(false);
                viewHolder.starTwo.setChecked(false);
                viewHolder.starThree.setChecked(false);
                viewHolder.starFour.setChecked(false);
                viewHolder.starFive.setChecked(false);
                return 0;
            }
        } else {
            viewHolder.starOne.setChecked(false);
            viewHolder.starTwo.setChecked(false);
            viewHolder.starThree.setChecked(false);
            viewHolder.starFour.setChecked(false);
            viewHolder.starFive.setChecked(false);
            return 0;
        }
        return 0;
    }

    private void processOnSubscribeButton(Context context, String offeringId, String tariffName, String subscribable) {
        if (context == null) return;

        ChangeTariff.requestForChangeTariffDialog(context, offeringId, tariffName, subscribable, KLASS, new ChangeTariffListener() {
            @Override
            public void onChangeTariffListenerSuccess(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_SUCCESS,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }

            @Override
            public void onChangeTariffListenerFailure(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_FAILURE,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (klassArrayList == null) {
            return 0;
        } else {
            return klassArrayList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ExpandableLayout expandable_title_layout;
        ExpandableLayout expandable_package_price_layout;
        ExpandableLayout expandable_payg_price_layout;
        ExpandableLayout expandable_detail_layout;
        private ImageView package_expand_icon, payg_price_expand_icon, detail_expand_icon;
        LinearLayout secondLayout, thirdLayout, fourthLayout, attribute_content_layout;
        HeaderLayout headerLayout = new HeaderLayout();
        PackagePriceLayout packagePriceLayout = new PackagePriceLayout();
        PAYgPricesLayout paYgPricesLayout = new PAYgPricesLayout();
        TariffDetailLayout detailLayout = new TariffDetailLayout();

        BakcellButtonNormal button_subscribe;
        ConstraintLayout ratingStarsLayout;
        CheckBox starOne, starTwo, starThree, starFour, starFive;

        View title_belew_line, package_belew_line, payg_belew_line, detail_belew_line;
        BakcellTextViewNormal detail_title_txt;

        class HeaderLayout {
            BakcellTextViewBold offer_title_txt;
            BakcellTextViewNormal tv_price_label, creditValueTxt;
        }

        class PackagePriceLayout {
            BakcellTextViewNormal package_price_title_txt;
            LinearLayout call_content_layout, sms_content_layout, internet_content_layout;
            BakcellTextViewNormal tv_call_lbl, tv_call_value, tv_sms_lbl, tv_sms_value,
                    tv_internet_lbl, tv_internet_value, tv_call_left_value, tv_InternetSubTitle, tv_InternetSubTitleValue;
            ImageView call_icon, sms_icon, internet_icon;
            View seperator;
        }

        class PAYgPricesLayout {
            BakcellTextViewNormal payg_price_title_txt;
            LinearLayout call_content_layout, sms_content_layout;
            BakcellTextViewNormal tv_call_lbl, tv_call_value, tv_sms_lbl, tv_sms_value,
                    tv_internet_lbl, tv_internet_value, tv_call_left_value_1, tv_InternetSubTitle, tv_InternetSubTitleValue;
            ImageView call_icon_1, sms_icon_1, internet_icon_1;
            View seperator_1;
        }


        public ViewHolder(View itemView) {
            super(itemView);
            int width = (int) Tools.convertDpToPixel(RootValues.getInstance().getTARIFFS_WIDTH(), itemView.getContext());
            itemView.setLayoutParams(new RecyclerView.LayoutParams(width,
                    RecyclerView.LayoutParams.MATCH_PARENT));
            expandable_title_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_title_layout);
            expandable_package_price_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_package_price_layout);
            expandable_payg_price_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_payg_price_layout);
            expandable_detail_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_detail_layout);
            secondLayout = (LinearLayout) itemView.findViewById(R.id.second_layout);
            thirdLayout = (LinearLayout) itemView.findViewById(R.id.third_layout);
            fourthLayout = (LinearLayout) itemView.findViewById(R.id.fouth_layout);
            attribute_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.attribute_content_layout);
            package_expand_icon = (ImageView) itemView.findViewById(R.id.package_expand_icon);
            payg_price_expand_icon = (ImageView) itemView.findViewById(R.id.payg_price_expand_icon);
            detail_expand_icon = (ImageView) itemView.findViewById(R.id.detail_expand_icon);

            //View for header
            headerLayout.offer_title_txt = (BakcellTextViewBold) itemView
                    .findViewById(R.id.offer_title_txt);
            headerLayout.tv_price_label = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_price_label);
            headerLayout.creditValueTxt = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.creditValueTxt);

            button_subscribe = (BakcellButtonNormal) itemView.findViewById(R.id.button_subscribe);

            // Rating Stars
            ratingStarsLayout = itemView.findViewById(R.id.ratingStarsLayout);
            starOne = itemView.findViewById(R.id.star1);
            starTwo = itemView.findViewById(R.id.star2);
            starThree = itemView.findViewById(R.id.star3);
            starFour = itemView.findViewById(R.id.star4);
            starFive = itemView.findViewById(R.id.star5);


            //View for PackagePrice
            packagePriceLayout.call_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.call_content_layout);
            packagePriceLayout.tv_call_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_call_lbl);
            packagePriceLayout.tv_call_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_call_value);
            //packagePriceLayout.tv_call_value.setSelected(true);
            packagePriceLayout.tv_sms_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_sms_lbl);
            packagePriceLayout.tv_sms_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_sms_value);
            packagePriceLayout.tv_internet_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_internet_lbl);
            packagePriceLayout.tv_internet_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_internet_value);
            packagePriceLayout.tv_call_left_value = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_call_left_value);
            packagePriceLayout.tv_call_left_value.setSelected(true);
            packagePriceLayout.seperator = (View) itemView.findViewById(R.id.seperator);
            packagePriceLayout.package_price_title_txt = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.package_price_title_txt);
            packagePriceLayout.package_price_title_txt.setSelected(true);
            packagePriceLayout.tv_InternetSubTitle = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitle);
            packagePriceLayout.tv_InternetSubTitleValue = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitleValue);
            packagePriceLayout.sms_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.sms_content_layout);

            packagePriceLayout.internet_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.internet_content_layout);

            packagePriceLayout.call_icon = (ImageView) itemView.findViewById(R.id.call_icon);
            packagePriceLayout.sms_icon = (ImageView) itemView.findViewById(R.id.sms_icon);
            packagePriceLayout.internet_icon = (ImageView) itemView.findViewById(R.id.internet_icon);

            //Views for PayGPrice
            paYgPricesLayout.payg_price_title_txt = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.payg_price_title_txt);
            paYgPricesLayout.payg_price_title_txt.setSelected(true);
            paYgPricesLayout.call_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.call_content_layout_1);
            paYgPricesLayout.tv_call_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_call_lbl_1);
            paYgPricesLayout.tv_call_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_call_value_1);
            //paYgPricesLayout.tv_call_value.setSelected(true);
            paYgPricesLayout.tv_sms_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_sms_lbl_1);
            paYgPricesLayout.tv_sms_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_sms_value_1);
            paYgPricesLayout.tv_internet_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_internet_lbl_1);
            paYgPricesLayout.tv_internet_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_internet_value_1);
            paYgPricesLayout.tv_call_left_value_1 = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_call_left_value_1);
            paYgPricesLayout.tv_call_left_value_1.setSelected(true);
            paYgPricesLayout.seperator_1 = (View) itemView.findViewById(R.id.seperator_1);
            paYgPricesLayout.sms_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.sms_content_layout_1);

            paYgPricesLayout.tv_InternetSubTitle = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitle_payg);
            paYgPricesLayout.tv_InternetSubTitleValue = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitleValue_payg);

            paYgPricesLayout.call_icon_1 = (ImageView) itemView.findViewById(R.id.call_icon_1);
            paYgPricesLayout.sms_icon_1 = (ImageView) itemView.findViewById(R.id.sms_icon_1);
            paYgPricesLayout.internet_icon_1 = (ImageView) itemView.findViewById(R.id.internet_icon_1);


            title_belew_line = itemView.findViewById(R.id.title_belew_line);
            package_belew_line = itemView.findViewById(R.id.package_belew_line);
            payg_belew_line = itemView.findViewById(R.id.payg_belew_line);
            detail_belew_line = itemView.findViewById(R.id.detail_belew_line);

            detail_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.detail_title_txt);
            detail_title_txt.setSelected(true);

            // Init Detail View
            detailLayout = TariffsFragment.loadDetailUI_Ids(itemView);

        }

        void prepareListItem(final Context context, final int i) {


            secondLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isPackagePriceExpanded) {
                        isPackagePriceExpanded = false;
                    } else {
                        isPackagePriceExpanded = true;
                    }
                    isPayGPriceExpanded = false;
                    isDetailExpanded = false;
                    notifyDataSetChanged();
                }
            });

            thirdLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isPayGPriceExpanded) {
                        isPayGPriceExpanded = false;
                    } else {
                        isPayGPriceExpanded = true;
                    }
                    isPackagePriceExpanded = false;
                    isDetailExpanded = false;
                    notifyDataSetChanged();
                }
            });

            fourthLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDetailExpanded) {
                        isDetailExpanded = false;
                    } else {
                        isDetailExpanded = true;
                    }
                    isPackagePriceExpanded = false;
                    isPayGPriceExpanded = false;
                    notifyDataSetChanged();
                }
            });


            if (isDetailExpanded) {
                if (klassArrayList.get(i).getDetails() != null) {

                } else {
                    return;
                }
                expandable_detail_layout.setExpanded(true);
                expandable_title_layout.setExpanded(false);
                expandable_package_price_layout.setExpanded(false);
                expandable_payg_price_layout.setExpanded(false);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_minus));
                package_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                payg_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
            } else if (isPackagePriceExpanded) {
                if (klassArrayList.get(i).getPackagePrice() != null) {

                } else {
                    return;
                }
                expandable_detail_layout.setExpanded(false);
                expandable_title_layout.setExpanded(false);
                expandable_package_price_layout.setExpanded(true);
                expandable_payg_price_layout.setExpanded(false);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                package_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_minus));
                payg_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
            } else if (isPayGPriceExpanded) {

                if (klassArrayList.get(i).getPaygPrice() != null) {

                } else {
                    return;
                }

                expandable_detail_layout.setExpanded(false);
                expandable_title_layout.setExpanded(false);
                expandable_package_price_layout.setExpanded(false);
                expandable_payg_price_layout.setExpanded(true);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                package_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                payg_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_minus));
            } else {
                expandable_detail_layout.setExpanded(false);
                expandable_title_layout.setExpanded(true);
                expandable_package_price_layout.setExpanded(false);
                expandable_payg_price_layout.setExpanded(false);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                package_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                payg_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
            }
        }

        void prepareHeaderItem(Header header, ViewHolder holder) {
            holder.headerLayout.creditValueTxt.setText("");
            holder.headerLayout.tv_price_label.setText("");
            holder.headerLayout.offer_title_txt.setText("");
            if (header.getName() != null) {
                holder.headerLayout.offer_title_txt.setText(header.getName());
                holder.headerLayout.offer_title_txt.setSelected(true);
            }
            if (header.getPriceLabel() != null) {
                holder.headerLayout.tv_price_label.setSelected(true);
                holder.headerLayout.tv_price_label.setText(header.getPriceLabel() + ": ");
            }
            if (header.getPriceValue() != null) {
                holder.headerLayout.creditValueTxt.setText(header.getPriceValue());
            }
            holder.attribute_content_layout.removeAllViews();
            if (header != null && header.getAttributes() != null) {
                for (int i = 0; i < header.getAttributes().size(); i++) {
                    ViewItemTariffsKlassAttribute viewItemTariffsKlassAttribute =
                            new ViewItemTariffsKlassAttribute(context);
                    viewItemTariffsKlassAttribute.getAttribute_name().setText("");
                    viewItemTariffsKlassAttribute.getAttribute_value().setText("");
                    if (header.getAttributes().get(i).getTitle() != null) {
                        viewItemTariffsKlassAttribute.getAttribute_name().setText(header.getAttributes()
                                .get(i).getTitle());
                    }
                    if (header.getAttributes().get(i).getValue() != null) {
                        String value = header.getAttributes().get(i).getValue() + " " + header.getAttributes().get(i).getMetrics();
                        viewItemTariffsKlassAttribute.getAttribute_value().setText(value);
                        /*viewItemTariffsKlassAttribute.getAttribute_value()
                                .append(" " + header.getAttributes().get(i).getMetrics());*/
                        if (Tools.isValueRed(value.trim())) {
                            viewItemTariffsKlassAttribute.setValueTextColor(R.color.colorPrimary);
                        } else {
                            viewItemTariffsKlassAttribute.setValueTextColor(R.color.black);
                        }

                    }
                    /*if (i == (header.getAttributes().size() - 1)) {
                        viewItemTariffsKlassAttribute.getAttribute_value().setTextColor(ContextCompat
                                .getColor(context, R.color.colorPrimary));
                    }*/
                    if (i == header.getAttributes().size() - 1) {
                        viewItemTariffsKlassAttribute.getView().setVisibility(View.GONE);
                    } else {
                        viewItemTariffsKlassAttribute.getView().setVisibility(View.VISIBLE);
                    }

                    if (header.getAttributes().get(i) != null) {
                        Tools.setTariffsIcons(header.getAttributes().get(i).getIconName(), viewItemTariffsKlassAttribute.getIcon(), context);
                        //setOffersSubscriptionsIcons(header.getAttributes().get(i).getIconName(), viewItemTariffsKlassAttribute.getIcon());
                    }


                    holder.attribute_content_layout.addView(viewItemTariffsKlassAttribute);

                }

                //handling subscribe button.!

                if (header.getSubscribable() != null) {

                    if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_NOT_SUBSCRIBE)) {

                        button_subscribe.setEnabled(false);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        ratingStarsLayout.setVisibility(View.GONE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.ALREADY_SUBSCRIBED)) {

                        button_subscribe.setText(R.string.subscribed_lbl);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        button_subscribe.setEnabled(false);
                        ratingStarsLayout.setVisibility(View.VISIBLE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.RENEWABLE)) {

                        button_subscribe.setText(R.string.renew_lbl);
                        button_subscribe.setEnabled(true);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        ratingStarsLayout.setVisibility(View.VISIBLE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_SUBSCRIBE)) {

                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.black));
                        button_subscribe.setText(R.string.subscribe_lbl);
                        button_subscribe.setEnabled(true);
                        ratingStarsLayout.setVisibility(View.GONE);

                    } else {
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        button_subscribe.setEnabled(false);
                        ratingStarsLayout.setVisibility(View.GONE);
                    }

                    button_subscribe.setSelected(true);
                }

            }
        }


        void preparePackagePriceItem(PackagePrice packagePriceItem, ViewHolder holder) {


            if (packagePriceItem == null) {
                secondLayout.setVisibility(View.GONE);
                package_belew_line.setVisibility(View.GONE);
                return;
            }
            secondLayout.setVisibility(View.VISIBLE);
            package_belew_line.setVisibility(View.VISIBLE);
            holder.packagePriceLayout.tv_call_lbl.setText("");
            holder.packagePriceLayout.tv_call_value.setText("");
            holder.packagePriceLayout.tv_sms_lbl.setText("");
            holder.packagePriceLayout.tv_sms_value.setText("");
            holder.packagePriceLayout.tv_internet_lbl.setText("");
            holder.packagePriceLayout.tv_internet_value.setText("");

            holder.packagePriceLayout.call_icon.setVisibility(View.INVISIBLE);
            holder.packagePriceLayout.sms_icon.setVisibility(View.INVISIBLE);
            holder.packagePriceLayout.internet_icon.setVisibility(View.INVISIBLE);

            if (Tools.hasValue(packagePriceItem.getPackagePriceLabel())) {
                holder.packagePriceLayout.package_price_title_txt.setText(packagePriceItem.getPackagePriceLabel());
            }

            if (packagePriceItem.getCall() != null) {
                String priceTemplate = "";
                if (Tools.hasValue(packagePriceItem.getCall().getPriceTemplate())) {
                    priceTemplate = packagePriceItem.getCall().getPriceTemplate();
                }
                boolean isDoubleVal = false;
                if (priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)) {
                    holder.packagePriceLayout.tv_call_left_value.setVisibility(View.VISIBLE);
                    isDoubleVal = true;
                } else {
                    holder.packagePriceLayout.tv_call_left_value.setVisibility(View.INVISIBLE);
                }
                if (Tools.hasValue(packagePriceItem.getCall().getIconName())) {
                    holder.packagePriceLayout.call_icon.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(packagePriceItem.getCall().getIconName()
                            , holder.packagePriceLayout.call_icon, context);
                }

                if (Tools.hasValue(packagePriceItem.getCall().getTitle())) {
                    holder.packagePriceLayout.tv_call_lbl.setText(packagePriceItem.getCall().getTitle());
                }
                if (Tools.hasValue(packagePriceItem.getCall().getTitleValueRight())) {
                    holder.packagePriceLayout.tv_call_value.setText(packagePriceItem.getCall().getTitleValueRight());
                }
                if (Tools.hasValue(packagePriceItem.getCall().getTitleValueLeft())) {
                    holder.packagePriceLayout.tv_call_left_value.setText(packagePriceItem.getCall().getTitleValueLeft());
                }
                if (isDoubleVal && Tools.hasValue(packagePriceItem.getCall().getTitleValueRight())
                        && Tools.hasValue(packagePriceItem.getCall().getTitleValueLeft())) {
                    holder.packagePriceLayout.seperator.setVisibility(View.VISIBLE);
                } else {
                    holder.packagePriceLayout.seperator.setVisibility(View.GONE);
                }
                if (packagePriceItem.getCall().getAttributes() != null) {
                    addAttributesToCall(packagePriceItem.getCall().getAttributes(), holder.packagePriceLayout.call_content_layout, priceTemplate);
                }
            }
            if (packagePriceItem.getSms() != null) {

                if (packagePriceItem.getSms().getIconName() != null) {
                    holder.packagePriceLayout.sms_icon.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(packagePriceItem.getSms().getIconName(), holder.packagePriceLayout.sms_icon, context);
                }

                if (packagePriceItem.getSms().getTitle() != null)
                    holder.packagePriceLayout.tv_sms_lbl.setText(packagePriceItem.getSms()
                            .getTitle());
                if (Tools.hasValue(packagePriceItem.getSms().getTitleValue()))
                    holder.packagePriceLayout.tv_sms_value.setText(packagePriceItem.getSms()
                            .getTitleValue());
                if (packagePriceItem.getSms().getAttributes() != null) {

                    addAttibuteToPackagePrice(packagePriceItem.getSms().getAttributes(),
                            holder.packagePriceLayout.sms_content_layout);
                }
            }
            if (packagePriceItem.getInterent() != null) {

                if (packagePriceItem.getInterent().getIconName() != null) {
                    holder.packagePriceLayout.internet_icon.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(packagePriceItem.getInterent().getIconName(), holder.packagePriceLayout.internet_icon, context);
                }

                if (packagePriceItem.getInterent().getTitle() != null)
                    holder.packagePriceLayout.tv_internet_lbl.setText(packagePriceItem.getInterent()
                            .getTitle());
                if (Tools.hasValue(packagePriceItem.getInterent().getTitleValue()))
                    holder.packagePriceLayout.tv_internet_value.setText(packagePriceItem
                            .getInterent().getTitleValue());
                if (Tools.hasValue(packagePriceItem.getInterent().getSubTitle())) {
                    holder.packagePriceLayout.tv_InternetSubTitle.setText(packagePriceItem.getInterent().getSubTitle());
                    holder.packagePriceLayout.tv_InternetSubTitle.setSelected(true);
                }
                if (Tools.hasValue(packagePriceItem.getInterent().getSubTitleValue())) {
                    holder.packagePriceLayout.tv_InternetSubTitleValue.setText(packagePriceItem.getInterent().getSubTitleValue());
                    holder.packagePriceLayout.tv_InternetSubTitleValue.setSelected(true);
                }
            }

        }

        private void addAttributesToCall(final ArrayList<Attributes> attributes, final LinearLayout container, final String priceTemplate) {
            container.removeAllViews();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < attributes.size(); i++) {
                        final ViewItemTariffsAttribute viewItemTariffsAttribute = new ViewItemTariffsAttribute(context);
                        viewItemTariffsAttribute.getAttribute_label_text().setText("");
                        viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                        viewItemTariffsAttribute.getCredit_value_txt_2().setText("");
                        boolean isDoubleVal = false;
                        if (Tools.hasValue(priceTemplate) && priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)
                                || priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_VALUES)) {
                            viewItemTariffsAttribute.getRl_left().setVisibility(View.VISIBLE);
                            isDoubleVal = true;
                        } else {
                            viewItemTariffsAttribute.getRl_left().setVisibility(View.INVISIBLE);
                        }
                        if (Tools.hasValue(attributes.get(i).getValueLeft())) {
                            viewItemTariffsAttribute.getCredit_value_txt_2().setText(attributes.get(i).getValueLeft());
                        }
                        if (Tools.hasValue(attributes.get(i).getValueRight())) {
                            viewItemTariffsAttribute.getCredit_value_txt_1().setText(attributes.get(i).getValueRight());
                        }
                        if (Tools.hasValue(attributes.get(i).getTitle())) {
                            viewItemTariffsAttribute.getAttribute_label_text().setText(attributes.get(i).getTitle());
                        }

                        if (isDoubleVal && Tools.hasValue(attributes.get(i).getValueLeft())
                                && Tools.hasValue(attributes.get(i).getValueRight())) {
                            viewItemTariffsAttribute.getSeperator().setVisibility(View.VISIBLE);
                            viewItemTariffsAttribute.getCredit_value_txt_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                            viewItemTariffsAttribute.getTv_tzn_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        } else {
                            viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                        }
                        container.addView(viewItemTariffsAttribute);
                    }
                }
            }, 10);

        }

        void preparePayGPriceItem(PaygPrice paygPrice, ViewHolder holder) {

            if (paygPrice == null) {
                thirdLayout.setVisibility(View.GONE);
                payg_belew_line.setVisibility(View.GONE);
                return;
            }
            payg_belew_line.setVisibility(View.VISIBLE);
            thirdLayout.setVisibility(View.VISIBLE);

            holder.paYgPricesLayout.tv_call_lbl.setText("");
            holder.paYgPricesLayout.tv_call_value.setText("");
            holder.paYgPricesLayout.tv_sms_lbl.setText("");
            holder.paYgPricesLayout.tv_sms_value.setText("");
            holder.paYgPricesLayout.tv_internet_lbl.setText("");
            holder.paYgPricesLayout.tv_internet_value.setText("");
            holder.paYgPricesLayout.tv_InternetSubTitle.setText("");
            holder.paYgPricesLayout.tv_InternetSubTitleValue.setText("");

            holder.paYgPricesLayout.call_icon_1.setVisibility(View.INVISIBLE);
            holder.paYgPricesLayout.sms_icon_1.setVisibility(View.INVISIBLE);
            holder.paYgPricesLayout.internet_icon_1.setVisibility(View.INVISIBLE);

            if (Tools.hasValue(paygPrice.getPaygPriceLabel())) {
                holder.paYgPricesLayout.payg_price_title_txt.setText(paygPrice.getPaygPriceLabel());
            }

            if (paygPrice.getCall() != null) {
                String priceTemplate = "";
                if (Tools.hasValue(paygPrice.getCall().getPriceTemplate()))
                    priceTemplate = paygPrice.getCall().getPriceTemplate();
                boolean isDoubleVal = false;
                if (priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)) {
                    holder.paYgPricesLayout.tv_call_left_value_1.setVisibility(View.VISIBLE);
                    isDoubleVal = true;
                } else {
                    holder.paYgPricesLayout.tv_call_left_value_1.setVisibility(View.INVISIBLE);
                }
                if (Tools.hasValue(paygPrice.getCall().getIconName())) {
                    holder.paYgPricesLayout.call_icon_1.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(paygPrice.getCall().getIconName()
                            , holder.paYgPricesLayout.call_icon_1, context);
                }

                if (Tools.hasValue(paygPrice.getCall().getTitle())) {
                    holder.paYgPricesLayout.tv_call_lbl.setText(paygPrice.getCall().getTitle());
                }
                if (Tools.hasValue(paygPrice.getCall().getTitleValueRight())) {
                    holder.paYgPricesLayout.tv_call_value.setText(paygPrice.getCall().getTitleValueRight());
                }
                if (Tools.hasValue(paygPrice.getCall().getTitleValueLeft())) {
                    holder.paYgPricesLayout.tv_call_left_value_1.setText(paygPrice.getCall().getTitleValueLeft());
                }
                if (isDoubleVal && Tools.hasValue(paygPrice.getCall().getTitleValueRight())
                        && Tools.hasValue(paygPrice.getCall().getTitleValueLeft())) {
                    holder.paYgPricesLayout.seperator_1.setVisibility(View.VISIBLE);
                } else {
                    holder.paYgPricesLayout.seperator_1.setVisibility(View.GONE);
                }
                if (paygPrice.getCall().getAttributes() != null) {
                    holder.paYgPricesLayout.call_content_layout.removeAllViews();
                    addAttributesToCall(paygPrice.getCall().getAttributes(), holder.paYgPricesLayout.call_content_layout, priceTemplate);
                }
            }
            if (paygPrice.getSms() != null) {

                if (paygPrice.getSms().getIconName() != null) {
                    holder.paYgPricesLayout.sms_icon_1.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(paygPrice.getSms().getIconName(), holder.paYgPricesLayout.sms_icon_1, context);
                }

                if (paygPrice.getSms().getTitle() != null)
                    holder.paYgPricesLayout.tv_sms_lbl.setText(paygPrice.getSms()
                            .getTitle());
                if (paygPrice.getSms().getTitleValue() != null)
                    holder.paYgPricesLayout.tv_sms_value.setText(paygPrice.getSms()
                            .getTitleValue());
                if (paygPrice.getSms().getAttributes() != null) {
                    holder.paYgPricesLayout.sms_content_layout.removeAllViews();
                    addAttibuteToPackagePrice(paygPrice.getSms().getAttributes(),
                            holder.paYgPricesLayout.sms_content_layout);
                }
            }
            if (paygPrice.getInterent() != null) {

                if (paygPrice.getInterent().getIconName() != null) {
                    holder.paYgPricesLayout.internet_icon_1.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(paygPrice.getInterent().getIconName(), holder.paYgPricesLayout.internet_icon_1, context);
                }

                if (paygPrice.getInterent().getTitle() != null) {
                    holder.paYgPricesLayout.tv_internet_lbl.setText(paygPrice.getInterent()
                            .getTitle());
                }
                if (paygPrice.getInterent().getTitleValue() != null) {
                    holder.paYgPricesLayout.tv_internet_value.setText(paygPrice.getInterent()
                            .getTitleValue());
                }

                if (Tools.hasValue(paygPrice.getInterent().getSubTitle())) {
                    holder.paYgPricesLayout.tv_InternetSubTitle.setText(paygPrice.getInterent().getSubTitle());
                    holder.paYgPricesLayout.tv_InternetSubTitle.setSelected(true);
                }
                if (Tools.hasValue(paygPrice.getInterent().getSubTitleValue())) {
                    holder.paYgPricesLayout.tv_InternetSubTitleValue.setText(paygPrice.getInterent().getSubTitleValue());
                }

            }

        }

        void prepareDetailItem(TariffDetails offerDetail, ViewHolder holder) {

            if (offerDetail == null) {
                fourthLayout.setVisibility(View.GONE);
                detail_belew_line.setVisibility(View.GONE);
                return;
            }
            detail_belew_line.setVisibility(View.VISIBLE);
            fourthLayout.setVisibility(View.VISIBLE);

            if (Tools.hasValue(offerDetail.getDetailLabel())) {
                detail_title_txt.setText(offerDetail.getDetailLabel());
            }

            TariffsFragment.populateDetailSectionContents(offerDetail, detailLayout, context);

        }


    }

    /**
     * Adding attributes to PackagePrice layout
     *
     * @param attributes
     * @param container
     */
    private void addAttibuteToPackagePrice(final ArrayList<Attributes> attributes, final LinearLayout container) {
        container.removeAllViews();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < attributes.size(); i++) {
                    ViewItemTariffsAttribute viewItemTariffsAttribute = new ViewItemTariffsAttribute(context);
                    viewItemTariffsAttribute.getRl_left().setVisibility(View.INVISIBLE);
                    viewItemTariffsAttribute.getAttribute_label_text().setText("");
                    viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                    viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                    if (attributes.get(i).getTitle() != null)
                        viewItemTariffsAttribute.getAttribute_label_text().setText(attributes.get(i).getTitle());
                    if (Tools.hasValue(attributes.get(i).getValue())) {
                        viewItemTariffsAttribute.getCredit_value_txt_1().setText(attributes.get(i).getValue());
                    } else {
                        viewItemTariffsAttribute.hideRightAZN();
                    }
                    container.addView(viewItemTariffsAttribute);
                }
            }
        }, 10);

    }
}
