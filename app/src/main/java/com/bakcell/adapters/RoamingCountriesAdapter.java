package com.bakcell.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.RoamingCountryTabsActivity;
import com.bakcell.fragments.menus.RoamingCountriesFragment;
import com.bakcell.models.roamingnewmodels.RoamingCountriesDetailsHelperModel;
import com.bakcell.models.roamingnewmodels.RoamingCountriesHelperModel;
import com.bakcell.models.roamingnewmodels.RoamingModelNew;
import com.bakcell.models.roamingnewmodels.ServiceListHelperModel;
import com.bakcell.models.roamingnewmodels.ServiceTypeListHelperModel;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

public class RoamingCountriesAdapter extends ArrayAdapter<RoamingCountriesHelperModel> implements Filterable {

    private RoamingCountriesAdapterFilter filter;
    private Context context;
    private Activity activity;
    private ArrayList<RoamingCountriesHelperModel> roamingCountriesHelperModel;
    private ArrayList<RoamingCountriesHelperModel> roamingCountriesHelperModelTemp;
    private RoamingModelNew roamingModelNew;

    public RoamingCountriesAdapter(Context context, Activity activity, ArrayList<RoamingCountriesHelperModel> roamingCountriesHelperModel, RoamingModelNew roamingModelNew) {
        super(context, R.layout.roaming_countries_new_items, roamingCountriesHelperModel);
        this.context = context;
        this.activity = activity;
        this.roamingCountriesHelperModel = roamingCountriesHelperModel;
        this.roamingCountriesHelperModelTemp = roamingCountriesHelperModel;
        this.roamingModelNew = roamingModelNew;
    }//RoamingCountriesAdapter ends

    public void refreshList(ArrayList<RoamingCountriesHelperModel> roamingCountriesHelperModel) {
        if (roamingCountriesHelperModel == null) return;
        this.roamingCountriesHelperModel = roamingCountriesHelperModel;
        this.roamingCountriesHelperModelTemp = roamingCountriesHelperModel;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new RoamingCountriesAdapterFilter();
        }
        return filter;
    }

    private class RoamingCountriesAdapterFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                ArrayList<RoamingCountriesHelperModel> filterList = new ArrayList<>();
                if (constraint.toString().trim().length() == 0) {
                    results.count = roamingCountriesHelperModelTemp.size();
                    results.values = roamingCountriesHelperModelTemp;
                }
                for (RoamingCountriesHelperModel model :roamingCountriesHelperModelTemp) {
                    if (model.country.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filterList.add(model);
                    }
                }
                if (filterList.size() > 0) {
                    results.count = filterList.size();
                    results.values = filterList;
                }
            } else {
                results.count = roamingCountriesHelperModelTemp.size();
                results.values = roamingCountriesHelperModelTemp;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults.count > 0) {
                roamingCountriesHelperModel = (ArrayList<RoamingCountriesHelperModel>) filterResults.values;
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public int getCount() {
        return roamingCountriesHelperModel.size();
    }

    @Override
    public RoamingCountriesHelperModel getItem(int position) {
        return roamingCountriesHelperModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.roaming_countries_new_items, parent, false);
            viewHolder.countryTitle = convertView.findViewById(R.id.countryTitle);
            viewHolder.mainContainer = convertView.findViewById(R.id.mainContainer);
            //set the tag
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //text view selections
        viewHolder.countryTitle.setSelected(true);
        viewHolder.countryTitle.setText(roamingCountriesHelperModel.get(position).country);

        //click listeners
        viewHolder.mainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<RoamingCountriesDetailsHelperModel> roamingCountriesDetailsHelperModel = new ArrayList<>();
                for (int i = 0; i < roamingModelNew.getDetailList().size(); i++) {
                    if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getCountry_id())) {
                        if (roamingModelNew.getDetailList().get(i).getCountry_id().equalsIgnoreCase(roamingCountriesHelperModel.get(position).countryId)) {
                            if (roamingModelNew.getDetailList().get(i).getOperator_list() != null) {
                                for (int j = 0; j < roamingModelNew.getDetailList().get(i).getOperator_list().size(); j++) {
                                    if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getOperator())) {
                                        //get operator
                                        String operator = roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getOperator();
                                        if (roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list() != null) {
                                            ArrayList<ServiceListHelperModel> serviceListHelperModelsList = new ArrayList<>();
                                            for (int k = 0; k < roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().size(); k++) {
                                                ArrayList<ServiceTypeListHelperModel> serviceTypeListHelperModelsList = new ArrayList<>();
                                                for (int l = 0; l < roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().size(); l++) {
                                                    //add the service type list helper object in the service type list helper array list
                                                    String unit = "";
                                                    if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().get(l).getUnit())) {
                                                        unit = roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().get(l).getUnit();
                                                    }//if unit ends
                                                    String value = "";
                                                    if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().get(l).getValue())) {
                                                        value = roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().get(l).getValue();
                                                    }//if value ends
                                                    String type = "";
                                                    if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().get(l).getService_type())) {
                                                        type = roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().get(l).getService_type();
                                                    }//if type ends
                                                    serviceTypeListHelperModelsList.add(new ServiceTypeListHelperModel(unit, value, type));
                                                }//for (int l = 0; l < roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_type_list().size(); l++) ends
                                                //add the service list object in the service list helper array list
                                                String service = "";
                                                if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService())) {
                                                    service = roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService();
                                                }//if service ends
                                                String serviceUnit = "";
                                                if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_unit())) {
                                                    serviceUnit = roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().get(k).getService_unit();
                                                }//if service unit ends
                                                serviceListHelperModelsList.add(new ServiceListHelperModel(service, serviceUnit, serviceTypeListHelperModelsList));
                                            }//for (int k = 0; k < roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list().size(); k++) ends
                                            //add the service list in the service list helper model
                                            roamingCountriesDetailsHelperModel.add(new RoamingCountriesDetailsHelperModel(operator, serviceListHelperModelsList));
                                        }//if (roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getService_list() != null) ends
                                    }//if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getOperator_list().get(j).getOperator())) ends
                                }//for (int j = 0; j < roamingModelNew.getDetailList().get(i).getOperator_list().size(); j++) ends
                            }//if (roamingModelNew.getDetailList().get(i).getOperator_list() != null) ends
                        }//if (roamingModelNew.getDetailList().get(i).getCountry_id().equalsIgnoreCase(roamingCountriesHelperModel.get(position).countryId)) ends
                    }// if (Tools.hasValue(roamingModelNew.getDetailList().get(i).getCountry_id())) ends
                }//for (int i = 0; i < roamingModelNew.getDetailList().size(); i++) ends

                //start the new activity
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(RoamingCountriesFragment.ROAMING_COUNTRIES_DETAILS_DATA_KEY, roamingCountriesDetailsHelperModel);
                bundle.putString(RoamingCountriesFragment.ROAMING_COUNTRIES_SELECTED_KEY, roamingCountriesHelperModel.get(position).country);
                BaseActivity.startNewActivity(activity, RoamingCountryTabsActivity.class, bundle);

                /*for (int i = 0; i < roamingCountriesDetailsHelperModel.size(); i++) {
                    Log.e("asdkjkl2jk", "operator:::" + roamingCountriesDetailsHelperModel.get(i).operator);
                    for (int k = 0; k < roamingCountriesDetailsHelperModel.get(i).serviceListHelperModel.size(); k++) {
                        Log.e("asdkjkl2jk", "service:::" + roamingCountriesDetailsHelperModel.get(i).serviceListHelperModel.get(k).service);
                    }
                }*/
            }//onClick ends
        });
        return convertView;
    }//getView ends

    public class ViewHolder {
        BakcellTextViewNormal countryTitle;
        RelativeLayout mainContainer;
    }
}//class ends
