package com.bakcell.adapters;

import static com.bakcell.adapters.CardAdapterSupplementaryOffers.OFFER_CURRENCY;

import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.Constants;
import com.bakcell.interfaces.UpdateListOnSuccessfullySurvey;
import com.bakcell.models.inappsurvey.Answers;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Questions;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.inappsurvey.UserSurveys;
import com.bakcell.models.mysubscriptions.Subscriptions;
import com.bakcell.models.mysubscriptions.details.DetailsAttributes;
import com.bakcell.models.mysubscriptions.details.RoamingDetailsCountries;
import com.bakcell.models.mysubscriptions.details.SubscriptionsDetailsMain;
import com.bakcell.models.mysubscriptions.header.OfferAction;
import com.bakcell.models.mysubscriptions.header.OfferUsage;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.header.HeaderAttributes;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.ViewItemMySubscriptionsUsage;
import com.bakcell.viewsitem.ViewItemOfferDetailBulletsItem;
import com.bakcell.viewsitem.ViewItemOfferDetailPriceAttribute;
import com.bakcell.viewsitem.ViewItemOfferDetailRoamingCountries;
import com.bakcell.viewsitem.ViewItemOfferDetailRoundingAttribute;
import com.bakcell.viewsitem.ViewItemOfferDetailTitleWithSubTitleDescAttribute;
import com.bakcell.webservices.subscribeunsubscribe.SubscribeUnsubscribe;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Noman on 9/25/2017.
 */

public class CardAdapterMySubscriptionsItem extends RecyclerView.Adapter<CardAdapterMySubscriptionsItem.ViewHolder> {

    private ArrayList<Subscriptions> subscriptionsArrayList;
    private Context context;
    private final String fromClass = "CardAdapterMySubscriptionsItem";
    private boolean isRoaming, isInternet;

    private static boolean isdetailExpanded = false;

    private final String IS_UNLIMITED = "free";

    private String dashboardUsageType = "";

    public CardAdapterMySubscriptionsItem(Context context, ArrayList<Subscriptions> arrayList,
                                          boolean isRoaming, boolean isInternet, String dashboardUsageTypeStr) {
        isdetailExpanded = false;
        this.subscriptionsArrayList = arrayList;
        this.context = context;
        this.isRoaming = isRoaming;
        this.isInternet = isInternet;
        this.dashboardUsageType = dashboardUsageTypeStr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_item_my_subscriptions, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        viewHolder.prepareListItem(context, subscriptionsArrayList.get(i), i);

        viewHolder.prepareOfferDetail(context, subscriptionsArrayList.get(i).getDetails());


        final int i1 = i;
        int rateStars = 0;

        viewHolder.btnDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (subscriptionsArrayList.get(i1) != null &&
                            subscriptionsArrayList.get(i1).getHeader() != null) {
                        SubscribeUnsubscribe.requestUnsubscribeOffers(context,
                                subscriptionsArrayList.get(i1).getHeader().getOfferingId(),
                                subscriptionsArrayList.get(i1).getHeader().getName());
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        });
        viewHolder.btnRenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (subscriptionsArrayList.get(i1) != null &&
                            subscriptionsArrayList.get(i1).getHeader() != null) {
                        SubscribeUnsubscribe.requestRenewalOffers(context,
                                subscriptionsArrayList.get(i1).getHeader().getOfferingId(),
                                subscriptionsArrayList.get(i1).getHeader().getName());
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        });

        Data data = PrefUtils.getInAppSurvey(context).getData();
        if (data != null && data.getUserSurveys() != null) {
            List<UserSurveys> userSurveys = data.getUserSurveys();
            for (int j = 0; j < userSurveys.size(); j++) {
                if (userSurveys.get(j).getOfferingId().equals(subscriptionsArrayList.get(i1).getHeader().getOfferingId())) {
                    if (userSurveys.get(j).getAnswer() != null) {
                        if (userSurveys.get(j).getAnswer() != null) {
                        String surveyId = userSurveys.get(j).getSurveyId();
                        if (surveyId != null) {
                            Surveys survey = data.findSurveyById(Integer.parseInt(surveyId));
                            if (Tools.isNumeric(userSurveys.get(j).getAnswerId())) {
                                if (Tools.isNumeric(userSurveys.get(j).getQuestionId())) {
                                    rateStars = setMySubscriptionViewHolder(Integer.parseInt(userSurveys.get(j).getAnswerId()),
                                            Integer.parseInt(userSurveys.get(j).getQuestionId()), survey, viewHolder);
                                }
                            }
                        }}
                    } else {
                        rateStars = setMySubscriptionViewHolder(0,0,null, viewHolder);
                    }
                }
            }
        }
        int finalRating = rateStars;
        viewHolder.ratingStarsLayout.setOnClickListener(
                v -> {
                    inAppFeedback(context, subscriptionsArrayList.get(i1).getHeader().getOfferingId(), finalRating, i);
                }
        );

    }

    private int setMySubscriptionViewHolder(int answerId, int questionId, Surveys survey, ViewHolder viewHolder) {
        if (survey != null) {
            Questions question = survey.findQuestion(questionId);
            if (question != null) {
                List<Answers> answers = question.getAnswers();
                if (answers != null) {
                    if (answers.size() > 0 && answers.get(0).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(false);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 1;
                    } else if (answers.size() > 1 && answers.get(1).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 2;
                    } else if (answers.size() > 2 && answers.get(2).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 3;
                    } else if (answers.size() > 3 && answers.get(3).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(false);
                        return 4;
                    } else if (answers.size() > 4 && answers.get(4).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(true);
                        return 5;
                    }

                } else {
                    viewHolder.starOne.setChecked(false);
                    viewHolder.starTwo.setChecked(false);
                    viewHolder.starThree.setChecked(false);
                    viewHolder.starFour.setChecked(false);
                    viewHolder.starFive.setChecked(false);
                    return 0;
                }
            } else {
                viewHolder.starOne.setChecked(false);
                viewHolder.starTwo.setChecked(false);
                viewHolder.starThree.setChecked(false);
                viewHolder.starFour.setChecked(false);
                viewHolder.starFive.setChecked(false);
                return 0;
            }
        } else {
            viewHolder.starOne.setChecked(false);
            viewHolder.starTwo.setChecked(false);
            viewHolder.starThree.setChecked(false);
            viewHolder.starFour.setChecked(false);
            viewHolder.starFive.setChecked(false);
            return 0;
        }
        return 0;
    }

    private void updateList(int position) {
        notifyItemChanged(position);
    }

    private void inAppFeedback(Context context, String tariffId, int rate, int position) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_BUNDLE_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.BUNDLE_PAGE);
            if (surveys != null) {
                InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                if (surveys.getQuestions() != null) {
                    currentVisit = 0;
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(new UpdateListOnSuccessfullySurvey() {
                        @Override
                        public void updateListOnSuccessfullySurvey() {
                            updateList(position);
                        }
                    });
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "2", tariffId, rate);
                }
            }
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_BUNDLE_PAGE, currentVisit);
    }

    @Override
    public int getItemCount() {
        return subscriptionsArrayList.size();
    }

    public void updateAdapter(ArrayList<Subscriptions> arrayList) {

        subscriptionsArrayList = new ArrayList<>();

        subscriptionsArrayList.addAll(arrayList);

        notifyDataSetChanged();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        ExpandableLayout expandable_title_layout;
        ExpandableLayout expandable_detail_layout;
        LinearLayout detail_header_layout, usage_content_layout, detail_layout;
        ImageView detail_expand_icon, ic_img_call;
        BakcellButtonNormal btnRenew, btnDeactivate;
        View dummyView;


        ConstraintLayout ratingStarsLayout;
        CheckBox starOne, starTwo, starThree, starFour, starFive;

        // Header
        BakcellTextViewBold subs_title_txt;

        BakcellTextViewNormal subs_status_txt, creditValueTxt, freePriceTxt, tv_tzn, tv_action_desc, attribute_name, priceIcon, attribute_value;
        RelativeLayout offer_type_layout, offer_group_layout, action_layout;
        //offer Group name
        BakcellTextViewNormal tv_offer_type_name, tv_offer_group_name;
        ImageView ic_call_type, ic_internet_type, ic_campaign_type;
        View line_below_type, lineBelowGroup;

        ImageView icOfferGroupPhone, icOfferGroupTab, icOfferGroupDesktop;

        class OfferDetailContents {
            // Offer Details content start
            // priceSection
            RelativeLayout priceSection, priceTitleLayout;
            ImageView priceTitleIcon, ic_img_call;
            BakcellTextViewNormal detailPriceTitle, detailPriceValue, detailPriceDescription;
            LinearLayout offerDetailAttributeLayout;
            View price_below_line;

            // roundingSection
            RelativeLayout roundingSection;
            LinearLayout roundingTitleLayout;
            ImageView roundingTitleIcon;
            BakcellTextViewNormal roundingTitleText, roundingTitleValue, detailRoundingDescription;
            LinearLayout offerDetailroundingAttributeLayout;
            View rounding_below_line;

            // TextWithTitleSection
            RelativeLayout textWithTitleSection;
            BakcellTextViewNormal text_title, decription_text;
            View textWithTitle_below_line;

            //textWithOutTitleSection
            RelativeLayout textWithOutTitleSection;
            BakcellTextViewNormal textWithOutTitle_text;
            View textWithOutTitle_below_line;

            // textWithPointsSections
            RelativeLayout textWithPointsSections;
            LinearLayout textWithPointsLayout;
            View textWithPoints_below_line;

            //titleSubTitleValueDescSection
            RelativeLayout titleSubTitleValueDescSection, titleSubTitleValueDescTitleLayout;
            BakcellTextViewNormal titleSubTitleValueDescTitle;
            LinearLayout titleSubTitleValueDescAttributeLayout;
            View textSubTitleDesc_below_line;

            //dateSection
            RelativeLayout dateSection;
            LinearLayout dateLayout;
            BakcellTextViewNormal dateDescription, dateFromLabel, dateFromValue, dateToLabel, dateToValue;
            View date_below_line;

            //timeSection
            RelativeLayout timeSection;
            LinearLayout timeLayout;
            BakcellTextViewNormal timeDescription, timefromLabel, timefromValue, timetoLabel, timetoValue;
            View time_below_line;

            //roamingSection
            RelativeLayout roamingSection;
            BakcellTextViewNormal roamingDescriptionAbove, roamingDescriptionBelow;
            LinearLayout roamingCountryLayout;
            View roaming_below_line;

            // freeResourceValiditySection
            RelativeLayout freeResourceValiditySection, freeResourceTitleLayout;
            LinearLayout freeResourceOnnetLayout;
            BakcellTextViewNormal freeResourceTitle, freeResourceValue, onnetTitle, onnetValue, freeResourceDescription;
            BakcellTextViewNormal priceIcon;
            // Offer Details content end
        }

        OfferDetailContents offerDetailContents = new OfferDetailContents();

        public ViewHolder(View itemView) {
            super(itemView);
            expandable_title_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_title_layout);
            expandable_detail_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_detail_layout);
            detail_header_layout = (LinearLayout) itemView.findViewById(R.id.detail_layout);
            detail_expand_icon = (ImageView) itemView.findViewById(R.id.detail_expand_icon);
            ic_img_call = (ImageView) itemView.findViewById(R.id.ic_img_call);

            btnRenew = (BakcellButtonNormal) itemView.findViewById(R.id.button_renew);
            btnDeactivate = (BakcellButtonNormal) itemView.findViewById(R.id.button_deactivate);

            // Rating Stars
            ratingStarsLayout = itemView.findViewById(R.id.ratingStarsLayout);
            starOne = itemView.findViewById(R.id.star1);
            starTwo = itemView.findViewById(R.id.star2);
            starThree = itemView.findViewById(R.id.star3);
            starFour = itemView.findViewById(R.id.star4);
            starFive = itemView.findViewById(R.id.star5);

            subs_title_txt = (BakcellTextViewBold) itemView.findViewById(R.id.subs_title_txt);
            subs_status_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.subs_status_txt);
            creditValueTxt = (BakcellTextViewNormal) itemView.findViewById(R.id.creditValueTxt);
            freePriceTxt = (BakcellTextViewNormal) itemView.findViewById(R.id.freePriceTxt);
            tv_tzn = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_tzn);
            tv_action_desc = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_action_desc);
            attribute_name = (BakcellTextViewNormal) itemView.findViewById(R.id.attribute_name);
            attribute_value = (BakcellTextViewNormal) itemView.findViewById(R.id.attribute_value);
            priceIcon = (BakcellTextViewNormal) itemView.findViewById(R.id.priceIcon);
            dummyView = itemView.findViewById(R.id.dummy_view);

            offer_type_layout = (RelativeLayout) itemView.findViewById(R.id.offer_type_layout);
            tv_offer_type_name = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_offer_type_name);
            line_below_type = itemView.findViewById(R.id.line_below_type);
            action_layout = (RelativeLayout) itemView.findViewById(R.id.action_layout);

            //Offer Group Layout
            offer_group_layout = (RelativeLayout) itemView.findViewById(R.id.offer_group_layout);
            tv_offer_group_name = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_offer_group_name);
            ic_call_type = (ImageView) itemView.findViewById(R.id.ic_call_type);
            ic_internet_type = (ImageView) itemView.findViewById(R.id.ic_internet_type);
            ic_campaign_type = (ImageView) itemView.findViewById(R.id.ic_campaign_type);
            lineBelowGroup = itemView.findViewById(R.id.lineBelowGroup);

            usage_content_layout = (LinearLayout) itemView.findViewById(R.id.usage_content_layout);

            icOfferGroupPhone = (ImageView) itemView.findViewById(R.id.icOfferGroupPhone);
            icOfferGroupTab = (ImageView) itemView.findViewById(R.id.icOfferGroupTab);
            icOfferGroupDesktop = (ImageView) itemView.findViewById(R.id.icOfferGroupDesktop);


            ///// Views for details
            detail_layout = (LinearLayout) itemView.findViewById(R.id.detail_layout);
            offerDetailContents.priceSection = (RelativeLayout) itemView.findViewById(R.id.priceSection);
            offerDetailContents.priceTitleLayout = (RelativeLayout) itemView.findViewById(R.id.priceTitleLayout);
            offerDetailContents.priceTitleIcon = (ImageView) itemView.findViewById(R.id.priceTitleIcon);
            offerDetailContents.detailPriceTitle = (BakcellTextViewNormal) itemView.findViewById(R.id.detailPriceTitle);
            offerDetailContents.detailPriceValue = (BakcellTextViewNormal) itemView.findViewById(R.id.detailPriceValue);
            offerDetailContents.priceIcon = (BakcellTextViewNormal) itemView.findViewById(R.id.priceIconP);
            offerDetailContents.detailPriceDescription = (BakcellTextViewNormal) itemView.findViewById(R.id.detailPriceDescription);
            offerDetailContents.offerDetailAttributeLayout = (LinearLayout) itemView.findViewById(R.id.offerDetailAttributeLayout);
            offerDetailContents.price_below_line = (View) itemView.findViewById(R.id.price_below_line);

            // roundingSection
            offerDetailContents.roundingSection = (RelativeLayout) itemView.findViewById(R.id.roundingSection);
            offerDetailContents.roundingTitleLayout = (LinearLayout) itemView.findViewById(R.id.roundingTitleLayout);
            offerDetailContents.roundingTitleIcon = (ImageView) itemView.findViewById(R.id.roundingTitleIcon);
            offerDetailContents.roundingTitleText = (BakcellTextViewNormal) itemView.findViewById(R.id.roundingTitleText);
            offerDetailContents.roundingTitleValue = (BakcellTextViewNormal) itemView.findViewById(R.id.roundingTitleValue);
            offerDetailContents.detailRoundingDescription = (BakcellTextViewNormal) itemView.findViewById(R.id.detailRoundingDescription);
            offerDetailContents.offerDetailroundingAttributeLayout = (LinearLayout) itemView.findViewById(R.id.offerDetailroundingAttributeLayout);
            offerDetailContents.rounding_below_line = (View) itemView.findViewById(R.id.rounding_below_line);

            // TextWithTitleSection
            offerDetailContents.textWithTitleSection = (RelativeLayout) itemView.findViewById(R.id.textWithTitleSection);
            offerDetailContents.text_title = (BakcellTextViewNormal) itemView.findViewById(R.id.text_title);
            offerDetailContents.decription_text = (BakcellTextViewNormal) itemView.findViewById(R.id.decription_text);
            offerDetailContents.textWithTitle_below_line = (View) itemView.findViewById(R.id.textWithTitle_below_line);

            //textWithOutTitleSection
            offerDetailContents.textWithOutTitleSection = (RelativeLayout) itemView.findViewById(R.id.textWithOutTitleSection);
            offerDetailContents.textWithOutTitle_text = (BakcellTextViewNormal) itemView.findViewById(R.id.textWithOutTitle_text);
            offerDetailContents.textWithOutTitle_below_line = (View) itemView.findViewById(R.id.textWithOutTitle_below_line);

            // textWithPointsSections
            offerDetailContents.textWithPointsSections = (RelativeLayout) itemView.findViewById(R.id.textWithPointsSections);
            offerDetailContents.textWithPointsLayout = (LinearLayout) itemView.findViewById(R.id.textWithPointsLayout);
            offerDetailContents.textWithPoints_below_line = (View) itemView.findViewById(R.id.textWithPoints_below_line);

            //titleSubTitleValueDescSection
            offerDetailContents.titleSubTitleValueDescSection = (RelativeLayout) itemView.findViewById(R.id.titleSubTitleValueDescSection);
            offerDetailContents.titleSubTitleValueDescTitleLayout = (RelativeLayout) itemView.findViewById(R.id.titleSubTitleValueDescTitleLayout);
            offerDetailContents.titleSubTitleValueDescTitle = (BakcellTextViewNormal) itemView.findViewById(R.id.titleSubTitleValueDescTitle);
            offerDetailContents.titleSubTitleValueDescAttributeLayout = (LinearLayout) itemView.findViewById(R.id.titleSubTitleValueDescAttributeLayout);
            offerDetailContents.textSubTitleDesc_below_line = (View) itemView.findViewById(R.id.textSubTitleDesc_below_line);

            //dateSection
            offerDetailContents.dateSection = (RelativeLayout) itemView.findViewById(R.id.dateSection);
            offerDetailContents.dateLayout = (LinearLayout) itemView.findViewById(R.id.dateLayout);
            offerDetailContents.dateDescription = (BakcellTextViewNormal) itemView.findViewById(R.id.dateDescription);
            offerDetailContents.dateFromLabel = (BakcellTextViewNormal) itemView.findViewById(R.id.dateFromLabel);
            offerDetailContents.dateFromValue = (BakcellTextViewNormal) itemView.findViewById(R.id.dateFromValue);
            offerDetailContents.dateToLabel = (BakcellTextViewNormal) itemView.findViewById(R.id.dateToLabel);
            offerDetailContents.dateToValue = (BakcellTextViewNormal) itemView.findViewById(R.id.dateToValue);
            offerDetailContents.date_below_line = (View) itemView.findViewById(R.id.date_below_line);

            //timeSection
            offerDetailContents.timeSection = (RelativeLayout) itemView.findViewById(R.id.timeSection);
            offerDetailContents.timeLayout = (LinearLayout) itemView.findViewById(R.id.timeLayout);
            offerDetailContents.timeDescription = (BakcellTextViewNormal) itemView.findViewById(R.id.timeDescription);
            offerDetailContents.timefromLabel = (BakcellTextViewNormal) itemView.findViewById(R.id.timefromLabel);
            offerDetailContents.timefromValue = (BakcellTextViewNormal) itemView.findViewById(R.id.timefromValue);
            offerDetailContents.timetoLabel = (BakcellTextViewNormal) itemView.findViewById(R.id.timetoLabel);
            offerDetailContents.timetoValue = (BakcellTextViewNormal) itemView.findViewById(R.id.timetoValue);
            offerDetailContents.time_below_line = (View) itemView.findViewById(R.id.time_below_line);

            //roamingSection
            offerDetailContents.roamingSection = (RelativeLayout) itemView.findViewById(R.id.roamingSection);
            offerDetailContents.roamingDescriptionAbove = (BakcellTextViewNormal) itemView.findViewById(R.id.roamingDescriptionAbove);
            offerDetailContents.roamingDescriptionBelow = (BakcellTextViewNormal) itemView.findViewById(R.id.roamingDescriptionBelow);
            offerDetailContents.roamingCountryLayout = (LinearLayout) itemView.findViewById(R.id.roamingCountryLayout);
            offerDetailContents.roaming_below_line = (View) itemView.findViewById(R.id.roaming_below_line);

            // freeResourceValiditySection
            offerDetailContents.freeResourceValiditySection = (RelativeLayout) itemView.findViewById(R.id.freeResourceValiditySection);
            offerDetailContents.freeResourceTitleLayout = (RelativeLayout) itemView.findViewById(R.id.freeResourceTitleLayout);
            offerDetailContents.freeResourceOnnetLayout = (LinearLayout) itemView.findViewById(R.id.freeResourceOnnetLayout);
            offerDetailContents.freeResourceTitle = (BakcellTextViewNormal) itemView.findViewById(R.id.freeResourceTitle);
            offerDetailContents.freeResourceValue = (BakcellTextViewNormal) itemView.findViewById(R.id.freeResourceValue);
            offerDetailContents.onnetTitle = (BakcellTextViewNormal) itemView.findViewById(R.id.onnetTitle);
            offerDetailContents.onnetValue = (BakcellTextViewNormal) itemView.findViewById(R.id.onnetValue);
            offerDetailContents.freeResourceDescription = (BakcellTextViewNormal) itemView.findViewById(R.id.freeResourceDescription);
        }

        public void prepareListItem(final Context context, Subscriptions subscriptions, final int i) {
            if (subscriptions == null) return;

            // Header
            subs_title_txt.setText("");
            subs_status_txt.setText("");
            creditValueTxt.setText("");
            freePriceTxt.setText("");

            btnRenew.setVisibility(View.GONE);
            btnDeactivate.setVisibility(View.GONE);


            if (subscriptions.getHeader() != null) {

                if (subscriptions.getHeader().getBtnDeactivate() != null && subscriptions.getHeader().getBtnDeactivate().equalsIgnoreCase("1")) {
                    btnDeactivate.setVisibility(View.VISIBLE);
                    btnDeactivate.setSelected(true);
                } else {
                    btnDeactivate.setVisibility(View.GONE);
                }
                if (subscriptions.getHeader().getBtnRenew() != null
                        && subscriptions.getHeader().getBtnRenew().equalsIgnoreCase("1")) {
                    btnRenew.setVisibility(View.VISIBLE);
                } else {

                    btnRenew.setVisibility(View.GONE);
                }

                if (Tools.hasValue(subscriptions.getHeader().getName())) {
                    subs_title_txt.setText(subscriptions.getHeader().getName());
                    subs_title_txt.setSelected(true);
                }
                if (Tools.hasValue(subscriptions.getHeader().getStatus())) {
                    subs_status_txt.setText(subscriptions.getHeader().getStatus());
                }
                if (Tools.hasValue(subscriptions.getHeader().getPrice())) {
                    creditValueTxt.setText(subscriptions.getHeader().getPrice());
                    tv_tzn.setVisibility(View.VISIBLE);
                    creditValueTxt.setVisibility(View.VISIBLE);
                } else {
                    tv_tzn.setVisibility(View.GONE);
                }
                if (Tools.hasValue(subscriptions.getHeader().getIsFreeResource())
                        && subscriptions.getHeader().getIsFreeResource().equalsIgnoreCase("true")) {
                    freePriceTxt.setText(R.string.lbl_free);
                    tv_tzn.setVisibility(View.GONE);
                    creditValueTxt.setVisibility(View.GONE);
                } else {
                    freePriceTxt.setVisibility(View.GONE);
                }

                if (Tools.hasValue(dashboardUsageType)) {
                    showOfferGroup(subscriptions);
                } else if (isInternet) {
                    showOfferGroup(subscriptions);
                } else {
                    offer_group_layout.setVisibility(View.GONE);
                    lineBelowGroup.setVisibility(View.GONE);
                }

                if (isRoaming) {
                    showOfferType(subscriptions);
                    showOfferGroup(subscriptions);
                } else {
                    offer_type_layout.setVisibility(View.GONE);
                    line_below_type.setVisibility(View.GONE);
//                    if (!isInternet) {
//                        offer_group_layout.setVisibility(View.GONE);
//                        lineBelowGroup.setVisibility(View.GONE);
//                    }
                }

                if (subscriptions.getHeader().getAttributeList() != null &&
                        subscriptions.getHeader().getAttributeList().size() > 0) {
                    action_layout.setVisibility(View.VISIBLE);
                    OfferAction offerAction = subscriptions.getHeader().getAttributeList().get(0);

                    if (Tools.hasValue(offerAction.getDesc())) {
                        tv_action_desc.setVisibility(View.VISIBLE);
                        tv_action_desc.setText(offerAction.getDesc());
                    } else {
                        tv_action_desc.setVisibility(View.GONE);
                    }
                    if (Tools.hasValue(offerAction.getTitle())) {
                        attribute_name.setText(offerAction.getTitle());
                    } else {
                        attribute_name.setText("");
                    }
                    if (Tools.hasValue(offerAction.getIconMap())) {
                        Tools.setOffersSubscriptionsIcons(context, offerAction.getIconMap(), ic_img_call);
                    } else {
                        ic_img_call.setVisibility(View.INVISIBLE);
                    }

                    /*if (Tools.hasValue(offerAction.getValue())) {
                        attribute_value.setText(offerAction.getValue());
                        if (Tools.hasValue(offerAction.getUnit())) {
                            if (offerAction.getUnit().equalsIgnoreCase(Constants.UserCurrentKeyword.CURRENCY_AZN)) {
                                priceIcon.setVisibility(View.VISIBLE);
                            } else {
                                priceIcon.setVisibility(View.GONE);
                                attribute_value.append(" " + offerAction.getUnit());
                            }
                        } else {
                            priceIcon.setVisibility(View.GONE);
                        }
                    } else {
                        attribute_value.setText("");
                    }*/
                    if (Tools.hasValue(offerAction.getValue())) {
                        attribute_value.setText(offerAction.getValue());
                        if (offerAction.getValue().equalsIgnoreCase("Unlimited")) {
                            priceIcon.setVisibility(View.GONE);
                            attribute_value.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        } else if (offerAction.getValue().equalsIgnoreCase("Безлимитный")) {
                            priceIcon.setVisibility(View.GONE);
                            attribute_value.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        } else if (offerAction.getValue().equalsIgnoreCase("Limitsiz")) {
                            priceIcon.setVisibility(View.GONE);
                            attribute_value.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        } else if (offerAction.getValue().equalsIgnoreCase("Free")) {
                            priceIcon.setVisibility(View.GONE);
                            attribute_value.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        } else if (offerAction.getValue().equalsIgnoreCase("БЕСПЛАТНО")) {
                            priceIcon.setVisibility(View.GONE);
                            attribute_value.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        } else if (offerAction.getValue().equalsIgnoreCase("PULSUZ")) {
                            priceIcon.setVisibility(View.GONE);
                            attribute_value.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        } else {
                            attribute_value.setTextColor(context.getResources().getColor(R.color.text_gray));
                            //check the unit
                            if (Tools.hasValue(offerAction.getUnit())) {
                                if (offerAction.getUnit().equalsIgnoreCase(Constants.UserCurrentKeyword.CURRENCY_AZN)) {
                                    priceIcon.setVisibility(View.VISIBLE);
                                } else {
                                    priceIcon.setVisibility(View.GONE);
                                    attribute_value.append(" " + offerAction.getUnit());
                                }
                            } else {
                                priceIcon.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        attribute_value.setText("");
                    }
                } else {
                    action_layout.setVisibility(View.GONE);
                }

                setUsageDataToUI(context, subscriptions);

            }


            // end Header


//            if (isdetailExpanded) {
//                expandable_detail_layout.expand();
//                expandable_title_layout.collapse();
//                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
//            } else {
            expandable_title_layout.expand();
            expandable_detail_layout.collapse();
            detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
//            }


            detail_header_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (expandable_title_layout.isExpanded()) {
                        expandable_detail_layout.expand();
                        detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
                        expandable_title_layout.collapse();
                        isdetailExpanded = true;
//                        notifyDataSetChanged();
                    } else {
                        expandable_title_layout.expand();
                        expandable_detail_layout.collapse();
                        detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                        isdetailExpanded = false;
//                        notifyDataSetChanged();
                    }
                }
            });

            if (i == subscriptionsArrayList.size() - 1) {
                dummyView.setVisibility(View.INVISIBLE);
            } else {
                dummyView.setVisibility(View.GONE);
            }


        }

        private void setUsageDataToUI(final Context context, final Subscriptions subscriptions) {
            if (context != null || subscriptions != null && subscriptions.getHeader() != null) {
            } else {
                return;
            }


            if (subscriptions.getHeader().getUsage() != null && subscriptions.getHeader()
                    .getUsage().size() > 0) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        usage_content_layout.removeAllViews();
                        for (int j = 0; j < subscriptions.getHeader().getUsage().size(); j++) {

                            ViewItemMySubscriptionsUsage viewItemMySubscriptionsUsage =
                                    new ViewItemMySubscriptionsUsage(context);

                            viewItemMySubscriptionsUsage.getRemaining_title().setText("");
                            viewItemMySubscriptionsUsage.getRemaining_time_title().setText("");
                            viewItemMySubscriptionsUsage.getRenew_date().setText("");
                            viewItemMySubscriptionsUsage.getActivation_date().setText("");
                            viewItemMySubscriptionsUsage.getRenew_title().setText("");
                            viewItemMySubscriptionsUsage.getRenew_time_title().setText("");

                            OfferUsage offerUsage = subscriptions.getHeader().getUsage().get(j);

                            if (offerUsage != null) {

                                // All Inclusive`
                                if (Tools.hasValue(dashboardUsageType) && dashboardUsageType.equalsIgnoreCase(offerUsage.getType())) {
                                    viewItemMySubscriptionsUsage.getUsageLayoutBG().setBackgroundResource(R.color.light_gray);
                                    viewItemMySubscriptionsUsage.getRemaining_progress_min().setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progress_bar_horizontal_bg_darkgrey_max));
                                    viewItemMySubscriptionsUsage.getRenew_progress_max().setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progress_bar_horizontal_bg_darkgrey_max));
                                    viewItemMySubscriptionsUsage.getLineBelow().setVisibility(View.VISIBLE);
                                } else {
                                    viewItemMySubscriptionsUsage.getUsageLayoutBG().setBackgroundResource(R.color.white);
                                    viewItemMySubscriptionsUsage.getRemaining_progress_min().setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progress_bar_horizontal_bg_max));
                                    viewItemMySubscriptionsUsage.getRenew_progress_max().setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progress_bar_horizontal_bg_max));
                                    viewItemMySubscriptionsUsage.getLineBelow().setVisibility(View.GONE);
                                }

                                viewItemMySubscriptionsUsage.getRenew_progress_layout().setVisibility(View.VISIBLE);
                                viewItemMySubscriptionsUsage.getRemaining_progress_layout().setVisibility(View.VISIBLE);


                                Tools.setOffersSubscriptionsIcons(context, offerUsage.getIconName(), viewItemMySubscriptionsUsage.getIc_usage_icon());

                                if (!Tools.hasValue(offerUsage.getTotalUsage()) || offerUsage.getTotalUsage().equalsIgnoreCase("0")) {
                                    viewItemMySubscriptionsUsage.getIc_usage_icon().setVisibility(View.INVISIBLE);
                                } else {
                                    viewItemMySubscriptionsUsage.getRemaining_progress_layout().setVisibility(View.VISIBLE);
                                }
                                if (Tools.hasValue(offerUsage.getRemainingTitle())) {
                                    viewItemMySubscriptionsUsage.getRemaining_title().setText(offerUsage.getRemainingTitle());
                                }
                                if (Tools.hasValue(offerUsage.getRenewalTitle())) {
                                    BakcellLogger.logE("reneneweltile1289", "title:::" + offerUsage.getRenewalTitle(), fromClass, "showingRenewelTitle");
                                    if (subscriptions.getHeader() != null) {
                                        if (subscriptions.getHeader().getBtnRenew() != null &&
                                                subscriptions.getHeader().getBtnRenew().equalsIgnoreCase("1")) {
                                            viewItemMySubscriptionsUsage.getRenew_title().setText(R.string.mysubs_to_renew_title);
                                        } else {
                                            viewItemMySubscriptionsUsage.getRenew_title().setText(R.string.subscription_to_expiration);
                                        }
                                    }
                                }

                                String remainTitleVal = offerUsage.getRemainingUsage() + " " + offerUsage.getUnit() + " / " + offerUsage.getTotalUsage();
                                viewItemMySubscriptionsUsage.getRemaining_time_title().setText(remainTitleVal);

                                if (Tools.hasValue(offerUsage.getRemainingUsage()) &&
                                        Tools.hasValue(offerUsage.getTotalUsage())) {

                                    if (offerUsage.getRemainingUsage().equalsIgnoreCase(IS_UNLIMITED) || offerUsage.getTotalUsage().equalsIgnoreCase(IS_UNLIMITED)) {

                                        viewItemMySubscriptionsUsage.getRemaining_time_title().setText(R.string.lbl_free);

                                        viewItemMySubscriptionsUsage.getRemaining_progress().setMax(100);
                                        viewItemMySubscriptionsUsage.getRemaining_progress().setProgress(100);

                                        viewItemMySubscriptionsUsage.getRemaining_progress().setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progress_bar_horizontal_green_bg));

                                    } else {
                                        double remainingUsageProgress = 0, totalUsageProgress = 0;
                                        if (Tools.isNumeric(offerUsage.getTotalUsage())) {
                                            totalUsageProgress = Tools.getDoubleFromString(offerUsage.getTotalUsage());
                                        }
                                        if (Tools.isNumeric(offerUsage.getRemainingUsage())) {
                                            remainingUsageProgress = Tools.getDoubleFromString(offerUsage.getRemainingUsage());
                                        }
                                        viewItemMySubscriptionsUsage.getRemaining_progress()
                                                .setMax((int) totalUsageProgress);
                                        viewItemMySubscriptionsUsage.getRemaining_progress()
                                                .setProgress((int) (totalUsageProgress - remainingUsageProgress));
                                    }
                                } else {
                                    viewItemMySubscriptionsUsage.getRemaining_progress_layout().setVisibility(View.GONE);
                                }

                                if (Tools.hasValue(offerUsage.getActivationDate()) && Tools.hasValue(offerUsage.getRenewalDate())) {

                                    long startMili = Tools.getMiliSecondsFromDate(offerUsage.getActivationDate());
                                    long endMili = Tools.getMiliSecondsFromDate(offerUsage.getRenewalDate());
                                    endMili = Tools.getOneDayMinus(endMili);
                                    long currentMili = Tools.getCurrentMiliSeconds();

                                    int daysDiffCurrent = Tools.getDaysDifferenceBetweenMiliseconds(startMili, currentMili);
                                    int daysDiffTotal = Tools.getDaysDifferenceBetweenMiliseconds(startMili, endMili);

                                    int days = daysDiffTotal - daysDiffCurrent;
                                    if (days <= -1) {
                                        days = 0;
                                    }

                                    if (days > 0) {
                                        viewItemMySubscriptionsUsage.getRenew_progress().setMax(daysDiffTotal);
                                        viewItemMySubscriptionsUsage.getRenew_progress().setProgress(daysDiffCurrent);
                                    } else {
                                        viewItemMySubscriptionsUsage.getRenew_progress().setMax(100);
                                        viewItemMySubscriptionsUsage.getRenew_progress().setProgress(100);
                                    }

                                    viewItemMySubscriptionsUsage.getRenew_time_title()
                                            .setText(context.getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, days, days));

                                    try {

                                        if (Tools.hasValue(offerUsage.getActivationDate())) {

                                            if (subscriptions.getHeader() != null) {
                                                if (subscriptions.getHeader().getBtnRenew() != null
                                                        && subscriptions.getHeader().getBtnRenew().equalsIgnoreCase("1")) {
                                                    viewItemMySubscriptionsUsage.getRenew_date().setText(context
                                                            .getString(R.string.subscription_renew, Tools
                                                                    .getDateAccordingToClientSaid(offerUsage.getRenewalDate())));
                                                } else {
                                                    viewItemMySubscriptionsUsage.getRenew_date().setText(context
                                                            .getString(R.string.subscription_validity, Tools
                                                                    .getDateAccordingToClientSaid(offerUsage.getRenewalDate())));
                                                }
                                            }
                                        }
                                        if (Tools.hasValue(offerUsage.getRenewalDate())) {
                                            viewItemMySubscriptionsUsage.getActivation_date().setText(context
                                                    .getString(R.string.subscription_activation, Tools
                                                            .getDateAccordingToClientSaid(offerUsage.getActivationDate())));
                                        }

                                    } catch (ParseException e) {
                                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                                    }
                                } else {

                                    viewItemMySubscriptionsUsage.getRenew_time_title()
                                            .setText(context.getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, 0, 0));

                                    viewItemMySubscriptionsUsage.getRenew_progress().setMax(100);
                                    viewItemMySubscriptionsUsage.getRenew_progress().setProgress(100);

                                    viewItemMySubscriptionsUsage.getRenew_date().setText(R.string.lbl_expired);

                                    // To hide of null
                                    if (offerUsage.getActivationDate() == null || offerUsage.getRenewalDate() == null) {
                                        viewItemMySubscriptionsUsage.getRenew_progress_layout().setVisibility(View.GONE);
                                    }
                                }


                                usage_content_layout.addView(viewItemMySubscriptionsUsage);
                            }
                        }
                    }
                }, 10);

            }
        }

        private void prepareOfferDetail(final Context context, final SubscriptionsDetailsMain offerDetail) {

            if (offerDetail == null) {
                detail_layout.setVisibility(View.GONE);
                return;
            }

            detail_layout.setVisibility(View.VISIBLE);

            // priceSection
            offerDetailContents.priceIcon.setText("");
            if (offerDetail.getPrice() != null && Tools.hasValue(offerDetail.getPrice().getValue())) {
                offerDetailContents.detailPriceValue.setText(offerDetail.getPrice().getValue());
                if (Tools.hasValue(offerDetail.getPrice().getOffersCurrency())) {
                    String offerCurreny = offerDetail.getPrice().getOffersCurrency();
                    if (offerCurreny.equalsIgnoreCase(OFFER_CURRENCY)) {
                        offerDetailContents.priceIcon.setText(Html.fromHtml("&nbsp;&#x20bc;"));
                    } else {
                        offerDetailContents.priceIcon.setText("");
                        offerDetailContents.detailPriceValue.append(" " + offerDetail.getPrice().getOffersCurrency());
                    }
                } else {
                    offerDetailContents.priceIcon.setText("");
                }
            } else {
                offerDetailContents.detailPriceValue.setText("");
                offerDetailContents.priceIcon.setText("");
            }
            if (offerDetail.getPrice() != null) {
                offerDetailContents.priceSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getPrice().getTitle())) {
                    offerDetailContents.detailPriceTitle.setText(offerDetail.getPrice().getTitle());
                    offerDetailContents.detailPriceTitle.setSelected(true);
                } else {
                    offerDetailContents.detailPriceTitle.setText("");

                }
                if (Tools.hasValue(offerDetail.getPrice().getDescription())) {
                    offerDetailContents.detailPriceDescription.setText(Html.fromHtml(offerDetail.getPrice().getDescription()));
                } else {
                    offerDetailContents.detailPriceDescription.setVisibility(View.GONE);
                }
                // Price Attributes
                if (offerDetail.getPrice().getAttributeList() != null && offerDetail.getPrice().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.offerDetailAttributeLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.offerDetailAttributeLayout.removeAllViews();
                            ArrayList<DetailsAttributes> detailPriceAttributesList = offerDetail.getPrice().getAttributeList();
                            for (int i = 0; i < detailPriceAttributesList.size(); i++) {
                                ViewItemOfferDetailPriceAttribute detailPriceAttribute = new ViewItemOfferDetailPriceAttribute(context);
                                if (Tools.hasValue(detailPriceAttributesList.get(i).getTitle())) {
                                    detailPriceAttribute.getPriceSubTitle().setText(detailPriceAttributesList.get(i).getTitle());
                                } else {
                                    detailPriceAttribute.getPriceSubTitle().setText("");
                                }
                                if (Tools.hasValue(detailPriceAttributesList.get(i).getValue())) {
                                    detailPriceAttribute.getPrceValue().setText(detailPriceAttributesList.get(i).getValue());
                                    if (Tools.hasValue(detailPriceAttributesList.get(i).getUnit())) {
                                        String unit = detailPriceAttributesList.get(i).getUnit();
                                        if (unit.equalsIgnoreCase(OFFER_CURRENCY)) {
                                            detailPriceAttribute.getPriceIcon().setText(Html.fromHtml("&nbsp;&#x20bc;"));
                                        } else {
                                            detailPriceAttribute.getPriceIcon().setText("");
                                            detailPriceAttribute.getPrceValue().append(" " + unit);
                                        }
                                    } else {
                                        detailPriceAttribute.getPriceIcon().setText("");
                                    }
                                } else {
                                    detailPriceAttribute.getPriceIcon().setText("");
                                    detailPriceAttribute.getPrceValue().setText("");
                                }
                                offerDetailContents.offerDetailAttributeLayout.addView(detailPriceAttribute);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.offerDetailAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.priceSection.setVisibility(View.GONE);
            }

            // roundingSection
            if (offerDetail.getRounding() != null) {
                offerDetailContents.roundingSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getRounding().getTitle())) {
                    offerDetailContents.roundingTitleText.setText(offerDetail.getRounding().getTitle());
                    offerDetailContents.roundingTitleText.setSelected(true);
                } else {
                    offerDetailContents.roundingTitleText.setText("");
                }
                if (Tools.hasValue(offerDetail.getRounding().getValue())) {
                    offerDetailContents.roundingTitleValue.setText(offerDetail.getRounding().getValue());
                } else {
                    offerDetailContents.roundingTitleValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getRounding().getDescription())) {
                    offerDetailContents.detailRoundingDescription.setText(Html.fromHtml(offerDetail.getRounding().getDescription()));
                } else {
                    offerDetailContents.detailRoundingDescription.setText("");
                }

                // Rounding Attributes
                if (offerDetail.getRounding().getAttributeList() != null && offerDetail.getRounding().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.offerDetailroundingAttributeLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.offerDetailroundingAttributeLayout.removeAllViews();
                            ArrayList<DetailsAttributes> roundingAttributeslist = offerDetail.getRounding().getAttributeList();
                            for (int i = 0; i < roundingAttributeslist.size(); i++) {
                                ViewItemOfferDetailRoundingAttribute detailRoundingAttribute = new ViewItemOfferDetailRoundingAttribute(context);
                                detailRoundingAttribute.hideAZN();
                                if (Tools.hasValue(roundingAttributeslist.get(i).getTitle())) {
                                    detailRoundingAttribute.getPriceSubTitle().setText(roundingAttributeslist.get(i).getTitle());
                                } else {
                                    detailRoundingAttribute.getPriceSubTitle().setText("");
                                }
                                if (Tools.hasValue(roundingAttributeslist.get(i).getValue())) {
                                    detailRoundingAttribute.getPrceValue().setText(roundingAttributeslist.get(i).getValue());
                                    if (Tools.hasValue(roundingAttributeslist.get(i).getUnit())) {
                                        String unit = roundingAttributeslist.get(i).getUnit();
                                        if (unit.equalsIgnoreCase(OFFER_CURRENCY)) {
                                            detailRoundingAttribute.getPriceIcon().setText(Html.fromHtml("&nbsp;&#x20bc;"));
                                        } else {
                                            detailRoundingAttribute.getPriceIcon().setText("");
                                            detailRoundingAttribute.getPrceValue().append(" " + unit);
                                        }
                                    } else {
                                        detailRoundingAttribute.getPriceIcon().setText("");
                                    }
                                } else {
                                    detailRoundingAttribute.getPriceIcon().setText("");
                                    detailRoundingAttribute.getPrceValue().setText("");
                                }
                                offerDetailContents.offerDetailroundingAttributeLayout.addView(detailRoundingAttribute);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.offerDetailroundingAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.roundingSection.setVisibility(View.GONE);
            }

            // TextWithTitleSection
            if (offerDetail.getTextWithTitle() != null) {
                offerDetailContents.textWithTitleSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTextWithTitle().getTitle())) {
                    offerDetailContents.text_title.setVisibility(View.VISIBLE);
                    offerDetailContents.text_title.setText(offerDetail.getTextWithTitle().getTitle());
                } else {
                    offerDetailContents.text_title.setVisibility(View.GONE);
                }
                if (Tools.hasValue(offerDetail.getTextWithTitle().getText())) {
                    offerDetailContents.decription_text.setVisibility(View.VISIBLE);
                    offerDetailContents.decription_text.setText(Html.fromHtml(offerDetail.getTextWithTitle().getText()));
                } else {
                    offerDetailContents.decription_text.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.textWithTitleSection.setVisibility(View.GONE);
            }

            //textWithOutTitleSection
            if (offerDetail.getTextWithOutTitle() != null) {
                offerDetailContents.textWithOutTitleSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTextWithOutTitle().getDescription())) {
                    offerDetailContents.textWithOutTitle_text.setText(Html.fromHtml(offerDetail.getTextWithOutTitle().getDescription()));
                } else {
                    offerDetailContents.textWithOutTitle_text.setText("");
                }
            } else {
                offerDetailContents.textWithOutTitleSection.setVisibility(View.GONE);
            }

            // textWithPointsSections
            if (offerDetail.getTextWithPoints() != null && offerDetail.getTextWithPoints().getPointsList() != null && offerDetail.getTextWithPoints().getPointsList().size() > 0) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        offerDetailContents.textWithPointsSections.setVisibility(View.VISIBLE);
                        offerDetailContents.textWithPointsLayout.removeAllViews();
                        ArrayList<String> pointsList = offerDetail.getTextWithPoints().getPointsList();
                        for (int i = 0; i < pointsList.size(); i++) {
                            ViewItemOfferDetailBulletsItem detailBulletsItem = new ViewItemOfferDetailBulletsItem(context);
                            if (Tools.hasValue(pointsList.get(i))) {
                                detailBulletsItem.getBulletText().setText(Html.fromHtml(pointsList.get(i)));
                                offerDetailContents.textWithPointsLayout.addView(detailBulletsItem);
                            } else {
                                detailBulletsItem.getBulletText().setText("");
                            }
                        }
                    }
                }, 10);
            } else {
                offerDetailContents.textWithPointsSections.setVisibility(View.GONE);
            }


            //titleSubTitleValueDescSection
            if (offerDetail.getTitleSubTitleValueAndDesc() != null) {
                offerDetailContents.titleSubTitleValueDescSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTitleSubTitleValueAndDesc().getTitle())) {
                    offerDetailContents.titleSubTitleValueDescTitle.setVisibility(View.VISIBLE);
                    offerDetailContents.titleSubTitleValueDescTitle.setText(offerDetail.getTitleSubTitleValueAndDesc().getTitle());
                } else {
                    offerDetailContents.titleSubTitleValueDescTitle.setVisibility(View.GONE);
                }
                // Sub Title Attributes
                if (offerDetail.getTitleSubTitleValueAndDesc().getAttributeList() != null
                        && offerDetail.getTitleSubTitleValueAndDesc().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.titleSubTitleValueDescAttributeLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.titleSubTitleValueDescAttributeLayout.removeAllViews();
                            ArrayList<HeaderAttributes> titleSubTitleList = offerDetail.getTitleSubTitleValueAndDesc().getAttributeList();
                            for (int i = 0; i < titleSubTitleList.size(); i++) {
                                ViewItemOfferDetailTitleWithSubTitleDescAttribute subTitleDescAttribute = new ViewItemOfferDetailTitleWithSubTitleDescAttribute(context);
                                if (Tools.hasValue(titleSubTitleList.get(i).getTitle())) {
                                    subTitleDescAttribute.getSubTitle().setText(titleSubTitleList.get(i).getTitle());
                                } else {
                                    subTitleDescAttribute.getSubTitle().setText("");
                                }
                                if (Tools.hasValue(titleSubTitleList.get(i).getValue())) {
                                    subTitleDescAttribute.getValue().setText(titleSubTitleList.get(i).getValue());
                                } else {
                                    subTitleDescAttribute.getValue().setText("");
                                }
                                if (Tools.hasValue(titleSubTitleList.get(i).getDescription())) {
                                    subTitleDescAttribute.getDesc_txt().setVisibility(View.VISIBLE);
                                    subTitleDescAttribute.getDesc_txt().setText(Html.fromHtml(titleSubTitleList.get(i).getDescription()));
                                } else {
                                    subTitleDescAttribute.getDesc_txt().setVisibility(View.GONE);
                                }
                                offerDetailContents.titleSubTitleValueDescAttributeLayout.addView(subTitleDescAttribute);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.titleSubTitleValueDescAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.titleSubTitleValueDescSection.setVisibility(View.GONE);
            }

            //dateSection
            if (offerDetail.getDate() != null) {
                offerDetailContents.dateSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getDate().getDescription())) {
                    offerDetailContents.dateDescription.setText(Html.fromHtml(offerDetail.getDate().getDescription()));
                } else {
                    offerDetailContents.dateDescription.setText("");
                }
                if (Tools.hasValue(offerDetail.getDate().getFromTitle())) {
                    offerDetailContents.dateFromLabel.setText(offerDetail.getDate().getFromTitle());
                } else {
                    offerDetailContents.dateFromLabel.setText("");
                }
                if (Tools.hasValue(offerDetail.getDate().getFromValue())) {
                    try {
                        offerDetailContents.dateFromValue.setText(
                                Tools.getDateAccordingToClientSaid(
                                        offerDetail.getDate().getFromValue()));
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    offerDetailContents.dateFromValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getDate().getToTitle())) {

                    try {
                        offerDetailContents.dateToLabel.setText(
                                Tools.getDateAccordingToClientSaid(offerDetail.getDate().getToTitle()));
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    offerDetailContents.dateToLabel.setText("");
                }
                if (Tools.hasValue(offerDetail.getDate().getToValue())) {
                    try {
                        offerDetailContents.dateToValue.setText(
                                Tools.getDateAccordingToClientSaid(offerDetail.getDate().getToValue()));
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    offerDetailContents.dateToValue.setText("");
                }
            } else {
                offerDetailContents.dateSection.setVisibility(View.GONE);
            }

            //timeSection
            if (offerDetail.getTime() != null) {
                offerDetailContents.timeSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTime().getDescription())) {
                    offerDetailContents.timeDescription.setText(Html.fromHtml(offerDetail.getTime().getDescription()));
                } else {
                    offerDetailContents.timeDescription.setText("");
                }
                if (Tools.hasValue(offerDetail.getTime().getFromTitle())) {
                    offerDetailContents.timefromLabel.setText(offerDetail.getTime().getFromTitle());
                } else {
                    offerDetailContents.timefromLabel.setText("");
                }
                if (Tools.hasValue(offerDetail.getTime().getFromValue())) {
                    offerDetailContents.timefromValue.setText(offerDetail.getTime().getFromValue());
                } else {
                    offerDetailContents.timefromValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getTime().getToTitle())) {
                    offerDetailContents.timetoLabel.setText(offerDetail.getTime().getToTitle());
                } else {
                    offerDetailContents.timetoLabel.setText("");
                }
                if (Tools.hasValue(offerDetail.getTime().getToValue())) {
                    offerDetailContents.timetoValue.setText(offerDetail.getTime().getToValue());
                } else {
                    offerDetailContents.timetoValue.setText("");
                }
            } else {
                offerDetailContents.timeSection.setVisibility(View.GONE);
            }


            //roamingSection
            if (offerDetail.getRoamingDetails() != null) {
                offerDetailContents.roamingSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getRoamingDetails().getDescriptionAbove())) {
                    offerDetailContents.roamingDescriptionAbove.setVisibility(View.VISIBLE);
                    offerDetailContents.roamingDescriptionAbove.setText(Html.fromHtml(offerDetail.getRoamingDetails().getDescriptionAbove()));
                } else {
                    offerDetailContents.roamingDescriptionAbove.setVisibility(View.GONE);
                }
                if (Tools.hasValue(offerDetail.getRoamingDetails().getDescriptionBelow())) {
                    offerDetailContents.roamingDescriptionBelow.setVisibility(View.VISIBLE);
                    offerDetailContents.roamingDescriptionBelow.setText(Html.fromHtml(offerDetail.getRoamingDetails().getDescriptionBelow()));
                } else {
                    offerDetailContents.roamingDescriptionBelow.setVisibility(View.GONE);
                }

                // RoamingsOffer Coutries list
                if (offerDetail.getRoamingDetails().getRoamingDetailsCountriesList() != null && offerDetail.getRoamingDetails().getRoamingDetailsCountriesList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.roamingCountryLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.roamingCountryLayout.removeAllViews();
                            ArrayList<RoamingDetailsCountries> roamingCountriesList = offerDetail.getRoamingDetails().getRoamingDetailsCountriesList();
                            for (int i = 0; i < roamingCountriesList.size(); i++) {
                                ViewItemOfferDetailRoamingCountries detailRoamingCountries = new ViewItemOfferDetailRoamingCountries(context);
                                if (Tools.hasValue(roamingCountriesList.get(i).getCountryName())) {
                                    detailRoamingCountries.getCountryName().setText(roamingCountriesList.get(i).getCountryName());
                                } else {
                                    detailRoamingCountries.getCountryName().setText("");
                                }
                                if (Tools.hasValue(roamingCountriesList.get(i).getFlag())) {
                                    detailRoamingCountries.getCountryFlag().setImageDrawable(Tools.getImageFromAssests(context, roamingCountriesList.get(i).getFlag()));
                                } else {
                                    detailRoamingCountries.getCountryFlag().setImageResource(R.drawable.dummy_flag);
                                }
                                // Country Operators list
                                if (roamingCountriesList.get(i).getOperatorList() != null && roamingCountriesList.get(i).getOperatorList().size() > 0) {
                                    detailRoamingCountries.getOperatorLayout().setVisibility(View.VISIBLE);
                                    detailRoamingCountries.getOperatorLayout().removeAllViews();
                                    ArrayList<String> countryOperatorsList = roamingCountriesList.get(i).getOperatorList();
                                    for (int j = 0; j < countryOperatorsList.size(); j++) {
                                        if (Tools.hasValue(countryOperatorsList.get(j))) {
                                            BakcellTextViewNormal textViewOperator = new BakcellTextViewNormal(context);
                                            textViewOperator.setText(countryOperatorsList.get(j));
                                            textViewOperator.setGravity(Gravity.RIGHT);
                                            detailRoamingCountries.getOperatorLayout().addView(textViewOperator);
                                        }
                                    }
                                } else {
                                    detailRoamingCountries.getOperatorLayout().setVisibility(View.GONE);
                                }
                                offerDetailContents.roamingCountryLayout.addView(detailRoamingCountries);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.roamingCountryLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.roamingSection.setVisibility(View.GONE);
            }

            // freeResourceValiditySection
            if (offerDetail.getFreeResourceValidity() != null) {
                offerDetailContents.freeResourceValiditySection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getTitle())) {
                    offerDetailContents.freeResourceTitle.setText(offerDetail.getFreeResourceValidity().getTitle());
                } else {
                    offerDetailContents.freeResourceTitle.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getTitleValue())) {
                    offerDetailContents.freeResourceValue.setText(offerDetail.getFreeResourceValidity().getTitleValue());
                } else {
                    offerDetailContents.freeResourceValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getSubTitle())) {
                    offerDetailContents.onnetTitle.setText(offerDetail.getFreeResourceValidity().getSubTitle());
                } else {
                    offerDetailContents.onnetTitle.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getSubTitleValue())) {
                    offerDetailContents.onnetValue.setText(offerDetail.getFreeResourceValidity().getSubTitleValue());
                } else {
                    offerDetailContents.onnetValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getDescription())) {
                    offerDetailContents.freeResourceDescription.setVisibility(View.VISIBLE);
                    offerDetailContents.freeResourceDescription.setText(Html.fromHtml(offerDetail.getFreeResourceValidity().getDescription()));
                } else {
                    offerDetailContents.freeResourceDescription.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.freeResourceValiditySection.setVisibility(View.GONE);
            }


        }


        private void showOfferType(Subscriptions subscriptions) {
            if (Tools.hasValue(subscriptions.getHeader().getType())) {
                offer_type_layout.setVisibility(View.VISIBLE);
                line_below_type.setVisibility(View.VISIBLE);
                tv_offer_type_name.setText(subscriptions.getHeader().getType());

                if (subscriptions != null && subscriptions.getHeader() != null) {
                    CardAdapterSupplementaryOffers.offerTypeInRoamingIconsMapping(context,
                            subscriptions.getHeader().getType(), ic_call_type, ic_internet_type, ic_campaign_type, tv_offer_type_name);
                }

            } else {
                offer_type_layout.setVisibility(View.GONE);
                line_below_type.setVisibility(View.GONE);
            }
        }


        private void showOfferGroup(Subscriptions subscriptions) {
            if (subscriptions.getHeader().getOfferGroup() != null) {
                offer_group_layout.setVisibility(View.VISIBLE);
                lineBelowGroup.setVisibility(View.VISIBLE);
                if (Tools.hasValue(subscriptions.getHeader().getOfferGroup().getGroupName())) {
                    tv_offer_group_name.setText(subscriptions.getHeader().getOfferGroup().getGroupName());
                    tv_offer_group_name.setSelected(true);
                } else {
                    tv_offer_group_name.setText("");
                }
                //Offer group mapping
                if (subscriptions != null && subscriptions.getHeader() != null && subscriptions.getHeader().getOfferGroup() != null) {
                    CardAdapterSupplementaryOffers.offerGroupInInternetAndRoamingIconsMapping(context, subscriptions.getHeader().getOfferGroup().getGroupValue(), icOfferGroupPhone, icOfferGroupTab, icOfferGroupDesktop);
                }
            } else {
                offer_group_layout.setVisibility(View.GONE);
                lineBelowGroup.setVisibility(View.GONE);
            }
        }


    }


}
