package com.bakcell.adapters;

import static com.bakcell.utilities.BakcellLogger.logE;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.LeadingMarginSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.ChangeTariffListener;
import com.bakcell.interfaces.UpdateListOnSuccessfullySurvey;
import com.bakcell.models.inappsurvey.Answers;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Questions;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.inappsurvey.UserSurveys;
import com.bakcell.models.tariffs.TariffCinNew;
import com.bakcell.models.tariffs.helper.TariffsHelperModel;
import com.bakcell.models.tariffs.tariffcinobject.TariffCin;
import com.bakcell.models.tariffs.tariffcinobject.description.Description;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.Attributes;
import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePrice;
import com.bakcell.models.tariffs.tariffklassobject.paygprice.PaygPrice;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.TariffDetailLayout;
import com.bakcell.viewsitem.ViewItemTariffsAttribute;
import com.bakcell.viewsitem.ViewItemTariffsKlassAttribute;
import com.bakcell.webservices.changetariff.ChangeTariff;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Junaid Hassan on 30, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class AdapterTariffsCINNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //vars
    private Activity activity;
    private Context context;
    private final String fromClass = "AdapterTariffsCINNew";
    private ArrayList<TariffsHelperModel> tariffsHelperModels;
    private String CIN = "cin";
    private final String KLASS = "klass";
    private boolean isCINPricesLayoutExpanded = false;
    private boolean isCIN_NewBonusesLayoutExpanded = false;
    private boolean isCinDetailsLayoutExpanded = false;
    private boolean isCIN_NewPaidLayoutExpanded = false;
    private boolean isCin_NewUnpaidLayoutExpanded = false;
    private boolean isCinDescriptionLayoutExpanded = false;
    private boolean isCin_NewDetailsLayoutExpanded = false;


    //constructor
    public AdapterTariffsCINNew(Activity activity, Context context, ArrayList<TariffsHelperModel> tariffsHelperModels) {
        this.activity = activity;
        this.context = context;
        this.tariffsHelperModels = tariffsHelperModels;
    }//constructor ends

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        if (viewType == Constants.TariffsConstants.LAYOUT_CIN) {
            /**CIN TYPE*/
            layout = R.layout.adapter_item_tariffs_cin;
            View CINview = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
            return new CINViewHolder(CINview);
        } else if (viewType == Constants.TariffsConstants.LAYOUT_CIN_NEW) {
            /**CIN_NEW TYPE*/
            layout = R.layout.adapter_item_traiffs_klass;
            View CIN_new_view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
            return new CIN_NewViewHolder(CIN_new_view);
        } else {
            /**ELSE CIN TYPE*/
            layout = R.layout.adapter_item_tariffs_cin;
            View CINview = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
            return new CINViewHolder(CINview);
        }
    }//onCreateViewHolder ends

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CIN_NewViewHolder cin_newViewHolder = null;
        CINViewHolder cinViewHolder = null;
        int rateStars = 0;
        /**check the holder item view type*/
        if (holder.getItemViewType() == Constants.TariffsConstants.LAYOUT_CIN) {
            logE("viewITX", "viewItemType:::CIN", fromClass, "onBindViewHolder");
            /**process CIN Tariff*/
            cinViewHolder = (CINViewHolder) holder;

            /**populate the ui*/
            if (tariffsHelperModels.get(position).getAnyTariffObject() instanceof TariffCin) {
                cinViewHolder.prepareListItem(context, position, ((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()), cinViewHolder, cin_newViewHolder);

                if (((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader() != null) {
                    cinViewHolder.prepareHeaderItem(((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader(), cinViewHolder);
                } else {
                    cinViewHolder.prepareHeaderItem(null, cinViewHolder);
                }

                if (((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader() != null) {
                    cinViewHolder.prepareDetailItem(((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getDetails(), cinViewHolder);
                } else {
                    cinViewHolder.prepareDetailItem(null, cinViewHolder);
                }

                if (((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getDescription() != null) {
                    cinViewHolder.prepareDescriptionItem(((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getDescription(), cinViewHolder);
                } else {
                    cinViewHolder.prepareDescriptionItem(null, cinViewHolder);
                }


                final int pos1 = position;
                cinViewHolder.button_subscribe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (context != null &&
                                tariffsHelperModels != null &&
                                tariffsHelperModels.get(pos1) != null &&
                                tariffsHelperModels.get(pos1).getAnyTariffObject() != null) {
                            if (tariffsHelperModels.get(pos1).getAnyTariffObject() instanceof TariffCin) {
                                if (((TariffCin) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader() != null &&
                                        Tools.hasValue(((TariffCin) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId())) {
                                    processOnSubscribeButtonForCin(context,
                                            ((TariffCin) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId(),
                                            ((TariffCin) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getName(),
                                            ((TariffCin) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getSubscribable());
                                }//offeringId not null
                            }//tariffCin Object
                        }//outer most if ends
                    }
                });
                Data data = PrefUtils.getInAppSurvey(context).getData();
                if (data != null && data.getUserSurveys() != null) {
                    List<UserSurveys> userSurveys = data.getUserSurveys();
                    for (int i = 0; i < userSurveys.size(); i++) {
                        if (userSurveys.get(i).getOfferingId().equals(((TariffCin) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId())) {
                            if (userSurveys.get(i).getAnswer() != null) {
                                String surveyId = userSurveys.get(i).getSurveyId();
                                if (surveyId != null) {
                                    Surveys survey = data.findSurveyById(Integer.parseInt(surveyId));
                                    if (Tools.isNumeric(userSurveys.get(i).getAnswerId())) {
                                        if (Tools.isNumeric(userSurveys.get(i).getQuestionId())) {
                                            rateStars = setStarsCinViewHolder(Integer.parseInt(userSurveys.get(i).getAnswerId()),
                                                    Integer.parseInt(userSurveys.get(i).getQuestionId()), survey, cinViewHolder);
                                        }
                                    }
                                }
                            } else {
                                rateStars = setStarsCinViewHolder(0, 0, null, cinViewHolder);
                            }
                        }
                    }
                }
                // Subscribe Button process
                int finalRateStars = rateStars;
                cinViewHolder.ratingStarsLayout.setOnClickListener(
                        v -> {
                            String tariffId = ((TariffCin) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId();
                            inAppFeedback(context, tariffId, finalRateStars, position);
                        }
                );
            }//if (tariffsHelperModels.get(position).getAnyTariffObject() instanceof TariffCin) ends
        } else if (holder.getItemViewType() == Constants.TariffsConstants.LAYOUT_CIN_NEW) {
            logE("viewITX", "viewItemType:::CIN_NEW", fromClass, "onBindViewHolder");
            /**process CIN_NEW Tariff*/
            cin_newViewHolder = (CIN_NewViewHolder) holder;
            String answer = "";

            /**populate the ui for CIN_NEW Tariff*/
            if (tariffsHelperModels.get(position).getAnyTariffObject() instanceof TariffCinNew) {
                cin_newViewHolder.prepareListItem(context, position, cinViewHolder, cin_newViewHolder);
                if (((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader() != null) {
                    cin_newViewHolder.prepareHeaderItem(((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader(), cin_newViewHolder);
                } else {
                    cin_newViewHolder.prepareHeaderItem(null, cin_newViewHolder);
                }
                if (((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getPackagePrice() != null) {
                    cin_newViewHolder.preparePackagePriceItem(((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getPackagePrice(), cin_newViewHolder);
                } else {
                    cin_newViewHolder.preparePackagePriceItem(null, cin_newViewHolder);
                }
                if (((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getPaygPrice() != null) {
                    cin_newViewHolder.preparePayGPriceItem(((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getPaygPrice(), cin_newViewHolder);
                } else {
                    cin_newViewHolder.preparePayGPriceItem(null, cin_newViewHolder);
                }
                if (((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getDetails() != null) {
                    cin_newViewHolder.prepareDetailItem(((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getDetails(), cin_newViewHolder);
                } else {
                    cin_newViewHolder.prepareDetailItem(null, cin_newViewHolder);
                }

                final int pos1 = position;

                // Subscribe Button process
                cin_newViewHolder.button_subscribe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (context != null &&
                                tariffsHelperModels != null &&
                                tariffsHelperModels.get(pos1) != null &&
                                ((TariffCinNew) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader() != null &&
                                Tools.hasValue(((TariffCinNew) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId())) {
                            processOnSubscribeButtonForCinNew(context,
                                    ((TariffCinNew) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId(),
                                    ((TariffCinNew) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getName(),
                                    ((TariffCinNew) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getSubscribable());
                        }//offeringId not null ends
                    }
                });
                Data data = PrefUtils.getInAppSurvey(context).getData();
                if (data != null && data.getUserSurveys() != null) {
                    List<UserSurveys> userSurveys = data.getUserSurveys();
                    for (int i = 0; i < userSurveys.size(); i++) {
                        if (userSurveys.get(i).getOfferingId().equals(((TariffCinNew) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId())) {
                            if (userSurveys.get(i).getAnswer() != null) {
                                String surveyId = userSurveys.get(i).getSurveyId();
                                if (surveyId != null) {
                                    Surveys survey = data.findSurveyById(Integer.parseInt(surveyId));
                                    if (Tools.isNumeric(userSurveys.get(i).getAnswerId())) {
                                        if (Tools.isNumeric(userSurveys.get(i).getQuestionId())) {
                                            rateStars = setStarsCinNewViewHolder(Integer.parseInt(userSurveys.get(i).getAnswerId()),
                                                    Integer.parseInt(userSurveys.get(i).getQuestionId()), survey, cin_newViewHolder);
                                        }
                                    }
                                }
                            } else {
                                rateStars = setStarsCinNewViewHolder(0, 0, null, cin_newViewHolder);
                            }
                        }
                    }
                }
                int finalRateStars = rateStars;
                cin_newViewHolder.ratingStarsLayout.setOnClickListener(
                        v -> {
                            String tariffId = ((TariffCinNew) tariffsHelperModels.get(pos1).getAnyTariffObject()).getHeader().getOfferingId();
                            inAppFeedback(context, tariffId, finalRateStars, position);
                        }
                );
            }


        }//else if (holder.getItemViewType() == Constants.TariffsConstants.LAYOUT_CIN_NEW) ends

        initExpandableLayoutsBehavior(position, cinViewHolder, cin_newViewHolder);
    }//onBindViewHolder ends

    private void inAppFeedback(Context context, String tariffId, int rate, int position) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TARIFF_PAGE);
            if (surveys != null) {
                InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                if (surveys.getQuestions() != null) {
                    currentVisit = 0;
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(new UpdateListOnSuccessfullySurvey() {
                        @Override
                        public void updateListOnSuccessfullySurvey() {
                            notifyItemChanged(position);
                        }
                    });
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "1", tariffId, rate);
                }
            }
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE, currentVisit);
    }

    private int setStarsCinNewViewHolder(int answerId, int questionId, Surveys survey, CIN_NewViewHolder viewHolder) {
        if (survey != null) {
            Questions question = survey.findQuestion(questionId);
            if (question != null) {
                List<Answers> answers = question.getAnswers();
                if (answers != null) {
                    if (answers.size() > 0 && answers.get(0).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(false);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 1;
                    } else if (answers.size() > 1 && answers.get(1).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 2;
                    } else if (answers.size() > 2 && answers.get(2).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 3;
                    } else if (answers.size() > 3 && answers.get(3).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(false);
                        return 4;
                    } else if (answers.size() > 4 && answers.get(4).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(true);
                        return 5;
                    }

                } else {
                    viewHolder.starOne.setChecked(false);
                    viewHolder.starTwo.setChecked(false);
                    viewHolder.starThree.setChecked(false);
                    viewHolder.starFour.setChecked(false);
                    viewHolder.starFive.setChecked(false);
                    return 0;
                }
            } else {
                viewHolder.starOne.setChecked(false);
                viewHolder.starTwo.setChecked(false);
                viewHolder.starThree.setChecked(false);
                viewHolder.starFour.setChecked(false);
                viewHolder.starFive.setChecked(false);
                return 0;
            }
        } else {
            viewHolder.starOne.setChecked(false);
            viewHolder.starTwo.setChecked(false);
            viewHolder.starThree.setChecked(false);
            viewHolder.starFour.setChecked(false);
            viewHolder.starFive.setChecked(false);
            return 0;
        }
        return 0;
    }

    private int setStarsCinViewHolder(int answerId, int questionId, Surveys survey, CINViewHolder viewHolder) {
        if (survey != null) {
            Questions question = survey.findQuestion(questionId);
            if (question != null) {
                List<Answers> answers = question.getAnswers();
                if (answers != null) {
                    if (answers.size() > 0 && answers.get(0).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(false);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 1;
                    } else if (answers.size() > 1 && answers.get(1).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 2;
                    } else if (answers.size() > 2 && answers.get(2).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 3;
                    } else if (answers.size() > 3 && answers.get(3).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(false);
                        return 4;
                    } else if (answers.size() > 4 && answers.get(4).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(true);
                        return 5;
                    }

                } else {
                    viewHolder.starOne.setChecked(false);
                    viewHolder.starTwo.setChecked(false);
                    viewHolder.starThree.setChecked(false);
                    viewHolder.starFour.setChecked(false);
                    viewHolder.starFive.setChecked(false);
                    return 0;
                }
            } else {
                viewHolder.starOne.setChecked(false);
                viewHolder.starTwo.setChecked(false);
                viewHolder.starThree.setChecked(false);
                viewHolder.starFour.setChecked(false);
                viewHolder.starFive.setChecked(false);
                return 0;
            }
        } else {
            viewHolder.starOne.setChecked(false);
            viewHolder.starTwo.setChecked(false);
            viewHolder.starThree.setChecked(false);
            viewHolder.starFour.setChecked(false);
            viewHolder.starFive.setChecked(false);
            return 0;
        }
        return 0;
    }

    private void initExpandableLayoutsBehavior(int position, CINViewHolder cinViewHolder, CIN_NewViewHolder cin_newViewHolder) {
        if (isCINPricesLayoutExpanded || isCinDetailsLayoutExpanded || isCinDescriptionLayoutExpanded ||
                isCIN_NewBonusesLayoutExpanded || isCin_NewDetailsLayoutExpanded || isCIN_NewPaidLayoutExpanded || isCin_NewUnpaidLayoutExpanded) {
            logE("tarX123", "anyone true", fromClass, "expandedPosition");

            if (isCinDescriptionLayoutExpanded || isCin_NewDetailsLayoutExpanded) {
                expandCINDescriptionLayout(cinViewHolder);
                expandCIN_NewDetailsLayout(cin_newViewHolder);

                //all other layouts should be collapsed
                collapseCINPricesLayout(cinViewHolder);
                collapseCIN_NewBonusesLayout(cin_newViewHolder);
                collapseCINDetailsLayout(cinViewHolder);
                collapseCIN_NewPaidLayout(cin_newViewHolder);
                collapseCIN_NewUnPaidLayout(cin_newViewHolder);
            } else if (isCINPricesLayoutExpanded || isCIN_NewBonusesLayoutExpanded) {
                collapseCINDescriptionLayout(cinViewHolder);
                collapseCIN_NewDetailsLayout(cin_newViewHolder);

                //expand the cin prices and cin new bonuses layout
                expandCINPricesLayout(cinViewHolder);
                expandCIN_NewBonusesLayout(cin_newViewHolder);

                //collapse the other layouts
                collapseCINDetailsLayout(cinViewHolder);
                collapseCIN_NewPaidLayout(cin_newViewHolder);
                collapseCIN_NewUnPaidLayout(cin_newViewHolder);
            } else if (isCinDetailsLayoutExpanded || isCIN_NewPaidLayoutExpanded) {
                if (!isCin_NewUnpaidLayoutExpanded) {
                    expandCIN_NewPaidLayout(cin_newViewHolder);
                    expandCINDetailsLayout(cinViewHolder);

                    //all other layouts should be collapsed
                    collapseCINPricesLayout(cinViewHolder);
                    collapseCIN_NewBonusesLayout(cin_newViewHolder);
                    collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                    collapseCIN_NewDetailsLayout(cin_newViewHolder);
                    collapseCINDescriptionLayout(cinViewHolder);
                } else {
                    expandCIN_NewUnPaidLayout(cin_newViewHolder);
                    expandCINDetailsLayout(cinViewHolder);

                    //all other layouts should be collapsed
                    collapseCINPricesLayout(cinViewHolder);
                    collapseCIN_NewBonusesLayout(cin_newViewHolder);
                    collapseCIN_NewPaidLayout(cin_newViewHolder);
                    collapseCIN_NewDetailsLayout(cin_newViewHolder);
                    collapseCINDescriptionLayout(cinViewHolder);
                }
            } else if (isCin_NewUnpaidLayoutExpanded) {
                expandCIN_NewUnPaidLayout(cin_newViewHolder);
                expandCINDetailsLayout(cinViewHolder);

                //all other layouts should be collapsed
                collapseCINPricesLayout(cinViewHolder);
                collapseCIN_NewBonusesLayout(cin_newViewHolder);
                collapseCIN_NewPaidLayout(cin_newViewHolder);
                collapseCIN_NewDetailsLayout(cin_newViewHolder);
                collapseCINDescriptionLayout(cinViewHolder);
            }
        } else {
            logE("tarX123", "allllll false", fromClass, "expandedPosition NOT");
            expandCINPricesLayout(cinViewHolder);
            expandCIN_NewBonusesLayout(cin_newViewHolder);
        }
    }//initExpandableLayoutsBehavior ends

    private void collapseCINDescriptionLayout(CINViewHolder cinViewHolder) {
        if (cinViewHolder != null && cinViewHolder.expandable_description_layout != null && cinViewHolder.description_expand_icon != null) {
            cinViewHolder.expandable_description_layout.setExpanded(false);
            cinViewHolder.description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
        }
    }//collapseCINDescriptionLayout ends

    private void collapseCIN_NewDetailsLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_detail_layout != null && cin_newViewHolder.detail_expand_icon != null) {
            cin_newViewHolder.expandable_detail_layout.setExpanded(false);
            cin_newViewHolder.detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
        }
    }//collapseCIN_NewDetailsLayout ends

    private void expandCIN_NewDetailsLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_detail_layout != null && cin_newViewHolder.detail_expand_icon != null) {
            cin_newViewHolder.expandable_detail_layout.setExpanded(true);
            cin_newViewHolder.detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
        }
    }//expandCIN_NewDetailsLayout ends

    private void expandCINDescriptionLayout(CINViewHolder cinViewHolder) {
        if (cinViewHolder != null && cinViewHolder.expandable_description_layout != null && cinViewHolder.description_expand_icon != null) {
            cinViewHolder.expandable_description_layout.setExpanded(true);
            cinViewHolder.description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
        }
    }//expandCINDescriptionLayout ends

    private void collapseCINDetailsLayout(CINViewHolder cinViewHolder) {
        if (cinViewHolder != null && cinViewHolder.expandable_detail_layout != null && cinViewHolder.detail_expand_icon != null) {
            cinViewHolder.expandable_detail_layout.setExpanded(false);
            cinViewHolder.detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
        }
    }//collapseCINDetailsLayout ends

    private void collapseCIN_NewPaidLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_package_price_layout != null && cin_newViewHolder.package_expand_icon != null) {
            cin_newViewHolder.expandable_package_price_layout.setExpanded(false);
            cin_newViewHolder.package_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
        }
    }//collapseCIN_NewPaidLayout ends

    private void collapseCIN_NewUnPaidLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_payg_price_layout != null && cin_newViewHolder.package_expand_icon != null) {
            cin_newViewHolder.expandable_payg_price_layout.setExpanded(false);
            cin_newViewHolder.payg_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
        }
    }//collapseCIN_NewUnPaidLayout ends

    private void collapseCIN_NewBonusesLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_title_layout != null) {
            cin_newViewHolder.expandable_title_layout.setExpanded(false);
        }
    }//collapseCIN_NewBonusesLayout ends

    private void collapseCINPricesLayout(CINViewHolder cinViewHolder) {
        if (cinViewHolder != null && cinViewHolder.expandable_title_layout != null) {
            cinViewHolder.expandable_title_layout.setExpanded(false);
        }
    }//collapseCINPricesLayout ends

    private void expandCINDetailsLayout(CINViewHolder cinViewHolder) {
        if (cinViewHolder != null && cinViewHolder.expandable_detail_layout != null && cinViewHolder.detail_expand_icon != null) {
            cinViewHolder.expandable_detail_layout.setExpanded(true);
            cinViewHolder.detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
        }
    }//expandCINDetailsLayout ends

    private void expandCIN_NewPaidLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_package_price_layout != null && cin_newViewHolder.package_expand_icon != null) {
            cin_newViewHolder.expandable_package_price_layout.setExpanded(true);
            cin_newViewHolder.package_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
        }
    }//expandCIN_NewPaidLayout ends

    private void expandCIN_NewUnPaidLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_payg_price_layout != null && cin_newViewHolder.payg_price_expand_icon != null) {
            cin_newViewHolder.expandable_payg_price_layout.setExpanded(true);
            cin_newViewHolder.payg_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
        }
    }//expandCIN_NewUnPaidLayout ends

    private void expandCIN_NewBonusesLayout(CIN_NewViewHolder cin_newViewHolder) {
        if (cin_newViewHolder != null && cin_newViewHolder.expandable_title_layout != null) {
            cin_newViewHolder.expandable_title_layout.setExpanded(true);
        }
    }//expandCIN_NewBonusesLayout ends

    private void expandCINPricesLayout(CINViewHolder cinViewHolder) {
        if (cinViewHolder != null && cinViewHolder.expandable_title_layout != null) {
            cinViewHolder.expandable_title_layout.setExpanded(true);
        }
    }//expandCINPricesLayout ends

    private void processOnSubscribeButtonForCinNew(Context context, String offeringId, String tariffName, String subscribable) {
        if (context == null) return;

        ChangeTariff.requestForChangeTariffDialog(context, offeringId, tariffName, subscribable, KLASS, new ChangeTariffListener() {
            @Override
            public void onChangeTariffListenerSuccess(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_SUCCESS,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }

            @Override
            public void onChangeTariffListenerFailure(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_FAILURE,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }
        });

    }//processOnSubscribeButtonForCinNew ends

    private void processOnSubscribeButtonForCin(Context context, String offeringId, String tariffName, String subscribable) {
        if (context == null) return;

        ChangeTariff.requestForChangeTariffDialog(context, offeringId, tariffName, subscribable, CIN, new ChangeTariffListener() {
            @Override
            public void onChangeTariffListenerSuccess(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_SUCCESS,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }

            @Override
            public void onChangeTariffListenerFailure(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_FAILURE,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }
        });

    }//processOnSubscribeButton ends

    class CINViewHolder extends RecyclerView.ViewHolder {
        LinearLayout secondLayout, thirdLayout;
        ExpandableLayout expandable_title_layout, expandable_description_layout, expandable_detail_layout;
        private ImageView detail_expand_icon, description_expand_icon;
        private HeaderLayout headerLayout = new HeaderLayout();
        private TariffDetailLayout detailLayout = new TariffDetailLayout();
        private DescriptionLayout descriptionLayout = new DescriptionLayout();

        BakcellButtonNormal button_subscribe;
        ConstraintLayout ratingStarsLayout;

        View line_belew_expanable_title, detail_belew_line, description_belew_line;

        BakcellTextViewNormal detail_title_txt;
        BakcellTextViewNormal description_title_txt;
        CheckBox starOne, starTwo, starThree, starFour, starFive;

        private class HeaderLayout {
            BakcellTextViewBold cin_title_txt;
            BakcellTextViewNormal bonus_txt, tv_bonus_text, tv_call_lbl, tv_call_value, tv_sms_lbl,
                    tv_sms_value, tv_internet_lbl, tv_internet_value, tv_InternetSubTitle,
                    tv_InternetSubTitleValue, tv_call_left_value;
            ImageView call_img, sms_img, internet_img;
            LinearLayout call_content_layout, sms_content_layout, internet_content_layout;
            View seperator;
        }//HeaderLayout ends

        private class DescriptionLayout {
            RelativeLayout advantage_header_layout, clarification_header_layout;
            BakcellTextViewNormal advantage_txt_title, clarification_txt_title,
                    tv_advantages_description, every_subcriber_txt;
        }//DescriptionLayout ends

        public CINViewHolder(View itemView) {
            super(itemView);
            int width = (int) Tools.convertDpToPixel(RootValues.getInstance().getTARIFFS_WIDTH(), itemView.getContext());
            itemView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
            //tv_bonus_text = (TextView) itemView.findViewById(R.id.tv_bonus_text);
            secondLayout = itemView.findViewById(R.id.second_layout);
            thirdLayout = itemView.findViewById(R.id.third_layout);
            expandable_title_layout = itemView.findViewById(R.id.expandable_title_layout);
            expandable_detail_layout = itemView.findViewById(R.id.expandable_detail_layout);
            expandable_description_layout = itemView.findViewById(R.id.expandable_description_layout);
            description_expand_icon = itemView.findViewById(R.id.description_expand_icon);
            detail_expand_icon = itemView.findViewById(R.id.detail_expand_icon);

            //Headers view
            headerLayout.cin_title_txt = itemView.findViewById(R.id.cin_title_txt);
            headerLayout.bonus_txt = itemView.findViewById(R.id.bonus_txt);
            headerLayout.tv_bonus_text = itemView.findViewById(R.id.tv_bonus_text);
            headerLayout.tv_bonus_text.setSelected(true);
            headerLayout.tv_call_lbl = itemView.findViewById(R.id.tv_call_lbl);
            headerLayout.tv_call_value = itemView.findViewById(R.id.tv_call_value);
            headerLayout.tv_call_value.setSelected(true);
            headerLayout.seperator = itemView.findViewById(R.id.seperator);
            headerLayout.tv_sms_lbl = itemView.findViewById(R.id.tv_sms_lbl);
            headerLayout.tv_sms_value = itemView.findViewById(R.id.tv_sms_value);
            headerLayout.tv_internet_lbl = itemView.findViewById(R.id.tv_internet_lbl);
            headerLayout.tv_internet_value = itemView.findViewById(R.id.tv_internet_value);
            headerLayout.tv_InternetSubTitle = itemView
                    .findViewById(R.id.tv_InternetSubTitle);

            headerLayout.tv_InternetSubTitleValue = itemView
                    .findViewById(R.id.tv_InternetSubTitleValue);
            headerLayout.tv_call_left_value = itemView
                    .findViewById(R.id.tv_call_left_value);
            headerLayout.tv_call_left_value.setSelected(true);

            headerLayout.call_content_layout = itemView.findViewById(R.id.call_content_layout);
            headerLayout.sms_content_layout = itemView.findViewById(R.id.sms_content_layout);
            headerLayout.internet_content_layout = itemView.findViewById(R.id.internet_content_layout);

            headerLayout.call_img = itemView.findViewById(R.id.call_img);
            headerLayout.sms_img = itemView.findViewById(R.id.call_img);
            headerLayout.internet_img = itemView.findViewById(R.id.call_img);

            button_subscribe = itemView.findViewById(R.id.button_subscribe);

            //Rating bar
            ratingStarsLayout = itemView.findViewById(R.id.ratingStarsLayout);
            starOne = itemView.findViewById(R.id.star1);
            starTwo = itemView.findViewById(R.id.star2);
            starThree = itemView.findViewById(R.id.star3);
            starFour = itemView.findViewById(R.id.star4);
            starFive = itemView.findViewById(R.id.star5);


            //Description view

            descriptionLayout.advantage_header_layout = itemView.findViewById(R.id.advantage_header_layout);
            descriptionLayout.clarification_header_layout = itemView.findViewById(R.id.clarification_header_layout);

            descriptionLayout.every_subcriber_txt = itemView.findViewById(R.id.every_subcriber_txt);
            descriptionLayout.tv_advantages_description = itemView.findViewById(R.id.tv_advantages_description);

            descriptionLayout.advantage_txt_title = itemView.findViewById(R.id.advantage_txt);
            descriptionLayout.clarification_txt_title = itemView.findViewById(R.id.clarification_txt);

            line_belew_expanable_title = itemView.findViewById(R.id.line_belew_expanable_title);
            detail_belew_line = itemView.findViewById(R.id.detail_belew_line);
            description_belew_line = itemView.findViewById(R.id.description_belew_line);

            detail_title_txt = itemView.findViewById(R.id.detail_title_txt);
            detail_title_txt.setSelected(true);

            description_title_txt = itemView.findViewById(R.id.description_title_txt);
            description_title_txt.setSelected(true);

            // Init Detail View
            detailLayout = TariffsFragment.loadDetailUI_Ids(itemView);


        }//constructor ends

        void prepareListItem(final Context context, final int i, TariffCin tariffCin, final CINViewHolder cinViewHolder, final CIN_NewViewHolder cin_newViewHolder) {
            secondLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**check if CIN Details layout already expanded*/
                    if (cinViewHolder != null && cinViewHolder.expandable_detail_layout.isExpanded()) {
                        //already expanded,collapse the CIN Details and CIN_New Package Prices Layout
                        collapseCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = false;
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;

                        //expand the cin prices and cin new bonuses layout
                        expandCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = true;
                        expandCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;
                    } else {
                        //expand the CIN Details and CIN_New Package Prices Layout
                        expandCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = true;
                        expandCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                        collapseCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;
                        collapseCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = false;
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                    }
                    notifyDataSetChanged();
                }
            });

            thirdLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**check if the CIN Description or CIN_NEW Details Layout already expanded*/
                    if ((cinViewHolder != null && cinViewHolder.expandable_description_layout.isExpanded())) {
                        //already expanded, collapse them
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;

                        //expand the cin prices and cin new bonuses layout
                        expandCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = true;
                        expandCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = true;

                        //collapse the other layouts
                        collapseCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = false;
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                    } else {
                        //expand the CIN Description and CIN_NEW Details Layout
                        expandCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = true;
                        expandCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = false;
                        collapseCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = false;
                        collapseCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = false;
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                    }
                    notifyDataSetChanged();
                }
            });
        }//prepareListItem ends

        void prepareHeaderItem(final Header header, CINViewHolder holder) {
            holder.headerLayout.cin_title_txt.setText("");
            holder.headerLayout.bonus_txt.setText("");
            holder.headerLayout.tv_bonus_text.setText("");
            holder.headerLayout.tv_call_lbl.setText("");
            holder.headerLayout.tv_call_value.setText("");
            holder.headerLayout.tv_internet_lbl.setText("");
            holder.headerLayout.tv_internet_value.setText("");
            holder.headerLayout.tv_InternetSubTitleValue.setText("");
            holder.headerLayout.tv_InternetSubTitle.setText("");
            holder.headerLayout.tv_sms_lbl.setText("");
            holder.headerLayout.tv_sms_value.setText("");
            if (Tools.hasValue(header.getName())) {
                headerLayout.cin_title_txt.setText(header.getName());
                headerLayout.cin_title_txt.setSelected(true);
            }
            if (Tools.hasValue(header.getBonusTitle())) {
                headerLayout.bonus_txt.setText(header.getBonusTitle() + ":");
                headerLayout.bonus_txt.measure(0, 0);
            } else {
                headerLayout.bonus_txt.setVisibility(View.GONE);
            }
            if (header.getBonusDescription() != null) {
                headerLayout.bonus_txt.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int width = headerLayout.bonus_txt.getWidth();
                        LeadingMarginSpan span = new LeadingMarginSpan.Standard(width, 0);
                        SpannableString spannableString = new SpannableString(header.getBonusDescription());
                        spannableString.setSpan(span, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        headerLayout.tv_bonus_text.setText(spannableString);
                    }
                }, 1);
            }
            if (header.getCall() != null) {

                String priceTemple = "";
                if (Tools.hasValue(header.getCall().getPriceTemplate())) {
                    priceTemple = header.getCall().getPriceTemplate();
                }
                boolean isDoubleVal = false;
                if (priceTemple.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)) {
                    holder.headerLayout.tv_call_left_value.setVisibility(View.VISIBLE);
                    isDoubleVal = true;
                } else {
                    holder.headerLayout.tv_call_left_value.setVisibility(View.INVISIBLE);
                }

                if (Tools.hasValue(header.getCall().getTitleValueRight())) {
                    holder.headerLayout.tv_call_value.setText(header.getCall().getTitleValueRight());
                }
                if (Tools.hasValue(header.getCall().getTitleValueLeft())) {
                    holder.headerLayout.tv_call_left_value.setText(header.getCall().getTitleValueLeft());
                }

                if (isDoubleVal && Tools.hasValue(header.getCall().getTitleValueLeft())
                        && Tools.hasValue(header.getCall().getTitleValueRight())) {
                    holder.headerLayout.seperator.setVisibility(View.VISIBLE);
                } else {
                    holder.headerLayout.seperator.setVisibility(View.GONE);
                }

                if (Tools.hasValue(header.getCall().getTitle())) {
                    holder.headerLayout.tv_call_lbl.setText(header.getCall().getTitle());
                    holder.headerLayout.tv_call_lbl.setSelected(true);
                }

//                Tools.setTariffsIcons(header.getCall().getIconName(), holder.headerLayout.call_img, context);

                if (header.getCall().getAttributes() != null) {
                    headerLayout.call_content_layout.removeAllViews();
                    Handler handler = new Handler();
                    final String finalPriceTemple = priceTemple;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < header.getCall().getAttributes().size(); i++) {
                                ViewItemTariffsAttribute viewItemTariffsAttribute =
                                        new ViewItemTariffsAttribute(context);
                                boolean isDoubleVal = false;
                                if (finalPriceTemple.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)
                                        || finalPriceTemple.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_VALUES)) {
                                    viewItemTariffsAttribute.getRl_left().setVisibility(View.VISIBLE);
                                    isDoubleVal = true;
                                } else {
                                    viewItemTariffsAttribute.getRl_left().setVisibility(View.INVISIBLE);
                                }
                                viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                                viewItemTariffsAttribute.getCredit_value_txt_2().setText("");
                                viewItemTariffsAttribute.getAttribute_label_text().setText("");
                                viewItemTariffsAttribute.getAttribute_label_text().setSelected(true);
                                if (Tools.hasValue(header.getCall().getAttributes().get(i).getTitle()))
                                    viewItemTariffsAttribute.getAttribute_label_text().setText(header.getCall().getAttributes().get(i).getTitle());

                                if (Tools.hasValue(header.getCall().getAttributes().get(i).getValueLeft())) {
                                    String value = header.getCall().getAttributes().get(i).getValueLeft().trim();
                                    if (!Tools.isNumeric(value)) {
                                        viewItemTariffsAttribute.hideLeftAZN();
                                    } else {
                                        viewItemTariffsAttribute.showLeftAZN();
                                    }
                                    if (Tools.isValueRed(value)) {
                                        viewItemTariffsAttribute.setLeftValueTextColor(R.color.colorPrimary);
                                    } else {
                                        viewItemTariffsAttribute.setLeftValueTextColor(R.color.black);
                                    }
                                    viewItemTariffsAttribute.getCredit_value_txt_2().setText(value);
                                } else {
                                    viewItemTariffsAttribute.hideLeftAZN();
                                }
                                if (Tools.hasValue(header.getCall().getAttributes().get(i).getValueRight())) {
                                    String value = header.getCall().getAttributes().get(i).getValueRight().trim();
                                    if (!Tools.isNumeric(value)) {
                                        viewItemTariffsAttribute.hideRightAZN();
                                    } else {
                                        viewItemTariffsAttribute.showRightAZN();
                                    }
                                    if (Tools.isValueRed(value)) {
                                        viewItemTariffsAttribute.setRightValueTextColor(R.color.colorPrimary);
                                    } else {
                                        viewItemTariffsAttribute.setRightValueTextColor(R.color.black);
                                    }
                                    viewItemTariffsAttribute.getCredit_value_txt_1().setText(value);
                                } else {
                                    viewItemTariffsAttribute.hideRightAZN();
                                }
                                // Hide/Show Saperat
                                if (isDoubleVal && Tools.hasValue(header.getCall().getAttributes().get(i).getValueLeft())
                                        && Tools.hasValue(header.getCall().getAttributes().get(i).getValueRight())) {
                                    viewItemTariffsAttribute.getSeperator().setVisibility(View.VISIBLE);
                                    viewItemTariffsAttribute.getCredit_value_txt_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                                    viewItemTariffsAttribute.getTv_tzn_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                                } else {
                                    viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                                }
                                viewItemTariffsAttribute.getAttribute_label_text().setSelected(true);
                                headerLayout.call_content_layout.addView(viewItemTariffsAttribute);
                            }
                        }
                    }, 10);

                }
            }
            if (header.getSms() != null) {
                if (header.getSms().getTitle() != null) {
                    holder.headerLayout.tv_sms_lbl.setText(header.getSms().getTitle());
                    holder.headerLayout.tv_sms_lbl.setSelected(true);
                }
                if (header.getSms().getTitleValue() != null) {
                    holder.headerLayout.tv_sms_value.setText(header.getSms().getTitleValue());
                }

                if (header.getSms().getAttributes() != null) {
                    addAttributesToHeader(header.getSms().getAttributes(), holder.headerLayout.sms_content_layout);
                }

//                Tools.setTariffsIcons(header.getSms().getIconName(), holder.headerLayout.sms_img, context);

            }
            if (header.getInternet() != null) {
                if (header.getInternet().getTitle() != null) {
                    holder.headerLayout.tv_internet_lbl.setText(header.getInternet().getTitle());
                    holder.headerLayout.tv_internet_lbl.setSelected(true);
                }
                if (header.getInternet().getTitleValue() != null) {
                    holder.headerLayout.tv_internet_value.setText(header.getInternet().getTitleValue());
                }

                if (header.getInternet().getAttributes() != null) {
                    addAttributesToHeader(header.getInternet().getAttributes(), holder.headerLayout.internet_content_layout);
                }
                if (Tools.hasValue(header.getInternet().getSubTitle())) {
                    holder.headerLayout.tv_InternetSubTitle.setText(header.getInternet().getSubTitle());
                }
                if (Tools.hasValue(header.getInternet().getSubTitleValue())) {
                    holder.headerLayout.tv_InternetSubTitleValue.setText(header.getInternet().getSubTitleValue());
                }

//                Tools.setTariffsIcons(header.getInternet().getIconName(), holder.headerLayout.internet_img, context);

            }

            //handling subscribe button.!

            if (header.getSubscribable() != null) {

                if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_NOT_SUBSCRIBE)) {

                    button_subscribe.setEnabled(false);
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                    ratingStarsLayout.setVisibility(View.GONE);

                } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.ALREADY_SUBSCRIBED)) {

                    button_subscribe.setText(R.string.subscribed_lbl);
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                    button_subscribe.setEnabled(false);
                    ratingStarsLayout.setVisibility(View.VISIBLE);

                } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.RENEWABLE)) {

                    button_subscribe.setText(R.string.renew_lbl);
                    button_subscribe.setEnabled(true);
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    ratingStarsLayout.setVisibility(View.VISIBLE);

                } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_SUBSCRIBE)) {

                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.black));
                    button_subscribe.setText(R.string.subscribe_lbl);
                    button_subscribe.setEnabled(true);
                    ratingStarsLayout.setVisibility(View.GONE);

                } else {
                    button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                    button_subscribe.setEnabled(false);
                    ratingStarsLayout.setVisibility(View.GONE);
                }
                button_subscribe.setSelected(true);


            }


        }

        void prepareDetailItem(TariffDetails offerDetail, CINViewHolder holder) {
            if (offerDetail == null) {
                holder.secondLayout.setVisibility(View.GONE);
                holder.detail_belew_line.setVisibility(View.GONE);
                return;
            }
            holder.detail_belew_line.setVisibility(View.VISIBLE);
            holder.secondLayout.setVisibility(View.VISIBLE);

            if (Tools.hasValue(offerDetail.getDetailLabel())) {
                detail_title_txt.setText(offerDetail.getDetailLabel());
            }

            TariffsFragment.populateDetailSectionContents(offerDetail, detailLayout, context);

        }

        void prepareDescriptionItem(Description description, CINViewHolder holder) {
            if (description == null) {
                holder.thirdLayout.setVisibility(View.GONE);
                holder.description_belew_line.setVisibility(View.GONE);
                return;
            }

            if (Tools.hasValue(description.getDescLabel())) {
                holder.description_title_txt.setText(description.getDescLabel());
            }

            holder.description_belew_line.setVisibility(View.VISIBLE);
            holder.thirdLayout.setVisibility(View.VISIBLE);
            holder.descriptionLayout.every_subcriber_txt.setText("");
            holder.descriptionLayout.tv_advantages_description.setText("");

            holder.descriptionLayout.advantage_txt_title.setText("");
            holder.descriptionLayout.clarification_txt_title.setText("");
            holder.descriptionLayout.advantage_header_layout.setVisibility(View.GONE);
            holder.descriptionLayout.clarification_header_layout.setVisibility(View.GONE);

            if (description.getAdvantages() != null) {
                holder.descriptionLayout.advantage_header_layout.setVisibility(View.VISIBLE);
                if (Tools.hasValue(description.getAdvantages().getTitle())) {
                    holder.descriptionLayout.advantage_txt_title.setText(description.getAdvantages().getTitle());
                }
                if (description.getAdvantages().getDescription() != null) {
                    holder.descriptionLayout.tv_advantages_description.setText(description.getAdvantages().getDescription());
                }
            }

            if (description.getClassification() != null) {
                holder.descriptionLayout.clarification_header_layout.setVisibility(View.VISIBLE);

                if (Tools.hasValue(description.getClassification().getTitle())) {
                    holder.descriptionLayout.clarification_txt_title.setText(description.getClassification().getTitle());
                }

                if (description.getClassification().getDescription() != null) {
                    holder.descriptionLayout.every_subcriber_txt.setText(description.getClassification().getDescription());
                }
            }
        }

        void addAttributesToHeader(final ArrayList<Attributes> attributes, final LinearLayout linearLayout) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    linearLayout.removeAllViews();
                    for (int i = 0; i < attributes.size(); i++) {
                        ViewItemTariffsAttribute item =
                                new ViewItemTariffsAttribute(context);
                        item.getAttribute_label_text().setText("");
                        item.getCredit_value_txt_1().setText("");
                        if (attributes.get(i).getTitle() != null)
                            item.getAttribute_label_text().setText(attributes.get(i).getTitle());
                        if (attributes.get(i).getValue() != null) {
                            String value = attributes.get(i).getValue().trim();
                            item.getCredit_value_txt_1().setText(value);
                            if (Tools.isValueRed(value)) {
                                item.setRightValueTextColor(R.color.colorPrimary);
                            } else {
                                item.setRightValueTextColor(R.color.black);
                            }
                            if (!Tools.isNumeric(value)) {
                                item.hideRightAZN();
                            }
                            item.getSeperator().setVisibility(View.GONE);
                        }
                        linearLayout.addView(item);
                    }
                }
            }, 10);

        }
    }//CINViewHolder ends

    class CIN_NewViewHolder extends RecyclerView.ViewHolder {
        ExpandableLayout expandable_title_layout;
        ExpandableLayout expandable_package_price_layout;
        ExpandableLayout expandable_payg_price_layout;
        ExpandableLayout expandable_detail_layout;
        private ImageView package_expand_icon, payg_price_expand_icon, detail_expand_icon;
        LinearLayout secondLayout, thirdLayout, fourthLayout, attribute_content_layout;
        HeaderLayout headerLayout = new HeaderLayout();
        PackagePriceLayout packagePriceLayout = new PackagePriceLayout();
        PAYgPricesLayout paYgPricesLayout = new PAYgPricesLayout();
        TariffDetailLayout detailLayout = new TariffDetailLayout();

        BakcellButtonNormal button_subscribe;
        ConstraintLayout ratingStarsLayout;
        CheckBox starOne, starTwo, starThree, starFour, starFive;

        View title_belew_line, package_belew_line, payg_belew_line, detail_belew_line;
        BakcellTextViewNormal detail_title_txt;

        class HeaderLayout {
            BakcellTextViewBold offer_title_txt;
            BakcellTextViewNormal tv_price_label, creditValueTxt, tv_tzn;
        }

        class PackagePriceLayout {
            BakcellTextViewNormal package_price_title_txt;
            LinearLayout call_content_layout, sms_content_layout, internet_content_layout;
            BakcellTextViewNormal tv_call_lbl, tv_call_value, tv_sms_lbl, tv_sms_value,
                    tv_internet_lbl, tv_internet_value, tv_call_left_value, tv_InternetSubTitle, tv_InternetSubTitleValue, tv_tzn_7;
            ImageView call_icon, sms_icon, internet_icon;
            View seperator;
        }

        class PAYgPricesLayout {
            BakcellTextViewNormal payg_price_title_txt;
            LinearLayout call_content_layout, sms_content_layout;
            BakcellTextViewNormal tv_call_lbl, tv_call_value, tv_sms_lbl, tv_sms_value,
                    tv_internet_lbl, tv_internet_value, tv_call_left_value_1, tv_InternetSubTitle, tv_InternetSubTitleValue;
            ImageView call_icon_1, sms_icon_1, internet_icon_1;
            View seperator_1;
        }

        public CIN_NewViewHolder(View itemView) {
            super(itemView);
            int width = (int) Tools.convertDpToPixel(RootValues.getInstance().getTARIFFS_WIDTH(), itemView.getContext());
            itemView.setLayoutParams(new RecyclerView.LayoutParams(width,
                    RecyclerView.LayoutParams.MATCH_PARENT));
            expandable_title_layout = itemView
                    .findViewById(R.id.expandable_title_layout);
            expandable_package_price_layout = itemView
                    .findViewById(R.id.expandable_package_price_layout);
            expandable_payg_price_layout = itemView
                    .findViewById(R.id.expandable_payg_price_layout);
            expandable_detail_layout = itemView
                    .findViewById(R.id.expandable_detail_layout);
            secondLayout = itemView.findViewById(R.id.second_layout);
            thirdLayout = itemView.findViewById(R.id.third_layout);
            fourthLayout = itemView.findViewById(R.id.fouth_layout);
            attribute_content_layout = itemView
                    .findViewById(R.id.attribute_content_layout);
            package_expand_icon = itemView.findViewById(R.id.package_expand_icon);
            payg_price_expand_icon = itemView.findViewById(R.id.payg_price_expand_icon);
            detail_expand_icon = itemView.findViewById(R.id.detail_expand_icon);

            //View for header
            headerLayout.offer_title_txt = itemView
                    .findViewById(R.id.offer_title_txt);
            headerLayout.tv_price_label = itemView
                    .findViewById(R.id.tv_price_label);
            headerLayout.creditValueTxt = itemView
                    .findViewById(R.id.creditValueTxt);
            headerLayout.tv_tzn = itemView
                    .findViewById(R.id.tv_tzn);

            button_subscribe = itemView.findViewById(R.id.button_subscribe);


            // Rating Stars
            ratingStarsLayout = itemView.findViewById(R.id.ratingStarsLayout);
            starOne = itemView.findViewById(R.id.star1);
            starTwo = itemView.findViewById(R.id.star2);
            starThree = itemView.findViewById(R.id.star3);
            starFour = itemView.findViewById(R.id.star4);
            starFive = itemView.findViewById(R.id.star5);


            //View for PackagePrice
            packagePriceLayout.call_content_layout = itemView
                    .findViewById(R.id.call_content_layout);
            packagePriceLayout.tv_call_lbl = itemView
                    .findViewById(R.id.tv_call_lbl);
            packagePriceLayout.tv_call_value = itemView
                    .findViewById(R.id.tv_call_value);
            //packagePriceLayout.tv_call_value.setSelected(true);
            packagePriceLayout.tv_sms_lbl = itemView
                    .findViewById(R.id.tv_sms_lbl);
            packagePriceLayout.tv_sms_value = itemView
                    .findViewById(R.id.tv_sms_value);
            packagePriceLayout.tv_internet_lbl = itemView
                    .findViewById(R.id.tv_internet_lbl);
            packagePriceLayout.tv_internet_value = itemView
                    .findViewById(R.id.tv_internet_value);
            packagePriceLayout.tv_call_left_value = itemView.findViewById(R.id.tv_call_left_value);
            packagePriceLayout.tv_call_left_value.setSelected(true);
            packagePriceLayout.seperator = itemView.findViewById(R.id.seperator);
            packagePriceLayout.package_price_title_txt = itemView
                    .findViewById(R.id.package_price_title_txt);
            packagePriceLayout.tv_tzn_7 = itemView
                    .findViewById(R.id.tv_tzn_7);
            packagePriceLayout.package_price_title_txt.setSelected(true);
            packagePriceLayout.tv_InternetSubTitle = itemView
                    .findViewById(R.id.tv_InternetSubTitle);
            packagePriceLayout.tv_InternetSubTitleValue = itemView
                    .findViewById(R.id.tv_InternetSubTitleValue);
            packagePriceLayout.sms_content_layout = itemView
                    .findViewById(R.id.sms_content_layout);

            packagePriceLayout.internet_content_layout = itemView
                    .findViewById(R.id.internet_content_layout);

            packagePriceLayout.call_icon = itemView.findViewById(R.id.call_icon);
            packagePriceLayout.sms_icon = itemView.findViewById(R.id.sms_icon);
            packagePriceLayout.internet_icon = itemView.findViewById(R.id.internet_icon);

            //Views for PayGPrice
            paYgPricesLayout.payg_price_title_txt = itemView
                    .findViewById(R.id.payg_price_title_txt);
            paYgPricesLayout.payg_price_title_txt.setSelected(true);
            paYgPricesLayout.call_content_layout = itemView
                    .findViewById(R.id.call_content_layout_1);
            paYgPricesLayout.tv_call_lbl = itemView
                    .findViewById(R.id.tv_call_lbl_1);
            paYgPricesLayout.tv_call_value = itemView
                    .findViewById(R.id.tv_call_value_1);
            //paYgPricesLayout.tv_call_value.setSelected(true);
            paYgPricesLayout.tv_sms_lbl = itemView
                    .findViewById(R.id.tv_sms_lbl_1);
            paYgPricesLayout.tv_sms_value = itemView
                    .findViewById(R.id.tv_sms_value_1);
            paYgPricesLayout.tv_internet_lbl = itemView
                    .findViewById(R.id.tv_internet_lbl_1);
            paYgPricesLayout.tv_internet_value = itemView
                    .findViewById(R.id.tv_internet_value_1);
            paYgPricesLayout.tv_call_left_value_1 = itemView.findViewById(R.id.tv_call_left_value_1);
            paYgPricesLayout.tv_call_left_value_1.setSelected(true);
            paYgPricesLayout.seperator_1 = itemView.findViewById(R.id.seperator_1);
            paYgPricesLayout.sms_content_layout = itemView
                    .findViewById(R.id.sms_content_layout_1);

            paYgPricesLayout.tv_InternetSubTitle = itemView
                    .findViewById(R.id.tv_InternetSubTitle_payg);
            paYgPricesLayout.tv_InternetSubTitleValue = itemView
                    .findViewById(R.id.tv_InternetSubTitleValue_payg);

            paYgPricesLayout.call_icon_1 = itemView.findViewById(R.id.call_icon_1);
            paYgPricesLayout.sms_icon_1 = itemView.findViewById(R.id.sms_icon_1);
            paYgPricesLayout.internet_icon_1 = itemView.findViewById(R.id.internet_icon_1);


            title_belew_line = itemView.findViewById(R.id.title_belew_line);
            package_belew_line = itemView.findViewById(R.id.package_belew_line);
            payg_belew_line = itemView.findViewById(R.id.payg_belew_line);
            detail_belew_line = itemView.findViewById(R.id.detail_belew_line);

            detail_title_txt = itemView.findViewById(R.id.detail_title_txt);
            detail_title_txt.setSelected(true);

            // Init Detail View
            detailLayout = TariffsFragment.loadDetailUI_Ids(itemView);

        }

        void prepareListItem(final Context context, final int i, final CINViewHolder cinViewHolder, final CIN_NewViewHolder cin_newViewHolder) {


            secondLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**check if CIN_NEW Paid layout already expanded*/
                    if (cin_newViewHolder != null && cin_newViewHolder.expandable_package_price_layout.isExpanded()) {
                        //already expanded,collapse the CIN_New Paid and CIN Description Layout
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;
                        collapseCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = false;

                        //expand the cin prices and cin new bonuses layout
                        expandCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = true;
                        expandCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                    } else {
                        //expand the CIN Details and CIN_New Package Prices Layout
                        expandCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = true;
                        expandCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = false;
                        collapseCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = false;
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                    }
                    notifyDataSetChanged();
                }
            });

            thirdLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**check if CIN_NEW UnPaid layout already expanded*/
                    if (cin_newViewHolder != null && cin_newViewHolder.expandable_payg_price_layout.isExpanded()) {
                        //already expanded,collapse the CIN_New UnPaid and CIN Description Layout
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                        collapseCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = false;

                        //expand the cin prices and cin new bonuses layout
                        expandCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = true;
                        expandCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                    } else {
                        //expand the CIN Details and CIN_New UnPaid Layout
                        expandCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = true;
                        expandCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = false;
                        collapseCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = false;
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                    }
                    notifyDataSetChanged();
                }
            });

            fourthLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**check if the CIN Description or CIN_NEW Details Layout already expanded*/
                    if (cin_newViewHolder != null && cin_newViewHolder.expandable_detail_layout.isExpanded()) {
                        //already expanded, collapse them
                        collapseCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = false;
                        collapseCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = false;

                        //expand the cin prices and cin new bonuses layout
                        expandCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = true;
                        expandCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = true;

                        //collapse the other layouts
                        collapseCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = false;
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                    } else {
                        //expand the CIN Description and CIN_NEW Details Layout
                        expandCINDescriptionLayout(cinViewHolder);
                        isCinDescriptionLayoutExpanded = true;
                        expandCIN_NewDetailsLayout(cin_newViewHolder);
                        isCin_NewDetailsLayoutExpanded = true;

                        //all other layouts should be collapsed
                        collapseCINPricesLayout(cinViewHolder);
                        isCINPricesLayoutExpanded = false;
                        collapseCIN_NewBonusesLayout(cin_newViewHolder);
                        isCIN_NewBonusesLayoutExpanded = false;
                        collapseCINDetailsLayout(cinViewHolder);
                        isCinDetailsLayoutExpanded = false;
                        collapseCIN_NewPaidLayout(cin_newViewHolder);
                        isCIN_NewPaidLayoutExpanded = false;
                        collapseCIN_NewUnPaidLayout(cin_newViewHolder);
                        isCin_NewUnpaidLayoutExpanded = false;
                    }
                    notifyDataSetChanged();
                }
            });
        }

        void prepareHeaderItem(Header header, CIN_NewViewHolder holder) {
            holder.headerLayout.creditValueTxt.setText("");
            holder.headerLayout.tv_price_label.setText("");
            holder.headerLayout.offer_title_txt.setText("");
            if (header.getName() != null) {
                holder.headerLayout.offer_title_txt.setText(header.getName());
                holder.headerLayout.offer_title_txt.setSelected(true);
            }
            if (header.getPriceLabel() != null) {
                holder.headerLayout.tv_price_label.setSelected(true);
                holder.headerLayout.tv_price_label.setText(header.getPriceLabel() + ": ");
            }
            if (header.getPriceValue() != null && Tools.hasValue(header.getPriceValue())) {
                holder.headerLayout.creditValueTxt.setText(header.getPriceValue());
                holder.headerLayout.tv_tzn.setVisibility(View.VISIBLE);
            } else {
                holder.headerLayout.creditValueTxt.setText("");
                holder.headerLayout.tv_tzn.setVisibility(View.GONE);
            }
            holder.attribute_content_layout.removeAllViews();
            if (header != null && header.getAttributes() != null) {
                for (int i = 0; i < header.getAttributes().size(); i++) {
                    ViewItemTariffsKlassAttribute viewItemTariffsKlassAttribute =
                            new ViewItemTariffsKlassAttribute(context);
                    viewItemTariffsKlassAttribute.getAttribute_name().setText("");
                    viewItemTariffsKlassAttribute.getAttribute_value().setText("");
                    if (header.getAttributes().get(i).getTitle() != null) {
                        viewItemTariffsKlassAttribute.getAttribute_name().setText(header.getAttributes()
                                .get(i).getTitle());
                    }
                    if (header.getAttributes().get(i).getValue() != null) {
                        String value = header.getAttributes().get(i).getValue() + " " + header.getAttributes().get(i).getMetrics();
                        viewItemTariffsKlassAttribute.getAttribute_value().setText(value);
                        /*viewItemTariffsKlassAttribute.getAttribute_value()
                                .append(" " + header.getAttributes().get(i).getMetrics());*/
                        if (Tools.isValueRed(value.trim())) {
                            viewItemTariffsKlassAttribute.setValueTextColor(R.color.colorPrimary);
                        } else {
                            viewItemTariffsKlassAttribute.setValueTextColor(R.color.black);
                        }

                    }
                    /*if (i == (header.getAttributes().size() - 1)) {
                        viewItemTariffsKlassAttribute.getAttribute_value().setTextColor(ContextCompat
                                .getColor(context, R.color.colorPrimary));
                    }*/
                    if (i == header.getAttributes().size() - 1) {
                        viewItemTariffsKlassAttribute.getView().setVisibility(View.GONE);
                    } else {
                        viewItemTariffsKlassAttribute.getView().setVisibility(View.VISIBLE);
                    }

                    if (header.getAttributes().get(i) != null) {
                        Tools.setTariffsIcons(header.getAttributes().get(i).getIconName(), viewItemTariffsKlassAttribute.getIcon(), context);
                        //setOffersSubscriptionsIcons(header.getAttributes().get(i).getIconName(), viewItemTariffsKlassAttribute.getIcon());
                    }


                    holder.attribute_content_layout.addView(viewItemTariffsKlassAttribute);

                }

                //handling subscribe button.!

                if (header.getSubscribable() != null) {

                    if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_NOT_SUBSCRIBE)) {

                        button_subscribe.setEnabled(false);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        ratingStarsLayout.setVisibility(View.GONE);


                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.ALREADY_SUBSCRIBED)) {

                        button_subscribe.setText(R.string.subscribed_lbl);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        button_subscribe.setEnabled(false);
                        ratingStarsLayout.setVisibility(View.VISIBLE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.RENEWABLE)) {

                        button_subscribe.setText(R.string.renew_lbl);
                        button_subscribe.setEnabled(true);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        ratingStarsLayout.setVisibility(View.VISIBLE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_SUBSCRIBE)) {

                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.black));
                        button_subscribe.setText(R.string.subscribe_lbl);
                        button_subscribe.setEnabled(true);
                        ratingStarsLayout.setVisibility(View.GONE);

                    } else {
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        button_subscribe.setEnabled(false);
                        ratingStarsLayout.setVisibility(View.GONE);
                    }

                    button_subscribe.setSelected(true);
                }

            }
        }

        void preparePackagePriceItem(PackagePrice packagePriceItem, CIN_NewViewHolder holder) {


            if (packagePriceItem == null) {
                secondLayout.setVisibility(View.GONE);
                package_belew_line.setVisibility(View.GONE);
                return;
            }
            secondLayout.setVisibility(View.VISIBLE);
            package_belew_line.setVisibility(View.VISIBLE);
            holder.packagePriceLayout.tv_call_lbl.setText("");
            holder.packagePriceLayout.tv_call_value.setText("");
            holder.packagePriceLayout.tv_sms_lbl.setText("");
            holder.packagePriceLayout.tv_sms_value.setText("");
            holder.packagePriceLayout.tv_internet_lbl.setText("");
            holder.packagePriceLayout.tv_internet_value.setText("");

            holder.packagePriceLayout.call_icon.setVisibility(View.INVISIBLE);
            holder.packagePriceLayout.sms_icon.setVisibility(View.INVISIBLE);
            holder.packagePriceLayout.internet_icon.setVisibility(View.INVISIBLE);

            if (Tools.hasValue(packagePriceItem.getPackagePriceLabel())) {
                holder.packagePriceLayout.package_price_title_txt.setText(packagePriceItem.getPackagePriceLabel());
            }

            if (packagePriceItem.getCall() != null) {
                String priceTemplate = "";
                if (Tools.hasValue(packagePriceItem.getCall().getPriceTemplate())) {
                    priceTemplate = packagePriceItem.getCall().getPriceTemplate();
                }
                boolean isDoubleVal = false;
                if (priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)) {
                    holder.packagePriceLayout.tv_call_left_value.setVisibility(View.VISIBLE);
                    isDoubleVal = true;
                } else {
                    holder.packagePriceLayout.tv_call_left_value.setVisibility(View.INVISIBLE);
                }
                if (Tools.hasValue(packagePriceItem.getCall().getIconName())) {
                    holder.packagePriceLayout.call_icon.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(packagePriceItem.getCall().getIconName()
                            , holder.packagePriceLayout.call_icon, context);
                }

                if (Tools.hasValue(packagePriceItem.getCall().getTitle())) {
                    holder.packagePriceLayout.tv_call_lbl.setText(packagePriceItem.getCall().getTitle());
                }
                if (Tools.hasValue(packagePriceItem.getCall().getTitleValueRight())) {
                    holder.packagePriceLayout.tv_call_value.setText(packagePriceItem.getCall().getTitleValueRight());
                    holder.packagePriceLayout.tv_call_value.setSelected(true);
                }
                if (Tools.hasValue(packagePriceItem.getCall().getTitleValueLeft())) {
                    holder.packagePriceLayout.tv_call_left_value.setText(packagePriceItem.getCall().getTitleValueLeft());
                }
                if (isDoubleVal && Tools.hasValue(packagePriceItem.getCall().getTitleValueRight())
                        && Tools.hasValue(packagePriceItem.getCall().getTitleValueLeft())) {
                    holder.packagePriceLayout.seperator.setVisibility(View.VISIBLE);
                } else {
                    holder.packagePriceLayout.seperator.setVisibility(View.GONE);
                }
                if (packagePriceItem.getCall().getAttributes() != null) {
                    addAttributesToCall(packagePriceItem.getCall().getAttributes(), holder.packagePriceLayout.call_content_layout, priceTemplate);
                }
            }
            if (packagePriceItem.getSms() != null) {

                if (packagePriceItem.getSms().getIconName() != null) {
                    holder.packagePriceLayout.sms_icon.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(packagePriceItem.getSms().getIconName(), holder.packagePriceLayout.sms_icon, context);
                }

                if (packagePriceItem.getSms().getTitle() != null)
                    holder.packagePriceLayout.tv_sms_lbl.setText(packagePriceItem.getSms()
                            .getTitle());
                if (Tools.hasValue(packagePriceItem.getSms().getTitleValue()))
                    holder.packagePriceLayout.tv_sms_value.setText(packagePriceItem.getSms()
                            .getTitleValue());
                if (packagePriceItem.getSms().getAttributes() != null) {

                    addAttibuteToPackagePrice(packagePriceItem.getSms().getAttributes(),
                            holder.packagePriceLayout.sms_content_layout);
                }
            }
            if (packagePriceItem.getInterent() != null) {

                if (packagePriceItem.getInterent().getIconName() != null) {
                    holder.packagePriceLayout.internet_icon.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(packagePriceItem.getInterent().getIconName(), holder.packagePriceLayout.internet_icon, context);
                }

                if (packagePriceItem.getInterent().getTitle() != null)
                    holder.packagePriceLayout.tv_internet_lbl.setText(packagePriceItem.getInterent()
                            .getTitle());
                if (Tools.hasValue(packagePriceItem.getInterent().getTitleValue()))
                    holder.packagePriceLayout.tv_internet_value.setText(packagePriceItem
                            .getInterent().getTitleValue());
                if (Tools.hasValue(packagePriceItem.getInterent().getSubTitle())) {
                    holder.packagePriceLayout.tv_InternetSubTitle.setText(packagePriceItem.getInterent().getSubTitle());
                    holder.packagePriceLayout.tv_InternetSubTitle.setSelected(true);
                }

                if (Tools.hasValue(packagePriceItem.getInterent().getSubTitleValue())) {

                    if (Tools.isNumeric(packagePriceItem.getInterent().getSubTitleValue())) {
                        holder.packagePriceLayout.tv_InternetSubTitleValue.setText(packagePriceItem.getInterent().getSubTitleValue());
                        holder.packagePriceLayout.tv_InternetSubTitleValue.setSelected(true);
                        holder.packagePriceLayout.tv_tzn_7.setVisibility(View.VISIBLE);
                    } else {
                        BakcellLogger.logE("HvSub", "subTitle:::" + packagePriceItem.getInterent().getSubTitleValue(), fromClass, "Showing packagePriceItem.getInterent().getSubTitleValue()");
                        holder.packagePriceLayout.tv_InternetSubTitleValue.setText(packagePriceItem.getInterent().getSubTitleValue());
                        holder.packagePriceLayout.tv_InternetSubTitleValue.setSelected(true);
                        holder.packagePriceLayout.tv_tzn_7.setVisibility(View.GONE);
                    }
                }
            }

        }

        private void addAttributesToCall(final ArrayList<Attributes> attributes, final LinearLayout container, final String priceTemplate) {
            container.removeAllViews();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < attributes.size(); i++) {
                        final ViewItemTariffsAttribute viewItemTariffsAttribute = new ViewItemTariffsAttribute(context);
                        viewItemTariffsAttribute.getAttribute_label_text().setText("");
                        viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                        viewItemTariffsAttribute.getCredit_value_txt_2().setText("");
                        boolean isDoubleVal = false;
                        if (Tools.hasValue(priceTemplate) && priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)
                                || priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_VALUES)) {
                            viewItemTariffsAttribute.getRl_left().setVisibility(View.VISIBLE);
                            isDoubleVal = true;
                        } else {
                            viewItemTariffsAttribute.getRl_left().setVisibility(View.INVISIBLE);
                        }
                        if (Tools.hasValue(attributes.get(i).getValueLeft())) {
                            viewItemTariffsAttribute.getCredit_value_txt_2().setText(attributes.get(i).getValueLeft());
                        }
                        if (Tools.hasValue(attributes.get(i).getValueRight())) {
                            viewItemTariffsAttribute.getCredit_value_txt_1().setText(attributes.get(i).getValueRight());
                        }
                        if (Tools.hasValue(attributes.get(i).getTitle())) {
                            viewItemTariffsAttribute.getAttribute_label_text().setText(attributes.get(i).getTitle());
                        }

                        if (isDoubleVal && Tools.hasValue(attributes.get(i).getValueLeft())
                                && Tools.hasValue(attributes.get(i).getValueRight())) {
                            viewItemTariffsAttribute.getSeperator().setVisibility(View.VISIBLE);
                            viewItemTariffsAttribute.getCredit_value_txt_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                            viewItemTariffsAttribute.getTv_tzn_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        } else {
                            viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                        }
                        viewItemTariffsAttribute.getAttribute_label_text().setSelected(true);
                        container.addView(viewItemTariffsAttribute);
                    }
                }
            }, 10);

        }

        void preparePayGPriceItem(PaygPrice paygPrice, CIN_NewViewHolder holder) {

            if (paygPrice == null) {
                thirdLayout.setVisibility(View.GONE);
                payg_belew_line.setVisibility(View.GONE);
                return;
            }
            payg_belew_line.setVisibility(View.VISIBLE);
            thirdLayout.setVisibility(View.VISIBLE);

            holder.paYgPricesLayout.tv_call_lbl.setText("");
            holder.paYgPricesLayout.tv_call_value.setText("");
            holder.paYgPricesLayout.tv_sms_lbl.setText("");
            holder.paYgPricesLayout.tv_sms_value.setText("");
            holder.paYgPricesLayout.tv_internet_lbl.setText("");
            holder.paYgPricesLayout.tv_internet_value.setText("");
            holder.paYgPricesLayout.tv_InternetSubTitle.setText("");
            holder.paYgPricesLayout.tv_InternetSubTitleValue.setText("");

            holder.paYgPricesLayout.call_icon_1.setVisibility(View.INVISIBLE);
            holder.paYgPricesLayout.sms_icon_1.setVisibility(View.INVISIBLE);
            holder.paYgPricesLayout.internet_icon_1.setVisibility(View.INVISIBLE);

            if (Tools.hasValue(paygPrice.getPaygPriceLabel())) {
                holder.paYgPricesLayout.payg_price_title_txt.setText(paygPrice.getPaygPriceLabel());
            }

            if (paygPrice.getCall() != null) {
                String priceTemplate = "";
                if (Tools.hasValue(paygPrice.getCall().getPriceTemplate()))
                    priceTemplate = paygPrice.getCall().getPriceTemplate();
                boolean isDoubleVal = false;
                if (priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)) {
                    holder.paYgPricesLayout.tv_call_left_value_1.setVisibility(View.VISIBLE);
                    isDoubleVal = true;
                } else {
                    holder.paYgPricesLayout.tv_call_left_value_1.setVisibility(View.INVISIBLE);
                }
                if (Tools.hasValue(paygPrice.getCall().getIconName())) {
                    holder.paYgPricesLayout.call_icon_1.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(paygPrice.getCall().getIconName()
                            , holder.paYgPricesLayout.call_icon_1, context);
                }

                if (Tools.hasValue(paygPrice.getCall().getTitle())) {
                    holder.paYgPricesLayout.tv_call_lbl.setText(paygPrice.getCall().getTitle());
                }
                if (Tools.hasValue(paygPrice.getCall().getTitleValueRight())) {
                    holder.paYgPricesLayout.tv_call_value.setText(paygPrice.getCall().getTitleValueRight());
                }
                if (Tools.hasValue(paygPrice.getCall().getTitleValueLeft())) {
                    holder.paYgPricesLayout.tv_call_left_value_1.setText(paygPrice.getCall().getTitleValueLeft());
                }
                if (isDoubleVal && Tools.hasValue(paygPrice.getCall().getTitleValueRight())
                        && Tools.hasValue(paygPrice.getCall().getTitleValueLeft())) {
                    holder.paYgPricesLayout.seperator_1.setVisibility(View.VISIBLE);
                } else {
                    holder.paYgPricesLayout.seperator_1.setVisibility(View.GONE);
                }
                if (paygPrice.getCall().getAttributes() != null) {
                    holder.paYgPricesLayout.call_content_layout.removeAllViews();
                    addAttributesToCall(paygPrice.getCall().getAttributes(), holder.paYgPricesLayout.call_content_layout, priceTemplate);
                }
            }
            if (paygPrice.getSms() != null) {

                if (paygPrice.getSms().getIconName() != null) {
                    holder.paYgPricesLayout.sms_icon_1.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(paygPrice.getSms().getIconName(), holder.paYgPricesLayout.sms_icon_1, context);
                }

                if (paygPrice.getSms().getTitle() != null)
                    holder.paYgPricesLayout.tv_sms_lbl.setText(paygPrice.getSms()
                            .getTitle());
                if (paygPrice.getSms().getTitleValue() != null)
                    holder.paYgPricesLayout.tv_sms_value.setText(paygPrice.getSms()
                            .getTitleValue());
                if (paygPrice.getSms().getAttributes() != null) {
                    holder.paYgPricesLayout.sms_content_layout.removeAllViews();
                    addAttibuteToPackagePrice(paygPrice.getSms().getAttributes(),
                            holder.paYgPricesLayout.sms_content_layout);
                }
            }
            if (paygPrice.getInterent() != null) {

                if (paygPrice.getInterent().getIconName() != null) {
                    holder.paYgPricesLayout.internet_icon_1.setVisibility(View.VISIBLE);
                    Tools.setTariffsIcons(paygPrice.getInterent().getIconName(), holder.paYgPricesLayout.internet_icon_1, context);
                }

                if (paygPrice.getInterent().getTitle() != null) {
                    holder.paYgPricesLayout.tv_internet_lbl.setText(paygPrice.getInterent()
                            .getTitle());
                }
                if (paygPrice.getInterent().getTitleValue() != null) {
                    holder.paYgPricesLayout.tv_internet_value.setText(paygPrice.getInterent()
                            .getTitleValue());
                }

                if (Tools.hasValue(paygPrice.getInterent().getSubTitle())) {
                    holder.paYgPricesLayout.tv_InternetSubTitle.setText(paygPrice.getInterent().getSubTitle());
                    holder.paYgPricesLayout.tv_InternetSubTitle.setSelected(true);
                }
                if (Tools.hasValue(paygPrice.getInterent().getSubTitleValue())) {
                    holder.paYgPricesLayout.tv_InternetSubTitleValue.setText(paygPrice.getInterent().getSubTitleValue());
                }

            }

        }

        void prepareDetailItem(TariffDetails offerDetail, CIN_NewViewHolder holder) {

            if (offerDetail == null) {
                fourthLayout.setVisibility(View.GONE);
                detail_belew_line.setVisibility(View.GONE);
                return;
            }
            detail_belew_line.setVisibility(View.VISIBLE);
            fourthLayout.setVisibility(View.VISIBLE);

            if (Tools.hasValue(offerDetail.getDetailLabel())) {
                detail_title_txt.setText(offerDetail.getDetailLabel());
            }

            TariffsFragment.populateDetailSectionContents(offerDetail, detailLayout, context);

        }

        private void addAttibuteToPackagePrice(final ArrayList<Attributes> attributes, final LinearLayout container) {
            container.removeAllViews();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < attributes.size(); i++) {
                        ViewItemTariffsAttribute viewItemTariffsAttribute = new ViewItemTariffsAttribute(context);
                        viewItemTariffsAttribute.getRl_left().setVisibility(View.INVISIBLE);
                        viewItemTariffsAttribute.getAttribute_label_text().setText("");
                        viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                        viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                        if (attributes.get(i).getTitle() != null)
                            viewItemTariffsAttribute.getAttribute_label_text().setText(attributes.get(i).getTitle());
                        if (Tools.hasValue(attributes.get(i).getValue())) {
                            viewItemTariffsAttribute.getCredit_value_txt_1().setText(attributes.get(i).getValue());
                        } else {
                            viewItemTariffsAttribute.hideRightAZN();
                        }
                        viewItemTariffsAttribute.getAttribute_label_text().setSelected(true);
                        container.addView(viewItemTariffsAttribute);
                    }
                }
            }, 10);

        }
    }//CIN_NewViewHolder ends

    @Override
    public int getItemCount() {
        return tariffsHelperModels.size();
    }//getItemCount ends

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }//getItemId ends

    @Override
    public int getItemViewType(int position) {
        if (tariffsHelperModels.get(position) != null &&
                tariffsHelperModels.get(position).getAnyTariffObject() != null) {
            if (tariffsHelperModels.get(position).getAnyTariffObject() instanceof TariffCin) {
                if (((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader() != null &&
                        Tools.hasValue(((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader().getTariffType()) &&
                        ((TariffCin) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader().getTariffType().
                                equalsIgnoreCase(Constants.TariffsConstants.CIN_TYPE)) {
                    return Constants.TariffsConstants.LAYOUT_CIN;
                }//tariff type not null ends
            } else if (tariffsHelperModels.get(position).getAnyTariffObject() instanceof TariffCinNew) {
                if (((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader() != null &&
                        Tools.hasValue(((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader().getTariffType()) &&
                        ((TariffCinNew) tariffsHelperModels.get(position).getAnyTariffObject()).getHeader().getTariffType().
                                equalsIgnoreCase(Constants.TariffsConstants.CIN_NEW_TYPE)) {
                    return Constants.TariffsConstants.LAYOUT_CIN_NEW;
                }//tariff type not null ends
            }//instanceof TariffCinNew ends
        }//outer most if ends

        return Constants.TariffsConstants.LAYOUT_CIN;
    }//getItemViewType ends
}//class ends
