package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.models.loan.PaymentHistory;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;

/**
 * Created by Noman on 6/9/2017.
 */

public class CardAdapterPaymentRequest extends BaseAdapter {

    private ArrayList<PaymentHistory> paymentHistoryArrayList;
    private Context context;

    public CardAdapterPaymentRequest(ArrayList<PaymentHistory> paymentHistoryArrayList,
                                     Context context) {
        this.paymentHistoryArrayList = paymentHistoryArrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return paymentHistoryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_item_payment_history, null);
            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.tvAmount = (TextView) convertView.findViewById(R.id.tv_amount);
            viewHolder.tvLoanId = (TextView) convertView.findViewById(R.id.tv_loan_id);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (position > -1 && position < paymentHistoryArrayList.size()) {
            if (Tools.hasValue(paymentHistoryArrayList.get(position).getDateTime())) {
                viewHolder.tvDate.setText(paymentHistoryArrayList.get(position).getDateTime());
            } else {
                viewHolder.tvDate.setText("");
            }
            if (Tools.hasValue(paymentHistoryArrayList.get(position).getAmount())) {
                viewHolder.tvAmount.setText(paymentHistoryArrayList.get(position).getAmount());
            } else {
                viewHolder.tvAmount.setText("");
            }
            if (Tools.hasValue(paymentHistoryArrayList.get(position).getLoanID())) {
                viewHolder.tvLoanId.setText(paymentHistoryArrayList.get(position).getLoanID());
            } else {
                viewHolder.tvLoanId.setText("");
            }
        }

        return convertView;
    }

    public void updateAdapterData(ArrayList<PaymentHistory> data) {
        paymentHistoryArrayList = new ArrayList<>();
        paymentHistoryArrayList.addAll(data);
        notifyDataSetChanged();
    }

    class ViewHolder {
        TextView tvDate, tvLoanId, tvAmount;
    }
}

