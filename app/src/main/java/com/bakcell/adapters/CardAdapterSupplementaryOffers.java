package com.bakcell.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Handler;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.TopUpActivity;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.SearchResultListener;
import com.bakcell.interfaces.UpdateListOnSuccessfullySurvey;
import com.bakcell.models.inappsurvey.Answers;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Questions;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.inappsurvey.UserSurveys;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription.DetailsAttributes;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription.RoamingDetailsCountries;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription.SupplementryOffersDetailsAndDescription;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.header.HeaderAttributes;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.ViewItemOfferAttribute;
import com.bakcell.viewsitem.ViewItemOfferDetailBulletsItem;
import com.bakcell.viewsitem.ViewItemOfferDetailPriceAttribute;
import com.bakcell.viewsitem.ViewItemOfferDetailRoamingCountries;
import com.bakcell.viewsitem.ViewItemOfferDetailRoundingAttribute;
import com.bakcell.viewsitem.ViewItemOfferDetailTitleWithSubTitleDescAttribute;
import com.bakcell.webservices.subscribeunsubscribe.SubscribeUnsubscribe;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noman on 8/30/2017.
 */
public class CardAdapterSupplementaryOffers extends RecyclerView.Adapter<CardAdapterSupplementaryOffers.ViewHolder>
        implements Filterable {

    private ArrayList<SupplementaryOffer> offerArrayList;
    private ArrayList<SupplementaryOffer> offerArrayListTemp;

    private SearchResultListener searchResultListener;

    private Context context;
    private boolean isCardView;

    private boolean isDetailExpanded;
    private boolean isDescriptionExpanded;
    private int expandedPosition = -1;

    private boolean isInternet;
    private boolean isRoaming;

    public static final String OFFER_GROUP_MOBILE = "mobile";
    public static final String OFFER_GROUP_TAB = "tab";
    public static final String OFFER_GROUP_DESKTOP = "desktop";

    public static final String OFFER_CURRENCY = "manat";

    public static final String SUPPLEMENTARY_OFFER_HEADER_CALL_ICON = "calls";
    public static final String SUPPLEMENTARY_OFFER_HEADER_INTERNET_ICON = "internet";
    public static final String SUPPLEMENTARY_OFFER_HEADER_WHATSAPP_ICON = "whatsapp";
    public static final String SUPPLEMENTARY_OFFER_HEADER_PHONE_ICON = "phone";
    public static final String SUPPLEMENTARY_OFFER_HEADER_INTERNATIONAL_CALLS_ICON = "InternationalCalls";
    public static final String SUPPLEMENTARY_OFFER_HEADER_ROAMING_CALLS_ICON = "RoamingCalls";
    public static final String SUPPLEMENTARY_OFFER_HEADER_FACEBOOK_ICON = "Facebook";
    public static final String SUPPLEMENTARY_OFFER_HEADER_MMS_ICON = "MMS";
    public static final String SUPPLEMENTARY_OFFER_HEADER_YOUTUBE_ICON = "YouTube";
    public static final String SUPPLEMENTARY_OFFER_HEADER_INSTAGRAM_ICON = "Instagram";
    public static final String SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_ICON = "Campaign";
    public static final String SUPPLEMENTARY_OFFER_HEADER_TM_REQUIRED_ICON = "TMRequired";
    public static final String SUPPLEMENTARY_OFFER_HEADER_TM_PRESEENTED_ICON = "TMPresented";
    public static final String SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_DISTCOUNTED_CALLS_ICON = "CampaignDiscountedCalls";
    public static final String SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_DISTCOUNTED_INTERNET_ICON = "CampaignDiscountedInternet";
    public static final String SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_DISTCOUNTED_SMS_ICON = "CampaignDiscountedSMS";
    public static final String SUPPLEMENTARY_OFFER_HEADER_SMS_ICON = "SMS";

    private String CURRENT_OFFER_TYPE = "";

    // These are offers Types that can be come from API in type field
    private static final String OFFER_TYPE_INTERNET_EN = "Internet";
    private static final String OFFER_TYPE_INTERNET_RU = "Интернет";
    private static final String OFFER_TYPE_INTERNET_AZ = "İnternet";
    private static final String OFFER_TYPE_CALL_EN = "Call";
    private static final String OFFER_TYPE_CALL_RU = "Звонки";
    private static final String OFFER_TYPE_CALL_AZ = "Zənglər";
    private static final String OFFER_TYPE_SMS_EN = "SMS";
    private static final String OFFER_TYPE_SMS_RU = "SMS";
    private static final String OFFER_TYPE_SMS_AZ = "SMS";
    public static final String OFFER_TYPE_HYBRID_EN = "Hybrid";
    public static final String OFFER_TYPE_HYBRID_RU = "Hybrid";
    public static final String OFFER_TYPE_HYBRID_AZ = "Hybrid";
    private static final String OFFER_TYPE_TM_EN = "TM";
    private static final String OFFER_TYPE_TM_RU = "Особенные";
    private static final String OFFER_TYPE_TM_AZ = "Xüsusilər";
    private static final String OFFER_TYPE_CAMPAIGN_EN = "Campaign";
    private static final String OFFER_TYPE_CAMPAIGN_RU = "Кампании";
    private static final String OFFER_TYPE_CAMPAIGN_AZ = "Kampaniyalar";
    public static final String OFFER_TYPE_ROAMING_EN = "Roaming";
    public static final String OFFER_TYPE_ROAMING_RU = "Роуминг";
    public static final String OFFER_TYPE_ROAMING_AZ = "Rouminq";

    // TM With FR(Free Resource) Mapping
    private static final String TM_OFFER_WITH_FR_EN = "Get";
    private static final String TM_OFFER_WITH_FR_RU = "Предоставляется";
    private static final String TM_OFFER_WITH_FR_AZ = "Təqdim edilir";
    private Activity activity = null;

    public CardAdapterSupplementaryOffers(Context context, Activity activity, ArrayList<SupplementaryOffer> arrayList,
                                          SearchResultListener searchResultListener, boolean isCardView,
                                          boolean isInternet, boolean isRoaming, String offerType) {
        this.isInternet = isInternet;
        this.isRoaming = isRoaming;
        this.activity = activity;
        isDescriptionExpanded = false;
        isDetailExpanded = false;
        this.offerArrayList = arrayList;
        this.context = context;
        offerArrayListTemp = offerArrayList;
        this.searchResultListener = searchResultListener;
        this.isCardView = isCardView;
        this.CURRENT_OFFER_TYPE = offerType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_card_item_supplementary_offer, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        viewHolder.prepareHeaderItem(context, offerArrayList.get(i), i);

        viewHolder.prepareOfferDetail(context, offerArrayList.get(i));

        viewHolder.prepareOfferDescription(context, offerArrayList.get(i));


        // Offer Actions Views
        final int i2 = i;

        int rateStars = 0;
        viewHolder.button_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (offerArrayList.get(i2) != null) {
                    subscribeOffer(offerArrayList.get(i2), false);
                }
            }
        });
        viewHolder.button_renew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (offerArrayList.get(i2) != null) {
                    subscribeOffer(offerArrayList.get(i2), true);

                }
            }
        });

        if (offerArrayList.get(i) != null
                && offerArrayList.get(i).getHeader() != null
                && SubscribeUnsubscribe.checkoutIfSameOfferIsInSubscribe(context, RootValues.getInstance().getSubscriptionsMain(), offerArrayList.get(i).getHeader().getOfferingId(), "", false, this.CURRENT_OFFER_TYPE)) {
            viewHolder.button_subscribe.setText(context.getString(R.string.subscribed_lbl));
            viewHolder.button_subscribe.setSelected(true);
            viewHolder.button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
            viewHolder.button_subscribe.setEnabled(false);

            if (offerArrayList.get(i).getHeader().getBtnRenew().equalsIgnoreCase("1")) {
                viewHolder.button_renew.setVisibility(View.VISIBLE);
            } else {
                viewHolder.button_renew.setVisibility(View.GONE);
            }

            Data data = PrefUtils.getInAppSurvey(context).getData();
            if (data != null && data.getUserSurveys() != null) {
                List<UserSurveys> userSurveys = data.getUserSurveys();
                for (int j = 0; j < userSurveys.size(); j++) {
                    if (userSurveys.get(j).getOfferingId().equals(offerArrayList.get(i2).getHeader().getOfferingId())) {
                        if (userSurveys.get(j).getAnswer() != null) {
                            String surveyId = userSurveys.get(j).getSurveyId();
                            if (surveyId != null) {
                                Surveys survey = data.findSurveyById(Integer.parseInt(surveyId));
                                if (Tools.isNumeric(userSurveys.get(j).getAnswerId())) {
                                    if (Tools.isNumeric(userSurveys.get(j).getQuestionId())) {
                                        rateStars = setSupplementaryViewHolder(Integer.parseInt(userSurveys.get(j).getAnswerId()),
                                                Integer.parseInt(userSurveys.get(j).getQuestionId()), survey, viewHolder);
                                    }
                                }
                            }
                        } else {
                            rateStars = setSupplementaryViewHolder(0, 0, null, viewHolder);
                        }
                    }
                }
            }
            viewHolder.ratingStarsLayout.setVisibility(View.VISIBLE);

        } else {
            viewHolder.button_subscribe.setText(context.getString(R.string.subscribe_lbl));
            viewHolder.button_subscribe.setSelected(true);
            viewHolder.button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.black));
            viewHolder.button_subscribe.setEnabled(true);
            viewHolder.button_renew.setVisibility(View.GONE);

            viewHolder.ratingStarsLayout.setVisibility(View.GONE);
        }

        //button topup click
        viewHolder.buttonTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //redirect the user to the top up screen
                BaseActivity.startNewActivity(activity, TopUpActivity.class);
            }
        });

        ///////////

        int finalRating = rateStars;
        viewHolder.ratingStarsLayout.setOnClickListener(
                v -> {
                    inAppFeedback(context, offerArrayList.get(i2).getHeader().getOfferingId(), finalRating, i);
                }
        );


    }

    private int setSupplementaryViewHolder(int answerId, int questionId, Surveys survey, ViewHolder viewHolder) {
        if (survey != null) {
            Questions question = survey.findQuestion(questionId);
            if (question != null) {
                List<Answers> answers = question.getAnswers();
                if (answers != null) {
                    if (answers.size() > 0 && answers.get(0).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(false);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 1;
                    } else if (answers.size() > 1 && answers.get(1).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 2;
                    } else if (answers.size() > 2 && answers.get(2).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 3;
                    } else if (answers.size() > 3 && answers.get(3).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(false);
                        return 4;
                    } else if (answers.size() > 4 && answers.get(4).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(true);
                        return 5;
                    }

                } else {
                    viewHolder.starOne.setChecked(false);
                    viewHolder.starTwo.setChecked(false);
                    viewHolder.starThree.setChecked(false);
                    viewHolder.starFour.setChecked(false);
                    viewHolder.starFive.setChecked(false);
                    return 0;
                }
            } else {
                viewHolder.starOne.setChecked(false);
                viewHolder.starTwo.setChecked(false);
                viewHolder.starThree.setChecked(false);
                viewHolder.starFour.setChecked(false);
                viewHolder.starFive.setChecked(false);
                return 0;
            }
        } else {
            viewHolder.starOne.setChecked(false);
            viewHolder.starTwo.setChecked(false);
            viewHolder.starThree.setChecked(false);
            viewHolder.starFour.setChecked(false);
            viewHolder.starFive.setChecked(false);
            return 0;
        }
        return 0;
    }

    private void updateList(int position) {
        notifyItemChanged(position);
    }

    private void inAppFeedback(Context context, String tariffId, int rate, int position) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_BUNDLE_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.BUNDLE_PAGE);
            if (surveys != null) {
                InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                if (surveys.getQuestions() != null) {
                    currentVisit = 0;
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(new UpdateListOnSuccessfullySurvey() {
                        @Override
                        public void updateListOnSuccessfullySurvey() {
                            updateList(position);
                        }
                    });
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "2", tariffId, rate);
                }
            }
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_BUNDLE_PAGE, currentVisit);
    }

    private void subscribeOffer(SupplementaryOffer supplementaryOffer, boolean isRenew) {
        if (supplementaryOffer == null || supplementaryOffer.getHeader() == null || context == null)
            return;

        try {
            BakcellLogger.logE("offerXIdY", "offeringId:::" + supplementaryOffer.getHeader().getOfferingId(), "CardAdapter", "subscribeOffer");
            SubscribeUnsubscribe.requestSubscribeAndCheckLevelOffers(context
                    , supplementaryOffer.getHeader().getOfferingId()
                    , supplementaryOffer.getHeader().getOfferLevel()
                    , supplementaryOffer.getHeader().getOfferName()
                    , this.CURRENT_OFFER_TYPE, isRenew
            );
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    @Override
    public int getItemCount() {
        return offerArrayList.size();
    }

    public void updateAdapter(ArrayList<SupplementaryOffer> arrayList) {
        if (arrayList == null) return;
        offerArrayList = new ArrayList<>(arrayList);
        notifyDataSetChanged();
    }

    private SupplementaryFilter supplementaryFilter;

    @Override
    public Filter getFilter() {
        if (supplementaryFilter == null) {
            supplementaryFilter = new SupplementaryFilter();
        }
        return supplementaryFilter;

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout subscribeButtonHolder;
        BakcellButtonNormal buttonTopup;
        BakcellTextViewBold offer_title_txt;
        BakcellTextViewNormal offer_detail_title_txt;
        BakcellTextViewNormal offer_desc_title_txt;
        BakcellButtonNormal button_subscribe, button_renew;
        RelativeLayout title_content_layout;
        RelativeLayout detail_content_layout;
        RelativeLayout desc_content_layout;
        ExpandableLayout expandable_title_layout;
        ExpandableLayout expandable_detail_layout;
        ExpandableLayout expandable_desc_layout;
        ImageView detail_expand_icon;
        ImageView desc_expand_icon;
        ImageView icOfferGroupPhone, icOfferGroupTab, icOfferGroupDesktop;
        ConstraintLayout ratingStarsLayout;
        CheckBox starOne, starTwo, starThree, starFour, starFive;
        View dummyview;
        View title_belew_line;
        View detail_belew_line;
        View desc_belew_line;

        RelativeLayout offer_type_layout;
        BakcellTextViewNormal tv_offer_type_name, price_icon;
        View seperator_2;
        ImageView ic_call_type, ic_internet_type, ic_campaign_type;


        BakcellTextViewNormal tv_offer_group_name;
        BakcellTextViewNormal validity_lbl, validity_value, offer_banner_lbl, tv_price;

        LinearLayout offerAttributeMainLayout;

        RelativeLayout offer_group_layout;
        View seperator_1;

        LinearLayout detailHeaderView, descriptionHeaderView;

        // Offer Details
        View detailMainLayout;
        OfferDetailContents offerDetailContents = new OfferDetailContents();
        // Offer Description
        View descriptionMainLayout;
        OfferDescriptionContents offerDescriptionContents = new OfferDescriptionContents();

        class OfferDetailContents {
            // Offer Details content start
            // priceSection
            RelativeLayout priceSection, priceTitleLayout;
            ImageView priceTitleIcon;
            BakcellTextViewNormal detailPriceTitle, detailPriceValue, detailPriceDescription;
            LinearLayout offerDetailAttributeLayout;
            View price_below_line;

            // roundingSection
            RelativeLayout roundingSection;
            LinearLayout roundingTitleLayout;
            ImageView roundingTitleIcon;
            BakcellTextViewNormal roundingTitleText, roundingTitleValue, detailRoundingDescription;
            LinearLayout offerDetailroundingAttributeLayout;
            View rounding_below_line;

            // TextWithTitleSection
            RelativeLayout textWithTitleSection;
            BakcellTextViewNormal text_title, decription_text;
            View textWithTitle_below_line;

            //textWithOutTitleSection
            RelativeLayout textWithOutTitleSection;
            BakcellTextViewNormal textWithOutTitle_text;
            View textWithOutTitle_below_line;

            // textWithPointsSections
            RelativeLayout textWithPointsSections;
            LinearLayout textWithPointsLayout;
            View textWithPoints_below_line;

            //titleSubTitleValueDescSection
            RelativeLayout titleSubTitleValueDescSection, titleSubTitleValueDescTitleLayout;
            BakcellTextViewNormal titleSubTitleValueDescTitle;
            LinearLayout titleSubTitleValueDescAttributeLayout;
            View textSubTitleDesc_below_line;

            //dateSection
            RelativeLayout dateSection;
            LinearLayout dateLayout;
            BakcellTextViewNormal dateDescription, dateFromLabel, dateFromValue, dateToLabel, dateToValue;
            View date_below_line;

            //timeSection
            RelativeLayout timeSection;
            LinearLayout timeLayout;
            BakcellTextViewNormal timeDescription, timefromLabel, timefromValue, timetoLabel, timetoValue;
            View time_below_line;

            //roamingSection
            RelativeLayout roamingSection;
            BakcellTextViewNormal roamingDescriptionAbove, roamingDescriptionBelow;
            LinearLayout roamingCountryLayout;
            View roaming_below_line;

            // freeResourceValiditySection
            RelativeLayout freeResourceValiditySection, freeResourceTitleLayout;
            LinearLayout freeResourceOnnetLayout;
            BakcellTextViewNormal freeResourceTitle, freeResourceValue, onnetTitle, onnetValue, freeResourceDescription;
            BakcellTextViewNormal priceIcon;
            // Offer Details content end
        }

        class OfferDescriptionContents {
            // Offer Description content start
            // priceSection
            RelativeLayout priceSection, priceTitleLayout;
            ImageView priceTitleIcon;
            BakcellTextViewNormal detailPriceTitle, detailPriceValue, detailPriceDescription;
            LinearLayout offerDetailAttributeLayout;
            View price_below_line;

            // roundingSection
            RelativeLayout roundingSection;
            LinearLayout roundingTitleLayout;
            ImageView roundingTitleIcon;
            BakcellTextViewNormal roundingTitleText, roundingTitleValue, detailRoundingDescription, priceIcon;
            LinearLayout offerDetailroundingAttributeLayout;
            View rounding_below_line;

            // TextWithTitleSection
            RelativeLayout textWithTitleSection;
            BakcellTextViewNormal text_title, decription_text;
            View textWithTitle_below_line;

            //textWithOutTitleSection
            RelativeLayout textWithOutTitleSection;
            BakcellTextViewNormal textWithOutTitle_text;
            View textWithOutTitle_below_line;

            // textWithPointsSections
            RelativeLayout textWithPointsSections;
            LinearLayout textWithPointsLayout;
            View textWithPoints_below_line;

            //titleSubTitleValueDescSection
            RelativeLayout titleSubTitleValueDescSection, titleSubTitleValueDescTitleLayout;
            BakcellTextViewNormal titleSubTitleValueDescTitle;
            LinearLayout titleSubTitleValueDescAttributeLayout;
            View textSubTitleDesc_below_line;

            //dateSection
            RelativeLayout dateSection;
            LinearLayout dateLayout;
            BakcellTextViewNormal dateDescription, dateFromLabel, dateFromValue, dateToLabel, dateToValue;
            View date_below_line;

            //timeSection
            RelativeLayout timeSection;
            LinearLayout timeLayout;
            BakcellTextViewNormal timeDescription, timefromLabel, timefromValue, timetoLabel, timetoValue;
            View time_below_line;

            //roamingSection
            RelativeLayout roamingSection;
            BakcellTextViewNormal roamingDescriptionAbove, roamingDescriptionBelow;
            LinearLayout roamingCountryLayout;
            View roaming_below_line;

            // freeResourceValiditySection
            RelativeLayout freeResourceValiditySection, freeResourceTitleLayout;
            LinearLayout freeResourceOnnetLayout;
            BakcellTextViewNormal freeResourceTitle, freeResourceValue, onnetTitle, onnetValue, freeResourceDescription;
            // Offer Description content end
        }

        public ViewHolder(View itemView) {
            super(itemView);
            if (isCardView) {
                int width = (int) Tools.convertDpToPixel(RootValues.getInstance().getTARIFFS_WIDTH(), itemView.getContext());
                itemView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
            } else {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) itemView.getLayoutParams();
                Resources r = context.getResources();
                int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                        25, r.getDisplayMetrics());
                layoutParams.setMargins(margin, 0, margin, 0);
                itemView.setLayoutParams(layoutParams);
            }
            subscribeButtonHolder = itemView.findViewById(R.id.subscribeButtonHolder);
            buttonTopup = itemView.findViewById(R.id.buttonTopup);
            tv_price = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_price);
            price_icon = itemView.findViewById(R.id.price_icon);
            offer_title_txt = (BakcellTextViewBold) itemView.findViewById(R.id.offer_title_txt);
            offer_detail_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.offer_detail_title_txt);
            offer_desc_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.offer_desc_title_txt);
            button_subscribe = (BakcellButtonNormal) itemView.findViewById(R.id.button_subscribe);
            button_renew = (BakcellButtonNormal) itemView.findViewById(R.id.button_renew);
            title_content_layout = (RelativeLayout) itemView.findViewById(R.id.title_content_layout);
            detail_content_layout = (RelativeLayout) itemView.findViewById(R.id.detail_header_layout);
            desc_content_layout = (RelativeLayout) itemView.findViewById(R.id.detail_desc_layout);
            expandable_title_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_title_layout);
            expandable_detail_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_detail_layout);
            expandable_desc_layout = (ExpandableLayout) itemView.findViewById(R.id.expandable_desc_layout);
            detail_expand_icon = (ImageView) itemView.findViewById(R.id.detail_expand_icon);
            desc_expand_icon = (ImageView) itemView.findViewById(R.id.desc_expand_icon);
            dummyview = itemView.findViewById(R.id.dummy_view);
            seperator_1 = (View) itemView.findViewById(R.id.seperator_1);
            title_belew_line = itemView.findViewById(R.id.title_belew_line);
            detail_belew_line = itemView.findViewById(R.id.detail_belew_line);
            desc_belew_line = itemView.findViewById(R.id.desc_belew_line);

            detailHeaderView = (LinearLayout) itemView.findViewById(R.id.second_layout);
            descriptionHeaderView = (LinearLayout) itemView.findViewById(R.id.third_layout);

            // Offer Type
            offer_type_layout = (RelativeLayout) itemView.findViewById(R.id.offer_type_layout);
            tv_offer_type_name = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_offer_type_name);
            ic_call_type = (ImageView) itemView.findViewById(R.id.ic_call_type);
            ic_internet_type = (ImageView) itemView.findViewById(R.id.ic_internet_type);
            ic_campaign_type = (ImageView) itemView.findViewById(R.id.ic_campaign_type);
            seperator_2 = itemView.findViewById(R.id.seperator_2);


            // Rating Stars
            ratingStarsLayout = itemView.findViewById(R.id.ratingStarsLayout);
            starOne = itemView.findViewById(R.id.star1);
            starTwo = itemView.findViewById(R.id.star2);
            starThree = itemView.findViewById(R.id.star3);
            starFour = itemView.findViewById(R.id.star4);
            starFive = itemView.findViewById(R.id.star5);

            //Offer group icons
            icOfferGroupPhone = (ImageView) itemView.findViewById(R.id.ic_phone);
            icOfferGroupTab = (ImageView) itemView.findViewById(R.id.ic_tab);
            icOfferGroupDesktop = (ImageView) itemView.findViewById(R.id.ic_tv);

            // Group
            offer_group_layout = (RelativeLayout) itemView.findViewById(R.id.offer_group_layout);
            tv_offer_group_name = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_offer_group_name);

            // Validity
            validity_lbl = (BakcellTextViewNormal) itemView.findViewById(R.id.validity_lbl);
            validity_value = (BakcellTextViewNormal) itemView.findViewById(R.id.validity_value);

            // Stiker
            offer_banner_lbl = (BakcellTextViewNormal) itemView.findViewById(R.id.offer_banner_lbl);

            // Attributes
            offerAttributeMainLayout = (LinearLayout) itemView.findViewById(R.id.offerAttributeMainLayout);

            // Offer Details
            ViewStub detailViewStub = (ViewStub) itemView.findViewById(R.id.detailLayout);
            detailViewStub.setLayoutResource(R.layout.offer_detail_layout);
            detailMainLayout = detailViewStub.inflate();
            offerDetailLayout(offerDetailContents, detailMainLayout);

            // Offer Description
            ViewStub descriptionViewStub = (ViewStub) itemView.findViewById(R.id.descriptionLayout);
            descriptionViewStub.setLayoutResource(R.layout.offer_detail_layout);
            descriptionMainLayout = descriptionViewStub.inflate();
            offerDescriptionLayout(offerDescriptionContents, descriptionMainLayout);

        }

        private void offerDetailLayout(OfferDetailContents offerDetailContents,
                                       View detailView) {

            // priceSection
            offerDetailContents.priceSection = (RelativeLayout) detailView.findViewById(R.id.priceSection);
            offerDetailContents.priceTitleLayout = (RelativeLayout) detailView.findViewById(R.id.priceTitleLayout);
            offerDetailContents.priceTitleIcon = (ImageView) detailView.findViewById(R.id.priceTitleIcon);
            offerDetailContents.detailPriceTitle = (BakcellTextViewNormal) detailView.findViewById(R.id.detailPriceTitle);
            offerDetailContents.detailPriceValue = (BakcellTextViewNormal) detailView.findViewById(R.id.detailPriceValue);
            offerDetailContents.detailPriceDescription = (BakcellTextViewNormal) detailView.findViewById(R.id.detailPriceDescription);
            offerDetailContents.detailPriceDescription = (BakcellTextViewNormal) detailView.findViewById(R.id.detailPriceDescription);
            offerDetailContents.priceIcon = (BakcellTextViewNormal) detailView.findViewById(R.id.priceIconP);
            offerDetailContents.offerDetailAttributeLayout = (LinearLayout) detailView.findViewById(R.id.offerDetailAttributeLayout);
            offerDetailContents.price_below_line = (View) detailView.findViewById(R.id.price_below_line);

            // roundingSection
            offerDetailContents.roundingSection = (RelativeLayout) detailView.findViewById(R.id.roundingSection);
            offerDetailContents.roundingTitleLayout = (LinearLayout) detailView.findViewById(R.id.roundingTitleLayout);
            offerDetailContents.roundingTitleIcon = (ImageView) detailView.findViewById(R.id.roundingTitleIcon);
            offerDetailContents.roundingTitleText = (BakcellTextViewNormal) detailView.findViewById(R.id.roundingTitleText);
            offerDetailContents.roundingTitleValue = (BakcellTextViewNormal) detailView.findViewById(R.id.roundingTitleValue);
            offerDetailContents.detailRoundingDescription = (BakcellTextViewNormal) detailView.findViewById(R.id.detailRoundingDescription);
            offerDetailContents.offerDetailroundingAttributeLayout = (LinearLayout) detailView.findViewById(R.id.offerDetailroundingAttributeLayout);
            offerDetailContents.rounding_below_line = (View) detailView.findViewById(R.id.rounding_below_line);

            // TextWithTitleSection
            offerDetailContents.textWithTitleSection = (RelativeLayout) detailView.findViewById(R.id.textWithTitleSection);
            offerDetailContents.text_title = (BakcellTextViewNormal) detailView.findViewById(R.id.text_title);
            offerDetailContents.decription_text = (BakcellTextViewNormal) detailView.findViewById(R.id.decription_text);
            offerDetailContents.textWithTitle_below_line = (View) detailView.findViewById(R.id.textWithTitle_below_line);

            //textWithOutTitleSection
            offerDetailContents.textWithOutTitleSection = (RelativeLayout) detailView.findViewById(R.id.textWithOutTitleSection);
            offerDetailContents.textWithOutTitle_text = (BakcellTextViewNormal) detailView.findViewById(R.id.textWithOutTitle_text);
            offerDetailContents.textWithOutTitle_below_line = (View) detailView.findViewById(R.id.textWithOutTitle_below_line);

            // textWithPointsSections
            offerDetailContents.textWithPointsSections = (RelativeLayout) detailView.findViewById(R.id.textWithPointsSections);
            offerDetailContents.textWithPointsLayout = (LinearLayout) detailView.findViewById(R.id.textWithPointsLayout);
            offerDetailContents.textWithPoints_below_line = (View) detailView.findViewById(R.id.textWithPoints_below_line);

            //titleSubTitleValueDescSection
            offerDetailContents.titleSubTitleValueDescSection = (RelativeLayout) detailView.findViewById(R.id.titleSubTitleValueDescSection);
            offerDetailContents.titleSubTitleValueDescTitleLayout = (RelativeLayout) detailView.findViewById(R.id.titleSubTitleValueDescTitleLayout);
            offerDetailContents.titleSubTitleValueDescTitle = (BakcellTextViewNormal) detailView.findViewById(R.id.titleSubTitleValueDescTitle);
            offerDetailContents.titleSubTitleValueDescAttributeLayout = (LinearLayout) detailView.findViewById(R.id.titleSubTitleValueDescAttributeLayout);
            offerDetailContents.textSubTitleDesc_below_line = (View) detailView.findViewById(R.id.textSubTitleDesc_below_line);

            //dateSection
            offerDetailContents.dateSection = (RelativeLayout) detailView.findViewById(R.id.dateSection);
            offerDetailContents.dateLayout = (LinearLayout) detailView.findViewById(R.id.dateLayout);
            offerDetailContents.dateDescription = (BakcellTextViewNormal) detailView.findViewById(R.id.dateDescription);
            offerDetailContents.dateFromLabel = (BakcellTextViewNormal) detailView.findViewById(R.id.dateFromLabel);
            offerDetailContents.dateFromValue = (BakcellTextViewNormal) detailView.findViewById(R.id.dateFromValue);
            offerDetailContents.dateToLabel = (BakcellTextViewNormal) detailView.findViewById(R.id.dateToLabel);
            offerDetailContents.dateToValue = (BakcellTextViewNormal) detailView.findViewById(R.id.dateToValue);
            offerDetailContents.date_below_line = (View) detailView.findViewById(R.id.date_below_line);

            //timeSection
            offerDetailContents.timeSection = (RelativeLayout) detailView.findViewById(R.id.timeSection);
            offerDetailContents.timeLayout = (LinearLayout) detailView.findViewById(R.id.timeLayout);
            offerDetailContents.timeDescription = (BakcellTextViewNormal) detailView.findViewById(R.id.timeDescription);
            offerDetailContents.timefromLabel = (BakcellTextViewNormal) detailView.findViewById(R.id.timefromLabel);
            offerDetailContents.timefromValue = (BakcellTextViewNormal) detailView.findViewById(R.id.timefromValue);
            offerDetailContents.timetoLabel = (BakcellTextViewNormal) detailView.findViewById(R.id.timetoLabel);
            offerDetailContents.timetoValue = (BakcellTextViewNormal) detailView.findViewById(R.id.timetoValue);
            offerDetailContents.time_below_line = (View) detailView.findViewById(R.id.time_below_line);

            //roamingSection
            offerDetailContents.roamingSection = (RelativeLayout) detailView.findViewById(R.id.roamingSection);
            offerDetailContents.roamingDescriptionAbove = (BakcellTextViewNormal) detailView.findViewById(R.id.roamingDescriptionAbove);
            offerDetailContents.roamingDescriptionBelow = (BakcellTextViewNormal) detailView.findViewById(R.id.roamingDescriptionBelow);
            offerDetailContents.roamingCountryLayout = (LinearLayout) detailView.findViewById(R.id.roamingCountryLayout);
            offerDetailContents.roaming_below_line = (View) detailView.findViewById(R.id.roaming_below_line);

            // freeResourceValiditySection
            offerDetailContents.freeResourceValiditySection = (RelativeLayout) detailView.findViewById(R.id.freeResourceValiditySection);
            offerDetailContents.freeResourceTitleLayout = (RelativeLayout) detailView.findViewById(R.id.freeResourceTitleLayout);
            offerDetailContents.freeResourceOnnetLayout = (LinearLayout) detailView.findViewById(R.id.freeResourceOnnetLayout);
            offerDetailContents.freeResourceTitle = (BakcellTextViewNormal) detailView.findViewById(R.id.freeResourceTitle);
            offerDetailContents.freeResourceValue = (BakcellTextViewNormal) detailView.findViewById(R.id.freeResourceValue);
            offerDetailContents.onnetTitle = (BakcellTextViewNormal) detailView.findViewById(R.id.onnetTitle);
            offerDetailContents.onnetValue = (BakcellTextViewNormal) detailView.findViewById(R.id.onnetValue);
            offerDetailContents.freeResourceDescription = (BakcellTextViewNormal) detailView.findViewById(R.id.freeResourceDescription);


        }

        private void offerDescriptionLayout(OfferDescriptionContents offerDescriptionContents,
                                            View descriptionView) {

            // priceSection
            offerDescriptionContents.priceSection = (RelativeLayout) descriptionView.findViewById(R.id.priceSection);
            offerDescriptionContents.priceTitleLayout = (RelativeLayout) descriptionView.findViewById(R.id.priceTitleLayout);
            offerDescriptionContents.priceTitleIcon = (ImageView) descriptionView.findViewById(R.id.priceTitleIcon);
            offerDescriptionContents.detailPriceTitle = (BakcellTextViewNormal) descriptionView.findViewById(R.id.detailPriceTitle);
            offerDescriptionContents.detailPriceValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.detailPriceValue);
            offerDescriptionContents.detailPriceDescription = (BakcellTextViewNormal) descriptionView.findViewById(R.id.detailPriceDescription);
            offerDescriptionContents.priceIcon = (BakcellTextViewNormal) descriptionView.findViewById(R.id.priceIconP);
            offerDescriptionContents.offerDetailAttributeLayout = (LinearLayout) descriptionView.findViewById(R.id.offerDetailAttributeLayout);
            offerDescriptionContents.price_below_line = (View) descriptionView.findViewById(R.id.price_below_line);

            // roundingSection
            offerDescriptionContents.roundingSection = (RelativeLayout) descriptionView.findViewById(R.id.roundingSection);
            offerDescriptionContents.roundingTitleLayout = (LinearLayout) descriptionView.findViewById(R.id.roundingTitleLayout);
            offerDescriptionContents.roundingTitleIcon = (ImageView) descriptionView.findViewById(R.id.roundingTitleIcon);
            offerDescriptionContents.roundingTitleText = (BakcellTextViewNormal) descriptionView.findViewById(R.id.roundingTitleText);
            offerDescriptionContents.roundingTitleValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.roundingTitleValue);
            offerDescriptionContents.detailRoundingDescription = (BakcellTextViewNormal) descriptionView.findViewById(R.id.detailRoundingDescription);
            offerDescriptionContents.offerDetailroundingAttributeLayout = (LinearLayout) descriptionView.findViewById(R.id.offerDetailroundingAttributeLayout);
            offerDescriptionContents.rounding_below_line = (View) descriptionView.findViewById(R.id.rounding_below_line);

            // TextWithTitleSection
            offerDescriptionContents.textWithTitleSection = (RelativeLayout) descriptionView.findViewById(R.id.textWithTitleSection);
            offerDescriptionContents.text_title = (BakcellTextViewNormal) descriptionView.findViewById(R.id.text_title);
            offerDescriptionContents.decription_text = (BakcellTextViewNormal) descriptionView.findViewById(R.id.decription_text);
            offerDescriptionContents.textWithTitle_below_line = (View) descriptionView.findViewById(R.id.textWithTitle_below_line);

            //textWithOutTitleSection
            offerDescriptionContents.textWithOutTitleSection = (RelativeLayout) descriptionView.findViewById(R.id.textWithOutTitleSection);
            offerDescriptionContents.textWithOutTitle_text = (BakcellTextViewNormal) descriptionView.findViewById(R.id.textWithOutTitle_text);
            offerDescriptionContents.textWithOutTitle_below_line = (View) descriptionView.findViewById(R.id.textWithOutTitle_below_line);

            // textWithPointsSections
            offerDescriptionContents.textWithPointsSections = (RelativeLayout) descriptionView.findViewById(R.id.textWithPointsSections);
            offerDescriptionContents.textWithPointsLayout = (LinearLayout) descriptionView.findViewById(R.id.textWithPointsLayout);
            offerDescriptionContents.textWithPoints_below_line = (View) descriptionView.findViewById(R.id.textWithPoints_below_line);

            //titleSubTitleValueDescSection
            offerDescriptionContents.titleSubTitleValueDescSection = (RelativeLayout) descriptionView.findViewById(R.id.titleSubTitleValueDescSection);
            offerDescriptionContents.titleSubTitleValueDescTitleLayout = (RelativeLayout) descriptionView.findViewById(R.id.titleSubTitleValueDescTitleLayout);
            offerDescriptionContents.titleSubTitleValueDescTitle = (BakcellTextViewNormal) descriptionView.findViewById(R.id.titleSubTitleValueDescTitle);
            offerDescriptionContents.titleSubTitleValueDescAttributeLayout = (LinearLayout) descriptionView.findViewById(R.id.titleSubTitleValueDescAttributeLayout);
            offerDescriptionContents.textSubTitleDesc_below_line = (View) descriptionView.findViewById(R.id.textSubTitleDesc_below_line);

            //dateSection
            offerDescriptionContents.dateSection = (RelativeLayout) descriptionView.findViewById(R.id.dateSection);
            offerDescriptionContents.dateLayout = (LinearLayout) descriptionView.findViewById(R.id.dateLayout);
            offerDescriptionContents.dateDescription = (BakcellTextViewNormal) descriptionView.findViewById(R.id.dateDescription);
            offerDescriptionContents.dateFromLabel = (BakcellTextViewNormal) descriptionView.findViewById(R.id.dateFromLabel);
            offerDescriptionContents.dateFromValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.dateFromValue);
            offerDescriptionContents.dateToLabel = (BakcellTextViewNormal) descriptionView.findViewById(R.id.dateToLabel);
            offerDescriptionContents.dateToValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.dateToValue);
            offerDescriptionContents.date_below_line = (View) descriptionView.findViewById(R.id.date_below_line);

            //timeSection
            offerDescriptionContents.timeSection = (RelativeLayout) descriptionView.findViewById(R.id.timeSection);
            offerDescriptionContents.timeLayout = (LinearLayout) descriptionView.findViewById(R.id.timeLayout);
            offerDescriptionContents.timeDescription = (BakcellTextViewNormal) descriptionView.findViewById(R.id.timeDescription);
            offerDescriptionContents.timefromLabel = (BakcellTextViewNormal) descriptionView.findViewById(R.id.timefromLabel);
            offerDescriptionContents.timefromValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.timefromValue);
            offerDescriptionContents.timetoLabel = (BakcellTextViewNormal) descriptionView.findViewById(R.id.timetoLabel);
            offerDescriptionContents.timetoValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.timetoValue);
            offerDescriptionContents.time_below_line = (View) descriptionView.findViewById(R.id.time_below_line);

            //roamingSection
            offerDescriptionContents.roamingSection = (RelativeLayout) descriptionView.findViewById(R.id.roamingSection);
            offerDescriptionContents.roamingDescriptionAbove = (BakcellTextViewNormal) descriptionView.findViewById(R.id.roamingDescriptionAbove);
            offerDescriptionContents.roamingDescriptionBelow = (BakcellTextViewNormal) descriptionView.findViewById(R.id.roamingDescriptionBelow);
            offerDescriptionContents.roamingCountryLayout = (LinearLayout) descriptionView.findViewById(R.id.roamingCountryLayout);
            offerDescriptionContents.roaming_below_line = (View) descriptionView.findViewById(R.id.roaming_below_line);

            // freeResourceValiditySection
            offerDescriptionContents.freeResourceValiditySection = (RelativeLayout) descriptionView.findViewById(R.id.freeResourceValiditySection);
            offerDescriptionContents.freeResourceTitleLayout = (RelativeLayout) descriptionView.findViewById(R.id.freeResourceTitleLayout);
            offerDescriptionContents.freeResourceOnnetLayout = (LinearLayout) descriptionView.findViewById(R.id.freeResourceOnnetLayout);
            offerDescriptionContents.freeResourceTitle = (BakcellTextViewNormal) descriptionView.findViewById(R.id.freeResourceTitle);
            offerDescriptionContents.freeResourceValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.freeResourceValue);
            offerDescriptionContents.onnetTitle = (BakcellTextViewNormal) descriptionView.findViewById(R.id.onnetTitle);
            offerDescriptionContents.onnetValue = (BakcellTextViewNormal) descriptionView.findViewById(R.id.onnetValue);
            offerDescriptionContents.freeResourceDescription = (BakcellTextViewNormal) descriptionView.findViewById(R.id.freeResourceDescription);


        }

        private void prepareAnimations(final Context context, final SupplementaryOffer offers, final int i) {
            detail_content_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isCardView) {
                        if (isDetailExpanded) {
                            isDetailExpanded = false;
                        } else {
                            isDetailExpanded = true;
                        }
                        isDescriptionExpanded = false;

                        if (isCardView) {
                            notifyDataSetChanged();
                        } else {
                            applyAnimations(context, offers);
                        }
                    } else {
                        expandedPosition = i;
                        if (!expandable_detail_layout.isExpanded()) {
                            isDetailExpanded = true;
                        } else {
                            isDetailExpanded = false;
                        }
                        isDescriptionExpanded = false;

                        notifyDataSetChanged();
                    }
                }
            });

            desc_content_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isCardView) {
                        if (isDescriptionExpanded) {
                            isDescriptionExpanded = false;
                        } else {
                            isDescriptionExpanded = true;
                        }
                        isDetailExpanded = false;

                        if (isCardView) {
                            notifyDataSetChanged();
                        } else {
                            applyAnimations(context, offers);
                        }
                    } else {
                        expandedPosition = i;
                        if (!expandable_desc_layout.isExpanded()) {
                            isDescriptionExpanded = true;
                        } else {
                            isDescriptionExpanded = false;
                        }
                        isDetailExpanded = false;

                        notifyDataSetChanged();
                    }

                }
            });

            if (isCardView) {
                if (isCardView) {
                    applyAnimations(context, offers);
                } else {
                    // Header section expand
                    expandable_title_layout.setExpanded(true);
                    expandable_desc_layout.setExpanded(false);
                    expandable_detail_layout.setExpanded(false);
                    detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                    desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                }

            } else {

                // Apply Animation on cards
                if (expandedPosition == i) {
                    if (isDetailExpanded) {
                        expandable_title_layout.setExpanded(false);
                        expandable_detail_layout.setExpanded(true);
                        expandable_desc_layout.setExpanded(false);
                        detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
                        desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                    } else if (isDescriptionExpanded) {
                        expandable_title_layout.setExpanded(false);
                        expandable_detail_layout.setExpanded(false);
                        expandable_desc_layout.setExpanded(true);
                        detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                        desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
                    } else {
                        expandable_title_layout.setExpanded(true);
                        expandable_detail_layout.setExpanded(false);
                        expandable_desc_layout.setExpanded(false);
                        detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                        desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                    }
                } else {
                    expandable_title_layout.setExpanded(true);
                    expandable_detail_layout.setExpanded(false);
                    expandable_desc_layout.setExpanded(false);
                    detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                    desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                }

            }
        }

        private void applyAnimations(Context context, SupplementaryOffer offers) {
            if (isDetailExpanded) {
                // Detail section expand
                if (offers != null && offers.getDetails() != null) {

                } else {
                    return;
                }
                expandable_detail_layout.setExpanded(true);
                expandable_desc_layout.setExpanded(false);
                expandable_title_layout.setExpanded(false);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
                desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
            } else if (isDescriptionExpanded) {
                // description section expand
                if (offers != null && offers.getDescription() != null) {

                } else {
                    return;
                }
                expandable_desc_layout.setExpanded(true);
                expandable_detail_layout.setExpanded(false);
                expandable_title_layout.setExpanded(false);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_minus));
            } else {
                // Header section expand
                expandable_title_layout.setExpanded(true);
                expandable_desc_layout.setExpanded(false);
                expandable_detail_layout.setExpanded(false);
                detail_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
                desc_expand_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_plus));
            }
        }

        public void prepareHeaderItem(final Context context, final SupplementaryOffer offers, final int i) {

            // check for the istopup
            if (offers != null && offers.getHeader() != null && Tools.hasValue(offers.getHeader().getIsTopUp())) {
                //istopup has some value

                if (offers.getHeader().getIsTopUp().equalsIgnoreCase("1")) {
                    subscribeButtonHolder.setVisibility(View.GONE);
                    buttonTopup.setVisibility(View.VISIBLE);
                } else {
                    subscribeButtonHolder.setVisibility(View.VISIBLE);
                    buttonTopup.setVisibility(View.GONE);
                }
            } else {
                //istopup has no value
                subscribeButtonHolder.setVisibility(View.VISIBLE);
                buttonTopup.setVisibility(View.GONE);
            }

            if (isRoaming) {
                // Offer Type in Roaming case
                if (offers != null && offers.getHeader() != null && Tools.hasValue(offers.getHeader().getType())) {
                    offer_type_layout.setVisibility(View.VISIBLE);
                    seperator_1.setVisibility(View.VISIBLE);
                    tv_offer_type_name.setText(offers.getHeader().getType());

                    if (offers != null && offers.getHeader() != null) {
                        offerTypeInRoamingIconsMapping(context, offers.getHeader().getType(), ic_call_type, ic_internet_type, ic_campaign_type, tv_offer_type_name);
                    }

                } else {
                    offer_type_layout.setVisibility(View.GONE);
                    seperator_1.setVisibility(View.GONE);
                }
            } else {
                offer_type_layout.setVisibility(View.GONE);
                seperator_2.setVisibility(View.GONE);
            }

            // Set Values to UI
            if (offers != null && offers.getHeader() != null && Tools.hasValue(offers.getHeader().getOfferName())) {
                offer_title_txt.setText(offers.getHeader().getOfferName());
                offer_title_txt.setSelected(true);
            } else {
                offer_title_txt.setText("");
            }

            if (offers != null && offers.getHeader() != null && Tools.hasValue(offers.getHeader().getPrice())) {
                if (Tools.isNumeric(offers.getHeader().getPrice())) {
                    tv_price.setText(offers.getHeader().getPrice());
                } else {
                    tv_price.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    price_icon.setVisibility(View.GONE);
                }
            } else {
                tv_price.setText("");
            }

            // Validity
            if (offers != null && offers.getHeader() != null && Tools.hasValue(offers.getHeader().getValidityTitle())) {
                validity_lbl.setText(offers.getHeader().getValidityTitle() + ":");
            } else {
                validity_lbl.setText("");
            }
            if (offers != null && offers.getHeader() != null && Tools.hasValue(offers.getHeader().getValidityValue())) {
                validity_value.setText(" " + offers.getHeader().getValidityValue());
            } else {
                validity_value.setText("");
            }

            // Stiker
            if (offers != null && offers.getHeader() != null && Tools.hasValue(offers.getHeader().getStickerLabel())) {
                offer_banner_lbl.setVisibility(View.VISIBLE);
                offer_banner_lbl.setText(offers.getHeader().getStickerLabel());
                if (Tools.hasValue(offers.getHeader().getStickerColorCode())) {
                    offer_banner_lbl.setTextColor(ContextCompat.getColor(context, R.color.white));
                    Drawable background = offer_banner_lbl.getBackground();
                    if (background instanceof ShapeDrawable) {
                        ((ShapeDrawable) background).getPaint().setColor(Color.parseColor(offers.getHeader().getStickerColorCode()));
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable) background).setColor(Color.parseColor(offers.getHeader().getStickerColorCode()));
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable) background).setColor(Color.parseColor(offers.getHeader().getStickerColorCode()));
                    }
                } else {
                    offer_banner_lbl.setTextColor(ContextCompat.getColor(context, R.color.text_gray));
                    Drawable background = offer_banner_lbl.getBackground();
                    if (background instanceof ShapeDrawable) {
                        ((ShapeDrawable) background).getPaint().setColor(ContextCompat
                                .getColor(context, R.color.light_gray));
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable) background).setColor(ContextCompat
                                .getColor(context, R.color.light_gray));
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable) background).setColor(ContextCompat
                                .getColor(context, R.color.light_gray));
                    }
                }
            } else {
                offer_banner_lbl.setVisibility(View.GONE);
            }


            // Group
            if (offers != null && offers.getHeader() != null && offers.getHeader().getOfferGroup() != null) {
                if (isInternet) {
                    seperator_2.setVisibility(View.VISIBLE);
                    offer_group_layout.setVisibility(View.VISIBLE);
                } else {
                    seperator_2.setVisibility(View.GONE);
                    offer_group_layout.setVisibility(View.GONE);
                }
                if (Tools.hasValue(offers.getHeader().getOfferGroup().getGroupName())) {
                    tv_offer_group_name.setText(offers.getHeader().getOfferGroup().getGroupName());
                    tv_offer_group_name.setSelected(true);
                } else {
                    tv_offer_group_name.setText("");
                }

                //Offer group mapping
                if (offers != null && offers.getHeader() != null && offers.getHeader().getOfferGroup() != null) {
                    offerGroupInInternetAndRoamingIconsMapping(context, offers.getHeader().getOfferGroup().getGroupValue(), icOfferGroupPhone, icOfferGroupTab, icOfferGroupDesktop);
                }

            } else {
                seperator_2.setVisibility(View.GONE);
                offer_group_layout.setVisibility(View.GONE);
            }

            // Offer Attributes

            if (offers != null && offers.getHeader() != null && offers.getHeader().getAttributeList() != null && offers.getHeader().getAttributeList().size() > 0) {
                offerAttributeMainLayout.setVisibility(View.VISIBLE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        offerAttributeMainLayout.removeAllViews();
                        for (int j = 0; j < offers.getHeader().getAttributeList().size(); j++) {

                            //dynamically creating header attributes here.!
                            ViewItemOfferAttribute attribute = new ViewItemOfferAttribute(context);

                            if (j == 0) {
                                attribute.getLineTopLine().setVisibility(View.GONE);
                            } else {
                                attribute.getLineTopLine().setVisibility(View.VISIBLE);
                            }

                            if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getIconMap())) {
                                Tools.setOffersSubscriptionsIcons(context, offers.getHeader().getAttributeList().get(j).getIconMap().trim(), attribute.getIcon_clock());
                            }

                            if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getTitle())) {
                                attribute.getAttributeTitle().setText(offers.getHeader().getAttributeList().get(j).getTitle());
                            } else {
                                attribute.getAttributeTitle().setText("");
                            }

                            if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getDescription())) {
                                attribute.getLineTextAttribute().setVisibility(View.VISIBLE);
                                attribute.getAttribute_description().setVisibility(View.VISIBLE);
                                attribute.getAttribute_description().setText(offers.getHeader().getAttributeList().get(j).getDescription());
                            } else {
                                attribute.getAttribute_description().setVisibility(View.GONE);
                                attribute.getLineTextAttribute().setVisibility(View.INVISIBLE);
                            }

                            String attributeValue = "";
                            if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getValue())) {
                                attributeValue = offers.getHeader().getAttributeList().get(j).getValue();
                                if (Tools.isValueRed(attributeValue)) {
                                    attribute.getAttributeValue().setTextColor(context.getResources().getColor(R.color.colorPrimary));
                                } else {
                                    attribute.getAttributeValue().setTextColor(context.getResources().getColor(R.color.text_gray));
                                }
                            }

                            //Attribute Value + Unit
                            if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getUnit())) {
                                if (offers.getHeader().getAttributeList().get(j).getUnit()
                                        .equalsIgnoreCase(Constants.UserCurrentKeyword.CURRENCY_AZN)) {
                                    attribute.getPriceIcon().setVisibility(View.VISIBLE);
                                } else {
                                    attribute.getPriceIcon().setVisibility(View.GONE);
                                    attributeValue += " " + offers.getHeader().getAttributeList().get(j).getUnit();
                                }
                            } else {
                                attribute.getPriceIcon().setVisibility(View.GONE);
                            }
                            attribute.getAttributeValue().setText(attributeValue);

                            // Onnet Offnet value changes
                            if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getOnnetValue()) && Tools.hasValue(offers.getHeader().getAttributeList().get(j).getOffnetValue())) {
                                attribute.getOnnetOffnetMainView().setVisibility(View.VISIBLE);
                                if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getOnnetValue())) {
                                    attribute.getOnnetView().setVisibility(View.VISIBLE);
                                    attribute.getOnnetLabel().setText(offers.getHeader().getAttributeList().get(j).getOnnetLabel());
                                    attribute.getOnnetValue().setText(offers.getHeader().getAttributeList().get(j).getOnnetValue());
                                } else {
                                    attribute.getOnnetView().setVisibility(View.GONE);
                                }
                                if (Tools.hasValue(offers.getHeader().getAttributeList().get(j).getOffnetValue())) {
                                    attribute.getOffnetView().setVisibility(View.VISIBLE);
                                    attribute.getOffnetLabel().setText(offers.getHeader().getAttributeList().get(j).getOffnetLabel());
                                    attribute.getOffnetValue().setText(offers.getHeader().getAttributeList().get(j).getOffnetValue());
                                } else {
                                    attribute.getOffnetView().setVisibility(View.GONE);
                                }
                            } else {
                                attribute.getOnnetOffnetMainView().setVisibility(View.GONE);
                            }

                            checkValueForRed(attribute, offers.getHeader().getType(), offers.getHeader().getAttributeList().get(j).getTitle(), offers.getHeader().getAttributeList().get(j).getValue());

                            offerAttributeMainLayout.addView(attribute);
                        }
                    }
                }, 10);


            } else {
                offerAttributeMainLayout.setVisibility(View.GONE);
            }

            //Extending animation
            prepareAnimations(context, offers, i);

            //
            if (i == offerArrayList.size() - 1) {
                dummyview.setVisibility(View.INVISIBLE);
            } else {
                dummyview.setVisibility(View.GONE);
            }


        }

        public void prepareOfferDetail(final Context context, final SupplementaryOffer offer) {

            if (offer == null || offer.getDetails() == null) {
                detailMainLayout.setVisibility(View.GONE);
                detailHeaderView.setVisibility(View.GONE);
                detail_belew_line.setVisibility(View.GONE);
                return;
            }
            detail_belew_line.setVisibility(View.VISIBLE);
            detailMainLayout.setVisibility(View.VISIBLE);
            detailHeaderView.setVisibility(View.VISIBLE);

            final SupplementryOffersDetailsAndDescription offerDetail = offer.getDetails();


            // priceSection
            if (offerDetail.getPrice() != null) {
                offerDetailContents.priceSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getPrice().getTitle())) {
                    offerDetailContents.detailPriceTitle.setText(offerDetail.getPrice().getTitle());
                    offerDetailContents.detailPriceTitle.setSelected(true);
                } else {
                    offerDetailContents.detailPriceTitle.setText("");
                }
                if (Tools.hasValue(offerDetail.getPrice().getValue())) {
                    offerDetailContents.detailPriceValue.setText(offerDetail.getPrice().getValue());
                    offerDetailContents.detailPriceValue.setSelected(true);
                } else {
                    offerDetailContents.detailPriceValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getPrice().getDescription())) {
                    offerDetailContents.detailPriceDescription.setText(Html.fromHtml(offerDetail.getPrice().getDescription()));
                } else {
                    offerDetailContents.detailPriceDescription.setVisibility(View.GONE);
                }
                // Price Attributes
                if (Tools.hasValue(offerDetail.getPrice().getValue())) {
                    if (Tools.hasValue(offerDetail.getPrice().getOffersCurrency())) {
                        String offerCurrency = offerDetail.getPrice().getOffersCurrency();
                        if (offerCurrency.equalsIgnoreCase(OFFER_CURRENCY)) {
                            offerDetailContents.priceIcon.setText(Html.fromHtml("&nbsp;&#x20bc;"));
                        } else {
                            offerDetailContents.priceIcon.setText("");
                            if (Tools.hasValue(offerDetail.getPrice().getValue()))
                                offerDetailContents.priceIcon.setText(offerDetail.getPrice().getValue());
                        }
                    }
                } else {
                    offerDetailContents.priceIcon.setText("");
                }
                if (offerDetail.getPrice().getAttributeList() != null && offerDetail.getPrice().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.offerDetailAttributeLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.offerDetailAttributeLayout.removeAllViews();
                            ArrayList<DetailsAttributes> detailPriceAttributesList = offerDetail.getPrice().getAttributeList();
                            for (int i = 0; i < detailPriceAttributesList.size(); i++) {
                                String offerCurrency = "";
                                if (Tools.hasValue(offerDetail.getPrice().getAttributeList().get(i).getUnit())) {
                                    offerCurrency = offerDetail.getPrice().getAttributeList().get(i).getUnit();
                                }
                                ViewItemOfferDetailPriceAttribute detailPriceAttribute = new ViewItemOfferDetailPriceAttribute(context);
                                if (Tools.hasValue(detailPriceAttributesList.get(i).getTitle())) {
                                    detailPriceAttribute.getPriceSubTitle().setText(detailPriceAttributesList.get(i).getTitle());
                                } else {
                                    detailPriceAttribute.getPriceSubTitle().setText("");
                                }

                                if (Tools.hasValue(detailPriceAttributesList.get(i).getValue())) {
                                    detailPriceAttribute.getPrceValue().setText(detailPriceAttributesList.get(i).getValue());
                                } else {
                                    detailPriceAttribute.getPrceValue().setText("");
                                }
                                if (Tools.hasValue(detailPriceAttributesList.get(i).getValue())) {
                                    if (Tools.hasValue(offerCurrency)) {
                                        if (offerCurrency.equalsIgnoreCase(OFFER_CURRENCY)) {
                                            detailPriceAttribute.getPriceIcon().setText(Html.fromHtml("&nbsp; &#x20bc;"));
                                        } else {
                                            detailPriceAttribute.getPriceIcon().setText("");
                                            detailPriceAttribute.getPriceIcon().setText(offerCurrency);
                                            detailPriceAttribute.getPrceValue().append(" " + offerCurrency);
                                        }
                                    } else {
                                        detailPriceAttribute.getPriceIcon().setText("");
                                    }
                                } else {
                                    detailPriceAttribute.getPriceIcon().setText("");
                                }

                                offerDetailContents.offerDetailAttributeLayout.addView(detailPriceAttribute);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.offerDetailAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.priceSection.setVisibility(View.GONE);
            }

            // roundingSection
            if (offerDetail.getRounding() != null) {
                offerDetailContents.roundingSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getRounding().getTitle())) {
                    offerDetailContents.roundingTitleText.setText(offerDetail.getRounding().getTitle());
                    offerDetailContents.roundingTitleText.setSelected(true);
                } else {
                    offerDetailContents.roundingTitleText.setText("");
                }
                if (Tools.hasValue(offerDetail.getRounding().getValue())) {
                    offerDetailContents.roundingTitleValue.setText(offerDetail.getRounding().getValue());
                    offerDetailContents.roundingTitleValue.setSelected(true);
                } else {
                    offerDetailContents.roundingTitleValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getRounding().getDescription())) {
                    offerDetailContents.detailRoundingDescription.setText(Html.fromHtml(offerDetail.getRounding().getDescription()));
                } else {
                    offerDetailContents.detailRoundingDescription.setText("");
                }
                // Rounding Attributes
                if (offerDetail.getRounding().getAttributeList() != null && offerDetail.getRounding().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.offerDetailroundingAttributeLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.offerDetailroundingAttributeLayout.removeAllViews();
                            ArrayList<DetailsAttributes> roundingAttributeslist = offerDetail.getRounding().getAttributeList();
                            for (int i = 0; i < roundingAttributeslist.size(); i++) {
                                ViewItemOfferDetailRoundingAttribute detailRoundingAttribute = new ViewItemOfferDetailRoundingAttribute(context);
                                detailRoundingAttribute.hideAZN();
                                if (Tools.hasValue(roundingAttributeslist.get(i).getTitle())) {
                                    detailRoundingAttribute.getPriceSubTitle().setText(roundingAttributeslist.get(i).getTitle());
                                } else {
                                    detailRoundingAttribute.getPriceSubTitle().setText("");
                                }
                                if (Tools.hasValue(roundingAttributeslist.get(i).getValue())) {
                                    detailRoundingAttribute.getPrceValue().setText(roundingAttributeslist.get(i).getValue());
                                } else {
                                    detailRoundingAttribute.getPrceValue().setText("");
                                }
                                offerDetailContents.offerDetailroundingAttributeLayout.addView(detailRoundingAttribute);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.offerDetailroundingAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.roundingSection.setVisibility(View.GONE);
            }

            // TextWithTitleSection
            if (offerDetail.getTextWithTitle() != null) {
                offerDetailContents.textWithTitleSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTextWithTitle().getTitle())) {
                    offerDetailContents.text_title.setVisibility(View.VISIBLE);
                    offerDetailContents.text_title.setText(offerDetail.getTextWithTitle().getTitle());
                } else {
                    offerDetailContents.text_title.setVisibility(View.GONE);
                }
                if (Tools.hasValue(offerDetail.getTextWithTitle().getText())) {
                    offerDetailContents.decription_text.setVisibility(View.VISIBLE);
                    offerDetailContents.decription_text.setText(Html.fromHtml(offerDetail.getTextWithTitle().getText()));
                } else {
                    offerDetailContents.decription_text.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.textWithTitleSection.setVisibility(View.GONE);
            }

            //textWithOutTitleSection
            if (offerDetail.getTextWithOutTitle() != null) {
                offerDetailContents.textWithOutTitleSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTextWithOutTitle().getDescription())) {
                    offerDetailContents.textWithOutTitle_text.setText(Html.fromHtml(offerDetail.getTextWithOutTitle().getDescription()));
                } else {
                    offerDetailContents.textWithOutTitle_text.setText("");
                }
            } else {
                offerDetailContents.textWithOutTitleSection.setVisibility(View.GONE);
            }

            // textWithPointsSections
            if (offerDetail.getTextWithPoints() != null && offerDetail.getTextWithPoints().getPointsList() != null && offerDetail.getTextWithPoints().getPointsList().size() > 0) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        offerDetailContents.textWithPointsSections.setVisibility(View.VISIBLE);
                        offerDetailContents.textWithPointsLayout.removeAllViews();
                        ArrayList<String> pointsList = offerDetail.getTextWithPoints().getPointsList();
                        for (int i = 0; i < pointsList.size(); i++) {
                            ViewItemOfferDetailBulletsItem detailBulletsItem = new ViewItemOfferDetailBulletsItem(context);
                            if (Tools.hasValue(pointsList.get(i))) {
                                detailBulletsItem.getBulletText().setText(Html.fromHtml(pointsList.get(i)));
                                offerDetailContents.textWithPointsLayout.addView(detailBulletsItem);
                            } else {
                                detailBulletsItem.getBulletText().setText("");
                            }
                        }
                    }
                }, 10);
            } else {
                offerDetailContents.textWithPointsSections.setVisibility(View.GONE);
            }


            //titleSubTitleValueDescSection
            if (offerDetail.getTitleSubTitleValueAndDesc() != null) {
                offerDetailContents.titleSubTitleValueDescSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTitleSubTitleValueAndDesc().getTitle())) {
                    offerDetailContents.titleSubTitleValueDescTitle.setVisibility(View.VISIBLE);
                    offerDetailContents.titleSubTitleValueDescTitle.setText(offerDetail.getTitleSubTitleValueAndDesc().getTitle());
                } else {
                    offerDetailContents.titleSubTitleValueDescTitle.setVisibility(View.GONE);
                }
                // Sub Title Attributes

                if (offerDetail.getTitleSubTitleValueAndDesc().getAttributeList() != null &&
                        offerDetail.getTitleSubTitleValueAndDesc().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.titleSubTitleValueDescAttributeLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.titleSubTitleValueDescAttributeLayout.removeAllViews();
                            ArrayList<HeaderAttributes> titleSubTitleList = offerDetail.getTitleSubTitleValueAndDesc().getAttributeList();
                            for (int i = 0; i < titleSubTitleList.size(); i++) {
                                ViewItemOfferDetailTitleWithSubTitleDescAttribute subTitleDescAttribute = new ViewItemOfferDetailTitleWithSubTitleDescAttribute(context);
                                if (Tools.hasValue(titleSubTitleList.get(i).getTitle())) {
                                    subTitleDescAttribute.getSubTitle().setText(titleSubTitleList.get(i).getTitle());
                                } else {
                                    subTitleDescAttribute.getSubTitle().setText("");
                                }


                                //Attribute Value + Unit
                                String attributeValue = "";
                                if (Tools.hasValue(titleSubTitleList.get(i).getValue())) {
                                    attributeValue = titleSubTitleList.get(i).getValue();
                                }
                                if (Tools.hasValue(titleSubTitleList.get(i).getUnit())) {
                                    if (titleSubTitleList.get(i).getUnit()
                                            .equalsIgnoreCase(Constants.UserCurrentKeyword.CURRENCY_AZN)) {
                                        subTitleDescAttribute.getPriceIcon().setVisibility(View.VISIBLE);
                                    } else {
                                        subTitleDescAttribute.getPriceIcon().setVisibility(View.GONE);
                                        attributeValue += " " + titleSubTitleList.get(i).getUnit();
                                    }
                                } else {
                                    subTitleDescAttribute.getPriceIcon().setVisibility(View.GONE);
                                }
                                subTitleDescAttribute.getValue().setText(attributeValue);


                                if (Tools.hasValue(titleSubTitleList.get(i).getDescription())) {
                                    subTitleDescAttribute.getDesc_txt().setVisibility(View.VISIBLE);
                                    subTitleDescAttribute.getDesc_txt().setText(Html.fromHtml(titleSubTitleList.get(i).getDescription()));
                                } else {
                                    subTitleDescAttribute.getDesc_txt().setVisibility(View.GONE);
                                }
                                offerDetailContents.titleSubTitleValueDescAttributeLayout.addView(subTitleDescAttribute);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.titleSubTitleValueDescAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.titleSubTitleValueDescSection.setVisibility(View.GONE);
            }

            //dateSection
            if (offerDetail.getDate() != null) {
                offerDetailContents.dateSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getDate().getDescription())) {
                    offerDetailContents.dateDescription.setText(Html.fromHtml(offerDetail.getDate().getDescription()));
                } else {
                    offerDetailContents.dateDescription.setText("");
                }

                String dateFromlabel = "";
                if (Tools.hasValue(offerDetail.getDate().getFromTitle())) {
//                    offerDetailContents.dateFromLabel.setText(offerDetail.getDate().getFromTitle());
                    dateFromlabel = offerDetail.getDate().getFromTitle();
                } else {
//                    offerDetailContents.dateFromLabel.setText("");
                    dateFromlabel = "";
                }
                if (Tools.hasValue(offerDetail.getDate().getFromValue())) {
                    try {
                        offerDetailContents.dateFromValue.setText(dateFromlabel + " " + Tools.getDateAccordingToClientSaid(offerDetail.getDate().getFromValue()));
                        offerDetailContents.dateFromValue.setSelected(true);
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    offerDetailContents.dateFromValue.setText("");
                }

                String dateToLabel = "";
                if (Tools.hasValue(offerDetail.getDate().getToTitle())) {
//                    offerDetailContents.dateToLabel.setText(offerDetail.getDate().getToTitle());
                    dateToLabel = offerDetail.getDate().getToTitle();
                } else {
//                    offerDetailContents.dateToLabel.setText("");
                    dateToLabel = "";
                }
                if (Tools.hasValue(offerDetail.getDate().getToValue())) {
                    try {
                        offerDetailContents.dateToValue.setText(dateToLabel + " " +
                                Tools.getDateAccordingToClientSaid(offerDetail.getDate().getToValue())
                        );
                        offerDetailContents.dateToValue.setSelected(true);
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    offerDetailContents.dateToValue.setText("");
                }
            } else {
                offerDetailContents.dateSection.setVisibility(View.GONE);
            }

            //timeSection
            if (offerDetail.getTime() != null) {
                offerDetailContents.timeSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getTime().getDescription())) {
                    offerDetailContents.timeDescription.setText(Html.fromHtml(offerDetail.getTime().getDescription()));
                } else {
                    offerDetailContents.timeDescription.setText("");
                }

                String fromTitle = "";
                if (Tools.hasValue(offerDetail.getTime().getFromTitle())) {
//                    offerDetailContents.timefromLabel.setText(offerDetail.getTime().getFromTitle());
                    fromTitle = offerDetail.getTime().getFromTitle();
                } else {
                    offerDetailContents.timefromLabel.setText("");
                    fromTitle = "";
                }
                if (Tools.hasValue(offerDetail.getTime().getFromValue())) {
                    offerDetailContents.timefromValue.setText(fromTitle + " " + offerDetail.getTime().getFromValue());
                    offerDetailContents.timefromValue.setSelected(true);
                } else {
                    offerDetailContents.timefromValue.setText("");
                }

                String toTitle = "";
                if (Tools.hasValue(offerDetail.getTime().getToTitle())) {
//                    offerDetailContents.timetoLabel.setText(offerDetail.getTime().getToTitle());
                    toTitle = offerDetail.getTime().getToTitle();
                } else {
                    offerDetailContents.timetoLabel.setText("");
                    toTitle = "";
                }
                if (Tools.hasValue(offerDetail.getTime().getToValue())) {
                    offerDetailContents.timetoValue.setText(toTitle + " " + offerDetail.getTime().getToValue());
                    offerDetailContents.timetoValue.setSelected(true);
                } else {
                    offerDetailContents.timetoValue.setText("");
                }
            } else {
                offerDetailContents.timeSection.setVisibility(View.GONE);
            }


            //roamingSection
            if (offerDetail.getRoamingDetails() != null) {
                offerDetailContents.roamingSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getRoamingDetails().getDescriptionAbove())) {
                    offerDetailContents.roamingDescriptionAbove.setVisibility(View.VISIBLE);
                    offerDetailContents.roamingDescriptionAbove.setText(Html.fromHtml(offerDetail.getRoamingDetails().getDescriptionAbove()));
                } else {
                    offerDetailContents.roamingDescriptionAbove.setVisibility(View.GONE);
                }
                if (Tools.hasValue(offerDetail.getRoamingDetails().getDescriptionBelow())) {
                    offerDetailContents.roamingDescriptionBelow.setVisibility(View.VISIBLE);
                    offerDetailContents.roamingDescriptionBelow.setText(Html.fromHtml(offerDetail.getRoamingDetails().getDescriptionBelow()));
                } else {
                    offerDetailContents.roamingDescriptionBelow.setVisibility(View.GONE);
                }

                // RoamingsOffer Coutries list
                if (offerDetail.getRoamingDetails().getRoamingDetailsCountriesList() != null
                        && offerDetail.getRoamingDetails().getRoamingDetailsCountriesList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDetailContents.roamingCountryLayout.setVisibility(View.VISIBLE);
                            offerDetailContents.roamingCountryLayout.removeAllViews();
                            ArrayList<RoamingDetailsCountries> roamingCountriesList = offerDetail.getRoamingDetails().getRoamingDetailsCountriesList();
                            for (int i = 0; i < roamingCountriesList.size(); i++) {
                                ViewItemOfferDetailRoamingCountries detailRoamingCountries = new ViewItemOfferDetailRoamingCountries(context);
                                if (Tools.hasValue(roamingCountriesList.get(i).getCountryName())) {
                                    detailRoamingCountries.getCountryName().setText(roamingCountriesList.get(i).getCountryName());
                                } else {
                                    detailRoamingCountries.getCountryName().setText("");
                                }
                                if (Tools.hasValue(roamingCountriesList.get(i).getFlag())) {
                                    detailRoamingCountries.getCountryFlag().setImageDrawable(Tools.getImageFromAssests(context, roamingCountriesList.get(i).getFlag()));
                                } else {
                                    detailRoamingCountries.getCountryFlag().setImageResource(R.drawable.dummy_flag);
                                }
                                // Country Operators list
                                if (roamingCountriesList.get(i).getOperatorList() != null && roamingCountriesList.get(i).getOperatorList().size() > 0) {
                                    detailRoamingCountries.getOperatorLayout().setVisibility(View.VISIBLE);
                                    detailRoamingCountries.getOperatorLayout().removeAllViews();
                                    ArrayList<String> countryOperatorsList = roamingCountriesList.get(i).getOperatorList();
                                    for (int j = 0; j < countryOperatorsList.size(); j++) {
                                        if (Tools.hasValue(countryOperatorsList.get(j))) {
                                            BakcellTextViewNormal textViewOperator = new BakcellTextViewNormal(context);
                                            textViewOperator.setText(countryOperatorsList.get(j));
                                            textViewOperator.setGravity(Gravity.RIGHT);
                                            detailRoamingCountries.getOperatorLayout().addView(textViewOperator);
                                        }
                                    }
                                } else {
                                    detailRoamingCountries.getOperatorLayout().setVisibility(View.GONE);
                                }
                                offerDetailContents.roamingCountryLayout.addView(detailRoamingCountries);
                            }
                        }
                    }, 10);
                } else {
                    offerDetailContents.roamingCountryLayout.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.roamingSection.setVisibility(View.GONE);
            }

            // freeResourceValiditySection
            if (offerDetail.getFreeResourceValidity() != null) {
                offerDetailContents.freeResourceValiditySection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getTitle())) {
                    offerDetailContents.freeResourceTitle.setText(offerDetail.getFreeResourceValidity().getTitle());
                } else {
                    offerDetailContents.freeResourceTitle.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getTitleValue())) {
                    offerDetailContents.freeResourceValue.setText(offerDetail.getFreeResourceValidity().getTitleValue());
                } else {
                    offerDetailContents.freeResourceValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getSubTitle())) {
                    offerDetailContents.onnetTitle.setText(offerDetail.getFreeResourceValidity().getSubTitle());
                    offerDetailContents.onnetTitle.setSelected(true);
                } else {
                    offerDetailContents.onnetTitle.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getSubTitleValue())) {
                    offerDetailContents.onnetValue.setText(offerDetail.getFreeResourceValidity().getSubTitleValue());
                    offerDetailContents.onnetValue.setSelected(true);
                } else {
                    offerDetailContents.onnetValue.setText("");
                }
                if (Tools.hasValue(offerDetail.getFreeResourceValidity().getDescription())) {
                    offerDetailContents.freeResourceDescription.setVisibility(View.VISIBLE);
                    offerDetailContents.freeResourceDescription.setText(Html.fromHtml(offerDetail.getFreeResourceValidity().getDescription()));
                } else {
                    offerDetailContents.freeResourceDescription.setVisibility(View.GONE);
                }
            } else {
                offerDetailContents.freeResourceValiditySection.setVisibility(View.GONE);
            }


        }

        public void prepareOfferDescription(final Context context, SupplementaryOffer offer) {

            if (offer == null || offer.getDescription() == null) {
                descriptionMainLayout.setVisibility(View.GONE);
                descriptionHeaderView.setVisibility(View.GONE);
                desc_belew_line.setVisibility(View.GONE);
                return;
            }
            desc_belew_line.setVisibility(View.VISIBLE);
            descriptionMainLayout.setVisibility(View.VISIBLE);
            descriptionHeaderView.setVisibility(View.VISIBLE);

            final SupplementryOffersDetailsAndDescription offerDescription = offer.getDescription();


            // priceSection
            if (offerDescription.getPrice() != null) {
                offerDescriptionContents.priceSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getPrice().getTitle())) {
                    offerDescriptionContents.detailPriceTitle.setText(offerDescription.getPrice().getTitle());
                    offerDescriptionContents.detailPriceTitle.setSelected(true);
                } else {
                    offerDescriptionContents.detailPriceTitle.setText("");
                }
                if (Tools.hasValue(offerDescription.getPrice().getValue())) {
                    offerDescriptionContents.detailPriceValue.setText(offerDescription.getPrice().getValue());
                    offerDescriptionContents.detailPriceValue.setSelected(true);
                } else {
                    offerDescriptionContents.detailPriceValue.setText("");
                }
                if (Tools.hasValue(offerDescription.getPrice().getDescription())) {
                    offerDescriptionContents.detailPriceDescription.setText(offerDescription.getPrice().getDescription());
                } else {
                    offerDescriptionContents.detailPriceDescription.setVisibility(View.GONE);
                }

                String offerCurrency = "";
                if (Tools.hasValue(offerDescription.getPrice().getOffersCurrency())) {
                    offerCurrency = offerDescription.getPrice().getOffersCurrency();
                }


                // Price Attributes
                if (offerDescription.getPrice().getAttributeList() != null &&
                        offerDescription.getPrice().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    final String finalOfferCurrency = offerCurrency;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDescriptionContents.offerDetailAttributeLayout.setVisibility(View.VISIBLE);
                            offerDescriptionContents.offerDetailAttributeLayout.removeAllViews();
                            ArrayList<DetailsAttributes> detailPriceAttributesList = offerDescription.getPrice().getAttributeList();
                            for (int i = 0; i < detailPriceAttributesList.size(); i++) {
                                ViewItemOfferDetailPriceAttribute detailPriceAttribute = new ViewItemOfferDetailPriceAttribute(context);
                                if (Tools.hasValue(detailPriceAttributesList.get(i).getTitle())) {
                                    detailPriceAttribute.getPriceSubTitle().setText(detailPriceAttributesList.get(i).getTitle());
                                } else {
                                    detailPriceAttribute.getPriceSubTitle().setText("");
                                }
                                if (Tools.hasValue(finalOfferCurrency)) {
                                    if (finalOfferCurrency.equalsIgnoreCase(OFFER_CURRENCY)) {
                                        detailPriceAttribute.getPriceIcon().setVisibility(View.VISIBLE);
                                    } else {
                                        detailPriceAttribute.getPriceIcon().setVisibility(View.VISIBLE);
                                        detailPriceAttribute.getPriceIcon().setText(finalOfferCurrency);
                                    }
                                } else {
                                    detailPriceAttribute.getPriceIcon().setVisibility(View.INVISIBLE);
                                }
                                if (Tools.hasValue(detailPriceAttributesList.get(i).getValue())) {
                                    detailPriceAttribute.getPrceValue().setText(detailPriceAttributesList.get(i).getValue());
                                } else {
                                    detailPriceAttribute.getPrceValue().setText("");
                                }
                                offerDescriptionContents.offerDetailAttributeLayout.addView(detailPriceAttribute);
                            }
                        }
                    }, 10);

                } else {
                    offerDescriptionContents.offerDetailAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDescriptionContents.priceSection.setVisibility(View.GONE);
            }

            // roundingSection
            if (offerDescription.getRounding() != null) {
                offerDescriptionContents.roundingSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getRounding().getTitle())) {
                    offerDescriptionContents.roundingTitleText.setText(offerDescription.getRounding().getTitle());
                    offerDescriptionContents.roundingTitleText.setSelected(true);
                } else {
                    offerDescriptionContents.roundingTitleText.setText("");
                }
                if (Tools.hasValue(offerDescription.getRounding().getValue())) {
                    BakcellLogger.logE("roundX12", "text:::" + offerDescription.getRounding().getValue(), "PackagesAdapter", "showing Rounding Title");
                    offerDescriptionContents.roundingTitleValue.setText(offerDescription.getRounding().getValue());
                    offerDescriptionContents.roundingTitleValue.setSelected(true);
                } else {
                    offerDescriptionContents.roundingTitleValue.setText("");
                }
                if (Tools.hasValue(offerDescription.getRounding().getDescription())) {
                    offerDescriptionContents.detailRoundingDescription.setText(offerDescription.getRounding().getDescription());
                } else {
                    offerDescriptionContents.detailRoundingDescription.setText("");
                }

                // Rounding Attributes
                if (offerDescription.getRounding().getAttributeList() != null && offerDescription.getRounding().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDescriptionContents.offerDetailroundingAttributeLayout.setVisibility(View.VISIBLE);
                            offerDescriptionContents.offerDetailroundingAttributeLayout.removeAllViews();
                            ArrayList<DetailsAttributes> roundingAttributeslist = offerDescription.getRounding().getAttributeList();
                            for (int i = 0; i < roundingAttributeslist.size(); i++) {
                                ViewItemOfferDetailRoundingAttribute detailRoundingAttribute = new ViewItemOfferDetailRoundingAttribute(context);
                                detailRoundingAttribute.hideAZN();
                                if (Tools.hasValue(roundingAttributeslist.get(i).getTitle())) {
                                    detailRoundingAttribute.getPriceSubTitle().setText(roundingAttributeslist.get(i).getTitle());
                                } else {
                                    detailRoundingAttribute.getPriceSubTitle().setText("");
                                }
                                if (Tools.hasValue(roundingAttributeslist.get(i).getValue())) {
                                    detailRoundingAttribute.getPrceValue().setText(roundingAttributeslist.get(i).getValue());
                                } else {
                                    detailRoundingAttribute.getPrceValue().setText("");
                                }
                                offerDescriptionContents.offerDetailroundingAttributeLayout.addView(detailRoundingAttribute);
                            }
                        }
                    }, 10);

                } else {
                    offerDescriptionContents.offerDetailroundingAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDescriptionContents.roundingSection.setVisibility(View.GONE);
            }

            // TextWithTitleSection
            if (offerDescription.getTextWithTitle() != null) {
                offerDescriptionContents.textWithTitleSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getTextWithTitle().getTitle())) {
                    offerDescriptionContents.text_title.setVisibility(View.VISIBLE);
                    offerDescriptionContents.text_title.setText(offerDescription.getTextWithTitle().getTitle());
                } else {
                    offerDescriptionContents.text_title.setVisibility(View.GONE);
                }
                if (Tools.hasValue(offerDescription.getTextWithTitle().getText())) {
                    offerDescriptionContents.decription_text.setVisibility(View.VISIBLE);
                    offerDescriptionContents.decription_text.setText(offerDescription.getTextWithTitle().getText());
                } else {
                    offerDescriptionContents.decription_text.setVisibility(View.GONE);
                }
            } else {
                offerDescriptionContents.textWithTitleSection.setVisibility(View.GONE);
            }

            //textWithOutTitleSection
            if (offerDescription.getTextWithOutTitle() != null) {
                offerDescriptionContents.textWithOutTitleSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getTextWithOutTitle().getDescription())) {
                    offerDescriptionContents.textWithOutTitle_text.setText(offerDescription.getTextWithOutTitle().getDescription());
                } else {
                    offerDescriptionContents.textWithOutTitle_text.setText("");
                }
            } else {
                offerDescriptionContents.textWithOutTitleSection.setVisibility(View.GONE);
            }

            // textWithPointsSections
            if (offerDescription.getTextWithPoints() != null && offerDescription.getTextWithPoints().getPointsList() != null && offerDescription.getTextWithPoints().getPointsList().size() > 0) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        offerDescriptionContents.textWithPointsSections.setVisibility(View.VISIBLE);
                        offerDescriptionContents.textWithPointsLayout.removeAllViews();
                        ArrayList<String> pointsList = offerDescription.getTextWithPoints().getPointsList();
                        for (int i = 0; i < pointsList.size(); i++) {
                            ViewItemOfferDetailBulletsItem detailBulletsItem = new ViewItemOfferDetailBulletsItem(context);
                            if (Tools.hasValue(pointsList.get(i))) {
                                detailBulletsItem.getBulletText().setText(pointsList.get(i));
                                offerDescriptionContents.textWithPointsLayout.addView(detailBulletsItem);
                            } else {
                                detailBulletsItem.getBulletText().setText("");
                            }
                        }
                    }
                }, 10);
            } else {
                offerDescriptionContents.textWithPointsSections.setVisibility(View.GONE);
            }


            //titleSubTitleValueDescSection
            if (offerDescription.getTitleSubTitleValueAndDesc() != null) {
                offerDescriptionContents.titleSubTitleValueDescSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getTitleSubTitleValueAndDesc().getTitle())) {
                    offerDescriptionContents.titleSubTitleValueDescTitle.setVisibility(View.VISIBLE);
                    offerDescriptionContents.titleSubTitleValueDescTitle.setText(offerDescription.getTitleSubTitleValueAndDesc().getTitle());
                } else {
                    offerDescriptionContents.titleSubTitleValueDescTitle.setVisibility(View.GONE);
                }
                // Sub Title Attributes
                if (offerDescription.getTitleSubTitleValueAndDesc().getAttributeList() != null && offerDescription.getTitleSubTitleValueAndDesc().getAttributeList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDescriptionContents.titleSubTitleValueDescAttributeLayout.setVisibility(View.VISIBLE);
                            offerDescriptionContents.titleSubTitleValueDescAttributeLayout.removeAllViews();
                            ArrayList<HeaderAttributes> titleSubTitleList = offerDescription.getTitleSubTitleValueAndDesc().getAttributeList();
                            for (int i = 0; i < titleSubTitleList.size(); i++) {
                                ViewItemOfferDetailTitleWithSubTitleDescAttribute subTitleDescAttribute = new ViewItemOfferDetailTitleWithSubTitleDescAttribute(context);
                                if (Tools.hasValue(titleSubTitleList.get(i).getTitle())) {
                                    subTitleDescAttribute.getSubTitle().setText(titleSubTitleList.get(i).getTitle());
                                } else {
                                    subTitleDescAttribute.getSubTitle().setText("");
                                }


                                String attributeValue = "";
                                if (Tools.hasValue(titleSubTitleList.get(i).getValue())) {
                                    attributeValue = titleSubTitleList.get(i).getValue();
                                }

                                //Attribute Value + Unit
                                if (Tools.hasValue(titleSubTitleList.get(i).getUnit())) {
                                    if (titleSubTitleList.get(i).getUnit()
                                            .equalsIgnoreCase(Constants.UserCurrentKeyword.CURRENCY_AZN)) {
                                        subTitleDescAttribute.getPriceIcon().setVisibility(View.VISIBLE);
                                    } else {
                                        subTitleDescAttribute.getPriceIcon().setVisibility(View.GONE);
                                        attributeValue += " " + titleSubTitleList.get(i).getValue();
                                    }
                                } else {
                                    subTitleDescAttribute.getPriceIcon().setVisibility(View.GONE);
                                }
                                subTitleDescAttribute.getValue().setText(attributeValue);


                                if (Tools.hasValue(titleSubTitleList.get(i).getDescription())) {
                                    subTitleDescAttribute.getDesc_txt().setVisibility(View.VISIBLE);
                                    subTitleDescAttribute.getDesc_txt().setText(titleSubTitleList.get(i).getDescription());
                                } else {
                                    subTitleDescAttribute.getDesc_txt().setVisibility(View.GONE);
                                }


                                offerDescriptionContents.titleSubTitleValueDescAttributeLayout.addView(subTitleDescAttribute);
                            }
                        }
                    }, 10);
                } else {
                    offerDescriptionContents.titleSubTitleValueDescAttributeLayout.setVisibility(View.GONE);
                }
            } else {
                offerDescriptionContents.titleSubTitleValueDescSection.setVisibility(View.GONE);
            }

            //dateSection
            if (offerDescription.getDate() != null) {
                offerDescriptionContents.dateSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getDate().getDescription())) {
                    offerDescriptionContents.dateDescription.setText(offerDescription.getDate().getDescription());
                } else {
                    offerDescriptionContents.dateDescription.setText("");
                }


                if (Tools.hasValue(offerDescription.getDate().getFromTitle())) {
                    offerDescriptionContents.dateFromLabel.setText(offerDescription.getDate().getFromTitle());
                } else {
                    offerDescriptionContents.dateFromLabel.setText("");
                }
                if (Tools.hasValue(offerDescription.getDate().getFromValue())) {
                    try {
                        offerDescriptionContents.dateFromValue.setText(
                                Tools.getDateAccordingToClientSaid(offerDescription.getDate().getFromValue()));
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    offerDescriptionContents.dateFromValue.setText("");
                }
                if (Tools.hasValue(offerDescription.getDate().getToTitle())) {
                    offerDescriptionContents.dateToLabel.setText(offerDescription.getDate().getToTitle());
                } else {
                    offerDescriptionContents.dateToLabel.setText("");
                }
                if (Tools.hasValue(offerDescription.getDate().getToValue())) {
                    try {
                        offerDescriptionContents.dateToValue.setText(
                                Tools.getDateAccordingToClientSaid(offerDescription.getDate().getToValue())
                        );
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    offerDescriptionContents.dateToValue.setText("");
                }
            } else {
                offerDescriptionContents.dateSection.setVisibility(View.GONE);
            }

            //timeSection
            if (offerDescription.getTime() != null) {
                offerDescriptionContents.timeSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getTime().getDescription())) {
                    offerDescriptionContents.timeDescription.setText(offerDescription.getTime().getDescription());
                } else {
                    offerDescriptionContents.timeDescription.setText("");
                }
                if (Tools.hasValue(offerDescription.getTime().getFromTitle())) {
                    offerDescriptionContents.timefromLabel.setText(offerDescription.getTime().getFromTitle());
                } else {
                    offerDescriptionContents.timefromLabel.setText("");
                }
                if (Tools.hasValue(offerDescription.getTime().getFromValue())) {
                    offerDescriptionContents.timefromValue.setText(offerDescription.getTime().getFromValue());
                } else {
                    offerDescriptionContents.timefromValue.setText("");
                }
                if (Tools.hasValue(offerDescription.getTime().getToTitle())) {
                    offerDescriptionContents.timetoLabel.setText(offerDescription.getTime().getToTitle());
                } else {
                    offerDescriptionContents.timetoLabel.setText("");
                }
                if (Tools.hasValue(offerDescription.getTime().getToValue())) {
                    offerDescriptionContents.timetoValue.setText(offerDescription.getTime().getToValue());
                } else {
                    offerDescriptionContents.timetoValue.setText("");
                }
            } else {
                offerDescriptionContents.timeSection.setVisibility(View.GONE);
            }


            //roamingSection
            if (offerDescription.getRoamingDetails() != null) {
                offerDescriptionContents.roamingSection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getRoamingDetails().getDescriptionAbove())) {
                    offerDescriptionContents.roamingDescriptionAbove.setVisibility(View.VISIBLE);
                    offerDescriptionContents.roamingDescriptionAbove.setText(offerDescription.getRoamingDetails().getDescriptionAbove());
                } else {
                    offerDescriptionContents.roamingDescriptionAbove.setVisibility(View.GONE);
                }
                if (Tools.hasValue(offerDescription.getRoamingDetails().getDescriptionBelow())) {
                    offerDescriptionContents.roamingDescriptionBelow.setVisibility(View.VISIBLE);
                    offerDescriptionContents.roamingDescriptionBelow.setText(offerDescription.getRoamingDetails().getDescriptionBelow());
                } else {
                    offerDescriptionContents.roamingDescriptionBelow.setVisibility(View.GONE);
                }

                // RoamingsOffer Coutries list
                if (offerDescription.getRoamingDetails().getRoamingDetailsCountriesList() != null && offerDescription.getRoamingDetails().getRoamingDetailsCountriesList().size() > 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            offerDescriptionContents.roamingCountryLayout.setVisibility(View.VISIBLE);
                            offerDescriptionContents.roamingCountryLayout.removeAllViews();
                            ArrayList<RoamingDetailsCountries> roamingCountriesList = offerDescription.getRoamingDetails().getRoamingDetailsCountriesList();
                            for (int i = 0; i < roamingCountriesList.size(); i++) {
                                ViewItemOfferDetailRoamingCountries detailRoamingCountries = new ViewItemOfferDetailRoamingCountries(context);
                                if (Tools.hasValue(roamingCountriesList.get(i).getCountryName())) {
                                    detailRoamingCountries.getCountryName().setText(roamingCountriesList.get(i).getCountryName());
                                } else {
                                    detailRoamingCountries.getCountryName().setText("");
                                }
                                if (Tools.hasValue(roamingCountriesList.get(i).getFlag())) {
                                    detailRoamingCountries.getCountryFlag().setImageDrawable(Tools.getImageFromAssests(context, roamingCountriesList.get(i).getFlag()));
                                } else {
                                    detailRoamingCountries.getCountryFlag().setImageResource(R.drawable.dummy_flag);
                                }
                                // Country Operators list
                                if (roamingCountriesList.get(i).getOperatorList() != null && roamingCountriesList.get(i).getOperatorList().size() > 0) {
                                    detailRoamingCountries.getOperatorLayout().setVisibility(View.VISIBLE);
                                    detailRoamingCountries.getOperatorLayout().removeAllViews();
                                    ArrayList<String> countryOperatorsList = roamingCountriesList.get(i).getOperatorList();
                                    for (int j = 0; j < countryOperatorsList.size(); j++) {
                                        if (Tools.hasValue(countryOperatorsList.get(j))) {
                                            BakcellTextViewNormal textViewOperator = new BakcellTextViewNormal(context);
                                            textViewOperator.setText(countryOperatorsList.get(j));
                                            textViewOperator.setGravity(Gravity.RIGHT);
                                            detailRoamingCountries.getOperatorLayout().addView(textViewOperator);
                                        }
                                    }
                                } else {
                                    detailRoamingCountries.getOperatorLayout().setVisibility(View.GONE);
                                }
                                offerDescriptionContents.roamingCountryLayout.addView(detailRoamingCountries);
                            }
                        }
                    }, 10);
                } else {
                    offerDescriptionContents.roamingCountryLayout.setVisibility(View.GONE);
                }
            } else {
                offerDescriptionContents.roamingSection.setVisibility(View.GONE);
            }

            // freeResourceValiditySection
            if (offerDescription.getFreeResourceValidity() != null) {
                offerDescriptionContents.freeResourceValiditySection.setVisibility(View.VISIBLE);
                if (Tools.hasValue(offerDescription.getFreeResourceValidity().getTitle())) {
                    offerDescriptionContents.freeResourceTitle.setText(offerDescription.getFreeResourceValidity().getTitle());
                } else {
                    offerDescriptionContents.freeResourceTitle.setText("");
                }
                if (Tools.hasValue(offerDescription.getFreeResourceValidity().getTitleValue())) {
                    offerDescriptionContents.freeResourceValue.setText(offerDescription.getFreeResourceValidity().getTitleValue());
                } else {
                    offerDescriptionContents.freeResourceValue.setText("");
                }
                if (Tools.hasValue(offerDescription.getFreeResourceValidity().getSubTitle())) {
                    offerDescriptionContents.onnetTitle.setText(offerDescription.getFreeResourceValidity().getSubTitle());
                } else {
                    offerDescriptionContents.onnetTitle.setText("");
                }
                if (Tools.hasValue(offerDescription.getFreeResourceValidity().getSubTitleValue())) {
                    offerDescriptionContents.onnetValue.setText(offerDescription.getFreeResourceValidity().getSubTitleValue());
                } else {
                    offerDescriptionContents.onnetValue.setText("");
                }
                if (Tools.hasValue(offerDescription.getFreeResourceValidity().getDescription())) {
                    offerDescriptionContents.freeResourceDescription.setVisibility(View.VISIBLE);
                    offerDescriptionContents.freeResourceDescription.setText(offerDescription.getFreeResourceValidity().getDescription());
                } else {
                    offerDescriptionContents.freeResourceDescription.setVisibility(View.GONE);
                }
            } else {
                offerDescriptionContents.freeResourceValiditySection.setVisibility(View.GONE);
            }


        }


    }

    private void checkValueForRed(ViewItemOfferAttribute attribute, String type, String title, String value) {
        if (context == null) return;

        if (Tools.isValueRed(value)) {
            attribute.getAttributeValue().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            attribute.hideRightAZN();
            return;
        }

        boolean isValueRed = false;

        if (Tools.hasValue(type)) {
            type = type.trim();
            if (OFFER_TYPE_INTERNET_EN.equalsIgnoreCase(type) ||
                    OFFER_TYPE_INTERNET_RU.equalsIgnoreCase(type) ||
                    OFFER_TYPE_INTERNET_AZ.equalsIgnoreCase(type) ||
                    OFFER_TYPE_CALL_EN.equalsIgnoreCase(type) ||
                    OFFER_TYPE_CALL_RU.equalsIgnoreCase(type) ||
                    OFFER_TYPE_CALL_AZ.equalsIgnoreCase(type) ||
                    OFFER_TYPE_SMS_EN.equalsIgnoreCase(type) ||
                    OFFER_TYPE_SMS_RU.equalsIgnoreCase(type) ||
                    OFFER_TYPE_SMS_AZ.equalsIgnoreCase(type)) {

                isValueRed = true;

            } else if (OFFER_TYPE_TM_EN.equalsIgnoreCase(type) ||
                    OFFER_TYPE_TM_RU.equalsIgnoreCase(type) ||
                    OFFER_TYPE_TM_AZ.equalsIgnoreCase(type)) {

                if (Tools.hasValue(title)) {
                    title = title.trim();
                    if (TM_OFFER_WITH_FR_EN.equalsIgnoreCase(title)
                            || TM_OFFER_WITH_FR_RU.equalsIgnoreCase(title)
                            || TM_OFFER_WITH_FR_AZ.equalsIgnoreCase(title)) {

                        isValueRed = true;

                    }
                }
            }
        }

        if (isValueRed) {
            attribute.getAttributeValue().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            attribute.getPriceIcon().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            attribute.getAttributeValue().setTextColor(ContextCompat.getColor(context, R.color.black));
            attribute.getPriceIcon().setTextColor(ContextCompat.getColor(context, R.color.black));
        }

    }


    private class SupplementaryFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null) {

            } else if (constraint.toString().trim().length() == 0) {
                searchResultListener.onSearchResult(true);
                results.values = offerArrayListTemp;
                results.count = offerArrayListTemp.size();
            } else {
                ArrayList<SupplementaryOffer> filteredResult = new ArrayList<>();
                for (SupplementaryOffer offer : offerArrayListTemp) {
                    if (offer != null && offer.getHeader() != null &&
                            offer.getHeader().getOfferName().toLowerCase()
                                    .contains(constraint.toString().toLowerCase())) {
                        filteredResult.add(offer);
                    }
                }
                if (filteredResult.size() > 0) {
                    searchResultListener.onSearchResult(true);
                    results.values = filteredResult;
                    results.count = filteredResult.size();
                } else {
                    //Toast.makeText(context, R.string.no_results_found, Toast.LENGTH_SHORT).show();
                    searchResultListener.onSearchResult(false);
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                offerArrayList = (ArrayList<SupplementaryOffer>) results.values;
                notifyDataSetChanged();
            }
        }

    }


    public static void offerGroupInInternetAndRoamingIconsMapping(Context context, String offerGroupValue, ImageView icOfferGroupPhone, ImageView icOfferGroupTab, ImageView icOfferGroupDesktop) {

        if (context == null || icOfferGroupPhone == null || icOfferGroupTab == null || icOfferGroupDesktop == null)
            return;

        if (Tools.hasValue(offerGroupValue)) {
            if (offerGroupValue.equalsIgnoreCase(OFFER_GROUP_MOBILE)) {
                icOfferGroupPhone.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_mobile_state_red));
                icOfferGroupTab.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_tablet_state_grey));
                icOfferGroupDesktop.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_desktop_state_grey));
            } else if (offerGroupValue.equalsIgnoreCase(OFFER_GROUP_TAB)) {
                icOfferGroupPhone.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_mobile_state_grey));
                icOfferGroupTab.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_tablet_state_red));
                icOfferGroupDesktop.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_desktop_state_grey));
            } else if (offerGroupValue.equalsIgnoreCase(OFFER_GROUP_DESKTOP)) {
                icOfferGroupPhone.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_mobile_state_grey));
                icOfferGroupTab.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_tablet_state_grey));
                icOfferGroupDesktop.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_desktop_state_red));
            } else {
                icOfferGroupPhone.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_mobile_state_grey));
                icOfferGroupTab.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_tablet_state_grey));
                icOfferGroupDesktop.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_desktop_state_grey));
            }
        } else {
            icOfferGroupPhone.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_mobile_state_grey));
            icOfferGroupTab.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_tablet_state_grey));
            icOfferGroupDesktop.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_desktop_state_grey));
        }

    }


    public static void offerTypeInRoamingIconsMapping(Context context, String offerType, ImageView ic_call_type, ImageView ic_internet_type, ImageView ic_campaign_type, BakcellTextViewNormal tv_offer_type_name) {
        if (context == null || ic_call_type == null || ic_internet_type == null || ic_campaign_type == null || tv_offer_type_name == null)
            return;

        if (Tools.hasValue(offerType)) {
            if (CardAdapterSupplementaryOffers.OFFER_TYPE_CALL_EN.equalsIgnoreCase(offerType)
                    || CardAdapterSupplementaryOffers.OFFER_TYPE_CALL_AZ.equalsIgnoreCase(offerType)
                    || CardAdapterSupplementaryOffers.OFFER_TYPE_CALL_RU.equalsIgnoreCase(offerType)) {

                ic_call_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_call_red));
                ic_internet_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_internet_grey));
                ic_campaign_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_campaing_grey));
                tv_offer_type_name.setText(context.getString(R.string.lbl_calls));

            } else if (CardAdapterSupplementaryOffers.OFFER_TYPE_INTERNET_EN.equalsIgnoreCase(offerType)
                    || CardAdapterSupplementaryOffers.OFFER_TYPE_INTERNET_RU.equalsIgnoreCase(offerType)
                    || CardAdapterSupplementaryOffers.OFFER_TYPE_INTERNET_AZ.equalsIgnoreCase(offerType)) {

                ic_call_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_call_grey));
                ic_internet_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_internet_red));
                ic_campaign_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_campaing_grey));

                tv_offer_type_name.setText(context.getString(R.string.lbl_internet));

            } else if (CardAdapterSupplementaryOffers.OFFER_TYPE_CAMPAIGN_EN.equalsIgnoreCase(offerType)
                    || CardAdapterSupplementaryOffers.OFFER_TYPE_CAMPAIGN_RU.equalsIgnoreCase(offerType)
                    || CardAdapterSupplementaryOffers.OFFER_TYPE_CAMPAIGN_AZ.equalsIgnoreCase(offerType)) {

                ic_call_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_call_grey));
                ic_internet_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_internet_grey));
                ic_campaign_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_campaing_red));
                tv_offer_type_name.setText(context.getString(R.string.supplementary_offer_tab_title_campaign));

            } else {
                ic_call_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_call_grey));
                ic_internet_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_internet_grey));
                ic_campaign_type.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.ic_roaming_type_campaing_grey));
            }
        } else {
            ic_call_type.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_roaming_type_call_grey));
            ic_internet_type.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_roaming_type_internet_grey));
            ic_campaign_type.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_roaming_type_campaing_grey));
        }

    }


}

