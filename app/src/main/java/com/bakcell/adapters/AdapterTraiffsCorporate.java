package com.bakcell.adapters;

import static com.bakcell.adapters.AdapterTraiffsKlass.KLASS;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.ChangeTariffListener;
import com.bakcell.interfaces.UpdateListOnSuccessfullySurvey;
import com.bakcell.models.inappsurvey.Answers;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Questions;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.inappsurvey.UserSurveys;
import com.bakcell.models.tariffs.corporateobject.TariffCorporate;
import com.bakcell.models.tariffs.corporateobject.description.Description;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.Attributes;
import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePrice;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.TariffDetailLayout;
import com.bakcell.viewsitem.ViewItemTariffsAttribute;
import com.bakcell.viewsitem.ViewItemTariffsKlassAttribute;
import com.bakcell.webservices.changetariff.ChangeTariff;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Zeeshan Shabbir on 9/6/2017.
 */

public class AdapterTraiffsCorporate extends RecyclerView.Adapter<AdapterTraiffsCorporate.ViewHolder> {


    private static boolean isDescriptionExpanded;
    private static boolean isPriceExpanded;
    private static boolean isDetailExpanded;

    ArrayList<TariffCorporate> tariffCorporateList;

    Context context;

    public AdapterTraiffsCorporate() {

    }

    public AdapterTraiffsCorporate(Context context, ArrayList<TariffCorporate> tariffCorporateList, boolean priceExp, boolean detailExp, boolean descExp) {
        this.context = context;
        isDescriptionExpanded = descExp;
        isPriceExpanded = priceExp;
        isDetailExpanded = detailExp;
        this.tariffCorporateList = tariffCorporateList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .adapter_item_tariffs_corporate, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.prepareAnimItem(context, position);

        int rateStars = 0;

        if (tariffCorporateList.get(position).getHeader() != null) {
            holder.prepareHeaderItem(tariffCorporateList.get(position).getHeader(), holder);
        } else {
            holder.prepareHeaderItem(null, holder);
        }
        if (tariffCorporateList.get(position).getPrice() != null) {
            holder.preparePriceItem(tariffCorporateList.get(position).getPrice(), holder);
        } else {
            holder.preparePriceItem(null, holder);
        }
        if (tariffCorporateList.get(position).getDetails() != null) {
            holder.prepareDetailItem(tariffCorporateList.get(position).getDetails(), holder);
        } else {
            holder.prepareDetailItem(null, holder);
        }

        if (tariffCorporateList.get(position).getDescription() != null) {
            holder.prepareDescriptionItem(tariffCorporateList.get(position).getDescription(), holder);
        } else {
            holder.prepareDescriptionItem(null, holder);
        }

        final int pos1 = position;


        // Subscribe Button process
        holder.button_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (context != null && tariffCorporateList != null && tariffCorporateList.get(pos1) != null && tariffCorporateList.get(pos1).getHeader() != null) {
                    processOnSubscribeButton(context, tariffCorporateList.get(pos1).getHeader().getOfferingId(), tariffCorporateList.get(pos1).getHeader().getName(), tariffCorporateList.get(pos1).getHeader().getSubscribable());
                }
            }
        });
        Data data = PrefUtils.getInAppSurvey(context).getData();
        if (data != null && data.getUserSurveys() != null) {
            List<UserSurveys> userSurveys = data.getUserSurveys();
            for (int i = 0; i < userSurveys.size(); i++) {
                if (userSurveys.get(i).getOfferingId().equals(tariffCorporateList.get(pos1).getHeader().getOfferingId())) {
                    if (userSurveys.get(i).getAnswer() != null) {
                        String surveyId = userSurveys.get(i).getSurveyId();
                        if (surveyId != null) {
                            Surveys survey = data.findSurveyById(Integer.parseInt(surveyId));
                            if (Tools.isNumeric(userSurveys.get(i).getAnswerId())) {
                                if (Tools.isNumeric(userSurveys.get(i).getQuestionId())) {
                                    rateStars = setStarsCorporateViewHolder(Integer.parseInt(userSurveys.get(i).getAnswerId()),
                                            Integer.parseInt(userSurveys.get(i).getQuestionId()), survey, holder);
                                }
                            }
                        }
                    } else {
                        rateStars = setStarsCorporateViewHolder(0,0,null, holder);
                    }
                }
            }
        }
        int finalRateStars = rateStars;
        holder.ratingStarsLayout.setOnClickListener(
                v -> {
                    String tariffId = tariffCorporateList.get(position).getHeader().getOfferingId();
                    inAppFeedback(context, tariffId, finalRateStars, position);
                }
        );
    }

    private void inAppFeedback(Context context, String tariffId, int rate, int position) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TARIFF_PAGE);
            if (surveys != null) {
                InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                if (surveys.getQuestions() != null) {
                    currentVisit = 0;
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(new UpdateListOnSuccessfullySurvey() {
                        @Override
                        public void updateListOnSuccessfullySurvey() {
                            notifyItemChanged(position);
                        }
                    });
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "1", tariffId, rate);
                }
            }
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE, currentVisit);
    }

    private int setStarsCorporateViewHolder(int answerId, int questionId, Surveys survey, ViewHolder viewHolder) {
        if (survey != null) {
            Questions question = survey.findQuestion(questionId);
            if (question != null) {
                List<Answers> answers = question.getAnswers();
                if (answers != null) {
                    if (answers.size() > 0 && answers.get(0).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(false);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 1;
                    } else if (answers.size() > 1 && answers.get(1).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(false);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 2;
                    } else if (answers.size() > 2 && answers.get(2).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(false);
                        viewHolder.starFive.setChecked(false);
                        return 3;
                    } else if (answers.size() > 3 && answers.get(3).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(false);
                        return 4;
                    } else if (answers.size() > 4 && answers.get(4).getId() == answerId) {
                        viewHolder.starOne.setChecked(true);
                        viewHolder.starTwo.setChecked(true);
                        viewHolder.starThree.setChecked(true);
                        viewHolder.starFour.setChecked(true);
                        viewHolder.starFive.setChecked(true);
                        return 5;
                    }

                } else {
                    viewHolder.starOne.setChecked(false);
                    viewHolder.starTwo.setChecked(false);
                    viewHolder.starThree.setChecked(false);
                    viewHolder.starFour.setChecked(false);
                    viewHolder.starFive.setChecked(false);
                    return 0;
                }
            } else {
                viewHolder.starOne.setChecked(false);
                viewHolder.starTwo.setChecked(false);
                viewHolder.starThree.setChecked(false);
                viewHolder.starFour.setChecked(false);
                viewHolder.starFive.setChecked(false);
                return 0;
            }
        } else {
            viewHolder.starOne.setChecked(false);
            viewHolder.starTwo.setChecked(false);
            viewHolder.starThree.setChecked(false);
            viewHolder.starFour.setChecked(false);
            viewHolder.starFive.setChecked(false);
            return 0;
        }
        return 0;
    }

    private void processOnSubscribeButton(Context context, String offeringId, String tariffName, String subscribable) {
        if (context == null) return;

        ChangeTariff.requestForChangeTariffDialog(context, offeringId, tariffName, subscribable, KLASS, new ChangeTariffListener() {
            @Override
            public void onChangeTariffListenerSuccess(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_SUCCESS,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }

            @Override
            public void onChangeTariffListenerFailure(String message) {
                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                        AppEventLogValues.TariffEvents.TARIFF_MIGRATION_FAILURE,
                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (tariffCorporateList == null) {
            return 0;
        } else {
            return tariffCorporateList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ExpandableLayout expandable_title_layout;
        ExpandableLayout expandable_prices_layout;
        ExpandableLayout expandable_details_layout;
        ExpandableLayout expandable_description_layout;
        private ImageView prices_expand_icon, details_price_expand_icon, description_expand_icon;
        LinearLayout secondLayout, thirdLayout, fourthLayout, attribute_content_layout, internet_content_layout;
        ConstraintLayout ratingStarsLayout;
        CheckBox starOne, starTwo, starThree, starFour, starFive;
        HeaderLayout headerLayout = new HeaderLayout();
        PricesLayout pricesLayout = new PricesLayout();
        TariffDetailLayout detailLayout = new TariffDetailLayout();
        DescriptionLayout descriptionLayout = new DescriptionLayout();

        BakcellButtonNormal button_subscribe;

        View title_belew_line, price_belew_line, detail_belew_line, desc_belew_line;

        BakcellTextViewNormal prices_title_txt, details_title_txt, description_title_txt;

        class HeaderLayout {
            BakcellTextViewBold offer_title_txt;
            BakcellTextViewNormal tv_price_label, creditValueTxt;

        }

        class PricesLayout {
            LinearLayout call_content_layout, sms_content_layout;
            BakcellTextViewNormal tv_call_lbl, tv_call_value, tv_sms_lbl, tv_sms_value,
                    tv_internet_lbl, tv_internet_value, tv_InternetSubTitle,
                    tv_InternetSubTitleValue, tv_call_left_value;
            ImageView call_icon, sms_icon, internet_icon;
            View seperator;
        }

        class DescriptionLayout {
            BakcellTextViewNormal advantage_txt_title, clarification_txt_title,
                    tv_advantages_description, every_subcriber_txt;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            int width = (int) Tools.convertDpToPixel(RootValues.getInstance().getTARIFFS_WIDTH(), itemView.getContext());
            itemView.setLayoutParams(new RecyclerView.LayoutParams(width,
                    RecyclerView.LayoutParams.MATCH_PARENT));
            expandable_title_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_title_layout);
            expandable_prices_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_package_price_layout);
            expandable_details_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_payg_price_layout);
            expandable_description_layout = (ExpandableLayout) itemView
                    .findViewById(R.id.expandable_detail_layout);
            secondLayout = (LinearLayout) itemView.findViewById(R.id.second_layout);
            thirdLayout = (LinearLayout) itemView.findViewById(R.id.third_layout);
            fourthLayout = (LinearLayout) itemView.findViewById(R.id.fouth_layout);
            attribute_content_layout = (LinearLayout) itemView.findViewById(R.id.attribute_content_layout);
            internet_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.internet_content_layout);
            prices_expand_icon = (ImageView) itemView.findViewById(R.id.package_expand_icon);
            details_price_expand_icon = (ImageView) itemView.findViewById(R.id.payg_price_expand_icon);
            description_expand_icon = (ImageView) itemView.findViewById(R.id.detail_expand_icon);

            //View for header
            headerLayout.offer_title_txt = (BakcellTextViewBold) itemView
                    .findViewById(R.id.offer_title_txt);
            headerLayout.tv_price_label = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_price_label);
            headerLayout.creditValueTxt = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.creditValueTxt);

            button_subscribe = (BakcellButtonNormal) itemView.findViewById(R.id.button_subscribe);


            // Rating Stars
            ratingStarsLayout = itemView.findViewById(R.id.ratingStarsLayout);
            starOne = itemView.findViewById(R.id.star1);
            starTwo = itemView.findViewById(R.id.star2);
            starThree = itemView.findViewById(R.id.star3);
            starFour = itemView.findViewById(R.id.star4);
            starFive = itemView.findViewById(R.id.star5);
            ;

            //View for Prices
            pricesLayout.call_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.call_content_layout);
            pricesLayout.tv_call_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_call_lbl);
            pricesLayout.tv_call_value = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_call_value);
            pricesLayout.tv_call_value.setSelected(true);
            pricesLayout.seperator = (View) itemView.findViewById(R.id.seperator);
            pricesLayout.tv_sms_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_sms_lbl);
            pricesLayout.tv_sms_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_sms_value);
            pricesLayout.tv_internet_lbl = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_internet_lbl);
            pricesLayout.tv_internet_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_internet_value);
            pricesLayout.sms_content_layout = (LinearLayout) itemView
                    .findViewById(R.id.sms_content_layout);
            pricesLayout.tv_InternetSubTitle = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitle);
            pricesLayout.sms_icon = (ImageView) itemView.findViewById(R.id.internet_icon_1);
            pricesLayout.call_icon = (ImageView) itemView.findViewById(R.id.internet_icon_1);
            pricesLayout.internet_icon = (ImageView) itemView.findViewById(R.id.internet_icon_1);

            pricesLayout.tv_InternetSubTitleValue = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_InternetSubTitleValue);
            pricesLayout.tv_call_left_value = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_call_left_value);
            pricesLayout.tv_call_left_value.setSelected(true);

            //Views for description
            descriptionLayout.every_subcriber_txt = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.every_subcriber_txt);
            descriptionLayout.tv_advantages_description = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.tv_advantages_description);
            //header description
            descriptionLayout.advantage_txt_title = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.advantage_txt);
            descriptionLayout.clarification_txt_title = (BakcellTextViewNormal) itemView
                    .findViewById(R.id.clarification_txt);


            title_belew_line = itemView.findViewById(R.id.title_belew_line);
            detail_belew_line = itemView.findViewById(R.id.detail_belew_line);
            desc_belew_line = itemView.findViewById(R.id.desc_belew_line);
            price_belew_line = itemView.findViewById(R.id.price_belew_line);

            prices_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.prices_title_txt);
            details_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.details_title_txt);
            description_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.description_title_txt);
            prices_title_txt.setSelected(true);
            details_title_txt.setSelected(true);
            description_title_txt.setSelected(true);

            // Init Detail View
            detailLayout = TariffsFragment.loadDetailUI_Ids(itemView);

        }

        void prepareAnimItem(final Context context, final int i) {


            secondLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isPriceExpanded) {
                        isPriceExpanded = false;
                    } else {
                        isPriceExpanded = true;
                    }
                    isDetailExpanded = false;
                    isDescriptionExpanded = false;
                    notifyDataSetChanged();
                }
            });

            thirdLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDetailExpanded) {
                        isDetailExpanded = false;
                    } else {
                        isDetailExpanded = true;
                    }
                    isPriceExpanded = false;
                    isDescriptionExpanded = false;
                    notifyDataSetChanged();
                }
            });

            fourthLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDescriptionExpanded) {
                        isDescriptionExpanded = false;
                    } else {
                        isDescriptionExpanded = true;
                    }
                    isPriceExpanded = false;
                    isDetailExpanded = false;
                    notifyDataSetChanged();
                }
            });


            if (isDescriptionExpanded) {
                if (tariffCorporateList.get(i).getDescription() != null) {

                } else {
                    return;
                }
                expandable_description_layout.setExpanded(true);
                expandable_title_layout.setExpanded(false);
                expandable_prices_layout.setExpanded(false);
                expandable_details_layout.setExpanded(false);
                description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_minus));
                prices_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                details_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
            } else if (isPriceExpanded) {
                if (tariffCorporateList.get(i).getPrice() != null) {

                } else {
                    return;
                }
                expandable_description_layout.setExpanded(false);
                expandable_title_layout.setExpanded(false);
                expandable_prices_layout.setExpanded(true);
                expandable_details_layout.setExpanded(false);
                description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                prices_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_minus));
                details_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
            } else if (isDetailExpanded) {
                if (tariffCorporateList.get(i).getDetails() != null) {

                } else {
                    return;
                }
                expandable_description_layout.setExpanded(false);
                expandable_title_layout.setExpanded(false);
                expandable_prices_layout.setExpanded(false);
                expandable_details_layout.setExpanded(true);
                description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                prices_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                details_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_minus));
            } else {
                expandable_description_layout.setExpanded(false);
                expandable_title_layout.setExpanded(true);
                expandable_prices_layout.setExpanded(false);
                expandable_details_layout.setExpanded(false);
                description_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                prices_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
                details_price_expand_icon.setImageDrawable(ContextCompat.getDrawable(context,
                        R.drawable.icon_plus));
            }
        }

        void prepareHeaderItem(final Header header, final ViewHolder holder) {
            holder.headerLayout.creditValueTxt.setText("");
            holder.headerLayout.tv_price_label.setText("");
            holder.headerLayout.offer_title_txt.setText("");
            if (header.getName() != null) {
                holder.headerLayout.offer_title_txt.setText(header.getName());
                holder.headerLayout.creditValueTxt.setSelected(true);
            }
            if (header.getPriceLabel() != null) {
                holder.headerLayout.tv_price_label.setText(header.getPriceLabel());
                holder.headerLayout.tv_price_label.setSelected(true);
            }
            if (header.getPriceValue() != null) {
                holder.headerLayout.creditValueTxt.setText(header.getPriceValue());
            }
            holder.attribute_content_layout.removeAllViews();
            if (header != null && header.getAttributes() != null) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < header.getAttributes().size(); i++) {
                            ViewItemTariffsKlassAttribute viewItemTariffsKlassAttribute =
                                    new ViewItemTariffsKlassAttribute(context);
                            viewItemTariffsKlassAttribute.getAttribute_name().setText("");
                            viewItemTariffsKlassAttribute.getAttribute_value().setText("");

                            if (Tools.hasValue(header.getAttributes().get(i).getTitle())) {
                                viewItemTariffsKlassAttribute.getAttribute_name().setText(header.getAttributes().get(i).getTitle());
                            }
                            if (Tools.hasValue(header.getAttributes().get(i).getValue())) {
                                String value = header.getAttributes().get(i).getValue();
                                if (Tools.isValueRed(value)) {
                                    viewItemTariffsKlassAttribute.setValueTextColor(R.color.colorPrimary);
                                } else {
                                    viewItemTariffsKlassAttribute.setValueTextColor(R.color.black);
                                }
                                viewItemTariffsKlassAttribute.getAttribute_value().setText(value);
                                if (Tools.hasValue(header.getAttributes().get(i).getMetrics())) {
                                    viewItemTariffsKlassAttribute.getAttribute_value().append(" " + header.getAttributes().get(i).getMetrics());
                                }
                            }

                            if (header.getAttributes().get(i) != null) {
                                Tools.setTariffsIcons(header.getAttributes().get(i).getIconName(), viewItemTariffsKlassAttribute.getIcon(), context);
                            }
                            holder.attribute_content_layout.addView(viewItemTariffsKlassAttribute);
                        }
                    }
                }, 10);


                //handling subscribe button.!
                if (header.getSubscribable() != null) {

                    if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_NOT_SUBSCRIBE)) {
                        button_subscribe.setEnabled(false);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        ratingStarsLayout.setVisibility(View.GONE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.ALREADY_SUBSCRIBED)) {
                        button_subscribe.setText(R.string.subscribed_lbl);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        button_subscribe.setEnabled(false);
                        ratingStarsLayout.setVisibility(View.VISIBLE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.RENEWABLE)) {
                        button_subscribe.setText(R.string.renew_lbl);
                        button_subscribe.setEnabled(true);
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        ratingStarsLayout.setVisibility(View.VISIBLE);

                    } else if (header.getSubscribable().equalsIgnoreCase(TariffsFragment.CAN_SUBSCRIBE)) {
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.black));
                        button_subscribe.setText(R.string.subscribe_lbl);
                        button_subscribe.setEnabled(true);
                        ratingStarsLayout.setVisibility(View.GONE);

                    } else {
                        button_subscribe.setTextColor(ContextCompat.getColor(context, R.color.white));
                        button_subscribe.setEnabled(false);
                        ratingStarsLayout.setVisibility(View.GONE);
                    }

                    button_subscribe.setSelected(true);
                }


            }


        }

        void preparePriceItem(PackagePrice packagePriceItem, ViewHolder holder) {
            if (packagePriceItem == null) {
                holder.secondLayout.setVisibility(View.GONE);
                holder.price_belew_line.setVisibility(View.GONE);
                return;
            }

            if (Tools.hasValue(packagePriceItem.getPriceLabel())) {
                holder.prices_title_txt.setText(packagePriceItem.getPriceLabel());
            }

            holder.secondLayout.setVisibility(View.VISIBLE);
            holder.pricesLayout.tv_call_lbl.setText("");
            holder.pricesLayout.tv_call_value.setText("");
            holder.pricesLayout.tv_sms_lbl.setText("");
            holder.pricesLayout.tv_sms_value.setText("");
            holder.pricesLayout.tv_internet_lbl.setText("");
            holder.pricesLayout.tv_internet_value.setText("");
            holder.pricesLayout.tv_InternetSubTitleValue.setText("");
            holder.pricesLayout.tv_InternetSubTitle.setText("");

            if (packagePriceItem.getCall() != null) {
                Tools.setTariffsIcons(packagePriceItem.getCall().getIconName(), holder.pricesLayout.call_icon, context);
                String priceTemplate = "";
                if (Tools.hasValue(packagePriceItem.getCall().getPriceTemplate())) {
                    priceTemplate = packagePriceItem.getCall().getPriceTemplate();
                }
                boolean isDoubleVal = false;
                if (priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)) {
                    holder.pricesLayout.tv_call_left_value.setVisibility(View.VISIBLE);
                    isDoubleVal = true;
                } else {
                    holder.pricesLayout.tv_call_left_value.setVisibility(View.INVISIBLE);
                }
                if (Tools.hasValue(packagePriceItem.getCall().getTitle())) {
                    holder.pricesLayout.tv_call_lbl.setText(packagePriceItem.getCall()
                            .getTitle());
                }
                if (Tools.hasValue(packagePriceItem.getCall().getTitleValueRight())) {
                    holder.pricesLayout.tv_call_value.setText(packagePriceItem.getCall()
                            .getTitleValueRight());
                }
                if (Tools.hasValue(packagePriceItem.getCall().getTitleValueLeft())) {
                    holder.pricesLayout.tv_call_left_value.setText(packagePriceItem.getCall().getTitleValueLeft());
                }

                if (isDoubleVal && Tools.hasValue(packagePriceItem.getCall().getTitleValueLeft())
                        && Tools.hasValue(packagePriceItem.getCall().getTitleValueRight())) {
                    holder.pricesLayout.seperator.setVisibility(View.VISIBLE);
                } else {
                    holder.pricesLayout.seperator.setVisibility(View.GONE);
                }

                if (packagePriceItem.getCall().getAttributes() != null) {
                    addAttributesToCall(packagePriceItem.getCall().getAttributes(),
                            holder.pricesLayout.call_content_layout, priceTemplate);
                }
            }
            if (packagePriceItem.getSms() != null) {
                Tools.setTariffsIcons(packagePriceItem.getSms().getIconName(), holder.pricesLayout.sms_icon, context);

                if (packagePriceItem.getSms().getTitle() != null)
                    holder.pricesLayout.tv_sms_lbl.setText(packagePriceItem.getSms()
                            .getTitle());
                if (packagePriceItem.getSms().getTitleValue() != null)
                    holder.pricesLayout.tv_sms_value.setText(packagePriceItem.getSms()
                            .getTitleValue());
                if (packagePriceItem.getSms().getAttributes() != null)
                    addAttributesToPackagePrice(packagePriceItem.getSms().getAttributes(),
                            holder.pricesLayout.sms_content_layout);
            }

            if (packagePriceItem.getInterent() != null) {
                Tools.setTariffsIcons(packagePriceItem.getInterent().getIconName(), holder.pricesLayout.internet_icon, context);

                if (packagePriceItem.getInterent().getTitle() != null)
                    holder.pricesLayout.tv_internet_lbl.setText(packagePriceItem.getInterent()
                            .getTitle());
                if (packagePriceItem.getInterent().getTitleValue() != null)
                    holder.pricesLayout.tv_internet_value.setText(packagePriceItem
                            .getInterent().getTitleValue());

                if (packagePriceItem.getInterent().getSubTitle() != null)
                    holder.pricesLayout.tv_InternetSubTitle.setText(packagePriceItem.getInterent().getSubTitle());
                if (packagePriceItem.getInterent().getSubTitleValue() != null)
                    holder.pricesLayout.tv_InternetSubTitleValue.setText(packagePriceItem.getInterent().getSubTitleValue());

            }

        }


        private void addAttributesToCall(final ArrayList<Attributes> attributes, final LinearLayout container, final String priceTemplate) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    container.removeAllViews();
                    for (int i = 0; i < attributes.size(); i++) {
                        ViewItemTariffsAttribute viewItemTariffsAttribute = new ViewItemTariffsAttribute(context);
                        viewItemTariffsAttribute.getAttribute_label_text().setText("");
                        viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                        viewItemTariffsAttribute.getCredit_value_txt_2().setText("");
                        boolean isDoubleVal = false;
                        if (priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_TITLES)
                                || priceTemplate.equalsIgnoreCase(Constants.UserCurrentKeyword.PRICE_TEMPLATE_DOUBLE_VALUES)) {
                            viewItemTariffsAttribute.getRl_left().setVisibility(View.VISIBLE);
                            isDoubleVal = true;
                        } else {
                            viewItemTariffsAttribute.getRl_left().setVisibility(View.INVISIBLE);
                        }
                        if (Tools.hasValue(attributes.get(i).getValueLeft())) {
                            viewItemTariffsAttribute.getCredit_value_txt_2().setText(attributes.get(i).getValueLeft());
                        } else {
                            viewItemTariffsAttribute.hideLeftAZN();
                        }
                        if (Tools.hasValue(attributes.get(i).getValueRight())) {
                            viewItemTariffsAttribute.getCredit_value_txt_1().setText(attributes.get(i).getValueRight());
                        } else {
                            viewItemTariffsAttribute.hideRightAZN();
                        }
                        if (isDoubleVal && Tools.hasValue(attributes.get(i).getValueLeft())
                                && Tools.hasValue(attributes.get(i).getValueRight())) {
                            viewItemTariffsAttribute.getSeperator().setVisibility(View.VISIBLE);
                            viewItemTariffsAttribute.getCredit_value_txt_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                            viewItemTariffsAttribute.getTv_tzn_1().setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        } else {
                            viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                        }
                        if (Tools.hasValue(attributes.get(i).getTitle())) {
                            viewItemTariffsAttribute.getAttribute_label_text().setText(attributes.get(i).getTitle());
                        }
                        container.addView(viewItemTariffsAttribute);
                    }
                }
            }, 10);

        }

        void prepareDetailItem(TariffDetails offerDetail, ViewHolder holder) {
            if (offerDetail == null) {
                holder.detail_belew_line.setVisibility(View.GONE);
                holder.thirdLayout.setVisibility(View.GONE);
                return;
            }
            holder.detail_belew_line.setVisibility(View.VISIBLE);
            holder.thirdLayout.setVisibility(View.VISIBLE);

            if (Tools.hasValue(offerDetail.getDetailLabel())) {
                holder.details_title_txt.setText(offerDetail.getDetailLabel());
            }

            TariffsFragment.populateDetailSectionContents(offerDetail, detailLayout, context);

        }

        void prepareDescriptionItem(Description description, ViewHolder holder) {
            if (description == null) {
                holder.fourthLayout.setVisibility(View.GONE);
                holder.desc_belew_line.setVisibility(View.GONE);
                return;
            }

            if (Tools.hasValue(description.getDescLabel())) {
                holder.description_title_txt.setText(description.getDescLabel());
            }

            holder.desc_belew_line.setVisibility(View.VISIBLE);
            holder.fourthLayout.setVisibility(View.VISIBLE);
            holder.descriptionLayout.tv_advantages_description.setText("");
            holder.descriptionLayout.every_subcriber_txt.setText("");
            holder.descriptionLayout.advantage_txt_title.setText("");
            holder.descriptionLayout.clarification_txt_title.setText("");


            if (description.getClassification() != null) {

                if (description.getClassification().getTitle() != null
                        && Tools.hasValue(description.getClassification().getTitle())) {
                    holder.descriptionLayout.clarification_txt_title.setText(
                            description.getClassification().getTitle());
                }

                if (description.getClassification().getDescription() != null
                        && Tools.hasValue(description.getClassification().getDescription())) {
                    holder.descriptionLayout.every_subcriber_txt.setText(description.getClassification()
                            .getDescription());
                }

            }//end of getDescription() null check

            if (description.getAdvantages() != null) {
                if (description.getAdvantages().getTitle() != null
                        && Tools.hasValue(description.getAdvantages().getTitle())) {
                    holder.descriptionLayout.advantage_txt_title.setText(
                            description.getAdvantages().getTitle());
                }
                if (description.getAdvantages().getDescription() != null
                        && Tools.hasValue(description.getAdvantages().getDescription())) {
                    holder.descriptionLayout.tv_advantages_description.setText(description.getAdvantages()
                            .getDescription());
                }
            }


        }//end of method

    }

    /**
     * Adding attributes to PackagePrice layout
     *
     * @param attributes
     * @param container
     */
    private void addAttributesToPackagePrice(final ArrayList<Attributes> attributes, final LinearLayout container) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                container.removeAllViews();
                for (int i = 0; i < attributes.size(); i++) {
                    ViewItemTariffsAttribute viewItemTariffsAttribute = new ViewItemTariffsAttribute(context);
                    viewItemTariffsAttribute.getAttribute_label_text().setText("");
                    viewItemTariffsAttribute.getCredit_value_txt_1().setText("");
                    viewItemTariffsAttribute.getSeperator().setVisibility(View.GONE);
                    if (attributes.get(i).getTitle() != null)
                        viewItemTariffsAttribute.getAttribute_label_text().setText(attributes.get(i).getTitle());
                    if (attributes.get(i).getValue() != null) {
                        viewItemTariffsAttribute.getCredit_value_txt_1()
                                .setText(attributes.get(i).getValue());
                    } else {
                        viewItemTariffsAttribute.hideRightAZN();
                    }
                    container.addView(viewItemTariffsAttribute);
                }
            }
        }, 10);

    }
}
