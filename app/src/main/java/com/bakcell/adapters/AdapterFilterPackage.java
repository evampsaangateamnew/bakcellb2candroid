package com.bakcell.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.bakcell.R;
import com.bakcell.models.supplementaryoffers.OfferFilter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellCheckBox;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Zeeshan Shabbir on 9/18/2017.
 */

public class AdapterFilterPackage extends RecyclerView.Adapter<AdapterFilterPackage.ViewHolder> {
    private Context mContext;
    private OnitemSelection onitemSelection;
    private String type;
    private ArrayList<OfferFilter> offerFilters;
    private ArrayList<OfferFilter> offerFilterArrayList = new ArrayList<>();


    public AdapterFilterPackage(Context mContext, String type,
                                ArrayList<OfferFilter> offerFilters,
                                OnitemSelection onitemSelection) {
        this.mContext = mContext;
        this.type = type;
        this.offerFilters = offerFilters;
        this.onitemSelection = onitemSelection;
    }


    private void addToFilteredIds(OfferFilter offerFilter) {
        if (offerFilterArrayList != null) {
            offerFilterArrayList.add(offerFilter);
        }
    }

    private void removeFromFilteredIds(OfferFilter offerFilter) {
        if (offerFilterArrayList != null) {
            try {
                offerFilterArrayList.remove(offerFilter);
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
        }
    }

    public void selectAll() {
        offerFilterArrayList.clear();
        offerFilterArrayList.addAll(offerFilters);
        notifyDataSetChanged();
    }

    public void deselectAll() {
        offerFilterArrayList.clear();
        notifyDataSetChanged();
    }


    public ArrayList<OfferFilter> getOfferFilters() {
        if (offerFilterArrayList == null)
            return offerFilterArrayList = (ArrayList<OfferFilter>) Collections.EMPTY_LIST;
        return offerFilterArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_filter_item,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Tools.hasValue(offerFilters.get(position).getValue()))
            viewHolder.checkbox.setText(offerFilters.get(position).getValue());
        boolean isSelected = false;
        if (offerFilterArrayList != null) {
            for (int i = 0; i < offerFilterArrayList.size(); i++) {
                if (offerFilterArrayList.get(i).getKey()
                        .equalsIgnoreCase(offerFilters.get(position).getKey())) {
                    isSelected = true;
                    break;
                }
            }
        }

        if (isSelected) {
            viewHolder.checkbox.setChecked(true, false);
        } else {
            viewHolder.checkbox.setChecked(false, false);
        }

        viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //add to filtered array list
                    if (!offerFilterArrayList.contains(offerFilters.get(position))) {
                        addToFilteredIds(offerFilters.get(position));
                    }
                } else {
                    //remove from filtered array list
                    if (offerFilterArrayList.contains(offerFilters.get(position))) {
                        removeFromFilteredIds(offerFilters.get(position));
                    }
                }
                onitemSelection.itemSelected(type, offerFilterArrayList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return offerFilters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        BakcellCheckBox checkbox;

        public ViewHolder(View itemView) {
            super(itemView);
            checkbox = (BakcellCheckBox) itemView.findViewById(R.id.checkbox);
        }
    }

    public interface OnitemSelection {
        void itemSelected(String type, ArrayList<OfferFilter> offerFilters);
    }

    public void alterSelectedFilter(ArrayList<OfferFilter> filters) {
        offerFilterArrayList.clear();
        offerFilterArrayList.addAll(filters);
        notifyDataSetChanged();
        onitemSelection.itemSelected(type, offerFilterArrayList);
    }
}
