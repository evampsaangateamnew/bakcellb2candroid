package com.bakcell.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

/**
 * @author Junaid Hassan on 22, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class ManageAccountsRecyclerAdapter extends RecyclerView.Adapter<ManageAccountsRecyclerAdapter.ViewHolder> {

    private final String fromClass = "ManageAccountsRecyclerAdapter";
    private Activity activity;
    private Context context;
    private ArrayList<UserModel> userModelArrayList;

    public ManageAccountsRecyclerAdapter(Activity activity, Context context, ArrayList<UserModel> userModelArrayList) {
        this.activity = activity;
        this.context = context;
        this.userModelArrayList = userModelArrayList;
    }//constructor ends

    @Override
    public long getItemId(int position) {
        return String.valueOf(position).hashCode();
    }//getItemId ends

    @Override
    public int getItemViewType(int position) {
        return 0;
    }//getItemViewType ends

    @Override
    public int getItemCount() {
        return userModelArrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_manage_accounts_recycler, parent, false);
        return new ViewHolder(v);
    }//onCreateViewHolder ends

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (Tools.hasValue(userModelArrayList.get(position).getMsisdn())) {
            holder.number.setText(userModelArrayList.get(position).getMsisdn());
            holder.number.setSelected(true);
        }

        String firstName = "";
        if (Tools.hasValue(userModelArrayList.get(position).getFirstName())) {
            firstName = userModelArrayList.get(position).getFirstName();
        }

        String lastName = "";
        if (Tools.hasValue(userModelArrayList.get(position).getLastName())) {
            lastName = userModelArrayList.get(position).getLastName();
        }

        /**check if first name has the values,
         * if not then show only the last name
         * will omit the space in this case!*/
        if (Tools.hasValue(firstName)) {
            holder.nameTV.setText(firstName + " " + lastName);
            holder.nameTV.setSelected(true);
        } else {
            holder.nameTV.setText(lastName);
            holder.nameTV.setSelected(true);
        }

        if (Tools.hasValue(userModelArrayList.get(position).getMsisdn())
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())
                && userModelArrayList.get(position).getMsisdn().equalsIgnoreCase(DataManager.getInstance().getCurrentUser().getMsisdn())) {
            holder.ovalIV.setVisibility(View.VISIBLE);
        } else {
            holder.ovalIV.setVisibility(View.INVISIBLE);
        }

        /**hide the very last bottom view below the content holder*/
        if (position == userModelArrayList.size() - 1) {
            holder.bottomView.setVisibility(View.GONE);
        } else {
            holder.bottomView.setVisibility(View.VISIBLE);
        }

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**check if the clicked item is currently logged in,
                 * if then do nothing
                 * else delete the user data*/
                String currentlyLoggedInMsisdn = "";
                if (DataManager.getInstance().getCurrentUser() != null && Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                    currentlyLoggedInMsisdn = DataManager.getInstance().getCurrentUser().getMsisdn();
                } else {
                    if (PrefUtils.getCustomerData(activity) != null &&
                            PrefUtils.getCustomerData(activity).getCustomerData() != null && Tools.hasValue(PrefUtils.getCustomerData(activity).getCustomerData().getMsisdn())) {
                        currentlyLoggedInMsisdn = PrefUtils.getCustomerData(activity).getCustomerData().getMsisdn();
                    }
                }

                if (currentlyLoggedInMsisdn.equalsIgnoreCase(userModelArrayList.get(position).getMsisdn())) {
                    BakcellPopUpDialog.showMessageDialog(
                            activity,
                            activity.getResources().getString(R.string.bakcell_error_title),
                            activity.getResources().getString(R.string.delete_number_error));
                } else if (userModelArrayList.size() == 1) {
                    /**the very last user to delete confirm from the user to delete this user*/
                    confirmDeleteLastAccountLeft(
                            getLocalizedNoteTitle(),
                            getLocalizedLastAccountDeletionMessage(),
                            getLocalizedYesButtonTitle(),
                            getLocalizedNoButtonTitle(),
                            position);
                } else {
                    /**remove the user from the multi accounts list in local preferences*/
                    ArrayList<UserModel> updatedList = MultiAccountsHandler.deleteUser(activity, userModelArrayList.get(position).getMsisdn());
                    if (updatedList.isEmpty()) {
                        //show no data left layout
                        RootValues.getInstance().getManageAccountsShowHideContentsEvents().onShowOrHideContents(false);
                        return;
                    }
                    RootValues.getInstance().getManageAccountsShowHideContentsEvents().onShowOrHideContents(true);
                    userModelArrayList.clear();
                    userModelArrayList.addAll(updatedList);
                    notifyDataSetChanged();
                }
            }
        });

        holder.contentHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**check if the clicked item is currently logged in,
                 * if, then do nothing
                 * else, switch the account*/
                String currentlyLoggedInMsisdn = "";
                if (DataManager.getInstance().getCurrentUser() != null && Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                    currentlyLoggedInMsisdn = DataManager.getInstance().getCurrentUser().getMsisdn();
                } else {
                    if (PrefUtils.getCustomerData(activity) != null &&
                            PrefUtils.getCustomerData(activity).getCustomerData() != null && Tools.hasValue(PrefUtils.getCustomerData(activity).getCustomerData().getMsisdn())) {
                        currentlyLoggedInMsisdn = PrefUtils.getCustomerData(activity).getCustomerData().getMsisdn();
                    }
                }

                if (currentlyLoggedInMsisdn.equalsIgnoreCase(userModelArrayList.get(position).getMsisdn())) {
                    //do nothing, item is the currently logged in user
                    BakcellPopUpDialog.showMessageDialog(activity, getLocalizedNoteTitle(),
                            getLocalizedUserAlreadySelectedMessage());
                } else {
                    RootValues.getInstance().getManageAccountSwitchingEvents().
                            onAccountSwitchFromManageAccountsRecycler(userModelArrayList.get(position));
                }
            }
        });
    }//onBindViewHolder ends

    private String getLocalizedNoButtonTitle() {
        String title;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            title = "NO";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            title = "XEYR";
        } else {
            title = "НЕТ";
        }
        return title;
    }//getLocalizedNoButtonTitle ends

    private String getLocalizedYesButtonTitle() {
        String title;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            title = "OK";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            title = "BƏLİ";
        } else {
            title = "ДА";
        }
        return title;
    }//getLocalizedYesButtonTitle ends

    private String getLocalizedLastAccountDeletionMessage() {
        String title;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            title = "Are you sure you want to remove last number?";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            title = "Son nömrəni silmək istədiyinizdən əminsiniz?";
        } else {
            title = "Вы уверены, что хотите удалить последний номер?";
        }
        return title;
    }//getLocalizedLastAccountDeletionMessage ends

    private String getLocalizedNoteTitle() {
        String title;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            title = "Note";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            title = "Qeyd";
        } else {
            title = "К сведению";
        }
        return title;
    }//getLocalizedNoteTitle ends

    private void confirmDeleteLastAccountLeft(String title, String message, String yesButtonTitle, String noButtonTitle, final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.last_account_deletion_confirmation_popup, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        BakcellTextViewNormal confirmationMessage = dialogView.findViewById(R.id.confirmationMessage);
        BakcellTextViewBold dialog_title = dialogView.findViewById(R.id.dialog_title);
        BakcellButtonNormal btn_no = dialogView.findViewById(R.id.btn_no);
        BakcellButtonNormal btn_yes = dialogView.findViewById(R.id.btn_yes);

        dialog_title.setText(title);
        confirmationMessage.setText(message);
        btn_no.setText(noButtonTitle);
        btn_yes.setText(yesButtonTitle);

        btn_yes.setOnClickListener(v -> {
            alertDialog.dismiss();
            MultiAccountsHandler.deleteAllUsers(activity);
            if (MultiAccountsHandler.getCustomerDataFromPreferences(activity).userModelArrayList.isEmpty()) {
                PrefUtils.addBoolean(context, PrefUtils.PreKeywords.PREF_IS_MANAGE_ACCOUNTS_SCREEN_NEED_TO_SHOW, false);
                HomeActivity.logoutUser(activity);
            } else {
                notifyDataSetChanged();
            }
        });

        btn_no.setOnClickListener(v -> alertDialog.dismiss());
    }//confirmDeleteLastAccountLeft ends

    private String getLocalizedUserAlreadySelectedMessage() {
        String title;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            title = "User already selected.";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            title = "İstifadəçi artıq seçilib.";
        } else {
            title = "Пользователь уже выбран.";
        }
        return title;
    }//getLocalizedUserAlreadySelectedMessage ends

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ovalIV;
        BakcellTextViewNormal nameTV;
        BakcellTextViewNormal number;
        View bottomView;
        ImageView deleteButton;
        ConstraintLayout contentHolder;

        public ViewHolder(View itemView) {
            super(itemView);
            contentHolder = itemView.findViewById(R.id.contentHolder);
            deleteButton = itemView.findViewById(R.id.deleteButton);
            bottomView = itemView.findViewById(R.id.bottomView);
            ovalIV = itemView.findViewById(R.id.ovalIV);
            nameTV = itemView.findViewById(R.id.nameTV);
            number = itemView.findViewById(R.id.number);
        }//ViewHolder ends
    }//class ViewHolder ends
}//class ends
