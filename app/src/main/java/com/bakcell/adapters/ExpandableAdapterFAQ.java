package com.bakcell.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.models.faq.Faqs;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 6/5/2017.
 */

public class ExpandableAdapterFAQ extends BaseExpandableListAdapter implements Filterable {
    private ArrayList<Faqs> tempFaqs;
    private ArrayList<Faqs> faqsArrayList;
    private Context context;
    private FAQFilter faqFilter;

    public ExpandableAdapterFAQ(Context context, ArrayList<Faqs> faqsArrayList) {
        this.context = context;
        this.tempFaqs = faqsArrayList;
        this.faqsArrayList = faqsArrayList;
    }

    @Override
    public int getGroupCount() {
        if (faqsArrayList == null) return 0;
        return faqsArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Faqs getGroup(int groupPosition) {
        return faqsArrayList.get(groupPosition);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        return faqsArrayList.get(groupPosition).getAnswer();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.faq_item, null);
            viewHolder.tvFaqTitle = (BakcellTextViewNormal) convertView.findViewById(R.id.tv_title);
            viewHolder.ivIndicator = (ImageView) convertView.findViewById(R.id.iv_group_indicator);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (Tools.hasValue(faqsArrayList.get(groupPosition).getQuestion())) {
            viewHolder.tvFaqTitle.setText(faqsArrayList.get(groupPosition).getQuestion());
        } else {
            viewHolder.tvFaqTitle.setText("");
        }

        if (isExpanded) {
            viewHolder.ivIndicator.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_minu_sign1));
        } else {
            viewHolder.ivIndicator.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_plus_sign1));
        }

        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.fag_child_item, null);
            viewHolder.tvFagDetail = (BakcellTextViewNormal)
                    convertView.findViewById(R.id.tv_fag_detail);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        if (Tools.hasValue(faqsArrayList.get(groupPosition).getAnswer())) {
            viewHolder.tvFagDetail.setText(faqsArrayList.get(groupPosition).getAnswer());
        } else {
            viewHolder.tvFagDetail.setText("");
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void refreshList(ArrayList<Faqs> faqsArrayList) {
        if (faqsArrayList == null) return;
        this.faqsArrayList = faqsArrayList;
        this.tempFaqs = faqsArrayList;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        if (faqFilter == null)
            faqFilter = new FAQFilter();
        return faqFilter;
    }

    public class ViewHolder {
        protected BakcellTextViewNormal tvFaqTitle, tvFagDetail;
        protected ImageView ivIndicator;
    }

    class FAQFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                if (constraint.length() == 0) {
                    results.values = tempFaqs;
                    results.count = tempFaqs.size();
                } else {
                    ArrayList<Faqs> filterList = new ArrayList<>();
                    for (Faqs faq : tempFaqs) {
                        if (faq.getAnswer().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            filterList.add(faq);
                        }
                    }
                    results.count = filterList.size();
                    results.values = filterList;

                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            faqsArrayList = (ArrayList<Faqs>) results.values;
            notifyDataSetChanged();
        }
    }
}
