package com.bakcell.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bakcell.R;
import com.bakcell.models.dashboard.installments.Installments;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.text.ParseException;
import java.util.ArrayList;


public class CardAdapterMyInstallmentsItem extends RecyclerView.Adapter<CardAdapterMyInstallmentsItem.ViewHolder> {

    private ArrayList<Installments> installmentsArrayList;
    private Context context;

    public CardAdapterMyInstallmentsItem(Context context, ArrayList<Installments> arrayList) {
        this.installmentsArrayList = arrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_item_my_installments, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {


        viewHolder.prepareListItem(context, installmentsArrayList.get(i), i);

    }


    @Override
    public int getItemCount() {
        return installmentsArrayList.size();
    }

    public void updateAdapter(ArrayList<Installments> arrayList) {

        installmentsArrayList.addAll(arrayList);

        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        BakcellTextViewBold installment_title_txt;
        BakcellTextViewNormal inst_amount_title_txt, creditValueTxt, inst_puchasedate_title_txt, inst_purchase_date_value_txt;

        BakcellTextViewNormal next_pay_title, next_pay_days_title, next_date_txt;
        ProgressBar next_progress;

        BakcellTextViewNormal remaining_title, tv_amount_1, tv_amount_2;
        ProgressBar remaining_progress;

        BakcellTextViewNormal period_title, period_value_title, period_begin_date, period_end_date;
        ProgressBar period_progress;

        public ViewHolder(View itemView) {
            super(itemView);
            installment_title_txt = (BakcellTextViewBold) itemView.findViewById(R.id.installment_title_txt);
            inst_amount_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.inst_amount_title_txt);
            creditValueTxt = (BakcellTextViewNormal) itemView.findViewById(R.id.creditValueTxt);
            inst_puchasedate_title_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.inst_puchasedate_title_txt);
            inst_purchase_date_value_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.inst_purchase_date_value_txt);

            next_pay_title = (BakcellTextViewNormal) itemView.findViewById(R.id.next_pay_title);
            next_pay_days_title = (BakcellTextViewNormal) itemView.findViewById(R.id.next_pay_days_title);
            next_date_txt = (BakcellTextViewNormal) itemView.findViewById(R.id.next_date_txt);
            next_progress = (ProgressBar) itemView.findViewById(R.id.next_progress);

            remaining_title = (BakcellTextViewNormal) itemView.findViewById(R.id.remaining_title);
            tv_amount_1 = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_amount_1);
            tv_amount_2 = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_amount_2);
            remaining_progress = (ProgressBar) itemView.findViewById(R.id.remaining_progress);

            period_title = (BakcellTextViewNormal) itemView.findViewById(R.id.period_title);
            period_value_title = (BakcellTextViewNormal) itemView.findViewById(R.id.period_value_title);
            period_begin_date = (BakcellTextViewNormal) itemView.findViewById(R.id.period_begin_date);
            period_end_date = (BakcellTextViewNormal) itemView.findViewById(R.id.period_end_date);
            period_progress = (ProgressBar) itemView.findViewById(R.id.period_progress);

            period_begin_date.setSelected(true);
            period_end_date.setSelected(true);

        }

        public void prepareListItem(final Context context, Installments installments, final int i) {
            if (installments == null) return;

            if (Tools.hasValue(installments.getName())) {
                installment_title_txt.setText(installments.getName());
            } else {
                installment_title_txt.setText("");
            }

            if (Tools.hasValue(installments.getAmountLabel())) {
                inst_amount_title_txt.setText(installments.getAmountLabel());
            } else {
                inst_amount_title_txt.setText("");
            }

            if (Tools.hasValue(installments.getAmountValue())) {
                creditValueTxt.setText(installments.getAmountValue());
            } else {
                creditValueTxt.setText("");
            }

            if (Tools.hasValue(installments.getPurchaseDateLabel())) {
                inst_puchasedate_title_txt.setText(installments.getPurchaseDateLabel() + ":");
            } else {
                inst_puchasedate_title_txt.setText("");
            }

            if (Tools.hasValue(installments.getPurchaseDateValue())) {
                try {
                    inst_purchase_date_value_txt.setText(Tools.getDateAccordingToClientSaid(installments.getPurchaseDateValue()));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            } else {
                inst_purchase_date_value_txt.setText("");
            }


            // Progress bars Next Payment
            if (Tools.hasValue(installments.getNextPaymentLabel())) {
                next_pay_title.setText(installments.getNextPaymentLabel());
            } else {
                next_pay_title.setText("");
            }

            String startDate = installments.getNextPaymentInitialDate();
            String endDate = installments.getNextPaymentValue();

            long startMili = Tools.getMiliSecondsFromDate(startDate);
            long endMili = Tools.getMiliSecondsFromDate(endDate);
//            endMili = Tools.getOneDayPlus(endMili);
            long currentMili = Tools.getCurrentMiliSeconds();

            int daysDiffCurrent = Tools.getDaysDifferenceBetweenMiliseconds(startMili, currentMili);
            int daysDiffTotal = Tools.getDaysDifferenceBetweenMiliseconds(startMili, endMili);

            int daysDiffCurrentEnd = Tools.getDaysDifferenceBetweenMiliseconds(currentMili, endMili);

            if (Tools.hasValue(endDate)) {
                try {
                    next_date_txt.setText(Tools.getDateAccordingToClientSaid(endDate));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            } else {
                next_date_txt.setText("");
            }
            //
            if (daysDiffTotal > -1) {

                int days = Tools.getNumberOfDaysInCurrentMonth() - Tools.getCurrentDayOfCurrentMonth();
                if (days < 0) {
                    days = 0;
                }
                next_pay_days_title.setText(context.getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, days, days));
                if (Tools.isNumeric(installments.getInstallmentFeeLimit()) && days < Tools.getDoubleFromString(installments.getInstallmentFeeLimit())) {
                    next_pay_days_title.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                } else {
                    next_pay_days_title.setTextColor(ContextCompat.getColor(context, R.color.black));
                }
            } else {
                next_pay_days_title.setText("");
            }

            next_progress.setMax(Tools.getNumberOfDaysInCurrentMonth());
            next_progress.setProgress((Tools.getCurrentDayOfCurrentMonth() - 1));

            //////

            // Remaining Amount
            if (Tools.hasValue(installments.getRemainingAmountLabel())) {
                remaining_title.setText(installments.getRemainingAmountLabel());
            } else {
                remaining_title.setText("");
            }
            if (Tools.hasValue(installments.getRemainingAmountCurrentValue())) {
                tv_amount_1.setText(installments.getRemainingAmountCurrentValue());
            } else {
                tv_amount_1.setText("");
            }
            if (Tools.hasValue(installments.getRemainingAmountTotalValue())) {
                tv_amount_2.setText(" / " + installments.getRemainingAmountTotalValue());
            } else {
                tv_amount_2.setText("");
            }
            if (Tools.hasValue(installments.getRemainingAmountCurrentValue()) && Tools.hasValue(installments.getRemainingAmountTotalValue()) && Tools.isNumeric(installments.getRemainingAmountCurrentValue()) && Tools.isNumeric(installments.getRemainingAmountTotalValue())) {
                remaining_progress.setMax(Tools.getIntegerFromString(installments.getRemainingAmountTotalValue()));
                int remaingVal = Tools.getIntegerFromString(installments.getRemainingAmountTotalValue()) - Tools.getIntegerFromString(installments.getRemainingAmountCurrentValue());
                remaining_progress.setProgress(remaingVal);
            } else {
                remaining_progress.setMax(100);
                remaining_progress.setProgress(0);
            }

            // Remaining Period
            if (Tools.hasValue(installments.getRemainingPeriodLabel())) {
                period_title.setText(installments.getRemainingPeriodLabel());
            } else {
                period_title.setText("");
            }

            String beginDatelbl = "", deginDateValue = "", endDatelbl = "", endDateValue = "";

            if (Tools.hasValue(installments.getRemainingPeriodBeginDateLabel())) {
                beginDatelbl = installments.getRemainingPeriodBeginDateLabel();
            }
            if (Tools.hasValue(installments.getRemainingPeriodBeginDateValue())) {
                deginDateValue = installments.getRemainingPeriodBeginDateValue();
            }

            if (Tools.hasValue(installments.getRemainingPeriodEndDateLabel())) {
                endDatelbl = installments.getRemainingPeriodEndDateLabel();
            }
            if (Tools.hasValue(installments.getRemainingPeriodEndDateValue())) {
                endDateValue = installments.getRemainingPeriodEndDateValue();
            }
            try {
                period_begin_date.setText(beginDatelbl + ":" + Tools.getDateAccordingToClientSaid(deginDateValue));
                period_begin_date.setSelected(true);
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            try {
                period_end_date.setText(endDatelbl + ":" + Tools.getDateAccordingToClientSaid(endDateValue));
                period_end_date.setSelected(true);
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            String currentValue = "", totalValue = "";
            if (Tools.hasValue(installments.getRemainingCurrentPeriod())) {
                currentValue = installments.getRemainingCurrentPeriod();
            }
            if (Tools.hasValue(installments.getRemainingTotalPeriod())) {
                totalValue = installments.getRemainingTotalPeriod();
            }
            period_value_title.setText(currentValue + " / " + totalValue);

            if (Tools.hasValue(currentValue) && Tools.hasValue(totalValue) && Tools.isNumeric(currentValue) && Tools.isNumeric(totalValue)) {
                period_progress.setMax(Tools.getIntegerFromString(totalValue));
                int remaingVal = Tools.getIntegerFromString(totalValue) - Tools.getIntegerFromString(currentValue);
                period_progress.setProgress(remaingVal);
            } else {
                period_progress.setMax(100);
                period_progress.setProgress(0);
            }

        }
    }


}
