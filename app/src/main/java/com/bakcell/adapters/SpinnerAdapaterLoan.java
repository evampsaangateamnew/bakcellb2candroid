package com.bakcell.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Zeeshan Shabbir on 6/9/2017.
 */

public class SpinnerAdapaterLoan extends BaseAdapter {
    Context context;
    String items[] = {};
    private int lastSelectedPos = -1;
    private boolean isOpened = false;

    public SpinnerAdapaterLoan(Context context, String[] item) {
        this.context = context;
        this.items = item;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.loan_spinner_item, null);
            viewHolder.filterTitle = (BakcellTextViewNormal) convertView
                    .findViewById(R.id.tv_filter_title);
            viewHolder.ivFilterIndicator = (ImageView) convertView.findViewById(R.id.iv_indicator);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setVisibility(View.VISIBLE);
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_red));
        } else if (!isOpened && position == 0) {
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else if (!isOpened && position == lastSelectedPos) {
            viewHolder.ivFilterIndicator.setImageDrawable(ContextCompat
                    .getDrawable(context, R.drawable.drop_down_arrow_state_grey));
        } else {
            viewHolder.ivFilterIndicator.setVisibility(View.INVISIBLE);
        }
        viewHolder.filterTitle.setText(items[position]);
        return convertView;
    }

    class ViewHolder {
        protected BakcellTextViewNormal filterTitle;
        protected ImageView ivFilterIndicator;
    }
}
