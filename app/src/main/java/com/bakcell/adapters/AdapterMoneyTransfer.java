package com.bakcell.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.models.user.userpredefinedobjects.PredefinedDataItem;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 9/19/2017.
 */

public class AdapterMoneyTransfer extends RecyclerView.Adapter<AdapterMoneyTransfer.ViewHolder> {
    private ArrayList<PredefinedDataItem> moneyTransfers;

    public AdapterMoneyTransfer(ArrayList<PredefinedDataItem> moneyTransfers) {
        this.moneyTransfers = moneyTransfers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_money_transfer,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvText.setText(moneyTransfers.get(position).getText());
        holder.tvAmount.setText(moneyTransfers.get(position).getAmount());
    }

    @Override
    public int getItemCount() {
        return moneyTransfers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        BakcellTextViewNormal tvNumber, tvText, tvAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            tvText = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_text);
            tvAmount = (BakcellTextViewNormal) itemView.findViewById(R.id.tv_amount);
        }
    }
}
