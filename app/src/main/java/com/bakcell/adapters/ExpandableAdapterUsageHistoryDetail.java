package com.bakcell.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.bakcell.R;
import com.bakcell.models.usagehistory.details.UsageHistoryDetails;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.ArrayList;

public class ExpandableAdapterUsageHistoryDetail extends BaseExpandableListAdapter {

    private Context context;
    ArrayList<UsageHistoryDetails> usageDetailArrayList;

    public ExpandableAdapterUsageHistoryDetail(Context context, ArrayList<UsageHistoryDetails> arrayList) {
        this.context = context;
        this.usageDetailArrayList = arrayList;
    }

    public void updateList(ArrayList<UsageHistoryDetails> arrayList) {
        usageDetailArrayList = new ArrayList<>();
        this.usageDetailArrayList.addAll(arrayList);
        notifyDataSetChanged();
    }

    public class ViewHolderGroup {
        BakcellTextViewNormal date_time_txt, service_txt, usage_txt, charge_txt, tv_azn;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        ViewHolderGroup viewHolderGroup = null;

        if (convertView == null) {
            viewHolderGroup = new ViewHolderGroup();
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_exp_adapter_header_usage_detail, null);
            viewHolderGroup.date_time_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.date_time_txt);
            viewHolderGroup.service_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.service_txt);
            viewHolderGroup.usage_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.usage_txt);
            viewHolderGroup.charge_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.charge_txt);
            viewHolderGroup.tv_azn = (BakcellTextViewNormal) convertView.findViewById(R.id.tv_azn);
            convertView.setTag(viewHolderGroup);
        } else {
            viewHolderGroup = (ViewHolderGroup) convertView.getTag();
        }


        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getEndDateTime())) {
            viewHolderGroup.date_time_txt.setText(usageDetailArrayList.get(groupPosition).getEndDateTime());
        } else {
            viewHolderGroup.date_time_txt.setText("");
        }

        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getService())) {
            viewHolderGroup.service_txt.setText(usageDetailArrayList.get(groupPosition).getService());
        } else {
            viewHolderGroup.service_txt.setText("");
        }

        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getUsage())) {
            viewHolderGroup.usage_txt.setText(usageDetailArrayList.get(groupPosition).getUsage());
        } else {
            viewHolderGroup.usage_txt.setText("");
        }

        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getChargedAmount())) {
            viewHolderGroup.charge_txt.setText(usageDetailArrayList.get(groupPosition).getChargedAmount());
            viewHolderGroup.tv_azn.setVisibility(View.VISIBLE);
        } else {
            viewHolderGroup.charge_txt.setText("");
            viewHolderGroup.tv_azn.setVisibility(View.GONE);
        }
        return convertView;
    }


    public class ViewHolderChild {
        BakcellTextViewNormal number_txt, peak_txt, zone_txt, destination_txt;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolderChild viewHolderChild = null;

        if (convertView == null) {
            viewHolderChild = new ViewHolderChild();
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_exp_adapter_sub_usage_detail, null);

            viewHolderChild.number_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.number_txt);
            viewHolderChild.peak_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.peak_txt);
            viewHolderChild.zone_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.zone_txt);
            viewHolderChild.destination_txt = (BakcellTextViewNormal) convertView.findViewById(R.id.destination_txt);

            convertView.setTag(viewHolderChild);
        } else {
            viewHolderChild = (ViewHolderChild) convertView.getTag();
        }

        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getNumber())) {
            viewHolderChild.number_txt.setText(usageDetailArrayList.get(groupPosition).getNumber());
        } else {
            viewHolderChild.number_txt.setText("");
        }

        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getPeriod())) {
            viewHolderChild.peak_txt.setText(usageDetailArrayList.get(groupPosition).getPeriod());
        } else {
            viewHolderChild.peak_txt.setText("");
        }
        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getZone())) {
            viewHolderChild.zone_txt.setText(usageDetailArrayList.get(groupPosition).getZone());
        } else {
            viewHolderChild.zone_txt.setText("");
        }
        if (Tools.hasValue(usageDetailArrayList.get(groupPosition).getDestination())) {
            viewHolderChild.destination_txt.setText(usageDetailArrayList.get(groupPosition).getDestination());
        } else {
            viewHolderChild.destination_txt.setText("");
        }

        return convertView;
    }


    @Override
    public String getChild(int groupPosition, int childPosititon) {
        return this.usageDetailArrayList.get(groupPosition).getService();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.usageDetailArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.usageDetailArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}