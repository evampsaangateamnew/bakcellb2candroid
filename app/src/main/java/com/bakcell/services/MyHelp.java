package com.bakcell.services;


import android.util.Base64;

import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//import org.apache.commons.codec.binary.Base64;


public class MyHelp {


    ////////////////////////////////////////////////////////////
    public static String encrypt(String input, String keySec) throws Exception {
        try {
            if (input == null) return "";
            byte[] keySecPass = keySec.getBytes();

            byte[] data = input.getBytes();

            SecretKeySpec key = new SecretKeySpec(keySecPass, "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");

            // encryption pass
            cipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] cipherText = new byte[cipher.getOutputSize(data.length)];

            int ctLength = cipher.update(data, 0, data.length, cipherText, 0);
            ctLength += cipher.doFinal(cipherText, ctLength);

            String encrited = "";

            try {
                encrited = new String(Base64.encode(cipherText, Base64.DEFAULT));
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            return encrited;
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return "";
    }

    public static String decrypt(String encryptedData, String keySec) throws Exception {
        try {
            byte[] keySecPass = keySec.getBytes();

            byte[] input = Base64.decode(encryptedData.getBytes(), Base64.DEFAULT);

            SecretKeySpec key = new SecretKeySpec(keySecPass, "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");

            cipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = new byte[cipher.getOutputSize(input.length)];
            int ptLength = cipher.update(input, 0, input.length, plainText, 0);
            ptLength += cipher.doFinal(plainText, ptLength);

            String simplText = new String(plainText);

            if (Tools.hasValue(simplText)) {
                simplText = simplText.trim();
            }

            return simplText;
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return "";
    }


}