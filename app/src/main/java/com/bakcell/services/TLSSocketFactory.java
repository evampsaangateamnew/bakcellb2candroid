package com.bakcell.services;


import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 * Created by Ibraheem on 03/05/2016.
 */
public class TLSSocketFactory extends SSLSocketFactory {

    private SSLSocketFactory internalSSLSocketFactory;

//    public TLSSocketFactory(KeyManager[] km, MyX509TrustManager[] tm, SecureRandom sr) throws KeyManagementException, NoSuchAlgorithmException {
//        SSLContext context = SSLContext.getInstance("TLSv1.2");
//        context.init(km, tm, sr);
//        internalSSLSocketFactory = context.getSocketFactory();
//    }//end of constructor

    @Override
    public String[] getDefaultCipherSuites() {
        return internalSSLSocketFactory.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return internalSSLSocketFactory.getSupportedCipherSuites();
    }

    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        return enableTLSOnSocket(internalSSLSocketFactory.createSocket(s, host, port, autoClose));
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        return enableTLSOnSocket(internalSSLSocketFactory.createSocket(host, port));
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
        return enableTLSOnSocket(internalSSLSocketFactory.createSocket(host, port, localHost, localPort));
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return enableTLSOnSocket(internalSSLSocketFactory.createSocket(host, port));
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return enableTLSOnSocket(internalSSLSocketFactory.createSocket(address, port, localAddress, localPort));
    }

    private Socket enableTLSOnSocket(Socket socket) {
        if (socket != null && (socket instanceof SSLSocket)) {
            ((SSLSocket) socket).setEnabledProtocols(new String[]{"TLSv1.2"});
            String[] tempArr = internalSSLSocketFactory.getDefaultCipherSuites();
            ArrayList<String> suites = new ArrayList<>();
            for (int i = 0; i < tempArr.length; i++) {
                if (tempArr[i].equalsIgnoreCase("TLS_RSA_WITH_AES_128_GCM_SHA256") ||
                        tempArr[i].equalsIgnoreCase("TLS_RSA_WITH_AES_256_GCM_SHA384") ||
                        tempArr[i].equalsIgnoreCase("TLS_RSA_WITH_AES_128_CBC_SHA") ||
                        tempArr[i].equalsIgnoreCase("TLS_RSA_WITH_AES_256_CBC_SHA")) {
                    suites.add(tempArr[i]);
                    //Constants.logMessage("Cipher: " ,"" + tempArr[i]);
                }
            }
            //for(String s : suites)
            // Constants.logMessage("Cipher ", s);
            String[] enabledSuites = new String[suites.size()];
            enabledSuites = suites.toArray(enabledSuites);
//           Constants.logMessage("Cipher size new" ,"" + enabledSuites.length);
            ((SSLSocket) socket).setEnabledCipherSuites(enabledSuites);
        }
        return socket;
    }
}