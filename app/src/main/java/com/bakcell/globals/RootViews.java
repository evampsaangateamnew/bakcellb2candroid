package com.bakcell.globals;

import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Noman on 02-Nov-17.
 */

public class RootViews {
    private RootViews() {
    }

    private static RootViews instance = null;

    public static RootViews getInstance() {
        if (instance == null) {
            instance = new RootViews();
        }
        return instance;
    }

    // Dashboard Counter TextView
    private BakcellTextViewNormal notificationCountText;

    public BakcellTextViewNormal getNotificationCountText() {
        return notificationCountText;
    }

    public void setNotificationCountText(BakcellTextViewNormal notificationCountText) {
        this.notificationCountText = notificationCountText;
    }


}
