package com.bakcell.globals;

import android.graphics.Typeface;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.bakcell.interfaces.BioMetricAuthenticationEvents;
import com.bakcell.interfaces.CallingSurveyFromDialog;
import com.bakcell.interfaces.CoreServiceStatusChangeListener;
import com.bakcell.interfaces.EmailChangedListener;
import com.bakcell.interfaces.LocationOnListener;
import com.bakcell.interfaces.ManageAccountSwitchingEvents;
import com.bakcell.interfaces.ManageAccountsShowHideContentsEvents;
import com.bakcell.interfaces.NotificationCounterPopupEvents;
import com.bakcell.interfaces.OfferFilterListener;
import com.bakcell.interfaces.OnProfileImageUpdate;
import com.bakcell.interfaces.PauseInUsageDetailPageListener;
import com.bakcell.interfaces.RedirectMapFromStoreListListener;
import com.bakcell.interfaces.RedirectMapListener;
import com.bakcell.interfaces.UlduzumLocationsListFilterListener;
import com.bakcell.interfaces.UlduzumMapLocationRedirectionListener;
import com.bakcell.interfaces.UsageHistoryDetailLoadAction;
import com.bakcell.models.menus.MenuHorizontalItem;
import com.bakcell.models.menus.MenuVerticalItem;
import com.bakcell.models.mysubscriptions.SubscriptionsMain;
import com.bakcell.models.supplementaryoffers.OfferFilter;

import java.util.ArrayList;

/**
 * Created by Noman on 02-Nov-17.
 */

public class RootValues {
    private RootValues() {
    }

    private static RootValues instance = null;

    public static RootValues getInstance() {
        if (instance == null) {
            instance = new RootValues();
        }
        return instance;
    }

    //--------------Values for Used here according to SonarQube--------------//
    private Typeface fontArialRegular = null;
    private Typeface fontArialBold = null;

    private boolean isDynamicLinkRedirection = false;

    // Home page currently selected fragment ID
    private String LAST_SELECT_MENU = "";
    // This Variable decide if app goes to backgroub, close or killed, And if OTP Verified
    private boolean IS_USAGE_DETAIL_OTP_VERIFY = false;
    // Set Supplementary Offer Orientation latest selected VAR
    private int OFFERS_LIST_ORIENTATION = LinearLayoutManager.VERTICAL;
    // Supplementary Offers filtered list
    private ArrayList<OfferFilter> offerFiltersCheckedList = new ArrayList<>();
    // Tariff Fragment Value used for redirection from dashborad
    private boolean isTariffFromDashBoard = false;
    // This value is used for redirection from notification to tariff
    private String offeringIdRedirectFromNotifications = "";


    private String loanCredit = "";

    // Interfaces Listeners
    private UsageHistoryDetailLoadAction usageHistoryDetailLoadActionListener;
    private PauseInUsageDetailPageListener pauseInUsageDetailPageListener;

    // Supplementary Offer Filter Listeners
    private OfferFilterListener offerFilterListenerCall;
    private OfferFilterListener offerFilterListenerInternet;
    private OfferFilterListener offerFilterListenerSms;
    private OfferFilterListener offerFilterListenerCampaign;
    private OfferFilterListener offerFilterListenerTm;
    private OfferFilterListener offerFilterListenerHybrid;
    private CallingSurveyFromDialog callingSurveyFromDialog;

    /**bottom horizontal and side menu vertical menus
     * as of 26 July 2019, coded by junaid.hassan@evampsaanga.com*/
    private ArrayList<MenuHorizontalItem> menuHorizontalItems;
    private  ArrayList<MenuVerticalItem> menuVerticalItems;

    // Lintener for email change
    private EmailChangedListener emailChangedListener;

    // Lintener for Map Redirection
    private RedirectMapListener redirectMapListener;
    private RedirectMapFromStoreListListener redirectMapFromStoreListListener;

    //Listener for profile Image
    private OnProfileImageUpdate onProfileImageUpdate;

    // Listener on location turn ON
    private LocationOnListener locationOnListener;

    // This Veriable represent that is user loggedIn and App is open after closed
    private boolean IS_APP_OPENED_AFTER_CLOSED = false;

    // Intend Inject Check random value
    private String TIME_STAMP_FOR_INTENT_INJECTION = String.valueOf(System.currentTimeMillis());

    // Vlaues That are decide at run time according to device screen resolution
    private float DASHBOARD_CIRCLE_WIDTH = 120;
    private float BACK_CIRCULAR_WIDTH = 5;
    private float CIRCULAR_WIDTH = 10;
    private int TARIFF_PAGGER_OFFSET = 30;
    private int TARIFFS_WIDTH = 200;

    // Supplemnatary Offers Filters List for Phone, Tab and Destop
    private ArrayList<OfferFilter> phoneFilter = null;
    private ArrayList<OfferFilter> tabFilter = null;
    private ArrayList<OfferFilter> tvFilter = null;

    // SO filter group filter  variable
    private String filterGroupAll = null;

    // This Value is used
    private String CLASS_NAME_APP = "";

    // My Subscription data
    private SubscriptionsMain subscriptionsMain;

    // Testing Needed Values
    private String CLASS_NAME_M = "";
    private String CLASS_NAME_SE = "";

    private boolean isPromoMessageDisplayed = false;
    // Flags Boolean Variaables for Session base cache APIs
    private boolean isTariffApiCall = true;
    private boolean isSupplementaryOffersApiCall = true;
    private boolean isDashboardApiCall = true;
    private boolean isMySubscriptionsApiCall = true;
    private boolean isMyStoreLocatorApiCall = true;
    private boolean isFaqsApiCall = true;
    private boolean isContactUsApiCall = true;
    private BioMetricAuthenticationEvents fingerPrintScannerToLoginCallBack = null;
    private CoreServiceStatusChangeListener coreServiceStatusChangeListener = null;
    private UlduzumMapLocationRedirectionListener ulduzumMapLocationRedirectionListener = null;
    private UlduzumLocationsListFilterListener ulduzumLocationsListFilterListener = null;
    private ManageAccountSwitchingEvents manageAccountSwitchingEvents = null;
    private ManageAccountsShowHideContentsEvents manageAccountsShowHideContentsEvents = null;
    private NotificationCounterPopupEvents notificationCounterPopupEvents=null;

    public NotificationCounterPopupEvents getNotificationCounterPopupEvents() {
        return notificationCounterPopupEvents;
    }

    public void setNotificationCounterPopupEvents(NotificationCounterPopupEvents notificationCounterPopupEvents) {
        this.notificationCounterPopupEvents = notificationCounterPopupEvents;
    }

    public ArrayList<MenuHorizontalItem> getMenuHorizontalItems() {
        return menuHorizontalItems;
    }

    public void setMenuHorizontalItems(ArrayList<MenuHorizontalItem> menuHorizontalItems) {
        this.menuHorizontalItems = menuHorizontalItems;
    }

    public ArrayList<MenuVerticalItem> getMenuVerticalItems() {
        return menuVerticalItems;
    }

    public void setMenuVerticalItems(ArrayList<MenuVerticalItem> menuVerticalItems) {
        this.menuVerticalItems = menuVerticalItems;
    }

    public ManageAccountsShowHideContentsEvents getManageAccountsShowHideContentsEvents() {
        return manageAccountsShowHideContentsEvents;
    }

    public void setManageAccountsShowHideContentsEvents(ManageAccountsShowHideContentsEvents manageAccountsShowHideContentsEvents) {
        this.manageAccountsShowHideContentsEvents = manageAccountsShowHideContentsEvents;
    }

    public ManageAccountSwitchingEvents getManageAccountSwitchingEvents() {
        return manageAccountSwitchingEvents;
    }

    public void setManageAccountSwitchingEvents(ManageAccountSwitchingEvents manageAccountSwitchingEvents) {
        this.manageAccountSwitchingEvents = manageAccountSwitchingEvents;
    }

    public boolean isPromoMessageDisplayed() {
        return isPromoMessageDisplayed;
    }

    public void setPromoMessageDisplayed(boolean promoMessageDisplayed) {
        isPromoMessageDisplayed = promoMessageDisplayed;
    }

    public CallingSurveyFromDialog getCallingSurveyFromDialog() {
        return callingSurveyFromDialog;
    }

    public void setCallingSurveyFromDialog(CallingSurveyFromDialog callingSurveyFromDialog) {
        this.callingSurveyFromDialog = callingSurveyFromDialog;
    }


    public UlduzumLocationsListFilterListener getUlduzumLocationsListFilterListener() {
        return ulduzumLocationsListFilterListener;
    }

    public void setUlduzumLocationsListFilterListener(UlduzumLocationsListFilterListener ulduzumLocationsListFilterListener) {
        this.ulduzumLocationsListFilterListener = ulduzumLocationsListFilterListener;
    }

    public UlduzumMapLocationRedirectionListener getUlduzumMapLocationRedirectionListener() {
        return ulduzumMapLocationRedirectionListener;
    }

    public void setUlduzumMapLocationRedirectionListener(UlduzumMapLocationRedirectionListener ulduzumMapLocationRedirectionListener) {
        this.ulduzumMapLocationRedirectionListener = ulduzumMapLocationRedirectionListener;
    }

    public CoreServiceStatusChangeListener getCoreServiceStatusChangeListener() {
        return coreServiceStatusChangeListener;
    }

    public void setCoreServiceStatusChangeListener(CoreServiceStatusChangeListener coreServiceStatusChangeListener) {
        this.coreServiceStatusChangeListener = coreServiceStatusChangeListener;
    }

    public void setFingerPrintScannerToLoginCallBack(BioMetricAuthenticationEvents fingerPrintScannerToLoginCallBack) {
        this.fingerPrintScannerToLoginCallBack = fingerPrintScannerToLoginCallBack;
    }

    public BioMetricAuthenticationEvents getFingerPrintScannerToLoginCallBack() {
        return fingerPrintScannerToLoginCallBack;
    }

    public boolean isFaqsApiCall() {
        return isFaqsApiCall;
    }

    public void setFaqsApiCall(boolean faqsApiCall) {
        isFaqsApiCall = faqsApiCall;
    }

    public boolean isMySubscriptionsApiCall() {
        return isMySubscriptionsApiCall;
    }

    public void setMySubscriptionsApiCall(boolean mySubscriptionsApiCall) {
        isMySubscriptionsApiCall = mySubscriptionsApiCall;
    }

    public boolean isMyStoreLocatorApiCall() {
        return isMyStoreLocatorApiCall;
    }

    public void setMyStoreLocatorApiCall(boolean myStoreLocatorApiCall) {
        isMyStoreLocatorApiCall = myStoreLocatorApiCall;
    }

    public boolean isContactUsApiCall() {
        return isContactUsApiCall;
    }

    public void setContactUsApiCall(boolean contactUsApiCall) {
        isContactUsApiCall = contactUsApiCall;
    }

    public boolean isSupplementaryOffersApiCall() {
        return isSupplementaryOffersApiCall;
    }

    public void setSupplementaryOffersApiCall(boolean supplementaryOffersApiCall) {
        isSupplementaryOffersApiCall = supplementaryOffersApiCall;
    }

    public boolean isDashboardApiCall() {
        return isDashboardApiCall;
    }

    public void setDashboardApiCall(boolean dashboardApiCall) {
        isDashboardApiCall = dashboardApiCall;
    }

    public boolean isTariffApiCall() {
        return isTariffApiCall;
    }

    public void setTariffApiCall(boolean tariffApiCall) {
        isTariffApiCall = tariffApiCall;
    }


    public String getCLASS_NAME_M() {
        return CLASS_NAME_M;
    }

    public void setCLASS_NAME_M(String CLASS_NAME_M) {
        this.CLASS_NAME_M = CLASS_NAME_M;
    }

    public String getCLASS_NAME_SE() {
        return CLASS_NAME_SE;
    }

    public void setCLASS_NAME_SE(String CLASS_NAME_SE) {
        this.CLASS_NAME_SE = CLASS_NAME_SE;
    }

    public SubscriptionsMain getSubscriptionsMain() {
        return subscriptionsMain;
    }

    public void setSubscriptionsMain(SubscriptionsMain subscriptionsMain) {
        this.subscriptionsMain = subscriptionsMain;
    }

    public String getFilterGroupAll() {
        return filterGroupAll;
    }

    public void setFilterGroupAll(String filterGroupAll) {
        this.filterGroupAll = filterGroupAll;
    }

    public String getCLASS_NAME_APP() {
        return CLASS_NAME_APP;
    }

    public void setCLASS_NAME_APP(String CLASS_NAME_APP) {
        this.CLASS_NAME_APP = CLASS_NAME_APP;
    }

    public ArrayList<OfferFilter> getPhoneFilter() {
        return phoneFilter;
    }

    public void setPhoneFilter(ArrayList<OfferFilter> phoneFilter) {
        this.phoneFilter = phoneFilter;
    }

    public ArrayList<OfferFilter> getTabFilter() {
        return tabFilter;
    }

    public void setTabFilter(ArrayList<OfferFilter> tabFilter) {
        this.tabFilter = tabFilter;
    }

    public ArrayList<OfferFilter> getTvFilter() {
        return tvFilter;
    }

    public void setTvFilter(ArrayList<OfferFilter> tvFilter) {
        this.tvFilter = tvFilter;
    }

    public float getDASHBOARD_CIRCLE_WIDTH() {
        return DASHBOARD_CIRCLE_WIDTH;
    }

    public void setDASHBOARD_CIRCLE_WIDTH(float DASHBOARD_CIRCLE_WIDTH) {
        this.DASHBOARD_CIRCLE_WIDTH = DASHBOARD_CIRCLE_WIDTH;
    }

    public float getBACK_CIRCULAR_WIDTH() {
        return BACK_CIRCULAR_WIDTH;
    }

    public void setBACK_CIRCULAR_WIDTH(float BACK_CIRCULAR_WIDTH) {
        this.BACK_CIRCULAR_WIDTH = BACK_CIRCULAR_WIDTH;
    }

    public float getCIRCULAR_WIDTH() {
        return CIRCULAR_WIDTH;
    }

    public void setCIRCULAR_WIDTH(float CIRCULAR_WIDTH) {
        this.CIRCULAR_WIDTH = CIRCULAR_WIDTH;
    }

    public int getTARIFF_PAGGER_OFFSET() {
        return TARIFF_PAGGER_OFFSET;
    }

    public void setTARIFF_PAGGER_OFFSET(int TARIFF_PAGGER_OFFSET) {
        this.TARIFF_PAGGER_OFFSET = TARIFF_PAGGER_OFFSET;
    }

    public int getTARIFFS_WIDTH() {
        return TARIFFS_WIDTH;
    }

    public void setTARIFFS_WIDTH(int TARIFFS_WIDTH) {
        this.TARIFFS_WIDTH = TARIFFS_WIDTH;
    }

    public String getTIME_STAMP_FOR_INTENT_INJECTION() {
        return TIME_STAMP_FOR_INTENT_INJECTION;
    }

    public void setTIME_STAMP_FOR_INTENT_INJECTION(String TIME_STAMP_FOR_INTENT_INJECTION) {
        this.TIME_STAMP_FOR_INTENT_INJECTION = TIME_STAMP_FOR_INTENT_INJECTION;
    }

    public boolean isIS_APP_OPENED_AFTER_CLOSED() {
        return IS_APP_OPENED_AFTER_CLOSED;
    }

    public void setIS_APP_OPENED_AFTER_CLOSED(boolean IS_APP_OPENED_AFTER_CLOSED) {
        this.IS_APP_OPENED_AFTER_CLOSED = IS_APP_OPENED_AFTER_CLOSED;
    }

    public LocationOnListener getLocationOnListener() {
        return locationOnListener;
    }

    public void setLocationOnListener(LocationOnListener locationOnListener) {
        this.locationOnListener = locationOnListener;
    }

    public OnProfileImageUpdate getOnProfileImageUpdate() {
        return onProfileImageUpdate;
    }

    public void setOnProfileImageUpdate(OnProfileImageUpdate onProfileImageUpdate) {
        this.onProfileImageUpdate = onProfileImageUpdate;
    }

    public RedirectMapListener getRedirectMapListener() {
        return redirectMapListener;
    }

    public void setRedirectMapListener(RedirectMapListener redirectMapListener) {
        this.redirectMapListener = redirectMapListener;
    }

    public RedirectMapFromStoreListListener getRedirectMapFromStoreListListener() {
        return redirectMapFromStoreListListener;
    }

    public void setRedirectMapFromStoreListListener(RedirectMapFromStoreListListener redirectMapFromStoreListListener) {
        this.redirectMapFromStoreListListener = redirectMapFromStoreListListener;
    }

    public EmailChangedListener getEmailChangedListener() {
        return emailChangedListener;
    }

    public void setEmailChangedListener(EmailChangedListener emailChangedListener) {
        this.emailChangedListener = emailChangedListener;
    }

    public OfferFilterListener getOfferFilterListenerCall() {
        return offerFilterListenerCall;
    }

    public void setOfferFilterListenerCall(OfferFilterListener offerFilterListenerCall) {
        this.offerFilterListenerCall = offerFilterListenerCall;
    }

    public OfferFilterListener getOfferFilterListenerInternet() {
        return offerFilterListenerInternet;
    }

    public void setOfferFilterListenerInternet(OfferFilterListener offerFilterListenerInternet) {
        this.offerFilterListenerInternet = offerFilterListenerInternet;
    }

    public OfferFilterListener getOfferFilterListenerSms() {
        return offerFilterListenerSms;
    }

    public void setOfferFilterListenerSms(OfferFilterListener offerFilterListenerSms) {
        this.offerFilterListenerSms = offerFilterListenerSms;
    }

    public OfferFilterListener getOfferFilterListenerCampaign() {
        return offerFilterListenerCampaign;
    }

    public void setOfferFilterListenerCampaign(OfferFilterListener offerFilterListenerCampaign) {
        this.offerFilterListenerCampaign = offerFilterListenerCampaign;
    }

    public OfferFilterListener getOfferFilterListenerTm() {
        return offerFilterListenerTm;
    }

    public void setOfferFilterListenerTm(OfferFilterListener offerFilterListenerTm) {
        this.offerFilterListenerTm = offerFilterListenerTm;
    }

    public OfferFilterListener getOfferFilterListenerHybrid() {
        return offerFilterListenerHybrid;
    }

    public void setOfferFilterListenerHybrid(OfferFilterListener offerFilterListenerHybrid) {
        this.offerFilterListenerHybrid = offerFilterListenerHybrid;
    }

    public UsageHistoryDetailLoadAction getUsageHistoryDetailLoadActionListener() {
        return usageHistoryDetailLoadActionListener;
    }

    public void setUsageHistoryDetailLoadActionListener(UsageHistoryDetailLoadAction usageHistoryDetailLoadActionListener) {
        this.usageHistoryDetailLoadActionListener = usageHistoryDetailLoadActionListener;
    }

    public PauseInUsageDetailPageListener getPauseInUsageDetailPageListener() {
        return pauseInUsageDetailPageListener;
    }

    public void setPauseInUsageDetailPageListener(PauseInUsageDetailPageListener pauseInUsageDetailPageListener) {
        this.pauseInUsageDetailPageListener = pauseInUsageDetailPageListener;
    }

    public Typeface getFontArialRegular() {
        return fontArialRegular;
    }

    public void setFontArialRegular(Typeface fontArialRegular) {
        this.fontArialRegular = fontArialRegular;
    }

    public Typeface getFontArialBold() {
        return fontArialBold;
    }

    public void setFontArialBold(Typeface fontArialBold) {
        this.fontArialBold = fontArialBold;
    }

    public boolean isIS_USAGE_DETAIL_OTP_VERIFY() {
        return IS_USAGE_DETAIL_OTP_VERIFY;
    }

    public void setIS_USAGE_DETAIL_OTP_VERIFY(boolean IS_USAGE_DETAIL_OTP_VERIFY) {
        this.IS_USAGE_DETAIL_OTP_VERIFY = IS_USAGE_DETAIL_OTP_VERIFY;
    }

    public boolean isIsTariffFromDashBoard() {
        return isTariffFromDashBoard;
    }

    public void setIsTariffFromDashBoard(boolean isTariffFromDashBoard) {
        this.isTariffFromDashBoard = isTariffFromDashBoard;
    }

    public String getOfferingIdRedirectFromNotifications() {
        return offeringIdRedirectFromNotifications;
    }

    public void setOfferingIdRedirectFromNotifications(String offeringIdRedirectFromNotifications) {
        this.offeringIdRedirectFromNotifications = offeringIdRedirectFromNotifications;
    }

    public String getLAST_SELECT_MENU() {
        return LAST_SELECT_MENU;
    }

    public void setLAST_SELECT_MENU(String LAST_SELECT_MENU) {
        this.LAST_SELECT_MENU = LAST_SELECT_MENU;
    }

    public int getOffersListOrientation() {
        return OFFERS_LIST_ORIENTATION;
    }

    public void setOffersListOrientation(int offersListOrientation) {
        OFFERS_LIST_ORIENTATION = offersListOrientation;
    }

    public ArrayList<OfferFilter> getOfferFiltersCheckedList() {
        return offerFiltersCheckedList;
    }

    public void setOfferFiltersCheckedList(ArrayList<OfferFilter> offerFiltersCheckedList) {
        this.offerFiltersCheckedList = offerFiltersCheckedList;
    }

    public String getLoanCredit() {
        return loanCredit;
    }

    public void setLoanCredit(String loanCredit) {
        this.loanCredit = loanCredit;
    }

    public boolean isDynamicLinkRedirection() {
        return isDynamicLinkRedirection;
    }

    public void setDynamicLinkRedirection(boolean dynamicLinkRedirection) {
        isDynamicLinkRedirection = dynamicLinkRedirection;
    }
}
