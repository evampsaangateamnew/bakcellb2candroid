package com.bakcell.globals;

import android.content.Context;

import com.bakcell.models.menus.MenuData;
import com.bakcell.models.menus.MenuHorizontalItem;
import com.bakcell.models.menus.MenuVerticalItem;

import java.util.ArrayList;

/**
 * @author Junaid Hassan on 26, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class MenusHandler {
    public static ArrayList<MenuHorizontalItem> getLocalizedHorizontalMenu(Context context, MenuData menuData) {
        ArrayList<MenuHorizontalItem> menuHorizontalItems = null;

        if (menuData != null) {
            if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
                if (menuData.getMenuEN() != null &&
                        menuData.getMenuEN().getMenuHorizontal() != null &&
                        !menuData.getMenuEN().getMenuHorizontal().isEmpty()) {
                    menuHorizontalItems = menuData.getMenuEN().getMenuHorizontal();
                }
            } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                if (menuData.getMenuAZ() != null &&
                        menuData.getMenuAZ().getMenuHorizontal() != null &&
                        !menuData.getMenuAZ().getMenuHorizontal().isEmpty()) {
                    menuHorizontalItems = menuData.getMenuAZ().getMenuHorizontal();
                }
            } else {
                if (menuData.getMenuRU() != null &&
                        menuData.getMenuRU().getMenuHorizontal() != null &&
                        !menuData.getMenuRU().getMenuHorizontal().isEmpty()) {
                    menuHorizontalItems = menuData.getMenuRU().getMenuHorizontal();
                }
            }
        }//menuData not null

        return menuHorizontalItems;
    }//getLocalizedHorizontalMenu ends

    public static ArrayList<MenuVerticalItem> getLocalizedVerticalMenu(Context context, MenuData menuData) {
        ArrayList<MenuVerticalItem> menuVerticalItems = null;

        if (menuData != null) {
            if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
                if (menuData.getMenuEN() != null &&
                        menuData.getMenuEN().getMenuVertical() != null &&
                        !menuData.getMenuEN().getMenuVertical().isEmpty()) {
                    menuVerticalItems = menuData.getMenuEN().getMenuVertical();
                }
            } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                if (menuData.getMenuAZ() != null &&
                        menuData.getMenuAZ().getMenuVertical() != null &&
                        !menuData.getMenuAZ().getMenuVertical().isEmpty()) {
                    menuVerticalItems = menuData.getMenuAZ().getMenuVertical();
                }
            } else {
                if (menuData.getMenuRU() != null &&
                        menuData.getMenuRU().getMenuVertical() != null &&
                        !menuData.getMenuRU().getMenuVertical().isEmpty()) {
                    menuVerticalItems = menuData.getMenuRU().getMenuVertical();
                }
            }
        }//menuData not null

        return menuVerticalItems;
    }//getLocalizedVerticalMenu ends
}//handler ends
