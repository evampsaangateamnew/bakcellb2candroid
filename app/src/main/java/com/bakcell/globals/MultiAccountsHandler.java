package com.bakcell.globals;

import android.app.Activity;
import android.content.Context;

import com.bakcell.models.DataManager;
import com.bakcell.models.manageaccounts.UsersHelperModel;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.CacheConfig;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Junaid Hassan on 19, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class MultiAccountsHandler {
    /**
     * set the customer's multi accounts list in the
     * local preferences
     */
    public static void setCustomerDataToPreferences(Activity activity, UsersHelperModel usersHelperModel) {
        PrefUtils.addCustomersAccountData(activity, usersHelperModel);
    }//setCustomerDataToPreferences ends

    /**
     * get the customer's multi accounts from the
     * local preferences
     */
    public static UsersHelperModel getCustomerDataFromPreferences(Activity activity) {
        return PrefUtils.getCustomersAccountData(activity);
    }//getCustomerDataFromPreferences ends

    /**
     * constants for the multi accounts
     */
    public static class MultiAccountConstants {
        public static final String ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY = "add.new.account.from.manage.accounts";
        public static final String ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE = "from.manage.account.add.new";
        public static final String FROM_FORCE_LOGOUT = "bundle.from.logout";
        public static final String FROM_FORCE_LOGOUT_VALUE = "force.logout.value";
    }//MultiAccountConstants ends

    /**
     * delete user from the multi accounts list
     * re-order the list before returning it back
     * the currently logged in user should be on the top of the list
     */
    public static ArrayList<UserModel> deleteUser(Activity activity, String msisdnToDelete) {
        ArrayList<UserModel> userModelArrayList =
                MultiAccountsHandler.getCustomerDataFromPreferences(activity).userModelArrayList;

        for (int i = 0; i < userModelArrayList.size(); i++) {
            if (userModelArrayList.get(i).getMsisdn() != null &&
                    Tools.hasValue(userModelArrayList.get(i).getMsisdn()) &&
                    userModelArrayList.get(i).getMsisdn().equalsIgnoreCase(msisdnToDelete)) {
                BakcellLogger.logE("msisdnDel", "deleted:::" + userModelArrayList.get(i).getMsisdn(), "MultiAccountsHandler", "deleteUser");
                userModelArrayList.remove(i);
            }
        }

        /**update the list*/
        MultiAccountsHandler.setCustomerDataToPreferences(activity, new UsersHelperModel(userModelArrayList));

        return MultiAccountsHandler.getOrderedMultiAccountsList(MultiAccountsHandler.getCustomerDataFromPreferences(activity).userModelArrayList);
    }//deleteUser ends

    /**delete all users from the array list of customer data*/
    public static void deleteAllUsers(Activity activity) {
        ArrayList<UserModel> userModelArrayList =
                MultiAccountsHandler.getCustomerDataFromPreferences(activity).userModelArrayList;

        if (userModelArrayList != null && !userModelArrayList.isEmpty()) {
            userModelArrayList.clear();
            /**update the list*/
            MultiAccountsHandler.setCustomerDataToPreferences(activity, new UsersHelperModel(userModelArrayList));
        }
    }//deleteUser ends

    /**
     * method for reseting all the the menu (needs to be called again)
     * promo message and api caller flags that all needs to be called again
     * when the user is switched
     */
    public static void resetAllUserDataAndAPICallerCaches(Activity activity) {
        RootValues.getInstance().setLAST_SELECT_MENU(null);//clear the last menu selected
        RootValues.getInstance().setPromoMessageDisplayed(false);//clear the promo message displayed to previous user
        DataManager.getInstance().setPromoMessage(null);//clear the promo message text
        RootValues.getInstance().setIS_APP_OPENED_AFTER_CLOSED(true);//set the app has been restarted
        CacheConfig.updateCacheFlagsForAPIsCalls(activity);//reset all api calling flags cache
    }//resetAllUserDataAndAPICallerCaches ends

    /**
     * there are two popups to be shown
     * in case there are multiple users show this info
     * on the logout popup, if there is a single user show the
     * hardcoded string from the popup show method
     */
    public static String getLocalizedLogOutMessageForMultipleUsers(Context context) {
        String localizedLogOutMessageForMultipleUsers;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            localizedLogOutMessageForMultipleUsers = "Are you sure you want to log out all numbers? You can also delete single number from Manage accounts.";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            localizedLogOutMessageForMultipleUsers = "Bütün nömrələrdən çxımaq istədiyinizdən əminsiniz? Siz cəmi bir nömrədən də çıxa bilərsiniz.";
        } else {
            localizedLogOutMessageForMultipleUsers = "Вы уверены, что хотите выйти из всех номеров? Вы также можете удалить один номер из Управления номерами.";
        }

        return localizedLogOutMessageForMultipleUsers;
    }//getLocalizedLogOutMessageForMultipleUsers ends

    /**
     * method for replacing the switched customer info
     * with the existing customer info
     * this needs to be done before switching to the new user
     * replace the data manager customer info as well as
     * in the preferences
     */
    public static void updateCurrentlyLoggedInUserModelWithNewUserModel(Context context, UserModel switchedUserModel) {
        UserMain currentUserModel = PrefUtils.getCustomerData(context);

        currentUserModel.getCustomerData().setEntityId(switchedUserModel.getEntityId());
        currentUserModel.getCustomerData().setCustomerId(switchedUserModel.getCustomerId());
        currentUserModel.getCustomerData().setAccountId(switchedUserModel.getAccountId());
        currentUserModel.getCustomerData().setBrandId(switchedUserModel.getBrandId());
        currentUserModel.getCustomerData().setCustomerType(switchedUserModel.getCustomerType());
        currentUserModel.getCustomerData().setDob(switchedUserModel.getDob());
        currentUserModel.getCustomerData().setEffectiveDate(switchedUserModel.getEffectiveDate());
        currentUserModel.getCustomerData().setExpiryDate(switchedUserModel.getExpiryDate());
        currentUserModel.getCustomerData().setFirstName(switchedUserModel.getFirstName());
        currentUserModel.getCustomerData().setGender(switchedUserModel.getGender());
        currentUserModel.getCustomerData().setLanguage(switchedUserModel.getLanguage());
        currentUserModel.getCustomerData().setLastName(switchedUserModel.getLastName());
        currentUserModel.getCustomerData().setLoyaltySegment(switchedUserModel.getLoyaltySegment());
        currentUserModel.getCustomerData().setMiddleName(switchedUserModel.getMiddleName());
        currentUserModel.getCustomerData().setMsisdn(switchedUserModel.getMsisdn());
        currentUserModel.getCustomerData().setOfferingId(switchedUserModel.getOfferingId());
        currentUserModel.getCustomerData().setStatus(switchedUserModel.getStatus());
        currentUserModel.getCustomerData().setStatusDetails(switchedUserModel.getStatusDetails());
        currentUserModel.getCustomerData().setSubscriberType(switchedUserModel.getSubscriberType());
        currentUserModel.getCustomerData().setTitle(switchedUserModel.getTitle());
        currentUserModel.getCustomerData().setToken(switchedUserModel.getToken());
        currentUserModel.getCustomerData().setBrandName(switchedUserModel.getBrandName());
        currentUserModel.getCustomerData().setOfferingName(switchedUserModel.getOfferingName());
        currentUserModel.getCustomerData().setEmail(switchedUserModel.getEmail());
        currentUserModel.getCustomerData().setPuk(switchedUserModel.getPuk());
        currentUserModel.getCustomerData().setPin(switchedUserModel.getPin());
        currentUserModel.getCustomerData().setSim(switchedUserModel.getSim());
        currentUserModel.getCustomerData().setImageURL(switchedUserModel.getImageURL());
        currentUserModel.getCustomerData().setOfferingNameDisplay(switchedUserModel.getOfferingNameDisplay());
        currentUserModel.getCustomerData().setGroupType(switchedUserModel.getGroupType());
        currentUserModel.getCustomerData().setFirstPopup(switchedUserModel.getFirstPopup());
        currentUserModel.getCustomerData().setLateOnPopup(switchedUserModel.getLateOnPopup());
        currentUserModel.getCustomerData().setPopupTitle(switchedUserModel.getPopupTitle());
        currentUserModel.getCustomerData().setPopupContent(switchedUserModel.getPopupContent());
        currentUserModel.getCustomerData().setHideNumberTariffIds(switchedUserModel.getHideNumberTariffIds());

        DataManager.getInstance().setCurrentUser(currentUserModel.getCustomerData());
        PrefUtils.addCustomerData(context, currentUserModel);
    }//updateCurrentlyLoggedInUserModelWithNewUserModel ends

    /**
     * method for getting the ordered multi accounts list
     * the currently logged in user and the recently switched
     * user would be on the top of the all other users
     * showing a green "on" bulb before the user info
     */
    public static ArrayList<UserModel> getOrderedMultiAccountsList(ArrayList<UserModel> unorderedList) {

        if (unorderedList == null) {
            return null;
        }

        int currentlyLoggedInUserIndex = -1;
        for (int i = 0; i < unorderedList.size(); i++) {
            if (DataManager.getInstance().getCurrentUser() != null &&
                    DataManager.getInstance().getCurrentUser().getMsisdn() != null &&
                    unorderedList.get(i).getMsisdn() != null &&
                    DataManager.getInstance().getCurrentUser().getMsisdn().equalsIgnoreCase(unorderedList.get(i).getMsisdn())) {
                currentlyLoggedInUserIndex = i;
                break;
            }//if ends
        }//for ends

        if (currentlyLoggedInUserIndex != -1) {
            /**swap the currently logged in user to the top of the list*/
            Collections.swap(unorderedList, currentlyLoggedInUserIndex, 0);
        }//if (currentlyLoggedInUserIndex != -1) ends

        return unorderedList;
    }//getOrderedMultiAccountsList ends

    /**
     * method for getting the currently logged in msisdn from the preferences
     */
    public static String getCurrentlyLoggedInMsisdn(Context context) {
        return PrefUtils.getCustomerData(context).getCustomerData().getMsisdn();
    }//getCurrentlyLoggedInMsisdn ends

    /**
     * class for getting the cache keys for the multi users
     */
    public static class CacheKeys {
        private static final String KEY_DAHSBOARD = "key.dashboard";
        private static final String KEY_FAQS = "key.faqs";
        private static final String KEY_TARIFF = "key.tariff";
        private static final String KEY_SUPPLEMENTARY_OFFERS = "key.supplemetary.offers";
        private static final String KEY_MY_SUBSCRIPTIONS = "key.my.subscriptions";
        private static final String KEY_FNF = "key.fiends.and.family";
        private static final String KEY_CONTACT_US = "key.contact.us";
        private static final String KEY_STORE_LOCATOR = "key.store.locator";
        private static final String KEY_ULDUZUM_LOCATOR = "key.ulduzum.locator";
        private static final String KEY_CORE_SERVICES = "key.core.services";
        private static final String KEY_FREE_SMS_SERVICES = "key.free.sms.service";
        private static final String KEY_NOTIFICATION_HISTORY = "key.notification.history";
        private static final String KEY_LOAN_HISTORY = "key.loan.history";
        private static final String KEY_PAYMENT_HISTORY = "key.payment.history";
        private static final String KEY_USAGE_HISTORY_SUMMARY = "key.usage.history.summary";
        private static final String KEY_USAGE_HISTORY_DETAIL = "key.usage.history.detail";
        private static final String KEY_OPERATION_HISTORY = "key.operation.history";
        public static final String KEY_APP_MENUS = "app.menus";
        public static final String KEY_ULDUZUM_CACHE = "key.ulduzum.cache";
        public static final String KEY_ROAMING_COUNTRIES_CACHE = "key.roaming.countries.cache";

        public static String getUlduzumCacheKey(Context context) {
            return KEY_ULDUZUM_CACHE + getCurrentlyLoggedInMsisdn(context);
        }

        public static String getRoamingCountriesCacheKey(Context context) {
            return KEY_ROAMING_COUNTRIES_CACHE + getCurrentlyLoggedInMsisdn(context);
        }

        public static String getMenusCacheKey(Context context) {
            return KEY_APP_MENUS + getCurrentlyLoggedInMsisdn(context);
        }//getMenusCacheKey ends

        public static String getNotificationsHistoryCacheKey(Context context) {
            return KEY_NOTIFICATION_HISTORY + getCurrentlyLoggedInMsisdn(context);
        }//getNotificationsHistoryCacheKey ends

        public static String getFreeSMSCacheKey(Context context) {
            return KEY_FREE_SMS_SERVICES + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getFreeSMSCacheKey ends

        public static String getFNFCacheKey(Context context) {
            return KEY_FNF + getCurrentlyLoggedInMsisdn(context);
        }//getFNFCacheKey ends

        public static String getCoreServicesCacheKey(Context context) {
            return KEY_CORE_SERVICES + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getCoreServicesCacheKey ends

        public static String getUlduzumLocatorCacheKey(Context context) {
            return KEY_ULDUZUM_LOCATOR + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getUlduzumLocatorCacheKey ends

        public static String getStoreLocatorCacheKey(Context context) {
            return KEY_STORE_LOCATOR + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getStoreLocatorCacheKey ends

        public static String getContactUsCacheKey(Context context) {
            return KEY_CONTACT_US + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getContactUsCacheKey ends

        public static String getMySubscriptionsCacheKey(Context context) {
            return KEY_MY_SUBSCRIPTIONS + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getMySubscriptionsCacheKey ends

        public static String getTariffsCacheKey(Context context) {
            return KEY_TARIFF + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getTariffsCacheKey ends

        public static String getSupplementaryOffersCacheKey(Context context) {
            return KEY_SUPPLEMENTARY_OFFERS + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getSupplementaryOffersCacheKey ends

        public static String getDashboardCacheKey(Context context) {
            return KEY_DAHSBOARD + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getDashboardCacheKey ends

        public static String getFAQsCacheKey(Context context) {
            return KEY_FAQS + AppClass.getCurrentLanguageKey(context) + getCurrentlyLoggedInMsisdn(context);
        }//getFAQsCacheKey ends

        public static String getOperationalHistoryDisclaimerPopupCacheKey(Context context) {
            return "key.disclaimer.dialog.operational.history.show" + getCurrentlyLoggedInMsisdn(context);
        }//getOperationalHistoryDisclaimerPopupCacheKey ends

        public static String getUsageHistoryDisclaimerPopupCacheKey(Context context) {
            return "key.disclaimer.dialog.usage.history.show" + getCurrentlyLoggedInMsisdn(context);
        }//getOperationalHistoryDisclaimerPopupCacheKey ends

        public static String getUserBalanceCacheKey(Context context) {
            return "key.user.balance" + getCurrentlyLoggedInMsisdn(context);
        }//getUserBalanceCacheKey ends

        public static String getUserInstallmentsCacheKey(Context context) {
            return "key.my.installment" + getCurrentlyLoggedInMsisdn(context);
        }//getUserInstallmentsCacheKey ends
    }//CacheKeys ends

    /**
     * method will return the size of the list
     * containing the currently logged in users
     */
    public static int getCurrentlyLoggedInUsersCount(Activity activity) {
        int count = 0;
        if (getCustomerDataFromPreferences(activity) != null&&
                getCustomerDataFromPreferences(activity).userModelArrayList!=null&&
                getCustomerDataFromPreferences(activity).userModelArrayList.isEmpty()) {
            count = getCustomerDataFromPreferences(activity).userModelArrayList.size();
        }

        return count;
    }//getCurrentlyLoggedInUsersCount ends
}//class ends
