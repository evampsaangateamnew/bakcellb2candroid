package com.bakcell.globals;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.adapters.TopUpCardTypeDialogAdapter;
import com.bakcell.interfaces.DisclaimerDialogListener;
import com.bakcell.interfaces.OnCardTypeClicked;
import com.bakcell.interfaces.OnOkClickListener;
import com.bakcell.models.user.userpredefinedobjects.CardType;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.List;

/**
 * Created by Noman on 4/16/2015.
 */
public class BakcellPopUpDialog {

    public static void ApiFailureMessage(Context context) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            if (!Tools.isNetworkAvailable(context)) {
                BakcellPopUpDialog.showMessageDialog(context, context.getString(R.string.bakcell_error_title), context.getString(R.string.message_no_internet));
            } else {
                BakcellPopUpDialog.showMessageDialog(context, context.getString(R.string.bakcell_error_title), context.getString(R.string.server_stopped_responding_please_try_again));
            }

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    public static void showMessageDialog(Context context, String title, String message) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_message, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    public static void showMessageDialogWithOKCallback(Context context, String title, String message, OnOkClickListener onOkClickListener) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_message, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onOkClickListener != null) {
                        onOkClickListener.onOkClick();
                    }
                    alertDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    public static void showMessageDisclaimerDialog(Context context, String title, String message, final DisclaimerDialogListener disclaimerDialogListener) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_disclaimer_message, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);

            final AppCompatCheckBox appCompatCheckBox = (AppCompatCheckBox) dialogView.findViewById(R.id.checkMarkIcon);

            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disclaimerDialogListener.onDisclaimerDialogOkListner(appCompatCheckBox.isChecked());
                    alertDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    public static void showConfirmationDialog(Context context, String title, String message, OnOkClickListener onOkClickListener) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_confirmation, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonCancel = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_cancel);
            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onOkClickListener != null) {
                        onOkClickListener.onOkClick();
                    }
                    alertDialog.dismiss();
                }
            });

            bakcellButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });


        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public static void showCardTypeDialog(Context context, String title, List<CardType> cardTypes, OnCardTypeClicked onCardTypeClicked) {
        final long[] selectedCardKey = {0};
        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.card_type_dialog, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.rv_card_types);
            ImageView imageCancel= dialogView.findViewById(R.id.imageViewCross);
            TopUpCardTypeDialogAdapter topUpCardTypeDialogAdapter = new TopUpCardTypeDialogAdapter(context, cardTypes, new OnCardTypeClicked() {
                @Override
                public void onCardClicked(long cardKey) {
                    selectedCardKey[0] = cardKey;
                }
            });
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(topUpCardTypeDialogAdapter);
            tvTitle.setText(title);

            if (cardTypes != null && cardTypes.size() > 0 && cardTypes.get(0) != null) {
                selectedCardKey[0] = cardTypes.get(0).getKey();
            }

            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onCardTypeClicked != null) {
                        onCardTypeClicked.onCardClicked(selectedCardKey[0]);
                    }
                    alertDialog.dismiss();
                }
            });


            imageCancel.setOnClickListener(v -> alertDialog.dismiss());
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }
}
