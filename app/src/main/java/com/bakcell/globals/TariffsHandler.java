package com.bakcell.globals;

import com.bakcell.models.tariffs.TariffCinNew;
import com.bakcell.models.tariffs.helper.TariffsHelperModel;
import com.bakcell.models.tariffs.tariffcinobject.TariffCin;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author Junaid Hassan on 29, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 * Handler class for the tariffs module
 * to combine the CIN & CIN_NEW tariffs into a single arraylist
 * sorts it and return it for use
 */
public class TariffsHandler {
    public static ArrayList<TariffsHelperModel> getListOfCINAndCIN_NEWTariffs(ArrayList<TariffCin> tariffCins, ArrayList<TariffCinNew> tariffCinNew) {

        /**merge the CIN and CIN_NEW tariffs to a single arraylist
         * sort the list and set the adapter*/
        ArrayList<TariffsHelperModel> combinedCINList = new ArrayList<>();

        if (tariffCins != null) {
            /**create the Tariffs Helper Model from the CIN Tariffs
             * with each tariff's sort order*/
            for (int i = 0; i < tariffCins.size(); i++) {
                //get the CIN sort order
                String CINSortOrder = "-1";
                if (tariffCins.get(i) != null &&
                        tariffCins.get(i).getHeader() != null &&
                        Tools.hasValue(tariffCins.get(i).getHeader().getSortOrder())) {
                    CINSortOrder = tariffCins.get(i).getHeader().getSortOrder();
                }//CINSortOrder null check ends

                //create the model
                combinedCINList.add(new TariffsHelperModel(tariffCins.get(i), CINSortOrder));
            }//for ends
        }

        if (tariffCinNew != null) {
            /**create the Tariffs Helper Model from the CIN_NEW Tariffs
             * with each tariff's sort order*/
            for (int i = 0; i < tariffCinNew.size(); i++) {
                //get the CIN NEW sort order
                String CIN_NewSortOrder = "-1";
                if (tariffCinNew.get(i) != null &&
                        tariffCinNew.get(i).getHeader() != null &&
                        Tools.hasValue(tariffCinNew.get(i).getHeader().getSortOrder())) {
                    CIN_NewSortOrder = tariffCinNew.get(i).getHeader().getSortOrder();
                }//CIN_NewSortOrder null check ends

                //create the model
                combinedCINList.add(new TariffsHelperModel(tariffCinNew.get(i), CIN_NewSortOrder));
            }//for ends
        }

        /**sort the CIN & CIN_NEW Combined arraylist*/
        try {
            Collections.sort(combinedCINList, (o1, o2) -> {
                if (o1.getSortOrderInt() == o2.getSortOrderInt())
                    return 0;
                return o1.getSortOrderInt() < o2.getSortOrderInt() ? -1 : 1;
            });
        } catch (Exception e) {
            BakcellLogger.logE("SortOrderX13",
                    "sortOrderError:::" + e.toString(),
                    "TariffsHandler",
                    "prepareCinTabData");
        }//try-catch ends

        /**for the testing purposes
         * just print the combined sorted list*/
        /*for (int i = 0; i < combinedCINList.size(); i++) {
            //CIN
            if (combinedCINList.get(i).getAnyTariffObject() instanceof TariffCin) {
                BakcellLogger.logE("comX", "TariffCINName:::"
                                + ((TariffCin) combinedCINList.get(i).getAnyTariffObject()).getHeader().getName(),
                        "TariffsHandler", "prepareCinTabData");
            }

            //TariffCinNew
            if (combinedCINList.get(i).getAnyTariffObject() instanceof TariffCinNew) {
                BakcellLogger.logE("comX", "TariffCinNewName:::"
                                + ((TariffCinNew) combinedCINList.get(i).getAnyTariffObject()).getHeader().getName(),
                        "TariffsHandler", "prepareCinTabData");
            }

        }//for ends*/

        return combinedCINList;
    }//getCombinedListOfCINAndCIN_NEWTariffs ends
}//class ends
