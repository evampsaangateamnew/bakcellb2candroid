package com.bakcell.globals;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.webservices.core.BakcellEaseConfig;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import allaudin.github.io.ease.EaseUtils;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;


/**
 * Created by Noman on 23-May-17.
 */

public class AppClass extends MultiDexApplication {


    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {

            /**
             * Init Ease Lib
             */
            EaseUtils.init(new BakcellEaseConfig());

            /**
             * Init Data Manager
             */
            DataManager.getInstance();

            /*
             * Init App logs Class
             */
            AppEventLogs.newInstance(this);

            installSSLSecurityProvider();
        } catch (Exception e) {
            BakcellLogger.logE("appX", "Error:::" + e.toString(), "AppClass", "onCreate");
        }
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
//        throw new RuntimeException("Test Crash"); // Force a crash
    }

    private void installSSLSecurityProvider() {

        try {
            ProviderInstaller.installIfNeeded(getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        } catch (GooglePlayServicesNotAvailableException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    public static void initializeCustomFonts(AssetManager assetManager) {
        try {
            RootValues.getInstance().setFontArialBold(Typeface.createFromAsset(assetManager, "fonts/arial_bold.ttf"));
            RootValues.getInstance().setFontArialRegular(Typeface.createFromAsset(assetManager, "fonts/arial_regular.TTF"));
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public static void setCurrentLanguageKey(Context context, String language) {
        PrefUtils.addString(context, PrefUtils.PreKeywords.PREF_KEY_CURRENT_LANGUAGE, language);
    }

    public static String getCurrentLanguageKey(Context context) {
        return PrefUtils.getString(context, PrefUtils.PreKeywords.PREF_KEY_CURRENT_LANGUAGE, Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ);
    }

    public static String getCurrentLanguageNum(Context context) {
        String lang = "";

        String saveLang = PrefUtils.getString(context, PrefUtils.PreKeywords.PREF_KEY_CURRENT_LANGUAGE, Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ);

        if (saveLang.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            lang = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN;
        } else if (saveLang.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            lang = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ;
        } else if (saveLang.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
            lang = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU;
        } else {
            lang = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ;
        }

        return lang;
    }


    public static boolean checkIfTextContainOtherThenEnglishCharacters(Context context, String StrText) {
        boolean isOtherLangText = false;
        if (context == null) return isOtherLangText;
        if (StrText != null) {
            StrText = StrText.toLowerCase();
            for (int i = 0; i < StrText.length(); i++) {
                if (!Constants.ALPHA_NUMARIC_WITH_SPACIFIC_SPACIAL_CHARS.contains(StrText.charAt(i) + "")) {
                    isOtherLangText = true;
                    BakcellLogger.logE("AlXms", "specialCharacter:::" + StrText.charAt(i), "AppClass", "checkIfTextContainOtherThenEnglishCharacters");
                    break;
                }
            }
        }
        return isOtherLangText;
    }

}
