package com.bakcell.globals;

/**
 * Created by Noman on 4/14/2015.
 */
public class Constants {

    public static final String PHONE_PREFIX = "+994";

    public static final String OTP_RECEIVE_NUMBER = "bakcell";
    public static final String TOP_UP_INIT_PAYMENT_URL = "TOP_UP_INIT_PAYMENT_URL";

    public static final float TARIFF_ITEM_MARGIN = 55;
    public static final String SSL_PINNING_SPLITTER_DELIMETER = ":::::::";
    public static final String SPACIFIC_SPACIAL_CHARS = "~`!@#$%^&*()_+=-]}[{|'\";:/?.>,<";
    public static final String ALPHA_NUMARIC_WITH_SPACIFIC_SPACIAL_CHARS = "abcdefghijklmnopqrstuvwxyz1234567890  ~`!@#$%^&*()_+=-]}[{|'\";:/?.>,<\nı";

    public static class MenusKeyword {// Remenber These keys are according to Server data

        // Side Drawer Menus Keywords
        public static final String MENU_NOTIFICATIONS = "notifications";
        public static final String MENU_STORE_LOCATOR = "store_locator";
        public static final String MENU_FAQ = "faq";
        public static final String MENU_ULDUZUM = "ulduzum";
        public static final String MENU_ROAMING_COUNTRIES = "roaming_vertical";
        public static final String MENU_TUTORIALS_AND_FAQS = "tutorial_and_faqs";
        public static final String MENU_LIVE_CHAT = "live_chat";
        public static final String MENU_CONTACT_US = "contact_us";//
        public static final String MENU_TERMS_AND_CONDITIONS = "terms_and_conditions";
        public static final String MENU_SETTINGS = "settings";
        public static final String MENU_LOGOUT = "logout";

        // Bottom Menus Keywords
        public static final String MENU_BOTTOM_DASHBOARD = "dashboard";
        public static final String MENU_BOTTOM_TARIFFS = "tariffs";
        public static final String MENU_BOTTOM_SERVICES = "services";
        public static final String MENU_BOTTOM_TOPUP = "topup";
        public static final String MENU_BOTTOM_ROAMING = "roaming";
        public static final String MENU_BOTTOM_MYACCOUTN = "myaccount";

    }

    public static class UserCurrentKeyword {
        public static final String SUBSCRIBER_TYPE_PREPAID = "prepaid";
        public static final String SUBSCRIBER_TYPE_POSTPAID = "postpaid";
        public static final String SUBSCRIBER_TYPE_KLASS_POSTPAID = "klasspostpaid";

        public static final String CUSTOMER_TYPE_POSTPAID_CORPORATE = "corporate";
        public static final String CUSTOMER_TYPE_POSTPAID_INDIVIDUAL = "individual";

        public static final String USER_BRAND_CIN = "cin";
        public static final String USER_BRAND_KLASS = "klass";
        public static final String USER_BRAND_BUSINESS = "business";
        public static final String USER_CURRENT_STATUS_CODE = "B01";

        public static final String DRC_USER_CIN_TARIFF_OFFERING_ID = "1770083072";

        public static final String CURRENCY_AZN = "azn";

        public static final String POSTPAID_TEMPLATE_INDIVIDUAL = "1";
        public static final String POSTPAID_TEMPLATE_CORPORATE_FULL_PAY = "2";
        public static final String POSTPAID_TEMPLATE_CORPORATE_INDIVIDUAL = "3";

        public static final String PRICE_TEMPLATE_DOUBLE_TITLES = "double titles";
        public static final String PRICE_TEMPLATE_DOUBLE_VALUES = "double values";

        public static final CharSequence CUSTOMER_TYPE_POSTPAID_INDIVIDUAL_TEM = "ndividual";

        public static final String DASHBOARD_MRC_UPPAID = "Unpaid";
    }

    public static class DateAndMonth {
        public static final String SAMPLE_DATE_FORMAT = "yyyy-MM-dd";
        public static final String SAMPLE_DATE_FORMAT_DAY_NAME = "EEE dd-MM-yy";
        public static final String SAMPLE_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public static final String NEW_SAMPLE_DATE_FORMAT = "dd/MM/yy";
        public static final String MONTH_FORMAT_NUM = "MM";
        public static final String MONTH_FORMAT_NAME = "MMMM";
        public static final int CURRENT_DAY = 0;
        public static final int LAST_SEVEN_DAY = 7;
        public static final int LAST_14_DAYS = 14;
        public static final int LAST_THIRTY_DAY = 30;
        public static final int LAST_NINETY_DAY = 90;
        public static final String NOTIFICATION_COUNTER_POPUP_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    }


    public static class AppLanguageKeyword {
        public static final String APP_LANGUAGE_KEY_EN = "en";
        public static final String APP_LANGUAGE_KEY_AZ = "az";
        public static final String APP_LANGUAGE_KEY_RU = "ru";

        public static final String APP_LANGUAGE_NUM_EN = "3";
        public static final String APP_LANGUAGE_NUM_AZ = "4";
        public static final String APP_LANGUAGE_NUM_RU = "2";
    }

    public static class NotificationConfigration {
        public static final String FCM_NOTIFICATION_API_CAUSE_LOGIN = "login";
        public static final String FCM_NOTIFICATION_API_CAUSE_SETTINGS = "settings";

        public static final String FCM_NOTIFICATION_ON = "1";
        public static final String FCM_NOTIFICATION_OFF = "0";

        public static final String FCM_NOTIFICATION_TONE = "tone";
        public static final String FCM_NOTIFICATION_VIBRATE = "vibrate";
        public static final String FCM_NOTIFICATION_MUTE = "mute";
    }

    public static class PermissionCodeConstants {
        public static final int FINGER_PRINT_SCANNER_PERMISSION_CODE = 103;
    }

    public static class UlduzumConstants {
        public static final String ULDUZUM_CATEGORIES_FIRST_ITEM = "allinone";
    }

    public static class TariffsConstants {
        public static final String KLASS_TYPE = "klass";
        public static final String CIN_TYPE = "cin";
        public static final String CIN_NEW_TYPE = "cinNew";

        public static final int LAYOUT_CIN_NEW = 0;
        public static final int LAYOUT_CIN = 1;

        public static final String CIN_TARIFF_SHOW = "show";
    }//TariffsConstants ends

    public static class RoamingBottomNavConstants{
        public static String ROAMING_ACTIVATION_OFFERING_ID="177943157";
        public static String ROAMING_ACTIVATION_STATUS_PRODUCT_ID="1779431571";
        public static String ROAMING_ACTIVATION_FLAG="2";
        public static String ROAMING_DEACTIVATION_FLAG="1";

    }

    public static class TopUpConstants
    {
        public final static  String MOBILE_NUMBER="MOBILE_NUMBER";
        public final static String AMOUNT = "AMOUNT";

    }

    public static class FastTopUpConstants {
        public final static String CARD_TYPE_VISA = "visa";
        public final static String CARD_TYPE_MASTER = "master";
        public final static String CARD_TYPE_LAST_ITEM = "last_item";

    }

    public static class AutoPaymentConstants {
        public final static String BILLING_CYCLE_DAILY = "1";
        public final static String BILLING_CYCLE_WEEKLY = "2";
        public final static String BILLING_CYCLE_MONTHLY = "3";


    }

    public class LiveChat {
        public static final String LICENCE_NUMBER = "12830742";
        public static final String PHONE_NUMBER = "Phone number";
    }

    public class DynamicLinksConstants {
        // Key constants for deep links
        public static final String TAB_NAME = "TAB_NAME";
        public static final String OFFERING_ID = "OFFERING_ID";

        //Tag constant for logs
        public static final String DYNAMIC_LINK_LOGGER_KEY = "DynamicLink";

        // Data constants for bundles
        public static final String DYNAMIC_LINK_SCREEN_NAME_DATA = "dynamic.link.screen.name.data";
        public static final String DYNAMIC_LINK_TAB_DATA = "dynamic.link.tab.data";
        public static final String DYNAMIC_LINK_OFFERING_DATA = "dynamic.link.offering.data";
    }

    public class DynamicLinksScreenConstants {
        public static final String PACKAGES_SCREEN_CONSTANT = "Packages";
    }

    public class DynamicLinksRedirectionValuesConstants {
        public static final String SUPPLEMENTARY_PAGE = "SUPPLEMENTARY_PAGE";
    }

    public static class InAppSurveyConstants{
        public static final String COMMENT_ENABLE = "enable";
        public static final String HOME_PAGE = "dashboard";
        public static final String TARIFF_PAGE = "tariff";
        public static final String TOP_UP_PAGE = "topup";
        public static final String BUNDLE_PAGE = "bundle";
        public static final String LOAN_PAGE = "get_loan";
        public static final String FNF_PAGE = "fnf";
        public static final String TRANSFER_MONEY_PAGE = "transfer_money";
    }
}

