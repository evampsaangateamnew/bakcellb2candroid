package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

public class RateUsAPIBeforeRedirectionWebService extends BaseService {


    private RateUsAPIBeforeRedirectionWebService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RateUsAPIBeforeRedirectionWebService newInstance(Context context, int requestId) {
        return new RateUsAPIBeforeRedirectionWebService(context, requestId);
    }

    public void execute(UserModel customerData, EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        JsonObject json = new JsonObject();
//        json.addProperty("entityId", customerData.getEntityId());

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.RateUsEndPoints.RATE_US_BEFORE_REDIRECTION_END_POINT)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}