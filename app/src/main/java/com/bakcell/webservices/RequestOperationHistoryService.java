package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class RequestOperationHistoryService extends BaseService {


    private RequestOperationHistoryService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestOperationHistoryService newInstance(Context context, int requestId) {
        return new RequestOperationHistoryService(context, requestId);
    }

    public void execute(UserModel customerData, String startDate, String endDate, EaseCallbacks<String> callbacks) {
        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        // Body Paramenters
        JsonObject json = new JsonObject();
        json.addProperty("startDate", startDate);
        json.addProperty("endDate", endDate);
        json.addProperty("accountId", customerData.getAccountId());
        json.addProperty("customerId", customerData.getCustomerId());

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.UsageHistory.REQUEST_GET_HISTORY_OPERATIONS)
                .method().post()
                .body(json)/*.hitEaseCacheAndRefresh(EndPoints.ApiCache.KEY_OPERATION_HISTORY + AppClass.getCurrentLanguageKey(context))*/
                .build().execute(context);

    } // execute
}
