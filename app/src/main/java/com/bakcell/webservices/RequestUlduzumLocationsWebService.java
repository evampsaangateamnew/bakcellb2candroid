package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

public class RequestUlduzumLocationsWebService extends BaseService {

    public RequestUlduzumLocationsWebService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestUlduzumLocationsWebService newInstance(Context context, int requestId) {
        return new RequestUlduzumLocationsWebService(context, requestId);
    }


    public void execute(UserModel customerData, String param, EaseCallbacks<String> callbacks) {

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        String ulduzumStatus = "";
        if (customerData.getLoyaltySegment() != null) {
            ulduzumStatus = customerData.getLoyaltySegment();
        }

        // Body Paramenters
        JsonObject json = new JsonObject();
        json.addProperty("loyaltySegment", ulduzumStatus);

// commented as per client request , date 31/12/2020
//        String cacheKey = MultiAccountsHandler.CacheKeys.getUlduzumLocatorCacheKey(context);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.UlduzumAPIs.ULDUZUM_LOCATIONS_MERCHANT)
                .method().post()
                .body(json)
//                .easeCache(cacheKey, true, Tools.haveNetworkConnection(context)) // commented as per client request , date 31/12/2020
                .build().execute(context);

    } // execute
}