package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.globals.RootValues;
import com.bakcell.services.MyHelp;
import com.bakcell.utilities.Logger;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class SignupPasswordService extends BaseService {


    private SignupPasswordService(Context context, int requestId) {
        super(context, requestId);
    }

    public static SignupPasswordService newInstance(Context context, int requestId) {
        return new SignupPasswordService(context, requestId);
    }

    public void execute(String msisdn, String password, String confirm_password, boolean terms_and_conditions, String pin, EaseCallbacks<String> callbacks) {

        RequestHeaders headers = RequestHeaders.newInstance();

        String valEncript = "", confirmValEncript = "", pinEncript = "";
        try {
            String decriptedKey = MyHelp.decrypt(RootValues.getInstance().getCLASS_NAME_SE(), new String(RootValues.getInstance().getCLASS_NAME_M().getBytes()));
            valEncript = MyHelp.encrypt(password, decriptedKey.trim());
            confirmValEncript = MyHelp.encrypt(confirm_password, decriptedKey.trim());
            pinEncript = MyHelp.encrypt(pin, decriptedKey.trim());
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        JsonObject json = new JsonObject();
        json.addProperty("msisdn", msisdn);
        json.addProperty("password", valEncript);
        json.addProperty("confirm_password", confirmValEncript);
        json.addProperty("terms_and_conditions", terms_and_conditions ? "1" : "0");
        json.addProperty("temp", pinEncript);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.SAVE_CUSTOMER)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
