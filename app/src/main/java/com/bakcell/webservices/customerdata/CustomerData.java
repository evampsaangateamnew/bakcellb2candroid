package com.bakcell.webservices.customerdata;

import android.content.Context;
import androidx.annotation.NonNull;

import com.bakcell.interfaces.CustomerInfoListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.user.UserMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.webservices.RequestGetCustomerDataService;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 23-Oct-17.
 */

public class CustomerData {


    public static void requestForGetCustomerData(final Context context, final CustomerInfoListener customerInfoListener) {
        if (context == null) return;

        RequestGetCustomerDataService.newInstance(context, ServiceIDs.REQUEST_CUSTOMER_DATA).
                execute(DataManager.getInstance().getCurrentUser(), false, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response == null) return;

                        String result = response.getData();

                        UserMain userMain = null;

                        try {
                            userMain = new Gson().fromJson(result, UserMain.class);
                        } catch (JsonSyntaxException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }

                        PrefUtils.addCustomerData(context, userMain);

                        if (userMain != null) {
                            DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
                            DataManager.getInstance().setUserPrimaryOffering(userMain.getPrimaryOffering());
                            DataManager.getInstance().setUserSupplementaryOfferingsList(userMain.getSupplementaryOfferingList());
                            DataManager.getInstance().setUserPredefinedData(userMain.getPredefinedData());
                        }

                        customerInfoListener.onCustomerInfoListener(null);
                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        customerInfoListener.onCustomerInfoListener(response);
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        customerInfoListener.onCustomerInfoListener(null);
                    }
                });
    }

}
