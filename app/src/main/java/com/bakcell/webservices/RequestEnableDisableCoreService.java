package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Created by Zeeshan Shabbir on 10/11/2017.
 */

public class RequestEnableDisableCoreService extends BaseService {
    RequestEnableDisableCoreService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestEnableDisableCoreService newInstance(Context c, int requestId) {
        return new RequestEnableDisableCoreService(c, requestId);
    }

    public void execute(UserModel customerData, String actionType, String offeringId, String number,
                        EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        JsonObject json = new JsonObject();
        json.addProperty("actionType", actionType);
        json.addProperty("offeringId", offeringId);
        json.addProperty("number", number);

        // Release 3 changes
        if (Tools.hasValue(customerData.getCustomerType())) {
            json.addProperty("accountType", customerData.getCustomerType());
        }
        if (Tools.hasValue(customerData.getGroupType())) {
            json.addProperty("groupType", customerData.getGroupType());
        }
        // Release 3 changes

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Services.ENABLE_DISABLE_CORE_SERVICE)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
