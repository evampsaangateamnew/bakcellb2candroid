package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Created by Nomsn on 9/29/2017.
 */

public class RequestStoreLocatorService extends BaseService {

    RequestStoreLocatorService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestStoreLocatorService newInstance(Context context, int requestId) {
        return new RequestStoreLocatorService(context, requestId);
    }


    public void execute(UserModel customerData, String param, EaseCallbacks<String> callbacks) {

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        // Body Paramenters
        JsonObject json = new JsonObject();

        String cacheKey = MultiAccountsHandler.CacheKeys.getStoreLocatorCacheKey(context);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.SideMenus.REQUEST_STORE_LOCATOR)
                .method().post()
                .body(json).easeCache(cacheKey, !RootValues.getInstance().isMyStoreLocatorApiCall(), RootValues.getInstance().isMyStoreLocatorApiCall())
                .build().execute(context);

    } // execute
}