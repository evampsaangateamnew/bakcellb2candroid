package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Created by Zeeshan Shabbir on 10/6/2017.
 */

public class RequestSendFreeSmsService extends BaseService {
    RequestSendFreeSmsService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestSendFreeSmsService newInstance(Context context, int requestId) {
        return new RequestSendFreeSmsService(context, requestId);
    }

    public void execute(UserModel customerData, String recieverMsisdn, String textmsg, String smsLangType, EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        JsonObject json = new JsonObject();
        json.addProperty("recieverMsisdn", recieverMsisdn);
        json.addProperty("textmsg", textmsg);
        json.addProperty("msgLang", smsLangType);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Services.SEND_FREE_SMS)
                .method().post()
                .body(json).build()
                .execute(context);
    }

}
