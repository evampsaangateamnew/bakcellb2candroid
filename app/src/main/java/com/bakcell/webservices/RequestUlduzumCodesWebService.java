package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

public class RequestUlduzumCodesWebService extends BaseService {

    public RequestUlduzumCodesWebService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestUlduzumCodesWebService newInstance(Context context, int requestId) {
        return new RequestUlduzumCodesWebService(context, requestId);
    }


    public void execute(UserModel customerData, String param, EaseCallbacks<String> callbacks) {

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        // Body Paramenters
        JsonObject json = new JsonObject();
//        json.addProperty("iP", "127.0.0.1");

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.UlduzumAPIs.ULDUZUM_GET_CODES)
                .method().post()
                .body(json).build().execute(context);

    } // execute
}