package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;

/**
 * @author Noman
 */

public class LoginService extends BaseService {


    private LoginService(Context context, int requestId) {
        super(context, requestId);
    }

    public static LoginService newInstance(Context context, int requestId) {
        return new LoginService(context, requestId);
    }

    public void execute(String msisdn, String password, EaseCallbacks<String> callbacks) {

        JsonObject json = new JsonObject();
        json.addProperty("msisdn", msisdn);
        json.addProperty("password", password);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.LOGIN)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
