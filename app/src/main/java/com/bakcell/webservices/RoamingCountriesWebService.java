package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

public class RoamingCountriesWebService extends BaseService {


    private RoamingCountriesWebService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RoamingCountriesWebService newInstance(Context context, int requestId) {
        return new RoamingCountriesWebService(context, requestId);
    }

    public void execute(UserModel customerData, EaseCallbacks<String> callbacks) {
        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        String brandName = "";
        if (customerData.getBrandName() != null) {
            brandName = customerData.getBrandName();//send the brand name in the body
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        // Body Paramenters
        JsonObject json = new JsonObject();
//        json.addProperty("iP", "127.0.0.1");
        json.addProperty("brandName", brandName);//juni1289

        String cacheKey = MultiAccountsHandler.CacheKeys.getRoamingCountriesCacheKey(context);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.RoamingCountriesAPIs.ROAMING_COUNTRIES_DATA)
                .method().post()
                .body(json).hitEaseCacheAndRefresh(cacheKey)
                .build().execute(context);

    } // execute
}