package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

public class UlduzumRequestWebService extends BaseService {


    private UlduzumRequestWebService(Context context, int requestId) {
        super(context, requestId);
    }

    public static UlduzumRequestWebService newInstance(Context context, int requestId) {
        return new UlduzumRequestWebService(context, requestId);
    }

    public void execute(UserModel customerData, boolean isRunInBackground, EaseCallbacks<String> callbacks) {
        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        // Body Paramenters
        JsonObject json = new JsonObject();

// commented as per client request , date 31/12/2020
//        String cacheKey = MultiAccountsHandler.CacheKeys.getUlduzumCacheKey(context);

        if(isRunInBackground){
            EaseRequest.asString(String.class)
                    .responseCallbacks(callbacks).headers(headers)
                    .requestId(requestId)
                    .endPoint(EndPoints.UlduzumAPIs.ULDUZUM_USAGE_DETAILS)
                    .method().post()
                    .runInBackground()
                    .body(json)
//                    .hitEaseCacheAndRefresh(cacheKey) // commented as per client request , date 31/12/2020
                    .build().execute(context);
        }else{
            EaseRequest.asString(String.class)
                    .responseCallbacks(callbacks).headers(headers)
                    .requestId(requestId)
                    .endPoint(EndPoints.UlduzumAPIs.ULDUZUM_USAGE_DETAILS)
                    .method().post()
                    .body(json)
//                    .hitEaseCacheAndRefresh(cacheKey)  // commented as per client request , date 31/12/2020
                    .build().execute(context);
        }

    } // execute
}