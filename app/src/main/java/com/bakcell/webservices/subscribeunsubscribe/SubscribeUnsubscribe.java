package com.bakcell.webservices.subscribeunsubscribe;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.NonNull;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bakcell.R;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.CallMySubscriptionFromOffersListener;
import com.bakcell.interfaces.CheckMySubscriptionListener;
import com.bakcell.interfaces.SubscribeUnsubscribeListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.mysubscriptions.Subscriptions;
import com.bakcell.models.mysubscriptions.SubscriptionsMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestMySubscriptionsService;
import com.bakcell.webservices.RequestSubscribeUnsubscribeService;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 16-Oct-17.
 */

public class SubscribeUnsubscribe {

    public static final String OFFER_SUBSCRIBE = "1";
    public static final String OFFER_UNSUBSCRIBE = "3";

    public static void requestSubscribeAndCheckLevelOffers(final Context context, final String offeringId, final String offerLevel, final String offerName, final String offerType, final boolean isRenew) {
        try {
            requestMySubscriptions(context, offeringId, offerLevel, offerType, new CheckMySubscriptionListener() {
                @Override
                public void onCheckMySubscriptionListener(boolean isLevelMatched) {
                    try {
                        if (context != null) {
                            if (isLevelMatched) {
                                showMessageDialogConfirmation(context, context.getString(R.string.dialog_warning_lbl), context.getString(R.string.msg_offer_subscribe_warning), offeringId, offerName, OFFER_SUBSCRIBE, false);
                            } else {
                                if (isRenew) {
                                    showMessageDialogConfirmation(context, context.getString(R.string.dialog_warning_lbl), context.getString(R.string.msg_offer_subscribe_warning), offeringId, offerName, OFFER_SUBSCRIBE, true);
                                } else {
                                    showMessageDialogConfirmation(context, context.getString(R.string.dialog_confirmation_lbl), context.getString(R.string.msg_offer_subscribe_confirmation), offeringId, offerName, OFFER_SUBSCRIBE, false);

                                }
                            }
                        }
                    } catch (Exception e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                }

            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public static void requestUnsubscribeOffers(final Context context, final String offeringId, final String offerName) {
        try {
            if (context != null) {
                showMessageDialogConfirmation(context, context.getString(R.string.dialog_confirmation_lbl), context.getString(R.string.msg_offer_unsubscribe_confirmation), offeringId, offerName, OFFER_UNSUBSCRIBE, false);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public static void requestRenewalOffers(final Context context, final String offeringId, final String offerName) {
        try {
            if (context != null) {
                showMessageDialogConfirmation(context, context.getString(R.string.dialog_confirmation_lbl), context.getString(R.string.msg_offer_renew_confirmation), offeringId, offerName, OFFER_SUBSCRIBE, true);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public static void showMessageDialogConfirmation(final Context context, String title, String message, final String offeringId, final String offerName, final String actionType, final boolean isRenewal) {
        try {

            if (context == null) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_top_up_confirmation, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            LinearLayout balance_layout = (LinearLayout) dialogView.findViewById(R.id.balance_layout);
            balance_layout.setVisibility(View.GONE);

            BakcellButtonNormal yesBtn = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_yes);
            BakcellButtonNormal noBtn = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_no);

            BakcellTextViewBold dialog_title = (BakcellTextViewBold) dialogView.findViewById(R.id.dialog_title);
            BakcellTextViewNormal tvConfirmationMsg = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_top_confirmation_msg);


            dialog_title.setText(title);
            tvConfirmationMsg.setText(message);


            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        requestSubscribeUnsubscribe(context, actionType, offeringId, offerName, new SubscribeUnsubscribeListener() {
                            @Override
                            public void onSubscribeUnsubscribeSuccess() {

                                if (isRenewal) {
                                    AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN,
                                            AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_RENEW_SUCCESS,
                                            AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN);
                                } else {
                                    if (actionType.equalsIgnoreCase(OFFER_SUBSCRIBE)) {
                                        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_OFFERS_SCREEN,
                                                AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_ACTIVATION_SUCCESS,
                                                AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_OFFERS_SCREEN);
                                    } else {
                                        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN,
                                                AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_DEACTIVATE_SUCCESS,
                                                AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN);
                                    }
                                }
                            }

                            @Override
                            public void onSubscribeUnsubscribeFailure() {
                                if (isRenewal) {
                                    AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN,
                                            AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_RENEW_FAILURE,
                                            AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN);
                                } else {
                                    if (actionType.equalsIgnoreCase(OFFER_SUBSCRIBE)) {
                                        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_OFFERS_SCREEN,
                                                AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_ACTIVATION_FAILURE,
                                                AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_OFFERS_SCREEN);
                                    } else {
                                        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN,
                                                AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_DEACTIVATE_FAILURE,
                                                AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN);
                                    }
                                }
                            }
                        });
                    } catch (Exception e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                    alertDialog.dismiss();
                }
            });

            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    public static void requestSubscribeUnsubscribe(final Context context, String actionType, String offeringId, String offerName, final SubscribeUnsubscribeListener subscribeUnsubscribeListener) {
        if (context == null) return;

        RequestSubscribeUnsubscribeService.newInstance(context, ServiceIDs.REQUEST_CHAGE_SUPPLEMENTARYOFFERS)
                .execute(DataManager.getInstance().getCurrentUser(), actionType, offeringId, offerName, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response == null) return;
                        String result = response.getData();
                        SubscriptionsMain subscriptionsMainNew = new Gson().fromJson(result, SubscriptionsMain.class);

                        if (subscriptionsMainNew != null) {
                            RootValues.getInstance().setSubscriptionsMain(subscriptionsMainNew);
                        }

                        if (context != null) {
                            BakcellPopUpDialog.showMessageDialog(context,
                                    context.getString(R.string.mesg_successful_title), response.getDescription());

//                            inAppFeedback(context, context.getString(R.string.mesg_successful_title), response.getDescription());
                        }

                        subscribeUnsubscribeListener.onSubscribeUnsubscribeSuccess();

                    }


                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        try {
                            if (context != null && response != null) {
                                BakcellPopUpDialog.showMessageDialog(context,
                                        context.getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        } catch (Exception e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }

                        subscribeUnsubscribeListener.onSubscribeUnsubscribeFailure();

                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        try {
                            if (context != null) {
                                BakcellPopUpDialog.ApiFailureMessage(context);
                            }
                        } catch (Exception ex) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                        }

                        subscribeUnsubscribeListener.onSubscribeUnsubscribeFailure();

                    }

                });

    }


    public static void requestMySubscriptions(final Context context, final String offeringId, final String offerLevel, final String offerType, final CheckMySubscriptionListener checkMySubscriptionListener) {
        if (DataManager.getInstance().getCurrentUser() == null) return;

        if (RootValues.getInstance().getSubscriptionsMain() != null) {
            if (RootValues.getInstance().getSubscriptionsMain() != null && checkoutIfSameOfferIsInSubscribe(context, RootValues.getInstance().getSubscriptionsMain(), offeringId, offerLevel, true, offerType)) {
                checkMySubscriptionListener.onCheckMySubscriptionListener(true);
            } else {
                checkMySubscriptionListener.onCheckMySubscriptionListener(false);
            }
        } else {
            RequestMySubscriptionsService.newInstance(context,
                    ServiceIDs.REQUEST_MY_SUBSCRIPTIONS).execute(DataManager.getInstance().getCurrentUser()
                    , new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String result = response.getData();
                            SubscriptionsMain subscriptionsMain = null;

                            if (result != null) {
                                subscriptionsMain = new Gson().fromJson(result, SubscriptionsMain.class);
                            }

                            if (subscriptionsMain != null && checkoutIfSameOfferIsInSubscribe(context, subscriptionsMain, offeringId, offerLevel, true, offerType)) {
                                checkMySubscriptionListener.onCheckMySubscriptionListener(true);
                            } else {
                                checkMySubscriptionListener.onCheckMySubscriptionListener(false);
                            }

                        }//end of onSuccess

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            checkMySubscriptionListener.onCheckMySubscriptionListener(false);
                        }//end of onFailure

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            checkMySubscriptionListener.onCheckMySubscriptionListener(false);
                        }//end of onError

                    });
        }

    }

    public static boolean checkoutIfSameOfferIsInSubscribe(Context context, SubscriptionsMain subscriptionsMain, String offeringId, String offerLevel, boolean isCheckLevel, String offerType) {

        boolean isOfferOnSameLevel = false;

        if (subscriptionsMain != null && context != null) {

//            if (isCheckLevel) {

            if (context.getString(R.string.supplementary_offer_tab_title_call).equalsIgnoreCase(offerType)) {
                if (subscriptionsMain.getCall() != null) {
                    isOfferOnSameLevel = checkIfOfferInSameLevel(subscriptionsMain.getCall().getOffers(), offeringId, offerLevel, isCheckLevel);
                    if (isOfferOnSameLevel) {
                        return isOfferOnSameLevel;
                    }
                }
            } else if (context.getString(R.string.supplementary_offer_tab_title_internet).equalsIgnoreCase(offerType)) {
                if (subscriptionsMain.getInternet() != null) {
                    isOfferOnSameLevel = checkIfOfferInSameLevel(subscriptionsMain.getInternet().getOffers(), offeringId, offerLevel, isCheckLevel);
                    if (isOfferOnSameLevel) {
                        return isOfferOnSameLevel;
                    }
                }
            } else if (context.getString(R.string.supplementary_offer_tab_title_sms).equalsIgnoreCase(offerType)) {
                if (subscriptionsMain.getSms() != null) {
                    isOfferOnSameLevel = checkIfOfferInSameLevel(subscriptionsMain.getSms().getOffers(), offeringId, offerLevel, isCheckLevel);
                    if (isOfferOnSameLevel) {
                        return isOfferOnSameLevel;
                    }
                }
            }
            //TODO Temporary Commented by client
//            else if (context.getString(R.string.supplementary_offer_tab_title_campaign).equalsIgnoreCase(offerType)) {
//                if (subscriptionsMain.getCampaign() != null) {
//                    isOfferOnSameLevel = checkIfOfferInSameLevel(subscriptionsMain.getCampaign().getOffers(), offeringId, offerLevel, isCheckLevel);
//                    if (isOfferOnSameLevel) {
//                        return isOfferOnSameLevel;
//                    }
//                }
//            }

            else if (context.getString(R.string.supplementary_offer_tab_title_tm).equalsIgnoreCase(offerType)) {
                if (subscriptionsMain.getTm() != null) {
                    isOfferOnSameLevel = checkIfOfferInSameLevel(subscriptionsMain.getTm().getOffers(), offeringId, offerLevel, isCheckLevel);
                    if (isOfferOnSameLevel) {
                        return isOfferOnSameLevel;
                    }
                }
            } else if (context.getString(R.string.supplementary_offer_tab_title_hybrid).equalsIgnoreCase(offerType)) {
                if (subscriptionsMain.getHybrid() != null) {
                    isOfferOnSameLevel = checkIfOfferInSameLevel(subscriptionsMain.getHybrid().getOffers(), offeringId, offerLevel, isCheckLevel);
                    if (isOfferOnSameLevel) {
                        return isOfferOnSameLevel;
                    }
                }
            } else if (context.getString(R.string.supplementary_offer_tab_title_roaming).equalsIgnoreCase(offerType)) {
                if (subscriptionsMain.getRoaming() != null) {
                    isOfferOnSameLevel = checkIfOfferInSameLevel(subscriptionsMain.getRoaming().getOffers(), offeringId, offerLevel, isCheckLevel);
                    if (isOfferOnSameLevel) {
                        return isOfferOnSameLevel;
                    }
                }
            }

        }

        return isOfferOnSameLevel;
    }

    private static boolean checkIfOfferInSameLevel(ArrayList<Subscriptions> offersList, String offeringId, String offerLevel, boolean isCheckLevel) {
        boolean isSameLevel = false;

        if (isCheckLevel) {
            if (offersList != null && offersList.size() > 0 && Tools.hasValue(offeringId) && Tools.hasValue(offerLevel)) {
                for (int i = 0; i < offersList.size(); i++) {
                    if (offersList.get(i) != null && offersList.get(i).getHeader() != null) {
                        if (Tools.hasValue(offersList.get(i).getHeader().getOfferingId())
                                && Tools.hasValue(offersList.get(i).getHeader().getOfferLevel())
                                && !offersList.get(i).getHeader().getOfferingId().equalsIgnoreCase(offeringId)
                                && offersList.get(i).getHeader().getOfferLevel().equalsIgnoreCase(offerLevel)) {
                            isSameLevel = true;
                            break;
                        }
                    }
                }
            }
        } else {
            if (offersList != null && offersList.size() > 0 && Tools.hasValue(offeringId)) {
                for (int i = 0; i < offersList.size(); i++) {
                    if (offersList.get(i) != null && offersList.get(i).getHeader() != null) {
                        if (Tools.hasValue(offersList.get(i).getHeader().getOfferingId())
                                && offersList.get(i).getHeader().getOfferingId().equalsIgnoreCase(offeringId)) {
                            isSameLevel = true;
                            break;
                        }
                    }
                }
            }
        }

        return isSameLevel;
    }


    public static void requestMySubscriptionsFromOffer(final Context context, final CallMySubscriptionFromOffersListener callMySubscriptionFromOffersListener) {
        if (DataManager.getInstance().getCurrentUser() == null) return;

        RequestMySubscriptionsService.newInstance(context,
                ServiceIDs.REQUEST_MY_SUBSCRIPTIONS).execute(DataManager.getInstance().getCurrentUser()
                , new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        String result = response.getData();

                        SubscriptionsMain subscriptionsMain = null;

                        if (result != null) {
                            subscriptionsMain = new Gson().fromJson(result, SubscriptionsMain.class);
                        }

                        callMySubscriptionFromOffersListener.onCheckMySubscriptionListener(subscriptionsMain);

                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        callMySubscriptionFromOffersListener.onCheckMySubscriptionListener(null);
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        callMySubscriptionFromOffersListener.onCheckMySubscriptionListener(null);
                    }//end of onError

                });

    }

    private static void inAppFeedback(Activity context, String title, String description) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_BUNDLE_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.BUNDLE_PAGE);
            if (surveys != null) {
//                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
//                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                        InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                        if (surveys.getQuestions() != null) {
                            currentVisit = 0;
                            inAppFeedbackDialog.showTitleWithMessageDialog(surveys, title, description);
                        } else {
                            BakcellPopUpDialog.showMessageDialog(context, title, description);
                        }
//                    } else {
//                        BakcellPopUpDialog.showMessageDialog(context, title, description);
//                    }
//                } else {
//                    BakcellPopUpDialog.showMessageDialog(context, title, description);
//                }
            } else {
                BakcellPopUpDialog.showMessageDialog(context, title, description);
            }
        } else {
            BakcellPopUpDialog.showMessageDialog(context, title, description);
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_BUNDLE_PAGE, currentVisit);
    }
}
