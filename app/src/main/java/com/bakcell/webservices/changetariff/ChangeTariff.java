package com.bakcell.webservices.changetariff;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.bakcell.R;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.interfaces.ChangeTariffListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.TariffUtil;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestChangeTariffService;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 24-Oct-17.
 */

public class ChangeTariff {

    public static void requestForChangeTariffDialog(final Context context, final String offeringId,
                                                    final String tariffName, final String subscribable,
                                                    String tariffType,
                                                    final ChangeTariffListener changeTariffListener) {

        try {
            /*String message = "0";
            if (context == null) return;
            if (DataManager.getInstance().getUserPredefinedData() != null
                    && DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices() != null &&
                    DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().size() > 0) {

                String key = DataManager.getInstance().getCurrentUser().getBrandName().toLowerCase()
                        + "@" + tariffType + "@" + DataManager.getInstance().getCurrentUser().getSubscriberType().toLowerCase()
                        + "@" + AppClass.getCurrentLanguageKey(context).toLowerCase();

                ArrayList<TariffMigrationPrices> tariffMigrationPricesArrayList = DataManager
                        .getInstance().getUserPredefinedData().getTariffMigrationPrices();
                for (int i = 0; i < tariffMigrationPricesArrayList.size(); i++) {
                    if (tariffMigrationPricesArrayList.get(i).getKey().equalsIgnoreCase(key)) {
                        message = tariffMigrationPricesArrayList.get(i).getValue();
                        break;
                    }
                }

            }*/

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_top_up_confirmation, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            LinearLayout rootLayout = dialogView.findViewById(R.id.root);
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                rootLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }
            LinearLayout balance_layout = dialogView.findViewById(R.id.balance_layout);
            balance_layout.setVisibility(View.GONE);

            BakcellButtonNormal yesBtn = dialogView.findViewById(R.id.btn_yes);
            BakcellButtonNormal noBtn = dialogView.findViewById(R.id.btn_no);

            BakcellTextViewBold dialog_title = dialogView.findViewById(R.id.dialog_title);
            BakcellTextViewNormal tvConfirmationMsg = dialogView.findViewById(R.id.tv_top_confirmation_msg);

            dialog_title.setText(context.getString(R.string.dialog_confirmation_lbl));
            tvConfirmationMsg.setText(TariffUtil.getTariffMigrationMessageWithPricing(offeringId));

            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        requestForChangeTariff(context, offeringId, tariffName, subscribable, changeTariffListener);
                    } catch (Exception e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                    alertDialog.dismiss();
                }
            });

            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    public static void requestForChangeTariff(final Context context, String offeringId, String tariffName, String subscribable, final ChangeTariffListener changeTariffListener) {
        if (context == null) return;

        String actionType = "";

        if (Tools.hasValue(subscribable)) {
            if (subscribable.equalsIgnoreCase(TariffsFragment.RENEWABLE)) {
                actionType = TariffsFragment.TARIFF_RENEW;
            } else if (subscribable.equalsIgnoreCase(TariffsFragment.CAN_SUBSCRIBE)) {
                actionType = TariffsFragment.TARIFF_SUBSCRIBE;
            }
        }

        RequestChangeTariffService.newInstance(context, ServiceIDs.REQUEST_CHANGE_TARIFF).execute(
                DataManager.getInstance().getCurrentUser(), offeringId, tariffName, actionType, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        changeTariffListener.onChangeTariffListenerSuccess(response.getDescription());
                        try {
                            if (context != null) {
                                BakcellPopUpDialog.showMessageDialog(context,
                                        context.getString(R.string.mesg_successful_title), response.getDescription());
//                                inAppFeedback(context, context.getString(R.string.mesg_successful_title), response.getDescription(), offeringId);
                            }
                        } catch (Exception e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        changeTariffListener.onChangeTariffListenerFailure(response.getDescription());
                        try {
                            if (context != null && response != null) {
                                BakcellPopUpDialog.showMessageDialog(context,
                                        context.getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        } catch (Exception e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        changeTariffListener.onChangeTariffListenerFailure(context.getString(R.string.server_stopped_responding_please_try_again));
                        try {
                            if (context != null) {
                                BakcellPopUpDialog.showMessageDialog(context,
                                        context.getString(R.string.bakcell_error_title), context.getString(R.string.server_stopped_responding_please_try_again));
                            }
                        } catch (Exception e1) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e1.getMessage());
                        }
                    }
                });

    }

    private static void inAppFeedback(Context context, String title, String description, String tariffId) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TARIFF_PAGE);
            if (surveys != null) {
//                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
//                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                if (surveys.getQuestions() != null) {
                    currentVisit = 0;
                    inAppFeedbackDialog.showTariffTitleWithMessageDialog(surveys, title, description, tariffId);
                } else {
                    BakcellPopUpDialog.showMessageDialog(context, title, description);
                }
//                    } else {
//                        BakcellPopUpDialog.showMessageDialog(context, title, description);
//                    }
//                } else {
//                    BakcellPopUpDialog.showMessageDialog(context, title, description);
//                }
            } else {
                BakcellPopUpDialog.showMessageDialog(context, title, description);
            }
        } else {
            BakcellPopUpDialog.showMessageDialog(context, title, description);
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_TARIFF_PAGE, currentVisit);
    }

}
