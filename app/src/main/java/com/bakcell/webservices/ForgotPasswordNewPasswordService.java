package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.globals.RootValues;
import com.bakcell.services.MyHelp;
import com.bakcell.utilities.Logger;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class ForgotPasswordNewPasswordService extends BaseService {


    private ForgotPasswordNewPasswordService(Context context, int requestId) {
        super(context, requestId);
    }

    public static ForgotPasswordNewPasswordService newInstance(Context context, int requestId) {
        return new ForgotPasswordNewPasswordService(context, requestId);
    }

    public void execute(String msisdn, String password, String confirmPassword, String pin, EaseCallbacks<String> callbacks) {

        RequestHeaders headers = RequestHeaders.newInstance();


        String valEncript = "", confirmValEncript = "", pinEncript = "";
        try {
            String decriptedKey = MyHelp.decrypt(RootValues.getInstance().getCLASS_NAME_SE(), new String(RootValues.getInstance().getCLASS_NAME_M().getBytes()));
            valEncript = MyHelp.encrypt(password, decriptedKey.trim());
            confirmValEncript = MyHelp.encrypt(confirmPassword, decriptedKey.trim());
            pinEncript = MyHelp.encrypt(pin, decriptedKey.trim());
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        JsonObject json = new JsonObject();
        json.addProperty("msisdn", msisdn);
        json.addProperty("password", valEncript);
        json.addProperty("confirmPassword", confirmValEncript);
        json.addProperty("temp", pinEncript);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.FORGOT_ENDPOINT)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
