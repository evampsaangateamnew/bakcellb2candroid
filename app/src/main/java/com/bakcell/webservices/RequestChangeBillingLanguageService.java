package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Created by Atif Arif on 9/27/2017.
 */

public class RequestChangeBillingLanguageService extends BaseService {

    RequestChangeBillingLanguageService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestChangeBillingLanguageService newInstance(Context context, int requestId) {
        return new RequestChangeBillingLanguageService(context, requestId);
    }


    public void execute(UserModel customerData, String language, EaseCallbacks<String> callbacks) {

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        // Body Paramenters
        JsonObject json = new JsonObject();
        json.addProperty("language", language);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Profile.REQUEST_CHANGE_BILLING_LANGUAGE)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}