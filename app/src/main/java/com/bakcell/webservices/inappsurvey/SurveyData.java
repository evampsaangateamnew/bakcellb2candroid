package com.bakcell.webservices.inappsurvey;

import android.content.Context;

import androidx.annotation.NonNull;

import com.bakcell.interfaces.CustomerInfoListener;
import com.bakcell.interfaces.InAppSurveyInfoListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.user.UserMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.webservices.RequestGetCustomerDataService;
import com.bakcell.webservices.RequestInAppSurveyDataService;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Muhammad Ahmed on 12-AUG-21.
 */

public class SurveyData {


    public static void requestForGetInAppSurveyData(final Context context, final InAppSurveyInfoListener inAppSurveyInfoListener) {
        if (context == null) return;

        RequestInAppSurveyDataService.newInstance(context, ServiceIDs.REQUEST_GET_IN_APP_SURVEY).
                execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response == null) return;

                        String result = response.getData();

                        InAppSurvey inAppSurvey = null;

                        try {
                            Data data = new Gson().fromJson(result, Data.class);
                            inAppSurvey = new InAppSurvey(response.callStatus(), response.getResultCode(), response.getDescription(), data);
                        } catch (JsonSyntaxException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }

                        PrefUtils.addInAppSurvey(context, inAppSurvey);

//                        if (inAppSurvey != null) {
//                            DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
//                            DataManager.getInstance().setUserPrimaryOffering(userMain.getPrimaryOffering());
//                            DataManager.getInstance().setUserSupplementaryOfferingsList(userMain.getSupplementaryOfferingList());
//                            DataManager.getInstance().setUserPredefinedData(userMain.getPredefinedData());
//                        }

                        inAppSurveyInfoListener.onInAppSurveyInfoListener(null);
                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        inAppSurveyInfoListener.onInAppSurveyInfoListener(response);
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        inAppSurveyInfoListener.onInAppSurveyInfoListener(null);
                    }
                });
    }

}
