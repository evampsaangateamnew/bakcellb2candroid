package com.bakcell.webservices.inappsurvey;

import android.content.Context;

import androidx.annotation.NonNull;

import com.bakcell.interfaces.InAppSurveyInfoListener;
import com.bakcell.interfaces.InAppSurveyUploadListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.inappsurvey.Data;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.UploadSurvey;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.webservices.RequestInAppSurveyDataService;
import com.bakcell.webservices.RequestInAppSurveyUploadService;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Muhammad Ahmed on 12-AUG-21.
 */

public class UploadSurveyData {


    public static void requestForUploadInAppSurveyData(final Context context, final UploadSurvey surveyData, final InAppSurveyUploadListener inAppSurveyUploadListener) {
        if (context == null) return;

        RequestInAppSurveyUploadService.newInstance(context, ServiceIDs.REQUEST_IN_APP_SURVEY_UPLOAD).
                execute(DataManager.getInstance().getCurrentUser(),surveyData, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response == null) return;

                        String result = response.getData();

                        InAppSurvey inAppSurvey = null;

                        try {
                            Data data = new Gson().fromJson(result, Data.class);
                            inAppSurvey = new InAppSurvey(response.callStatus(), response.getResultCode(), response.getDescription(), data);
                        } catch (JsonSyntaxException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }

                        PrefUtils.addInAppSurvey(context, inAppSurvey);

                        inAppSurveyUploadListener.onInAppSurveySuccessListener();
                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        inAppSurveyUploadListener.onInAppSurveyFailListener(response);
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        inAppSurveyUploadListener.onInAppSurveyFailListener(null);
                    }
                });
    }

}
