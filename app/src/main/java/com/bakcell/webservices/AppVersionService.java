package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;

/**
 * @author Noman
 */

public class AppVersionService extends BaseService {


    private AppVersionService(Context context, int requestId) {
        super(context, requestId);
    }

    public static AppVersionService newInstance(Context context, int requestId) {
        return new AppVersionService(context, requestId);
    }

    public void execute(String AppVersion, EaseCallbacks<String> callbacks) {

        JsonObject json = new JsonObject();
        json.addProperty("appversion", AppVersion);

        EaseRequest.asString(String.class).runInBackground()
                .responseCallbacks(callbacks)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.APP_VERSION)
                .method().post().socketTimeOut(15000)
                .body(json)
                .build().execute(context);

    } // execute
}
