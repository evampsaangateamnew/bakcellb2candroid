package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Humayun Sikander on 07/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
public class RequestDeleteFastTopUpService extends BaseService {
    RequestDeleteFastTopUpService(Context context, int requestId) {
        super(context, requestId);
    }

    RequestDeleteFastTopUpService(Context context, int requestId, boolean runInBackground) {
        super(context, requestId, runInBackground);
    }

    public static RequestDeleteFastTopUpService newInstance(Context context, int requestId) {
        return new RequestDeleteFastTopUpService(context, requestId);
    }


    public void execute(UserModel customerData, String fastPaymentId, boolean isBackground, EaseCallbacks<String> callbacks) {
        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        // Body Paramenters
        JsonObject json = new JsonObject();
        json.addProperty("id", fastPaymentId);

        EaseRequest.asString(String.class).runInBackground(isBackground)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.FastTopUp.REQUEST_DELETE_FAST_TOP_UP)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
