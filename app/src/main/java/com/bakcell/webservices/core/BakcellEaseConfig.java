package com.bakcell.webservices.core;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bakcell.BuildConfig;
import com.bakcell.globals.AppClass;
import com.bakcell.utilities.Tools;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import allaudin.github.io.ease.EaseDefaultConfig;
import allaudin.github.io.ease.EaseDialog;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Configuration for Ease wrapper.
 *
 * @author Noman
 */

public class BakcellEaseConfig extends EaseDefaultConfig {


    @NonNull
    @Override
    public EaseDialog getDialog(Context context) {
        return BakcellProgressDialog.newDialog(context);
    }

    @NonNull
    @Override
    public String getBaseUrl() {
        return BuildConfig.SERVER_URL;
    }

    @Override
    public boolean enableLogging() {
        return BuildConfig.LOG_ENABLED;
    }

    @Override
    public boolean enableSSLPinning() {
        return BuildConfig.SSL_ENABLED;
    }

    @Nullable
    @Override
    public RequestHeaders defaultHeaders(Context context) {
        return RequestHeaders.newInstance()
                .put("deviceID", Tools.getDeviceID(context))
                .put("Content-Type", "application/json")
                .put("UserAgent", "android")
                .put("lang", AppClass.getCurrentLanguageNum(context));
    }

    @Override
    public int socketTimeOut() {
        return (int) TimeUnit.SECONDS.toMillis(30);
    }


    private HashMap<String, String> removeBackslashAndExtraSpaces(HashMap<String, String> map) {
        HashMap<String, String> parrams = new HashMap<String, String>();
        try {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = null, value = null;
                if (entry != null) {
                    if (entry.getKey() != null) {
                        key = entry.getKey().trim();
                    }
                    if (entry.getValue() != null) {
                        value = entry.getValue().trim();
                    }
                }
                parrams.put(key, value);
            }
        } catch (Exception e) {
        }
        return parrams;
    }

} // BakcellEaseConfig
