package com.bakcell.webservices.core;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bakcell.R;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.Constants;
import com.bakcell.utilities.Logger;

import allaudin.github.io.ease.EaseDialog;

/**
 * @author Zeeshan
 */

public class BakcellProgressDialog implements EaseDialog {

    private CharSequence mTitle;
    private ProgressDialog mProgressDialog;


    private BakcellProgressDialog(@NonNull CharSequence title) {
        this.mTitle = title;
    }

    public static BakcellProgressDialog newDialog(Context context) {
        return new BakcellProgressDialog(context.getString(R.string.loading));
    }

    @Override
    public EaseDialog init(Context context) {
        try {
            if (mProgressDialog == null) {
                this.mProgressDialog = new ProgressDialog(context);
                this.mProgressDialog.setTitle(getLoadingText(context));
            }
             } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return this;
    }

    private String getLoadingText(Context context) {
        switch (AppClass.getCurrentLanguageNum(context)) {
            case Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ:
                return "Yüklənir....";
            case Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU:
                return "Загружается.... ";
            default:
                return "Loading....";
        }
    }

    @Override
    public void show() {
        try {
            if (mProgressDialog != null && !mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    @Override
    public void hide() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

}
