package com.bakcell.webservices.core;

import androidx.annotation.NonNull;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * @author Zeeshan
 */
public class SimpleEaseCallback<T> implements EaseCallbacks<T> {

    @Override
    public void onSuccess(@NonNull EaseRequest<T> request, EaseResponse<T> response) {

    }

    @Override
    public void onFailure(@NonNull EaseRequest<T> request, EaseResponse<T> response) {

    }

    @Override
    public void onError(@NonNull EaseRequest<T> request, @NonNull EaseException e) {

    }
}
