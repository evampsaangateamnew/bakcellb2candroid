package com.bakcell.webservices.core;

/**
 * Created by Freeware Sys on 26-Jul-17.
 */

public class ServiceIDs {

    public static final int LOGIN = 1001;
    public static final int SIGNUP_NUMBER_VERIFY = 1002;
    public static final int SIGNUP_PIN_VERIFY = 1003;
    public static final int SIGNUP_PASSWORD = 1004;
    public static final int RESEND_PIN = 1005;
    public static final int FORGOTPASSWORD_NEW_PASSWORD = 1006;
    public static final int CHANGE_PASSWORD = 1007;
    public static final int APP_MENUS = 1008;
    public static final int FAQS = 1009;
    public static final int GET_LOAD = 1010;
    public static final int GET_SUPPLEMENTARY_OFFERS = 1011;
    public static final int GET_TARIFFS_LISTS = 1012;
    public static final int GET_DASHBOARD_PAGE = 1013;
    public static final int REQUEST_TOPUP = 1014;
    public static final int REQUEST_MONEY_TRANSFER = 1015;
    public static final int REQUEST_LOAN_HISTORY = 1016;
    public static final int REQUEST_PAYMENT_HISTORY = 1017;
    public static final int REQUEST_USAGE_HISTORY_SUMMARY = 1018;
    public static final int REQUEST_USAGE_HISTORY_DETAILS = 1019;
    public static final int REQUEST_OPERATION_HISTORY = 1020;
    public static final int REQUEST_MY_SUBSCRIPTIONS = 1021;
    public static final int REQUEST_HISTORY_ACCOUNT_VERIFY = 1022;
    public static final int REQUEST_HISTORY_PIN_VERIFY = 1023;
    public static final int REQUEST_UPDATE_EMAIL = 1024;
    public static final int REQUEST_STORE_LOCATOR = 1025;
    public static final int REQUEST_GET_FF = 1026;
    public static final int REQUEST_DELETE_FF = 1027;
    public static final int REQUEST_ADD_FF = 1028;
    public static final int REQUEST_CONTACT_US = 1029;
    public static final int REQUEST_FREE_SMS_STATUS = 1030;
    public static final int REQUEST_SEND_FREE_SMS = 1031;
    public static final int REQUEST_NOTIFICATION = 1032;
    public static final int REQUEST_ADD_FCM = 1033;
    public static final int REQUEST_ADD_UPLOAD_IMAGE = 1034;
    public static final int REQUEST_LOGOUT_USER = 1035;
    public static final int REQUEST_APP_VERSION = 1036;
    public static final int REQUEST_CORE_SERVICES = 1037;
    public static final int REQUEST_ENABLE_DISABLE_CORE_SERVICE = 1038;
    public static final int REQUEST_SEND_INTERNET_SETTINGS = 1039;
    public static final int REQUEST_CHANGE_BILLING_LANGUAGE = 1040;
    public static final int REQUEST_HISTORY_RESEND = 1041;
    public static final int REQUEST_CHAGE_SUPPLEMENTARYOFFERS = 1042;
    public static final int REQUEST_CUSTOMER_DATA = 1043;
    public static final int REQUEST_CHANGE_TARIFF = 1044;
    public static final int REQUEST_REPORT_SIM_LOST = 1045;
    public static final int REQUEST_GET_NOTIFICATION_COUNT = 1046;
    public static final int REQUEST_OTP_RESENT_MONEY_TRANSFER = 1047;
    public static final int REQUEST_MONEY_TRANSFER_OTP_VERIFY = 1048;
    public static final int ULDUZUM_USAGE_DETAILS_ID = 1049;
    public static final int ULDUZUM_USAGE_HISTORY_ID = 1050;
    public static final int ULDUZUM_UNUSED_CODES_ID = 1051;
    public static final int ULDUZUM_GET_LOCATIONS_ID = 1052;
    public static final int ULDUZUM_GET_CODES_ID = 1053;
    public static final int ROAMING_COUNTRIES_DATA_ID = 1054;
    public static final int RATE_US_SERVICE_ID = 1055;
    public static final int RATE_US_API_BEFORE_REDIRECTION_SERVICE_ID = 1056;
    public static final int REQUEST_ENABLE_DISABLE_ROAMING_ACTIVATION = 1057;
    public static final int REQUEST_GET_FAST_PAYMENTS = 1058;
    public static final int REQUEST_GET_SAVED_CARDS = 1059;
    public static final int REQUEST_GET_INITIATE_PAYMENT = 1060;
    public static final int REQUEST_MAKE_PAYMENT = 1061;
    public static final int REQUEST_ADD_PAYMENT_SCHEDULER = 1062;
    public static final int REQUEST_GET_SCHEDULED_PAYMENTS = 1063;
    public static final int REQUEST_DELETE_SCHEDULED_PAYMENTS = 1064;
    public static final int REQUEST_DELETE_SAVED_CARDS = 1065;
    public static final int REQUEST_DELETE_FAST_PAYMENT = 1066;
    public static final int REQUEST_GET_IN_APP_SURVEY = 1067;
    public static final int REQUEST_IN_APP_SURVEY_UPLOAD = 1068;
}
