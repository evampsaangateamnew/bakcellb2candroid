package com.bakcell.webservices.core;

/**
 * Service endpoints.
 * <p>
 * Each class represents a different group of endpoints.
 *
 * @author Zeeshan
 */

public class EndPoints {

    public static class HeaderKeywords {
        public static final String KEY_MSISDN = "msisdn";
        public static final String KEY_TOKEN = "token";
        public static final String KEY_SUBSCRIBER_TYPE = "subscriberType";
        public static final String KEY_TARIFF_TYPE = "tariffType";

    }// HeaderType Keywords

    public static class Constants {
        public static final String CAUSE_SIGNUP = "signup";
        public static final String CAUSE_FORGOT = "forgotpassword";
        public static final String CAUSE_USAGEDETAIL = "usagehistory-details-view";
        public static final String CAUSE_MONEY_TRANSFER = "moneytransfer";
        public static final String KEY_CODE_LOGOUT = "7";

        public static final String KEY_CODE_WRONG_OLD_PASS_ATTEMPS = "104";

    }// Constants

    public static class ApiCache {
//        public static final String KEY_APP_MENUS = "app.menus";
//        public static final String KEY_DAHSBOARD = "key.dashboard";
//        public static final String KEY_FAQS = "key.faqs";
//        public static final String KEY_TARIFF = "key.tariff";
//        public static final String KEY_SUPPLEMENTARY_OFFERS = "key.supplemetary.offers";
//        public static final String KEY_MY_SUBSCRIPTIONS = "key.my.subscriptions";
//        public static final String KEY_FNF = "key.fiends.and.family";
//        public static final String KEY_CONTACT_US = "key.contact.us";
//        public static final String KEY_STORE_LOCATOR = "key.store.locator";
//        public static final String KEY_ULDUZUM_LOCATOR = "key.ulduzum.locator";
//        public static final String KEY_CORE_SERVICES = "key.core.services";
//        public static final String KEY_FREE_SMS_SERVICES = "key.free.sms.service";
//        public static final String KEY_NOTIFICATION_HISTORY = "key.notification.history";
//        public static final String KEY_LOAN_HISTORY = "key.loan.history";
//        public static final String KEY_PAYMENT_HISTORY = "key.payment.history";
//        public static final String KEY_USAGE_HISTORY_SUMMARY = "key.usage.history.summary";
//        public static final String KEY_USAGE_HISTORY_DETAIL = "key.usage.history.detail";
//        public static final String KEY_OPERATION_HISTORY = "key.operation.history";
    }// API Cache

    public static class Auth {
        public static final String APP_VERSION = "/generalservices/verifyappversion";
        public static final String LOGIN = "/customerservices/authenticateuser";
        public static final String SIGNUP_NUMBER_VERIFY = "/customerservices/signup";
        public static final String SIGNUP_PIN_VERIFY = "/customerservices/verifyotp";
        public static final String RESEND_PIN = "/customerservices/resendpin";
        public static final String SAVE_CUSTOMER = "/customerservices/savecustomer";
        public static final String FORGOT_ENDPOINT = "/customerservices/forgotpassword";
        public static final String CHANGE_ENDPOINT = "/customerservices/changepassword";
        public static final String REQUEST_USER_LOGOUT = "/customerservices/logout";
        public static final String REQUEST_CUSTOMER_DATA = "/customerservices/appresume";


    } // Auth

    public static class Dashboard {
        public static final String MENUS = "/menus/getappmenu";
        public static final String DASHBOARD_PAGE = "/homepageservices/gethomepage";
    }// Dashboard

    public static class Faqs {
        public static final String FAQS = "/generalservices/getfaqs";
    }// Faqs

    public static class Services {
        public static final String SUPPLEMENTARY_OFFERS = "/supplementaryofferings/getsupplementaryofferings";
        public static final String REQUEST_GET_FRIEND_AND_FAMILY = "/fnf/getfnf";
        public static final String REQUEST_DELETE_FRIEND_AND_FAMILY = "/fnf/deletefnf";
        public static final String REQUEST_ADD_FRIEND_AND_FAMILY = "/fnf/addfnf";
        public static final String GET_FREE_SMS_STATUS = "/quickservices/getfreesmsstatus";
        public static final String SEND_FREE_SMS = "/quickservices/sendfreesms";
        public static final String GET_CORE_SERVICE = "/coreservices/getcoreservices";
        public static final String ENABLE_DISABLE_CORE_SERVICE = "/coreservices/processcoreservices";
        public static final String SEND_INTERNET_SETTING_SERVICE = "/generalservices/sendinternetsettings";
        public static final String REQUEST_CHANGE_SUPPLEMENTARY_OFFERS = "/supplementaryofferings/changesupplementaryoffering";
        public static final String REQUEST_REPORT_SIM_LOST = "/generalservices/reportlostsim";

    }// Supplementary Offers

    public static class RateUsEndPoints {
        public static final String RATE_US_END_POINT = "/rateV2/getrateus";
        public static final String RATE_US_BEFORE_REDIRECTION_END_POINT = "/rateV2/rateus";
    }

    public static class Tariffs {
        public static final String TARIFF_LIST = "/tariffservices/gettariffdetails";
        public static final String TARIFF_CHANGE = "/tariffservices/changetariff";
    }// Tariffs

    public static class Topup {
        public static final String REQUEST_TOPUP = "/financialservices/requesttopup";
        public static final String REQUEST_MONEY_TRANSFER = "/financialservices/requestmoneytransfer";
        public static final String REQUEST_GET_LOAD = "/financialservices/getloan";
        public static final String REQUEST_GET_LOAD_HISTORY = "/financialservices/getloanhistory";
        public static final String REQUEST_GET_PAYMENT_HISTORY = "/financialservices/getpaymenthistory";
        public static final String REQUEST_GET_SAVED_CARDS = "/plasticcard/getsavedcards";
        public static final String REQUEST_INITIATE_PAYMENT = "/plasticcard/initiatepayment";
        public static final String REQUEST_MAKE_PAYMENT = "/plasticcard/makepayment";
        public static final String REQUEST_ADD_PAYMENT_SCHEDULER = "/plasticcard/addpaymentscheduler";
        public static final String REQUEST_GET_SCHEDULED_PAYMENTS = "/plasticcard/getscheduledpayments";

        public static final String REQUEST_DELETE_SAVED_CARD = "/plasticcard/deletesavedcard";
        public static final String REQUEST_DELETE_SCHEDULER_PAYMENT = "/plasticcard/deletepaymentscheduler";
    }// Topup

    public static class FastTopUp {
        public static final String REQUEST_FAST_TOP_UP = "/plasticcard/getfastpayments";
        public static final String REQUEST_DELETE_FAST_TOP_UP = "/plasticcard/deletepayment";
    }

    public static class UsageHistory {
        public static final String REQUEST_GET_USAGE_HISTORY_SUMMARY = "/history/getusagesummary";
        public static final String REQUEST_GET_USAGE_HISTORY_DETAILS = "/history/getusagedetails";
        public static final String REQUEST_GET_HISTORY_OPERATIONS = "/history/getoperationshistory";
        public static final String REQUEST_VERIFY_ACCOUNT_ID = "/history/verifyaccountdetails";
        public static final String REQUEST_VERIFY_PIN = "/history/verifypin";
        public static final String REQUEST_RESEND_PIN = "/history/historyresendpin";
    }// Usage History

    public static class MyAccount {
        public static final String REQUEST_MY_SUBSCRIPTIONS = "/mysubscriptions/getsubscriptions";
    }// My Account

    public static class SideMenus {
        public static final String REQUEST_STORE_LOCATOR = "/generalservices/getstoresdetails";
        public static final String REQUEST_CONTACT_US = "/generalservices/getcontactusdetails";
    }// My Account

    public static class Notifications {
        public static final String REQUEST_GET_NOTIFICATIONS = "/notifications/getnotifications";
        public static final String REQUEST_ADD_FCM = "/notifications/addfcm";
        public static final String REQUEST_GET_NOTIFICATIONS_COUNT = "/notifications/getnotificationscount";
    }// Notifications

    public static class Profile {
        public static final String REQUEST_UPLOAD_IMAGE = "/generalservices/uploadimage";
        public static final String REQUEST_UPDATE_EMAIL = "/customerservices/updatecustomeremail";
        public static final String REQUEST_CHANGE_BILLING_LANGUAGE = "/customerservices/changebillinglanguage";
    }// Profile

    public static class UlduzumAPIs {
        public static final String ULDUZUM_USAGE_DETAILS = "/ulduzum/getusagetotals";
        public static final String ULDUZUM_USAGE_HISTORY = "/ulduzum/getusagehistory";
        public static final String ULDUZUM_UNUSED_CODES = "/ulduzum/getunusedcodes";
        public static final String ULDUZUM_LOCATIONS_MERCHANT = "/ulduzum/getmerchants";
        public static final String ULDUZUM_DETAILS_MERCHANT = "/ulduzum/getmerchantdetails";
        public static final String ULDUZUM_GET_CODES = "/ulduzum/getcodes";
    }

    public static class RoamingCountriesAPIs {
        public static final String ROAMING_COUNTRIES_DATA = "/generalservices/getroaming";
        public static final String ROAMING_ACTIVATION = "/roaming/process";
        public static final String ROAMING_ACTIVATION_STATUS = "/roaming/status";
    }
    public static class InAppSurveyAPIs {
        public static final String GET_SURVEY_DATA = "/survey/getsurveys";
        public static final String UPLOAD_SURVEY_DATA = "/survey/savesurvey";
    }
} // EndPoints
