package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;

/**
 * @author Noman
 */

public class SignupNumberVerifyService extends BaseService {


    private SignupNumberVerifyService(Context context, int requestId) {
        super(context, requestId);
    }

    public static SignupNumberVerifyService newInstance(Context context, int requestId) {
        return new SignupNumberVerifyService(context, requestId);
    }

    public void execute(String msisdn, String cause, EaseCallbacks<String> callbacks) {

        JsonObject json = new JsonObject();
        json.addProperty("msisdn", msisdn);
        json.addProperty("cause", cause);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.SIGNUP_NUMBER_VERIFY)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
