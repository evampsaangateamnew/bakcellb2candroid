package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Created by Umair Mustafa on 07/12/2020.
 */

public class RequestRoamingActivationStatus extends BaseService {
    RequestRoamingActivationStatus(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestRoamingActivationStatus newInstance(Context c, int requestId) {
        return new RequestRoamingActivationStatus(c, requestId);
    }

    public void execute(UserModel customerData, String offeringId,
                        EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        JsonObject json = new JsonObject();
        json.addProperty("offeringId", offeringId);



        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.RoamingCountriesAPIs.ROAMING_ACTIVATION_STATUS)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
