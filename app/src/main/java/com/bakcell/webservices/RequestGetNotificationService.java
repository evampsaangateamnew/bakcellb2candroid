package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Created by Atif Arif on 10/6/2017.
 */

public class RequestGetNotificationService extends BaseService {

    private RequestGetNotificationService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestGetNotificationService newInstance(Context context, int requestId) {
        return new RequestGetNotificationService(context, requestId);
    }

    public void execute(UserModel customerData, String accountId, String pin, EaseCallbacks<String> callbacks) {
        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        String cacheKey = MultiAccountsHandler.CacheKeys.getNotificationsHistoryCacheKey(context);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Notifications.REQUEST_GET_NOTIFICATIONS)
                .method().post().hitEaseCacheAndRefresh(cacheKey)
                .build().execute(context);

    } // execute

}
