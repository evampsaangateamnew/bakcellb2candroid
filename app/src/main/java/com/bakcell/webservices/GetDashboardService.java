package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class GetDashboardService extends BaseService {

    private final String fromClass = "GetDashboardService";

    private GetDashboardService(Context context, int requestId) {
        super(context, requestId);
    }

    public static GetDashboardService newInstance(Context context, int requestId) {
        return new GetDashboardService(context, requestId);
    }

    public void execute(UserModel customerData, EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        JsonObject json = new JsonObject();
        json.addProperty("brandId", customerData.getBrandId());
        json.addProperty("offeringId", customerData.getOfferingId());

        if (customerData.getSubscriberType() != null) {
            //juni12891226
            json.addProperty("subscriberType", customerData.getSubscriberType().toLowerCase());
            json.addProperty("customerType", customerData.getSubscriberType().toLowerCase());
        }

        String cacheKey = MultiAccountsHandler.CacheKeys.getDashboardCacheKey(context);

        BakcellLogger.logE("homeKeyP", "cacheKey:::" + cacheKey, fromClass, "GetDashboardService->execute");

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Dashboard.DASHBOARD_PAGE)
                .method().post().easeCache(cacheKey, !RootValues.getInstance().isDashboardApiCall(), RootValues.getInstance().isDashboardApiCall())
                .body(json)
                .build().execute(context);

    } // execute
}
