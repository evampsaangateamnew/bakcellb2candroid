package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class GetSupplementaryOffersService extends BaseService {

    private GetSupplementaryOffersService(Context context, int requestId) {
        super(context, requestId);
    }

    public static GetSupplementaryOffersService newInstance(Context context, int requestId) {
        return new GetSupplementaryOffersService(context, requestId);
    }

    public void execute(UserModel customerData, EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        JsonObject json = new JsonObject();
        json.addProperty("offeringName", customerData.getOfferingName());
        json.addProperty("brandName", customerData.getBrandName());

        String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(context);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Services.SUPPLEMENTARY_OFFERS)
                .method().post()
                .body(json).easeCache(cacheKey, true, Tools.haveNetworkConnection(context))
                /*.easeCache(EndPoints.ApiCache.KEY_SUPPLEMENTARY_OFFERS + AppClass.getCurrentLanguageKey(context), !RootValues.getInstance().isSupplementaryOffersApiCall(), RootValues.getInstance().isSupplementaryOffersApiCall())*/
                .build().execute(context);

    } // execute
}
