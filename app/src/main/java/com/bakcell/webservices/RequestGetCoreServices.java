package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * Created by Zeeshan Shabbir on 10/10/2017.
 */

public class RequestGetCoreServices extends BaseService {
    RequestGetCoreServices(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestGetCoreServices newInstance(Context c, int requestId) {
        return new RequestGetCoreServices(c, requestId);
    }

    public void execute(UserModel customerData, EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (Tools.hasValue(customerData.getSubscriberType())) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (Tools.hasValue(customerData.getBrandName())) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        // Release 3 changes
        JsonObject json = new JsonObject();
        if (Tools.hasValue(customerData.getCustomerType())) {
            json.addProperty("accountType", customerData.getCustomerType());
        }
        if (Tools.hasValue(customerData.getGroupType())) {
            json.addProperty("groupType", customerData.getGroupType());
        }
        // Release 3 changes

        String cacheKey = MultiAccountsHandler.CacheKeys.getCoreServicesCacheKey(context);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Services.GET_CORE_SERVICE)
                .method().post()
                .body(json).hitEaseCacheAndRefresh(cacheKey)
                .build().execute(context);

    } // execute
}
