package com.bakcell.webservices;

import android.content.Context;

/**
 * @author Zeeshan
 *
 */

class BaseService {

    int requestId;
    Context context;
    boolean runInBackground;

    BaseService(Context context, int requestId) {
        this.requestId = requestId;
        this.context = context;
        this.runInBackground = false;
    }

    BaseService(Context context, int requestId, boolean runInBackground) {
        this.requestId = requestId;
        this.context = context;
        this.runInBackground = runInBackground;
    }


} // BaseService
