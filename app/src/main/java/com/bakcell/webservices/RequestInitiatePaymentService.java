package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Humayun Sikander on 07/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
public class RequestInitiatePaymentService extends BaseService {
    RequestInitiatePaymentService(Context context, int requestId) {
        super(context, requestId);
    }

    RequestInitiatePaymentService(Context context, int requestId, boolean runInBackground) {
        super(context, requestId, runInBackground);
    }

    public static RequestInitiatePaymentService newInstance(Context context, int requestId) {
        return new RequestInitiatePaymentService(context, requestId);
    }


    public void execute(UserModel customerData, String mobileNumber, String amount, long selectedCardKey, boolean isSaveCard, boolean isBackground, EaseCallbacks<String> callbacks) {
        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        // Body Paramenters
        JsonObject json = new JsonObject();

        json.addProperty("topupNumber", mobileNumber);
        json.addProperty("amount", amount);
        json.addProperty("cardType", selectedCardKey);
        json.addProperty("saved", "" + isSaveCard);




        EaseRequest.asString(String.class).runInBackground(isBackground)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Topup.REQUEST_INITIATE_PAYMENT)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
