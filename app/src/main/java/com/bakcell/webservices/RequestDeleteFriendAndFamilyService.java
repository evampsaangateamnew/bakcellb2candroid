package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class RequestDeleteFriendAndFamilyService extends BaseService {


    private RequestDeleteFriendAndFamilyService(Context context, int requestId) {
        super(context, requestId);
    }

    public static RequestDeleteFriendAndFamilyService newInstance(Context context, int requestId) {
        return new RequestDeleteFriendAndFamilyService(context, requestId);
    }

    public void execute(UserModel customerData, String fnfMsisdn, String s, EaseCallbacks<String> callbacks) {
        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }
        // Body Paramenters
        JsonObject json = new JsonObject();
        json.addProperty("deleteMsisdn", fnfMsisdn.substring(3,fnfMsisdn.length()));
        json.addProperty("offeringId", customerData.getOfferingId());

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Services.REQUEST_DELETE_FRIEND_AND_FAMILY)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
