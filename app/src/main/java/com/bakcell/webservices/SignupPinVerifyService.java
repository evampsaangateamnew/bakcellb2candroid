package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.utilities.Tools;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class SignupPinVerifyService extends BaseService {


    private SignupPinVerifyService(Context context, int requestId) {
        super(context, requestId);
    }

    public static SignupPinVerifyService newInstance(Context context, int requestId) {
        return new SignupPinVerifyService(context, requestId);
    }

    public void execute(String msisdn, String pin, String cause, EaseCallbacks<String> callbacks) {

        if (Tools.hasValue(pin)) {
            pin = pin.trim();
        }

        RequestHeaders headers = RequestHeaders.newInstance();

        JsonObject json = new JsonObject();
        json.addProperty("msisdn", msisdn);
        json.addProperty("pin", pin);
        json.addProperty("cause", cause);


        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.SIGNUP_PIN_VERIFY)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
