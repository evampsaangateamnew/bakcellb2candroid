package com.bakcell.webservices;

import android.content.Context;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.globals.RootValues;
import com.bakcell.models.user.UserModel;
import com.bakcell.services.MyHelp;
import com.bakcell.utilities.Logger;
import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class ChangePasswordService extends BaseService {


    private ChangePasswordService(Context context, int requestId) {
        super(context, requestId);
    }

    public static ChangePasswordService newInstance(Context context, int requestId) {
        return new ChangePasswordService(context, requestId);
    }

    public void execute(UserModel customerData, String currentPassword, String newPassword, String confirmPassword, EaseCallbacks<String> callbacks) {

        if (customerData == null) {
            Toast.makeText(context, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Header Paramenters
        RequestHeaders headers = RequestHeaders.newInstance();
        headers.put(EndPoints.HeaderKeywords.KEY_MSISDN, customerData.getMsisdn());
        headers.put(EndPoints.HeaderKeywords.KEY_TOKEN, customerData.getToken());
        if (customerData.getSubscriberType() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_SUBSCRIBER_TYPE, customerData.getSubscriberType().toLowerCase());
        }
        if (customerData.getBrandName() != null) {
            headers.put(EndPoints.HeaderKeywords.KEY_TARIFF_TYPE, customerData.getBrandName().toLowerCase(Locale.ENGLISH));
        }

        String oldValEncript = "", newValEncript = "", confirmValEncript = "";
        try {
            String decriptedKey = MyHelp.decrypt(RootValues.getInstance().getCLASS_NAME_SE(), new String(RootValues.getInstance().getCLASS_NAME_M().getBytes()));
            oldValEncript = MyHelp.encrypt(currentPassword, decriptedKey.trim());
            newValEncript = MyHelp.encrypt(newPassword, decriptedKey.trim());
            confirmValEncript = MyHelp.encrypt(confirmPassword, decriptedKey.trim());
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        // Body Paramenters
        JsonObject json = new JsonObject();
        json.addProperty("oldPassword", oldValEncript);
        json.addProperty("newPassword", newValEncript);
        json.addProperty("confirmNewPassword", confirmValEncript);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.CHANGE_ENDPOINT)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
