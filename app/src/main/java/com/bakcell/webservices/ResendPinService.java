package com.bakcell.webservices;

import android.content.Context;

import com.bakcell.webservices.core.EndPoints;
import com.google.gson.JsonObject;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.RequestHeaders;

/**
 * @author Noman
 */

public class ResendPinService extends BaseService {


    private ResendPinService(Context context, int requestId) {
        super(context, requestId);
    }

    public static ResendPinService newInstance(Context context, int requestId) {
        return new ResendPinService(context, requestId);
    }

    public void execute(String msisdn, String cause, EaseCallbacks<String> callbacks) {

        RequestHeaders headers = RequestHeaders.newInstance();

        JsonObject json = new JsonObject();
        json.addProperty("msisdn", msisdn);
        json.addProperty("cause", cause);

        EaseRequest.asString(String.class)
                .responseCallbacks(callbacks).headers(headers)
                .requestId(requestId)
                .endPoint(EndPoints.Auth.RESEND_PIN)
                .method().post()
                .body(json)
                .build().execute(context);

    } // execute
}
