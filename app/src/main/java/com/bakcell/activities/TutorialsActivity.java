package com.bakcell.activities;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.activities.authentications.LoginActivity;
import com.bakcell.adapters.AdapterTutorials;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityTutorialsBinding;
import com.bakcell.fragments.pager.tutorials.TutorialPaggerFragment;
import com.bakcell.models.tutorials.Tutorial;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class TutorialsActivity extends BaseActivity implements View.OnClickListener,
        TutorialPaggerFragment.OnSplashPageChangeListener {

    protected ViewPager viewPager;

    protected CircleIndicator indicator;

    protected TextView tvSkip;

    protected ActivityTutorialsBinding binding;

    protected List<Tutorial> tutorialList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tutorials);
        AppEventLogs.applyAppEvent(AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN,
                AppEventLogValues.TutorialEvents.NEXT_1,
                AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN);
        initUiContent();

        iniPaggerView();
    }

    private void initUiContent() {
        viewPager = binding.viewPager;
        indicator = binding.indicator;
        tvSkip = binding.tvSkip;
        tvSkip.setOnClickListener(this);
        binding.btnGetStarted.setOnClickListener(this);
    }

    private void iniPaggerView() {
        tutorialList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            if (i == 0) {
                Tutorial tutorial = new Tutorial();
                tutorial.setTitle(getResources().getString(R.string.welcome));//Welcome
                tutorial.setMessage(getResources().getString(R.string.toBackcell));//To my Backcell
                tutorial.setRes(Tools.getBakcellLocalizedLogo(TutorialsActivity.this));
                tutorialList.add(tutorial);
            }
            if (i == 1) {
                Tutorial tutorial = new Tutorial();
                tutorial.setTitle(getResources().getString(R.string.explore));
                tutorial.setMessage(getResources().getString(R.string.exploreMessage));
                tutorial.setRes(R.drawable.tutorial_explore_gif);
                tutorialList.add(tutorial);
            }
            if (i == 2) {
                Tutorial tutorial = new Tutorial();
                tutorial.setTitle(getResources().getString(R.string.manage));
                tutorial.setMessage(getResources().getString(R.string.onTheGo));
                tutorial.setRes(R.drawable.tutorial_manage_gif);
                tutorialList.add(tutorial);
            }
            if (i == 3) {
                Tutorial tutorial = new Tutorial();
                tutorial.setTitle(getResources().getString(R.string.topup));
                tutorial.setMessage(getResources().getString(R.string.accountinstantly));
                tutorial.setRes(R.drawable.tutorial_topup_gif);
                tutorialList.add(tutorial);
            }
            if (i == 4) {
                Tutorial tutorial = new Tutorial();
                tutorial.setTitle(getResources().getString(R.string.getStarted));
                tutorial.setMessage(getResources().getString(R.string.withMyBakcellapp));
                tutorial.setRes(R.drawable.tutorial_getstarted_gif);
                tutorialList.add(tutorial);
            }
        }
        viewPager.setAdapter(new AdapterTutorials(getSupportFragmentManager(), tutorialList, this));
        indicator.setViewPager(viewPager);

        binding.tvSkip.setVisibility(View.VISIBLE);
        binding.btnGetStarted.setVisibility(View.INVISIBLE);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 4) {
                    binding.tvSkip.setVisibility(View.INVISIBLE);
                    binding.btnGetStarted.setVisibility(View.VISIBLE);
                } else {
                    binding.tvSkip.setVisibility(View.VISIBLE);
                    binding.btnGetStarted.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void skipClicked() {
        btnGetStartedClicked();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.tv_skip:
                skipClicked();
                break;
            case R.id.btn_get_started:
                btnGetStartedClicked();
                break;
        }
    }

    private void btnGetStartedClicked() {
        PrefUtils.addBoolean(this, PrefUtils.PreKeywords.PREF_IS_FIRST_TIME, false);
        startNewActivityAndClear(this, LoginActivity.class);
    }

    @Override
    public void changePage(int i) {
        viewPager.setCurrentItem(viewPager.getCurrentItem() + i);
        sendTutoralEvent(i);
    }

    private void sendTutoralEvent(int i) {
        switch (i) {
            case 0:
                AppEventLogs.applyAppEvent(AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN,
                        AppEventLogValues.TutorialEvents.NEXT_1,
                        AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN);
                break;
            case 1:
                AppEventLogs.applyAppEvent(AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN,
                        AppEventLogValues.TutorialEvents.NEXT_2,
                        AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN);
                break;
            case 2:
                AppEventLogs.applyAppEvent(AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN,
                        AppEventLogValues.TutorialEvents.NEXT_3,
                        AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN);
                break;
            case 3:
                AppEventLogs.applyAppEvent(AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN,
                        AppEventLogValues.TutorialEvents.NEXT_4,
                        AppEventLogValues.TutorialEvents.TUTORIAL_SCREEN);
                break;
            default:
        }
    }

}
