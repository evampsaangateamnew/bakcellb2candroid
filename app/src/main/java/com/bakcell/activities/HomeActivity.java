package com.bakcell.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.activities.authentications.LoginActivity;
import com.bakcell.activities.landing.ChangePasswordActivity;
import com.bakcell.activities.landing.InAppTutorialActivity;
import com.bakcell.activities.landing.ManageAccountsActivity;
import com.bakcell.activities.landing.ProfileActivity;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.activities.livechat.LiveChatSDKActivity;
import com.bakcell.adapters.MultiAccountsPopupRecyclerAdapter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityHomeBinding;
import com.bakcell.dialogs.InAppThankYouDialog;
import com.bakcell.fragments.menus.ContactUsFragment;
import com.bakcell.fragments.menus.DashboardFragment;
import com.bakcell.fragments.menus.FAQFragment;
import com.bakcell.fragments.menus.MyAccountFragment;
import com.bakcell.fragments.menus.NotificationsFragment;
import com.bakcell.fragments.menus.RoamingBottomNavFragment;
import com.bakcell.fragments.menus.RoamingCountriesFragment;
import com.bakcell.fragments.menus.ServicesFragment;
import com.bakcell.fragments.menus.SettingsFragment;
import com.bakcell.fragments.menus.StoreLocatorFragment;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.fragments.menus.TermsAndConditionsFragment;
import com.bakcell.fragments.menus.UlduzumFragment;
import com.bakcell.fragments.pager.StoreLocatorMapPaggerFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MenusHandler;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.globals.RootViews;
import com.bakcell.interfaces.CustomerInfoListener;
import com.bakcell.interfaces.InAppSurveyUploadListener;
import com.bakcell.interfaces.ManageAccountSwitchingEvents;
import com.bakcell.interfaces.NotificationCounterPopupEvents;
import com.bakcell.interfaces.OnProfileImageUpdate;
import com.bakcell.models.DataManager;
import com.bakcell.models.manageaccounts.UsersHelperModel;
import com.bakcell.models.menus.MenuData;
import com.bakcell.models.menus.MenuHorizontalItem;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.CacheConfig;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.MenusService;
import com.bakcell.webservices.RequestLogoutService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.webservices.customerdata.CustomerData;
import com.bakcell.webservices.inappsurvey.SurveyData;
import com.bakcell.webservices.inappsurvey.UploadSurveyData;
import com.bakcell.widgets.BakcellButtonBold;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;
import de.hdodenhof.circleimageview.CircleImageView;


public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener {

    //webview vars
    private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> uploadMessage;
    private final int REQUEST_SELECT_FILE = 134;
    private final int FILECHOOSER_RESULTCODE = 128;
    ////////////////
    public static final String KEY_HOME_FIRST_SCREEN = "key_first_screen";
    private ProgressDialog progressDialog = null;
    public static final int MAX_CHARACTERS_ALLOWED = 20;
    public static final int MAX_CHARACTERS_BEFORE_DOTS = 16;
    private boolean isURLLoadSuccess = false;
    protected DrawerLayout drawer_layout;
    protected NavigationView navMenuView;
    protected CircleImageView profileImage;

    protected BakcellTextViewNormal page_title;

    protected RelativeLayout notification_menu;
    protected RelativeLayout offer_list_change_layout;
    protected ImageView listing_icon;
    protected RelativeLayout setting_layout;
    protected RelativeLayout search_menu;

    private View navHeaderLayout;

    protected BakcellTextViewNormal menu_name_txt;
    protected BakcellTextViewNormal menu_number_txt;
    private RelativeLayout userInfoHolder;
    protected ImageView userBrandIcon;

    private ActionBarDrawerToggle toggle;
    BakcellButtonBold manageAccountsAdderButton;
    private ActivityHomeBinding binding;
    private AlertDialog alertDialog;
    private boolean isNotificationScreen = false;
    private boolean isRoamingCountriesScreen = false;
    private Bundle webViewBundle;
    private final String fromClass = "HomeActivity";

    public static void logoutUser(Activity activity) {

        if (activity != null && !activity.isFinishing()) {
        } else {
            return;
        }

        if (MultiAccountsHandler.getCurrentlyLoggedInUsersCount(activity) > 1) {
            /**there are multiple users logged in
             * so delete the currently logged in user from
             * the list of currently logged in users
             * and redirect the user to the manage accounts screen
             * in this case the user can only stay on the manage accounts screen
             * put a flag of "isManageAccountsScreenToShow so that
             * when the user goes back and comes again, should only see the manage accounts screen
             * same is the case of change password"*/
            //delete the current user
            MultiAccountsHandler.deleteUser(activity, DataManager.getInstance().getCurrentUser().getMsisdn());

            //Disabling Dynamic Linking here
            RootValues.getInstance().setDynamicLinkRedirection(false);

            //manage accounts screen needs to be shown
            PrefUtils.addBoolean(activity, PrefUtils.PreKeywords.PREF_IS_MANAGE_ACCOUNTS_SCREEN_NEED_TO_SHOW, true);
            //redirect the user to the manage accounts screen, killing all back stacked screens; so that he won't come back
            Bundle bundle = new Bundle();
            bundle.putString(MultiAccountsHandler.MultiAccountConstants.FROM_FORCE_LOGOUT,
                    MultiAccountsHandler.MultiAccountConstants.FROM_FORCE_LOGOUT_VALUE);
            BaseActivity.startNewActivityAndClear(activity, ManageAccountsActivity.class, bundle);
        } else {
            /**there is currently a single user logged in
             * so logged out that single user at the moment*/
            try {
                PrefUtils.addCustomerData(activity.getApplicationContext(), null);
                PrefUtils.addBoolean(activity.getApplicationContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false);
                PrefUtils.addBoolean(activity.getApplicationContext(), PrefUtils.PreKeywords.PREF_APP_RATER_SHOWN, false);
                ChangePasswordActivity.resetWrongPasswordAttemps(activity);

                DataManager.getInstance().setCurrentUser(null);
                DataManager.getInstance().setUserPredefinedData(null);
                DataManager.getInstance().setUserPrimaryOffering(null);
                DataManager.getInstance().setUserSupplementaryOfferingsList(null);

                EaseCacheManager.destroyCache(activity.getApplicationContext());

                RootValues.getInstance().setIS_USAGE_DETAIL_OTP_VERIFY(false);

                //Disabling Dynamic Linking here
                RootValues.getInstance().setDynamicLinkRedirection(false);

                // Clear Cache for disclaimer dialog
                CacheConfig.removeCacheDisclaimerDialog(activity);

                startNewActivityAndClear(activity, LoginActivity.class);

                AppEventLogs.applyAppEvent(AppEventLogValues.LogoutDialogEvents.USER_LOGGED_OUT,
                        AppEventLogValues.LogoutDialogEvents.USER_LOGGED_OUT,
                        AppEventLogValues.LogoutDialogEvents.USER_LOGGED_OUT);

            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        PrefUtils.addBoolean(HomeActivity.this, PrefUtils.PreKeywords.PREF_IS_MANAGE_ACCOUNTS_SCREEN_NEED_TO_SHOW, false);
        RootValues.getInstance().setMenuHorizontalItems(null);
        RootValues.getInstance().setMenuVerticalItems(null);

        /**setup the manage accounts popup ui*/
        manageAccountsAdderButton = binding.manageAccountPopup.findViewById(R.id.manageAccountsAdderButton);
        manageAccountsAdderButton.setSelected(true);

        // Get Current User Data from DB
        loadCustomerDataFromDB();

        initUiContent();

        initUiListeners();

        initNavigationDrawer();

        initFooterMenus();

        binding.mainContentContainer.setVisibility(View.VISIBLE);
        binding.webViewContent.setVisibility(View.GONE);

        RootValues.getInstance().setOnProfileImageUpdate(onProfileImageUpdate);
        /**only for the testing purposes, if you want to force logout the user
         * uncomment this code and the user will be force logged out*/
//        HomeActivity.logoutUser(HomeActivity.this);

        getIntentDataForDynamicLinks();
    }

    private void getIntentDataForDynamicLinks() {

        String screenNameForRedirection = "";
        String tabNameForRedirection = "";
        String offeringIdForRedirection = "";

        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA)
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA) &&
                getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA) &&
                Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA)) &&
                Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA)) &&
                Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA))) {
            screenNameForRedirection = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA);
            tabNameForRedirection = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA);
            offeringIdForRedirection = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA);
        } else if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA)
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA) &&
                Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA)) &&
                Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA))) {
            screenNameForRedirection = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA);
            tabNameForRedirection = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA);
        } else if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA)
                && Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA))) {
            screenNameForRedirection = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA);
        }

        if (Tools.hasValue(screenNameForRedirection) && Tools.hasValue(tabNameForRedirection) && Tools.hasValue(offeringIdForRedirection)) {
            processDynamicLinkDataForScreenNameTabNameAndOfferingID(screenNameForRedirection, tabNameForRedirection, offeringIdForRedirection);
        } else if (Tools.hasValue(screenNameForRedirection) && Tools.hasValue(tabNameForRedirection)) {
            processDynamicLinkDataForScreenNameAndTabName(screenNameForRedirection, tabNameForRedirection);
        } else if (Tools.hasValue(screenNameForRedirection)) {
            processDynamicLinkDataForScreenNameOnly(screenNameForRedirection);
        }
    }

    private void processDynamicLinkDataForScreenNameOnly(String screenNameForRedirection) {
        BakcellLogger.logE(Constants.DynamicLinksConstants.DYNAMIC_LINK_LOGGER_KEY,
                "screenNameForRedirection = " + screenNameForRedirection,
                fromClass, "processDynamicLinkDataForScreenNameOnly");
        if (screenNameForRedirection.equalsIgnoreCase(Constants.DynamicLinksRedirectionValuesConstants.SUPPLEMENTARY_PAGE)) {
            BaseActivity.startNewActivity(HomeActivity.this, SupplementaryOffersActivity.class);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RootViews.getInstance().setNotificationCountText(null);
        RootValues.getInstance().setIsTariffFromDashBoard(false);
        RootValues.getInstance().setOfferingIdRedirectFromNotifications(null);

    }

    /**
     * In this function we are loading all customer saved data from DB into classes
     */
    private void loadCustomerDataFromDB() {

        DataManager.getInstance().setCurrentUser(null);
        DataManager.getInstance().setUserPrimaryOffering(null);
        DataManager.getInstance().setUserSupplementaryOfferingsList(null);
        DataManager.getInstance().setUserPredefinedData(null);

        UserMain userMain = PrefUtils.getCustomerData(HomeActivity.this);
        if (userMain != null) {
            DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
            DataManager.getInstance().setUserPrimaryOffering(userMain.getPrimaryOffering());
            DataManager.getInstance().setUserSupplementaryOfferingsList(userMain.getSupplementaryOfferingList());
            DataManager.getInstance().setUserPredefinedData(userMain.getPredefinedData());
        }

    }

    private void initUiListeners() {

        binding.idViewHeader.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hamBurgerMenuOpen();
            }
        });

        binding.idViewHeader.notificationMenu.setOnClickListener(this);
        binding.idViewHeader.searchMenu.setOnClickListener(this);
        binding.idViewHeader.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isNotificationScreen) {
                    try {
                        NotificationsFragment notificationsFragment = (NotificationsFragment)
                                getSupportFragmentManager()
                                        .findFragmentByTag(Constants.MenusKeyword.MENU_NOTIFICATIONS);
                        notificationsFragment.onSearch(s.toString());
                    } catch (Exception e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    if (isRoamingCountriesScreen) {
                        //search on roaming countries
                        RoamingCountriesFragment roamingCountriesFragment = (RoamingCountriesFragment) getSupportFragmentManager()
                                .findFragmentByTag(Constants.MenusKeyword.MENU_ROAMING_COUNTRIES);
                        roamingCountriesFragment.onSearch(s.toString());
                    } else {
                        try {
                            FAQFragment faqFragment = (FAQFragment) getSupportFragmentManager()
                                    .findFragmentByTag(Constants.MenusKeyword.MENU_FAQ);
                            faqFragment.onSearch(s.toString());
                        } catch (Exception e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void initUiContent() {

        drawer_layout = binding.drawerLayout;
        navMenuView = binding.navMenuView;

        //Toolbar menus
        page_title = binding.idViewHeader.pageTitle;
        notification_menu = binding.idViewHeader.notificationMenu;
        RootViews.getInstance().setNotificationCountText(binding.idViewHeader.notificationCountText);
        RootViews.getInstance().getNotificationCountText().setVisibility(View.INVISIBLE);
        offer_list_change_layout = binding.idViewHeader.offerListChangeLayout;
        listing_icon = binding.idViewHeader.listingIcon;
        setting_layout = binding.idViewHeader.settingLayout;
        search_menu = binding.idViewHeader.searchMenu;
    }


    private void initFooterMenus() {
        binding.idViewFooter.footerMenu1.setOnClickListener(this);
        binding.idViewFooter.footerMenu2.setOnClickListener(this);
        binding.idViewFooter.footerMenu3.setOnClickListener(this);
        binding.idViewFooter.footerMenu4.setOnClickListener(this);
        binding.idViewFooter.footerMenu5.setOnClickListener(this);

        binding.idViewFooter.footerMenuText1.setSelected(true);
        binding.idViewFooter.footerMenuText2.setSelected(true);
        binding.idViewFooter.footerMenuText3.setSelected(true);
        binding.idViewFooter.footerMenuText4.setSelected(true);
        binding.idViewFooter.footerMenuText5.setSelected(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (StoreLocatorMapPaggerFragment.LOCATION_ON == requestCode && RootValues.getInstance().getLocationOnListener() != null) {
            RootValues.getInstance().getLocationOnListener().onLocationOnListener(requestCode);
        }

        //code for file handling for the webview
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
            Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else {
            Toast.makeText(HomeActivity.this, "Failed to Upload Image", Toast.LENGTH_LONG).show();
        }
    }

    public void loadPicture(String url) {
        Picasso.with(this).load(url).networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(target);
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            try {
                profileImage.setImageBitmap(bitmap);
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    public void initNavigationDrawer() {
        navHeaderLayout = navMenuView.getHeaderView(0);

        menu_name_txt = navHeaderLayout.findViewById(R.id.menu_name_txt);
        menu_name_txt.setSelected(true);
        menu_number_txt = navHeaderLayout.findViewById(R.id.menu_number_txt);
        menu_number_txt.setSelected(true);
        userBrandIcon = navHeaderLayout.findViewById(R.id.userBrandIcon);
        userInfoHolder = navHeaderLayout.findViewById(R.id.userInfoHolder);
        profileImage = navHeaderLayout.findViewById(R.id.profile_image);

        manageAccountsAdderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideManageAccountsList();
                closeNavigationMenuDrawer();
                /**redirect the user to the manage accounts screen*/
                BaseActivity.startNewActivity(HomeActivity.this, ManageAccountsActivity.class);
            }
        });

        userInfoHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**show the manage accounts popup*/
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    showManageAccountsList();
                    processCurrentlyLoggedInUsers();
                }//if (!isManageAccountsListVisible()) ends
            }
        });

        setUserNameAndNumber();

////        Request for getting menus from server
////        And Also request for Customer data if User loggedIn and app is open after close
        if (RootValues.getInstance().isIS_APP_OPENED_AFTER_CLOSED() && Tools.isNetworkAvailable(HomeActivity.this)) {
            RootValues.getInstance().setIS_APP_OPENED_AFTER_CLOSED(false);
            CustomerData.requestForGetCustomerData(HomeActivity.this, new CustomerInfoListener() {
                @Override
                public void onCustomerInfoListener(EaseResponse<String> response) {

                    // Check if user logout by server
                    try {
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            HomeActivity.logoutUser(HomeActivity.this);
                            return;
                        }
                    } catch (Exception e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                    // Continue process
                    requestForMenus();
                }
            });


        } else {
            requestForMenus();
        }

        SurveyData.requestForGetInAppSurveyData(HomeActivity.this, response -> { });

        toggle = new ActionBarDrawerToggle(this, drawer_layout, R.string.nav_drawer_opened, R.string.nav_drawer_closed) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideManageAccountsList();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                hideManageAccountsList();
            }
        };
        drawer_layout.addDrawerListener(toggle);

        navMenuView.setNavigationItemSelectedListener(this);
        navMenuView.setItemIconTintList(null);
        View headerView = navMenuView.getHeaderView(0);
        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    if (drawer_layout != null && drawer_layout.isDrawerOpen(GravityCompat.START)) {
                        drawer_layout.closeDrawer(GravityCompat.START);
                    }
                    startNewActivity(HomeActivity.this, ProfileActivity.class);
                }
            }
        });

        /**get the user accounts data from the preferences,
         * get the currently logged in users and show in the side menu
         * drawer recyclerview*/
        processCurrentlyLoggedInUsers();
    }

    private void processCurrentlyLoggedInUsers() {
        if (MultiAccountsHandler.getCustomerDataFromPreferences(HomeActivity.this) != null &&
                !MultiAccountsHandler.getCustomerDataFromPreferences(HomeActivity.this).toString().
                        equalsIgnoreCase("{}") &&
                MultiAccountsHandler.getCustomerDataFromPreferences(HomeActivity.this).userModelArrayList != null &&
                !MultiAccountsHandler.getCustomerDataFromPreferences(HomeActivity.this).userModelArrayList.isEmpty()) {

            /**process the users accounts data to the recyclerview*/
            ArrayList<UserModel> userModelArrayList =
                    MultiAccountsHandler.getOrderedMultiAccountsList(
                            MultiAccountsHandler.getCustomerDataFromPreferences(HomeActivity.this).userModelArrayList);

            RecyclerView manageAccountsRecycler = binding.manageAccountPopup.findViewById(R.id.manageAccountsRecycler);

            //create the recycler adapter
            MultiAccountsPopupRecyclerAdapter multiAccountsPopupRecyclerAdapter =
                    new MultiAccountsPopupRecyclerAdapter(HomeActivity.this, getApplicationContext(), userModelArrayList);
            //create the layout manager
            LinearLayoutManager layoutManager = new LinearLayoutManager(
                    getApplicationContext(), RecyclerView.VERTICAL, false);
            //set the layout manager
            manageAccountsRecycler.setLayoutManager(layoutManager);
            //set the adapter
            manageAccountsRecycler.setAdapter(multiAccountsPopupRecyclerAdapter);
        } else {
            BakcellLogger.logE("cuRenX", "no one user logged in!!!", fromClass, "processCurrentlyLoggedInUsers");
            ArrayList<UserModel> newUserModelArrayList = new ArrayList<>();
            newUserModelArrayList.add(0, PrefUtils.getCustomerData(HomeActivity.this).getCustomerData());
            MultiAccountsHandler.setCustomerDataToPreferences(HomeActivity.this, new UsersHelperModel(newUserModelArrayList));
        }
    }//processCurrentlyLoggedInUsers ends

    private boolean isManageAccountsListVisible() {
        return binding.manageAccountPopup.getVisibility() == View.VISIBLE;
    }//isManageAccountsListVisible ends

    private void showManageAccountsList() {
        binding.manageAccountPopup.setVisibility(View.VISIBLE);
    }//showManageAccountsList ends

    private void hideManageAccountsList() {
        binding.manageAccountPopup.setVisibility(View.GONE);
        RootValues.getInstance().setLAST_SELECT_MENU("");
    }//hideManageAccountsList ends

    private void setUserNameAndNumber() {

        menu_name_txt.setText("");
        menu_name_txt.setSelected(true);
        menu_number_txt.setText("");

        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String userName = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getFirstName())) {
                userName = DataManager.getInstance().getCurrentUser().getFirstName();
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMiddleName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getMiddleName());
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLastName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getLastName());
            }

            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getImageURL())) {
                loadPicture(DataManager.getInstance().getCurrentUser().getImageURL());
            }

           /* if (userName.length() > MAX_CHARACTERS_ALLOWED) {
                String shortenName = userName.substring(0, Math.min(userName.length(),
                        MAX_CHARACTERS_BEFORE_DOTS))
                        .concat(getString(R.string.side_drawer_name_long_name_indicator));

                SpannableString ss = new SpannableString(shortenName);
                Drawable d = ContextCompat.getDrawable(this, R.drawable.drop_down_arrow_state_grey);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                ss.setSpan(span, 0, shortenName.length() - 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                menu_name_txt.setText(ss);
            } else {*/
                /*SpannableString ss = new SpannableString(userName);
                Drawable d = ContextCompat.getDrawable(this, R.drawable.drop_down_arrow_state_grey);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                ss.setSpan(span, 0, userName.length() - 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

            }*/

         /*   Spannable span = new SpannableString(userName);
            Drawable d = getResources().getDrawable(R.drawable.multidrop);
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            ImageSpan image = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
            span.setSpan(image, userName.length()-1, userName.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);*/
            menu_name_txt.setText(userName);

            menu_name_txt.setSelected(true);
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                menu_number_txt.setText(DataManager.getInstance().getCurrentUser().getMsisdn());
            }
        }

        // Set user brand icon
        userBrandIcon.setImageDrawable(null);
        if (DataManager.getInstance().getCurrentUser() != null && Tools.hasValue(DataManager.getInstance().getCurrentUser().getBrandName())) {
            if (DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
                userBrandIcon.setImageDrawable(ContextCompat.getDrawable(HomeActivity.this, R.drawable.title_cin));
            } else if (DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_KLASS)) {
                userBrandIcon.setImageDrawable(ContextCompat.getDrawable(HomeActivity.this, R.drawable.title_klass));
            } else if (DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_BUSINESS)) {
                userBrandIcon.setImageDrawable(ContextCompat.getDrawable(HomeActivity.this, R.drawable.title_business));
            }
        }

    }


    public void hamBurgerMenuOpen() {
        if (drawer_layout != null) {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START);
            } else {
                drawer_layout.openDrawer(GravityCompat.START);
                drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            }
        }
    }


    @Override
    public void onBackPressed() {

        if (drawer_layout != null && drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
            return;
        }

       /* if (binding.embeddedChatWindow != null && binding.embeddedChatWindow.isShown()) {
            Tools.hideKeyboard(HomeActivity.this);
            binding.embeddedChatWindow.onBackPressed();
            return;
        }*/

        if (binding.idViewHeader.searchView.getVisibility() == View.VISIBLE) {
            hideOrShowSearchView(View.VISIBLE, View.GONE);
        }

        if (getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS) != null &&
                getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS).getArguments() != null &&
                getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS).getArguments().containsKey(TariffsFragment.KEY_DATA_FROM_NOTIFICATION)) {
            // Check if Redirection from Tariff
            selectMenu(Constants.MenusKeyword.MENU_NOTIFICATIONS, null);
        } else if (getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_STORE_LOCATOR) != null &&
                getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_STORE_LOCATOR).getArguments() != null &&
                getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_STORE_LOCATOR).getArguments().containsKey(StoreLocatorFragment.KEY_FROM_CONTACT_US)) {
            // Check if Redirection from Contact US
            selectMenu(Constants.MenusKeyword.MENU_CONTACT_US, null);
        } else {
            //check if webview is visible, then hide it and show the main content
            //if webview is not visible, then finish the activity or on back pressed call
            if (binding.webViewContent.getVisibility() == View.VISIBLE) {
                //check if the webview is visible
                //if visible, check if it can go back
                //if it can go back, redirect it to back
                //else hide the webview container
                //if its not visible, check for other statements!
                if (binding.webView.copyBackForwardList().getCurrentIndex() > 0) {
                    binding.webView.goBack();
                } else {
                    binding.webView.pauseTimers();
                    binding.webViewContent.setVisibility(View.GONE);
                    binding.mainContentContainer.setVisibility(View.VISIBLE);
                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }//if (binding.webView.copyBackForwardList().getCurrentIndex() > 0)
            } else {
                drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                binding.webView.pauseTimers();
                if (RootValues.getInstance().getLAST_SELECT_MENU() != null &&
                        !RootValues.getInstance().getLAST_SELECT_MENU().equalsIgnoreCase(Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD)) {
                    selectMenu(Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD, null);
                } else {
                    super.onBackPressed();
                }
            }//if (binding.webViewContent.getVisibility() == View.VISIBLE)
        }//main else ends

    }//on back press ends

    boolean isSearchOpened = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notification_menu:
                hideOrShowSearchView(View.VISIBLE, View.GONE);
                selectMenu(Constants.MenusKeyword.MENU_NOTIFICATIONS, null);
                break;
            case R.id.search_menu:
                if (isSearchOpened) {
                    hideOrShowSearchView(View.VISIBLE, View.GONE);
                    binding.idViewHeader.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.actionbar_search));
                    isSearchOpened = false;
                    resetAdapters();
                } else {
                    clearSearchView();
                    hideOrShowSearchView(View.GONE, View.VISIBLE);
                    binding.idViewHeader.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.ic_cross));
                    isSearchOpened = true;
                }
                break;
            default:
                hideOrShowSearchView(View.VISIBLE, View.GONE);
                selectMenu(getHorizontalMenuKeyFromMenuID(v.getId()), null);

        }
    }

    private void resetAdapters() {
        if (isNotificationScreen) {
            try {
                NotificationsFragment notificationsFragment = (NotificationsFragment) getSupportFragmentManager()
                        .findFragmentByTag(Constants.MenusKeyword.MENU_NOTIFICATIONS);
                notificationsFragment.onSearchClosed();
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());

            }
        } else {
            if (isRoamingCountriesScreen) {
                //reset roaming countries adapter
                RoamingCountriesFragment roamingCountriesFragment = (RoamingCountriesFragment) getSupportFragmentManager()
                        .findFragmentByTag(Constants.MenusKeyword.MENU_ROAMING_COUNTRIES);
                roamingCountriesFragment.onSearchClosed();
            } else {
                try {
                    FAQFragment faqFragment = (FAQFragment) getSupportFragmentManager()
                            .findFragmentByTag(Constants.MenusKeyword.MENU_FAQ);
                    faqFragment.onSearchClosed();
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        }
    }

    public void clearSearchView() {
        if (binding.idViewHeader.etSearch.getVisibility() == View.VISIBLE) {
            binding.idViewHeader.etSearch.setText("");
        }
    }

    /**
     * Shows and hide searchview
     *
     * @param pageTitle
     * @param searchView
     */
    private void hideOrShowSearchView(int pageTitle, int searchView) {
        binding.idViewHeader.pageTitle.setVisibility(pageTitle);
        binding.idViewHeader.searchView.setVisibility(searchView);
        if (binding.idViewHeader.searchView.getVisibility() == View.VISIBLE) {
            binding.idViewHeader.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.ic_cross));
            binding.idViewHeader.etSearch.requestFocus();
            Tools.showKeyboard(this);
            isSearchOpened = true;
        } else {
            binding.idViewHeader.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.actionbar_search));
            Tools.hideKeyboard(this);
            isSearchOpened = false;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        try {
            // Set all Uncheck before menu selection
            for (int i = 0; i < navMenuView.getMenu().size(); i++) {
                navMenuView.getMenu().getItem(i).setChecked(false);
            }

            item.setChecked(true);

            selectMenu(getVeriticalMenuKeyFromMenuID(item.getItemId()), null);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        return false;
    }

    /**
     * Request for getString App menus from server
     */
    private void requestForMenus() {

        MenusService.newInstance(HomeActivity.this, ServiceIDs.APP_MENUS)
                .execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        String resultData = response.getData();

                        MenuData menuData = new Gson().fromJson(resultData, MenuData.class);
                        getMenusFromResponse(menuData);
                        // Set Fragment for first time
                        setFragmentForFirstTime();

                        if (PrefUtils.getBoolean(HomeActivity.this, PrefUtils.PreKeywords.PREF_IS_FIRST_TIME_LOGIN, true)) {
                            PrefUtils.addBoolean(HomeActivity.this, PrefUtils.PreKeywords.PREF_IS_FIRST_TIME_LOGIN, false);
                            startNewActivity(HomeActivity.this, InAppTutorialActivity.class);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        // Check if user logout by server
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            HomeActivity.logoutUser(HomeActivity.this);
                            return;
                        }

                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getMenusCacheKey(HomeActivity.this);
                        String cacheResponse = EaseCacheManager.get(HomeActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();

                                MenuData menuData = new Gson().fromJson(cacheData, MenuData.class);
                                getMenusFromResponse(menuData);
                                // Set Fragment for first time
                                setFragmentForFirstTime();

                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                prepareLogOutMenuOnFailure();
                            }
                        } else {
                            prepareLogOutMenuOnFailure();
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {

                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getMenusCacheKey(HomeActivity.this);
                        String cacheResponse = EaseCacheManager.get(HomeActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();

                                MenuData menuData = new Gson().fromJson(cacheData, MenuData.class);
                                getMenusFromResponse(menuData);
                                // Set Fragment for first time
                                setFragmentForFirstTime();

                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                prepareLogOutMenuOnFailure();
                            }
                        } else {
                            prepareLogOutMenuOnFailure();
                        }
                    }
                });

    }

    private void setFragmentForFirstTime() {
        try {
            // Select Default Value for fragment to Open
            if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(KEY_HOME_FIRST_SCREEN)) {
                String firstScreen = getIntent().getExtras().getString(KEY_HOME_FIRST_SCREEN);
                if (Tools.hasValue(firstScreen)) {
                    selectMenu(firstScreen, null);
                } else {
                    selectMenu(Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD, null);
                }
            } else {
                selectMenu(Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD, null);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    private void getMenusFromResponse(MenuData menuData) {
        /**set the horizontal and vertical menus in the root values*/
        if (menuData != null) {
            //vertical menus
            RootValues.getInstance().setMenuVerticalItems(MenusHandler.getLocalizedVerticalMenu(getApplicationContext(), menuData));
            //horizontal menus
            RootValues.getInstance().setMenuHorizontalItems(MenusHandler.getLocalizedHorizontalMenu(getApplicationContext(), menuData));

            /**set the localized side navigation drawer menus*/
            prepareDrawerMenus();

            /**set the localized bottom menus*/
            prepareBottomMenus();
        } else {
            prepareLogOutMenuOnFailure();
            BakcellLogger.logE("menuX", "no menus from server:::111", fromClass, "getMenusFromResponse");
            Toast.makeText(HomeActivity.this, "No menus from server.", Toast.LENGTH_SHORT).show();
        }//menus not null from the server
    }

    private void prepareBottomMenus() {
        if (RootValues.getInstance().getMenuHorizontalItems() != null &&
                !RootValues.getInstance().getMenuHorizontalItems().isEmpty()) {
            //clear to proceed
        } else {
            prepareLogOutMenuOnFailure();
            BakcellLogger.logE("menuX", "no bottom menus from server", fromClass, "getMenusFromResponse");
            Toast.makeText(HomeActivity.this, "No bottom menus from server.", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            Collections.sort(RootValues.getInstance().getMenuHorizontalItems(), new Comparator<MenuHorizontalItem>() {
                public int compare(MenuHorizontalItem o1, MenuHorizontalItem o2) {
                    if (o1.getSortOrderInt() == o2.getSortOrderInt())
                        return 0;
                    return o1.getSortOrderInt() < o2.getSortOrderInt() ? -1 : 1;
                }
            });
        } catch (Exception e) {
            BakcellLogger.logE("SortOrderX12", "sortOrderError:::" + e.toString(), fromClass, "prepareBottomMenus");
        }


        if (RootValues.getInstance().getMenuHorizontalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuHorizontalItems().size(); i++) {
                int menuId = -1;
                switch (RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier()) {
                    case Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD:
                        menuId = setMenusIconByOrder(RootValues.getInstance().getMenuHorizontalItems()
                                        .get(i).getSortOrder(), R.drawable.bottom_menu_dashboard_deseleted,
                                getResources().getString(R.string.footer_icon_dashboard_label));
                        break;
                    case Constants.MenusKeyword.MENU_BOTTOM_TARIFFS:
                        menuId = setMenusIconByOrder(RootValues.getInstance().getMenuHorizontalItems()
                                        .get(i).getSortOrder(), R.drawable.bottom_menu_tariffs_deseleted,
                                getResources().getString(R.string.footer_icon_tariffs_label));
                        break;
                    case Constants.MenusKeyword.MENU_BOTTOM_SERVICES:
                        menuId = setMenusIconByOrder(RootValues.getInstance().getMenuHorizontalItems()
                                        .get(i).getSortOrder(), R.drawable.bottom_menu_supplementory_offer_deseleted,
                                getResources().getString(R.string.footer_icon_services_label));
                        break;
                    case Constants.MenusKeyword.MENU_BOTTOM_ROAMING:
                        menuId = setMenusIconByOrder(RootValues.getInstance().getMenuHorizontalItems()
                                        .get(i).getSortOrder(), R.drawable.bottom_menu_roaming_deselected,
                                getResources().getString(R.string.footer_icon_roaming_label));
                        break;
                    case Constants.MenusKeyword.MENU_BOTTOM_MYACCOUTN:
                        menuId = setMenusIconByOrder(RootValues.getInstance().getMenuHorizontalItems()
                                        .get(i).getSortOrder(), R.drawable.bottom_menu_account_deseleted,
                                getResources().getString(R.string.footer_icon_myaccount_label));
                        break;
                    default:
                        break;
                }

                RootValues.getInstance().getMenuHorizontalItems().get(i).setMenuItemId(menuId);

            }
        }

    }

    private int setMenusIconByOrder(String order, int icon, String labelText) {
        int menuId = -1;
        switch (order) {
            case "1":
                binding.idViewFooter.footerMenuText1.setText(labelText);
                binding.idViewFooter.icFooterMenu1.setImageDrawable(ContextCompat
                        .getDrawable(HomeActivity.this, icon));
                menuId = R.id.footerMenu1;
                break;
            case "2":
                binding.idViewFooter.footerMenuText2.setText(labelText);
                binding.idViewFooter.icFooterMenu2.setImageDrawable(ContextCompat
                        .getDrawable(HomeActivity.this, icon));
                menuId = R.id.footerMenu2;
                break;
            case "3":
                binding.idViewFooter.footerMenuText3.setText(labelText);
                binding.idViewFooter.icFooterMenu3.setImageDrawable(ContextCompat
                        .getDrawable(HomeActivity.this, icon));
                menuId = R.id.footerMenu3;
                break;
            case "4":
                binding.idViewFooter.footerMenuText4.setText(labelText);
                binding.idViewFooter.icFooterMenu4.setImageDrawable(ContextCompat
                        .getDrawable(HomeActivity.this, icon));
                menuId = R.id.footerMenu4;
                break;
            case "5":
                binding.idViewFooter.footerMenuText5.setText(labelText);
                binding.idViewFooter.icFooterMenu5.setImageDrawable(ContextCompat
                        .getDrawable(HomeActivity.this, icon));
                menuId = R.id.footerMenu5;
                break;
        }

        return menuId;
    }

    private void prepareLogOutMenuOnFailure() {
        Menu m = navMenuView.getMenu();
        if (m == null) return;

        if (m.size() > 0) {
            return;
        }

        setNagigationMenusTextColor();

        m.setGroupCheckable(0, true, true);

        int menuItemId = 1006;
        int menusIcon = -1;
        menusIcon = R.drawable.menu_logout;

        try {
            m.add(0, menuItemId, 0, getResources().getString(R.string.logout_popup_signout_button)).setIcon(menusIcon);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        try {
            if (m != null) {
                MenuItem mi = m.getItem(m.size() - 1);
                if (mi != null) {
                    mi.setTitle(mi.getTitle());
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        navMenuView.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == 1006) {
                BakcellLogger.logE("loGX", "called", fromClass, "onNavigationItemSelected");
                closeNavigationMenuDrawer();
                isRoamingCountriesScreen = false;

                ArrayList<UserModel> userModelArrayList =
                        MultiAccountsHandler.getCustomerDataFromPreferences(HomeActivity.this).userModelArrayList;

                if (userModelArrayList != null && userModelArrayList.size() > 1) {
                    /**show the logout popup for multiple users logged in*/
                    showLogoutAlertForMultipleUsers(MultiAccountsHandler.getLocalizedLogOutMessageForMultipleUsers(getApplicationContext()));
                } else {
                    /**show the logout popup for single user logged in*/
                    showLogoutAlert();
                }
            }
            return false;
        });
    }//prepareLogOutMenuOnFailure ends

    private void prepareDrawerMenus() {
        if (RootValues.getInstance().getMenuVerticalItems() != null &&
                !RootValues.getInstance().getMenuVerticalItems().isEmpty()) {
            //clear to proceed
        } else {
            prepareLogOutMenuOnFailure();
            BakcellLogger.logE("menuX", "no vertical menus from server", fromClass, "getMenusFromResponse");
            return;
        }

        try {
            Collections.sort(RootValues.getInstance().getMenuVerticalItems(), (o1, o2) -> {
                if (o1.getSortOrderInt() == o2.getSortOrderInt())
                    return 0;
                return o1.getSortOrderInt() < o2.getSortOrderInt() ? -1 : 1;
            });
        } catch (Exception e) {
            BakcellLogger.logE("SortOrderX12", "sortOrderError:::" + e.toString(), fromClass, "prepareDrawerMenus");
        }

        Menu m = navMenuView.getMenu();
        if (m == null) return;

        setNagigationMenusTextColor();

        if (RootValues.getInstance().getMenuVerticalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuVerticalItems().size(); i++) {
                m.setGroupCheckable(i, true, true);

                int menuItemId = 100 + i;

                RootValues.getInstance().getMenuVerticalItems().get(i).setMenuItemId(menuItemId);

                int menusIcon = -1;
                String menusTitle = RootValues.getInstance().getMenuVerticalItems().get(i).getTitle();

                switch (RootValues.getInstance().getMenuVerticalItems().get(i).getIdentifier()) {
                    case Constants.MenusKeyword.MENU_NOTIFICATIONS:
                        menusIcon = R.drawable.menu_notification;
                        break;
                    case Constants.MenusKeyword.MENU_STORE_LOCATOR:
                        menusIcon = R.drawable.menu_storelocator;
                        break;
                    case Constants.MenusKeyword.MENU_FAQ:
                        menusIcon = R.drawable.menu_faqs;
                        break;
                    case Constants.MenusKeyword.MENU_TUTORIALS_AND_FAQS:
                        menusIcon = R.drawable.menu_toturial_and_faqs;
                        break;
                    case Constants.MenusKeyword.MENU_LIVE_CHAT:
                        menusIcon = R.drawable.menu_live_chat;
                        break;
                    case Constants.MenusKeyword.MENU_CONTACT_US:
                        menusIcon = R.drawable.menu_contact_us;
                        break;
                    case Constants.MenusKeyword.MENU_TERMS_AND_CONDITIONS:
                        menusIcon = R.drawable.menu_terms_and_conditions;
                        break;
                    case Constants.MenusKeyword.MENU_SETTINGS:
                        menusIcon = R.drawable.menu_settings;
                        break;
                    case Constants.MenusKeyword.MENU_ULDUZUM:
                        menusIcon = R.drawable.ulduzummenuicon;
                        break;
                    case Constants.MenusKeyword.MENU_ROAMING_COUNTRIES:
                        menusIcon = R.drawable.bottom_menu_roaming_selected;
                        break;
                    case Constants.MenusKeyword.MENU_LOGOUT:
                        menusIcon = R.drawable.menu_logout;
                        break;
                    default:
                        continue;
                }
                try {
                    m.add(i, menuItemId, i, menusTitle).setIcon(menusIcon);
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        }

        try {
            if (m != null) {
                MenuItem mi = m.getItem(m.size() - 1);
                if (mi != null) {
                    mi.setTitle(mi.getTitle());
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    private void setNagigationMenusTextColor() {
        int[][] state = new int[][]{
                new int[]{-android.R.attr.state_checkable},
                new int[]{android.R.attr.state_checked}
        };
        int[] color = new int[]{
                R.color.colorPrimary,
                R.color.text_gray
        };
        ColorStateList cs = new ColorStateList(state, color);
        navMenuView.setItemTextColor(cs);
    }

    private void closeNavigationMenuDrawer() {
        if (drawer_layout != null && drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        }
    }//closeNavigationMenuDrawer ends

    public void selectMenu(String menuKey, Bundle bundle) {
        BakcellLogger.logE("menuKeyX", "menu key saved wali:::" + RootValues.getInstance().getLAST_SELECT_MENU(), fromClass, "selectMenu");
        if (!Tools.hasValue(menuKey)) return;

       /* if (Tools.hasValue(RootValues.getInstance().getLAST_SELECT_MENU()) && RootValues.getInstance().getLAST_SELECT_MENU().equalsIgnoreCase(menuKey)) {
            BakcellLogger.logE("menuKeyX", "empty or matched:::" + RootValues.getInstance().getLAST_SELECT_MENU(), fromClass, "selectMenu");
            return;
        }*/

        RootValues.getInstance().setLAST_SELECT_MENU(menuKey);

        Fragment fragment = null;
        hideOrShowSearchView(View.VISIBLE, View.GONE);

        switch (menuKey) {
            // Side Drawer menus
            case Constants.MenusKeyword.MENU_NOTIFICATIONS:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isNotificationScreen = true;
                    isRoamingCountriesScreen = false;
                    updatePageTitle(getString(R.string.menu_notifications));
                    fragment = new NotificationsFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_STORE_LOCATOR:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    updatePageTitle(getString(R.string.menu_store_locator));
                    fragment = new StoreLocatorFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_FAQ:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    isNotificationScreen = false;
                    updatePageTitle(getString(R.string.faq));
                    fragment = new FAQFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_TUTORIALS_AND_FAQS:
                RootValues.getInstance().setLAST_SELECT_MENU("");
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    startNewActivity(HomeActivity.this, InAppTutorialActivity.class);
                }
                break;
            case Constants.MenusKeyword.MENU_LIVE_CHAT:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    //juni1289
                    if (getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD) != null) {
                        RootValues.getInstance().setLAST_SELECT_MENU(Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD);
                    } else if (getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_ROAMING) != null) {
                        RootValues.getInstance().setLAST_SELECT_MENU(Constants.MenusKeyword.MENU_BOTTOM_ROAMING);
                    } else if (getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_SERVICES) != null) {
                        RootValues.getInstance().setLAST_SELECT_MENU(Constants.MenusKeyword.MENU_BOTTOM_SERVICES);
                    } else if (getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS) != null) {
                        RootValues.getInstance().setLAST_SELECT_MENU(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS);
                    } else if (getSupportFragmentManager().findFragmentByTag(Constants.MenusKeyword.MENU_BOTTOM_MYACCOUTN) != null) {
                        RootValues.getInstance().setLAST_SELECT_MENU(Constants.MenusKeyword.MENU_BOTTOM_MYACCOUTN);
                    }
                    //setup the webview
//                    setupWebView();
                    loadChatActivity();
                }
                break;
            case Constants.MenusKeyword.MENU_CONTACT_US:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    updatePageTitle(getString(R.string.menu_contact_us));
                    fragment = new ContactUsFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_TERMS_AND_CONDITIONS:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    updatePageTitle(getString(R.string.menu_terms_and_conditions));
                    fragment = new TermsAndConditionsFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_SETTINGS:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    updatePageTitle(getString(R.string.menu_settings));
                    fragment = new SettingsFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_ULDUZUM:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;
                    isNotificationScreen = false;
                    updatePageTitle(getString(R.string.ulduzum_screen__title_label));
                    fragment = new UlduzumFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_ROAMING_COUNTRIES:
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = true;
                    isNotificationScreen = false;
                    updatePageTitle(getString(R.string.roaming_countries_fragment_screen_label));
                    fragment = new RoamingCountriesFragment();
                    updateUIFooterMenuSeleted("", -1);
                }
                break;
            case Constants.MenusKeyword.MENU_LOGOUT:
                RootValues.getInstance().setLAST_SELECT_MENU("");
                if (isManageAccountsListVisible()) {
                    hideManageAccountsList();
                } else {
                    closeNavigationMenuDrawer();
                    isRoamingCountriesScreen = false;

                    ArrayList<UserModel> userModelArrayList =
                            MultiAccountsHandler.getCustomerDataFromPreferences(HomeActivity.this).userModelArrayList;

                    if (userModelArrayList != null && userModelArrayList.size() > 1) {
                        /**show the logout popup for multiple users logged in*/
                        showLogoutAlertForMultipleUsers(MultiAccountsHandler.getLocalizedLogOutMessageForMultipleUsers(getApplicationContext()));
                    } else {
                        /**show the logout popup for single user logged in*/
                        showLogoutAlert();
                    }
                }
                break;
            // Footer Menus
            case Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD:
                isRoamingCountriesScreen = false;
                updatePageTitle(getString(R.string.app_name_dashboard));
                fragment = new DashboardFragment();
                updateUIFooterMenuSeleted(Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD, R.drawable.bottom_menu_dashboard_seleted);
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_TARIFFS:
                isRoamingCountriesScreen = false;
                updatePageTitle(getString(R.string.menu_footer_tariffs));
                fragment = new TariffsFragment();
                updateUIFooterMenuSeleted(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS, R.drawable.bottom_menu_tariffs_seleted);
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_SERVICES:
                isRoamingCountriesScreen = false;
                updatePageTitle(getString(R.string.menu_footer_services));
                fragment = new ServicesFragment();
                updateUIFooterMenuSeleted(Constants.MenusKeyword.MENU_BOTTOM_SERVICES, R.drawable.bottom_menu_supplementory_offer_seleted);
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_ROAMING:
                isRoamingCountriesScreen = false;
                updatePageTitle(getString(R.string.menu_footer_roaming));
                fragment = new RoamingBottomNavFragment();
                updateUIFooterMenuSeleted(Constants.MenusKeyword.MENU_BOTTOM_ROAMING, R.drawable.bottom_menu_roaming_selected);
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_MYACCOUTN:
                isRoamingCountriesScreen = false;
                updatePageTitle(getString(R.string.menu_footer_account));
                fragment = new MyAccountFragment();
                updateUIFooterMenuSeleted(Constants.MenusKeyword.MENU_BOTTOM_MYACCOUTN, R.drawable.bottom_menu_account_seleted);
                break;

            default:
        }

        if (fragment != null) {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            replaceNewFragment(fragment, menuKey);
        }


        actionBarMenuUpdateAccordingToScreen(menuKey);

    }

    private void loadChatActivity() {
        BaseActivity.startNewActivity(this, LiveChatSDKActivity.class);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebView() {
        try {
            if (this.isFinishing()) {
                //safe catch for activity not running and do not show the dialog etc
                return;
            }//if (this.isFinishing())

            //user have no active data connection show error
            if (!Tools.haveNetworkConnection(HomeActivity.this)) {
                if (HomeActivity.this.isFinishing()) {
                    return;
                }
                isURLLoadSuccess = false;
                binding.mainContentContainer.setVisibility(View.VISIBLE);
                binding.webViewContent.setVisibility(View.GONE);
                //show error dialog
                BakcellPopUpDialog.showMessageDialog(HomeActivity.this, getString(R.string.bakcell_error_title), getString(R.string.message_no_internet));
            } else {
                //if progress dialog already is showing kill it
                killProgressDialog();

                /*hide the main content*/
                binding.mainContentContainer.setVisibility(View.GONE);
                binding.webViewContent.setVisibility(View.VISIBLE);

                //disable the drawer
                drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                //show the loading dialog
                progressDialog = new ProgressDialog(HomeActivity.this);
                progressDialog.setTitle(R.string.loading);
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);

                //enable zoom view controls
                binding.webView.resumeTimers();
                binding.webView.getSettings().setBuiltInZoomControls(true);
                binding.webView.getSettings().setDisplayZoomControls(true);

                //load the links inside the web view
                binding.webView.getSettings().setAllowContentAccess(true);
                binding.webView.getSettings().setAllowFileAccess(true);
                binding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

                //enable javascript
                binding.webView.getSettings().setJavaScriptEnabled(true);

                //enable the dom storage
                binding.webView.getSettings().setDomStorageEnabled(true);

                //enable sounds
                binding.webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

                //set cache settings
                binding.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

                //for images loading
                binding.webView.getSettings().setLoadsImagesAutomatically(true);

                //set the plugin state
                binding.webView.getSettings().setPluginState(WebSettings.PluginState.ON);

                //load the url inside the webview
                String url = "";

                if (DataManager.getInstance().getUserPredefinedData() != null) {
                    if (Tools.hasValue(DataManager.getInstance().getUserPredefinedData().getLiveChat())) {
                        url = DataManager.getInstance().getUserPredefinedData().getLiveChat();
                    } else {
                        //show error dialog
                        BakcellPopUpDialog.showMessageDialog(HomeActivity.this, getString(R.string.bakcell_error_title), getString(R.string.server_stopped_responding_please_try_again));
                    }
                } else {
                    //show error dialog
                    BakcellPopUpDialog.showMessageDialog(HomeActivity.this, getString(R.string.bakcell_error_title), getString(R.string.server_stopped_responding_please_try_again));
                }

                //old url stylos
               /* if (getUserEmail().contains(getUserMsisdn())) {
                    url = "https://my.bakcell.com:8444/livezilla/enschat.php?ptn=" + getUserName() +
                            "&ptl=" + AppClass.getCurrentLanguageKey(getApplicationContext()) +
                            "&ptp=" + getUserMsisdn() +
                            "&hcgs=MQ__&htgs=MQ__&hfk=MQ__&epc=I0UwMDAzNA__&esc=I0UwMDAzNA__";
                } else {
                    url = "https://my.bakcell.com:8444/livezilla/enschat.php?ptn=" + getUserName() +
                            "&pte=" + getUserEmail() +
                            "&ptl=" + AppClass.getCurrentLanguageKey(getApplicationContext()) +
                            "&ptp=" + getUserMsisdn() +
                            "&hcgs=MQ__&htgs=MQ__&hfk=MQ__&epc=I0UwMDAzNA__&esc=I0UwMDAzNA__";
                }*/

                JsonObject json = new JsonObject();
                json.addProperty("authenticate", "Ev@mp1iv3Ch@t");

                //register the webview listener
                registerWebViewListener();
                initWebViewHeaderContent();
                setupChromeClientWithWebView();

                if (!isURLLoadSuccess) {
                    progressDialog.show();
                    binding.webView.resumeTimers();
                    binding.webView.postUrl(url, json.toString().getBytes());

                }// if (!isURLLoadSuccess) ends
                else {
                    binding.webView.resumeTimers();
                }

            }//else for internet check ends here
        } catch (Exception exp) {
            if (HomeActivity.this.isFinishing()) {
                return;
            }//if ends
            Logger.debugLog(Logger.TAG_CATCH_LOGS, "setup webview:::" + exp.getMessage());
            killProgressDialog();
            //show error dialog
            BakcellPopUpDialog.showMessageDialog(HomeActivity.this, getString(R.string.bakcell_error_title), getString(R.string.server_stopped_responding_please_try_again));
        }//catch ends
    }//setupWebView ends

    private void setupChromeClientWithWebView() {
        //get the camera permissions
        String permission = android.Manifest.permission.CAMERA;
        int grant = ContextCompat.checkSelfPermission(this, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(this, permission_list, 1);
        }//if ends

        //set the webchrome client here
        binding.webView.setWebChromeClient(new WebChromeClient() {
            // For 3.0+ Devices (Start)
            // onActivityResult attached before constructor
            protected void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"), FILECHOOSER_RESULTCODE);
            }


            // For Lollipop 5.0+ Devices
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = fileChooserParams.createIntent();
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(HomeActivity.this, "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isURLLoadSuccess) {
            binding.webView.resumeTimers();
        }

        binding.webView.onResume();
        initManageAccountEvents();
        initNotificationCounterPopupEvents();
        RootValues.getInstance().setCallingSurveyFromDialog(
                (uploadSurvey, onTransactionComplete, dialog, updateListOnSuccessfullySurvey) -> {
                    UploadSurveyData.requestForUploadInAppSurveyData(this, uploadSurvey, new InAppSurveyUploadListener() {
                        @Override
                        public void onInAppSurveyFailListener(EaseResponse<String> response) {

                            if (response != null && response.getDescription() != null) {
                                if (HomeActivity.this != null && !HomeActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.showMessageDialog(HomeActivity.this,
                                            getString(R.string.bakcell_error_title),
                                            response.getDescription()
                                    );
                                }
                            } else {
                                if (HomeActivity.this != null && !HomeActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.showMessageDialog(HomeActivity.this,
                                            getString(R.string.bakcell_error_title),
                                            getString(R.string.server_stopped_responding_please_try_again)
                                    );
                                }
                            }
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onInAppSurveySuccessListener() {
                            if (updateListOnSuccessfullySurvey != null) {
                                updateListOnSuccessfullySurvey.updateListOnSuccessfullySurvey();
                            }
                            if (HomeActivity.this != null && !HomeActivity.this.isFinishing()) {
                                InAppThankYouDialog thankYouDialog = new InAppThankYouDialog(HomeActivity.this, onTransactionComplete);
                                thankYouDialog.show();
                            }
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    });

                });
    }

    private void initNotificationCounterPopupEvents() {
        NotificationCounterPopupEvents notificationCounterPopupEvents = () -> {
            selectMenu(Constants.MenusKeyword.MENU_NOTIFICATIONS, null);//go to the notifications screen
        };

        RootValues.getInstance().setNotificationCounterPopupEvents(notificationCounterPopupEvents);
    }//initNotificationCounterPopupEvents ends

    private void initManageAccountEvents() {
        ManageAccountSwitchingEvents manageAccountSwitchingEvents = switchedUserModel -> {
            MultiAccountsHandler.updateCurrentlyLoggedInUserModelWithNewUserModel(HomeActivity.this, switchedUserModel);
            /**reset the user data for apis calling, menus and promo message*/
            MultiAccountsHandler.resetAllUserDataAndAPICallerCaches(HomeActivity.this);
            /**restart the current activity*/
            BaseActivity.startNewActivityAndClear(HomeActivity.this, HomeActivity.class);
        };

        RootValues.getInstance().setManageAccountSwitchingEvents(manageAccountSwitchingEvents);
    }//initManageAccountEvents ends

    @Override
    protected void onPause() {
        super.onPause();
        killProgressDialog();
        binding.webView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initWebViewHeaderContent() {
        binding.webViewtoolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.webViewtoolbar.pageTitle.setText(R.string.menu_live_chat);

        binding.webViewtoolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if it can go back
                //if it can go back, redirect it to back
                //else hide the webview container
                //if its not visible, check for other statements!
                if (binding.webView.copyBackForwardList().getCurrentIndex() > 0) {
                    binding.webView.goBack();
                } else {
                    binding.webView.pauseTimers();
                    binding.webViewContent.setVisibility(View.GONE);
                    binding.mainContentContainer.setVisibility(View.VISIBLE);
                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }//if (binding.webView.copyBackForwardList().getCurrentIndex() > 0)
            }
        });
    }

    private String getUserName() {
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String userName = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getFirstName())) {
                userName = DataManager.getInstance().getCurrentUser().getFirstName();
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMiddleName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getMiddleName());
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLastName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getLastName());
            }

            //return the username
            return userName;
        }

        return "";
    }

    private String getUserEmail() {
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String email = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getEmail())) {
                email = DataManager.getInstance().getCurrentUser().getEmail();
            }

            //return the email
            return email;
        }

        return "";
    }

    private String getUserMsisdn() {
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String msisdn = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                msisdn = DataManager.getInstance().getCurrentUser().getMsisdn();
            }

            //return the email
            return msisdn;
        }

        return "";
    }

    private String getUserPhone() {
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String phone = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                phone = DataManager.getInstance().getCurrentUser().getMsisdn();
            }

            //return the email
            return phone;
        }

        return "";
    }

    private void killProgressDialog() {
        if (this.isFinishing()) {
            return; //safe passage
        }// if (this.isFinishing()) ends
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }//if(progressDialog.isShowing())
        }//if(progressDialog!=null)
    }//killProgressDialog ends

    private void registerWebViewListener() {
        try {
            binding.webView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    if (!Tools.haveNetworkConnection(HomeActivity.this)) {
                        //check if there is no active network connection
                        //if no active connection, hide the view and show the toast
                        Toast.makeText(HomeActivity.this, HomeActivity.this.getResources().getString(R.string.live_chat_load_failed), Toast.LENGTH_SHORT).show();
                        isURLLoadSuccess = false;
                        binding.webView.setVisibility(View.GONE);
                        binding.noWebData.setVisibility(View.VISIBLE);
                    } else {
                        //there is an active connection show the web view and kill the progress dialog
                        binding.webView.setVisibility(View.VISIBLE);
                        binding.noWebData.setVisibility(View.GONE);
                        isURLLoadSuccess = true;
                    }
                    //hide the dialog
                    killProgressDialog();
                    // do your stuff here for url being loaded in the webview
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, "onPageFinished->webview");
                }//onPageFinished ends

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    isURLLoadSuccess = false;
                    //hide the dialog
                    if (HomeActivity.this.isFinishing()) {
                        return;
                    }

                    //hide the webview and kill the progress dialog
                    binding.webView.setVisibility(View.GONE);
                    binding.noWebData.setVisibility(View.VISIBLE);
                    killProgressDialog();
                    //url load success is false because on received error called
                    isURLLoadSuccess = false;
                    //Your code to do for handling the error
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, "onReceivedError2->webview:::" + errorCode + " description:::" + description);
                    //show error dialog
                    BakcellPopUpDialog.showMessageDialog(HomeActivity.this, getString(R.string.bakcell_error_title), getString(R.string.server_stopped_responding_please_try_again));
                }//onReceivedError ends

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.toLowerCase().contains("&file=")) {
                        //if url contains &file= it means that the url is pointing to some file
                        //open the url in the external browser and let the file to download itself to user device storage
                        Intent i1 = new Intent(Intent.ACTION_VIEW, Uri.parse("about:blank"));
                        startActivity(i1);
                        Intent i2 = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(i2);
                    } else {
                        //the url has no file param
                        //so we have to load the url inside the webview
                        //show the dialog
                        if (progressDialog == null) {
                            //show the loading dialog
                            progressDialog = new ProgressDialog(HomeActivity.this);
                            progressDialog.setTitle(R.string.loading);
                            progressDialog.setCancelable(false);
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.show();
                        }//if (progressDialog != null) ends

                        //load the respective url inside the web view
                        view.loadUrl(url);
                    }
                    return true;
                }//shouldOverrideUrlLoading ends
            });
        } catch (Exception exp) {
            if (HomeActivity.this.isFinishing()) {
                return;
            }
            Logger.debugLog(Logger.TAG_CATCH_LOGS, "registerListener:::" + exp.getMessage());
            //hide the dialog
            killProgressDialog();
            //show error dialog
            BakcellPopUpDialog.showMessageDialog(HomeActivity.this, getString(R.string.bakcell_error_title), getString(R.string.server_stopped_responding_please_try_again));
        }//catch ends
    }//registerWebViewListener ends

    // Live Chat Code Below
    // Real: 3528751
    private String LIVECHAT_SUPPORT_LICENCE_NR = "3528751";
    private String GROUP_ID = "0";
    private String VISITOR_NAME = "";
    private String VISITOR_EMAIL = "";

    private void showEmbadedViewInHome() {

        if (!Tools.isNetworkAvailable(HomeActivity.this)) {
            Toast.makeText(getApplicationContext(), getString(R.string.message_no_internet), Toast.LENGTH_SHORT).show();
            return;
        }

        //juni1289 changes
       /* if (binding.embeddedChatWindow.isInitialized()) {
            binding.embeddedChatWindow.showChatWindow();
            return;
        }

        binding.embeddedChatWindow.setUpWindow(getChatWindowConfiguration());
        binding.embeddedChatWindow.initialize();
        binding.embeddedChatWindow.showChatWindow();

        binding.embeddedChatWindow.setUpListener(new ChatWindowView.ChatWindowEventsListener() {
            @Override
            public void onChatWindowVisibilityChanged(boolean windowVisible) {
                if (!windowVisible) {
                    Tools.hideKeyboard(HomeActivity.this);
                    binding.embeddedChatWindow.onBackPressed();
                }
            }

            @Override
            public void onNewMessage(NewMessageModel newMessageModel, boolean windowVisible) {
                if (!windowVisible) {
                    showMessageDialogAppUpdate(HomeActivity.this, getString(R.string.live_chat_attention), newMessageModel.getText());
                }
            }

            @Override
            public void onStartFilePickerActivity(Intent intent, int i) {

            }

            @Override
            public boolean handleUri(Uri uri) {
                return false;
            }
        });*/


    }

   /* public ChatWindowConfiguration getChatWindowConfiguration() {

        if (DataManager.getInstance().getCurrentUser() != null) {
            VISITOR_NAME = DataManager.getInstance().getCurrentUser().getFirstName();
            VISITOR_EMAIL = DataManager.getInstance().getCurrentUser().getEmail();
        }
        return new ChatWindowConfiguration(LIVECHAT_SUPPORT_LICENCE_NR, GROUP_ID, VISITOR_NAME, VISITOR_EMAIL, null);
    }*/

    private void showMessageDialogAppUpdate(final Context context, String title, String message) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            if (((Activity) context).isFinishing()) return;

            android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_app_live_chat, null);
            LinearLayout linearLayout = dialogView.findViewById(R.id.root_layout);
            LinearLayout actionLayoutOptionUpdate = dialogView.findViewById(R.id.actionLayoutOptionUpdate);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final android.app.AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal btn_cancel = dialogView.findViewById(R.id.btn_cancel);
            BakcellButtonNormal btn_update = dialogView.findViewById(R.id.btn_update);
            BakcellTextViewBold tvTitle = dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {// Got to live Chat
                    showEmbadedViewInHome();
                    alertDialog.dismiss();
                }
            });
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {// Cancel

                    alertDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    ///////////// Live Chat listner above

    private void showLogoutAlertForMultipleUsers(String title) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.log_out_popup_for_multiple_users, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal cancelBtn = dialogView.findViewById(R.id.btn_cancel);
            cancelBtn.setSelected(true);
            BakcellButtonNormal manageAccountsButton = dialogView.findViewById(R.id.manageAccountsButton);
            manageAccountsButton.setSelected(true);
            BakcellButtonNormal logoutBtn = dialogView.findViewById(R.id.btn_logout);
            logoutBtn.setSelected(true);
            BakcellTextViewNormal tv_pop_up_msg = dialogView.findViewById(R.id.tv_pop_up_msg);
            tv_pop_up_msg.setText(title);

            manageAccountsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    BaseActivity.startNewActivity(HomeActivity.this, ManageAccountsActivity.class);
                }
            });

            logoutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                    requestLogoutUser(HomeActivity.this);


                }
            });
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }//showLogoutAlertForMultipleUsers ends

    private void showLogoutAlert() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.diaog_logout_alert, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal cancelBtn = dialogView.findViewById(R.id.btn_cancel);
            BakcellButtonNormal logoutBtn = dialogView.findViewById(R.id.btn_logout);

            logoutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                    requestLogoutUser(HomeActivity.this);


                }
            });
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    private void updatePageTitle(String pageTitle) {
        if (page_title != null && Tools.hasValue(pageTitle)) {
            page_title.setText(pageTitle);
        }
    }

    public static void logoutUserOnAPISuccess(Activity activity) {

        if (activity != null && !activity.isFinishing()) {
        } else {
            return;
        }
        try {
            PrefUtils.addCustomerData(activity.getApplicationContext(), null);
            PrefUtils.addBoolean(activity.getApplicationContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false);
            PrefUtils.addBoolean(activity.getApplicationContext(), PrefUtils.PreKeywords.PREF_APP_RATER_SHOWN, false);
            ChangePasswordActivity.resetWrongPasswordAttemps(activity);

            DataManager.getInstance().setCurrentUser(null);
            DataManager.getInstance().setUserPredefinedData(null);
            DataManager.getInstance().setUserPrimaryOffering(null);
            DataManager.getInstance().setUserSupplementaryOfferingsList(null);

            EaseCacheManager.destroyCache(activity.getApplicationContext());

            RootValues.getInstance().setIS_USAGE_DETAIL_OTP_VERIFY(false);

            // Clear Cache for disclaimer dialog
            CacheConfig.removeCacheDisclaimerDialog(activity);

            startNewActivityAndClear(activity, LoginActivity.class);

            AppEventLogs.applyAppEvent(AppEventLogValues.LogoutDialogEvents.USER_LOGGED_OUT,
                    AppEventLogValues.LogoutDialogEvents.USER_LOGGED_OUT,
                    AppEventLogValues.LogoutDialogEvents.USER_LOGGED_OUT);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    private void processDynamicLinkDataForScreenNameTabNameAndOfferingID(String screenNameForRedirection, String tabNameForRedirection, String offeringIdForRedirection) {
        BakcellLogger.logE(Constants.DynamicLinksConstants.DYNAMIC_LINK_LOGGER_KEY,
                "screenNameForRedirection = " + screenNameForRedirection + ", tabNameForRedirection = " + tabNameForRedirection + ", offeringIdForRedirection = " + offeringIdForRedirection,
                fromClass, "processDynamicLinkDataForScreenNameAndTabName");
        if (screenNameForRedirection.equalsIgnoreCase(Constants.DynamicLinksScreenConstants.PACKAGES_SCREEN_CONSTANT)) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA, tabNameForRedirection);
            bundle.putString(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA, offeringIdForRedirection);
            BaseActivity.startNewActivity(HomeActivity.this, SupplementaryOffersActivity.class, bundle);
        }
    }

    private void processDynamicLinkDataForScreenNameAndTabName(String screenNameForRedirection, String tabNameForRedirection) {
        BakcellLogger.logE(Constants.DynamicLinksConstants.DYNAMIC_LINK_LOGGER_KEY,
                "screenNameForRedirection = " + screenNameForRedirection + ", tabNameForRedirection = " + tabNameForRedirection,
                fromClass, "processDynamicLinkDataForScreenNameAndTabName");
        if (screenNameForRedirection.equalsIgnoreCase(Constants.DynamicLinksScreenConstants.PACKAGES_SCREEN_CONSTANT)) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA, tabNameForRedirection);
            BaseActivity.startNewActivity(HomeActivity.this, SupplementaryOffersActivity.class, bundle);
        }
    }

    private void replaceNewFragment(Fragment newFragment, String fragmentId) {
        if (getSupportFragmentManager().findFragmentByTag(fragmentId) != null) {
            return;
        }

        if (newFragment == null) return;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newFragment, fragmentId);
        transaction.setReorderingAllowed(true);
        transaction.commitAllowingStateLoss();
    }

    public void actionBarMenuUpdateAccordingToScreen(String menuKey) {
        switch (menuKey) {
            case Constants.MenusKeyword.MENU_BOTTOM_SERVICES:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_ULDUZUM:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_ROAMING_COUNTRIES:
                toolbarWithOnlySearchIcon();
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_TARIFFS:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_ROAMING:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_MYACCOUTN:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_TUTORIALS_AND_FAQS:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_STORE_LOCATOR:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_FAQ:
                toolbarWithOnlySearchIcon();
                break;
            case Constants.MenusKeyword.MENU_LIVE_CHAT:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_CONTACT_US:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_SETTINGS:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_TERMS_AND_CONDITIONS:
                toolbarWithoutNotificationIcon();
                break;
            case Constants.MenusKeyword.MENU_NOTIFICATIONS:
                toolbarWithOnlySearchIcon();
                break;
            default:
                notification_menu.setVisibility(View.VISIBLE);
                offer_list_change_layout.setVisibility(View.GONE);
                setting_layout.setVisibility(View.GONE);
                search_menu.setVisibility(View.GONE);
                break;
        }

    }

    private void toolbarWithoutNotificationIcon() {
        notification_menu.setVisibility(View.GONE);
        offer_list_change_layout.setVisibility(View.GONE);
        setting_layout.setVisibility(View.GONE);
        search_menu.setVisibility(View.GONE);
    }

    private void toolbarWithOnlySearchIcon() {
        notification_menu.setVisibility(View.GONE);
        offer_list_change_layout.setVisibility(View.GONE);
        setting_layout.setVisibility(View.GONE);
        search_menu.setVisibility(View.VISIBLE);
    }

    public void updateUIFooterMenuSeleted(String menuKey, int selectedMenuIcon) {
        String menuOrder = getHorizontalMenuOrderFromMneuKey(menuKey);
        switch (menuOrder) {
            case "1":
                binding.idViewFooter.footerMenuText1.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.idViewFooter.footerMenuText2.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText3.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText4.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText5.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.icFooterMenu1.setImageDrawable(ContextCompat.getDrawable(this, selectedMenuIcon));
                binding.idViewFooter.icFooterMenu2.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("2"))));
                binding.idViewFooter.icFooterMenu3.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("3"))));
                binding.idViewFooter.icFooterMenu4.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("4"))));
                binding.idViewFooter.icFooterMenu5.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("5"))));
                break;
            case "2":
                binding.idViewFooter.footerMenuText1.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText2.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.idViewFooter.footerMenuText3.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText4.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText5.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.icFooterMenu1.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("1"))));
                binding.idViewFooter.icFooterMenu2.setImageDrawable(ContextCompat.getDrawable(this, selectedMenuIcon));
                binding.idViewFooter.icFooterMenu3.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("3"))));
                binding.idViewFooter.icFooterMenu4.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("4"))));
                binding.idViewFooter.icFooterMenu5.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("5"))));
                break;
            case "3":
                binding.idViewFooter.footerMenuText1.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText2.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText3.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.idViewFooter.footerMenuText4.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText5.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.icFooterMenu1.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("1"))));
                binding.idViewFooter.icFooterMenu2.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("2"))));
                binding.idViewFooter.icFooterMenu3.setImageDrawable(ContextCompat.getDrawable(this, selectedMenuIcon));
                binding.idViewFooter.icFooterMenu4.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("4"))));
                binding.idViewFooter.icFooterMenu5.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("5"))));
                break;
            case "4":
                binding.idViewFooter.footerMenuText1.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText2.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText3.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText4.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.idViewFooter.footerMenuText5.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.icFooterMenu1.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("1"))));
                binding.idViewFooter.icFooterMenu2.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("2"))));
                binding.idViewFooter.icFooterMenu3.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("3"))));
                binding.idViewFooter.icFooterMenu4.setImageDrawable(ContextCompat.getDrawable(this, selectedMenuIcon));
                binding.idViewFooter.icFooterMenu5.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("5"))));
                break;
            case "5":
                binding.idViewFooter.footerMenuText1.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText2.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText3.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText4.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText5.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.idViewFooter.icFooterMenu1.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("1"))));
                binding.idViewFooter.icFooterMenu2.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("2"))));
                binding.idViewFooter.icFooterMenu3.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("3"))));
                binding.idViewFooter.icFooterMenu4.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("4"))));
                binding.idViewFooter.icFooterMenu5.setImageDrawable(ContextCompat.getDrawable(this, selectedMenuIcon));
                break;
            default:
                binding.idViewFooter.footerMenuText1.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText2.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText3.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText4.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.footerMenuText5.setTextColor(getResources().getColor(R.color.text_gray));
                binding.idViewFooter.icFooterMenu1.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("1"))));
                binding.idViewFooter.icFooterMenu2.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("2"))));
                binding.idViewFooter.icFooterMenu3.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("3"))));
                binding.idViewFooter.icFooterMenu4.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("4"))));
                binding.idViewFooter.icFooterMenu5.setImageDrawable(ContextCompat.getDrawable(this, getNormalFooterIconFromMenuKey(getHorizontalMenuKeyByMenuOrder("5"))));
                break;
        }


    }

    /**
     * getString Vertical Menu Item ID from Menu Keyword
     *
     * @param menuKey
     * @return
     */
    public int getVeriticalMenuIdFromMenuKey(String menuKey) {
        int menuId = -1;
        if (RootValues.getInstance().getMenuVerticalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuVerticalItems().size(); i++) {
                if (menuKey.equalsIgnoreCase(RootValues.getInstance().getMenuVerticalItems().get(i).getIdentifier())) {
                    menuId = RootValues.getInstance().getMenuVerticalItems().get(i).getMenuItemId();
                    break;
                }
            }
        }
        return menuId;
    }

    /**
     * getString Vertical Menus Keyword from ID
     *
     * @param menuId
     * @return
     */
    public String getVeriticalMenuKeyFromMenuID(int menuId) {
        String menuKey = "";
        if (RootValues.getInstance().getMenuVerticalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuVerticalItems().size(); i++) {
                if (menuId == RootValues.getInstance().getMenuVerticalItems().get(i).getMenuItemId()) {
                    menuKey = RootValues.getInstance().getMenuVerticalItems().get(i).getIdentifier();
                    break;
                }
            }
        }
        return menuKey;
    }

    /**
     * getString Horizontal Menu Index ID from Menu Keyword
     *
     * @param menuKey
     * @return
     */
    public int getHorizontalMenuIdFromMenuKey(String menuKey) {
        int menuId = -1;
        if (RootValues.getInstance().getMenuHorizontalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuHorizontalItems().size(); i++) {
                if (menuKey.equalsIgnoreCase(RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier())) {
                    menuId = RootValues.getInstance().getMenuHorizontalItems().get(i).getMenuItemId();
                    break;
                }
            }
        }
        return menuId;
    }

    /**
     * getString Horizontal Menus Keyword from ID
     *
     * @param menuId
     * @return
     */
    public String getHorizontalMenuKeyFromMenuID(int menuId) {
        String menuKey = "";
        if (RootValues.getInstance().getMenuHorizontalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuHorizontalItems().size(); i++) {
                if (Tools.hasValue(RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier())
                        && menuId == RootValues.getInstance().getMenuHorizontalItems().get(i).getMenuItemId()) {
                    menuKey = RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier();
                    break;
                }
            }
        }
        return menuKey;
    }

    /**
     * Get Sort Order from Horizontal menus by Menu Id
     *
     * @param menuId
     * @return
     */
    public String getHorizontalMenuOrderFromId(int menuId) {
        String menuOrder = "";
        if (RootValues.getInstance().getMenuHorizontalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuHorizontalItems().size(); i++) {
                if (menuId == RootValues.getInstance().getMenuHorizontalItems().get(i).getMenuItemId()) {
                    menuOrder = RootValues.getInstance().getMenuHorizontalItems().get(i).getSortOrder();
                    break;
                }
            }
        }
        return menuOrder;
    }

    /**
     * Get Sort Order from Horizontal menus by Menu Key
     *
     * @param menuKey
     * @return
     */
    public String getHorizontalMenuOrderFromMneuKey(String menuKey) {
        String menuOrder = "";
        if (RootValues.getInstance().getMenuHorizontalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuHorizontalItems().size(); i++) {
                if (Tools.hasValue(RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier())
                        && menuKey.equalsIgnoreCase(RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier())) {
                    menuOrder = RootValues.getInstance().getMenuHorizontalItems().get(i).getSortOrder();
                    break;
                }
            }
        }
        return menuOrder;
    }

    /**
     * getString Horizontal Menus Keyword from Order
     *
     * @param menuOrder
     * @return
     */
    public String getHorizontalMenuKeyByMenuOrder(String menuOrder) {
        String menuKey = "";
        if (RootValues.getInstance().getMenuHorizontalItems() != null) {
            for (int i = 0; i < RootValues.getInstance().getMenuHorizontalItems().size(); i++) {
                if (Tools.hasValue(RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier())
                        && menuOrder.equalsIgnoreCase(RootValues.getInstance().getMenuHorizontalItems().get(i).getSortOrder())) {
                    menuKey = RootValues.getInstance().getMenuHorizontalItems().get(i).getIdentifier();
                    break;
                }
            }
        }
        return menuKey;
    }

    public int getNormalFooterIconFromMenuKey(String menuKey) {
        int menuNormalIcon = R.drawable.bottom_menu_dashboard_deseleted;
        switch (menuKey) {
            case Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD:
                menuNormalIcon = R.drawable.bottom_menu_dashboard_deseleted;
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_TARIFFS:
                menuNormalIcon = R.drawable.bottom_menu_tariffs_deseleted;
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_SERVICES:
                menuNormalIcon = R.drawable.bottom_menu_supplementory_offer_deseleted;
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_ROAMING:
                menuNormalIcon = R.drawable.bottom_menu_roaming_deselected;
                break;
            case Constants.MenusKeyword.MENU_BOTTOM_MYACCOUTN:
                menuNormalIcon = R.drawable.bottom_menu_account_deseleted;
                break;
        }
        return menuNormalIcon;
    }


    public void requestLogoutUser(final Context context) {
        if (context == null || DataManager.getInstance() == null) return;

        RequestLogoutService.newInstance(context, ServiceIDs.REQUEST_LOGOUT_USER).execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                String result = response.getData();

                try {
                    if (context != null && !((Activity) context).isFinishing()) {
                        logoutUserOnAPISuccess(((Activity) context));
                        updateUIFooterMenuSeleted("", -1);
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }

            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                try {
                    if (context != null && !((Activity) context).isFinishing() && response != null) {
                        BakcellPopUpDialog.showMessageDialog(context,
                                getString(R.string.bakcell_error_title), response.getDescription());
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                try {
                    if (context != null && !((Activity) context).isFinishing()) {
                        BakcellPopUpDialog.ApiFailureMessage(context);
                    }
                } catch (Exception e1) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e1.getMessage());
                }
            }

        });

    }


    OnProfileImageUpdate onProfileImageUpdate = new OnProfileImageUpdate() {
        @Override
        public void onProfileImageUpdate(Bitmap bitmap) {
            if (bitmap != null) {
                profileImage.setImageBitmap(bitmap);
            } else {
                profileImage.setImageDrawable(ContextCompat.getDrawable(getApplicationContext()
                        , R.drawable.dummy_profile));
            }
        }
    };


}