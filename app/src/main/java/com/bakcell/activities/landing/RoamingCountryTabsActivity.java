package com.bakcell.activities.landing;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.databinding.ActivityRoamingCountryBinding;
import com.bakcell.fragments.menus.RoamingCountriesFragment;
import com.bakcell.fragments.pager.RoamingNewFragment;
import com.bakcell.fragments.pager.RoamingNewFragmentBundle;
import com.bakcell.models.DataManager;
import com.bakcell.models.roamingnewmodels.RoamingCountriesDetailsHelperModel;
import com.bakcell.models.supplementaryoffers.RoamingsOffer;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class RoamingCountryTabsActivity extends BaseActivity implements View.OnClickListener {

    private ActivityRoamingCountryBinding binding;
    private ArrayList<RoamingCountriesDetailsHelperModel> roamingCountriesDetailsHelperModel = new ArrayList<>();
    private String screenName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_roaming_country);

        //get the data from the intent
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            roamingCountriesDetailsHelperModel = bundle.getParcelableArrayList(RoamingCountriesFragment.ROAMING_COUNTRIES_DETAILS_DATA_KEY);
            screenName = bundle.getString(RoamingCountriesFragment.ROAMING_COUNTRIES_SELECTED_KEY);
        }

        initUiContent();
        initUiListener();
        initTabs(roamingCountriesDetailsHelperModel);
        initViewPager(roamingCountriesDetailsHelperModel);

        binding.flagImage.setImageDrawable(Tools.getImageFromAssests(this, getCountryFlagName()));

    }//onCreate ends

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.GONE);
        binding.toolbar.pageTitle.setText(screenName);
        binding.tvCountryName.setText(screenName);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
    }//initUiContent ends

    private void initUiListener() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
    }//initUiListener ends

    private void initTabs(ArrayList<RoamingCountriesDetailsHelperModel> model) {
        //create th tabs here
        //adding tabs
        for (int i = 0; i < model.size(); i++) {
            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(model.get(i).operator));
        }
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.roaming_tab_bundle_title));

        //set margins to the tab layout
        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (10 * getResources().getDisplayMetrics().density);
            p.setMargins(margin, 0, margin, 0);
            tab.requestLayout();
        }//for ends

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
        binding.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            // optional
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == binding.tabLayout.getTabCount() - 1) {
                    if (DataManager.getInstance().getSwipeDisableCheckForOperators())
                        binding.viewPager.disableScroll(true);
                } else {
                    binding.viewPager.disableScroll(false);

                }
            }

            // optional
            @Override
            public void onPageSelected(int position) {
                if (position == binding.tabLayout.getTabCount() - 1) {
                    if (DataManager.getInstance().getSwipeDisableCheckForOperators())
                        binding.viewPager.disableScroll(true);
                } else {
                    binding.viewPager.disableScroll(false);

                }

            }

            // optional
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }//onTabSelected ends

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }//onTabUnselected ends

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }//onTabReselected ends
        });
    }//initTabs ends

    private void initViewPager(ArrayList<RoamingCountriesDetailsHelperModel> model) {
        final ArrayList<Fragment> fragmentsList = new ArrayList<>();
        //adding fragments for data display
        for (int i = 0; i < model.size(); i++) {
            fragmentsList.add(RoamingNewFragment.getInstance(model.get(i).serviceListHelperModel));
        }//for ends

        fragmentsList.add(RoamingNewFragmentBundle.getInstance(screenName, ""));
        binding.viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    binding.viewPager.setAdapter(new PagerAdapterOffers(getSupportFragmentManager(), fragmentsList));
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        }, 300);
    }//initViewPager ends

    private String getCountryFlagName() {
        String flag = "";
        RoamingsOffer roamingsOffer = null;
        if (DataManager.getInstance().getSupplementaryOfferMainData() != null) {
            roamingsOffer = DataManager.getInstance().getSupplementaryOfferMainData().getRoaming();
        }

        if (roamingsOffer != null && roamingsOffer.getOffers() != null) {
            if (roamingsOffer.getCountries() != null && roamingsOffer.getOffers().size() > 0) {
                for (int i = 0; i < roamingsOffer.getCountries().size(); i++) {
                    if (screenName.equalsIgnoreCase(roamingsOffer.getCountries().get(i).getName())) {
                        flag = roamingsOffer.getCountries().get(i).getFlag();
                        break;
                    }
                }

            }
        }
        return flag;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
        }//switch ends
    }//onClick ends
}//class ends
