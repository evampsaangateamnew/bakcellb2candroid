package com.bakcell.activities.landing;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.adapters.SpinnerAdapterUlduzumCategories;
import com.bakcell.databinding.ActivityUlduzumLocationsBinding;
import com.bakcell.fragments.pager.UlduzumLocationsListPagerFragment;
import com.bakcell.fragments.pager.UlduzumLocationsPagerFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.ulduzum.CategoryList;
import com.bakcell.models.ulduzum.UlduzumCategoriesSpinnerModel;
import com.bakcell.models.ulduzum.UlduzumLocationsServiceModel;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestUlduzumLocationsWebService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class UlduzumLocationsActivity extends BaseActivity implements View.OnClickListener {

    private ActivityUlduzumLocationsBinding binding;
    private SpinnerAdapterUlduzumCategories spinnerAdapterUlduzumCategories;
    private UlduzumLocationsServiceModel ulduzumLocationsServiceModel = null;
    public static final String ULDUZUM_LOCATIONS_BUNDLE_KEY = "ulduzum.bundle.locations.key";
    boolean flagLocation = false;
    private ArrayList<UlduzumCategoriesSpinnerModel> ulduzumCategoriesSpinnerModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ulduzum_locations);

        initUiContent();
        initUIListener();
        initUITabsAndPager();
    }//onCreate ends

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
//        binding.toolbar.pageTitle.setText(getResources().getString(R.string.ulduzum_locations_title_label));
        binding.toolbar.pageTitle.setText(getResources().getString(R.string.ulduzum_location_lbl_details));
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
    }//initUiContent ends

    private void initUIListener() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.spinnerPeriodButton.setOnClickListener(this);
    }//initUIListener ends

    private void initUITabsAndPager() {
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getResources().getString(R.string.ulduzum_location_lbl_details)));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getResources().getString(R.string.ulduzum_locations_list_tab_label)));

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (10 * getResources().getDisplayMetrics().density);
            p.setMargins(margin, 0, margin, 0);
            tab.requestLayout();
        }

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }//onTabSelected ends

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }//onTabUnselected ends

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }//onTabReselected ends
        });

        requestStoreLocator();
    }//initUITabsAndPager ends

    public void requestStoreLocator() {
        RequestUlduzumLocationsWebService.newInstance(UlduzumLocationsActivity.this, ServiceIDs.ULDUZUM_GET_LOCATIONS_ID)
                .execute(DataManager.getInstance().getCurrentUser(), "",
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                String result = response.getData();
                                if (result == null) return;
                                ulduzumLocationsServiceModel = new Gson().fromJson(result, UlduzumLocationsServiceModel.class);
                                initPagerView(ulduzumLocationsServiceModel);
                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (!UlduzumLocationsActivity.this.isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(UlduzumLocationsActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(UlduzumLocationsActivity.this);
                                    return;
                                }
                                //removed request cache feature as directed by SA : Date 31/12/2020
                                /*
                                // Load Cashed Data
                                if (!UlduzumLocationsActivity.this.isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getUlduzumLocatorCacheKey(UlduzumLocationsActivity.this);
                                    String cacheResponse = EaseCacheManager.get(UlduzumLocationsActivity.this, cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            ulduzumLocationsServiceModel = new Gson().fromJson(cacheData, UlduzumLocationsServiceModel.class);
                                            initPagerView(ulduzumLocationsServiceModel);
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                        }
                                    }
                                }

*/
                                if (!UlduzumLocationsActivity.this.isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(UlduzumLocationsActivity.this,
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                    initPagerView(ulduzumLocationsServiceModel);
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (!UlduzumLocationsActivity.this.isFinishing()) {
                                    //removed request cache feature as directed by SA : Date 31/12/2020
                                    /*
                                    // Load Cashed Data
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getUlduzumLocatorCacheKey(UlduzumLocationsActivity.this);
                                    String cacheResponse = EaseCacheManager.get(UlduzumLocationsActivity.this, cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            ulduzumLocationsServiceModel = new Gson().fromJson(cacheData, UlduzumLocationsServiceModel.class);
                                            initPagerView(ulduzumLocationsServiceModel);
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                        }
                                    } else {
                                        initPagerView(ulduzumLocationsServiceModel);
                                        BakcellPopUpDialog.ApiFailureMessage(UlduzumLocationsActivity.this);
                                    }

*/
                                    BakcellPopUpDialog.ApiFailureMessage(UlduzumLocationsActivity.this);
                                }
                            }

                        });

    }//requestStoreLocator ends

    private void initPagerView(final UlduzumLocationsServiceModel serviceModel) {
        final ArrayList<Fragment> fragmentsList = new ArrayList<>();
        fragmentsList.add(UlduzumLocationsPagerFragment.getInstance(serviceModel));
        fragmentsList.add(UlduzumLocationsListPagerFragment.getInstance(serviceModel));

        binding.viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    binding.viewPager.setAdapter(new PagerAdapterOffers(getSupportFragmentManager(), fragmentsList));
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        }, 300);

        if (serviceModel != null) {
            if (serviceModel.getCategoryList() != null) {
                if (serviceModel.getCategoryList().size() > 0) {
                    ulduzumCategoriesSpinnerModel = getSpinnerModelList(serviceModel.getCategoryList());
                    if (ulduzumCategoriesSpinnerModel != null) {
                        if (ulduzumCategoriesSpinnerModel.size() > 0) {
                            spinnerAdapterUlduzumCategories = new SpinnerAdapterUlduzumCategories(UlduzumLocationsActivity.this, ulduzumCategoriesSpinnerModel);
                            binding.spinnerCategories.setAdapter(spinnerAdapterUlduzumCategories);
                            binding.spinnerHolder.setVisibility(View.VISIBLE);
                            binding.spinnerCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    if (flagLocation) {
                                        String categoryId = "";
                                        //get the id from the position
                                        if (ulduzumCategoriesSpinnerModel.get(i).categoryId.equalsIgnoreCase(Constants.UlduzumConstants.ULDUZUM_CATEGORIES_FIRST_ITEM)) {
                                            categoryId = Constants.UlduzumConstants.ULDUZUM_CATEGORIES_FIRST_ITEM;//user select the all categories
                                        } else {
                                            categoryId = ulduzumCategoriesSpinnerModel.get(i).categoryId;//user select other than the all categories
                                        }//if ends

                                        //apply filters
                                        RootValues.getInstance().getUlduzumMapLocationRedirectionListener().onMapLocationSelected(categoryId, serviceModel);
                                        RootValues.getInstance().getUlduzumLocationsListFilterListener().onListLocationSelected(categoryId, serviceModel);//saving the filter listener object
                                    } else {
                                        flagLocation = true;
                                    }//if (flagLocation) ends
                                }//onItemSelected ends

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {
                                    //do nothing
                                }//onNothingSelected ends
                            });//binding.spinnerCategories.setOnItemSelectedListener ends
                        } else {
                            binding.spinnerHolder.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        binding.spinnerHolder.setVisibility(View.INVISIBLE);
                    }
                } else {
                    binding.spinnerHolder.setVisibility(View.INVISIBLE);
                }//if (serviceModel.getCategoryNameList().size() > 0) ends
            } else {
                binding.spinnerHolder.setVisibility(View.INVISIBLE);
            }// if (serviceModel.getCategoryNameList() != null) ends
        } else {
            binding.spinnerHolder.setVisibility(View.INVISIBLE);
        }//if (serviceModel != null) ends

    }//initPagerView ends

    private ArrayList<UlduzumCategoriesSpinnerModel> getSpinnerModelList(ArrayList<CategoryList> categoryList) {
        ArrayList<UlduzumCategoriesSpinnerModel> result = new ArrayList<>();
        result.add(new UlduzumCategoriesSpinnerModel(getResources().getString(R.string.lbl_all), Constants.UlduzumConstants.ULDUZUM_CATEGORIES_FIRST_ITEM));
        for (int i = 0; i < categoryList.size(); i++) {
            result.add(new UlduzumCategoriesSpinnerModel(categoryList.get(i).getName(), categoryList.get(i).getId()));
        }//for ends

        return result;
    }//getSpinnerModelList ends

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.spinnerPeriodButton:
                binding.spinnerCategories.performClick();
                break;
        }//switch ends
    }//onClick ends
}//class ends
