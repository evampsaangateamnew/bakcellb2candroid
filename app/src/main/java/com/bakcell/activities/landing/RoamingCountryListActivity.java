package com.bakcell.activities.landing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.RoamingCountriesAdapter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityRoamingCountryListBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.roamingnewmodels.RoamingCountriesHelperModel;
import com.bakcell.models.roamingnewmodels.RoamingModelNew;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RoamingCountriesWebService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class RoamingCountryListActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityRoamingCountryListBinding binding;

    //ROamingCountriesFragment
    private RoamingCountriesAdapter roamingCountriesAdapter = null;
    private RoamingModelNew roamingModelNew = null;
    private ArrayList<RoamingCountriesHelperModel> roamingCountriesHelperModel = null;
    public static final String ROAMING_COUNTRIES_DETAILS_DATA_KEY = "roaming_country_details_key";
    public static final String ROAMING_COUNTRIES_SELECTED_KEY = "roaming_country_selected_key";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_roaming_country_list);
        initUiContent();

        initUiListeners();
        requestRoamingCountriesData();
        AppEventLogs.applyAppEvent(AppEventLogValues.RoamingScreen.ROAMING_PRICE_OPERATORS_SCREEN,
                AppEventLogValues.RoamingScreen.ROAMING_PRICE_OPERATORS_SCREEN,
                AppEventLogValues.RoamingScreen.ROAMING_PRICE_OPERATORS_SCREEN);

    }

    private void initUiListeners() {

    }

    private void initUiContent() {


        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(getString(R.string.prices_and_operators));

        if(RoamingCountryListActivity.this!=null){
            if (AppClass.getCurrentLanguageKey(RoamingCountryListActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
                binding.toolbar.pageTitle.setText("Prices and operators");
            } else if (AppClass.getCurrentLanguageKey(RoamingCountryListActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                binding.toolbar.pageTitle.setText("Qiymətlər və operatorlar");
            } else if (AppClass.getCurrentLanguageKey(RoamingCountryListActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
                binding.toolbar.pageTitle.setText("Цены и операторы");
            }
        }


        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });


        binding.toolbar.offerListChangeLayout.setVisibility(View.GONE);
        binding.toolbar.settingLayout.setVisibility(View.GONE);
        binding.toolbar.searchMenu.setVisibility(View.VISIBLE);
        binding.toolbar.searchMenu.setOnClickListener(this);
        binding.toolbar.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
// Do nothing because of X and Y.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onSearch(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
// Do nothing because of X and Y.
            }
        });


    }

    public void onSearch(String keyword) {
        if (roamingCountriesAdapter != null) {
            roamingCountriesAdapter.getFilter().filter(keyword);
        }
    }

    public void onSearchClosed() {
        if (roamingModelNew != null) {
            roamingCountriesAdapter.refreshList(getModelList(roamingModelNew));
        }
    }

    private void initUIListeners() {

    }//initUIListeners ends

    public void requestRoamingCountriesData() {
        RoamingCountriesWebService.newInstance(RoamingCountryListActivity.this, ServiceIDs.ROAMING_COUNTRIES_DATA_ID)
                .execute(DataManager.getInstance().getCurrentUser(),
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                String result = response.getData();
                                if (result == null) return;
                                try {
                                    roamingModelNew = new Gson().fromJson(result, RoamingModelNew.class);
                                } catch (JsonSyntaxException e) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                                }
                                if (roamingModelNew != null) {
                                    if (roamingModelNew.getCountryList() != null) {
                                        if (roamingModelNew.getCountryList().size() > 0) {
                                            //initiate the ui
                                            initiateUI(roamingModelNew);
                                        } else {
                                            binding.roamingCountriesList.setVisibility(View.GONE);
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        binding.roamingCountriesList.setVisibility(View.GONE);
                                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    binding.roamingCountriesList.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                }

                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (RoamingCountryListActivity.this != null && !RoamingCountryListActivity.this.isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(RoamingCountryListActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(RoamingCountryListActivity.this);
                                    return;
                                }

                                // Load Cashed Data
                                if (RoamingCountryListActivity.this != null && !RoamingCountryListActivity.this.isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getRoamingCountriesCacheKey(RoamingCountryListActivity.this);
                                    String cacheResponse = EaseCacheManager.get(RoamingCountryListActivity.this, cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            roamingModelNew = new Gson().fromJson(cacheData, RoamingModelNew.class);
                                            if (roamingModelNew != null) {
                                                if (roamingModelNew.getCountryList() != null) {
                                                    if (roamingModelNew.getCountryList().size() > 0) {
                                                        //initiate the ui
                                                        initiateUI(roamingModelNew);
                                                    } else {
                                                        binding.roamingCountriesList.setVisibility(View.GONE);
                                                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                    }
                                                } else {
                                                    binding.roamingCountriesList.setVisibility(View.GONE);
                                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                }
                                            } else {
                                                binding.roamingCountriesList.setVisibility(View.GONE);
                                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                            }
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                            binding.roamingCountriesList.setVisibility(View.GONE);
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }

                                if (RoamingCountryListActivity.this != null && !RoamingCountryListActivity.this.isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(RoamingCountryListActivity.this,
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (RoamingCountryListActivity.this != null && !RoamingCountryListActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(RoamingCountryListActivity.this);
                                }

                                // Load Cashed Data
                                if (RoamingCountryListActivity.this != null && !RoamingCountryListActivity.this.isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getRoamingCountriesCacheKey(RoamingCountryListActivity.this);
                                    String cacheResponse = EaseCacheManager.get(RoamingCountryListActivity.this, cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            roamingModelNew = new Gson().fromJson(cacheData, RoamingModelNew.class);
                                            if (roamingModelNew != null) {
                                                if (roamingModelNew.getCountryList() != null) {
                                                    if (roamingModelNew.getCountryList().size() > 0) {
                                                        //initiate the ui
                                                        initiateUI(roamingModelNew);
                                                    } else {
                                                        binding.roamingCountriesList.setVisibility(View.GONE);
                                                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                    }
                                                } else {
                                                    binding.roamingCountriesList.setVisibility(View.GONE);
                                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                }
                                            } else {
                                                binding.roamingCountriesList.setVisibility(View.GONE);
                                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                            }
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                            binding.roamingCountriesList.setVisibility(View.GONE);
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            }

                        });

    }//requestRoamingCountriesData ends

    private void initiateUI(RoamingModelNew roamingModel) {
        if (RoamingCountryListActivity.this != null && !RoamingCountryListActivity.this.isFinishing()) {
            roamingCountriesHelperModel = getModelList(roamingModel);
            if (roamingCountriesHelperModel != null) {
                if (roamingCountriesHelperModel.size() > 0) {
                    roamingCountriesAdapter = new RoamingCountriesAdapter(getApplicationContext(), RoamingCountryListActivity.this, roamingCountriesHelperModel, roamingModel);
                    binding.roamingCountriesList.setAdapter(roamingCountriesAdapter);
                    binding.roamingCountriesList.setVisibility(View.VISIBLE);
                    binding.noDataFoundLayout.setVisibility(View.GONE);
                } else {
                    binding.roamingCountriesList.setVisibility(View.GONE);
                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                }
            } else {
                binding.roamingCountriesList.setVisibility(View.GONE);
                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            }
        }
    }//initiateUI ends

    private ArrayList<RoamingCountriesHelperModel> getModelList(RoamingModelNew countries) {
        ArrayList<RoamingCountriesHelperModel> model = new ArrayList<>();

        for (int i = 0; i < countries.getCountryList().size(); i++) {
            if (Tools.hasValue(countries.getCountryList().get(i).getCountry()) &&
                    Tools.hasValue(countries.getCountryList().get(i).getCountry_id())) {
                model.add(new RoamingCountriesHelperModel(countries.getCountryList().get(i).getCountry(),
                        countries.getCountryList().get(i).getCountry_id()));
            }
        }//for ends

        /*Collections.sort(model, new Comparator<RoamingCountriesHelperModel>() {
            @Override
            public int compare(RoamingCountriesHelperModel o1, RoamingCountriesHelperModel o2) {
                return o1.country.compareTo(o2.country);
            }
        });*/
        return model;
    }//getModelList ends

    boolean isSearchOpened = false;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_menu:
                if (isSearchOpened) {
                    hideOrShowSearchView(View.VISIBLE, View.GONE);
                    binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.actionbar_search));
                    isSearchOpened = false;
                    onSearchClosed();
                } else {
                    clearSearchView();
                    hideOrShowSearchView(View.GONE, View.VISIBLE);
                    binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.ic_cross));
                    isSearchOpened = true;
                }
                break;
            default:
                hideOrShowSearchView(View.VISIBLE, View.GONE);

        }
    }

    private void hideOrShowSearchView(int pageTitle, int searchView) {
        binding.toolbar.pageTitle.setVisibility(pageTitle);
        binding.toolbar.searchView.setVisibility(searchView);
        if (binding.toolbar.searchView.getVisibility() == View.VISIBLE) {
            binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.ic_cross));
            binding.toolbar.etSearch.requestFocus();
            Tools.showKeyboard(this);
            isSearchOpened = true;
        } else {
            binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.actionbar_search));
            Tools.hideKeyboard(this);
            isSearchOpened = false;
        }
    }

    public void clearSearchView() {
        if (binding.toolbar.etSearch.getVisibility() == View.VISIBLE) {
            binding.toolbar.etSearch.setText("");
        }
    }

}