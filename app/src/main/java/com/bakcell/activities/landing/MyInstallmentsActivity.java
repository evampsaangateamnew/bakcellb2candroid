package com.bakcell.activities.landing;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.adapters.CardAdapterMyInstallmentsItem;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityMyInstallmentsBinding;
import com.bakcell.models.dashboard.installments.Installments;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;

/**
 * Created by Freeware Sys on 18-May-17.
 */

public class MyInstallmentsActivity extends BaseActivity implements View.OnClickListener {

    public static final String KEY_ISNTALLMENTS_TITLE = "key.installments.titles";
    public static final String KEY_ISNTALLMENTS_LIST = "key.installments.list";

    ActivityMyInstallmentsBinding binding;

    private CardAdapterMyInstallmentsItem adapterInstallmentsItem;
    LinearLayoutManager layoutManager;

    ArrayList<Installments> installmentsList;
    String titleDescription = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_installments);

        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(KEY_ISNTALLMENTS_TITLE)) {
                titleDescription = getIntent().getExtras().getString(KEY_ISNTALLMENTS_TITLE);
            }
            if (getIntent().getExtras().containsKey(KEY_ISNTALLMENTS_LIST)) {
                installmentsList = getIntent().getExtras().getParcelableArrayList(KEY_ISNTALLMENTS_LIST);
            }
        }

        initUIContents();

        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.MY_INSTALLMENT_SCREEN,
                AppEventLogValues.AccountEvents.MY_INSTALLMENT_SCREEN,
                AppEventLogValues.AccountEvents.MY_INSTALLMENT_SCREEN);
    }

    private void initUIContents() {

        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setText(R.string.title_my_installments);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));

        if (Tools.hasValue(titleDescription)) {
            binding.titleTxt.setText(titleDescription);
        } else {
            binding.titleTxt.setText("");
        }

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.mRecylerview.setLayoutManager(layoutManager);
        binding.mRecylerview.setHasFixedSize(true);
        binding.mRecylerview.setNestedScrollingEnabled(false);

        if (installmentsList != null && installmentsList.size() > 0) {
            binding.nestedScrollMainLayout.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            adapterInstallmentsItem = new CardAdapterMyInstallmentsItem(this, installmentsList);
            binding.mRecylerview.setAdapter(adapterInstallmentsItem);
        } else {
            binding.nestedScrollMainLayout.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
        }
    }


}
