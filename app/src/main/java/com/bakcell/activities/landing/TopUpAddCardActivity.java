package com.bakcell.activities.landing;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.SpinnerAdapterTopup;
import com.bakcell.databinding.ActivityTopUpAddCardBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.interfaces.OnOkClickListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.topup.InitiatePaymentData;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.models.user.userpredefinedobjects.CardType;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestInitiatePaymentService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class TopUpAddCardActivity extends BaseActivity {
    ArrayList<String> arrayList;
    ActivityTopUpAddCardBinding binding;
    private long selectedCardKey = 0;
    String topUpNumber="";
    String amount="";
    private boolean isSaveCard = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_top_up_add_card);
        initData();

        initUiContent();

        initUiListeners();

    }

    private void initData() {

        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.TopUpConstants.MOBILE_NUMBER)
                && getIntent().getExtras().containsKey(Constants.TopUpConstants.AMOUNT)) {
            topUpNumber = getIntent().getExtras().getString(Constants.TopUpConstants.MOBILE_NUMBER);
            amount = getIntent().getExtras().getString(Constants.TopUpConstants.AMOUNT);
        }
    }

    private void initUiListeners() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(getString(R.string.lbl_top_up_card_type));

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        binding.cbSaveCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isSaveCard = b;
            }
        });
        binding.btnContinue.setOnClickListener(view -> {
            if(selectedCardKey > 0 && isValidAmount(amount)& isValidMobileNumber(topUpNumber)) {
                String dialogMessage = getString(R.string.payment_loading) + amount + getString(R.string.azn_to) + topUpNumber;
                BakcellPopUpDialog.showConfirmationDialog(TopUpAddCardActivity.this, getString(R.string.dialog_confirmation_lbl), dialogMessage, new OnOkClickListener() {
                    @Override
                    public void onOkClick() {
                        requestInitiatePayment(topUpNumber, amount, selectedCardKey, isSaveCard);
                    }
                });
            }

        });

    }

    private void initUiContent() {
        setupVisaDropDown();
    }

    //Setup Visa Dropdown spinner to select card type
    private void setupVisaDropDown() {
        UserMain userMain = PrefUtils.getCustomerData(TopUpAddCardActivity.this);

        if (userMain != null && userMain.getPredefinedData() != null && userMain.getPredefinedData().getTopup() != null &&
                userMain.getPredefinedData().getTopup().getPlasticCard() != null &&
                userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes() != null &&
                userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes().size() > 0) {
            List<CardType> cardTypes = userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes();

            if (cardTypes != null && cardTypes.size() > 0) {
                SpinnerAdapterTopup spinnerAdapterTopUp = new SpinnerAdapterTopup(cardTypes, this);
                binding.spCardType.setAdapter(spinnerAdapterTopUp);
                selectedCardKey = cardTypes.get(0).getKey();
                binding.spCardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        spinnerAdapterTopUp.setLastSelectedPos(position);
                        spinnerAdapterTopUp.notifyDataSetChanged();
                        spinnerAdapterTopUp.setOpened(false);

                        if (cardTypes != null && cardTypes.size() > 0) {
                            selectedCardKey = cardTypes.get(position).getKey();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

        }


    } // setupVisaDropDown


    private void requestInitiatePayment(String mobileNumber, String amount, long selectedCardKey, boolean isSaveCard) {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestInitiatePaymentService.newInstance(TopUpAddCardActivity.this, ServiceIDs.REQUEST_GET_INITIATE_PAYMENT)
                    .execute(userModel, mobileNumber, amount, selectedCardKey, isSaveCard, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String responseDescription="";
                            if(!TopUpAddCardActivity.this.isFinishing() && response != null&&
                                    Tools.hasValue(response.getDescription())){
                                responseDescription=response.getDescription();
                            }

                            if (!TopUpAddCardActivity.this.isFinishing() && response != null) {
                                String data = response.getData();
                                Gson gson = new Gson();
                                InitiatePaymentData initiatePaymentData = gson.fromJson(data, InitiatePaymentData.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constants.TOP_UP_INIT_PAYMENT_URL, initiatePaymentData.getPaymentKey());

                                startNewActivity(TopUpAddCardActivity.this, TopupPlasticCardActivity.class, bundle);
                                finish();

                            }else{
                                BakcellPopUpDialog.showMessageDialog(TopUpAddCardActivity.this,
                                        getString(R.string.bakcell_error_title), responseDescription);
                            }


                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!TopUpAddCardActivity.this.isFinishing() &&response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(TopUpAddCardActivity.this);
                                return;
                            }

                            if (!TopUpAddCardActivity.this.isFinishing() && response != null) {
                                BakcellPopUpDialog.showMessageDialog(TopUpAddCardActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!TopUpAddCardActivity.this.isFinishing() ) {
                                BakcellPopUpDialog.ApiFailureMessage(TopUpAddCardActivity.this);
                            }
                        }
                    });

        }
    } // requestInitiatePayment ends

    private boolean isValidAmount(String amount) {
        boolean isValid = true;
        if (!Tools.hasValue(amount) || !Tools.isNumeric(amount) || Double.parseDouble(amount) <= 0) {
            BakcellPopUpDialog.showMessageDialog(TopUpAddCardActivity.this,
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_msg_invalid_amount));

            isValid = false;
        }

        return isValid;
    }

    private boolean isValidMobileNumber(String mobileNumber) {
        boolean isValid = true;
        if (!FieldFormatter.isValidMsisdn(mobileNumber)) {
            BakcellPopUpDialog.showMessageDialog(TopUpAddCardActivity.this,
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_msg_invalid_number));
            isValid = false;
        }
        return isValid;
    }

}