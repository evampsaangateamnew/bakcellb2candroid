package com.bakcell.activities.landing;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivitySupplementaryOffersBinding;
import com.bakcell.fragments.dialogs.BakcellFilterDialogFragment;
import com.bakcell.fragments.menus.DashboardFragment;
import com.bakcell.fragments.pager.OffersPaggerFragment;
import com.bakcell.fragments.pager.RoamingPaggerFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.CallMySubscriptionFromOffersListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.mysubscriptions.SubscriptionsMain;
import com.bakcell.models.supplementaryoffers.OfferCategory;
import com.bakcell.models.supplementaryoffers.OfferFilter;
import com.bakcell.models.supplementaryoffers.OfferFiltersMain;
import com.bakcell.models.supplementaryoffers.RoamingsOffer;
import com.bakcell.models.supplementaryoffers.SupplementaryOfferMain;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription.Country;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.GetSupplementaryOffersService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.webservices.subscribeunsubscribe.SubscribeUnsubscribe;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class SupplementaryOffersActivity extends BaseActivity implements
        View.OnClickListener {

    public static final String TAB_NAVIGATION_KEY = "key.tab.navigation";
    public static final String OFFER_NAVIGATION_ID_KEY = "key.offer.navigation.id";

    public static final String LAYOUT_PHONE = "phone";
    public static final String LAYOUT_TAB = "tab";
    public static final String LAYOUT_DESKTOP = "tv";

    ActivitySupplementaryOffersBinding binding;

    PagerAdapterOffers pagerAdapterOffers;

    private boolean isSearchOpened = false;
    private SupplementaryOfferMain offerMain;
    private OfferFiltersMain offerFilter;
    private Toast toast;

    private String tabOpenedFromDashboard = "";
    private String offerIdFromNotificationOrDynamicLinksRedirection = "";
    private int lastSelectedTab = 1;
    private final String DIALOG_FILTER = "supplementary.filter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RootValues.getInstance().setOffersListOrientation(LinearLayoutManager.HORIZONTAL);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_supplementary_offers);

        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(TAB_NAVIGATION_KEY)) {
            tabOpenedFromDashboard = getIntent().getExtras().getString(TAB_NAVIGATION_KEY);
        } else if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA)
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA)
                && Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA))
                && Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA))) {
            offerIdFromNotificationOrDynamicLinksRedirection = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA);
            tabOpenedFromDashboard = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA);
        } else if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA)
                && Tools.hasValue(getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA))) {
            tabOpenedFromDashboard = getIntent().getExtras().getString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA);
        }

        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(OFFER_NAVIGATION_ID_KEY)) {
            offerIdFromNotificationOrDynamicLinksRedirection = getIntent().getExtras().getString(OFFER_NAVIGATION_ID_KEY);
        }
        BakcellLogger.logE("offerNavigationX", "offerId:::" + offerIdFromNotificationOrDynamicLinksRedirection, "SupplementaryOffersActivity", "below intent value");
        if (Tools.hasValue(tabOpenedFromDashboard)) {
            if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_VOICE)) {
                lastSelectedTab = 0;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_DATA)) {
                lastSelectedTab = 1;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_SMS) ||
                    tabOpenedFromDashboard.equalsIgnoreCase(getString(R.string
                            .supplementary_offer_tab_title_sms))) {
                lastSelectedTab = 2;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_ROAMING) ||
                    tabOpenedFromDashboard.equalsIgnoreCase(getString(R.string
                            .supplementary_offer_tab_title_roaming))) {
                lastSelectedTab = 5;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(getString(R.string
                    .supplementary_offer_tab_title_call))) {
                lastSelectedTab = 0;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(getString(R.string
                    .supplementary_offer_tab_title_internet))) {
                lastSelectedTab = 1;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(getString(R.string
                    .supplementary_offer_tab_title_hybrid))) {
                //ToDO : This tab id is previously set for notification tap, Need to confirm
                lastSelectedTab = 3;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(getString(R.string
                    .supplementary_offer_tab_title_tm))) {
                lastSelectedTab = 3;
            }
        }

        onCreateProcess();

        // Call API Or Get from Cache My Subscriptions
        callMySubscriptionsData();

        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_OFFERS_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_OFFERS_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_SUPPLEMENTARY_OFFERS_SCREEN);
        int i = 0;
        i++;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RootValues.getInstance().setOfferFilterListenerCall(null);
        RootValues.getInstance().setOfferFilterListenerInternet(null);
        RootValues.getInstance().setOfferFilterListenerSms(null);
        RootValues.getInstance().setOfferFilterListenerCampaign(null);
        RootValues.getInstance().setOfferFilterListenerTm(null);
        RootValues.getInstance().setOfferFilterListenerHybrid(null);

        RootValues.getInstance().setPhoneFilter(null);
        RootValues.getInstance().setTabFilter(null);
        RootValues.getInstance().setTvFilter(null);

    }

    private void callMySubscriptionsData() {

        RootValues.getInstance().setSubscriptionsMain(null);
        SubscribeUnsubscribe.requestMySubscriptionsFromOffer(SupplementaryOffersActivity.this, new CallMySubscriptionFromOffersListener() {
            @Override
            public void onCheckMySubscriptionListener(SubscriptionsMain subscriptionsMain) {
                RootValues.getInstance().setSubscriptionsMain(subscriptionsMain);
                /**
                 * In order to call api for supplementary Offers
                 * every time when user lands on this screen
                 * so we commented below code
                 */
                requestForGetSupplementaryOffers();

            /*    if(DataManager.getInstance().getSupplementaryOfferMainData()!=null){
                    offerMain =DataManager.getInstance().getSupplementaryOfferMainData();
                    loadUIAfterApi(offerMain);
                }
                else{
                    if (Tools.haveNetworkConnection(SupplementaryOffersActivity.this)) {
                        requestForGetSupplementaryOffers(true);
                    } else {
                        requestForGetSupplementaryOffers();
                    }
               }*/

            }
        });

    }

    private void onCreateProcess() {

        try {
            initUiContents();

            if (offerMain != null) {
                initUITabsAndPagger(offerMain);
            }

            if (RootValues.getInstance().getOffersListOrientation() == LinearLayoutManager.VERTICAL) {
                binding.toolbar.listingIcon.setImageDrawable(ContextCompat.getDrawable(this,
                        R.drawable.icon_listicon));
            } else {
                binding.toolbar.listingIcon.setImageDrawable(ContextCompat.getDrawable(this,
                        R.drawable.actionbar_sort));
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    private void initUiContents() {
        setToolbarAsPerScreen();
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.toolbar.offerListChangeLayout.setOnClickListener(this);
        binding.toolbar.settingLayout.setOnClickListener(this);
        binding.toolbar.searchMenu.setOnClickListener(this);
        binding.toolbar.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
// Do nothing because of X and Y.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (getString(R.string.supplementary_offer_tab_title_call)
                        .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.
                                getCurrentItem()))) {
                    RootValues.getInstance().getOfferFilterListenerCall().onFilterCall(true, s.toString(), null, false, "");
                } else if (getString(R.string.supplementary_offer_tab_title_internet)
                        .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                                .getCurrentItem()))) {
                    RootValues.getInstance().getOfferFilterListenerInternet().onFilterCall(true, s.toString(), null, false, "");
                } else if (getString(R.string.supplementary_offer_tab_title_sms)
                        .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                                .getCurrentItem()))) {
                    RootValues.getInstance().getOfferFilterListenerSms().onFilterCall(true, s.toString(), null, false, "");
                }
                //TODO Temporary Commented by client
//                else if (getString(R.string.supplementary_offer_tab_title_campaign)
//                        .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
//                                .getCurrentItem()))) {
//                    RootValues.getInstance().getOfferFilterListenerCampaign().onFilterCall(true, s.toString(), null, false, "");
//                } else if (getString(R.string.supplementary_offer_tab_title_tm)
//                        .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
//                                .getCurrentItem()))) {
//                    RootValues.getInstance().getOfferFilterListenerTm().onFilterCall(true, s.toString(), null, false, "");
//                }
                else if (getString(R.string.supplementary_offer_tab_title_hybrid)
                        .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                                .getCurrentItem()))) {
                    RootValues.getInstance().getOfferFilterListenerHybrid().onFilterCall(true, s.toString(), null, false, "");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
// Do nothing because of X and Y.
            }
        });
    }

    private void setToolbarAsPerScreen() {
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.activity_supplementary_offers_lbl);

        binding.toolbar.offerListChangeLayout.setVisibility(View.VISIBLE);
        binding.toolbar.settingLayout.setVisibility(View.VISIBLE);
        binding.toolbar.searchMenu.setVisibility(View.VISIBLE);
    }

    private void initUITabsAndPagger(SupplementaryOfferMain offerMain) {
        // Adding Tabs here
        binding.tabLayout.removeAllTabs();

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_call));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_internet));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_sms));
        //TODO Temporary Commented by client
//        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_campaign));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_tm));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_hybrid));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_roaming));

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (20 * getResources().getDisplayMetrics().density);
            p.setMargins(0, 0, margin, 0);
            tab.requestLayout();
        }

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                try {
                    if (position == 4) {
                        hideRightMenu();
                    } else if (position == 1) {
                        showRightMenu();
                    } else {
                        showRightMenuWithoutFilter();
                    }
                    if (RootValues.getInstance().getOfferFiltersCheckedList() != null) {
                        RootValues.getInstance().setOfferFiltersCheckedList(new ArrayList<OfferFilter>());
                    }

                    RootValues.getInstance().setPhoneFilter(null);
                    RootValues.getInstance().setTabFilter(null);
                    RootValues.getInstance().setTvFilter(null);

                    RootValues.getInstance().setFilterGroupAll(null);
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }

            @Override
            public void onPageSelected(int position) {
// Do nothing because of X and Y.
            }

            @Override
            public void onPageScrollStateChanged(int state) {
// Do nothing because of X and Y.
            }
        });

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
// Do nothing because of X and Y.
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
// Do nothing because of X and Y.
            }
        });

        iniPaggerView(offerMain);

    }


    /**
     * Toggles the layout of ListView to CardView and CardView to ListView
     */
    private void changeListLayout() {
        lastSelectedTab = binding.viewPager.getCurrentItem();
        if (RootValues.getInstance().getOffersListOrientation() == LinearLayoutManager.VERTICAL) {
            RootValues.getInstance().setOffersListOrientation(LinearLayoutManager.HORIZONTAL);
            binding.toolbar.listingIcon.setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.actionbar_sort));
        } else {
            RootValues.getInstance().setOffersListOrientation(LinearLayoutManager.VERTICAL);
            binding.toolbar.listingIcon.setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.icon_listicon));
        }
        onCreateProcess();
    }


    private void iniPaggerView(SupplementaryOfferMain offerMain) {
        if (offerMain == null) return;

        OfferCategory offerCategoryTemp = new OfferCategory();
        offerCategoryTemp.setOffers(new ArrayList<SupplementaryOffer>());
        if (offerMain.getCall() == null) {
            offerMain.setCall(offerCategoryTemp);
        }
        if (offerMain.getInternet() == null) {
            offerMain.setInternet(offerCategoryTemp);
        }
        if (offerMain.getSms() == null) {
            offerMain.setSms(offerCategoryTemp);
        }
        if (offerMain.getCampaign() == null) {
            offerMain.setCampaign(offerCategoryTemp);
        }
        if (offerMain.getTm() == null) {
            offerMain.setTm(offerCategoryTemp);
        }
        if (offerMain.getHybrid() == null) {
            offerMain.setHybrid(offerCategoryTemp);
        }
        if (offerMain.getRoaming() == null) {
            RoamingsOffer roamingsOffer = new RoamingsOffer();
            roamingsOffer.setOffers(new ArrayList<SupplementaryOffer>());
            roamingsOffer.setCountries(new ArrayList<Country>());
            offerMain.setRoaming(roamingsOffer);
        }
        binding.viewPager.disableScroll(true);
        ArrayList<Fragment> paggerFragmentsList = new ArrayList<>();
        int foundTabPosFromNotificationsOrDynamicLinks = -1;
        if (binding.tabLayout != null && binding.tabLayout.getTabCount() > 0) {
            for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
                if (getString(R.string.supplementary_offer_tab_title_call).equalsIgnoreCase(getCurrentSelectedTab(i))
                        && offerMain.getCall() != null
                        && offerMain.getCall().getOffers() != null) {

                    int foundOfferItemFromNotificationsOrDynamicLinks = findOfferIdAfterRedirectingFromNotificationsOrDynamicLinks(offerIdFromNotificationOrDynamicLinksRedirection, offerMain.getCall().getOffers());
                    if (foundOfferItemFromNotificationsOrDynamicLinks != -1) {
                        foundTabPosFromNotificationsOrDynamicLinks = 0;
                    }
                    paggerFragmentsList.add(OffersPaggerFragment.getInstance(foundOfferItemFromNotificationsOrDynamicLinks, getCurrentSelectedTab(i),
                            offerMain.getCall().getOffers()));

                } else if (getString(R.string.supplementary_offer_tab_title_internet).equalsIgnoreCase(getCurrentSelectedTab(i))
                        && offerMain.getInternet() != null
                        && offerMain.getInternet().getOffers() != null) {

                    int foundOfferItemFromNotificationsOrDynamicLinks = findOfferIdAfterRedirectingFromNotificationsOrDynamicLinks(offerIdFromNotificationOrDynamicLinksRedirection, offerMain.getInternet().getOffers());
                    BakcellLogger.logE("offerIdNotifyX", "offerId:::net:::" + foundOfferItemFromNotificationsOrDynamicLinks, "supplementaryoffersactivity", "showingofferid");
                    if (foundOfferItemFromNotificationsOrDynamicLinks != -1) {
                        foundTabPosFromNotificationsOrDynamicLinks = 1;
                    }
                    paggerFragmentsList.add(OffersPaggerFragment.getInstance(foundOfferItemFromNotificationsOrDynamicLinks, getCurrentSelectedTab(i),
                            offerMain.getInternet().getOffers()));
                } else if (getString(R.string.supplementary_offer_tab_title_sms).equalsIgnoreCase(getCurrentSelectedTab(i))
                        && offerMain.getSms() != null
                        && offerMain.getSms().getOffers() != null) {
                    int foundOfferItemFromNotificationsOrDynamicLinks = findOfferIdAfterRedirectingFromNotificationsOrDynamicLinks(offerIdFromNotificationOrDynamicLinksRedirection, offerMain.getSms().getOffers());
                    if (foundOfferItemFromNotificationsOrDynamicLinks != -1) {
                        foundTabPosFromNotificationsOrDynamicLinks = 2;
                    }
                    paggerFragmentsList.add(OffersPaggerFragment.getInstance(foundOfferItemFromNotificationsOrDynamicLinks, getCurrentSelectedTab(i),
                            offerMain.getSms().getOffers()));
                }
                //TODO Temporary Commented by client
//                else if (getString(R.string.supplementary_offer_tab_title_campaign).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
//                        offerMain.getCampaign() != null &&
//                        offerMain.getCampaign().getOffers() != null) {
//                    int foundOfferItemFromNotifications = findOfferIdAfterRedirectingFromNotifications(offerIdFromNotificationRedirection, offerMain.getCampaign().getOffers());
//                    if (foundOfferItemFromNotifications != -1) {
//                        foundTabPosFromNotifications = 3;
//                    }
//                    paggerFragmentsList.add(OffersPaggerFragment.getInstance(foundOfferItemFromNotifications, getCurrentSelectedTab(i),
//                            offerMain.getCampaign().getOffers()));
//                }
                else if (getString(R.string.supplementary_offer_tab_title_tm).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
                        offerMain.getTm() != null &&
                        offerMain.getTm().getOffers() != null) {
                    int foundOfferItemFromNotificationsOrDynamicLinks = findOfferIdAfterRedirectingFromNotificationsOrDynamicLinks(offerIdFromNotificationOrDynamicLinksRedirection, offerMain.getTm().getOffers());
                    if (foundOfferItemFromNotificationsOrDynamicLinks != -1) {
                        foundTabPosFromNotificationsOrDynamicLinks = 3;
                    }
                    paggerFragmentsList.add(OffersPaggerFragment.getInstance(foundOfferItemFromNotificationsOrDynamicLinks, getCurrentSelectedTab(i),
                            offerMain.getTm().getOffers()));
                } else if (getString(R.string.supplementary_offer_tab_title_hybrid).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
                        offerMain.getHybrid() != null &&
                        offerMain.getHybrid().getOffers() != null) {
                    int foundOfferItemFromNotificationsOrDynamicLinks = findOfferIdAfterRedirectingFromNotificationsOrDynamicLinks(offerIdFromNotificationOrDynamicLinksRedirection, offerMain.getHybrid().getOffers());
                    if (foundOfferItemFromNotificationsOrDynamicLinks != -1) {
                        foundTabPosFromNotificationsOrDynamicLinks = 4;
                    }
                    paggerFragmentsList.add(OffersPaggerFragment.getInstance(foundOfferItemFromNotificationsOrDynamicLinks, getCurrentSelectedTab(i),
                            offerMain.getHybrid().getOffers()));
                } else if (getString(R.string.supplementary_offer_tab_title_roaming).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
                        offerMain.getRoaming() != null) {
                    paggerFragmentsList.add(RoamingPaggerFragment.getInstance(offerMain.getRoaming()));
                }

            }
        }

        if (foundTabPosFromNotificationsOrDynamicLinks != -1 && Tools.hasValue(offerIdFromNotificationOrDynamicLinksRedirection)) {
            lastSelectedTab = foundTabPosFromNotificationsOrDynamicLinks;
        }

        try {
            pagerAdapterOffers = new PagerAdapterOffers(getSupportFragmentManager(), paggerFragmentsList);
            binding.viewPager.setAdapter(pagerAdapterOffers);
            BakcellLogger.logE("lastSelectedTab", "tab#:::" + lastSelectedTab, "SupplementaryOffersActivity", "showing tab index");
            binding.viewPager.setCurrentItem(lastSelectedTab);
        } catch (Exception ex) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
        }
    }

    private int findOfferIdAfterRedirectingFromNotificationsOrDynamicLinks(String offeringId, ArrayList<SupplementaryOffer> offersList) {
        int foundOfferPosFromNotification = -1;
        try {
            if (Tools.hasValue(offeringId) && offersList != null && offersList.size() > 0) {
                for (int i = 0; i < offersList.size(); i++) {
                    if (offersList.get(i).getHeader() != null && Tools.hasValue(offersList.get(i).getHeader().getOfferingId()) && offersList.get(i).getHeader().getOfferingId().equalsIgnoreCase(offeringId)) {
                        foundOfferPosFromNotification = i;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return foundOfferPosFromNotification;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.offer_list_change_layout:
                changeListLayout();
                break;
            case R.id.setting_layout:
                showFilterAlert();
                break;
            case R.id.search_menu:
                if (!isSearchOpened) {
                    binding.toolbar.etSearch.requestFocus();
                    Tools.showKeyboard(this);
                    binding.toolbar.etSearch.setText("");
                    binding.toolbar.etSearch.setHint(R.string.tool_bar_search_lbl);
                    binding.toolbar.pageTitle.setVisibility(View.GONE);
                    binding.toolbar.searchView.setVisibility(View.VISIBLE);
                    binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.ic_cross));
                    isSearchOpened = true;

                } else {
                    Tools.hideKeyboard(this);
                    binding.toolbar.pageTitle.setVisibility(View.VISIBLE);
                    binding.toolbar.searchView.setVisibility(View.GONE);
                    binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.actionbar_search));
                    isSearchOpened = false;
                    if (getString(R.string.supplementary_offer_tab_title_call)
                            .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.
                                    getCurrentItem()))) {
                        RootValues.getInstance().getOfferFilterListenerCall().onFilterCall(false, null, null, true, "");
                    } else if (getString(R.string.supplementary_offer_tab_title_internet)
                            .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                                    .getCurrentItem()))) {
                        RootValues.getInstance().getOfferFilterListenerInternet().onFilterCall(false, null, null, true, "");
                    } else if (getString(R.string.supplementary_offer_tab_title_sms)
                            .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                                    .getCurrentItem()))) {
                        RootValues.getInstance().getOfferFilterListenerSms().onFilterCall(false, null, null, true, "");
                    }
                    //TODO Temporary Commented by client
//                    else if (getString(R.string.supplementary_offer_tab_title_campaign)
//                            .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
//                                    .getCurrentItem()))) {
//                        RootValues.getInstance().getOfferFilterListenerCampaign().onFilterCall(false, null, null, true, "");
//                    } else if (getString(R.string.supplementary_offer_tab_title_tm)
//                            .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
//                                    .getCurrentItem()))) {
//                        RootValues.getInstance().getOfferFilterListenerTm().onFilterCall(false, null, null, true, "");
//                    }
                    else if (getString(R.string.supplementary_offer_tab_title_hybrid)
                            .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                                    .getCurrentItem()))) {
                        RootValues.getInstance().getOfferFilterListenerHybrid().onFilterCall(false, null, null, true, "");
                    }
                }
                break;
        }
    }

    private void showFilterAlert() {
        offerFilter = null;
        if (getString(R.string.supplementary_offer_tab_title_call).equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))
                && offerMain.getCall() != null && offerMain.getCall().getFilters() != null) {
            offerFilter = offerMain.getCall().getFilters();
        } else if (getString(R.string.supplementary_offer_tab_title_internet).equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))
                && offerMain.getInternet() != null && offerMain.getInternet().getFilters() != null) {
            offerFilter = offerMain.getInternet().getFilters();
        } else if (getString(R.string.supplementary_offer_tab_title_sms).equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))
                && offerMain.getSms() != null && offerMain.getSms().getFilters() != null) {
            offerFilter = offerMain.getSms().getFilters();
        }
        //TODO Temporary Commented by client
//        else if (getString(R.string.supplementary_offer_tab_title_campaign).equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))
//                && offerMain.getCampaign() != null && offerMain.getCampaign().getFilters() != null) {
//            offerFilter = offerMain.getCampaign().getFilters();
//        } else if (getString(R.string.supplementary_offer_tab_title_tm).equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))
//                && offerMain.getTm() != null && offerMain.getTm().getFilters() != null) {
//            offerFilter = offerMain.getTm().getFilters();
//        }
        else if (getString(R.string.supplementary_offer_tab_title_hybrid).equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))
                && offerMain.getHybrid() != null && offerMain.getHybrid().getFilters() != null) {
            offerFilter = offerMain.getHybrid().getFilters();
        } else if (getString(R.string.supplementary_offer_tab_title_roaming).equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))
                && offerMain.getRoaming() != null && offerMain.getRoaming().getFilters() != null) {
            offerFilter = offerMain.getRoaming().getFilters();
        }

        if (offerFilter != null) {
            BakcellFilterDialogFragment
                    .newInstance(offerFilter, false)
                    .show(getSupportFragmentManager(), DIALOG_FILTER);
        } else {
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(this, R.string.lbl_no_filter_msg, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * shows right menu in toolbar
     */
    public void showRightMenu() {
        binding.toolbar.searchMenu.setVisibility(View.VISIBLE);
        binding.toolbar.offerListChangeLayout.setVisibility(View.VISIBLE);
        binding.toolbar.settingLayout.setVisibility(View.VISIBLE);
        binding.toolbar.searchView.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setVisibility(View.VISIBLE);
        setSearchIcon();
    }


    /**
     * shows right menu in toolbar
     */
    public void showRightMenuWithoutFilter() {
        binding.toolbar.searchMenu.setVisibility(View.VISIBLE);
        binding.toolbar.offerListChangeLayout.setVisibility(View.VISIBLE);
        binding.toolbar.settingLayout.setVisibility(View.GONE);
        binding.toolbar.searchView.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setVisibility(View.VISIBLE);
        setSearchIcon();
    }

    /**
     * Hides right menu
     */
    public void hideRightMenu() {
        binding.toolbar.searchMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.offerListChangeLayout.setVisibility(View.INVISIBLE);
        binding.toolbar.settingLayout.setVisibility(View.INVISIBLE);
        binding.toolbar.searchView.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setVisibility(View.VISIBLE);
        setSearchIcon();
    }

    /**
     * sets icon for search icon in toolbar
     */
    private void setSearchIcon() {
        if (isSearchOpened) {
            binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.actionbar_search));
            isSearchOpened = false;
        }
    }


    public void applyFilter(ArrayList<OfferFilter> filteredIds, String offerGroupFilterAll) {
        if (getString(R.string.supplementary_offer_tab_title_call)
                .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager.getCurrentItem()))) {
            RootValues.getInstance().getOfferFilterListenerCall().onFilterCall(false, getString(R.string.supplementary_offer_tab_title_call), filteredIds, false, offerGroupFilterAll);
        } else if (getString(R.string.supplementary_offer_tab_title_internet)
                .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                        .getCurrentItem()))) {
            RootValues.getInstance().getOfferFilterListenerInternet().onFilterCall(false, getString(R.string.supplementary_offer_tab_title_internet), filteredIds, false, offerGroupFilterAll);
        } else if (getString(R.string.supplementary_offer_tab_title_sms)
                .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                        .getCurrentItem()))) {
            RootValues.getInstance().getOfferFilterListenerSms().onFilterCall(false, getString(R.string.supplementary_offer_tab_title_sms), filteredIds, false, offerGroupFilterAll);
        }
        //TODO Temporary Commented by client
//        else if (getString(R.string.supplementary_offer_tab_title_campaign)
//                .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
//                        .getCurrentItem()))) {
//            RootValues.getInstance().getOfferFilterListenerCampaign().onFilterCall(false, getString(R.string.supplementary_offer_tab_title_campaign), filteredIds, false, offerGroupFilterAll);
//        } else if (getString(R.string.supplementary_offer_tab_title_tm)
//                .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
//                        .getCurrentItem()))) {
//            RootValues.getInstance().getOfferFilterListenerTm().onFilterCall(false, getString(R.string.supplementary_offer_tab_title_tm), filteredIds, false, offerGroupFilterAll);
//        }
        else if (getString(R.string.supplementary_offer_tab_title_hybrid)
                .equalsIgnoreCase(getCurrentSelectedTab(binding.viewPager
                        .getCurrentItem()))) {
            RootValues.getInstance().getOfferFilterListenerHybrid().onFilterCall(false, getString(R.string.supplementary_offer_tab_title_hybrid), filteredIds, false, offerGroupFilterAll);
        }
        Fragment prev = getSupportFragmentManager().findFragmentByTag(DIALOG_FILTER);
        if (prev != null) {
            BakcellFilterDialogFragment df = (BakcellFilterDialogFragment) prev;
            df.dismiss();
        }
    }

    public String getCurrentSelectedTab(int index) {
        TextView tv = null;
        String tabTitle = null;
        try {
            try {
                tv = (TextView) (((LinearLayout) ((LinearLayout) binding.tabLayout.getChildAt(0))
                        .getChildAt(index)).getChildAt(1));
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            tabTitle = "";
            if (tv != null) {
                tabTitle = tv.getText().toString();
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return tabTitle;
    }

    private void requestForGetSupplementaryOffers() {

        UserModel customerData = DataManager.getInstance().getCurrentUser();

        if (customerData == null) {
            Toast.makeText(SupplementaryOffersActivity.this, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        GetSupplementaryOffersService.newInstance(SupplementaryOffersActivity.this,
                ServiceIDs.GET_SUPPLEMENTARY_OFFERS).execute(customerData,
                new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request,
                                          EaseResponse<String> response) {

                        RootValues.getInstance().setSupplementaryOffersApiCall(false);

                        try {
                            String result = response.getData();
                            offerMain = new Gson().fromJson(result, SupplementaryOfferMain.class);
                            if (offerMain != null) {
                                DataManager.getInstance().setSupplementaryOfferMainData(offerMain);
                                loadUIAfterApi(offerMain);
                            }
                        } catch (JsonSyntaxException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }

                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request,
                                          EaseResponse<String> response) {
                        // Check if user logout by server
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(SupplementaryOffersActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(SupplementaryOffersActivity.this);
                            return;
                        }

                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getApplicationContext());
                        String cacheResponse = EaseCacheManager.get(SupplementaryOffersActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                if (offerMain != null) {
                                    loadUIAfterApi(offerMain);
                                }
                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(SupplementaryOffersActivity.this,
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getApplicationContext());
                        String cacheResponse = EaseCacheManager.get(SupplementaryOffersActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                if (offerMain != null) {
                                    loadUIAfterApi(offerMain);
                                }
                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        BakcellPopUpDialog.ApiFailureMessage(SupplementaryOffersActivity.this);
                    }
                });

    }

    private void loadUIAfterApi(SupplementaryOfferMain offerMain) {
        initUITabsAndPagger(offerMain);
    }

    public interface SearchListener {
        void onSearchEnded();
    }


}
