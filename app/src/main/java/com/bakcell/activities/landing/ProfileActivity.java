package com.bakcell.activities.landing;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityProfileBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.EmailChangedListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.profile.ProfileImage;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestChangeBillingLanguageService;
import com.bakcell.webservices.RequestUploadImage;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {
    ActivityProfileBinding binding;
    private static final int PICK_IMAGE_REQUEST = 100;
    private static final int TAKE_IMAGE_REQUEST = 200;
    private static final int STORAGE_PERMISSION_CODE = 102;

    String languageId = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        RootValues.getInstance().setEmailChangedListener(emailChangedListener);

        initUiContent();
        initUiListeners();
        loadProfileImage();

        AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                AppEventLogValues.ProfileScreen.PROFILE_SCREEN);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RootValues.getInstance().setEmailChangedListener(null);
    }

    private void loadProfileImage() {
        if (DataManager.getInstance().getCurrentUser() != null) {
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getImageURL())) {
                String imageUrl = DataManager.getInstance().getCurrentUser()
                        .getImageURL();
                Picasso.with(ProfileActivity.this)
                        .load(imageUrl)
                        .into(target);

            }
        }
    }

    private void loadProfileImage(String imageUrl) {
        Picasso.with(ProfileActivity.this).load(imageUrl).networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(target);
    }

    EmailChangedListener emailChangedListener = new EmailChangedListener() {
        @Override
        public void onEmailChangedListen(String email) {
//            if (!Tools.hasValue(email)) return;
//            binding.tvEmail.setText(email);
//            if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
//                if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getEmail())) {
//                    binding.tvEmail.setText(DataManager.getInstance().getCurrentUser().getEmail());
//                }
//            }
        }
    };

    private void initUiListeners() {
        binding.llBillingLanguage.setOnClickListener(this);
        binding.profileImage.setOnClickListener(this);
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.llEmail.setOnClickListener(this);

    }

    private void initUiContent() {
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));

        binding.toolbar.pageTitle.setText(R.string.lbl_profile);

        setUserNameAndNumber();
        setProfileInfo();

        binding.profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfileClicked();
            }
        });


        binding.llBillingLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLanguageClicked();
            }
        });

        binding.toolbar.ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setUserNameAndNumber() {

        binding.menuNameTxt.setText("");
        binding.menuNumberTxt.setText("");

        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String userName = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getFirstName())) {
                userName = DataManager.getInstance().getCurrentUser().getFirstName();
            }

            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMiddleName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getMiddleName());
            }

            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLastName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getLastName());
            }
            binding.menuNameTxt.setText(userName);

            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                binding.menuNumberTxt.setText(DataManager.getInstance().getCurrentUser().getMsisdn());
            }
        }
    }

    private void setProfileInfo() {
        binding.tvSim.setText("");
        binding.tvPin.setText("");
        binding.tvPuk.setText("");
        binding.tvEmail.setText("");
        binding.tvActivationDate.setText("");
        binding.billingLanguageText.setText("");
        binding.tvLoyalityIndicator.setText("");
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getEmail())) {
                binding.tvEmail.setText(DataManager.getInstance().getCurrentUser().getEmail());
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getPin())) {
                binding.tvPin.setText(DataManager.getInstance().getCurrentUser().getPin());
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getPuk())) {
                binding.tvPuk.setText(DataManager.getInstance().getCurrentUser().getPuk());
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getSim())) {
                binding.tvSim.setText(DataManager.getInstance().getCurrentUser().getSim());
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getEffectiveDate())) {
                try {
                    binding.tvActivationDate.setText(Tools.getDateAccordingToClientSaid(
                            DataManager.getInstance().getCurrentUser().getEffectiveDate()
                    ));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLoyaltySegment())) {
                binding.tvLoyalityIndicator.setText(DataManager.getInstance().getCurrentUser().getLoyaltySegment());
            }

            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLanguage())) {
                if (DataManager.getInstance().getCurrentUser().getLanguage().equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ)) {
                    binding.billingLanguageText.setText(getString(R.string.lbl_az_azarbaycan));
                } else if (DataManager.getInstance().getCurrentUser().getLanguage().equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU)) {
                    binding.billingLanguageText.setText(getString(R.string.lbl_ru_rusian));
                } else if (DataManager.getInstance().getCurrentUser().getLanguage().equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN)) {
                    binding.billingLanguageText.setText(getString(R.string.lbl_en_english));
                }
            }

        }
    }


    protected void changeLanguageClicked() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_select_langue, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RadioGroup languageGroup = (RadioGroup) dialogView.findViewById(R.id.languageGroup);

        AppCompatRadioButton azeriRadio = (AppCompatRadioButton) dialogView.findViewById(R.id.azeri);
        AppCompatRadioButton russianRadio = (AppCompatRadioButton) dialogView.findViewById(R.id.russian);
        AppCompatRadioButton englishRadio = (AppCompatRadioButton) dialogView.findViewById(R.id.english);

        languageGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int id) {

                switch (id) {
                    case R.id.azeri:
                        languageId = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ;
                        break;
                    case R.id.russian:
                        languageId = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU;
                        break;
                    case R.id.english:
                        languageId = Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN;
                        break;

                }

            }
        });

        Button yesBtn = (Button) dialogView.findViewById(R.id.btn_done);
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Tools.hasValue(languageId)) {
                    if (DataManager.getInstance().getCurrentUser() != null) {
                        String lang = DataManager.getInstance().getCurrentUser().getLanguage();
                        if (Tools.hasValue(lang)) {
                            if (!languageId.equalsIgnoreCase(lang))
                                requestChangeBillingLanguage(languageId);
                        } else {
                            requestChangeBillingLanguage(languageId);
                        }
                    }
                }
                alertDialog.dismiss();
            }
        });


        if (DataManager.getInstance().getCurrentUser().getLanguage().equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ)) {
            azeriRadio.setChecked(true);
            russianRadio.setChecked(false);
            englishRadio.setChecked(false);
        } else if (DataManager.getInstance().getCurrentUser().getLanguage().equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU)) {
            azeriRadio.setChecked(false);
            russianRadio.setChecked(true);
            englishRadio.setChecked(false);
        } else if (DataManager.getInstance().getCurrentUser().getLanguage().equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN)) {
            azeriRadio.setChecked(false);
            russianRadio.setChecked(false);
            englishRadio.setChecked(true);
        }


    }

    protected void editProfileClicked() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_profile_picture, null);
        dialogBuilder.setView(dialogView);

        TextView tvGallery = (TextView) dialogView.findViewById(R.id.tv_gallery);
        TextView tvCamera = (TextView) dialogView.findViewById(R.id.tv_camera);
        TextView tvRemove = (TextView) dialogView.findViewById(R.id.tv_remove);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams param = alertDialog.getWindow().getAttributes();
        param.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        param.y = 400;
        alertDialog.getWindow().setAttributes(param);
        alertDialog.show();
        alertDialog.getWindow().setLayout((int) Tools.convertDpToPixel(200, this), WindowManager.LayoutParams.WRAP_CONTENT);
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.lbl_select_pic)), PICK_IMAGE_REQUEST);
            }
        });

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (ContextCompat.checkSelfPermission(ProfileActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                } else {
                    performExternalStoragePermission();
                }
            }
        });

        tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImageToServer("", ".jpg", 3);
                alertDialog.dismiss();
            }
        });

    }


    private void performExternalStoragePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        STORAGE_PERMISSION_CODE);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        STORAGE_PERMISSION_CODE);
            }
        }
    }


    private void cameraIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, TAKE_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {
            Uri uri = data.getData();
            if (uri != null) {
                CropImage.activity(uri)
                        .start(this);
            }
        }
        if (data != null && data.getExtras() != null && requestCode == TAKE_IMAGE_REQUEST && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            if (photo != null) {
                Uri tempUri = getImageUri(getApplicationContext(), photo);
                if (tempUri != null) {
                    CropImage.activity(tempUri).start(this);
                }
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (result != null) {
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    uploadImage(resultUri);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        try {
            if (inContext == null || inImage == null) return null;
            String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                    inImage, getString(R.string.profile_picture), null);
            if (path == null) return null;
            return Uri.parse(path);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                } else {
                }
                return;
        }
    }

    public void uploadImage(Uri uri) {
        if (uri == null) return;
        String imageBase64 = convertImageToBase64(uri);
        String imgExt = ".jpg";
        uploadImageToServer(imageBase64, imgExt, 1);
    }

    private void uploadImageToServer(String imageBase64, String imgExt, final int actionType) {
        if (DataManager.getInstance().getCurrentUser() != null && imageBase64 != null) {
            final UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestUploadImage.newInstance(this, ServiceIDs.REQUEST_ADD_UPLOAD_IMAGE).execute(userModel,
                    imageBase64, imgExt, actionType, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String result = response.getData();

                            ProfileImage profileImage = new Gson().fromJson(result, ProfileImage.class);

                            if (actionType == 1) {
                                if (profileImage != null && Tools.hasValue(profileImage.getImageURL())) {
                                    loadProfileImage(profileImage.getImageURL());
                                    DataManager.getInstance().updateCurrentUserProfileImage(ProfileActivity.this, profileImage.getImageURL());
                                }
                                AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                                        AppEventLogValues.ProfileScreen.ADD_IMAGE_SUCCESS,
                                        AppEventLogValues.ProfileScreen.PROFILE_SCREEN);
                            } else {
                                DataManager.getInstance().updateCurrentUserProfileImage(ProfileActivity.this, "abc");
                                RootValues.getInstance().getOnProfileImageUpdate().onProfileImageUpdate(null);
                                binding.profileImage.setImageBitmap(null);
                                binding.profileImage.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                                binding.profileImage.setImageDrawable(ContextCompat.getDrawable(ProfileActivity.this,
                                        R.drawable.icon_profilesettings));
                                AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                                        AppEventLogValues.ProfileScreen.REMOVE_IMAGE_SUCCESS,
                                        AppEventLogValues.ProfileScreen.PROFILE_SCREEN);
                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            // Check if user logout by server
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(ProfileActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(ProfileActivity.this);
                                return;
                            }
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(ProfileActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                            if (actionType == 1) {
                                AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                                        AppEventLogValues.ProfileScreen.ADD_IMAGE_FAILURE,
                                        AppEventLogValues.ProfileScreen.PROFILE_SCREEN);
                            } else {
                                AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                                        AppEventLogValues.ProfileScreen.REMOVE_IMAGE_FAILURE,
                                        AppEventLogValues.ProfileScreen.PROFILE_SCREEN);
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(ProfileActivity.this);
                        }
                    });
        }
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            try {
                binding.profileImage.setImageBitmap(null);
                binding.profileImage.setImageBitmap(bitmap);
                if (bitmap != null)
                    RootValues.getInstance().getOnProfileImageUpdate().onProfileImageUpdate(bitmap);
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            int i = 0;
            i++;
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            int i = 0;
            i++;
        }
    };

    public String convertImageToBase64(Uri uri) {
        try {
            Bitmap bm = BitmapFactory.decodeFile(uri.getPath());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.ll_billing_language:
                changeLanguageClicked();
                break;
            case R.id.profile_image:
                editProfileClicked();
                break;
            case R.id.ll_email:
                startNewActivity(this, ChangeEmailActivity.class);
                break;

        }
    }


    private void requestChangeBillingLanguage(final String langId) {

        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestChangeBillingLanguageService.newInstance(this, ServiceIDs.REQUEST_CHANGE_BILLING_LANGUAGE)
                    .execute(userModel, langId, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            if (response != null) {

                                BakcellPopUpDialog.showMessageDialog(ProfileActivity.this,
                                        getString(R.string.mesg_successful_title), response.getDescription());
                                if (langId.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ)) {
                                    DataManager.getInstance().updateCurrentUserBillingLanguage(ProfileActivity.this,
                                            Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ);
                                    binding.billingLanguageText.setText(getString(R.string.lbl_az_azarbaycan));
                                } else if (langId.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU)) {
                                    DataManager.getInstance().updateCurrentUserBillingLanguage(ProfileActivity.this,
                                            Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU);
                                    binding.billingLanguageText.setText(getString(R.string.lbl_ru_rusian));
                                } else if (langId.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN)) {
                                    DataManager.getInstance().updateCurrentUserBillingLanguage(ProfileActivity.this,
                                            Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN);
                                    binding.billingLanguageText.setText(getString(R.string.lbl_en_english));
                                }

                                AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                                        AppEventLogValues.ProfileScreen.BILLING_LANGUAGE_CHANGED_SUCCESS + " " + langId + " Success",
                                        AppEventLogValues.ProfileScreen.PROFILE_SCREEN);

                                setUserNameAndNumber();
                                setProfileInfo();

                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {

                            // Check if user logout by server
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(ProfileActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(ProfileActivity.this);
                                return;
                            }
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(ProfileActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                            AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.PROFILE_SCREEN,
                                    AppEventLogValues.ProfileScreen.BILLING_LANGUAGE_CHANGED_FAILURE + " " + langId + " Failure",
                                    AppEventLogValues.ProfileScreen.PROFILE_SCREEN);
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(ProfileActivity.this);
                        }
                    });
        }
    }

}
