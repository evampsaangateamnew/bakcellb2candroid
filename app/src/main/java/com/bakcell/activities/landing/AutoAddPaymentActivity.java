package com.bakcell.activities.landing;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.databinding.ActivityAutoAddPaymentScreenBinding;
import com.bakcell.fragments.pager.autopayment.AutoPaymentDailyFragment;
import com.bakcell.fragments.pager.autopayment.AutoPaymentMonthlyFragment;
import com.bakcell.fragments.pager.autopayment.AutoPaymentWeeklyFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class AutoAddPaymentActivity extends BaseActivity {
    ActivityAutoAddPaymentScreenBinding binding;
    PagerAdapterOffers pagerAdapterOffers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_auto_add_payment_screen);
        initUiContent();

        initUiListeners();

    }

    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
    }

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.lbl_new_auto_payment);

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.add_auto_payment_daily));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.add_auto_payment_weekly));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.add_auto_payment_monthly));

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (10 * getResources().getDisplayMetrics().density);
            p.setMargins(margin, 0, margin, 0);
            tab.requestLayout();
        }
        binding.viewPager.disableScroll(true);
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
// Do nothing because of X and Y.
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
// Do nothing because of X and Y.
            }
        });

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
        initViewPager();
    }

    private void initViewPager() {
        ArrayList<Fragment> paggerFragmentsList = new ArrayList<>();
        paggerFragmentsList.add(new AutoPaymentDailyFragment());
        paggerFragmentsList.add(new AutoPaymentWeeklyFragment());
        paggerFragmentsList.add(new AutoPaymentMonthlyFragment());
        pagerAdapterOffers = new PagerAdapterOffers(getSupportFragmentManager(), paggerFragmentsList);
        binding.viewPager.setAdapter(pagerAdapterOffers);

    }



}