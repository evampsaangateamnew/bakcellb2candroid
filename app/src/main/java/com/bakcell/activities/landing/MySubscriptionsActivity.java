package com.bakcell.activities.landing;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityMySubscriptionsBinding;
import com.bakcell.fragments.menus.DashboardFragment;
import com.bakcell.fragments.pager.MySubscriptionsPaggerFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.mysubscriptions.SubscriptionCategory;
import com.bakcell.models.mysubscriptions.Subscriptions;
import com.bakcell.models.mysubscriptions.SubscriptionsMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestMySubscriptionsService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;


/**
 * Created by Noman on 18-May-17.
 */

public class MySubscriptionsActivity extends BaseActivity implements View.OnClickListener {

    ActivityMySubscriptionsBinding binding;

    // My Subscriptions Free Resource Usage Type
    public static final String MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET = "511";
    public static final String MY_SUBSCRIPTIONS_FREE_USAGE_HYBRID = "512";
    public static final String MY_SUBSCRIPTIONS_FREE_USAGE_CALL = "513";
    public static final String MY_SUBSCRIPTIONS_FREE_USAGE_SMS = "514";
    public static final String MY_SUBSCRIPTIONS_FREE_USAGE_TM = "515";
    public static final String MY_SUBSCRIPTIONS_FREE_USAGE_CAMPAIGN = "516";
    public static final String MY_SUBSCRIPTIONS_FREE_USAGE_ROAMING = "517";

    private String tabOpenedFromDashboard = "";
    private int lastSelectedTab = 1;
    private boolean isFromDashboard = false;

    private boolean isFromDashboardUsage = false;

    private SubscriptionsMain subscriptionsMain;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_subscriptions);

        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(SupplementaryOffersActivity.TAB_NAVIGATION_KEY)) {
            tabOpenedFromDashboard = getIntent().getExtras().getString(SupplementaryOffersActivity.TAB_NAVIGATION_KEY);
        }

        if (Tools.hasValue(tabOpenedFromDashboard)) {
            isFromDashboard = true;
            if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_VOICE)) {
                lastSelectedTab = 0;
                isFromDashboardUsage = true;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_DATA)) {
                lastSelectedTab = 1;
                isFromDashboardUsage = true;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_SMS)) {
                lastSelectedTab = 2;
                isFromDashboardUsage = true;
            } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                    .CIRCLE_RESOURCE_TYPE_ROAMING)) {
                lastSelectedTab = 5;
                isFromDashboardUsage = false;
            }
        } else {
            isFromDashboard = false;
            isFromDashboardUsage = false;
        }

        initUIContents();

        initUiListeners();

        requestMySubscriptions();

        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN,
                AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN,
                AppEventLogValues.AccountEvents.MY_SUBSCRIPTION_SCREEN);

    }


    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
    }

    private void initUIContents() {

        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setText(R.string.title_my_subscriptions);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));

    }

    private void initUITabsAndPagger(SubscriptionsMain subscriptionsMain) {
        if (subscriptionsMain == null) return;

        // Adding Tabs here
        binding.tabLayout.removeAllTabs();
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_call));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_internet));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_sms));
        //TODO Temporary Commented by client
//        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_campaign));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_tm));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_hybrid));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_roaming));

        if (isFromDashboardUsage && isAllInclusizeTabNeedToShow(subscriptionsMain)) {
            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.subscriptions_all_inclusive));
        }

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (20 * getResources().getDisplayMetrics().density);
            p.setMargins(0, 0, margin, 0);
            tab.requestLayout();
        }

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        iniPaggerView(subscriptionsMain);

    }

    private boolean isAllInclusizeTabNeedToShow(SubscriptionsMain subscriptionsMain) {
        boolean isAllInclusizeTabShow = false;
        if (subscriptionsMain == null) return isAllInclusizeTabShow;

        if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                .CIRCLE_RESOURCE_TYPE_VOICE)) {
            if (subscriptionsMain.getVoiceInclusiveOffers() != null
                    && subscriptionsMain.getVoiceInclusiveOffers().getOffers() != null
                    && subscriptionsMain.getVoiceInclusiveOffers().getOffers().size() > 0) {
                isAllInclusizeTabShow = true;
            }
        } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                .CIRCLE_RESOURCE_TYPE_DATA)) {
            if (subscriptionsMain.getInternetInclusiveOffers() != null
                    && subscriptionsMain.getInternetInclusiveOffers().getOffers() != null
                    && subscriptionsMain.getInternetInclusiveOffers().getOffers().size() > 0) {
                isAllInclusizeTabShow = true;
            }
        } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                .CIRCLE_RESOURCE_TYPE_SMS)) {
            if (subscriptionsMain.getSmsInclusiveOffers() != null
                    && subscriptionsMain.getSmsInclusiveOffers().getOffers() != null
                    && subscriptionsMain.getSmsInclusiveOffers().getOffers().size() > 0) {
                isAllInclusizeTabShow = true;
            }
        }

        return isAllInclusizeTabShow;
    }

    private void iniPaggerView(SubscriptionsMain subscriptionsMain) {
        if (subscriptionsMain == null) return;

        SubscriptionCategory subscriptionCategoryTemp = new SubscriptionCategory();
        subscriptionCategoryTemp.setOffers(new ArrayList<Subscriptions>());
        if (subscriptionsMain.getCall() == null) {
            subscriptionsMain.setCall(subscriptionCategoryTemp);
        }
        if (subscriptionsMain.getInternet() == null) {
            subscriptionsMain.setInternet(subscriptionCategoryTemp);
        }
        if (subscriptionsMain.getSms() == null) {
            subscriptionsMain.setSms(subscriptionCategoryTemp);
        }
        if (subscriptionsMain.getCampaign() == null) {
            subscriptionsMain.setCampaign(subscriptionCategoryTemp);
        }
        if (subscriptionsMain.getTm() == null) {
            subscriptionsMain.setTm(subscriptionCategoryTemp);
        }
        if (subscriptionsMain.getHybrid() == null) {
            subscriptionsMain.setHybrid(subscriptionCategoryTemp);
        }
        if (subscriptionsMain.getRoaming() == null) {
            subscriptionsMain.setRoaming(subscriptionCategoryTemp);
        }

        if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                .CIRCLE_RESOURCE_TYPE_VOICE)) {
            if (subscriptionsMain.getVoiceInclusiveOffers() == null) {
                subscriptionsMain.setVoiceInclusiveOffers(subscriptionCategoryTemp);
            }
        } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                .CIRCLE_RESOURCE_TYPE_DATA)) {
            if (subscriptionsMain.getInternetInclusiveOffers() == null) {
                subscriptionsMain.setInternetInclusiveOffers(subscriptionCategoryTemp);
            }
        } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                .CIRCLE_RESOURCE_TYPE_SMS)) {
            if (subscriptionsMain.getSmsInclusiveOffers() == null) {
                subscriptionsMain.setSmsInclusiveOffers(subscriptionCategoryTemp);
            }
        }

        try {
            ArrayList<Fragment> paggerFragmentsList = new ArrayList<>();
            if (binding.tabLayout != null && binding.tabLayout.getTabCount() > 0) {
                for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {

                    if (getString(R.string.supplementary_offer_tab_title_call).equalsIgnoreCase(getCurrentSelectedTab(i)) && subscriptionsMain.getCall() != null
                            && subscriptionsMain.getCall().getOffers() != null
                    ) {
                        paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                subscriptionsMain.getCall().getOffers(), ""));
                    } else if (getString(R.string.supplementary_offer_tab_title_internet).equalsIgnoreCase(getCurrentSelectedTab(i))
                            && subscriptionsMain.getInternet() != null
                            && subscriptionsMain.getInternet().getOffers() != null) {
                        paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                subscriptionsMain.getInternet().getOffers(), ""));
                    } else if (getString(R.string.supplementary_offer_tab_title_sms).equalsIgnoreCase(getCurrentSelectedTab(i))
                            && subscriptionsMain.getSms() != null
                            && subscriptionsMain.getSms().getOffers() != null) {
                        paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                subscriptionsMain.getSms().getOffers(), ""));
                    }
                    //TODO Temporary Commented by client
//                    else if (getString(R.string.supplementary_offer_tab_title_campaign).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
//                            subscriptionsMain.getCampaign() != null &&
//                            subscriptionsMain.getCampaign().getOffers() != null) {
//                        paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
//                                subscriptionsMain.getCampaign().getOffers()));
//                    }
                    else if (getString(R.string.supplementary_offer_tab_title_tm).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
                            subscriptionsMain.getTm() != null &&
                            subscriptionsMain.getTm().getOffers() != null) {
                        paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                subscriptionsMain.getTm().getOffers(), ""));
                    } else if (getString(R.string.supplementary_offer_tab_title_hybrid).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
                            subscriptionsMain.getHybrid() != null &&
                            subscriptionsMain.getHybrid().getOffers() != null) {
                        paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                subscriptionsMain.getHybrid().getOffers(), ""));
                    } else if (getString(R.string.supplementary_offer_tab_title_roaming).equalsIgnoreCase(getCurrentSelectedTab(i)) &&
                            subscriptionsMain.getRoaming() != null && subscriptionsMain.getRoaming().getOffers() != null) {
                        paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i), subscriptionsMain.getRoaming().getOffers(), ""));
                    } else if (getString(R.string.subscriptions_all_inclusive).equalsIgnoreCase(getCurrentSelectedTab(i))) {

                        if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                                .CIRCLE_RESOURCE_TYPE_VOICE)) {
                            if (subscriptionsMain.getVoiceInclusiveOffers() != null && subscriptionsMain.getVoiceInclusiveOffers().getOffers() != null) {
                                paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                        subscriptionsMain.getVoiceInclusiveOffers().getOffers(), MY_SUBSCRIPTIONS_FREE_USAGE_CALL));
                            }
                        } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                                .CIRCLE_RESOURCE_TYPE_DATA)) {
                            if (subscriptionsMain.getInternetInclusiveOffers() != null && subscriptionsMain.getInternetInclusiveOffers().getOffers() != null) {
                                paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                        subscriptionsMain.getInternetInclusiveOffers().getOffers(), MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET));
                            }
                        } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                                .CIRCLE_RESOURCE_TYPE_SMS)) {
                            if (subscriptionsMain.getSmsInclusiveOffers() != null && subscriptionsMain.getSmsInclusiveOffers().getOffers() != null) {
                                paggerFragmentsList.add(MySubscriptionsPaggerFragment.getInstance(getCurrentSelectedTab(i),
                                        subscriptionsMain.getSmsInclusiveOffers().getOffers(), MY_SUBSCRIPTIONS_FREE_USAGE_SMS));
                            }
                        }


                    }
                }
            }

            if (paggerFragmentsList != null) {
                binding.viewPager.setAdapter(new PagerAdapterOffers(getSupportFragmentManager(), paggerFragmentsList));
                if (isFromDashboardUsage) {//  The check is for if THERE IS any offer in all incusive list
                    lastSelectedTab = (binding.tabLayout.getTabCount() - 1);
                    if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                            .CIRCLE_RESOURCE_TYPE_VOICE)) {
                        if (subscriptionsMain.getVoiceInclusiveOffers().getOffers().size() == 0) {
                            lastSelectedTab = 0;
                        }
                    } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                            .CIRCLE_RESOURCE_TYPE_DATA)) {
                        if (subscriptionsMain.getInternetInclusiveOffers().getOffers().size() == 0) {
                            lastSelectedTab = 1;
                        }
                    } else if (tabOpenedFromDashboard.equalsIgnoreCase(DashboardFragment
                            .CIRCLE_RESOURCE_TYPE_SMS)) {
                        if (subscriptionsMain.getSmsInclusiveOffers().getOffers().size() == 0) {
                            lastSelectedTab = 2;
                        }
                    }
                }
                if (isFromDashboard) {// These checks for if there is no data in Current selected tab and then redirect to hybrid Tab
                    if (lastSelectedTab == 0 && subscriptionsMain.getCall().getOffers().size() == 0) {
                        lastSelectedTab = 3;
                    } else if (lastSelectedTab == 1 && subscriptionsMain.getInternet().getOffers().size() == 0) {
                        lastSelectedTab = 3;
                    } else if (lastSelectedTab == 2 && subscriptionsMain.getSms().getOffers().size() == 0) {
                        lastSelectedTab = 3;
                    } else if (lastSelectedTab == 4 && subscriptionsMain.getRoaming().getOffers().size() == 0) {
                        lastSelectedTab = 3;
                    }
                }

                binding.viewPager.setCurrentItem(lastSelectedTab);

            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
        }
    }


    public void requestMySubscriptions() {
        if (DataManager.getInstance().getCurrentUser() == null) {
            Toast.makeText(MySubscriptionsActivity.this, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestMySubscriptionsService.newInstance(MySubscriptionsActivity.this,
                ServiceIDs.REQUEST_MY_SUBSCRIPTIONS).execute(DataManager.getInstance().getCurrentUser()
                , new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        RootValues.getInstance().setMySubscriptionsApiCall(false);

                        String result = response.getData();

                        if (result != null) {
                            subscriptionsMain = new Gson().fromJson(result, SubscriptionsMain.class);
                        }

                        if (subscriptionsMain != null) {
                            loadMySubscriptionsUI(subscriptionsMain);
                        }

                    }//end of onSuccess


                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(MySubscriptionsActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(MySubscriptionsActivity.this);
                            return;
                        }

                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getMySubscriptionsCacheKey(getApplicationContext());
                        String cacheResponse = EaseCacheManager.get(MySubscriptionsActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                subscriptionsMain = new Gson().fromJson(cacheData, SubscriptionsMain.class);
                                if (subscriptionsMain != null) {
                                    loadMySubscriptionsUI(subscriptionsMain);
                                }
                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(MySubscriptionsActivity.this,
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }


                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {

                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getMySubscriptionsCacheKey(getApplicationContext());
                        String cacheResponse = EaseCacheManager.get(MySubscriptionsActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                subscriptionsMain = new Gson().fromJson(cacheData, SubscriptionsMain.class);
                                if (subscriptionsMain != null) {
                                    loadMySubscriptionsUI(subscriptionsMain);
                                }
                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        BakcellPopUpDialog.ApiFailureMessage(MySubscriptionsActivity.this);
                    }//end of onError
                });

    }


    public void loadMySubscriptionsUI(SubscriptionsMain subscriptionsMain) {
        if (subscriptionsMain == null) return;

        initUITabsAndPagger(subscriptionsMain);

    }


    public String getCurrentSelectedTab(int index) {
        TextView tv = (TextView) (((LinearLayout) ((LinearLayout) binding.tabLayout.getChildAt(0))
                .getChildAt(index)).getChildAt(1));
        String tabTitle = "";
        if (tv != null) {
            tabTitle = tv.getText().toString();
        }
        return tabTitle;
    }


}
