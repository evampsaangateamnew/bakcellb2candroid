package com.bakcell.activities.landing;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;

/**
 * Created by Zeeshan Shabbir on 8/7/2017.
 */

public class VideoTutorialActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_tutorial);
    }
}
