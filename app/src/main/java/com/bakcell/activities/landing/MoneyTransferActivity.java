package com.bakcell.activities.landing;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.res.Resources;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.AdapterMoneyTransfer;
import com.bakcell.adapters.SelectAmountAdapter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityMoneyTransferBinding;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.pin.DataPin;
import com.bakcell.models.topup.Topup;
import com.bakcell.models.user.UserPredefinedData;
import com.bakcell.models.user.userpredefinedobjects.PredefinedDataItem;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestHistoryResendPinService;
import com.bakcell.webservices.RequestMoneyTransferService;
import com.bakcell.webservices.RequestVerifyPinService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellEditText;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;


public class MoneyTransferActivity extends BaseActivity implements View.OnClickListener {

    ActivityMoneyTransferBinding binding;

    String[] spinnerAmout;

    SelectAmountAdapter spinnerAdapaterLoan;

    private double transferAmount = -1;
    private String msisdnTo = "";
    private AlertDialog otpDialog = null;

    @Override
    protected void onResume() {
        super.onResume();
//        showOTPDialog();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_money_transfer);

        initUiContent();

        initListener();

        initRecycler();

    }

    @SuppressLint("InflateParams")
    private void showOTPDialog() {
        if (MoneyTransferActivity.this.isFinishing()) {
            return; //safe passage
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MoneyTransferActivity.this);
        LayoutInflater inflater = MoneyTransferActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.money_transfer_otp_dialog, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();//go back
            }
        });

        otpDialog = dialogBuilder.create();
        otpDialog.setCancelable(false);
        otpDialog.setCanceledOnTouchOutside(true);
        otpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        otpDialog.show();
        RelativeLayout resendLayout = dialogView.findViewById(R.id.resendLayout);
        final BakcellEditText pinInput = dialogView.findViewById(R.id.pinInput);
        BakcellButtonNormal buttonSubmit = dialogView.findViewById(R.id.buttonSubmit);

        resendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestForResendPin();
            }
        });


        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String otp = pinInput.getText().toString();
                if (Tools.hasValue(otp)) {
                    if (otp.length() == 4) {
                        //verify the otp
                        requestForVerifyAccount(otp);
                    } else {
                        BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this, getString(R.string.bakcell_error_title), getString(R.string.msg_invalid_passport_number));
                    }
                } else {
                    if (!MoneyTransferActivity.this.isFinishing()) {
                        BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this, getString(R.string.bakcell_error_title), getString(R.string.msg_please_enter_pin));
                    }
                }
            }
        });


        requestForResendPin();
    }//showFingerPrintScannerDialog ends

    public void requestForVerifyAccount(String otp) {
        RequestVerifyPinService.newInstance(MoneyTransferActivity.this,
                ServiceIDs.REQUEST_MONEY_TRANSFER_OTP_VERIFY)
                .execute(DataManager.getInstance().getCurrentUser(), otp, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        DataPin dataPin = new Gson().fromJson(result, DataPin.class);
                        dismissOTPPopUp();
                        requestForMoneyTransfer(msisdnTo, transferAmount + "");
                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (!MoneyTransferActivity.this.isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(MoneyTransferActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(MoneyTransferActivity.this);
                            return;
                        }

                        if (!MoneyTransferActivity.this.isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this, getString(R.string.bakcell_error_title), response.getDescription());
                        }

                        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_DETAIL_PASSPORT_VERIFICATION_FAILURE,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN);

                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (!MoneyTransferActivity.this.isFinishing()) {
                            BakcellPopUpDialog.ApiFailureMessage(MoneyTransferActivity.this);
                        }
                    }

                });

    }

    /**
     * Request for Resend Pin
     */
    private void requestForResendPin() {

        if (DataManager.getInstance().getCurrentUser() == null) {
            Toast.makeText(getApplicationContext(), R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestHistoryResendPinService.newInstance(MoneyTransferActivity.this,
                ServiceIDs.REQUEST_OTP_RESENT_MONEY_TRANSFER).
                execute(DataManager.getInstance().getCurrentUser(), EndPoints.Constants.CAUSE_MONEY_TRANSFER,
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {


                                String result = response.getData();

                                DataPin dataPin = new Gson().fromJson(result, DataPin.class);

                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (!MoneyTransferActivity.this.isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(MoneyTransferActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(MoneyTransferActivity.this);
                                    return;
                                }

                                if (!MoneyTransferActivity.this.isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this,
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (!MoneyTransferActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(MoneyTransferActivity.this);
                                }
                            }
                        });
    }

    private void dismissOTPPopUp() {
        if (otpDialog != null) {
            if (otpDialog.isShowing()) {
                otpDialog.dismiss();
            }
        }
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rcMoneyTransfer.setLayoutManager(layoutManager);
        
        if (DataManager.getInstance().getUserPredefinedData() != null &&
                DataManager.getInstance().getUserPredefinedData().getTopup() != null &&
                DataManager.getInstance().getUserPredefinedData().getTopup().getMoneyTransfer() != null &&
                DataManager.getInstance().getUserPredefinedData().getTopup().getMoneyTransfer().getTermsAndCondition() != null) {

            ArrayList<PredefinedDataItem> termsAndConditionsCin = DataManager.getInstance().getUserPredefinedData().getTopup().getMoneyTransfer().getTermsAndCondition().getCin();
            ArrayList<PredefinedDataItem> termsAndConditionsKlass = DataManager.getInstance().getUserPredefinedData().getTopup().getMoneyTransfer().getTermsAndCondition().getKlass();

            if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null &&
                    DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
                if (termsAndConditionsCin != null) {
                    binding.rcMoneyTransfer.setAdapter(new AdapterMoneyTransfer(termsAndConditionsCin));
                }
            } else {
                if (termsAndConditionsKlass != null) {
                    binding.rcMoneyTransfer.setAdapter(new AdapterMoneyTransfer(termsAndConditionsKlass));
                }
            }

        }


        binding.rcMoneyTransfer.setNestedScrollingEnabled(false);
    }

    private void initUiContent() {
        populateArrayForSpinner();
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setText(R.string.activity_money_transfer_lbl);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        spinnerAdapaterLoan = new SelectAmountAdapter(this, spinnerAmout);
        binding.spSelectAmount.setAdapter(spinnerAdapaterLoan);

        if (spinnerAmout != null && spinnerAmout.length == 2) {
            binding.spSelectAmount.setSelection(1);
            binding.spSelectAmount.setEnabled(false);
        } else {
            binding.spSelectAmount.setSelection(0);
            binding.spSelectAmount.setEnabled(true);
        }
    }

    private void populateArrayForSpinner() {
        if (DataManager.getInstance().getUserPredefinedData() != null) {
            UserPredefinedData userPredefinedData = DataManager.getInstance().getUserPredefinedData();
            if (userPredefinedData.getTopup() != null &&
                    userPredefinedData.getTopup().getMoneyTransfer() != null &&
                    userPredefinedData.getTopup().getMoneyTransfer().getSelectAmount() != null) {

                ArrayList<PredefinedDataItem> selectAmountList = new ArrayList<>();

                if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null &&
                        DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
                    selectAmountList = userPredefinedData.getTopup().getMoneyTransfer().getSelectAmount().getCin();
                } else {
                    selectAmountList = userPredefinedData.getTopup().getMoneyTransfer().getSelectAmount().getKlass();
                }

                if (selectAmountList != null && selectAmountList.size() > 0) {
                    spinnerAmout = new String[selectAmountList.size() + 1];
                    spinnerAmout[0] = getString(R.string.spinner_label_select_all);
                    for (int i = 0; i < selectAmountList.size(); i++) {
                        spinnerAmout[i + 1] = selectAmountList.get(i).getAmount();
                    }
                }

            }
        }
    }

    protected void moneyTransferClicked(final String msisdnTo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_top_up_confirmation, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button yesBtn = dialogView.findViewById(R.id.btn_yes);
        BakcellTextViewNormal currentBalance = dialogView.findViewById(R.id.currentBalance);
        currentBalance.setText(DataManager.getInstance().getCurrentBalance() + "");

        Button noBtn = dialogView.findViewById(R.id.btn_no);
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        BakcellTextViewNormal tvConfirmationMsg = dialogView.findViewById(R.id.tv_top_confirmation_msg);
        String confirmationStr = getString(R.string.money_transfer_confirmation_message, transferAmount + "", msisdnTo);
        tvConfirmationMsg.setText(Html.fromHtml(confirmationStr));

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showOTPDialog();

                alertDialog.dismiss();
            }
        });


    }

    private void showSuccessfulDialog(String newBalance, String oldBalance) {
        if (this.isFinishing()) return;

        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_top_up_successful, null);
            dialogBuilder.setView(dialogView);

            DataManager.getInstance().setNewBalance(newBalance);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button okayBtn = dialogView.findViewById(R.id.btn_okay);

            BakcellTextViewNormal tvBalnce = dialogView.findViewById(R.id.tvBalnce);

            okayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            TextView textView = dialogView.findViewById(R.id.tv_top_confirmation_msg);
            String confirmationStr = getResources().getString(R.string.money_transfered_message, transferAmount + "", msisdnTo);
            textView.setText(Html.fromHtml(confirmationStr));

            tvBalnce.setText(newBalance + "");
        } catch (Resources.NotFoundException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    private void initListener() {
        binding.btnTransfer.setOnClickListener(this);
        binding.btnTransfer.setSelected(true);
        binding.toolbar.hamburgerMenu.setOnClickListener(this);

        binding.spSelectAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerAdapaterLoan.setLastSelectedPos(position);
                spinnerAdapaterLoan.notifyDataSetChanged();
                spinnerAdapaterLoan.setOpened(false);

                transferAmount = -1;

                if (position != 0) selectAmount(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spSelectAmount.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                spinnerAdapaterLoan.setOpened(true);
                return false;
            }
        });

    }

    private void selectAmount(int position) {

        if (position < 1 && spinnerAmout.length > position) {
            BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this, getString(R.string.bakcell_error_title), getString(R.string.msg_select_amount));
            return;
        }

        String amountStr = spinnerAmout[position];

        if (Tools.hasValue(amountStr) && Tools.isNumeric(amountStr)) {
            transferAmount = Tools.getDoubleFromString(amountStr);
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.btn_transfer:
                if (binding.mobileInput.getText().toString().trim().length() == 0) {
                    BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_top_up_enter_mobile_number_message));
                } else {
                    if (binding.mobileInput.getText().toString().trim().length() != 9) {
                        BakcellPopUpDialog.showMessageDialog(this,
                                getString(R.string.bakcell_error_title),
                                getString(R.string.error_msg_invalid_number));
                        return;
                    }
                    if (((int) transferAmount) == -1) {
                        BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this,
                                getString(R.string.bakcell_error_title), getString(R.string.msg_select_amount));
                        return;
                    }
                    msisdnTo = binding.mobileInput.getText().toString();
                    if (Tools.hasValue(msisdnTo)) {
                        moneyTransferClicked(msisdnTo);
                    }
                }
                break;
        }
    }


    public void requestForMoneyTransfer(String msisdnTo, String amount) {


        RequestMoneyTransferService.newInstance(MoneyTransferActivity.this, ServiceIDs.REQUEST_MONEY_TRANSFER).execute(DataManager.getInstance().getCurrentUser(), msisdnTo, amount, new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                String result = response.getData();

                Topup topup = new Gson().fromJson(result, Topup.class);

                if (topup != null) {
                    inAppFeedback(topup);
                }

                RootValues.getInstance().setDashboardApiCall(true);

                AppEventLogs.applyAppEvent(AppEventLogValues.MoneyTransferEvents.MONEY_TRANSFER_SCREEN,
                        AppEventLogValues.MoneyTransferEvents.MONEY_TRANSFER_SUCCESS,
                        AppEventLogValues.MoneyTransferEvents.MONEY_TRANSFER_SCREEN);

            }


            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                if (response != null) {
                    BakcellPopUpDialog.showMessageDialog(MoneyTransferActivity.this,
                            getString(R.string.bakcell_error_title), response.getDescription());
                }
                AppEventLogs.applyAppEvent(AppEventLogValues.MoneyTransferEvents.MONEY_TRANSFER_SCREEN,
                        AppEventLogValues.MoneyTransferEvents.MONEY_TRANSFER_FAILURE,
                        AppEventLogValues.MoneyTransferEvents.MONEY_TRANSFER_SCREEN);
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                BakcellPopUpDialog.ApiFailureMessage(MoneyTransferActivity.this);
            }

        });

    }
    private void inAppFeedback(Topup topup) {
        int currentVisit = PrefUtils.getAsInt(MoneyTransferActivity.this, PrefUtils.PreKeywords.PREF_TRANSFER_MONEY);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(MoneyTransferActivity.this);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TRANSFER_MONEY_PAGE);
            if (surveys != null) {
                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                        InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(MoneyTransferActivity.this);
                        if (surveys.getQuestions() != null) {
                            currentVisit = 0;
                            String confirmationStr = getResources().getString(R.string.money_transfered_message, transferAmount + "", msisdnTo);
                            Spanned message = Html.fromHtml(confirmationStr);
                            inAppFeedbackDialog.showSpannableMessageDialog(surveys, topup.getNewBalance(), message);
                        } else {
                            showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
                        }
                    } else {
                        showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
                    }
                } else {
                    showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
                }
            } else {
                showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
            }
        } else {
            showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
        }
        PrefUtils.addInt(MoneyTransferActivity.this, PrefUtils.PreKeywords.PREF_TRANSFER_MONEY, currentVisit);
    }
}
