package com.bakcell.activities.landing;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.RoamingCountryAdapter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityRoamingCountrySearchBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.CallMySubscriptionFromOffersListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.mysubscriptions.SubscriptionsMain;
import com.bakcell.models.supplementaryoffers.RoamingsOffer;
import com.bakcell.models.supplementaryoffers.SupplementaryOfferMain;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.GetSupplementaryOffersService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.webservices.subscribeunsubscribe.SubscribeUnsubscribe;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class RoamingCountrySearchActivity extends BaseActivity implements View.OnClickListener {
    ActivityRoamingCountrySearchBinding binding;
    private RoamingsOffer roamingsOffer;
    private SupplementaryOfferMain offerMain;
    private String selectedCountry;
    private ArrayList<SupplementaryOffer> filteredRoamingOffers;
    ArrayList<String> countryListSearch;
    private Toast toast;
    private String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_roaming_country_search);
        initUiContent();
        initUiListeners();
        offerMain = DataManager.getInstance().getSupplementaryOfferMainData();
        if (offerMain == null) {
            requestForGetSupplementaryOffers();
        } else {
            loadUIAfterApi(offerMain);
            callMySubscriptionsData();
        }
        AppEventLogs.applyAppEvent(AppEventLogValues.RoamingScreen.ROAMING_PACKAGES_SCREEN,
                AppEventLogValues.RoamingScreen.ROAMING_PACKAGES_SCREEN,
                AppEventLogValues.RoamingScreen.ROAMING_PACKAGES_SCREEN);

    }

    private void initUiListeners() {
        binding.btnRoaming.setOnClickListener(this);
    }

    private void initUiContent() {

        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(getResources().getString(R.string.lbl_packages));

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_roaming:
                prepareForRoamingActivity();
                break;

        }
    }

    private void prepareForRoamingActivity() {
        selectedCountry = binding.allCountiesInout.getText().toString();
        if (selectedCountry.trim().length() == 0) {
            if (toast != null) {
                toast.cancel();
            }
            if (!this.isFinishing()) {
                toast = Toast.makeText(this, R.string.activity_roaming_select_country_lbl,
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        } else {
            startRoamingActivity();
        }
    }

    private void startRoamingActivity() {
        flag = "";

        if (selectedCountry != null && roamingsOffer != null) {
            if (roamingsOffer.getOffers() != null) {
                if (roamingsOffer.getCountries() != null && roamingsOffer.getOffers().size() > 0) {
                    filteredRoamingOffers = new ArrayList<>();
                    for (int i = 0; i < roamingsOffer.getOffers().size(); i++) {
                        if (roamingsOffer.getOffers().get(i).getDetails() != null) {
                            if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails() != null) {
                                if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails()
                                        .getRoamingDetailsCountriesList() != null &&
                                        roamingsOffer.getOffers().get(i).getDetails()
                                                .getRoamingDetails().getRoamingDetailsCountriesList().size() > 0) {
                                    for (int j = 0; j < roamingsOffer.getOffers().get(i).getDetails()
                                            .getRoamingDetails().getRoamingDetailsCountriesList().size(); j++) {
                                        if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails()
                                                .getRoamingDetailsCountriesList().get(j).getCountryName().trim()
                                                .equalsIgnoreCase(selectedCountry.trim())) {

                                            if (!Tools.hasValue(flag))
                                                flag = roamingsOffer.getOffers().get(i).getDetails()
                                                        .getRoamingDetails()
                                                        .getRoamingDetailsCountriesList().get(j).getFlag();
                                            filteredRoamingOffers.add(roamingsOffer.getOffers().get(i));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (filteredRoamingOffers != null && filteredRoamingOffers.size() > 0) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(RoamingOfferActivity.KEY_ROAMING_OFFERS,
                        filteredRoamingOffers);
                if (roamingsOffer.getFilters() != null)
                    bundle.putParcelable(RoamingOfferActivity.KEY_ROAMING_FILTERS,
                            roamingsOffer.getFilters());
                bundle.putString(RoamingOfferActivity.KEY_COUNTRY_NAME, binding.allCountiesInout
                        .getText().toString());
                bundle.putString(RoamingOfferActivity.KEY_FLAG, flag);
                BaseActivity.startNewActivity(RoamingCountrySearchActivity.this, RoamingOfferActivity.class, bundle);
            } else {
                try {
                    if (toast != null) {
                        toast.cancel();
                    }
                    if (RoamingCountrySearchActivity.this != null && !RoamingCountrySearchActivity.this.isFinishing()) {
                        toast = Toast.makeText(RoamingCountrySearchActivity.this, R.string.activity_roaming_no_offers_found_lbl, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } catch (Exception ex) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                }
            }

        }
    }

    private void requestForGetSupplementaryOffers() {

        UserModel customerData = DataManager.getInstance().getCurrentUser();

        if (customerData == null) {
            Toast.makeText(RoamingCountrySearchActivity.this, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        GetSupplementaryOffersService.newInstance(RoamingCountrySearchActivity.this,
                ServiceIDs.GET_SUPPLEMENTARY_OFFERS).execute(customerData,
                new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request,
                                          EaseResponse<String> response) {
                        RootValues.getInstance().setSupplementaryOffersApiCall(false);
                        try {
                            String result = response.getData();
                            offerMain = new Gson().fromJson(result, SupplementaryOfferMain.class);
                            if (offerMain != null) {
                                loadUIAfterApi(offerMain);
                            }
                            callMySubscriptionsData();
                        } catch (JsonSyntaxException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }

                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request,
                                          EaseResponse<String> response) {
                        // Check if user logout by server
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(RoamingCountrySearchActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(RoamingCountrySearchActivity.this);
                            return;
                        }

                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getApplicationContext());
                        String cacheResponse = EaseCacheManager.get(RoamingCountrySearchActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                if (offerMain != null) {
                                    loadUIAfterApi(offerMain);
                                }
                                callMySubscriptionsData();

                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(RoamingCountrySearchActivity.this,
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getApplicationContext());
                        String cacheResponse = EaseCacheManager.get(RoamingCountrySearchActivity.this, cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                if (offerMain != null) {
                                    loadUIAfterApi(offerMain);
                                }
                                callMySubscriptionsData();
                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        BakcellPopUpDialog.ApiFailureMessage(RoamingCountrySearchActivity.this);
                    }
                });

    }

    private void loadUIAfterApi(SupplementaryOfferMain offerMain) {
        roamingsOffer = offerMain.getRoaming();
        if (roamingsOffer == null) return;
        if (roamingsOffer.getCountries() == null) return;
        countryListSearch = new ArrayList<>();
        for (int i = 0; i < roamingsOffer.getCountries().size(); i++) {
            countryListSearch.add(roamingsOffer.getCountries().get(i).getName());
        }
        RoamingCountryAdapter countryAdapter = new RoamingCountryAdapter(this, binding.allCountiesInout.getId(), countryListSearch);
        binding.allCountiesInout.setAdapter(countryAdapter);

        binding.allCountiesInout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.allCountiesInout.showDropDown();
            }
        });

        binding.allCountiesInout.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    prepareForRoamingActivity();
                    return true;
                }
                return false;
            }
        });

    }


    private void callMySubscriptionsData() {
        if (RoamingCountrySearchActivity.this == null) return; //safe passege
        RootValues.getInstance().setSubscriptionsMain(null);
        SubscribeUnsubscribe.requestMySubscriptionsFromOffer(RoamingCountrySearchActivity.this, new CallMySubscriptionFromOffersListener() {
            @Override
            public void onCheckMySubscriptionListener(SubscriptionsMain subscriptionsMain) {
                RootValues.getInstance().setSubscriptionsMain(subscriptionsMain);
            }
        });

    }

}