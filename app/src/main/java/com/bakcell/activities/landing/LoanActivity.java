package com.bakcell.activities.landing;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityLoanBinding;
import com.bakcell.fragments.pager.LoanPaggerFragment;
import com.bakcell.fragments.pager.PaymentHistoryPaggerFragment;
import com.bakcell.fragments.pager.RequestHistoryPaggerFragment;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;


public class LoanActivity extends BaseActivity implements View.OnClickListener {

    ActivityLoanBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_loan);

        initUiContent();

        initUiListeners();

        initUITabsAndPagger();

        AppEventLogs.applyAppEvent(AppEventLogValues.LoanEvents.LOAN_SCREEN,
                AppEventLogValues.LoanEvents.LOAN_SCREEN,
                AppEventLogValues.LoanEvents.LOAN_SCREEN);
    }

    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
    }

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));

        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getBrandName())
                && DataManager.getInstance().getCurrentUser().getBrandName()
                .equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
            binding.toolbar.pageTitle.setText(R.string.activity_loan_cin_lbl);
        } else if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getBrandName())
                && DataManager.getInstance().getCurrentUser().getBrandName()
                .equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_KLASS)) {
            binding.toolbar.pageTitle.setText(R.string.activity_loan_klass_lbl);
        } else {
            binding.toolbar.pageTitle.setText(R.string.activity_loan_lbl);
        }

    }

    private void initUITabsAndPagger() {

        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getBrandName()) //fix NPE
                && DataManager.getInstance().getCurrentUser().getBrandName()
                .equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.activity_loan_cin_lbl));
        } else if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getBrandName()) //fix NPE
                && DataManager.getInstance().getCurrentUser().getBrandName()
                .equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_KLASS)) {
            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.activity_loan_klass_lbl));
        } else {
            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.activity_loan_lbl));
        }
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.loan_activity_payment_request_tab_lbl));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.loan_activity_payment_history_tab_lbl));

        iniPaggerView();

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (10 * getResources().getDisplayMetrics().density);
            p.setMargins(margin, 0, margin, 0);
            tab.requestLayout();
        }

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void iniPaggerView() {

        ArrayList<Fragment> fragmentsList = new ArrayList<>();

        fragmentsList.add(new LoanPaggerFragment());
        fragmentsList.add(new RequestHistoryPaggerFragment());
        fragmentsList.add(new PaymentHistoryPaggerFragment());
        binding.viewPager.setAdapter(new PagerAdapterOffers(getSupportFragmentManager(), fragmentsList));
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
        }
    }

}
