package com.bakcell.activities.landing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.AdapterUlduzumMerchantDetails;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityUlduzumMerchantDetailsBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.models.DataManager;
import com.bakcell.models.coreservices.CoreServices;
import com.bakcell.models.ulduzumdetails.BranchList;
import com.bakcell.models.ulduzumdetails.MerchantModel;
import com.bakcell.models.ulduzumdetails.UlduzumDetailsModel;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.webservices.RequestGetCoreServices;
import com.bakcell.webservices.RequestUlduzumDetailsWebService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class UlduzumMerchantDetailsActivity extends BaseActivity implements View.OnClickListener  {
    ActivityUlduzumMerchantDetailsBinding binding;
    AdapterUlduzumMerchantDetails adapter;
    MerchantModel merchantModel;
    String merchantId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_ulduzum_merchant_details);
        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey("merchantId")) {
            merchantId = getIntent().getExtras().getString("merchantId");
        }
        initUiContent();
        initUiListeners();
        requestMerchantDetailApi();
        AppEventLogs.applyAppEvent(AppEventLogValues.UlduzumScreen.ULDUZUM_MERCHANT_DETAILS_SCREEN,
                AppEventLogValues.UlduzumScreen.ULDUZUM_MERCHANT_DETAILS_SCREEN,
                AppEventLogValues.UlduzumScreen.ULDUZUM_MERCHANT_DETAILS_SCREEN);
    }

    private void requestMerchantDetailApi() {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestUlduzumDetailsWebService.newInstance(UlduzumMerchantDetailsActivity.this, ServiceIDs.REQUEST_CORE_SERVICES)
                    .execute(userModel,merchantId, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            try{
                                String result = response.getData();
                                UlduzumDetailsModel model= new Gson().fromJson(result, UlduzumDetailsModel.class);
                                if (model.getMerchantModel() != null) {
                                    merchantModel = model.getMerchantModel();
                                    updateUI(model.getMerchantModel());
                                }
                            }
                            catch (JsonSyntaxException e) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {

                            // Check if user logout by server
                            if (!UlduzumMerchantDetailsActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(UlduzumMerchantDetailsActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(UlduzumMerchantDetailsActivity.this);
                                return;
                            }

                            if (!UlduzumMerchantDetailsActivity.this.isFinishing() && response != null) {
                                BakcellPopUpDialog.showMessageDialog(UlduzumMerchantDetailsActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(UlduzumMerchantDetailsActivity.this);
                        }
                    });
        }

    }

    private void updateUI(MerchantModel merchantModel) {
        if(merchantModel.getName()!=null)
        binding.txtViewName.setText(merchantModel.getName());
        if(merchantModel.getDiscount()!=null){
            String loyality =merchantModel.getDiscount();
            if (loyality!=null && loyality.matches("[0-9]+")) {
                loyality +="%";
                binding.txtViewDiscount.setText(loyality);
                binding.giftPackIcon.setVisibility(View.GONE);
            } else {
                binding.txtViewDiscount.setVisibility(View.GONE);
                binding.giftPackIcon.setVisibility(View.VISIBLE);
            }

        }

        if(merchantModel.getBranches()!=null && merchantModel.getBranches().size()>0)
        {
            binding.txtNoBranchesFound.setVisibility(View.GONE);
            setUpRecyclerView(merchantModel.getBranches());
        } else {
            binding.recyclerview.setVisibility(View.GONE);

        }
    }

    private void setUpRecyclerView(ArrayList<BranchList> branches)
    {
        adapter = new AdapterUlduzumMerchantDetails(branches);
        binding.recyclerview.setAdapter(adapter);
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(this));
        adapter.notifyDataSetChanged();
    }

    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);

    }

    private void initUiContent() {
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(getString(R.string.ulduzum_merchent_details_lbl));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
        }
    }
}