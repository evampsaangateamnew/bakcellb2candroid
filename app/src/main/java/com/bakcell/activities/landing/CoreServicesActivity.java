package com.bakcell.activities.landing;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.CardAdapterCoreService;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityCoreServicesBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.coreservices.CoreServices;
import com.bakcell.models.coreservices.CoreServicesList;
import com.bakcell.models.coreservices.CoreServicesMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestGetCoreServices;
import com.bakcell.webservices.RequestReportLostSim;
import com.bakcell.webservices.RequestSendInternetSettingService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

import static com.bakcell.adapters.CardAdapterCoreService.OFFERING_ID_TURNED_OFF;
import static com.bakcell.adapters.CardAdapterCoreService.SUSPENDED;
import static com.bakcell.adapters.CardAdapterCoreService.SWITCH_STATUS;

public class CoreServicesActivity extends BaseActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener {
    private static final int ADD_FORWARD_NUMBER_FOR_RESULT = 912;
    ActivityCoreServicesBinding binding;
    private AlertDialog alertDialog;
    CoreServices coreServices;
    private CardAdapterCoreService cardAdapterCoreService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_core_services);
        initUiContent();
        initListeners();
        setUserStatus();
        requestCoreService();

        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN);
    }

    private void requestCoreService() {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestGetCoreServices.newInstance(CoreServicesActivity.this, ServiceIDs.REQUEST_CORE_SERVICES)
                    .execute(userModel, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String result = response.getData();
                            coreServices = new Gson().fromJson(result, CoreServices.class);
                            if (coreServices != null && coreServices.getCoreServices() != null) {
                                loadCoreServices(coreServices.getCoreServices());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {

                            // Check if user logout by server
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(CoreServicesActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(CoreServicesActivity.this);
                                return;
                            }

                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(CoreServicesActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(CoreServicesActivity.this);
                        }
                    });
        }
    }

    private void loadCoreServices(ArrayList<CoreServicesMain> coreServices) {
        if (coreServices != null && coreServices.size() > 0) {

//            boolean isIcalledYouBackActive = false;

            /*for (int i = 0; i < coreServices.size(); i++) {
                if (Tools.hasValue(coreServices.get(i).getCoreServiceCategory())) {
                    if (coreServices.get(i).getCoreServicesList() != null) {
                        for (int j = 0; j < coreServices.get(i).getCoreServicesList().size(); j++) {
                            if (Tools.hasValue(coreServices.get(i).getCoreServicesList().get(j).getOfferingId())) {
                                if (coreServices.get(i).getCoreServicesList().get(j).getOfferingId()
                                        .equalsIgnoreCase(OFFERING_ID_I_CALLED_YOU)) {
                                    if (coreServices.get(i).getCoreServicesList().get(j).getStatus()
                                            .equalsIgnoreCase(SWITCH_STATUS)) {
                                        isIcalledYouBackActive = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }*/

            /**new checks added as per client CR on 27 August 2019*/
            boolean is_turned_off_suspended = false;
            boolean activeInActiveStatus = false;

            for (int j = 0; j < coreServices.size(); j++) {
                if (Tools.hasValue(coreServices.get(j).getCoreServiceCategory())) {
                    if (coreServices.get(j).getCoreServicesList() != null) {
                        for (int i = 0; i < coreServices.get(j).getCoreServicesList().size(); i++) {
                            if (coreServices.get(j).getCoreServicesList().get(i).getOfferingId().contentEquals(OFFERING_ID_TURNED_OFF)) {
                                //get the status
                                if (Tools.hasValue(coreServices.get(j).getCoreServicesList().get(i).getStatus())) {
                                    String status = coreServices.get(j).getCoreServicesList().get(i).getStatus();
                                    BakcellLogger.logE("staX", "status:::" + status + " " + coreServices.get(j).getCoreServicesList().get(i).getOfferingId(), "core services activity", "onBindViewHolder");
                                    if (status.equalsIgnoreCase(SUSPENDED)) {
                                        //check for suspended
                                        is_turned_off_suspended = true;
                                    } else {
                                        //check for active and inactive status
                                        if (status.equalsIgnoreCase(SWITCH_STATUS)) {
                                            //active check
                                            activeInActiveStatus = true;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            cardAdapterCoreService = new CardAdapterCoreService(callForwardListner, coreServices,
                    this, is_turned_off_suspended, activeInActiveStatus);
            binding.mRecylerview.setAdapter(cardAdapterCoreService);


        }
    }


    private void initListeners() {
        binding.btnAccountSend.setOnClickListener(this);
        binding.btnAccountSend.setSelected(true);
        binding.btnSend.setOnClickListener(this);
        binding.btnSend.setSelected(true);
        binding.toolbar.hamburgerMenu.setOnClickListener(this);

    }

    private void initUiContent() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.mRecylerview.setLayoutManager(layoutManager);
        binding.mRecylerview.setNestedScrollingEnabled(false);
        binding.btnAccountSend.setSelected(true);
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.core_services);

        /**if the customer type is corporate customer
         * that is if the customer type contains the corporate keyword
         * or if the group type is paybysub or the paybysubs
         * we need to hide the suspend number layout in that cases*/
        String groupType = "";
        String customerType = "";
        if (DataManager.getInstance().getCurrentUser() != null) {
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getCustomerType())) {
                customerType = DataManager.getInstance().getCurrentUser().getCustomerType();
                if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getGroupType())) {
                    groupType = DataManager.getInstance().getCurrentUser().getGroupType();
                }//if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getGroupType())) ends
                if (customerType.toLowerCase().contains("corporate") ||
                        groupType.equalsIgnoreCase("paybysub") ||
                        groupType.equalsIgnoreCase("paybysubs")) {
                    binding.llBlocking.setVisibility(View.GONE);
                } else {
                    binding.llBlocking.setVisibility(View.VISIBLE);
                }
            }//if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getCustomerType())) ends
        }//if (DataManager.getInstance().getCurrentUser() != null) ends
    }


    private void showLogoutAlert() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.diaog_logout_alert, null);
        dialogBuilder.setView(dialogView);

        alertDialog = dialogBuilder.create();
        alertDialog.show();
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        BakcellButtonNormal cancelBtn = dialogView.findViewById(R.id.btn_cancel);
        BakcellButtonNormal logoutBtn = dialogView.findViewById(R.id.btn_logout);
        BakcellTextViewNormal tvMessage = dialogView.findViewById(R.id.tv_pop_up_msg);
        BakcellTextViewBold tvTitle = dialogView.findViewById(R.id.tv_dialog_title);
        tvTitle.setText(getString(R.string.warning));
        logoutBtn.setText(getString(R.string.suspend));
        tvMessage.setText("Lorum ispsum dalors lorum ispsum dalors lorum ispsum dalors");

        logoutBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.btn_cancel:
                alertDialog.dismiss();
                break;
            case R.id.btn_logout: //this btn is suspend btn in this case.
                alertDialog.dismiss();
                break;
            case R.id.btn_send:
                requestSendInternetSettings();
                break;
            case R.id.btn_account_send:
                showReportLostSimAlert();
                break;
        }
    }

    private void setUserStatus() {
        if (DataManager.getInstance().getCurrentUser() != null &&
                Tools.hasValue(DataManager.getInstance().getCurrentUser().getStatus())) {
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getStatusDetails())
                    && DataManager.getInstance().getCurrentUser().getStatusDetails()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword.USER_CURRENT_STATUS_CODE)) {
                binding.btnAccountSend.setEnabled(true);
                binding.btnAccountSend.setTextColor(ContextCompat.getColor(this, R.color.black));
                binding.btnAccountSend.setText(R.string.btn_lbl_suspend);
            } else {
                binding.btnAccountSend.setEnabled(false);
                binding.btnAccountSend.setTextColor(ContextCompat.getColor(this, R.color.white));
                binding.btnAccountSend.setText(R.string.btn_lbl_suspended);
            }
        }

    }

    private void requestSendInternetSettings() {
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestSendInternetSettingService.newInstance(this, ServiceIDs.REQUEST_SEND_INTERNET_SETTINGS)
                    .execute(userModel, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String result = response.getData();
                            CoreServices coreServices = new Gson().fromJson(result, CoreServices.class);
                            if (Tools.hasValue(coreServices.getMessage())) {
                                BakcellPopUpDialog.showMessageDialog(CoreServicesActivity.this,
                                        getString(R.string.mesg_successful_title), coreServices.getMessage());

                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {

                            // Check if user logout by server
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(CoreServicesActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(CoreServicesActivity.this);
                                return;
                            }
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(CoreServicesActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(CoreServicesActivity.this);
                        }
                    });
        }
    }


    private void showReportLostSimAlert() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.diaog_logout_alert, null);
        dialogBuilder.setView(dialogView);

        alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        BakcellButtonNormal cancelBtn = dialogView.findViewById(R.id.btn_cancel);
        BakcellButtonNormal logoutBtn = dialogView.findViewById(R.id.btn_logout);
        BakcellTextViewNormal tvMessage = dialogView.findViewById(R.id.tv_pop_up_msg);
        BakcellTextViewBold tvTitle = dialogView.findViewById(R.id.tv_dialog_title);
        tvTitle.setText(getString(R.string.warning));
        logoutBtn.setText(getString(R.string.suspend));
        logoutBtn.setSelected(true);
        cancelBtn.setSelected(true);
        if (DataManager.getInstance().getCurrentUser() != null) {
            tvMessage.setText(getString(R.string.msg_are_you_sure_you_want_to_suspend) + " " + DataManager.getInstance().getCurrentUser().getMsisdn());
        }
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                requestReportLostSim();

            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }


    private void requestReportLostSim() {


        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();

            RequestReportLostSim.newInstance(this, ServiceIDs.REQUEST_REPORT_SIM_LOST)
                    .execute(userModel, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            String result = response.getData();


                            BakcellPopUpDialog.showMessageDialog(CoreServicesActivity.this,
                                    getString(R.string.mesg_successful_title), response.getDescription());


                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {
                            if (response != null) {
                                // Check if user logout by server
                                BakcellPopUpDialog.showMessageDialog(CoreServicesActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(CoreServicesActivity.this);
                        }
                    });
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            showLogoutAlert();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ADD_FORWARD_NUMBER_FOR_RESULT) {
            if (data != null) {
                String number = data.getStringExtra(AddForwardNumberActivity.FORWARD_NUMBER);
                if (Tools.hasValue(number)) {

                    cardAdapterCoreService.updateCoreServicesAfterCallForwardAdded(number);

//                    binding.tvPhoneNumber.setText(number);
//                    binding.switchPhoneSettings.setChecked(true);
//                    binding.switchPhoneSettings.setEnabled(true);
                }
            }
        }
    }

    private CoreServiceCallForwardListener callForwardListner = new CoreServiceCallForwardListener() {
        @Override
        public void onCallForwardListener(CoreServicesMain coreServicesMain, int finalCallForwardItemIndex) {
            Bundle bundle = new Bundle();
            if (coreServicesMain != null && coreServicesMain.getCoreServicesList() != null &&
                    finalCallForwardItemIndex > -1
                    && finalCallForwardItemIndex < coreServicesMain.getCoreServicesList().size()) {

                CoreServicesList callForwardNumber = coreServicesMain.getCoreServicesList().get(finalCallForwardItemIndex);

                if (callForwardNumber != null) {
                    bundle.putString(AddForwardNumberActivity.CALL_FARWORD_OFFERID, callForwardNumber.getOfferingId());
                    bundle.putString(AddForwardNumberActivity.CALL_FARWORD_NUMBER, callForwardNumber.getForwardNumber());
                }
            }

            startNewActivityForResult(CoreServicesActivity.this, AddForwardNumberActivity.class, ADD_FORWARD_NUMBER_FOR_RESULT, bundle);

        }
    };


    public interface CoreServiceCallForwardListener {
        void onCallForwardListener(CoreServicesMain coreServicesMain, int finalCallForwardItemIndex);
    }

}
