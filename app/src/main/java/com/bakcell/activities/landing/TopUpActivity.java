package com.bakcell.activities.landing;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.TopUpCardsAdapter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityTopUpBinding;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.OnCardTypeClicked;
import com.bakcell.interfaces.OnOkClickListener;
import com.bakcell.interfaces.SaveCardsItemClickListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.topup.GetSavedCardsData;
import com.bakcell.models.topup.GetSavedCardsItems;
import com.bakcell.models.topup.InitiatePaymentData;
import com.bakcell.models.topup.Topup;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.models.user.userpredefinedobjects.CardType;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.DecimalDigitsInputFilter;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestDeleteSavedCardService;
import com.bakcell.webservices.RequestGetSavedCards;
import com.bakcell.webservices.RequestInitiatePaymentService;
import com.bakcell.webservices.RequestMakePaymentService;
import com.bakcell.webservices.RequestTopupService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.util.List;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class TopUpActivity extends BaseActivity implements View.OnClickListener {

    ActivityTopUpBinding binding;
    private String msisdnTo = "";
    private long selectedCardKey = 0;
    private List<GetSavedCardsItems> savedCards;
    private String paymentKey = "";
    private boolean isShowingNumberDialog = false;
    private boolean isShowingAmountDialog = false;
    private double lastAmount = 0;
    private UserMain userMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_top_up);

        initUiListeners();

        AppEventLogs.applyAppEvent(AppEventLogValues.TopupEvents.TOP_UP_SCREEN,
                AppEventLogValues.TopupEvents.TOP_UP_SCREEN,
                AppEventLogValues.TopupEvents.TOP_UP_SCREEN);
    }

    private void inAppFeedback(Topup topup) {
        int currentVisit = PrefUtils.getAsInt(TopUpActivity.this, PrefUtils.PreKeywords.PREF_TOP_UP_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(TopUpActivity.this);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TOP_UP_PAGE);
            if (surveys != null) {
                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                        InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(TopUpActivity.this);
                        if (surveys.getQuestions() != null) {
                            currentVisit = 0;
                            String message = getString(R.string.mesg_top_up_successful, Constants.PHONE_PREFIX + " " + msisdnTo);
                            inAppFeedbackDialog.showMessageWithBalanceDialog(surveys, topup.getNewBalance(), message);
                        } else {
                            showSuccessfulDialog(topup.getNewBalance(), topup.getOldBalance());
                        }
                    } else {
                        showSuccessfulDialog(topup.getNewBalance(), topup.getOldBalance());
                    }
                } else {
                    showSuccessfulDialog(topup.getNewBalance(), topup.getOldBalance());
                }
            } else {
                showSuccessfulDialog(topup.getNewBalance(), topup.getOldBalance());
            }
        } else {
            showSuccessfulDialog(topup.getNewBalance(), topup.getOldBalance());
        }
        PrefUtils.addInt(TopUpActivity.this, PrefUtils.PreKeywords.PREF_TOP_UP_PAGE, currentVisit);
    }
    private void inAppFeedbackCard(String title,String message) {
        int currentVisit = PrefUtils.getAsInt(TopUpActivity.this, PrefUtils.PreKeywords.PREF_TOP_UP_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(TopUpActivity.this);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.TOP_UP_PAGE);
            if (surveys != null) {
                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                        InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(TopUpActivity.this);
                        if (surveys.getQuestions() != null) {
                            currentVisit = 0;
                            inAppFeedbackDialog.showTitleWithMessageDialog(surveys, title, message);
                        } else {
                            showMessageDialog(TopUpActivity.this, title, message);
                        }
                    } else {
                        showMessageDialog(TopUpActivity.this, title, message);
                    }
                } else {
                    showMessageDialog(TopUpActivity.this, title, message);
                }
            } else {
                showMessageDialog(TopUpActivity.this, title, message);
            }
        } else {
            showMessageDialog(TopUpActivity.this, title, message);
        }
        PrefUtils.addInt(TopUpActivity.this, PrefUtils.PreKeywords.PREF_TOP_UP_PAGE, currentVisit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestSavedCards();
    }

    private void initUiListeners() {
        binding.radioPlasticCard.setOnClickListener(this);
        binding.radioScratchCard.setOnClickListener(this);
        binding.radioScratch.setChecked(true);

        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.btnTopUp.setOnClickListener(this);
        binding.btnTopUp.setSelected(true);
        binding.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radioScratch:
                    break;
                case R.id.radioPlastic:
                    String mobileNumber = binding.mobileInput.getText().toString();
                    if (Tools.hasValue(mobileNumber)) {
                        if (mobileNumber.length() != 9) {
                            BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                    getString(R.string.bakcell_error_title),
                                    getString(R.string.error_msg_invalid_number));
                        } else {
                            //redirect to the plastic card activity
                            Bundle bundle = new Bundle();
                            bundle.putString(TopupPlasticCardActivity.TOUP_PLASTIC_CARD_PREFIX, Constants.PHONE_PREFIX);
                            bundle.putString(TopupPlasticCardActivity.TOUP_PLASTIC_CARD_NUMBER, binding.mobileInput.getText().toString());

                            startNewActivity(TopUpActivity.this, TopupPlasticCardActivity.class, bundle);

                            binding.radioScratch.setChecked(true);
                        }
                    } else {
                        BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                getString(R.string.bakcell_error_title),
                                getString(R.string.error_msg_enter_number));
                    }

                    break;
            }
        });

        // Card Input field on Text change Listeners
        binding.cardInput.addTextChangedListener(new TextWatcher() {
            boolean isChageProg = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {

                if (!isChageProg) {
                    isChageProg = true;
                    String textFormated = getCreditCardFormatedText(text.toString());
                    binding.cardInput.setText(textFormated);
                    binding.cardInput.setSelection(binding.cardInput.getText().length());
                    isChageProg = false;
                }
            }
        });

        binding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String amount = binding.cardPlasticInput.getText().toString();
                String mobileNumber = binding.mobileInput.getText().toString();

                if (isValidMobileNumber(mobileNumber) && isValidAmount(amount) && isValidPaymentKey()) {
                    String dialogMessage = "";

                    if (AppClass.getCurrentLanguageNum(TopUpActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ)) {
                        dialogMessage = getString(R.string.payment_loading) + mobileNumber + " nömrəsinə " + amount + getString(R.string.azn_to);
                    } else {
                        dialogMessage = getString(R.string.payment_loading) + amount + getString(R.string.azn_to) + mobileNumber + ".";
                    }


                    BakcellPopUpDialog.showConfirmationDialog(TopUpActivity.this, getString(R.string.dialog_confirmation_lbl), dialogMessage, new OnOkClickListener() {
                        @Override
                        public void onOkClick() {
                            requestMakePayment(mobileNumber, amount, paymentKey);
                        }
                    });
                } else if (isValidMobileNumber(mobileNumber) && isValidAmount(amount) && userMain != null && userMain.getPredefinedData() != null &&
                        userMain.getPredefinedData().getTopup() != null && userMain.getPredefinedData().getTopup().getPlasticCard() != null &&
                        userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes() != null) {
                    List<CardType> cardTypes = userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes();
                    String title = getString(R.string.lbl_top_up_card_type);
                    BakcellPopUpDialog.showCardTypeDialog(TopUpActivity.this, title, cardTypes, new OnCardTypeClicked() {
                        @Override
                        public void onCardClicked(long cardKey) {
                            requestInitiatePayment(mobileNumber, amount, cardKey, true);
                        }
                    });
                }


            }
        });

        binding.cardPlasticInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {

                String value = text.toString();
                String[] valueSplits = value.split("\\.");

                if (value.length() <= 2 && valueSplits.length >= 1 && Tools.hasValue(valueSplits[0]) && valueSplits[0].equals("0")) {
                    binding.cardPlasticInput.setText("");
                } else if (value.startsWith(".")) {
                    binding.cardPlasticInput.setText("");
                }

                binding.cardPlasticInput.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 2),});

            }
        });

    }

    private boolean isValidPaymentKey() {
        boolean isValid = true;
        if (!Tools.hasValue(paymentKey)) {
            isValid = false;
        }
        return isValid;
    }

    private boolean isValidAmount(String amount) {
        boolean isValid = true;

        if (!Tools.hasValue(amount) || !Tools.isNumeric(amount)) {
            if (!isShowingAmountDialog) {
                BakcellPopUpDialog.showMessageDialogWithOKCallback(TopUpActivity.this,
                        getString(R.string.bakcell_error_title),
                        getString(R.string.lbl_auto_payment_enter_amount), new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                isShowingAmountDialog = false;
                            }
                        });
            }

            isShowingAmountDialog = true;
            return false;
        } else if (savedCards != null && savedCards.size() > 1 && Double.parseDouble(amount) < 1) {
            if (!isShowingAmountDialog) {
                BakcellPopUpDialog.showMessageDialogWithOKCallback(TopUpActivity.this,
                        getString(R.string.bakcell_error_title),
                        getString(R.string.error_msg_invalid_amount), new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                isShowingAmountDialog = false;
                            }
                        });
            }

            isShowingAmountDialog = true;
            return false;
        }

        return isValid;
    }

    private boolean isValidMobileNumber(String mobileNumber) {
        boolean isValid = true;
        if ((mobileNumber == null || TextUtils.isEmpty(mobileNumber) && !isShowingNumberDialog)) {
            BakcellPopUpDialog.showMessageDialogWithOKCallback(TopUpActivity.this,
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_msg_enter_number), new OnOkClickListener() {
                        @Override
                        public void onOkClick() {
                            isShowingNumberDialog = false;
                        }
                    });
            isValid = false;
            isShowingNumberDialog = true;
        } else if (!FieldFormatter.isValidMsisdn(mobileNumber) && !isShowingNumberDialog) {
            BakcellPopUpDialog.showMessageDialogWithOKCallback(TopUpActivity.this,
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_msg_invalid_number), new OnOkClickListener() {
                        @Override
                        public void onOkClick() {
                            isShowingNumberDialog = false;
                        }
                    });
            isValid = false;
            isShowingNumberDialog = true;
        }
        return isValid;
    }

    private String getCreditCardFormatedText(String string) {
        string = string.replace("-", "");
        if (Tools.hasValue(string)) {
            String s1 = "", s2 = "", s3 = "";
            if (string.length() >= 5) {
                s1 = string.substring(0, 5);
            }

            if (string.length() >= 10) {
                s2 = string.substring(5, 10);
            } else if (string.length() >= 6) {
                s2 = string.substring(5, string.length());
            }

            if (string.length() > 10) {
                s3 = string.substring(10, string.length());
            }

            if (!s1.equalsIgnoreCase("")) {
                string = s1;
            }
            if (!s2.equalsIgnoreCase("")) {
                string = string + "-" + s2;
            }
            if (!s3.equalsIgnoreCase("")) {
                string = string + "-" + s3;
            }
        }
        return string;
    }

    private void initUiContent() {

        userMain = PrefUtils.getCustomerData(TopUpActivity.this);
        //region Check if to show recent Visa card list layout or simple add card layout


        if (savedCards != null && savedCards.size() > 1) {
            setUiAccordingTCardsListSize(View.VISIBLE);
            if (lastAmount > 0) {
                binding.cardPlasticInput.setText("" + lastAmount);
            }
        } else {
            setUiAccordingTCardsListSize(View.GONE);
        }

        setupRecentVisaCardRecyclerView(userMain);

        //endregion
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.top_up_lbl);

        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null && Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
            binding.mobileInput.setText(DataManager.getInstance().getCurrentUser().getMsisdn());
        } else {
            binding.mobileInput.setText("");
        }

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setUiAccordingTCardsListSize(int visibility) {
        binding.visaCardLabel.setVisibility(visibility);
//        binding.rlAmount.setVisibility(visibility);
    }

    //Setup recycleView for recent cards
    private void setupRecentVisaCardRecyclerView(UserMain userMain) {
        binding.recyclerviewRecentVisa.setVisibility(View.VISIBLE);
        binding.spCardType.setVisibility(View.GONE);
        binding.visaCardLabel.setText(R.string.top_up_lbl_select_card);

        TopUpCardsAdapter topUpCardsAdapter = new TopUpCardsAdapter(savedCards, this, new SaveCardsItemClickListener() {
            @Override
            public void onItemClick(GetSavedCardsItems savedCardsItems, int position) {
                if (savedCardsItems != null && Tools.hasValue(savedCardsItems.getPaymentKey())) {
                    paymentKey = savedCardsItems.getPaymentKey();
                } else if (position != -1) {
                    String amount = binding.cardPlasticInput.getText().toString();
                    String mobileNumber = binding.mobileInput.getText().toString();

                    if (/*isValidMobileNumber(mobileNumber) && isValidAmount(amount) && */isCardTypesAvailable(userMain)) {
                        List<CardType> cardTypes = userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes();
                        String title = getString(R.string.lbl_top_up_card_type);
                        BakcellPopUpDialog.showCardTypeDialog(TopUpActivity.this, title, cardTypes, new OnCardTypeClicked() {
                            @Override
                            public void onCardClicked(long cardKey) {
                                requestInitiatePayment(mobileNumber, "", cardKey, true);
                            }
                        });
                    }
                } else {
                    paymentKey = "";
                }
            }
        });
        if (savedCards != null && savedCards.size() > 0 && savedCards.get(0) != null && savedCards.get(0).getPaymentKey() != null) {
            paymentKey = savedCards.get(0).getPaymentKey();
        }
        binding.recyclerviewRecentVisa.setLayoutManager(new LinearLayoutManager(this));
        if (savedCards != null && savedCards.size() > 1) {
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(enableSwipeToDeleteAndUndo(topUpCardsAdapter, savedCards));
            itemTouchHelper.attachToRecyclerView(binding.recyclerviewRecentVisa);
        }
        binding.recyclerviewRecentVisa.setAdapter(topUpCardsAdapter);

    } // end setupRecentVisaCardRecyclerView

    private boolean isCardTypesAvailable(UserMain userMain) {
        return userMain != null && userMain.getPredefinedData() != null && userMain.getPredefinedData().getTopup() != null &&
                userMain.getPredefinedData().getTopup().getPlasticCard() != null &&
                userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes() != null &&
                userMain.getPredefinedData().getTopup().getPlasticCard().getCardTypes().size() > 0;
    }

    protected void topUpClicked(final String msisdnTo, final String cardNumber) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_top_up_confirmation, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button yesBtn = dialogView.findViewById(R.id.btn_yes);
        LinearLayout balance_layout = dialogView.findViewById(R.id.balance_layout);
        BakcellTextViewNormal tvConfirmationMsg = dialogView
                .findViewById(R.id.tv_top_confirmation_msg);
        BakcellTextViewBold tvTitle = dialogView
                .findViewById(R.id.dialog_title);
        BakcellTextViewNormal currentBalance = dialogView.findViewById(R.id.currentBalance);
        String confirmationStr = getResources().getString(R.string.top_up_confirmation_message,
                "", Constants.PHONE_PREFIX + " " + msisdnTo);
        tvConfirmationMsg.setText(Html.fromHtml(confirmationStr));
        tvTitle.setSelected(true);

        if (DataManager.getInstance().getCurrentUser() != null) {
            if (DataManager.getInstance().getCurrentUser().getSubscriberType().toLowerCase()
                    .contains(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)
                    && DataManager.getInstance().getCurrentUser().getMsisdn().trim()
                    .equalsIgnoreCase(binding.mobileInput.getText().toString().trim())) {
                balance_layout.setVisibility(View.VISIBLE);
            } else {
                balance_layout.setVisibility(View.GONE);
            }
            currentBalance.setText(DataManager.getInstance().getCurrentBalance() + "");
        }
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestForTopup(msisdnTo, cardNumber);
                alertDialog.dismiss();
            }
        });

        Button noBtn = dialogView.findViewById(R.id.btn_no);
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void showSuccessfulDialog(String newBalance, String oldBalance1) {
        if (this.isFinishing()) return;

        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_top_up_successful, null);
            dialogBuilder.setView(dialogView);

            LinearLayout balance_layout = dialogView.findViewById(R.id.balance_layout);
            BakcellTextViewNormal tvBalnce = dialogView.findViewById(R.id.tvBalnce);
            BakcellTextViewNormal tv_top_confirmation_msg = dialogView.findViewById(R.id.tv_top_confirmation_msg);

            tvBalnce.setText(newBalance);

            if (DataManager.getInstance().getCurrentUser() != null) {

                if (DataManager.getInstance().getCurrentUser().getMsisdn().trim()
                        .contentEquals(binding.mobileInput.getText().toString().trim())) {
                    DataManager.getInstance().setNewBalance(newBalance);
                }

                if (DataManager.getInstance().getCurrentUser() != null) {
                    if (DataManager.getInstance().getCurrentUser().getSubscriberType().toLowerCase()
                            .contains(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)
                            && DataManager.getInstance().getCurrentUser().getMsisdn().trim()
                            .equalsIgnoreCase(binding.mobileInput.getText().toString().trim())) {

                        balance_layout.setVisibility(View.VISIBLE);
                    } else {
                        balance_layout.setVisibility(View.INVISIBLE);
                    }

                }
            }

            tv_top_confirmation_msg.setText(getString(R.string.mesg_top_up_successful, Constants.PHONE_PREFIX + " " + msisdnTo));

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button okayBtn = dialogView.findViewById(R.id.btn_okay);
            okayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.btn_top_up:
                if (!binding.radioScratch.isChecked()) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_top_up_select_scratch_card));
                } else if (binding.mobileInput.getText().toString().length() == 0) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_top_up_enter_mobile_number_message));
                } else if (binding.cardInput.getText().toString().length() == 0) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_top_up_enter_card_number_message));
                } else {
                    checksForTopup();
                }
                break;
            case R.id.radioScratchCard:
                showScratchCardLayout();
                break;
            case R.id.radioPlasticCard:
                showPlasticCardLayout();
                break;

        }
    }

    private void showPlasticCardLayout() {
        //set Plastic Radio Button state to false
        binding.radioScratchCard.setChecked(false);
        //set Scratch Radio Button state to true
        binding.radioPlasticCard.setChecked(true);
        //show layout plastic card
        binding.layoutPlasticCard.setVisibility(View.VISIBLE);
        //hide layout scratch card
        binding.layoutScratchCard.setVisibility(View.GONE);
    } //showPlasticCardLayout ends


    private void showScratchCardLayout() {
        //set Plastic Radio Button state to false
        binding.radioPlasticCard.setChecked(false);
        //set Scratch Radio Button state to true
        binding.radioScratchCard.setChecked(true);
        //show layout scratch card
        binding.layoutScratchCard.setVisibility(View.VISIBLE);
        //hide layout plastic card
        binding.layoutPlasticCard.setVisibility(View.GONE);
    } // showScratchCardLayout ends

    private void checksForTopup() {
        msisdnTo = binding.mobileInput.getText().toString();
        String cardNumber = binding.cardInput.getText().toString();
        if (Tools.hasValue(msisdnTo) && Tools.hasValue(cardNumber)) {
            if (msisdnTo.length() != 9) {
                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                        getString(R.string.bakcell_error_title),
                        getString(R.string.error_msg_invalid_number));
                return;
            }
            if (cardNumber.length() != 17) {
                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                        getString(R.string.bakcell_error_title),
                        getString(R.string.error_msg_invalid_card_number));
                return;
            }
            String cardNumberWithoutDashes = cardNumber.replace("-", "");
            topUpClicked(msisdnTo, cardNumberWithoutDashes);
        }
    }


    public void requestForTopup(String msisdnTo, String cardNumber) {


        RequestTopupService.newInstance(TopUpActivity.this, ServiceIDs.REQUEST_TOPUP)
                .execute(DataManager.getInstance().getCurrentUser(), msisdnTo, cardNumber,
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                String responseDescription = "";
                                if (!TopUpActivity.this.isFinishing() && response != null &&
                                        Tools.hasValue(response.getDescription())) {
                                    responseDescription = response.getDescription();
                                }

                                if (!TopUpActivity.this.isFinishing() && response != null) {
                                    String result = response.getData();

                                    Topup topup = new Gson().fromJson(result, Topup.class);

                                    inAppFeedback(topup);

                                    binding.cardInput.setText("");

                                    RootValues.getInstance().setDashboardApiCall(true);

                                    AppEventLogs.applyAppEvent(AppEventLogValues.TopupEvents.TOP_UP_SCREEN,
                                            AppEventLogValues.TopupEvents.TOP_UP_SCRATCH_CARD_SUCCESS
                                            , AppEventLogValues.TopupEvents.TOP_UP_SCREEN);
                                } else {
                                    BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                            getString(R.string.bakcell_error_title), responseDescription);
                                }
                            }


                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                                // Check if user logout by server
                                if (!TopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(TopUpActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(TopUpActivity.this);
                                    return;
                                }
                                if (!TopUpActivity.this.isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }

                                AppEventLogs.applyAppEvent(AppEventLogValues.TopupEvents.TOP_UP_SCREEN,
                                        AppEventLogValues.TopupEvents.TOP_UP_SCRATCH_CARD_FAILURE
                                        , AppEventLogValues.TopupEvents.TOP_UP_SCREEN);
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (!TopUpActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(TopUpActivity.this);
                                }
                            }
                        });

    } // requestForTopup ends

    private void requestSavedCards() {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestGetSavedCards.newInstance(TopUpActivity.this, ServiceIDs.REQUEST_GET_SAVED_CARDS)
                    .execute(userModel, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            String responseDescription = "";
                            if (!TopUpActivity.this.isFinishing() && response != null &&
                                    Tools.hasValue(response.getDescription())) {
                                responseDescription = response.getDescription();
                            }

                            if (!TopUpActivity.this.isFinishing() && response != null) {
                                String data = response.getData();
                                Gson gson = new Gson();
                                GetSavedCardsData getSavedCardsData = gson.fromJson(data, GetSavedCardsData.class);
                                lastAmount = getSavedCardsData.getLastAmount();
                                savedCards = getSavedCardsData.getData();
                                savedCards.add(new GetSavedCardsItems());
                                initUiContent();
                            } else {
                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.bakcell_error_title), responseDescription);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!TopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(TopUpActivity.this);
                                return;
                            }

                            if (!TopUpActivity.this.isFinishing() && response != null) {
                                initUiContent();
                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!TopUpActivity.this.isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(TopUpActivity.this);
                                initUiContent();
                            }
                        }
                    });

        }
    } // requestSavedCards ends

    private void requestInitiatePayment(String mobileNumber, String amount, long selectedCardKey, boolean isSaved) {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestInitiatePaymentService.newInstance(TopUpActivity.this, ServiceIDs.REQUEST_GET_INITIATE_PAYMENT)
                    .execute(userModel, mobileNumber, amount, selectedCardKey, isSaved, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            String responseDescription = "";
                            if (!TopUpActivity.this.isFinishing() && response != null &&
                                    Tools.hasValue(response.getDescription())) {
                                responseDescription = response.getDescription();
                            }

                            if (!TopUpActivity.this.isFinishing() && response != null) {
                                String data = response.getData();
                                Gson gson = new Gson();
                                InitiatePaymentData initiatePaymentData = gson.fromJson(data, InitiatePaymentData.class);
                                if (initiatePaymentData != null && Tools.hasValue(initiatePaymentData.getPaymentKey())) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString(Constants.TOP_UP_INIT_PAYMENT_URL, initiatePaymentData.getPaymentKey());
                                    startNewActivity(TopUpActivity.this, TopupPlasticCardActivity.class, bundle);
                                }

                            } else {
                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.bakcell_error_title), responseDescription);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!TopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(TopUpActivity.this);
                                return;
                            }

                            if (!TopUpActivity.this.isFinishing() && response != null) {
                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!TopUpActivity.this.isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(TopUpActivity.this);
                            }
                        }
                    });

        }
    } // requestInitiatePayment ends

    /**
     * Api call for Make Payment after getting mobile number, amount and payment kye
     *
     * @param mobileNumber of user
     * @param amount       entered by user
     * @param paymentKey   got from card  by the user
     */
    private void requestMakePayment(String mobileNumber, String amount, String paymentKey) {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestMakePaymentService.newInstance(TopUpActivity.this, ServiceIDs.REQUEST_MAKE_PAYMENT)
                    .execute(userModel, paymentKey, amount, mobileNumber, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            String responseDescription = "";
                            if (!TopUpActivity.this.isFinishing() && response != null &&
                                    Tools.hasValue(response.getDescription())) {
                                responseDescription = response.getDescription();
                            }

                            if (!TopUpActivity.this.isFinishing() && response != null) {
                                inAppFeedbackCard(getString(R.string.app_name), responseDescription);
//                                showMessageDialog(TopUpActivity.this,
//                                        getString(R.string.app_name), responseDescription);
                            }

                            //refresh dashboard check
                            RootValues.getInstance().setDashboardApiCall(true);

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!TopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(TopUpActivity.this);
                                return;
                            }

                            if (!isFinishing() && response != null) {
                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!TopUpActivity.this.isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(TopUpActivity.this);
                            }

                        }
                    });

        }
    }// requestMakePayment ends

    private void requestDeleteSavedCard(TopUpCardsAdapter topUpCardsAdapter, int position, String savedCardId) {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestDeleteSavedCardService.newInstance(TopUpActivity.this, ServiceIDs.REQUEST_DELETE_SAVED_CARDS)
                    .execute(userModel, savedCardId, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            if (!TopUpActivity.this.isFinishing() && response != null && response.getDescription() != null) {
                                topUpCardsAdapter.deleteItem(position);
                                paymentKey = "";
                                if (topUpCardsAdapter.getItemCount() <= 1) {
                                    setUiAccordingTCardsListSize(View.GONE);
                                } else {
                                    GetSavedCardsItems getSavedCardsItems = topUpCardsAdapter.getFirstItem();
                                    if (getSavedCardsItems != null && Tools.hasValue(getSavedCardsItems.getPaymentKey())) {
                                        paymentKey = getSavedCardsItems.getPaymentKey();
                                    }
                                    setUiAccordingTCardsListSize(View.VISIBLE);
                                }

                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.dialog_title_successful), response.getDescription());
                            } else if (!TopUpActivity.this.isFinishing()) {
                                topUpCardsAdapter.notifyDataSetChanged();
                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!TopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(TopUpActivity.this);
                                return;
                            }

                            initUiContent();

                            if (!TopUpActivity.this.isFinishing() && response != null) {
                                topUpCardsAdapter.notifyDataSetChanged();
                                BakcellPopUpDialog.showMessageDialog(TopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!TopUpActivity.this.isFinishing()) {
                                topUpCardsAdapter.notifyDataSetChanged();
                                BakcellPopUpDialog.ApiFailureMessage(TopUpActivity.this);
                                initUiContent();
                            }
                        }
                    });
        }
    } // requestDeleteSavedCard ends

    private void showMessageDialog(Context context, String title, String message) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_message, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final android.app.AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    //go back
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    private ItemTouchHelper.SimpleCallback enableSwipeToDeleteAndUndo(TopUpCardsAdapter topUpCardsAdapter, List<GetSavedCardsItems> savedCards) {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
                if (savedCards != null && position != savedCards.size() - 1 && savedCards.get(position) != null && Tools.hasValue(savedCards.get(position).getId())) {
                    requestDeleteSavedCard(topUpCardsAdapter, position, savedCards.get(position).getId());
                } else {
                    topUpCardsAdapter.notifyItemChanged(position);
                }
            }
        };
        return simpleItemTouchCallback;
    } //enableSwipeToDeleteAndUndo


    private String getTopUpPlasticCardURL(String mobileNumber, String amount) {
        String url = null;
        String currentLanguage = AppClass.getCurrentLanguageKey(this);
        switch (currentLanguage) {
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN:
                if (DataManager.getInstance().getUserPredefinedData() != null &&
                        DataManager.getInstance().getUserPredefinedData().getRedirectionLinks() != null) {
                    url = DataManager.getInstance().getUserPredefinedData().getRedirectionLinks()
                            .getOnlinePaymentGatewayURL_EN();
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, "url english:::" + url);
                }
                break;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ:
                if (DataManager.getInstance().getUserPredefinedData() != null
                        && DataManager.getInstance().getUserPredefinedData().getRedirectionLinks() != null) {
                    url = DataManager.getInstance().getUserPredefinedData().getRedirectionLinks()
                            .getOnlinePaymentGatewayURL_AZ();
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, "url azeri:::" + url);
                }
                break;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU:
                if (DataManager.getInstance().getUserPredefinedData() != null &&
                        DataManager.getInstance().getUserPredefinedData().getRedirectionLinks() != null) {
                    url = DataManager.getInstance().getUserPredefinedData().getRedirectionLinks()
                            .getOnlinePaymentGatewayURL_RU();
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, "url russian:::" + url);
                }
                break;
        }
        //?prefix=\("55")&number=\(topUpToMSISDN)
        /*if (getIntent().getExtras() != null && getIntent().hasExtra(TOUP_PLASTIC_CARD_PREFIX)
                && getIntent().hasExtra(TOUP_PLASTIC_CARD_NUMBER)) {
            url = url + "?prefix=" + getIntent().getExtras().getString(TOUP_PLASTIC_CARD_PREFIX)
                    + "&number=" + getIntent().getExtras().getString(TOUP_PLASTIC_CARD_NUMBER);
        }*/
        //String url = "https://ode.goldenpay.az/merchant.html#/" + AppClass.getCurrentLanguageKey(TopupPlasticCardActivity.this) + "/parameters/bakcell3/bakcellecare";


        //get the prefix and the number from the mobile number
        String prefix = "";
        String number = "";
        /**the prefix will be the very first two digits of the mobile number
         * add them to the url as: &prefix=DD
         * and the number will the rest of the seven digits that is the mobile number minus first two chars*/
        if (mobileNumber != null) {
            if (mobileNumber.length() == 9) {
                try {
                    prefix = Character.toString(mobileNumber.charAt(0)) + Character.toString(mobileNumber.charAt(1));
                    number = Character.toString(mobileNumber.charAt(2)) +
                            Character.toString(mobileNumber.charAt(3)) +
                            Character.toString(mobileNumber.charAt(4)) +
                            Character.toString(mobileNumber.charAt(5)) +
                            Character.toString(mobileNumber.charAt(6)) +
                            Character.toString(mobileNumber.charAt(7)) +
                            Character.toString(mobileNumber.charAt(8));
                } catch (Exception exp) {
                    BakcellLogger.logE("extractingMobileNumber", "error:::" + exp.toString(), "TopupPlasticCardActivity", "getTopupPasticCardURL");
                }
            }
        }

        /**
         * create the url as followed
         * https://hesab.az/unregistered/#/mobile/bakcell/parameters?lang=az&prefix=first two chars of the mobile number&number=last seven digits*/

        url = url + "&prefix=" + prefix + "&number=" + number + "&amount=" + amount;
        return url;
    }//getTopupPasticCardURL
}
