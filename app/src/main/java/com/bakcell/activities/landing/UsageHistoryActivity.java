package com.bakcell.activities.landing;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.databinding.ActivityUsageHistoryBinding;
import com.bakcell.fragments.pager.UsageHistoryDetailPaggerFragment;
import com.bakcell.fragments.pager.UsageHistorySummaryPaggerFragment;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;

/**
 * Created by Noman on 18-May-17.
 */

public class UsageHistoryActivity extends BaseActivity implements View.OnClickListener {

    ActivityUsageHistoryBinding binding;
    private PagerAdapterOffers paggerAdapter;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_usage_history);

        initUiListener();
        initUITabsAndPagger();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initUiListener() {


        if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getCustomerType()) && //Fixed NPE
                DataManager.getInstance().getCurrentUser().getCustomerType().toLowerCase()
                        .contains(Constants.UserCurrentKeyword.CUSTOMER_TYPE_POSTPAID_CORPORATE)) {
            binding.viewPager.disableScroll(true);
            binding.tabLayout.setVisibility(View.GONE);
        } else {
            binding.viewPager.disableScroll(false);
            binding.tabLayout.setVisibility(View.VISIBLE);
        }

        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 1) {
                    try {
                        UsageHistoryDetailPaggerFragment fragment = (UsageHistoryDetailPaggerFragment)
                                paggerAdapter.getItem(position);
                        fragment.showTextAnimation();
                    } catch (Exception e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initUITabsAndPagger() {

        binding.toolbar.pageTitle.setText(R.string.title_usage_history);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));


        // Adding Tabs here
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.usage_history_summary_lbl));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.usage_history_detail_lbl));

        iniPaggerView();

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (15 * getResources().getDisplayMetrics().density);
            p.setMargins(0, 0, margin, 0);
            tab.requestLayout();
        }

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 1) {
                    if (RootValues.getInstance().getUsageHistoryDetailLoadActionListener() != null) {
                        RootValues.getInstance().getUsageHistoryDetailLoadActionListener().onUsageHistoryDetailActionCall(-1);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }

    private void iniPaggerView() {
        ArrayList<Fragment> paggerFragmentsList = new ArrayList<>();

        paggerFragmentsList.add(new UsageHistorySummaryPaggerFragment());
        paggerFragmentsList.add(new UsageHistoryDetailPaggerFragment());
        paggerAdapter = new PagerAdapterOffers(getSupportFragmentManager(), paggerFragmentsList);
        binding.viewPager.setAdapter(paggerAdapter);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
        }
    }
}
