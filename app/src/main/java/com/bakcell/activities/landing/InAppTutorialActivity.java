package com.bakcell.activities.landing;

import android.app.ProgressDialog;
import androidx.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.databinding.ActivityInapptutorialBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.Tools;

public class InAppTutorialActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "Video";
    ActivityInapptutorialBinding binding;

    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_inapptutorial);

        initUiContent();

        initUiListener();


    }

    private void initUiListener() {

        binding.ivSkip.setOnClickListener(this);

        binding.tutorialsVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                }
                finish();
            }
        });
    }

    private void initUiContent() {
        String videoUrl = "https://myapp.bakcell.com/bakcellappserver/videos/";
        if (DataManager.getInstance().getCurrentUser() != null && Tools.hasValue(DataManager.getInstance().getCurrentUser().getSubscriberType())) { //Fixed NPE
            if (DataManager.getInstance().getCurrentUser().getSubscriberType()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)) {
                videoUrl = videoUrl + "prepaid";
            } else {
                if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getCustomerType())
                        && DataManager.getInstance().getCurrentUser().getCustomerType().toLowerCase().contains(Constants.UserCurrentKeyword
                        .CUSTOMER_TYPE_POSTPAID_INDIVIDUAL_TEM) || DataManager.getInstance().getCurrentUser().getCustomerType().toLowerCase().contains(Constants.UserCurrentKeyword
                        .SUBSCRIBER_TYPE_KLASS_POSTPAID)) {
                    videoUrl = videoUrl + "individual";
                } else if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getCustomerType())
                        && DataManager.getInstance().getCurrentUser().getCustomerType().toLowerCase().contains(Constants.UserCurrentKeyword
                        .CUSTOMER_TYPE_POSTPAID_CORPORATE)) {
                    videoUrl = videoUrl + "corporate";
                }
            }
        }

        String currentLanguage = AppClass.getCurrentLanguageKey(this);
        switch (currentLanguage) {
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN:
                binding.ivSkip.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.skip));
                videoUrl = videoUrl + "_en.mp4";
                break;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ:
                binding.ivSkip.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.skip_az));
                videoUrl = videoUrl + "_az.mp4";
                break;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU:
                binding.ivSkip.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.skip_ru));
                videoUrl = videoUrl + "_ru.mp4";
                break;
        }

        if (Tools.haveNetworkConnection(this)) {
            Uri vidUri = Uri.parse(videoUrl);
            binding.tutorialsVideo.setVideoURI(vidUri);
//            binding.tutorialsVideo.start();

            pDialog = new ProgressDialog(this);
            pDialog.setTitle(R.string.loading);
//            pDialog.setMessage("Buffering...");
            pDialog.setIndeterminate(false);
            pDialog.show();
            binding.redColorDB.setVisibility(View.VISIBLE);

//            binding.tutorialsVideo.requestFocus();
            binding.tutorialsVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mp) {
                    pDialog.dismiss();
                    binding.redColorDB.setVisibility(View.GONE);
                    binding.tutorialsVideo.start();
                }
            });

        } else {
            Toast.makeText(this, R.string.message_no_internet, Toast.LENGTH_SHORT).show();
            finish();
        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_skip:
                finish();
                break;
        }

    }


}
