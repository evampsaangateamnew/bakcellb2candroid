package com.bakcell.activities.landing;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityQuickServicesBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.models.DataManager;
import com.bakcell.models.quickservices.FreeSms;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestGetFreeSmsStatusService;
import com.bakcell.webservices.RequestSendFreeSmsService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class QuickServicesActivity extends BaseActivity implements View.OnClickListener {

    ActivityQuickServicesBinding binding;

    public static final int SMS_LENGTH_EN = 160;
    public static final int SMS_LENGTH_AZ = 70;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_quick_services);

        initUiContent();

        initUiListeners();

        getFreeSmsStatus();

        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_SCREEN);
    }

    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.etTextMessage.setOnClickListener(this);
        binding.btnSend.setOnClickListener(this);
        binding.etTextMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                int textLength = s.length();
                int smsLength;
                if (AppClass.checkIfTextContainOtherThenEnglishCharacters(QuickServicesActivity.this, s.toString())) {
                    smsLength = SMS_LENGTH_AZ;
                } else {
                    smsLength = SMS_LENGTH_EN;
                }
                if (textLength >= (smsLength * 2)) {
                    BakcellPopUpDialog.showMessageDialog(QuickServicesActivity.this, getString(R.string.bakcell_error_title), getString(R.string.more_char_error_message, (smsLength * 2)));
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                int textLength = s.length();
                int smsLength;
                if (AppClass.checkIfTextContainOtherThenEnglishCharacters(QuickServicesActivity.this, s.toString())) {
                    smsLength = SMS_LENGTH_AZ;
                    if (textLength > (SMS_LENGTH_AZ * 2)) {
                        BakcellPopUpDialog.showMessageDialog(QuickServicesActivity.this, getString(R.string.bakcell_error_title), getString(R.string.more_char_error_message, (SMS_LENGTH_AZ * 2)));
                    }
                } else {
                    smsLength = SMS_LENGTH_EN;
                }

                setEditTextMaxLength((smsLength * 2), binding.etTextMessage);

                String text = getResources().getString(R.string.free_sms_symbols_lenght, textLength);
                binding.tvSymbolsLength.setText(text);
                int smsCount = 0;
                if (textLength > 0 && textLength <= smsLength) {
                    smsCount = 1;
                } else if (textLength > smsLength && textLength <= (smsLength * 2)) {
                    smsCount = 2;
                }

                String smsText = getResources().getString(R.string.free_sms_count, smsCount);
                binding.tvSmsCount.setText(smsText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.etTextMessage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                binding.scrollView.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    public void setEditTextMaxLength(int length, EditText editText) {
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(length);
        if (editText != null) {
            editText.setFilters(filterArray);
        }
    }

    private void initUiContent() {
        binding.offNetLeftOver.setText("0");
        binding.onNetLeftOver.setText("0");
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setText(R.string.activity_quick_service_lbl);

        String text = getResources().getString(R.string.free_sms_count, 0);
        binding.tvSmsCount.setText(text);
        String text1 = getResources().getString(R.string.free_sms_symbols_lenght, 0);
        binding.tvSymbolsLength.setText(text1);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.et_text_message:
                binding.scrollView.scrollTo(binding.btnSend.getScrollX(), binding.btnSend.getScrollY());
                binding.btnSend.requestLayout();
                break;
            case R.id.btn_send:
                processSendAction();
                break;
        }
    }

    private void processSendAction() {

        String number = binding.mobileInput.getText().toString();
        String messageText = binding.etTextMessage.getText().toString();

        if (AppClass.checkIfTextContainOtherThenEnglishCharacters(QuickServicesActivity.this, messageText)) {
            if (messageText.length() > SMS_LENGTH_AZ) {
                BakcellPopUpDialog.showMessageDialog(QuickServicesActivity.this, getString(R.string.bakcell_error_title), getString(R.string.more_char_error_message, (SMS_LENGTH_AZ * 2)));
                return;
            }
        }

        if (number.length() != 0) {
            if (FieldFormatter.isValidMsisdn(number)) {
                if (messageText.length() == 0) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.enter_text));
                } else {
                    sendTextMessage(number, messageText);
                }
            } else {
                BakcellPopUpDialog.showMessageDialog(this,
                        getString(R.string.bakcell_error_title),
                        getString(R.string.error_msg_invalid_number));
            }
        } else {
            BakcellPopUpDialog.showMessageDialog(this,
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_top_up_enter_mobile_number_message));
        }
    }

    private void sendTextMessage(String msisdn, String textMessage) {
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();

            byte[] data = new byte[0];
            try {
                data = textMessage.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            String base64Msg = Base64.encodeToString(data, Base64.DEFAULT);

            String smsLangType = "EN";
            if (AppClass.checkIfTextContainOtherThenEnglishCharacters(QuickServicesActivity.this, textMessage)) {
                smsLangType = "NE";
            }

            RequestSendFreeSmsService.newInstance(this, ServiceIDs.REQUEST_SEND_FREE_SMS)
                    .execute(userModel, msisdn, base64Msg, smsLangType, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String result = response.getData();
                            FreeSms freeSmsStatus = new Gson().fromJson(result, FreeSms.class);
                            if (freeSmsStatus != null) {
                                updateOffNetAndOnNet(freeSmsStatus);
                                if (freeSmsStatus.isSmsSent()) {
                                    Toast.makeText(QuickServicesActivity.this, R.string.msg_sent_successfullu,
                                            Toast.LENGTH_SHORT).show();
                                    binding.etTextMessage.setText("");
                                } else {
                                    Toast.makeText(QuickServicesActivity.this, R.string.message_wasnot_sent,
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                            AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_SCREEN,
                                    AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_MESSAGE_SEND_SUCCESS,
                                    AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_SCREEN);
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(QuickServicesActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                            AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_SCREEN,
                                    AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_MESSAGE_SEND_FAILURE,
                                    AppEventLogValues.ServiceEvents.SERVICE_QUICK_SERVICES_SCREEN);
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(QuickServicesActivity.this);
                        }
                    });

        }
    }

    public void getFreeSmsStatus() {
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();

            RequestGetFreeSmsStatusService.newInstance(this, ServiceIDs.REQUEST_FREE_SMS_STATUS)
                    .execute(userModel, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String result = response.getData();
                            if (result == null) return;
                            FreeSms freeSmsStatus = new Gson().fromJson(result, FreeSms.class);
                            if (freeSmsStatus != null) {
                                updateOffNetAndOnNet(freeSmsStatus);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            // Check if user logout by server
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(QuickServicesActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(QuickServicesActivity.this);
                                return;
                            }
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(QuickServicesActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(QuickServicesActivity.this);
                        }
                    });

        }
    }

    private void updateOffNetAndOnNet(FreeSms freeSmsStatus) {
        if (freeSmsStatus == null) return;
        binding.offNetLeftOver.setText("0");
        binding.onNetLeftOver.setText("0");
        if (Tools.hasValue(freeSmsStatus.getOffNetSMS())) {
            binding.offNetLeftOver.setText(freeSmsStatus.getOffNetSMS());
        }
        if (Tools.hasValue(freeSmsStatus.getOnNetSMS())) {
            binding.onNetLeftOver.setText(freeSmsStatus.getOnNetSMS());
        }
    }
}
