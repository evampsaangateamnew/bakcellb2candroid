package com.bakcell.activities.landing;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.MainAutoPaymentAdapter;
import com.bakcell.databinding.ActivityAutoPaymentBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.interfaces.ScheduledPaymentItemEvent;
import com.bakcell.models.DataManager;
import com.bakcell.models.getscheduledpayments.ScheduledPaymentData;
import com.bakcell.models.getscheduledpayments.ScheduledPaymentDataItem;
import com.bakcell.models.user.UserModel;
import com.bakcell.webservices.RequestDeleteAutoPaymentService;
import com.bakcell.webservices.RequestGetScheduledPayments;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class AutoPaymentActivity extends BaseActivity implements View.OnClickListener {
    ActivityAutoPaymentBinding binding;
    private List<ScheduledPaymentDataItem> scheduledPaymentDataItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auto_payment);
        initUiContent();

        initUiListeners();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //request Fast Payments API
        requestAutoPayments();
    }

    private void requestAutoPayments() {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestGetScheduledPayments.newInstance(AutoPaymentActivity.this, ServiceIDs.REQUEST_GET_SCHEDULED_PAYMENTS)
                    .execute(userModel, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            String data = response.getData();
                            Gson gson = new Gson();
                            ScheduledPaymentData scheduledPaymentData = gson.fromJson(data, ScheduledPaymentData.class);
                            if (scheduledPaymentData != null && scheduledPaymentData.getData() != null
                                    && scheduledPaymentData.getData().size() > 0) {
                                setupRecentVisaCardRecyclerView(scheduledPaymentData.getData());
                            } else
                            {
                                setupRecentVisaCardRecyclerView(new ArrayList<>());
                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(AutoPaymentActivity.this);
                                return;
                            }

                            if (!AutoPaymentActivity.this.isFinishing() && response != null) {
                                BakcellPopUpDialog.showMessageDialog(AutoPaymentActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                                setupRecentVisaCardRecyclerView(new ArrayList<>());
                            }


                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!AutoPaymentActivity.this.isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(AutoPaymentActivity.this);
                                setupRecentVisaCardRecyclerView(new ArrayList<>());
                            }

                        }
                    });

        }
    } // requestFastPayments ends

    private void initUiListeners() {

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
    }

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.lbl_auto_payment);

    }

    private void setupRecentVisaCardRecyclerView(List<ScheduledPaymentDataItem> scheduledPaymentData) {

        scheduledPaymentData.add(new ScheduledPaymentDataItem());

        MainAutoPaymentAdapter adapterFastTopUp = new MainAutoPaymentAdapter(scheduledPaymentData, this, new ScheduledPaymentItemEvent() {
            @Override
            public void onItemClick(int position) {
                if (position == scheduledPaymentData.size() - 1) {
                    goToAddPaymentActivity();
                }
            }
        });
        binding.recyclerViewFastTopUp.setLayoutManager(new LinearLayoutManager(this));
        if (scheduledPaymentData != null && scheduledPaymentData.size() > 1) {
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(enableSwipeToDeleteAndUndo(adapterFastTopUp, scheduledPaymentData));
            itemTouchHelper.attachToRecyclerView(binding.recyclerViewFastTopUp);
        }
        binding.recyclerViewFastTopUp.setAdapter(adapterFastTopUp);
    }

    @Override
    public void onClick(View view) {

    }

    public void goToAddPaymentActivity() {
        BaseActivity.startNewActivity(AutoPaymentActivity.this, AutoAddPaymentActivity.class);
    }

    private ItemTouchHelper.SimpleCallback enableSwipeToDeleteAndUndo(MainAutoPaymentAdapter topUpCardsAdapter, List<ScheduledPaymentDataItem> scheduledPaymentDataItems) {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
                if (scheduledPaymentDataItems != null && position <= scheduledPaymentDataItems.size() - 1 && scheduledPaymentDataItems.get(position) != null && scheduledPaymentDataItems.get(position).getId() > 0) {
                    requestDeleteSchedulerPayment(topUpCardsAdapter, position, scheduledPaymentDataItems.get(position).getId());
                } else {
                    topUpCardsAdapter.notifyItemChanged(position);
                }

            }
        };
        return simpleItemTouchCallback;
    }

    private void requestDeleteSchedulerPayment(MainAutoPaymentAdapter mainAutoPaymentAdapter, int position, int id) {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestDeleteAutoPaymentService.newInstance(AutoPaymentActivity.this, ServiceIDs.REQUEST_DELETE_SCHEDULED_PAYMENTS)
                    .execute(userModel, id, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            if (!AutoPaymentActivity.this.isFinishing() && response != null && response.getDescription() != null) {
                                mainAutoPaymentAdapter.deleteItem(position);
                                BakcellPopUpDialog.showMessageDialog(AutoPaymentActivity.this,
                                        getString(R.string.dialog_title_successful), response.getDescription());
                            } else {
                                mainAutoPaymentAdapter.notifyDataSetChanged();
                                BakcellPopUpDialog.showMessageDialog(AutoPaymentActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!AutoPaymentActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(AutoPaymentActivity.this);
                                return;
                            }

                            initUiContent();

                            if (!AutoPaymentActivity.this.isFinishing() && response != null) {
                                mainAutoPaymentAdapter.notifyDataSetChanged();
                                BakcellPopUpDialog.showMessageDialog(AutoPaymentActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!AutoPaymentActivity.this.isFinishing()) {
                                mainAutoPaymentAdapter.notifyDataSetChanged();
                                BakcellPopUpDialog.ApiFailureMessage(AutoPaymentActivity.this);
                                initUiContent();
                            }
                        }
                    });
        }
    }
}