package com.bakcell.activities.landing;

import android.app.DatePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.adapters.UlduzumUsageHistoryAdapter;
import com.bakcell.databinding.ActivityUlduzumUsageHistoryBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.ulduzum.UlduzumUsageHistoryModel;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestUlduzumUsageHistoryService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellCalendar;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class UlduzumUsageHistoryActivity extends BaseActivity implements View.OnClickListener {

    private SpinnerAdapterOperationHistoryFilter spinnerAdapterPeriodFilter;
    private ActivityUlduzumUsageHistoryBinding binding;
    private UlduzumUsageHistoryAdapter ulduzumUsageHistoryAdapter;
    private String startDate = "", endDate = "", date = "";
    String dateOne = "";
    private UlduzumUsageHistoryModel ulduzumUsageHistoryModel = null;
    String dateTwo = "";
    boolean flagDate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ulduzum_usage_history);

        initUIContents();

        initUIListener();

        try {
            setDefaultDate();
            date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.CURRENT_DAY);
            updateDates(date, Tools.getCurrentDate());
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    private void initUIContents() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.pageTitle.setText(getResources().getString(R.string.ulduzum_usage_history_label));
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        spinnerAdapterPeriodFilter = new SpinnerAdapterOperationHistoryFilter(UlduzumUsageHistoryActivity.this, getResources().getStringArray(R.array.period_filter));
        binding.spinnerPeriod.setAdapter(spinnerAdapterPeriodFilter);
    }

    private void updateUI(UlduzumUsageHistoryModel model) {
        if (model.getUsageHistoryList().size() > 0) {
            binding.usageHistoryList.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
        } else {
            binding.usageHistoryList.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }
        ulduzumUsageHistoryAdapter = new UlduzumUsageHistoryAdapter(
                UlduzumUsageHistoryActivity.this, model);
        binding.usageHistoryList.setAdapter(ulduzumUsageHistoryAdapter);
    }

    private void initUIListener() {
        binding.spinnerPeriodButton.setOnClickListener(this);
        binding.tvDateOne.setOnClickListener(this);
        binding.tvDateTwo.setOnClickListener(this);
        binding.toolbar.hamburgerMenu.setOnClickListener(this);

        binding.spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    if (flagDate) {
                        try {
                            binding.llCalendar.setVisibility(View.GONE);
                            updateDates(Tools.getCurrentDate(), Tools.getCurrentDate());

                        } catch (ParseException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                    } else {
                        binding.llCalendar.setVisibility(View.GONE);
                        flagDate = !flagDate;
                    }
                } else if (position == 1) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_SEVEN_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 2) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_THIRTY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 3) {
                    binding.llCalendar.setVisibility(View.GONE);
                    date = "";
                    String maxDate = "";
                    try {
                        date = Tools.getLastMonthFirstDate();
                        maxDate = Tools.getLastMonthLastDate();

                        updateDates(date, maxDate);

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                } else if (position == 4) {
                    try {
                        setDefaultDate();
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                    binding.llCalendar.setVisibility(View.VISIBLE);
                } else {
                    binding.llCalendar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }//initUIListener ends

    private void setDefaultDate() throws ParseException {
        binding.tvDateOne.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
        binding.tvDateTwo.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.spinnerPeriodButton:
                binding.spinnerPeriod.performClick();
                break;
            case R.id.tvDateOne:
                try {
                    showCalendar(binding.tvDateOne, true);//false for max date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
            case R.id.tvDateTwo:
                try {
                    showCalendar(binding.tvDateTwo, false);//true for min date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;

        }
    }

    public void showCalendar(BakcellTextViewNormal textViewNormal, boolean isStartDateSelected) throws ParseException {

        if (UlduzumUsageHistoryActivity.this.isFinishing()) return;

        try {
            dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());

            Date minDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
            Date maxDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);

            BakcellCalendar bakcellCalendar;

            final Calendar calendar = Calendar.getInstance();
            if (isStartDateSelected) {
                calendar.setTime(minDate);
            } else {
                calendar.setTime(maxDate);
            }
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            bakcellCalendar = new BakcellCalendar(textViewNormal, ondate);


            DatePickerDialog datePickerDialog = new DatePickerDialog(UlduzumUsageHistoryActivity.this, bakcellCalendar,
                    year, month, day);
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {

                Calendar calndPre = Calendar.getInstance();
                calndPre.add(Calendar.MONTH, -3);
                calndPre.set(Calendar.DATE, 1);
                datePickerDialog.getDatePicker().setMinDate(calndPre.getTimeInMillis());

                Date dateMax = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);
                calendar.setTime(dateMax);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());

            } else {

                Date dateMin = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
                calendar.setTime(dateMin);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("");//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show();
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            //it will return index of Month for june it will return 05 +1 will make it 06
            //   monthOfYear++;
//            String dateNew = String.valueOf(year) + "-" + String.format("%02d", monthOfYear)
//                    + "-" + String.format("%02d", dayOfMonth);
            try {
                dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            try {
                dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            updateDates(dateOne, dateTwo);
        }
    };

    private void updateDates(String newStartDate, String newEndDate) {
        startDate = newStartDate;
        endDate = newEndDate;
        requestForGetUsageSummaryHistory(startDate, endDate);
    }

    public void requestForGetUsageSummaryHistory(String startDate, String endDate) {
        RequestUlduzumUsageHistoryService.newInstance(UlduzumUsageHistoryActivity.this, ServiceIDs.ULDUZUM_USAGE_HISTORY_ID)
                .execute(DataManager.getInstance().getCurrentUser(),
                        startDate, endDate, new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                ulduzumUsageHistoryModel = new Gson().fromJson(response.getData(), UlduzumUsageHistoryModel.class);
                                if (ulduzumUsageHistoryModel != null) {
                                    updateUI(ulduzumUsageHistoryModel);
                                }else {
                                    binding.usageHistoryList.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                }

                            }//end of onSuccess


                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (!UlduzumUsageHistoryActivity.this.isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(UlduzumUsageHistoryActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(UlduzumUsageHistoryActivity.this);
                                    return;
                                }

                                if (!UlduzumUsageHistoryActivity.this.isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(UlduzumUsageHistoryActivity.this,
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                    binding.usageHistoryList.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                }//end of if
                            }//end of onFailure

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (!UlduzumUsageHistoryActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(UlduzumUsageHistoryActivity.this);
                                    binding.usageHistoryList.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                }
                            }//end of onError
                        });
    }
}
