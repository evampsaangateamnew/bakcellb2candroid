package com.bakcell.activities.landing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.databinding.ActivityTopupPlasticCardBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;

import static com.bakcell.globals.Constants.TOP_UP_INIT_PAYMENT_URL;
import static com.bakcell.utilities.BakcellLogger.logE;

public class TopupPlasticCardActivity extends BaseActivity {

    public static final String TOUP_PLASTIC_CARD_PREFIX = "topup.prefix";
    public static final String TOUP_PLASTIC_CARD_NUMBER = "topup.number";

    ActivityTopupPlasticCardBinding binding;
    String paymentKey = "";
    String resultValue = "";
    String errorValue = "";
    String languageKey = "";
    String[] parameters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_topup_plastic_card);
        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(TOP_UP_INIT_PAYMENT_URL)) {
            paymentKey = getIntent().getExtras().getString(TOP_UP_INIT_PAYMENT_URL);
        }

        initUiContent();

        initUiListeners();
    }

    private void initUiListeners() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);

    }

    private void initUiContent() {
        languageKey = AppClass.getCurrentLanguageKey(TopupPlasticCardActivity.this);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.top_up_lbl);

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //binding.webView.setWebViewClient(new WebViewClient());
        registerWebViewListener();
        //enable zoom view controls
        binding.webView.resumeTimers();//resume the timers if needed by the DOM
        binding.webView.getSettings().setDomStorageEnabled(true);// the url specified need DOM kind a model
        // Here we are using setJavaScriptEnabled(true);, This not required by IS , But we have to use it as Balkcell Golden Pay required it.
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setAppCacheEnabled(false);//for loading language based urls
        binding.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);//load no cache at all
        binding.webView.getSettings().setUseWideViewPort(true);
        binding.webView.getSettings().setSupportZoom(true);
        binding.webView.getSettings().setBuiltInZoomControls(true);
        binding.webView.setInitialScale(1);
        if (Tools.hasValue(paymentKey)) {
            logE("plasticUrl", "url:::" + paymentKey, "TopupPlasticCardActivity", "loadURL in webview");
            binding.webView.loadUrl(paymentKey);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void registerWebViewListener() {
        try {
            binding.webView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {

                    logE(Logger.TAG_CATCH_LOGS, "1 onPageFinished->webview", "TopupPlasticCardActivity " + url, "onPageFinished");

                }//onPageFinished ends

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    //Your code to do for handling the error
                    logE(Logger.TAG_CATCH_LOGS, "2 onReceivedError->webview:::" + error.toString(), "TopupPlasticCardActivity ", "onReceivedError");
                }//onReceivedError ends

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    if (url != null && url.contains("/user/account/topup?result=") && url.contains("result") && url.contains("error")) {
                        int resultIndex = url.indexOf("result");
                        int urlLengthIndex = url.length();
                        String extractedString = url.substring(resultIndex, urlLengthIndex);
                        if (extractedString != null && !extractedString.isEmpty()) {
                            parameters = extractedString.split("&");

                            if (languageKey != null && !languageKey.isEmpty() && parameters != null && parameters.length > 1 && parameters[0] != null && !parameters[0].isEmpty() && parameters[1] != null && !parameters[1].isEmpty() && parameters[0].split("=").length > 1 && parameters[1].split("=").length > 1)    {
                                resultValue = parameters[0].split("=")[1];
                                errorValue = parameters[1].split("=")[1];
                                String data;
                                if (errorValue != null && resultValue != null) {
                                    String mainKey = "goldenpay_message_" + errorValue + "_" + languageKey;
                                    if (DataManager.getInstance().getUserPredefinedData() != null && DataManager.getInstance().getUserPredefinedData().getGoldenPayMessages() != null) {
                                        data = DataManager.getInstance().getUserPredefinedData().getGoldenPayMessages().get(mainKey);
                                    } else {
                                        data = "";
                                    }

                                    if (resultValue != null && resultValue.equals("true")) {
                                        String message = getString(R.string.topup_plastic_card_successful_message);
                                        if (data != null && !data.isEmpty()) {
                                            message = data;
                                        }
                                        view.setVisibility(View.INVISIBLE);

                                        showMessageDialog(TopupPlasticCardActivity.this,
                                                getString(R.string.mesg_successful_title), message);
                                        //if result= true and error = 200, make refresh dashboard check to true
                                        if(errorValue.equals("200"))
                                        {
                                            RootValues.getInstance().setDashboardApiCall(true);
                                        }

                                    } else if (resultValue != null && resultValue.equals("false")) {
                                        String message = getString(R.string.biometric_general_error_label);
                                        if (data != null && !data.isEmpty()) {
                                            message = data;
                                        }
                                        view.setVisibility(View.INVISIBLE);

                                        showMessageDialog(TopupPlasticCardActivity.this,
                                                getString(R.string.bakcell_error_title), message);

                                    } else {
                                        showErrorDialogWithDefaultMessage(view);

                                    }

                                } else {
                                    showErrorDialogWithDefaultMessage(view);
                                }
                            } else {
                                showErrorDialogWithDefaultMessage(view);
                            }
                        } else {
                            showErrorDialogWithDefaultMessage(view);
                        }


                    }

                    view.loadUrl(url);

                    logE(Logger.TAG_CATCH_LOGS, "3 onPageFinished->webview", "TopupPlasticCardActivity " + url, "shouldOverrideUrlLoading");
                    return true;

                }//shouldOverrideUrlLoading ends
            });
        } catch (Exception exp) {
            logE(Logger.TAG_CATCH_LOGS, "registerListener:::" + exp.getMessage(), "TopupPlasticCardActivity", "onReceivedError");
        }//catch ends
    }//registerWebViewListener ends

    // Old Function used to load URL but now it is not used.
    private String getTopupPasticCardURL() {
        String url = null;
        String currentLanguage = AppClass.getCurrentLanguageKey(this);
        switch (currentLanguage) {
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN:
                if (DataManager.getInstance().getUserPredefinedData() != null &&
                        DataManager.getInstance().getUserPredefinedData().getRedirectionLinks() != null) {
                    url = DataManager.getInstance().getUserPredefinedData().getRedirectionLinks()
                            .getOnlinePaymentGatewayURL_EN();
                    logE(Logger.TAG_CATCH_LOGS, "url english:::" + url, "TopupPlasticCardActivity", "getTopupPasticCardURL");
                }
                break;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ:
                if (DataManager.getInstance().getUserPredefinedData() != null
                        && DataManager.getInstance().getUserPredefinedData().getRedirectionLinks() != null) {
                    url = DataManager.getInstance().getUserPredefinedData().getRedirectionLinks()
                            .getOnlinePaymentGatewayURL_AZ();
                    logE(Logger.TAG_CATCH_LOGS, "url azeri:::" + url, "TopupPlasticCardActivity", "getTopupPasticCardURL");
                }
                break;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU:
                if (DataManager.getInstance().getUserPredefinedData() != null &&
                        DataManager.getInstance().getUserPredefinedData().getRedirectionLinks() != null) {
                    url = DataManager.getInstance().getUserPredefinedData().getRedirectionLinks()
                            .getOnlinePaymentGatewayURL_RU();
                    logE(Logger.TAG_CATCH_LOGS, "url russian:::" + url, "TopupPlasticCardActivity", "getTopupPasticCardURL");
                }
                break;
        }
        //?prefix=\("55")&number=\(topUpToMSISDN)
        /*if (getIntent().getExtras() != null && getIntent().hasExtra(TOUP_PLASTIC_CARD_PREFIX)
                && getIntent().hasExtra(TOUP_PLASTIC_CARD_NUMBER)) {
            url = url + "?prefix=" + getIntent().getExtras().getString(TOUP_PLASTIC_CARD_PREFIX)
                    + "&number=" + getIntent().getExtras().getString(TOUP_PLASTIC_CARD_NUMBER);
        }*/
        //String url = "https://ode.goldenpay.az/merchant.html#/" + AppClass.getCurrentLanguageKey(TopupPlasticCardActivity.this) + "/parameters/bakcell3/bakcellecare";

        //get the mobile number from the intent
        String mobileNumber = "";
        if (getIntent().getExtras() != null && getIntent().hasExtra(TOUP_PLASTIC_CARD_NUMBER)) {
            mobileNumber = getIntent().getExtras().getString(TOUP_PLASTIC_CARD_NUMBER);
        }

        //get the prefix and the number from the mobile number
        String prefix = "";
        String number = "";
        /**the prefix will be the very first two digits of the mobile number
         * add them to the url as: &prefix=DD
         * and the number will the rest of the seven digits that is the mobile number minus first two chars*/
        if (mobileNumber != null) {
            if (mobileNumber.length() == 9) {
                try {
                    prefix = Character.toString(mobileNumber.charAt(0)) + mobileNumber.charAt(1);
                    number = Character.toString(mobileNumber.charAt(2)) +
                            mobileNumber.charAt(3) +
                            mobileNumber.charAt(4) +
                            mobileNumber.charAt(5) +
                            mobileNumber.charAt(6) +
                            mobileNumber.charAt(7) +
                            mobileNumber.charAt(8);
                } catch (Exception exp) {
                    logE("extractingMobileNumber", "error:::" + exp.toString(), "TopupPlasticCardActivity", "getTopupPasticCardURL");
                }
            }
        }

        /**
         * create the url as followed
         * https://hesab.az/unregistered/#/mobile/bakcell/parameters?lang=az&prefix=first two chars of the mobile number&number=last seven digits*/

        url = url + "&prefix=" + prefix + "&number=" + number;
        return url;
    }

    @Override
    public void onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack();
            return;
        }
        super.onBackPressed();
    }

    private void showMessageDialog(Context context, String title, String message) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_message, null);
            LinearLayout linearLayout = dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    setResult(RESULT_OK);
                    finish();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    void showErrorDialogWithDefaultMessage(WebView view) {
        if (view != null) {
            view.setVisibility(View.INVISIBLE);
        }
        showMessageDialog(TopupPlasticCardActivity.this, getString(R.string.bakcell_error_title), getString(R.string.biometric_general_error_label));
    }
}
