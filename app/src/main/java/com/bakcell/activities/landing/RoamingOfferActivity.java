package com.bakcell.activities.landing;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.adapters.CardAdapterSupplementaryOffers;
import com.bakcell.databinding.ActivityRoamingOfferBinding;
import com.bakcell.fragments.dialogs.BakcellFilterDialogFragment;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.SearchResultListener;
import com.bakcell.models.supplementaryoffers.OfferFilter;
import com.bakcell.models.supplementaryoffers.OfferFiltersMain;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.RecyclerItemDecorator;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

public class RoamingOfferActivity extends BaseActivity implements View.OnClickListener,
        SearchResultListener {
    public static final String KEY_ROAMING_OFFERS = "key.roaming.offers";
    public static final String KEY_ROAMING_FILTERS = "key.roaming.filters";
    public static final String KEY_COUNTRY_NAME = "key.roaming.country";
    public static final String KEY_FLAG = "key.roaming.flag";
    private static int OFFERS_LIST_ORIENTATION = LinearLayoutManager.HORIZONTAL;

    public static final String LAYOUT_PHONE = "phone";
    public static final String LAYOUT_TAB = "tab";
    public static final String LAYOUT_TV = "tv";


    private AlertDialog alertDialog;

    private ImageView iconlayout;
    private Toast toast;
    private ArrayList<OfferFilter> filteredIds;
    private String selectedLayout;

    private ActivityRoamingOfferBinding binding;
    private ArrayList<SupplementaryOffer> offerList;
    private int prevCenterPos;
    private boolean isSearchOpened;
    private CardAdapterSupplementaryOffers roamingAdapter;
    private OfferFiltersMain offerFilter;
    private ArrayList<SupplementaryOffer> filteredSupplementaryOffersList = new ArrayList<SupplementaryOffer>();
    private final String DIALOG_FILTER = "roaming.filter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_roaming_offer);
        Bundle bundle = getIntent().getExtras();

        if (bundle.containsKey(KEY_ROAMING_OFFERS)) {
            offerList = bundle.getParcelableArrayList(KEY_ROAMING_OFFERS);
        }
        if (bundle.containsKey(KEY_ROAMING_FILTERS)) {
            offerFilter = bundle.getParcelable(KEY_ROAMING_FILTERS);
        }
        if (bundle.containsKey(KEY_COUNTRY_NAME)) {
            String country = bundle.getString(KEY_COUNTRY_NAME);
            binding.tvCountryName.setText(country);
        }

        if (bundle.containsKey(KEY_FLAG)) {
            String flag = bundle.getString(KEY_FLAG);
            binding.flagImage.setImageDrawable(Tools.getImageFromAssests(this, flag));
        }


        //Clear filter static dataset before showing the filter dialog after the  activity creation
        if (RootValues.getInstance().getPhoneFilter() != null) {
            RootValues.getInstance().getPhoneFilter().clear();
        }
        if (RootValues.getInstance().getTabFilter() != null) {
            RootValues.getInstance().getTabFilter().clear();
        }
        if (RootValues.getInstance().getTvFilter() != null) {
            RootValues.getInstance().getTvFilter().clear();
        }

        initUiContent();
        initUiListeners();
    }

    @Override
    protected void onDestroy() {


        RootValues.getInstance().setPhoneFilter(null);
        RootValues.getInstance().setTabFilter(null);
        RootValues.getInstance().setTvFilter(null);

        RootValues.getInstance().setFilterGroupAll(null);

        super.onDestroy();
    }

    private void initUiContent() {
        //Seeting title and back icon in toolbar
        binding.toolbar.pageTitle.setText(R.string.activity_roaming_offers_lbl);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));

        //Setting right icons in toolbar
        binding.toolbar.offerListChangeLayout.setVisibility(View.VISIBLE);
        binding.toolbar.settingLayout.setVisibility(View.VISIBLE);
        binding.toolbar.searchMenu.setVisibility(View.VISIBLE);

        OFFERS_LIST_ORIENTATION = LinearLayoutManager.HORIZONTAL;
        binding.toolbar.listingIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.actionbar_sort));

        //Setting layout to recycler
        setOffersAdapter();
    }

    boolean isListCreated = false;
    boolean isCardCreated = false;

    private void setOffersAdapter() {
        binding.noSearchLayout.setVisibility(View.INVISIBLE);
        binding.mainLayout.setVisibility(View.VISIBLE);
        if (RoamingOfferActivity.OFFERS_LIST_ORIENTATION == LinearLayoutManager.VERTICAL) {
            binding.roamingOffers.setVisibility(View.VISIBLE);
            binding.mDiscreteView.setVisibility(View.INVISIBLE);
            if (!isListCreated) {
                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                layoutManager.setOrientation(OFFERS_LIST_ORIENTATION);
                binding.roamingOffers.setLayoutManager(layoutManager);
                roamingAdapter = new CardAdapterSupplementaryOffers(this,this, offerList, this, false, true, true, getString(R.string.supplementary_offer_tab_title_roaming));
                binding.roamingOffers.setAdapter(roamingAdapter);
                setUpPagination(binding.roamingOffers);
                isListCreated = true;
            }
        } else {
            //This code make recylerview same like pagger view one item at a time on screen

            binding.mDiscreteView.setVisibility(View.VISIBLE);
            binding.roamingOffers.setVisibility(View.INVISIBLE);
            binding.mDiscreteView.setOrientation(Orientation.HORIZONTAL);
            if (!isCardCreated) {
                binding.mDiscreteView.setItemTransformer(new ScaleTransformer.Builder()
                        .build());
                binding.mDiscreteView.addItemDecoration(new RecyclerItemDecorator());
                roamingAdapter = new CardAdapterSupplementaryOffers(this,this,
                        offerList, this, true, true, true
                        , getString(R.string.supplementary_offer_tab_title_roaming));
                binding.mDiscreteView.setAdapter(roamingAdapter);
                setUpPagination(binding.mDiscreteView);
                isCardCreated = true;
            }
        }


    }

    public void setUpPagination(RecyclerView recyclerView) {
        if (roamingAdapter == null) return;
        if (roamingAdapter.getItemCount() > 0) {
            binding.tvPagination.setText("1/" + roamingAdapter.getItemCount());
        } else {
            binding.tvPagination.setText("0/0");
        }
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int center = recyclerView.getWidth() / 2;
                View centerView = recyclerView.findChildViewUnder(center, recyclerView.getTop());
                int centerPos = recyclerView.getChildAdapterPosition(centerView);

                if (prevCenterPos != centerPos) {
                    // dehighlight the previously highlighted view
                    prevCenterPos = centerPos;
                }
                Logger.debugLog("POSTION", prevCenterPos + "");
                binding.tvPagination.setText(prevCenterPos + 1 + "/" +
                        recyclerView.getAdapter().getItemCount());
            }
        });
    }

    private void initUiListeners() {
        binding.toolbar.offerListChangeLayout.setOnClickListener(this);
        binding.toolbar.settingLayout.setOnClickListener(this);
        binding.toolbar.searchMenu.setOnClickListener(this);
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.toolbar.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                roamingAdapter.getFilter().filter(s.toString());
                if (roamingAdapter.getItemCount() > 0) {
                    binding.tvPagination.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.tvPagination.setText(1 + "/" + roamingAdapter.getItemCount());
                        }
                    }, 10);
                } else {
                    binding.tvPagination.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.tvPagination.setText(0 + "/" + roamingAdapter.getItemCount());
                        }
                    }, 10);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeListLayout() {
        if (RoamingOfferActivity.OFFERS_LIST_ORIENTATION == LinearLayoutManager.VERTICAL) {
            RoamingOfferActivity.OFFERS_LIST_ORIENTATION = LinearLayoutManager.HORIZONTAL;
            binding.toolbar.listingIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.actionbar_sort));
        } else {
            RoamingOfferActivity.OFFERS_LIST_ORIENTATION = LinearLayoutManager.VERTICAL;
            binding.toolbar.listingIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_listicon));
        }
        setOffersAdapter();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.search_menu:
                if (!isSearchOpened) {
                    binding.toolbar.etSearch.requestFocus();
                    Tools.showKeyboard(this);
                    binding.toolbar.etSearch.setText("");
                    binding.toolbar.etSearch.setHint(getString(R.string.tool_bar_search_lbl));
                    binding.toolbar.pageTitle.setVisibility(View.GONE);
                    binding.toolbar.searchView.setVisibility(View.VISIBLE);
                    binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.ic_cross));
                    isSearchOpened = true;
                } else {
                    Tools.hideKeyboard(this);
                    binding.toolbar.pageTitle.setVisibility(View.VISIBLE);
                    binding.toolbar.searchView.setVisibility(View.GONE);
                    binding.toolbar.icSearch.setImageDrawable(ContextCompat.getDrawable(this,
                            R.drawable.actionbar_search));
                    isSearchOpened = false;
                    setOffersAdapter();
                }
                break;
            case R.id.offer_list_change_layout:
                changeListLayout();
                break;
            case R.id.setting_layout:
                showFilterAlert();
                break;
        }
    }

    private void showFilterAlert() {
        if (offerFilter != null) {
            BakcellFilterDialogFragment
                    .newInstance(offerFilter, true)
                    .show(getSupportFragmentManager(), DIALOG_FILTER);
        } else {
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(this, "No filter available", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void performFiltering(ArrayList<OfferFilter> filteredIds, String offerGroupFilterAll) {
        if (filteredIds == null ||
                (Tools.hasValue(offerGroupFilterAll) && offerGroupFilterAll.toLowerCase()
                        .contains(CardAdapterSupplementaryOffers.OFFER_GROUP_MOBILE.toLowerCase()))) {
            showNoResultFound(true, R.string.no_offer_found);
            roamingAdapter.updateAdapter(offerList);
            binding.tvPagination.setText(1 + "/" + roamingAdapter.getItemCount());
        } else if (filteredIds.size() > 0 && offerList != null) {
            filteredSupplementaryOffersList = new ArrayList<>();
            for (int i = 0; i < offerList.size(); i++) {
                if (offerList.get(i).getHeader() != null) {

                    boolean isFilterExist = false;
                    for (int j = 0; j < filteredIds.size(); j++) {
                        if (Tools.hasValue(filteredIds.get(j).getKey())
                                && Tools.hasValue(offerList.get(i).getHeader().getAppOfferFilter())) {
                            if (offerList.get(i).getHeader().getAppOfferFilter().trim().toLowerCase()
                                    .contains(filteredIds.get(j).getKey().trim().toLowerCase())) {
                                isFilterExist = true;
                                break;
                            }
                        }
                    }
                    if (isFilterExist) {
                        filteredSupplementaryOffersList.add(offerList.get(i));
                    }

                }
            }
            if (filteredSupplementaryOffersList.size() > 0) {
                showNoResultFound(true, R.string.no_offer_found);
                roamingAdapter.updateAdapter(filteredSupplementaryOffersList);
                binding.tvPagination.setText(1 + "/" + roamingAdapter.getItemCount());
                //adapterSupplementaryOffers.notifyDataSetChanged();
            } else {
                showNoResultFound(false, R.string.no_offer_found);
            }
        } else if (filteredIds.size() == 0) {
            showNoResultFound(true, R.string.no_offer_found);
            roamingAdapter.updateAdapter(offerList);
            if (binding.mDiscreteView.getVisibility() == View.VISIBLE) {
                binding.mDiscreteView.scrollToPosition(0);
            }

            if (binding.roamingOffers.getVisibility() == View.VISIBLE) {
                binding.roamingOffers.scrollToPosition(0);
            }
            binding.tvPagination.setText(1 + "/" + roamingAdapter.getItemCount());
        }


        Fragment prev = getSupportFragmentManager().findFragmentByTag(DIALOG_FILTER);
        if (prev != null) {
            BakcellFilterDialogFragment df = (BakcellFilterDialogFragment) prev;
            df.dismiss();
        }
    }

    @Override
    public void onSearchResult(final boolean isResultFound) {
        showNoResultFound(isResultFound, R.string.no_results_found);
    }

    private void showNoResultFound(final boolean isResultFound, @StringRes final int id) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isResultFound) {
                    binding.noSearchLayout.setVisibility(View.INVISIBLE);
                    binding.mainLayout.setVisibility(View.VISIBLE);
                } else {
                    binding.noSearchLayout.setVisibility(View.VISIBLE);
                    binding.mainLayout.setVisibility(View.INVISIBLE);
                    binding.tvNoResultFound.setText(id);
                }
            }
        });
    }
}
