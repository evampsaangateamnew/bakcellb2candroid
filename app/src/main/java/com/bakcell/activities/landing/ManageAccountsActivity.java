package com.bakcell.activities.landing;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.activities.authentications.LoginActivity;
import com.bakcell.adapters.ManageAccountsRecyclerAdapter;
import com.bakcell.databinding.ActivityManageAccountsBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.ManageAccountSwitchingEvents;
import com.bakcell.interfaces.ManageAccountsShowHideContentsEvents;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;

import java.util.ArrayList;

public class ManageAccountsActivity extends BaseActivity {

    private ActivityManageAccountsBinding activityManageAccountsBinding;
    private final String fromClass = "ManageAccountsActivity";

    @Override
    protected void onResume() {
        super.onResume();
        initManageAccountEvents();
    }

    private void initManageAccountEvents() {
        ManageAccountSwitchingEvents manageAccountSwitchingEvents = new ManageAccountSwitchingEvents() {
            @Override
            public void onAccountSwitchFromManageAccountsRecycler(UserModel userModel) {
                MultiAccountsHandler.updateCurrentlyLoggedInUserModelWithNewUserModel(ManageAccountsActivity.this, userModel);
                /**reset the user data for apis calling, menus and promo message*/
                MultiAccountsHandler.resetAllUserDataAndAPICallerCaches(ManageAccountsActivity.this);
                /**restart the current activity*/
                BaseActivity.startNewActivityAndClear(ManageAccountsActivity.this, HomeActivity.class);
            }
        };

        RootValues.getInstance().setManageAccountSwitchingEvents(manageAccountSwitchingEvents);
    }//initManageAccountEvents ends

    private void getIntentData() {
        String fromForceLogout = "";
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(MultiAccountsHandler.MultiAccountConstants.FROM_FORCE_LOGOUT)) {
            if (getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.FROM_FORCE_LOGOUT) != null &&
                    getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.FROM_FORCE_LOGOUT).
                            equalsIgnoreCase(MultiAccountsHandler.MultiAccountConstants.FROM_FORCE_LOGOUT_VALUE)) {
                fromForceLogout = MultiAccountsHandler.MultiAccountConstants.FROM_FORCE_LOGOUT_VALUE;
                activityManageAccountsBinding.titleTV.setText(getResources().getString(R.string.force_logout_title));
            }
        }

        BakcellLogger.logE("fromKeyX", "isFromManageAccountsFlow:::" + fromForceLogout, fromClass, "getIntentData");
    }//getIntentData ends

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityManageAccountsBinding = DataBindingUtil.setContentView(this, R.layout.activity_manage_accounts);

        getIntentData();
        initActionBarHolder();
        initUIContent();
        initUIEvents();
        processCurrentlyLoggedInUsers();
    }//onCreate ends

    private void processCurrentlyLoggedInUsers() {
        if (MultiAccountsHandler.getCustomerDataFromPreferences(ManageAccountsActivity.this).userModelArrayList != null &&
                !MultiAccountsHandler.getCustomerDataFromPreferences(ManageAccountsActivity.this).userModelArrayList.isEmpty()) {

            /**process the users accounts data to the recyclerview*/
            ArrayList<UserModel> userModelArrayList =
                    MultiAccountsHandler.getOrderedMultiAccountsList(
                            MultiAccountsHandler.getCustomerDataFromPreferences(ManageAccountsActivity.this).userModelArrayList);

            if (userModelArrayList != null && !userModelArrayList.isEmpty()) {
                showContents();
            } else {
                hideContents();
                return;//safe passage
            }

            //creating the adapter
            ManageAccountsRecyclerAdapter manageAccountsRecyclerAdapter =
                    new ManageAccountsRecyclerAdapter(
                            ManageAccountsActivity.this, getApplicationContext(), userModelArrayList);
            //layout manager
            LinearLayoutManager layoutManager = new LinearLayoutManager(
                    getApplicationContext(), LinearLayout.VERTICAL, false);
            //setting layout manager
            activityManageAccountsBinding.newAccountsRecycler.setLayoutManager(layoutManager);
            //setting the recycler view
            activityManageAccountsBinding.newAccountsRecycler.setAdapter(manageAccountsRecyclerAdapter);
        } else {
            BakcellLogger.logE("cuRenX", "no users logged in!!!", fromClass, "processCurrentlyLoggedInUsers");
            hideContents();
        }
    }//processCurrentlyLoggedInUsers ends

    private void showContents() {
        activityManageAccountsBinding.newAccountsRecycler.setVisibility(View.VISIBLE);
        activityManageAccountsBinding.noDataFoundLayout.setVisibility(View.GONE);
    }//showContents ends

    private void hideContents() {
        activityManageAccountsBinding.noDataFoundLayout.setVisibility(View.VISIBLE);
        activityManageAccountsBinding.newAccountsRecycler.setVisibility(View.GONE);
    }//hideContents ends

    private void initManageAccountsShowHideContentsEvents() {
        ManageAccountsShowHideContentsEvents manageAccountsShowHideContentsEvents = new ManageAccountsShowHideContentsEvents() {
            @Override
            public void onShowOrHideContents(boolean isShowContents) {
                if (isShowContents) {
                    showContents();
                } else {
                    hideContents();
                }
            }
        };

        RootValues.getInstance().setManageAccountsShowHideContentsEvents(manageAccountsShowHideContentsEvents);
    }//ManageAccountsShowHideContentsEvents ends

    private void initUIEvents() {
        initManageAccountsShowHideContentsEvents();
        activityManageAccountsBinding.manageAccountsActionBar.ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        activityManageAccountsBinding.addNewAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**count the users,
                 * if the currently logged in users are equal to 5
                 * show the maximum users logged in error message*/
                ArrayList<UserModel> userModelArrayList =
                        MultiAccountsHandler.getCustomerDataFromPreferences(ManageAccountsActivity.this).userModelArrayList;

                if (userModelArrayList != null && !userModelArrayList.isEmpty() && userModelArrayList.size() == 5) {
                    BakcellPopUpDialog.showMessageDialog(
                            ManageAccountsActivity.this,
                            getResources().getString(R.string.bakcell_error_title),
                            getResources().getString(R.string.maximum_number_added_msg));
                } else {
                    //redirect the user to the login screen to add a new account
                    Bundle bundle = new Bundle();
                    bundle.putString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY,
                            MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE);
                    BaseActivity.startNewActivity(ManageAccountsActivity.this, LoginActivity.class, bundle);//start the login activity
                }
            }
        });
    }//initUIEvents ends

    private void initUIContent() {
        activityManageAccountsBinding.addNewAccountButton.setSelected(true);
    }//initUIContent ends

    private void initActionBarHolder() {
        activityManageAccountsBinding.manageAccountsActionBar.notificationMenu.setVisibility(View.INVISIBLE);
        activityManageAccountsBinding.manageAccountsActionBar.ivHome.setImageDrawable(
                ContextCompat.getDrawable(this, R.drawable.arrowleft));
        activityManageAccountsBinding.manageAccountsActionBar.pageTitle.setText(R.string.manageTV);
    }//initActionBarHolder ends
}//class ens
