package com.bakcell.activities.landing;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityChangeEmailBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.Email.DataEmail;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestEmailUpdateService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Zeeshan Shabbir on 8/17/2017.
 */

public class ChangeEmailActivity extends BaseActivity implements View.OnClickListener {
    ActivityChangeEmailBinding binding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_email);

        initUiContent();
        initUiListeners();

        AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SCREEN,
                AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SCREEN,
                AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SCREEN);
    }


    private void initUiContent() {
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.activity_change_email_lbl);
    }

    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.btn_update:
                if (binding.confirmEmailInput.getText().toString().length() == 0) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_change_activity_enter_email_message));
                    return;
                }
                if (!FieldFormatter.isEmailValid(binding.confirmEmailInput.getText().toString())) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_change_activity_enter_valid_email_message));
                    return;
                }


                updateClicked(binding.confirmEmailInput.getText().toString());
                break;
        }
    }


    private void requestForEmailUpdate(final String email) {


        RequestEmailUpdateService.newInstance(ChangeEmailActivity.this, ServiceIDs.REQUEST_UPDATE_EMAIL)
                .execute(DataManager.getInstance().getCurrentUser(), email,
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                String result = response.getData();

                                DataEmail dataEmail = new Gson().fromJson(result, DataEmail.class);

                                if (dataEmail != null && Tools.hasValue(dataEmail.getMessage())) {
//                                    DataManager.getInstance().updateCurrentUserEmail(ChangeEmailActivity.this, email);
                                    showSuccessfulDialog(dataEmail.getMessage());
                                    if (RootValues.getInstance().getEmailChangedListener() != null && email != null) // Fixed NPE exception
                                        RootValues.getInstance().getEmailChangedListener().onEmailChangedListen(email);
                                }

                                AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SCREEN,
                                        AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SUCCESS,
                                        AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SCREEN);

                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(ChangeEmailActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(ChangeEmailActivity.this);
                                    return;
                                }
                                if (response != null) {
                                    BakcellPopUpDialog.showMessageDialog(ChangeEmailActivity.this,
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }

                                AppEventLogs.applyAppEvent(AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SCREEN,
                                        AppEventLogValues.ProfileScreen.CHANGE_EMAIL_FAILURE,
                                        AppEventLogValues.ProfileScreen.CHANGE_EMAIL_SCREEN);
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                BakcellPopUpDialog.ApiFailureMessage(ChangeEmailActivity.this);
                            }

                        });

    }


    protected void updateClicked(final String email) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_top_up_confirmation, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.balance_layout);
        linearLayout.setVisibility(View.INVISIBLE);
        BakcellTextViewNormal tvConfirmationMsg = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_top_confirmation_msg);
        tvConfirmationMsg.setText(getString(R.string.email_update_confirmation));
        Button yesBtn = (Button) dialogView.findViewById(R.id.btn_yes);
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                requestForEmailUpdate(email);
                alertDialog.dismiss();
            }
        });

        Button noBtn = (Button) dialogView.findViewById(R.id.btn_no);
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void showSuccessfulDialog(String message) {

        try {

            android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(ChangeEmailActivity.this);
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_message, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(ChangeEmailActivity.this, R.color.white));
            }

            final android.app.AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(R.string.mesg_successful_title);
            tvMessage.setText(message);
            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    finish();
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }


}
