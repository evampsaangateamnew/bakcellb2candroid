package com.bakcell.activities.landing;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.databinding.ActivityFrbtBinding;

public class FRBTActivity extends BaseActivity implements View.OnClickListener {

    ActivityFrbtBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_frbt);

        initUiContent();

        initUiListeners();
    }

    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
    }

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText("FRBT");
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
        }

    }
}
