package com.bakcell.activities.landing;


import android.app.DatePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.DatePicker;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.adapters.ExpandableAdapterOperationsHistory;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityOperationsHistoryBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.interfaces.DisclaimerDialogListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.operationhistory.OperationHistoryMain;
import com.bakcell.models.operationhistory.OperationHistoryRecord;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestOperationHistoryService;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellCalendar;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 18-May-17.
 */

public class OperationsHistoryActivity extends BaseActivity implements View.OnClickListener {

    ActivityOperationsHistoryBinding binding;

    public static final String TRANSITION_ALL_EN = "All";
    public static final String TRANSITION_ALL_RU = "Все";
    public static final String TRANSITION_ALL_AZ = "Hamısı";

    public static final String TRANSITION_OTHERS_EN = "Others";
    public static final String TRANSITION_OTHERS_RU = "Другие";
    public static final String TRANSITION_OTHERS_AZ = "Digərlər";

    private ExpandableAdapterOperationsHistory expandableAdapterOperationsHistory;

    private String startDate = "", endDate = "", date = "";

    boolean flagDate = false;

    String dateOne = "";
    String dateTwo = "";

    String transitionType = null;
    private OperationHistoryMain operationHistoryMain;
    private SpinnerAdapterOperationHistoryFilter spinnerAdapterOperationHistoryFilter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_operations_history);

        iniUIContent();

        iniUIListerners();

        try {
            setDefaultDate();
            date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.CURRENT_DAY);
            updateDates(date, Tools.getCurrentDate());
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        loadOperationHistoryList(null);


        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.OPERATION_HISTORY_SCREEN,
                AppEventLogValues.AccountEvents.OPERATION_HISTORY_SCREEN,
                AppEventLogValues.AccountEvents.OPERATION_HISTORY_SCREEN);


    }

    private void iniUIListerners() {

        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.tvDateOne.setOnClickListener(this);
        binding.tvDateTwo.setOnClickListener(this);

        binding.spinnerTransationTypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.spinnerTransationType.performClick();
            }
        });

        binding.spinnerPeriodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.spinnerPeriod.performClick();
            }
        });


        binding.spinnerTransationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                transitionType = binding.spinnerTransationType.getSelectedItem().toString();
                filterUsageDetails(transitionType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                    if (flagDate) {
                        try {
                            binding.llCalendar.setVisibility(View.GONE);
                            updateDates(Tools.getCurrentDate(), Tools.getCurrentDate());

                        } catch (ParseException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                    } else {
                        flagDate = true;
                    }

                } else if (position == 1) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_SEVEN_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 2) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_THIRTY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 3) {
                    binding.llCalendar.setVisibility(View.GONE);
                    date = "";
                    String maxDate = "";
                    try {
                        date = Tools.getLastMonthFirstDate();
                        maxDate = Tools.getLastMonthLastDate();

                        updateDates(date, maxDate);

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                } else if (position == 4) {
                    try {
                        setDefaultDate();
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                    binding.llCalendar.setVisibility(View.VISIBLE);
                    binding.tvDateOne.setTextColor(ContextCompat.getColor(view.getContext(), R.color.text_gray));
                    binding.tvDateTwo.setTextColor(ContextCompat.getColor(view.getContext(), R.color.text_gray));
                } else {
                    binding.llCalendar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    private void iniUIContent() {


        if (DataManager.getInstance().getCurrentUser() != null && Tools.hasValue(DataManager
                .getInstance().getCurrentUser().getSubscriberType())) {
            if (DataManager.getInstance().getCurrentUser().getSubscriberType()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_POSTPAID)) {
                binding.endingBalance.setVisibility(View.GONE);
            } else {
                binding.endingBalance.setVisibility(View.VISIBLE);
            }
        }

        binding.toolbar.pageTitle.setText(R.string.title_operation_history);

        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));

        binding.paggerNoTxt.setText("0/0");

        ArrayList<OperationHistoryRecord> recordArrayList = new ArrayList<>();

        expandableAdapterOperationsHistory = new ExpandableAdapterOperationsHistory(this, recordArrayList);
        binding.expandableListview.setAdapter(expandableAdapterOperationsHistory);

        spinnerAdapterOperationHistoryFilter = new SpinnerAdapterOperationHistoryFilter(this, getResources().getStringArray(R.array.transation_type_filter));
        binding.spinnerTransationType.setAdapter(spinnerAdapterOperationHistoryFilter);

        SpinnerAdapterOperationHistoryFilter spinnerAdapterPeriodFilter = new SpinnerAdapterOperationHistoryFilter(this, getResources().getStringArray(R.array.period_filter));
        binding.spinnerPeriod.setAdapter(spinnerAdapterPeriodFilter);


        binding.expandableListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                binding.paggerNoTxt.setText((firstVisibleItem + visibleItemCount) + "/" + totalItemCount);

            }
        });


    }


    private void updateDates(String newStartDate, String newEndDate) {

        startDate = newStartDate;
        endDate = newEndDate;
        requestForGetOperationsHistory(startDate, endDate);
    }


    ArrayList<OperationHistoryRecord> filterList = new ArrayList<>();

    private void filterUsageDetails(String item) {
        if (!Tools.hasValue(item)) return;

        if (item.equalsIgnoreCase(TRANSITION_ALL_EN) || item.equalsIgnoreCase(TRANSITION_ALL_AZ)
                || item.equalsIgnoreCase(TRANSITION_ALL_RU)) {
            if (operationHistoryMain == null) return;
            loadOperationHistoryList(operationHistoryMain.getRecords());
        } else if (item.equalsIgnoreCase(TRANSITION_OTHERS_EN) || item.equalsIgnoreCase(TRANSITION_OTHERS_AZ)
                || item.equalsIgnoreCase(TRANSITION_OTHERS_RU)) {
            if (operationHistoryMain == null) return;
            filterList.clear();
            String[] filterArray = getResources().getStringArray(R.array.transation_type_filter);
            HashMap<String, String> hashMap = new HashMap<>();
            List<String> list = Arrays.asList(filterArray);
            //list.remove(11);
//                list.remove(list.size() - 1);
            for (int i = 0; i < list.size() - 1; i++) {
                hashMap.put(list.get(i).toLowerCase(), list.get(i));
            }
            if (operationHistoryMain != null) {
                if (operationHistoryMain.getRecords() != null) {
                    for (int i = 0; i < operationHistoryMain.getRecords().size(); i++) {
                        if (Tools.hasValue(operationHistoryMain.getRecords().get(i)
                                .getTransactionType())) {
                            if (hashMap.containsKey(operationHistoryMain.getRecords().get(i).getTransactionType().toLowerCase())) {

                            } else {
                                filterList.add(operationHistoryMain.getRecords().get(i));
                            }
                        }
                    }
                }
                loadOperationHistoryList(filterList);
            }
        } else {
            filterList.clear();
            if (operationHistoryMain != null) {
                if (operationHistoryMain.getRecords() != null) {
                    for (int i = 0; i < operationHistoryMain.getRecords().size(); i++) {
                        if (Tools.hasValue(operationHistoryMain.getRecords().get(i)
                                .getTransactionType())) {
                            if (item.equalsIgnoreCase(operationHistoryMain.getRecords()
                                    .get(i).getTransactionType().trim())) {
                                filterList.add(operationHistoryMain.getRecords().get(i));
                            }
                        }
                    }
                }
                loadOperationHistoryList(filterList);
            }
        }

        /*switch (item.toLowerCase()) {
            case TRANSITION_ALL:
                if (operationHistoryMain == null) return;
                loadOperationHistoryList(operationHistoryMain.getRecords());
                break;
            case TRANSITION_OTHERS:
                if (operationHistoryMain == null) return;
                filterList.clear();
                String[] filterArray = getResources().getStringArray(R.array.transation_type_filter);
                HashMap<String, String> hashMap = new HashMap<>();
                List<String> list = Arrays.asList(filterArray);
                //list.remove(11);
//                list.remove(list.size() - 1);
                for (int i = 0; i < list.size() - 1; i++) {
                    hashMap.put(list.get(i).toLowerCase(), list.get(i));
                }
                if (operationHistoryMain != null) {
                    if (operationHistoryMain.getRecords() != null) {
                        for (int i = 0; i < operationHistoryMain.getRecords().size(); i++) {
                            if (Tools.hasValue(operationHistoryMain.getRecords().get(i)
                                    .getTransactionType())) {
                                if (hashMap.containsKey(operationHistoryMain.getRecords().get(i).getTransactionType().toLowerCase())) {

                                } else {
                                    filterList.add(operationHistoryMain.getRecords().get(i));
                                }
                            }
                        }
                    }
                    loadOperationHistoryList(filterList);
                }
                break;
            default:
                filterList.clear();
                if (operationHistoryMain != null) {
                    if (operationHistoryMain.getRecords() != null) {
                        for (int i = 0; i < operationHistoryMain.getRecords().size(); i++) {
                            if (Tools.hasValue(operationHistoryMain.getRecords().get(i)
                                    .getTransactionType())) {
                                if (item.equalsIgnoreCase(operationHistoryMain.getRecords()
                                        .get(i).getTransactionType().trim())) {
                                    filterList.add(operationHistoryMain.getRecords().get(i));
                                }
                            }
                        }
                    }
                    loadOperationHistoryList(filterList);
                }
                break;
        }*/
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                return;
            case R.id.tv_date_one:
                try {
                    showCalendar(binding.tvDateOne, true);//false for max date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
            case R.id.tv_date_two:
                try {
                    showCalendar(binding.tvDateTwo, false);//true for min date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;


        }
    }


    public void showCalendar(BakcellTextViewNormal textViewNormal, boolean isStartDateSelected) throws ParseException {

        try {
            dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());

            Date minDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
            Date maxDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);

            BakcellCalendar bakcellCalendar;

            final Calendar calendar = Calendar.getInstance();
            if (isStartDateSelected) {
                calendar.setTime(minDate);
            } else {
                calendar.setTime(maxDate);
            }
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            bakcellCalendar = new BakcellCalendar(textViewNormal, ondate);


            DatePickerDialog datePickerDialog = new DatePickerDialog(OperationsHistoryActivity.this, bakcellCalendar,
                    year, month, day);
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {

                Calendar calndPre = Calendar.getInstance();
                calndPre.add(Calendar.MONTH, -3);
                calndPre.set(Calendar.DATE, 1);
                datePickerDialog.getDatePicker().setMinDate(calndPre.getTimeInMillis());

                Date dateMax = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);
                calendar.setTime(dateMax);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());

            } else {

                Date dateMin = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
                calendar.setTime(dateMin);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("");//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show();
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            try {
                dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            try {
                dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            updateDates(dateOne, dateTwo);
        }
    };

    public void requestForGetOperationsHistory(String startDate, String endDate) {


        RequestOperationHistoryService.newInstance(OperationsHistoryActivity.this, ServiceIDs.REQUEST_OPERATION_HISTORY).execute(DataManager.getInstance().getCurrentUser(),
                startDate, endDate, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        operationHistoryMain = new Gson().fromJson(result, OperationHistoryMain.class);

                        if (operationHistoryMain != null) {
                            loadOperationHistoryList(operationHistoryMain.getRecords());
                            if (Tools.hasValue(transitionType)) {
                                filterUsageDetails(transitionType);
                            }
                        }//end of if

                        showDisclaimerDialog();

                    }//end of onSuccess


                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(OperationsHistoryActivity.this,
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        BakcellPopUpDialog.ApiFailureMessage(OperationsHistoryActivity.this);
                    }//end of onError
                });

    }

    private void showDisclaimerDialog() {
        if (PrefUtils.getBoolean(this,
                MultiAccountsHandler.CacheKeys.getOperationalHistoryDisclaimerPopupCacheKey(getApplicationContext()), true))
            BakcellPopUpDialog.showMessageDisclaimerDialog(this, getString(R.string.lbl_disclaimer), getString(R.string.message_disclaimer_message), new DisclaimerDialogListener() {
                @Override
                public void onDisclaimerDialogOkListner(boolean checked) {
                    if (checked) {
                        PrefUtils.addBoolean(OperationsHistoryActivity.this,
                                MultiAccountsHandler.CacheKeys.getOperationalHistoryDisclaimerPopupCacheKey(getApplicationContext()), false);
                    }
                }
            });

    }

    private void loadOperationHistoryList(ArrayList<OperationHistoryRecord> operationHistoryMainList) {

        if (operationHistoryMainList != null && operationHistoryMainList.size() > 0 && expandableAdapterOperationsHistory != null) {
            binding.expandableListview.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            expandableAdapterOperationsHistory.updateList(operationHistoryMainList);
        } else {
            binding.expandableListview.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            binding.paggerNoTxt.setText("0/0");
        }

    }


    private void setDefaultDate() throws ParseException {
        binding.tvDateOne.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
        binding.tvDateTwo.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
    }


}
