package com.bakcell.activities.landing;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityForwardNumberBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.models.DataManager;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestEnableDisableCoreService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class AddForwardNumberActivity extends BaseActivity implements View.OnClickListener {

    public static final String CALL_FARWORD_OFFERID = "key.offering_id";
    public static final String CALL_FARWORD_NUMBER = "key.offering_number";

    ActivityForwardNumberBinding binding;

    public static final String FORWARD_NUMBER = "forward.number";

    private String offerid = "", lastAddedNumber = "";

    ImageView ivHome;

    TextView pageTitle;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forward_number);
        intent = getIntent();

        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(CALL_FARWORD_OFFERID)) {
            offerid = intent.getExtras().getString(CALL_FARWORD_OFFERID);
        }
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(CALL_FARWORD_NUMBER)) {
            lastAddedNumber = intent.getExtras().getString(CALL_FARWORD_NUMBER);
        }

        initUiContent();

        initUiListeners();
    }

    private void initUiListeners() {
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
    }

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        ivHome = binding.toolbar.ivHome;
        pageTitle = binding.toolbar.pageTitle;
        binding.btnSave.setOnClickListener(this);
        ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        pageTitle.setText(R.string.core_services);

        if (Tools.hasValue(lastAddedNumber)) {
            binding.mobileInput.setText(lastAddedNumber);
        } else {
            binding.mobileInput.setText("");
        }

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
            case R.id.btn_save:
                if (binding.mobileInput.getText().toString().trim().length() == 0) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_top_up_enter_mobile_number_message));
                } else {
                    if (binding.mobileInput.getText().toString().trim().length() != 9) {
                        BakcellPopUpDialog.showMessageDialog(this,
                                getString(R.string.bakcell_error_title),
                                getString(R.string.error_msg_invalid_number));
                    } else {
                        saveNumber(binding.mobileInput.getText().toString());
                    }
                }
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    private void saveNumber(String msisdn) {
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestEnableDisableCoreService.newInstance(this,
                    ServiceIDs.REQUEST_ENABLE_DISABLE_CORE_SERVICE).execute(userModel, "1",
                    offerid, msisdn, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            setResult(RESULT_OK, getIntent().putExtra(FORWARD_NUMBER, binding.mobileInput.getText().toString()));
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(AddForwardNumberActivity.this, getString(R.string.mesg_successful_title), response.getDescription());
                            }


                            AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                                    AppEventLogValues.ServiceEvents.FORWARD_NUMBER_SUCCESS,
                                    AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN);

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {

                            // Check if user logout by server
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(AddForwardNumberActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(AddForwardNumberActivity.this);
                                return;
                            }
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(AddForwardNumberActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }


                            AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                                    AppEventLogValues.ServiceEvents.FORWARD_NUMBER_FAILURE,
                                    AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN);
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(AddForwardNumberActivity.this);
                        }
                    });
        }
    }
}
