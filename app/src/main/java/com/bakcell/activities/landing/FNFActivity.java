package com.bakcell.activities.landing;

import android.app.AlertDialog;
import android.content.Context;

import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.AdapterFnF;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityFfBinding;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.friendfamily.FriendFamily;
import com.bakcell.models.friendfamily.FriendFamilyMain;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestAddFriendAndFamilyService;
import com.bakcell.webservices.RequestDeleteFriendAndFamilyService;
import com.bakcell.webservices.RequestGetFriendAndFamilyService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class FNFActivity extends BaseActivity implements View.OnClickListener, AdapterFnF.UpdateNumberCounter {

    ActivityFfBinding binding;

    private AdapterFnF adapterFnF;

    private int fnfMaxCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ff);

        initUiContent();

        initUiListener();

        requestGetFriendsAndFamily();
    }

    private void initUiListener() {
        binding.ivAddImage.setOnClickListener(this);
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
    }

    private void initUiContent() {
        binding.tvNumberCounter.setText("0/" + fnfMaxCount);
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(getString(R.string.friends_amp_family));
        ArrayList<FriendFamily> fnfs = new ArrayList<>();
        adapterFnF = new AdapterFnF(fnfs, this, this);
        binding.lvFnf.setAdapter(adapterFnF);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ivAddImage:
                final String number = binding.mobileInput.getText().toString();

                if (!Tools.hasValue(number)) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_top_up_enter_mobile_number_message));
                    return;
                } else if (!FieldFormatter.isValidMsisdn(number)) {
                    BakcellPopUpDialog.showMessageDialog(this,
                            getString(R.string.bakcell_error_title),
                            getString(R.string.error_msg_invalid_number));
                    return;
                }
                Tools.hideKeyboard(this);
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(R.layout.dialog_top_up_confirmation, null);
                dialogBuilder.setView(dialogView);
                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                LinearLayout rootLayout = dialogView.findViewById(R.id.root);
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    rootLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
                }
                LinearLayout balance_layout = dialogView.findViewById(R.id.balance_layout);
                balance_layout.setVisibility(View.GONE);

                BakcellButtonNormal yesBtn = dialogView.findViewById(R.id.btn_yes);
                BakcellButtonNormal noBtn = dialogView.findViewById(R.id.btn_no);

                BakcellTextViewBold dialog_title = dialogView.findViewById(R.id.dialog_title);
                BakcellTextViewNormal tvConfirmationMsg = dialogView.findViewById(R.id.tv_top_confirmation_msg);

                dialog_title.setText(getString(R.string.dialog_confirmation_lbl));
                tvConfirmationMsg.setText(getString(R.string.lovely_number_add_confirmation_message, number));

                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestAddFriendsAndFamily(number);
                        alertDialog.dismiss();
                    }
                });

                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();

                break;
            case R.id.hamburgerMenu:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onUpdateCounter(FriendFamily friendFamily) {
        if (friendFamily != null) {
            requestDeleteFriendsAndFamily(friendFamily);
        }
    }

    private void loadFriendsAndFamilyList(ArrayList<FriendFamily> fnfList) {
        if (fnfList != null) {
            adapterFnF.updateFnf(fnfList);
            binding.tvNumberCounter.setText(adapterFnF.getCount() + "/" + fnfMaxCount);

            if (fnfList.size() < fnfMaxCount) {
                binding.ivAddImage.setImageResource(R.drawable.ic_ff_plus_red);
                binding.ivAddImage.setEnabled(true);
            } else {
                binding.ivAddImage.setImageResource(R.drawable.ic_ff_plus_grey);
                binding.ivAddImage.setEnabled(false);
            }

        }
    }

    public void requestGetFriendsAndFamily() {


        RequestGetFriendAndFamilyService.newInstance(FNFActivity.this, ServiceIDs.REQUEST_GET_FF).execute(DataManager.getInstance().getCurrentUser(),
                "", "", new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();
                        if (result == null) return;

                        FriendFamilyMain friendFamilyMain = new Gson().fromJson(result, FriendFamilyMain.class);

                        if (friendFamilyMain != null) {

                            if (Tools.isNumeric(friendFamilyMain.getFnfLimit())) {
                                fnfMaxCount = Tools.getIntegerFromString(friendFamilyMain.getFnfLimit());
                            }

                            loadFriendsAndFamilyList(friendFamilyMain.getFnfList());
                        }


                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(FNFActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(FNFActivity.this);
                            return;
                        }
                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(FNFActivity.this,
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        BakcellPopUpDialog.ApiFailureMessage(FNFActivity.this);
                    }//end of onError
                });
    }

    public void requestDeleteFriendsAndFamily(FriendFamily friendFamily) {
        if (friendFamily == null) return;

        RequestDeleteFriendAndFamilyService.newInstance(FNFActivity.this, ServiceIDs.REQUEST_DELETE_FF).execute(DataManager.getInstance().getCurrentUser(),
                friendFamily.getMsisdn(), "", new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();
                        if (result == null) return;

                        FriendFamilyMain friendFamilyMain = new Gson().fromJson(result, FriendFamilyMain.class);

                        if (friendFamilyMain != null) {
                            loadFriendsAndFamilyList(friendFamilyMain.getFnfList());
                        }
//                        BakcellPopUpDialog.showMessageDialog(FNFActivity.this, getString(R.string.mesg_successful_title), response.getDescription());
                        inAppFeedback(FNFActivity.this, getString(R.string.mesg_successful_title), response.getDescription());
                        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_NUMBER_DELETE_SUCCESS,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN);

                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(FNFActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(FNFActivity.this);
                            return;
                        }
                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(FNFActivity.this,
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_NUMBER_DELETE_FAILURE,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN);
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        BakcellPopUpDialog.ApiFailureMessage(FNFActivity.this);
                    }//end of onError
                });
    }

    public void requestAddFriendsAndFamily(String number) {


        RequestAddFriendAndFamilyService.newInstance(FNFActivity.this, ServiceIDs.REQUEST_ADD_FF).execute(DataManager.getInstance().getCurrentUser(),
                number, "", new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        binding.mobileInput.setText("");
                        String result = response.getData();
                        if (result == null) return;

                        FriendFamilyMain friendFamilyMain = new Gson().fromJson(result, FriendFamilyMain.class);

                        if (friendFamilyMain != null) {
                            loadFriendsAndFamilyList(friendFamilyMain.getFnfList());
                        }

                        inAppFeedback(FNFActivity.this, getString(R.string.mesg_successful_title), response.getDescription());

                        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_NUMBER_ADD_SUCCESS,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN);

                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(FNFActivity.this,
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_NUMBER_ADD_FAILURE,
                                AppEventLogValues.ServiceEvents.SERVICES_FRIENDS_AND_FAMILY_SCREEN);
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        BakcellPopUpDialog.ApiFailureMessage(FNFActivity.this);
                    }//end of onError
                });
    }

    private static void inAppFeedback(Context context, String title, String description) {
        int currentVisit = PrefUtils.getAsInt(context, PrefUtils.PreKeywords.PREF_FNF_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(context);
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.FNF_PAGE);
            if (surveys != null) {
                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                        InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(context);
                        if (surveys.getQuestions() != null) {
                            currentVisit = 0;
                            inAppFeedbackDialog.showTitleWithMessageDialog(surveys, title, description);
                        } else {
                            BakcellPopUpDialog.showMessageDialog(context, title, description);
                        }
                    } else {
                        BakcellPopUpDialog.showMessageDialog(context, title, description);
                    }
                } else {
                    BakcellPopUpDialog.showMessageDialog(context, title, description);
                }
            } else {
                BakcellPopUpDialog.showMessageDialog(context, title, description);
            }
        } else {
            BakcellPopUpDialog.showMessageDialog(context, title, description);
        }
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_FNF_PAGE, currentVisit);
    }
}
