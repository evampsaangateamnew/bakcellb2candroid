package com.bakcell.activities.landing;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.databinding.ActivityValueAddedBinding;

public class VASActivity extends BaseActivity implements View.OnClickListener {

    ActivityValueAddedBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_value_added);


        initUiContent();

        initUiListeners();
    }

    private void initUiListeners() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.ffLayout.setOnClickListener(this);
        binding.frbtLayout.setOnClickListener(this);
    }

    private void initUiContent() {
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.lbl_vas);

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ff_layout:
                startNewActivity(this, FNFActivity.class);
                break;
            case R.id.frbt_layout:
                startNewActivity(this, FRBTActivity.class);
                break;
        }
    }

}
