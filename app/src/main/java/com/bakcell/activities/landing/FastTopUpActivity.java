package com.bakcell.activities.landing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.AdapterFastTopUp;
import com.bakcell.databinding.ActivityFastTopUpBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.OnOkClickListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.fastpayments.FastTopUpData;
import com.bakcell.models.fastpayments.FastTopUpDetail;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestDeleteFastTopUpService;
import com.bakcell.webservices.RequestGetFastPayments;
import com.bakcell.webservices.RequestMakePaymentService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class FastTopUpActivity extends BaseActivity {
    ActivityFastTopUpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fast_top_up);
        //initialize UI
        initUiContent();
        //initialize listeners
        initUiListeners();


    }//onCreate ends

    private void initUiListeners() {

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
    } //initUiListeners ends

    @Override
    protected void onResume() {
        super.onResume();
        //request Fast Payments API
        requestFastPayments();
    }

    private void initUiContent() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(getString(R.string.lbl_fast_topup));
    }  //initUiContent ends

    private boolean isValidPaymentKey(String paymentKey) {
        boolean isValid = true;
        if (!Tools.hasValue(paymentKey)) {
            isValid = false;
        }
        return isValid;
    }

    private boolean isValidAmount(String amount) {
        boolean isValid = true;

        if (!Tools.hasValue(amount)) {
            BakcellPopUpDialog.showMessageDialog(FastTopUpActivity.this,
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_msg_invalid_amount));

            isValid = false;
        }

        return isValid;
    }

    private boolean isValidMobileNumber(String mobileNumber) {
        boolean isValid = true;
        if (!Tools.hasValue(mobileNumber) || mobileNumber.length() != 9) {
            BakcellPopUpDialog.showMessageDialog(FastTopUpActivity.this,
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_msg_invalid_number));
            isValid = false;
        }
        return isValid;
    }

    private void setupRecentVisaCardRecyclerView(List<FastTopUpDetail> fastPaymentDetails) {
        //we add empty element for our last item i.e Add more payments
        fastPaymentDetails.add(new FastTopUpDetail());
        AdapterFastTopUp adapterFastTopUp = new AdapterFastTopUp(fastPaymentDetails, this, new AdapterFastTopUp.SetOnItemSelected() {
            @Override
            public void onSelected(FastTopUpDetail fastTopUpDetail) {
                if (!FastTopUpActivity.this.isFinishing() && fastTopUpDetail != null && isValidMobileNumber(fastTopUpDetail.getTopupNumber())
                        && isValidAmount(fastTopUpDetail.getAmount())
                        && isValidPaymentKey(fastTopUpDetail.getPaymentKey())) {
                    String dialogMessage = "";

                    if (AppClass.getCurrentLanguageNum(FastTopUpActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ)) {
                        dialogMessage = getString(R.string.payment_loading) + fastTopUpDetail.getTopupNumber() + " nömrəsinə " + fastTopUpDetail.getAmount() + getString(R.string.azn_to);
                    } else {
                        dialogMessage = getString(R.string.payment_loading) + fastTopUpDetail.getAmount() + getString(R.string.azn_to) + fastTopUpDetail.getTopupNumber() + ".";
                    }

                    BakcellPopUpDialog.showConfirmationDialog(FastTopUpActivity.this, getString(R.string.dialog_confirmation_lbl), dialogMessage, new OnOkClickListener() {
                        @Override
                        public void onOkClick() {
                            requestMakePayment(fastTopUpDetail.getTopupNumber(), fastTopUpDetail.getAmount(), fastTopUpDetail.getPaymentKey());
                        }
                    });

                }
            }
        });
        binding.recyclerViewFastTopUp.setLayoutManager(new LinearLayoutManager(this));
        if (fastPaymentDetails != null && fastPaymentDetails.size() > 1) {
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(enableSwipeToDeleteAndUndo(adapterFastTopUp, fastPaymentDetails));
            itemTouchHelper.attachToRecyclerView(binding.recyclerViewFastTopUp);
        }
        binding.recyclerViewFastTopUp.setAdapter(adapterFastTopUp);

    } //setupRecentVisaCardRecyclerView ends

    private void requestFastPayments() {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestGetFastPayments.newInstance(FastTopUpActivity.this, ServiceIDs.REQUEST_GET_FAST_PAYMENTS)
                    .execute(userModel, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            if (!FastTopUpActivity.this.isFinishing() && response != null && response.getData() != null &&
                                    Tools.hasValue(response.getData())) {
                                String data = response.getData();
                                Gson gson = new Gson();
                                FastTopUpData fastTopUpData = gson.fromJson(data, FastTopUpData.class);
                                setupRecentVisaCardRecyclerView(fastTopUpData.getFastPaymentDetails());
                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!FastTopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(FastTopUpActivity.this);
                                return;
                            }

                            if (!FastTopUpActivity.this.isFinishing() && response != null) {
                                BakcellPopUpDialog.showMessageDialog(FastTopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                                setupRecentVisaCardRecyclerView(new ArrayList<FastTopUpDetail>());

                            }


                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!FastTopUpActivity.this.isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(FastTopUpActivity.this);
                                setupRecentVisaCardRecyclerView(new ArrayList<FastTopUpDetail>());
                            }
                        }
                    });

        }
    } // requestFastPayments ends

    private void requestMakePayment(String mobileNumber, String amount, String paymentKey) {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestMakePaymentService.newInstance(FastTopUpActivity.this, ServiceIDs.REQUEST_MAKE_PAYMENT)
                    .execute(userModel, paymentKey, amount, mobileNumber, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            String responseDescription = "";
                            if (!FastTopUpActivity.this.isFinishing() && response != null && response.getDescription() != null &&
                                    Tools.hasValue(response.getDescription())) {
                                responseDescription = response.getDescription();
                            }
                            if(!FastTopUpActivity.this.isFinishing() )
                            {
                            if ( responseDescription != null) {
                                showMessageDialog(FastTopUpActivity.this,
                                        getString(R.string.app_name), responseDescription);
                            } else {
                                BakcellPopUpDialog.ApiFailureMessage(FastTopUpActivity.this);
                            }

                            }

                            //refresh dashboard check
                            RootValues.getInstance().setDashboardApiCall(true);

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!FastTopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(FastTopUpActivity.this);
                                return;
                            }

                            if (!FastTopUpActivity.this.isFinishing() && response != null&&response.getDescription()!=null) {
                                BakcellPopUpDialog.showMessageDialog(FastTopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!FastTopUpActivity.this.isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(FastTopUpActivity.this);
                            }

                        }
                    });

        }
    }// requestMakePayment ends


    private void requestDeletePayment(AdapterFastTopUp adapterFastTopUp, int position, String paymentId) {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestDeleteFastTopUpService.newInstance(FastTopUpActivity.this, ServiceIDs.REQUEST_DELETE_FAST_PAYMENT)
                    .execute(userModel, paymentId, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            adapterFastTopUp.deleteItem(position);
                            String responseDescription = "";
                            if (!FastTopUpActivity.this.isFinishing() && response != null && response.getDescription() != null &&
                                    Tools.hasValue(response.getDescription())) {
                                responseDescription = response.getDescription();
                            }
                            if(!FastTopUpActivity.this.isFinishing() )
                            {
                                if ( responseDescription != null) {
                                    showMessageDialog(FastTopUpActivity.this,
                                            getString(R.string.dialog_title_successful), responseDescription);
                                } else {
                                    BakcellPopUpDialog.ApiFailureMessage(FastTopUpActivity.this);
                                }
                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!FastTopUpActivity.this.isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(FastTopUpActivity.this);
                                return;
                            }

                            if (!FastTopUpActivity.this.isFinishing() && response != null) {
                                adapterFastTopUp.notifyDataSetChanged();
                                BakcellPopUpDialog.showMessageDialog(FastTopUpActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!FastTopUpActivity.this.isFinishing()) {
                                adapterFastTopUp.notifyDataSetChanged();
                                BakcellPopUpDialog.ApiFailureMessage(FastTopUpActivity.this);
                            }
                        }
                    });
        }
    }// requestMakePayment ends


    private void showMessageDialog(Context context, String title, String message) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_message, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            BakcellButtonNormal bakcellButtonNormal = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_okay);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    //go back
                }
            });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    private ItemTouchHelper.SimpleCallback enableSwipeToDeleteAndUndo(AdapterFastTopUp adapterFastTopUp, List<FastTopUpDetail> fastPaymentDetails) {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
                if (fastPaymentDetails != null && position <= fastPaymentDetails.size() - 1 && fastPaymentDetails.get(position) != null && Tools.hasValue(fastPaymentDetails.get(position).getId())) {
                    requestDeletePayment(adapterFastTopUp, position, fastPaymentDetails.get(position).getId());
                } else {
                    adapterFastTopUp.notifyItemChanged(position);
                }
            }
        };
        return simpleItemTouchCallback;
    }


}