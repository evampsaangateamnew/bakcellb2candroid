package com.bakcell.activities.landing;

import android.app.ProgressDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;

public class LiveChatActivity extends BaseActivity /*implements ChatWindowView.ChatWindowEventsListener*/ {

//    ActivityLiveChatBinding binding;

    // Real: 3528751
    private String LIVECHAT_SUPPORT_LICENCE_NR = "3528751";
    private String GROUP_ID = "0";
    private String VISITOR_NAME = "";
    private String VISITOR_EMAIL = "";
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

//        initUiContent();

//        initUiListeners();
        AppEventLogs.applyAppEvent(AppEventLogValues.LiveChatEvents.LIVE_CHAT_SCREEN,
                AppEventLogValues.LiveChatEvents.LIVE_CHAT_SCREEN,
                AppEventLogValues.LiveChatEvents.LIVE_CHAT_SCREEN);

    }


    /*private void initUiListeners() {
        binding.toolbar.notificationMenu.setVisibility(View.INVISIBLE);

    }

    private void initUiContent() {
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
        binding.toolbar.pageTitle.setText(R.string.menu_live_chat);

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        juni1289 change here
//        loadLiveChat();
        setupWebView();
    }

    private void setupWebView() {
        try {
            if (this.isFinishing()) {
                //safe catch for activity not running and do not show the dialog etc
                return;
            }//if (this.isFinishing())

            //if progress dialog already is showing kill it
            killProgressDialog();

            //show the loading dialog
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Loading ...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            //enable zoom view controls
            binding.webView.getSettings().setBuiltInZoomControls(true);
            binding.webView.getSettings().setDisplayZoomControls(true);

            //load the links inside the web view
            binding.webView.setWebViewClient(new WebViewClient());
            binding.webView.getSettings().setAllowFileAccess(true);
            binding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

            //enable javascript
            binding.webView.getSettings().setJavaScriptEnabled(true);

            //load the url inside the webview
            String url = "https://my.bakcell.com:8444/livezilla/chat.php?ptn=" + getUserName() + "&pte=" + getUserEmail() + "&ptp=" + getUserPhone()+"&hfk=MQ__";
            binding.webView.loadUrl(url);

            //register the webview listener
            registerWebViewListener();

        } catch (Exception exp) {
            killProgressDialog();
            Logger.debugLog(Logger.TAG_CATCH_LOGS, "setup webview:::" + exp.getMessage());
        }//catch ends
    }//setupWebView ends

    private String getUserName() {
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String userName = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getFirstName())) {
                userName = DataManager.getInstance().getCurrentUser().getFirstName();
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMiddleName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getMiddleName());
            }
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLastName())) {
                userName = userName.concat(" " + DataManager.getInstance().getCurrentUser().getLastName());
            }

            //return the username
            return userName;
        }

        return "";
    }

    private String getUserEmail() {
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String email = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getEmail())) {
                email = DataManager.getInstance().getCurrentUser().getEmail();
            }

            //return the email
            return email;
        }

        return "";
    }

    private String getUserPhone() {
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null) {
            String phone = "";
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                phone = DataManager.getInstance().getCurrentUser().getMsisdn();
            }

            //return the email
            return phone;
        }

        return "";
    }

    private void killProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }//if(progressDialog.isShowing())
        }//if(progressDialog!=null)
    }//killProgressDialog ends

    private void registerWebViewListener() {
        try {
            binding.webView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    //hide the dialog
                    killProgressDialog();
                    // do your stuff here for url being loaded in the webview
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, "onPageFinished->webview");
                }//onPageFinished ends

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    //hide the dialog
                    killProgressDialog();
                    //Your code to do for handling the error
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, "onReceivedError->webview:::" + error.toString());
                }//onReceivedError ends

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }//shouldOverrideUrlLoading ends
            });
        } catch (Exception exp) {
            //hide the dialog
            killProgressDialog();
            Logger.debugLog(Logger.TAG_CATCH_LOGS, "registerListener:::" + exp.getMessage());
        }//catch ends
    }//registerWebViewListener ends*/

   /* private void loadLiveChat() {
        binding.embeddedChatWindow.setUpWindow(getChatWindowConfiguration());
        binding.embeddedChatWindow.setUpListener(LiveChatActivity.this);
        binding.embeddedChatWindow.initialize();

        binding.embeddedChatWindow.showChatWindow();
    }*/


    /*public ChatWindowConfiguration getChatWindowConfiguration() {

        if (DataManager.getInstance().getCurrentUser() != null) {
            VISITOR_NAME = DataManager.getInstance().getCurrentUser().getFirstName();
            VISITOR_EMAIL = DataManager.getInstance().getCurrentUser().getEmail();
        }
        return new ChatWindowConfiguration(LIVECHAT_SUPPORT_LICENCE_NR, GROUP_ID, VISITOR_NAME, VISITOR_EMAIL, null);
    }*/

    /*@Override
    public void onChatWindowVisibilityChanged(boolean visible) {
        if (!visible) {
            onBackPressed();
        }
    }

    @Override
    public void onNewMessage(NewMessageModel message, boolean windowVisible) {
        if (!windowVisible) {
            Toast.makeText(getApplicationContext(), message.getText(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStartFilePickerActivity(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public boolean handleUri(Uri uri) {
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        binding.embeddedChatWindow.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }*/


}
