package com.bakcell.activities.landing;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityChangePasswordBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.changepassword.ChangePasswordData;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.webservices.ChangePasswordService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    ActivityChangePasswordBinding binding;

    RelativeLayout ivQustionMark;

    RelativeLayout error_message_box_view;

    private boolean isMatchComplexity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);

        initUiContent();

        initListeners();

        AppEventLogs.applyAppEvent(AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SCREEN,
                AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SCREEN,
                AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SCREEN);
    }

    private void initUiContent() {

        error_message_box_view = binding.errorMessageBoxView;
        ivQustionMark = binding.questionIcon;
        binding.toolbar.pageTitle.setText(R.string.activity_change_password_lbl);
        binding.toolbar.ivHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.arrowleft));
    }

    private void initListeners() {
        ivQustionMark.setOnClickListener(this);
        binding.toolbar.hamburgerMenu.setOnClickListener(this);
        binding.btnPasswordUpdate.setOnClickListener(this);
        binding.passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isMatchComplexity = FieldFormatter.setPasswordStrengthStatus(ChangePasswordActivity.this,
                        binding.passwordInput,
                        binding.passwordStrengthView,
                        binding.redView,
                        binding.orangeView,
                        binding.greenView,
                        binding.tvPasswordStrengthMessage,
                        true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.toolbar.hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.confirmPasswordInput.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    prceedOnUpdatePassword();
                }
                return false;
            }
        });

    }


    protected void updateClicked(String message) {

        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_top_up_successful, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(false);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button okayBtn = (Button) dialogView.findViewById(R.id.btn_okay);
            okayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    /**remove the currently logged in user from the list
                     * count the number of users that are left after deleting
                     * if, users are greater than 1 redirect to the manage accounts screen
                     * else, log out the single currently logged in user*/
                    if (MultiAccountsHandler.getCurrentlyLoggedInUsersCount(ChangePasswordActivity.this) > 1) {
                        /**there are multiple users logged in
                         * so delete the currently logged in user from
                         * the list of currently logged in users
                         * and redirect the user to the manage accounts screen
                         * in this case the user can only stay on the manage accounts screen
                         * put a flag of "isManageAccountsScreenToShow so that
                         * when the user goes back and comes again, should only see the manage accounts screen
                         * same is the case of change password"*/
                        //delete the current user
                        MultiAccountsHandler.deleteUser(ChangePasswordActivity.this, DataManager.getInstance().getCurrentUser().getMsisdn());
                        //manage accounts screen needs to be shown
                        PrefUtils.addBoolean(ChangePasswordActivity.this, PrefUtils.PreKeywords.PREF_IS_MANAGE_ACCOUNTS_SCREEN_NEED_TO_SHOW, true);
                        //redirect the user to the manage accounts screen, killing all back stacked screens; so that he won't come back
                        BaseActivity.startNewActivityAndClear(ChangePasswordActivity.this, ManageAccountsActivity.class);
                    } else {
                        HomeActivity.logoutUser(ChangePasswordActivity.this);
                    }
                }
            });
            TextView label = (TextView) dialogView.findViewById(R.id.lbl_balance);
            TextView tvConfirmation = (TextView) dialogView.findViewById(R.id.tv_top_confirmation_msg);
            TextView bal = (TextView) dialogView.findViewById(R.id.tvBalnce);
            TextView title = (TextView) dialogView.findViewById(R.id.tv_title);
            TextView azn = (TextView) dialogView.findViewById(R.id.tv_azn);
            title.setText(R.string.dialog_confirmation_lbl);
            label.setVisibility(View.GONE);
            bal.setVisibility(View.GONE);
            azn.setVisibility(View.GONE);
            tvConfirmation.setText(message);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_password_update:

                prceedOnUpdatePassword();
                break;
            case R.id.question_icon:
                if (error_message_box_view.getVisibility() == View.INVISIBLE) {
                    error_message_box_view.setVisibility(View.VISIBLE);
                } else {
                    error_message_box_view.setVisibility(View.INVISIBLE);
                }
                break;
        }
    }

    private void prceedOnUpdatePassword() {
        if (!FieldFormatter.isValidPassword(ChangePasswordActivity.this, binding.currentPasswordInput.getText().toString())) {
            BakcellPopUpDialog.showMessageDialog(ChangePasswordActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_msg_invalid_current_password));
            return;
        }

        if (!TextUtils.isEmpty(binding.currentPasswordInput.getText().toString()) && !TextUtils.isEmpty(binding.passwordInput.getText().toString())) {
            if (binding.passwordInput.getText().toString().equals(binding.confirmPasswordInput.getText().toString())) {
                if (isMatchComplexity) {
                    requestForChangePassword();
                } else {
                    BakcellPopUpDialog.showMessageDialog(ChangePasswordActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_message_password_match_complexity));
                }
            } else {
                BakcellPopUpDialog.showMessageDialog(ChangePasswordActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_message_password_not_match));
            }
        }
    }

    private void requestForChangePassword() {


        ChangePasswordService.newInstance(ChangePasswordActivity.this, ServiceIDs.CHANGE_PASSWORD).execute(DataManager.getInstance().getCurrentUser(), binding.currentPasswordInput.getText().toString(), binding.passwordInput.getText().toString(), binding.confirmPasswordInput.getText().toString(), new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                String result = response.getData();

                ChangePasswordData data = new Gson().fromJson(result, ChangePasswordData.class);

                updateClicked(data.getMessage());

                resetWrongPasswordAttemps(ChangePasswordActivity.this);
                AppEventLogs.applyAppEvent(AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SCREEN,
                        AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SUCCESS,
                        AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SCREEN);
            }


            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                // Check if user logout by server
                if (response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                    Toast.makeText(ChangePasswordActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                    HomeActivity.logoutUser(ChangePasswordActivity.this);
                    return;
                }

                if (response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_WRONG_OLD_PASS_ATTEMPS)) {
                    updateWrongPasswordAttemptCount();
                }

                if (response != null) {
                    BakcellPopUpDialog.showMessageDialog(ChangePasswordActivity.this,
                            getString(R.string.bakcell_error_title), response.getDescription());
                }

                AppEventLogs.applyAppEvent(AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SCREEN,
                        AppEventLogValues.SettingsEvents.CHANGE_PASWRD_FAILURE,
                        AppEventLogValues.SettingsEvents.CHANGE_PASWRD_SCREEN);
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                BakcellPopUpDialog.ApiFailureMessage(ChangePasswordActivity.this);
            }

        });

    }


    public void updateWrongPasswordAttemptCount() {
        int preCount = PrefUtils.getAsInt(ChangePasswordActivity.this, PrefUtils.PreKeywords.PREF_WRONG_ATTEMPS_COUNT);
        preCount = preCount + 1;
        if (preCount == 5) {
            HomeActivity.logoutUser(ChangePasswordActivity.this);
            return;
        }
        PrefUtils.addInt(ChangePasswordActivity.this, PrefUtils.PreKeywords.PREF_WRONG_ATTEMPS_COUNT, preCount);
    }

    public static void resetWrongPasswordAttemps(Context context) {
        if (context == null) return;
        int preCount = 0;
        PrefUtils.addInt(context, PrefUtils.PreKeywords.PREF_WRONG_ATTEMPS_COUNT, preCount);
    }

}
