package com.bakcell.activities.livechat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.databinding.ActivityLivechatBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.Logger;
import com.livechatinc.inappchat.ChatWindowConfiguration;
import com.livechatinc.inappchat.ChatWindowErrorType;
import com.livechatinc.inappchat.ChatWindowView;
import com.livechatinc.inappchat.models.NewMessageModel;

import java.util.HashMap;

public class LiveChatSDKActivity extends BaseActivity implements ChatWindowView.ChatWindowEventsListener {

    private ActivityLivechatBinding binding;
    private ChatWindowConfiguration configuration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_livechat);

        setUpChatConfiguration();
    }

    private void setUpChatConfiguration() {
        String VISITOR_NAME = "", VISITOR_EMAIL = "", phoneNumber = "", groupId = "";
        if (DataManager.getInstance().getCurrentUser() != null) {
            VISITOR_NAME = DataManager.getInstance().getCurrentUser().getFirstName();
            VISITOR_EMAIL = DataManager.getInstance().getCurrentUser().getEmail();
            phoneNumber = DataManager.getInstance().getCurrentUser().getMsisdn();
        }

        if (AppClass.getCurrentLanguageNum(LiveChatSDKActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_AZ)) {
            groupId = "1";
        } else if (AppClass.getCurrentLanguageNum(LiveChatSDKActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_RU)) {
            groupId = "2";
        } else if (AppClass.getCurrentLanguageNum(LiveChatSDKActivity.this).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_NUM_EN)) {
            groupId = "0";
        }

        HashMap<String, String> PHONENUMBER = new HashMap<>();

        PHONENUMBER.put(Constants.LiveChat.PHONE_NUMBER, phoneNumber);

        configuration = new ChatWindowConfiguration(
                Constants.LiveChat.LICENCE_NUMBER,
                groupId,
                VISITOR_NAME,
                VISITOR_EMAIL,
                PHONENUMBER
        );

        startEmbeddedChat();
    }

    public void startEmbeddedChat() {
        if (!binding.embeddedChatWindow.isInitialized()) {
            binding.embeddedChatWindow.setUpWindow(configuration);
            binding.embeddedChatWindow.setUpListener(this);
            binding.embeddedChatWindow.initialize();
        }
        binding.embeddedChatWindow.showChatWindow();
    }

    @Override
    public void onChatWindowVisibilityChanged(boolean visible) {
        if (!visible) {
            onBackPressed();
        }
    }

    @Override
    public void onNewMessage(NewMessageModel message, boolean windowVisible) {
        if (!windowVisible) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, message.getText());
        }
    }

    @Override
    public void onStartFilePickerActivity(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public boolean onError(ChatWindowErrorType errorType, int errorCode, String errorDescription) {
        if (errorType == ChatWindowErrorType.WebViewClient && errorCode == -2 && binding.embeddedChatWindow.isChatLoaded()) {
            //Chat window can handle reconnection. You might want to delegate this to chat window
            return false;
        }
        return true;
    }

    @Override
    public boolean handleUri(Uri uri) {
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        binding.embeddedChatWindow.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        binding.embeddedChatWindow.onBackPressed();
        super.onBackPressed();
    }
}
