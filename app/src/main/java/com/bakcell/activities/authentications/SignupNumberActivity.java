package com.bakcell.activities.authentications;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivitySignupNumberBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.pin.DataPin;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.SignupNumberVerifyService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class SignupNumberActivity extends BaseActivity implements View.OnClickListener {

    ActivitySignupNumberBinding binding;
    private final String fromClass = "SignupNumberActivity";
    private boolean isFromManageAccountsFlow = false;

    private void getIntentData() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY)) {
            if (getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY) != null &&
                    getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY).
                            equalsIgnoreCase(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE)) {
                isFromManageAccountsFlow = true;
            }
        }

        BakcellLogger.logE("fromKeyX", "isFromManageAccountsFlow:::" + isFromManageAccountsFlow, fromClass, "getIntentData");
    }//getIntentData ends

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup_number);
        getIntentData();
        binding.logo.setImageResource(Tools.getBakcellLocalizedLogo(this));
        disableNextButton();

        initListeners();

    }

    private void initListeners() {
        binding.nextLayout.setOnClickListener(this);
        binding.alreadySignupLayout.setOnClickListener(this);

        binding.mobileInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {


                if (FieldFormatter.isValidMsisdn(s.toString()) && s.length() == 9) {
                    binding.nextLayout.setEnabled(true);
                    binding.nextLbl.setTextColor(ContextCompat.getColor(
                            SignupNumberActivity.this, R.color.white));
                    binding.arrowIcon.setImageResource(R.drawable.arrow_right);
                } else {
                    binding.nextLayout.setEnabled(false);
                    binding.nextLbl.setTextColor(ContextCompat.getColor(
                            SignupNumberActivity.this, R.color.signup_status_line));
                    binding.arrowIcon.setImageResource(R.drawable.forward_arrow_disabled);
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (FieldFormatter.isValidMsisdn(s.toString())) {
                    enableNextButton();
                } else {
                    disableNextButton();
                }

            }
        });

        binding.mobileInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    processOnNext();
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.nextLayout:
                processOnNext();
                break;
            case R.id.alreadySignupLayout:
                finish();
                break;
        }
    }

    private void processOnNext() {
        String msisdn = binding.mobileInput.getText().toString();
        if (FieldFormatter.isValidMsisdn(msisdn)) {
            /**check if the user is logged in already,
             * if, then show error
             * else, log the user in*/
            if (isFromManageAccountsFlow) {
                ArrayList<UserModel> userModelArrayList =
                        MultiAccountsHandler.getCustomerDataFromPreferences(SignupNumberActivity.this).userModelArrayList;

                if (MultiAccountsHandler.getCustomerDataFromPreferences(SignupNumberActivity.this) != null &&
                        !MultiAccountsHandler.getCustomerDataFromPreferences(SignupNumberActivity.this).toString().equalsIgnoreCase("{}") &&
                        userModelArrayList != null &&
                        !userModelArrayList.isEmpty()) {
                    boolean isUserAlreadyExists = false;
                    for (int i = 0; i < userModelArrayList.size(); i++) {
                        if (msisdn.equalsIgnoreCase(userModelArrayList.get(i).getMsisdn())) {
                            isUserAlreadyExists = true;
                        }
                    }

                    if (isUserAlreadyExists) {
                        //user is already logged in with the same msisdn
                        BakcellPopUpDialog.showMessageDialog(
                                SignupNumberActivity.this,
                                getResources().getString(R.string.bakcell_error_title),
                                getResources().getString(R.string.number_already_exists));
                        BakcellLogger.logE("fromKeyX", "user already exists", fromClass, "proceedOnLogin");
                    } else {
                        BakcellLogger.logE("fromKeyX", "user does not exist", fromClass, "proceedOnLogin");
                        requestForSignupNumberVerify(msisdn);
                    }//if (isUserAlreadyExists) ends
                } else {
                    BakcellLogger.logE("fromKeyX", "no one user, does not exist", fromClass, "proceedOnLogin");
                    if (DataManager.getInstance().getCurrentUser() != null &&
                            Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn()) &&
                            DataManager.getInstance().getCurrentUser().getMsisdn().equalsIgnoreCase(msisdn)) {
                        //user is already logged in with the same msisdn
                        BakcellPopUpDialog.showMessageDialog(
                                SignupNumberActivity.this,
                                getResources().getString(R.string.bakcell_error_title),
                                getResources().getString(R.string.number_already_exists));
                        BakcellLogger.logE("fromKeyX", "user already exists111", fromClass, "proceedOnLogin");
                    }else {
                        if (PrefUtils.getCustomerData(SignupNumberActivity.this).getCustomerData() != null &&
                                PrefUtils.getCustomerData(SignupNumberActivity.this).getCustomerData().getMsisdn().equalsIgnoreCase(msisdn)) {
                            //user is already logged in with the same msisdn
                            BakcellPopUpDialog.showMessageDialog(
                                    SignupNumberActivity.this,
                                    getResources().getString(R.string.bakcell_error_title),
                                    getResources().getString(R.string.number_already_exists));
                            BakcellLogger.logE("fromKeyX", "user already exists222", fromClass, "proceedOnLogin");
                        } else {
                            requestForSignupNumberVerify(msisdn);
                        }
                    }
                }//if (userModelArrayList != null) ends
            } else {
                requestForSignupNumberVerify(msisdn);
            }//if (isFromManageAccountsFlow) ends
        } else {
            BakcellPopUpDialog.showMessageDialog(SignupNumberActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_msg_invalid_number));
        }
    }

    private void requestForSignupNumberVerify(String msisdn) {

        SignupNumberVerifyService.newInstance(SignupNumberActivity.this, ServiceIDs.SIGNUP_NUMBER_VERIFY).execute(msisdn, EndPoints.Constants.CAUSE_SIGNUP, new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                DataPin dataPin = new Gson().fromJson(response.getData(), DataPin.class);

                if (dataPin != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(SignupPinActivity.NUMBER_KEY, binding.mobileInput.getText().toString());
                    if (isFromManageAccountsFlow) {
                        BakcellLogger.logE("fromKeyX", "from manage accounts", fromClass, "requestForSignupNumberVerify");
                        bundle.putString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY,
                                MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE);
                    }
                    BakcellLogger.logE("fromKeyX", "NOT from manage accounts", fromClass, "requestForSignupNumberVerify");
                    startNewActivity(SignupNumberActivity.this, SignupPinActivity.class, bundle);
                    AppEventLogs.applyAppEvent(AppEventLogValues.SignupEvents.SIGN_UP_STEP_ONE,
                            AppEventLogValues.SignupEvents.NUMBER_VERIFICATION_SUCCESS,
                            AppEventLogValues.SignupEvents.SIGN_UP_STEP_ONE);
                }

            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                BakcellPopUpDialog.showMessageDialog(SignupNumberActivity.this,
                        getString(R.string.bakcell_error_title), response.getDescription());
                AppEventLogs.applyAppEvent(AppEventLogValues.SignupEvents.SIGN_UP_STEP_ONE,
                        AppEventLogValues.SignupEvents.NUMBER_VERIFICATION_FAILURE,
                        AppEventLogValues.SignupEvents.SIGN_UP_STEP_ONE);
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                BakcellPopUpDialog.ApiFailureMessage(SignupNumberActivity.this);
            }
        });

    }

    private void disableNextButton() {
        binding.nextLayout.setEnabled(false);
        binding.nextLbl.setTextColor(ContextCompat.getColor(
                SignupNumberActivity.this, R.color.signup_status_line));
        binding.arrowIcon.setImageResource(R.drawable.forward_arrow_disabled);
    }

    private void enableNextButton() {
        binding.nextLayout.setEnabled(true);
        binding.nextLbl.setTextColor(ContextCompat.getColor(
                SignupNumberActivity.this, R.color.white));
        binding.arrowIcon.setImageResource(R.drawable.arrow_right);
    }

}
