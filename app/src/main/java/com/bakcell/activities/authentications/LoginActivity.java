package com.bakcell.activities.authentications;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityLoginBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.BioMetricAuthenticationEvents;
import com.bakcell.models.DataManager;
import com.bakcell.models.manageaccounts.UsersHelperModel;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.notifications.MyFirebaseMessagingService;
import com.bakcell.utilities.AppRaterTool;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.BiometricHandler;
import com.bakcell.utilities.CacheConfig;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.LoginService;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Objects;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

@TargetApi(Build.VERSION_CODES.M)
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    ActivityLoginBinding binding;
    private String selectedLanguage;
    private BiometricHandler biometricHandler = null;
    private BioMetricAuthenticationEvents bioMetricAuthenticationEvents = null;
    private AlertDialog fingerPrintAlertDialog = null;
    private final String fromClass = "LoginActivity";
    private boolean cancelBiometric = false;
    private FingerprintManager fingerprintManager = null;
    private boolean isFromManageAccountsFlow = false;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    private void getIntentData() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY)) {
            if (getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY) != null &&
                    getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY).
                            equalsIgnoreCase(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE)) {
                isFromManageAccountsFlow = true;
            }
        }

        BakcellLogger.logE("fromKeyX", "isFromManageAccountsFlow:::" + isFromManageAccountsFlow, fromClass, "getIntentData");
    }//getIntentData ends

    @Override
    protected void onResume() {
        super.onResume();
        getIntentData();
        RootValues.getInstance().setPromoMessageDisplayed(false);
        DataManager.getInstance().setPromoMessage(null);
        stopAuthentication();
        registerBiometricEventsListener();
        setCurrentLanguageLabel();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            try {
                /**
                 * check if user has enabled the biometric sign in then get the finger print manager
                 * otherwise don't do anything
                 * */
                if (PrefUtils.getBoolean(getApplicationContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false)) {
                    // Initializing Fingerprint Manager
                    fingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
                    if (fingerprintManager != null) {
                        if (fingerprintManager.isHardwareDetected()) {
                            if (fingerprintManager.hasEnrolledFingerprints()) {
                                if (PrefUtils.getCustomerData(getApplicationContext()) != null) {
                                    if (Tools.hasValue(PrefUtils.getCustomerData(getApplicationContext()).getCustomerData().getMsisdn())) {
                                        if (PrefUtils.getBoolean(getApplicationContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false)) {
                                            //init the finger print management handler
                                            biometricHandler = new BiometricHandler();
                                            requestFingerPrint();
                                        }
                                    }
                                }
                            } else {
                                BakcellPopUpDialog.showMessageDialog(LoginActivity.this,
                                        getResources().getString(R.string.bakcell_error_title),
                                        getResources().getString(R.string.biometric_no_finger_prints_enrolled_error));
                            }
                        }
                    }
                }
                //else don't prompt for the biometric sign in
            } catch (Exception exp) {
                BakcellLogger.logE("SastiDevice", "Since you have an sasti device, crash occurs2", fromClass, "onresume");
            }
        }
    }

    @Override
    protected void onPause() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            stopAuthentication();
        }
        killBiometricPopup();
        super.onPause();
    }

    private void requestFingerPrint() {
        /**check if the user is redirected from the manage accounts screen,
         * if, then don't show the finger print flow
         * else, show the finger print flow*/
        if (isFromManageAccountsFlow) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            //Fingerprint authentication permission not enabled
            requestFingerPrintScannerPermission();
        } else {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                showFingerPrintScannerDialog();
            }
        }
    }//requestFingerPrint ends

    private void requestFingerPrintScannerPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.USE_FINGERPRINT},
                Constants.PermissionCodeConstants.FINGER_PRINT_SCANNER_PERMISSION_CODE);
    }//requestFingerPrintScannerPermission ends

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("InflateParams")
    private void showFingerPrintScannerDialog() {
        if (LoginActivity.this.isFinishing()) {
            return; //safe passage
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_finger_print_scanner, null);
        dialogBuilder.setView(dialogView);

        fingerPrintAlertDialog = dialogBuilder.create();
        fingerPrintAlertDialog.show();
        fingerPrintAlertDialog.setCancelable(false);
        fingerPrintAlertDialog.setCanceledOnTouchOutside(false);
        Objects.requireNonNull(fingerPrintAlertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        BakcellButtonNormal btnCancelFingerPrintDialog = dialogView.findViewById(R.id.btnCancelFingerPrintDialog);

        btnCancelFingerPrintDialog.setOnClickListener(view -> {
            stopAuthentication();
            cancelBiometric = true;
        });

        //start finger print authentication
        fingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
        biometricHandler.starAuthentication(fingerprintManager);
    }//showFingerPrintScannerDialog ends

    private void registerBiometricEventsListener() {
        bioMetricAuthenticationEvents = new BioMetricAuthenticationEvents() {
            @Override
            public void onBiometricAuthenticationError(int errorCode, String errorMessage) {
                BakcellLogger.logE("biometricError", "ErrorCode:::" + (errorCode) + (" ErrorMessage:::") + (errorMessage), fromClass, "onBiometricAuthenticationError");
                if (!cancelBiometric) {
                    if (errorCode == FingerprintManager.FINGERPRINT_ERROR_CANCELED) {
                        //Fingerprint operation canceled.
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.biometric_invalid_credentials_error), Toast.LENGTH_SHORT).show();
                        stopAuthentication();
                        showFingerPrintScannerDialog();
                    } else {
                        //general error
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.biometric_general_error_label), Toast.LENGTH_SHORT).show();
                    }
                    cancelBiometric = false;
                }
            }

            @Override
            public void onBiometricAuthenticationSuccess() {
                //stop the authentication
                stopAuthentication();
                //start the home activity
                startNewActivityAndClear(LoginActivity.this, HomeActivity.class);
                Tools.hideKeyboard(LoginActivity.this);
            }

            @Override
            public void onBiometricAuthenticationFailed() {
                //stop the authentication
                stopAuthentication();
                //show the general error
//                Toast.makeText(applicationContext, resources.getString(R.string.biometric_general_error_label), Toast.LENGTH_SHORT).show()
            }
        };

        RootValues.getInstance().setFingerPrintScannerToLoginCallBack(bioMetricAuthenticationEvents);
    }

    private void stopAuthentication() {
        //stop the authentication
        if (biometricHandler != null) {
            biometricHandler.stopAuthentication();
        }
        killBiometricPopup();
    }//stopAuthentication ends

    private void killBiometricPopup() {
        //kill the dialog
        if (fingerPrintAlertDialog != null) {
            if (fingerPrintAlertDialog.isShowing()) {
                fingerPrintAlertDialog.dismiss();
                fingerPrintAlertDialog = null;
            }
        }//if (fingerPrintAlertDialog != null) ends
    }//killBiometricPopup ends

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);


        binding.titleLogin.setImageResource(Tools.getBakcellLocalizedLogo(LoginActivity.this));

        initListeners();

    }

    private void setCurrentLanguageLabel() {
        try {
            String currentLanguageKey = AppClass.getCurrentLanguageKey(getApplicationContext());

            if (Tools.hasValue(currentLanguageKey)) {
                if (currentLanguageKey.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                    /*binding.languageLabel.setText(getString(R.string.lbl_az_azarbaycan));*/
                    binding.languageLabel.setText("AZ");
                } else if (currentLanguageKey.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
//                    binding.languageLabel.setText(getString(R.string.lbl_en_english));
                    binding.languageLabel.setText("EN");
                } else if (currentLanguageKey.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
//                    binding.languageLabel.setText(getString(R.string.lbl_ru_rusian));
                    binding.languageLabel.setText("RU");
                } else {
//                    binding.languageLabel.setText(getString(R.string.lbl_en_english));
                    binding.languageLabel.setText("EN");
                }
            }
        } catch (Exception exp) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, exp.getMessage());
        }
    }

    protected void changeLanguageClicked() {

        if (LoginActivity.this == null) return;
        if (LoginActivity.this.isFinishing()) return;

        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
            LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_select_langue, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            Button yesBtn = dialogView.findViewById(R.id.btn_done);

            RadioGroup radioGroup = dialogView.findViewById(R.id.languageGroup);

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.azeri:
                            selectedLanguage = Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ;
                            break;
                        case R.id.russian:
                            selectedLanguage = Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU;
                            break;
                        case R.id.english:
                            selectedLanguage = Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN;
                            break;
                    }
                }
            });


            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Tools.haveNetworkConnection(LoginActivity.this)) {
                        if (!selectedLanguage.equalsIgnoreCase(AppClass.getCurrentLanguageKey(LoginActivity.this))) {
                            AppClass.setCurrentLanguageKey(LoginActivity.this, selectedLanguage);
                            BaseActivity.startNewActivityAndClear(LoginActivity.this, LoginActivity.class);

                        }

                        CacheConfig.updateCacheFlagsForAPIsCalls(LoginActivity.this);

                        alertDialog.dismiss();
                    } else {
                        Toast.makeText(LoginActivity.this, getString(R.string.message_no_internet), Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }
                }
            });

            AppCompatRadioButton azeri = dialogView.findViewById(R.id.azeri);
            AppCompatRadioButton russian = dialogView.findViewById(R.id.russian);
            AppCompatRadioButton english = dialogView.findViewById(R.id.english);

            // Update check boxes according to selected languaeg
            if (!TextUtils.isEmpty(AppClass.getCurrentLanguageKey(getApplicationContext()))) {
                if (AppClass.getCurrentLanguageKey(getApplicationContext()).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
                    english.setChecked(true);
                } else if (AppClass.getCurrentLanguageKey(getApplicationContext()).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                    azeri.setChecked(true);
                } else if (AppClass.getCurrentLanguageKey(getApplicationContext()).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
                    russian.setChecked(true);
                } else {
                    english.setChecked(true);
                }
            } else {
                english.setChecked(true);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    private void initListeners() {
        binding.loginLayout.setOnClickListener(this);
        binding.signupLayout.setOnClickListener(this);
        binding.tvForgotpasswordLbl.setOnClickListener(this);
        binding.languageLabel.setOnClickListener(this);
        binding.languageLabelLayout.setOnClickListener(this);

        binding.passwordInput.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    proceedOnLogin();
                }
                return false;
            }
        });

        binding.passwordInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    proceedOnLogin();
                }
                return false;
            }
        });

    }

    protected void forgotPasswordClicked() {
        if (isFromManageAccountsFlow) {
            BakcellLogger.logE("fromKeyX", "from manage accounts", fromClass, "going forgotpassword");
            Bundle bundle = new Bundle();
            bundle.putString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY,
                    MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE);
            startNewActivity(this, ForgotPasswordNumberActivity.class, bundle);
        } else {
            BakcellLogger.logE("fromKeyX", "NOT from manage accounts", fromClass, "going forgotpassword");
            startNewActivity(this, ForgotPasswordNumberActivity.class);
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.languageLabelLayout:

                changeLanguageClicked();

                break;
            case R.id.loginLayout:

                proceedOnLogin();

                break;
            case R.id.tv_forgotpassword_lbl:
                forgotPasswordClicked();
                break;
            case R.id.signupLayout:
                if (isFromManageAccountsFlow) {
                    BakcellLogger.logE("fromKeyX", "from manage accounts", fromClass, "going signup");
                    Bundle bundle = new Bundle();
                    bundle.putString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY,
                            MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE);
                    BaseActivity.startNewActivity(LoginActivity.this, SignupNumberActivity.class, bundle);//start the signup activity
                } else {
                    BakcellLogger.logE("fromKeyX", "NOT from manage accounts", fromClass, "going signup");
                    startNewActivity(LoginActivity.this, SignupNumberActivity.class);
                }
                break;
        }
    }

    private void proceedOnLogin() {
        BakcellLogger.logE("fromKeyX", "called", fromClass, "proceedOnLogin");
        String msisdn = binding.mobileInput.getText().toString();
        String password = binding.passwordInput.getText().toString();

        if (!FieldFormatter.isValidMsisdn(msisdn)) {
            BakcellPopUpDialog.showMessageDialog(LoginActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_msg_invalid_number));
            return;
        }
        if (!FieldFormatter.isValidPassword(LoginActivity.this, password)) {
            BakcellPopUpDialog.showMessageDialog(LoginActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_msg_invalid_password));
            return;
        }

        /**check if the user is logged in already,
         * if, then show error
         * else, log the user in*/
        if (isFromManageAccountsFlow) {
            ArrayList<UserModel> userModelArrayList =
                    MultiAccountsHandler.getCustomerDataFromPreferences(LoginActivity.this).userModelArrayList;

            if (MultiAccountsHandler.getCustomerDataFromPreferences(LoginActivity.this) != null &&
                    !MultiAccountsHandler.getCustomerDataFromPreferences(LoginActivity.this).toString().equalsIgnoreCase("{}") &&
                    userModelArrayList != null &&
                    !userModelArrayList.isEmpty()) {
                boolean isUserAlreadyExists = false;
                for (int i = 0; i < userModelArrayList.size(); i++) {
                    if (msisdn.equalsIgnoreCase(userModelArrayList.get(i).getMsisdn())) {
                        isUserAlreadyExists = true;
                    }
                }

                if (isUserAlreadyExists) {
                    //user is already logged in with the same msisdn
                    BakcellPopUpDialog.showMessageDialog(
                            LoginActivity.this,
                            getResources().getString(R.string.bakcell_error_title),
                            getResources().getString(R.string.number_already_exists));
                    BakcellLogger.logE("fromKeyX", "user already exists", fromClass, "proceedOnLogin");
                } else {
                    BakcellLogger.logE("fromKeyX", "user does not exist", fromClass, "proceedOnLogin");
                    loginRequest(msisdn, password);
                }//if (isUserAlreadyExists) ends
            } else {
                BakcellLogger.logE("fromKeyX", "no one user, does not exist", fromClass, "proceedOnLogin");
                if (DataManager.getInstance().getCurrentUser() != null &&
                        Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn()) &&
                        DataManager.getInstance().getCurrentUser().getMsisdn().equalsIgnoreCase(msisdn)) {
                    //user is already logged in with the same msisdn
                    BakcellPopUpDialog.showMessageDialog(
                            LoginActivity.this,
                            getResources().getString(R.string.bakcell_error_title),
                            getResources().getString(R.string.number_already_exists));
                    BakcellLogger.logE("fromKeyX", "user already exists111", fromClass, "proceedOnLogin");
                } else {
                    if (PrefUtils.getCustomerData(LoginActivity.this).getCustomerData() != null &&
                            PrefUtils.getCustomerData(LoginActivity.this).getCustomerData().getMsisdn().equalsIgnoreCase(msisdn)) {
                        //user is already logged in with the same msisdn
                        BakcellPopUpDialog.showMessageDialog(
                                LoginActivity.this,
                                getResources().getString(R.string.bakcell_error_title),
                                getResources().getString(R.string.number_already_exists));
                        BakcellLogger.logE("fromKeyX", "user already exists222", fromClass, "proceedOnLogin");
                    } else {
                        loginRequest(msisdn, password);
                    }
                }
            }//if (userModelArrayList != null) ends
        } else {
            loginRequest(msisdn, password);
        }//if (isFromManageAccountsFlow) ends
    }

    private void loginRequest(String msisdn, String password) {
        BakcellLogger.logE("fromKeyX", "called", fromClass, "loginRequest");
        LoginService.newInstance(LoginActivity.this, ServiceIDs.LOGIN).execute(msisdn, password, new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                String result = response.getData();

                UserMain userMain = null;
                try {
                    userMain = new Gson().fromJson(result, UserMain.class);
                } catch (JsonSyntaxException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }

                PrefUtils.addCustomerData(LoginActivity.this, userMain);

                if (userMain != null) {
                    if (isFromManageAccountsFlow) {
                        ArrayList<UserModel> userModelArrayList =
                                MultiAccountsHandler.getCustomerDataFromPreferences(LoginActivity.this).userModelArrayList;

                        if (userModelArrayList != null) {
                            userModelArrayList.add(0, userMain.getCustomerData());
                        } else {
                            userModelArrayList = new ArrayList<>();
                            userModelArrayList.add(0, userMain.getCustomerData());
                        }
                        //save the users helper model to the preferences
                        MultiAccountsHandler.setCustomerDataToPreferences(LoginActivity.this, new UsersHelperModel(userModelArrayList));
                        BakcellLogger.logE("cuRenL", "adding Existing user", fromClass, "loginRequest");

                        /**reset the user data for apis calling, menus and promo message*/
                        MultiAccountsHandler.resetAllUserDataAndAPICallerCaches(LoginActivity.this);
                    } else {
                        BakcellLogger.logE("cuRenL", "adding NEW user", fromClass, "loginRequest");
                        /**create the user model array list,
                         * then create the users helper model and save it in the preferences*/
                        ArrayList<UserModel> userModelArrayList = new ArrayList<>();
                        userModelArrayList.add(0, userMain.getCustomerData());
                        //save the users helper model to the preferences
                        MultiAccountsHandler.setCustomerDataToPreferences(LoginActivity.this, new UsersHelperModel(userModelArrayList));
                    }

                    /**set the updates customer data to the local preferences*/
                    DataManager.getInstance().setCurrentUser(MultiAccountsHandler.getCustomerDataFromPreferences(LoginActivity.this).userModelArrayList.get(0));
                } else {
                    Toast.makeText(LoginActivity.this, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
                    return;
                }

                // Call For Add FCM Token
                MyFirebaseMessagingService.addFCMTokenToServer(LoginActivity.this,
                        Constants.NotificationConfigration.FCM_NOTIFICATION_TONE,
                        Constants.NotificationConfigration.FCM_NOTIFICATION_ON,
                        Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_LOGIN);

                //save the time of the login
                BakcellLogger.logE("rateUs", "time:::" + (AppRaterTool.getCurrentDateTime()), fromClass, "currentDateTime");
                PrefUtils.addString(getApplicationContext(), PrefUtils.PreKeywords.USER_LOGIN_TIME, AppRaterTool.getCurrentDateTime());

                startNewActivityAndClear(LoginActivity.this, HomeActivity.class);


                AppEventLogs.applyAppEvent(AppEventLogValues.LoginEvents.LOGIN_SCREEN,
                        AppEventLogValues.LoginEvents.USER_LOGGED_IN,
                        AppEventLogValues.LoginEvents.LOGIN_SCREEN);

                Tools.hideKeyboard(LoginActivity.this);
            }


            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                BakcellPopUpDialog.showMessageDialog(LoginActivity.this,
                        getString(R.string.bakcell_error_title), response.getDescription());
                AppEventLogs.applyAppEvent(AppEventLogValues.LoginEvents.LOGIN_SCREEN,
                        AppEventLogValues.LoginEvents.USER_FAILED_TO_LOGIN,
                        AppEventLogValues.LoginEvents.LOGIN_SCREEN);
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                BakcellPopUpDialog.ApiFailureMessage(LoginActivity.this);
            }

        });
    }
}
