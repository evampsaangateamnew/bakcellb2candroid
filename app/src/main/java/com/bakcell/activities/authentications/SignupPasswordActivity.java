package com.bakcell.activities.authentications;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivitySignupPasswordBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.manageaccounts.UsersHelperModel;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.notifications.MyFirebaseMessagingService;
import com.bakcell.utilities.AppRaterTool;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.SignupPasswordService;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class SignupPasswordActivity extends BaseActivity implements View.OnClickListener {

    RelativeLayout next_layout;
    RelativeLayout accept_bakcell_tc;
    RelativeLayout error_message_box_view;
    RelativeLayout question_icon;
    BakcellEditText etPassword;
    private final String fromClass = "SignupPasswordActivity";
    private boolean isFromManageAccountsFlow = false;

    private boolean isChecked = false;

    private String number = "";

    ActivitySignupPasswordBinding binding;

    private boolean isMatchComplexity = false;

    private String pin;

    private void getIntentData() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY)) {
            if (getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY) != null &&
                    getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY).
                            equalsIgnoreCase(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE)) {
                isFromManageAccountsFlow = true;
            }
        }

        BakcellLogger.logE("fromKeyX", "isFromManageAccountsFlow:::" + isFromManageAccountsFlow, fromClass, "getIntentData");
    }//getIntentData ends

    @Override
    public void onBackPressed() {
// To stop to go back

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup_password);

        getIntentData();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(SignupPinActivity.NUMBER_KEY)) {
                number = bundle.getString(SignupPinActivity.NUMBER_KEY);
            }
            if (bundle.containsKey(SignupPinActivity.PIN_KEY)) {
                pin = bundle.getString(SignupPinActivity.PIN_KEY);
            }
        }

        initUiContents();

        initListeners();

        disableNextButton();

    }

    private void initUiContents() {
        binding.logo.setImageResource(Tools.getBakcellLocalizedLogo(this));
        accept_bakcell_tc = binding.acceptBakcellTc;
        next_layout = binding.nextLayout;
        error_message_box_view = binding.errorMessageBoxView;
        question_icon = binding.questionIcon;
        etPassword = binding.passwordInput;
        binding.nextLayout.setEnabled(false);
    }

    private void initListeners() {

        question_icon.setOnClickListener(this);
        binding.checkMarkIcon.setOnClickListener(this);
        binding.tvTc.setOnClickListener(this);
        binding.nextLayout.setOnClickListener(this);
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isMatchComplexity = FieldFormatter.setPasswordStrengthStatus(SignupPasswordActivity.this,
                        etPassword,
                        binding.passwordStrengthView,
                        binding.redView,
                        binding.orangeView,
                        binding.greenView,
                        binding.tvPasswordStrengthMessage,
                        false);
                if (isMatchComplexity) {
                    enableNextButton();
                } else {
                    disableNextButton();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.confirmPasswordInput.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    processOnNext();
                }
                return false;
            }
        });


    }

    private void requestForSaveCustomer() {

        SignupPasswordService.newInstance(SignupPasswordActivity.this, ServiceIDs.SIGNUP_PASSWORD)
                .execute(number, binding.passwordInput.getText().toString(),
                        binding.confirmPasswordInput.getText().toString(),
                        isChecked, pin, new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                                String result = response.getData();
                                if (result == null) return;

                                UserMain userMain = null;
                                try {
                                    userMain = new Gson().fromJson(result, UserMain.class);
                                } catch (JsonSyntaxException e) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                                }

                                //save the time of the login
                                BakcellLogger.logE("rateUs", "time:::" + (AppRaterTool.getCurrentDateTime()), fromClass, "currentDateTime");
                                PrefUtils.addString(getApplicationContext(), PrefUtils.PreKeywords.USER_LOGIN_TIME, AppRaterTool.getCurrentDateTime());

                                if (userMain != null) {
                                    if (isFromManageAccountsFlow) {
                                        ArrayList<UserModel> userModelArrayList =
                                                MultiAccountsHandler.getCustomerDataFromPreferences(SignupPasswordActivity.this).userModelArrayList;

                                        if (userModelArrayList != null) {
                                            userModelArrayList.add(0, userMain.getCustomerData());
                                        } else {
                                            userModelArrayList = new ArrayList<>();
                                            userModelArrayList.add(0, userMain.getCustomerData());
                                        }
                                        //save the users helper model to the preferences
                                        MultiAccountsHandler.setCustomerDataToPreferences(SignupPasswordActivity.this, new UsersHelperModel(userModelArrayList));
                                        BakcellLogger.logE("cuRenL", "adding Existing user", fromClass, "requestForSaveCustomer");

                                        /**reset the user data for apis calling, menus and promo message*/
                                        MultiAccountsHandler.resetAllUserDataAndAPICallerCaches(SignupPasswordActivity.this);
                                    } else {
                                        BakcellLogger.logE("cuRenL", "adding NEW user", fromClass, "requestForSaveCustomer");
                                        /**create the user model array list,
                                         * then create the users helper model and save it in the preferences*/
                                        ArrayList<UserModel> userModelArrayList = new ArrayList<>();
                                        userModelArrayList.add(0, userMain.getCustomerData());
                                        //save the users helper model to the preferences
                                        MultiAccountsHandler.setCustomerDataToPreferences(SignupPasswordActivity.this, new UsersHelperModel(userModelArrayList));
                                    }

                                    PrefUtils.addCustomerData(SignupPasswordActivity.this, userMain);
                                    DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
                                    /**save the promo message in the data manager*/
                                    if (userMain.getPromoMessage() != null) {
                                        if (Tools.hasValue(userMain.getPromoMessage())) {
                                            DataManager.getInstance().setPromoMessage(userMain.getPromoMessage());
                                        }
                                    }
                                } else {
                                    Toast.makeText(SignupPasswordActivity.this, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                // Call For Add FCM Token
                                MyFirebaseMessagingService.addFCMTokenToServer(SignupPasswordActivity.this, Constants.NotificationConfigration.FCM_NOTIFICATION_TONE, Constants.NotificationConfigration.FCM_NOTIFICATION_ON,
                                        Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_SETTINGS);

                                // Start Home Activity
                                startNewActivityAndClear(SignupPasswordActivity.this, HomeActivity.class);
                                AppEventLogs.applyAppEvent(AppEventLogValues.SignupEvents.SIGN_UP_STEP_THREE,
                                        AppEventLogValues.SignupEvents.SIGN_UP_SUCCESS,
                                        AppEventLogValues.SignupEvents.SIGN_UP_STEP_THREE);

                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                                BakcellPopUpDialog.showMessageDialog(SignupPasswordActivity.this,
                                        getString(R.string.bakcell_error_title), response.getDescription());
                                AppEventLogs.applyAppEvent(AppEventLogValues.SignupEvents.SIGN_UP_STEP_THREE,
                                        AppEventLogValues.SignupEvents.SIGN_UP_FAILURE,
                                        AppEventLogValues.SignupEvents.SIGN_UP_STEP_THREE);
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                BakcellPopUpDialog.ApiFailureMessage(SignupPasswordActivity.this);
                            }
                        });


    }

    private void toggleNextLayout() {
        if (isChecked) {
            isChecked = false;
            binding.checkMarkIcon.setChecked(false);
//            binding.nextLayout.setEnabled(false);
//            binding.nextLbl.setEnabled(false);
//            binding.nextLbl.setTextColor(ContextCompat
//                    .getColor(SignupPasswordActivity.this, R.color.signup_status_line));
//            binding.arrowIcon.setImageDrawable(ContextCompat
//                    .getDrawable(SignupPasswordActivity.this, R.drawable.ic_arrow_dim));
            disableNextButton();
        } else {
            isChecked = true;
            binding.checkMarkIcon.setChecked(true);
//            binding.nextLayout.setEnabled(true);
//            binding.nextLbl.setEnabled(true);
//            binding.nextLbl.setTextColor(ContextCompat
//                    .getColor(SignupPasswordActivity.this, R.color.white));
//            binding.arrowIcon.setImageDrawable(ContextCompat
//                    .getDrawable(SignupPasswordActivity.this, R.drawable.ic_arrow_right));

            enableNextButton();
        }

    }

    private void showTermsAndConditions() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater1 = this.getLayoutInflater();
        final View dialogView = inflater1.inflate(R.layout.dialog_terms_and_conditions, null);
        Button btnOkay = (Button) dialogView.findViewById(R.id.btn_okay);
        dialogBuilder.setView(dialogView);
        final AlertDialog ad = dialogBuilder.show();

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.question_icon:

                if (error_message_box_view.getVisibility() == View.VISIBLE) {
                    error_message_box_view.setVisibility(View.INVISIBLE);
                } else {
                    error_message_box_view.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tv_tc:
                showTermsAndConditions();
                break;
            case R.id.checkMarkIcon:
                toggleNextLayout();
                break;
            case R.id.nextLayout:

                processOnNext();
                break;
        }
    }

    private void processOnNext() {
        if (isChecked) {
            if (binding.passwordInput.getText().toString()
                    .equals(binding.confirmPasswordInput.getText().toString())) {
                if (isMatchComplexity) {
                    requestForSaveCustomer();
                } else {
                    BakcellPopUpDialog.showMessageDialog(SignupPasswordActivity.this,
                            getString(R.string.bakcell_error_title), getString(R.string.error_message_password_match_complexity));
                }
            } else {
                BakcellPopUpDialog.showMessageDialog(SignupPasswordActivity.this,
                        getString(R.string.bakcell_error_title), getString(R.string.error_message_password_not_match));
            }
        } else {
            Tools.hideKeyboard(this);
        }
    }


    private void enableNextButton() {
        if (isChecked && isMatchComplexity) {
            binding.nextLayout.setEnabled(true);
            binding.nextLbl.setTextColor(ContextCompat.getColor(
                    SignupPasswordActivity.this, R.color.white));
            binding.arrowIcon.setImageResource(R.drawable.arrow_right);
        }
    }

    private void disableNextButton() {
        binding.nextLayout.setEnabled(false);
        binding.nextLbl.setTextColor(ContextCompat.getColor(
                SignupPasswordActivity.this, R.color.signup_status_line));
        binding.arrowIcon.setImageResource(R.drawable.forward_arrow_disabled);
    }
}
