package com.bakcell.activities.authentications;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivityForgotPasswordNewPasswordBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.manageaccounts.UsersHelperModel;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.notifications.MyFirebaseMessagingService;
import com.bakcell.utilities.AppRaterTool;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.ForgotPasswordNewPasswordService;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

public class ForgotPasswordNewPasswordActivity extends BaseActivity implements View.OnClickListener {

    ActivityForgotPasswordNewPasswordBinding binding;
    private final String fromClass = "ForgotPasswordNewPasswordActivity";
    private boolean isMatchComplexity = false;
    private boolean isFromManageAccountsFlow = false;
    private String number = "";
    private String pin;

    @Override
    public void onBackPressed() {


    }

    private void getIntentData() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY)) {
            if (getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY) != null &&
                    getIntent().getExtras().getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY).
                            equalsIgnoreCase(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE)) {
                isFromManageAccountsFlow = true;
            }
        }

        BakcellLogger.logE("fromKeyX", "isFromManageAccountsFlow:::" + isFromManageAccountsFlow, fromClass, "getIntentData");
    }//getIntentData ends

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password_new_password);
        binding.logo.setImageResource(Tools.getBakcellLocalizedLogo(this));
        getIntentData();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(ForgotPasswordPinActivity.NUMBER_KEY)) {
                number = bundle.getString(ForgotPasswordPinActivity.NUMBER_KEY);
            }
            if (bundle.containsKey(ForgotPasswordPinActivity.PIN_KEY)) {
                pin = bundle.getString(ForgotPasswordPinActivity.PIN_KEY);
            }
        }

        initUiListeners();

        disableNextButton();
    }

    private void initUiListeners() {
        binding.passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isMatchComplexity = FieldFormatter.setPasswordStrengthStatus(ForgotPasswordNewPasswordActivity.this,
                        binding.passwordInput,
                        binding.passwordStrengthView,
                        binding.redView,
                        binding.orangeView,
                        binding.greenView,
                        binding.tvPasswordStrengthMessage,
                        false);

                if (isMatchComplexity) {
                    enableNextButton();
                } else {
                    disableNextButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.confirmPasswordInput.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    processOnNext();
                }
                return false;
            }
        });

        binding.questionIcon.setOnClickListener(this);
        binding.nextLayout.setOnClickListener(this);
    }

    private void enableNextButton() {
        binding.nextLayout.setEnabled(true);
        binding.nextLbl.setTextColor(ContextCompat.getColor(
                ForgotPasswordNewPasswordActivity.this, R.color.white));
        binding.arrowIcon.setImageResource(R.drawable.arrow_right);
    }

    private void disableNextButton() {
        binding.nextLayout.setEnabled(false);
        binding.nextLbl.setTextColor(ContextCompat.getColor(
                ForgotPasswordNewPasswordActivity.this, R.color.signup_status_line));
        binding.arrowIcon.setImageResource(R.drawable.forward_arrow_disabled);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.question_icon:

                if (binding.errorMessageBoxView.getVisibility() == View.VISIBLE) {
                    binding.errorMessageBoxView.setVisibility(View.INVISIBLE);
                } else {
                    binding.errorMessageBoxView.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.nextLayout:
                processOnNext();
                break;
        }
    }

    private void processOnNext() {
        if (binding.passwordInput.getText().toString().equals(binding.confirmPasswordInput.getText().toString())) {
            if (isMatchComplexity) {
                requestForSetNewPassword();
            } else {
                BakcellPopUpDialog.showMessageDialog(ForgotPasswordNewPasswordActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_message_password_match_complexity));
            }
        } else {
            BakcellPopUpDialog.showMessageDialog(ForgotPasswordNewPasswordActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_message_password_not_match));
        }
    }

    private void requestForSetNewPassword() {

        ForgotPasswordNewPasswordService.newInstance(ForgotPasswordNewPasswordActivity.this, ServiceIDs.FORGOTPASSWORD_NEW_PASSWORD).execute(number, binding.passwordInput.getText().toString(), binding.confirmPasswordInput.getText().toString(), pin, new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                String result = response.getData();

                UserMain userMain = null;
                try {
                    userMain = new Gson().fromJson(result, UserMain.class);
                } catch (JsonSyntaxException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }

                //save the time of the login
                BakcellLogger.logE("rateUs", "time:::" + (AppRaterTool.getCurrentDateTime()), fromClass, "currentDateTime");
                PrefUtils.addString(getApplicationContext(), PrefUtils.PreKeywords.USER_LOGIN_TIME, AppRaterTool.getCurrentDateTime());

                if (userMain != null) {
                    if (isFromManageAccountsFlow) {
                        ArrayList<UserModel> userModelArrayList =
                                MultiAccountsHandler.getCustomerDataFromPreferences(ForgotPasswordNewPasswordActivity.this).userModelArrayList;

                        if (userModelArrayList != null) {
                            userModelArrayList.add(0, userMain.getCustomerData());
                        } else {
                            userModelArrayList = new ArrayList<>();
                            userModelArrayList.add(0, userMain.getCustomerData());
                        }
                        //save the users helper model to the preferences
                        MultiAccountsHandler.setCustomerDataToPreferences(ForgotPasswordNewPasswordActivity.this, new UsersHelperModel(userModelArrayList));
                        BakcellLogger.logE("cuRenL", "adding Existing user", fromClass, "requestForSetNewPassword");

                        /**reset the user data for apis calling, menus and promo message*/
                        MultiAccountsHandler.resetAllUserDataAndAPICallerCaches(ForgotPasswordNewPasswordActivity.this);
                    } else {
                        BakcellLogger.logE("cuRenL", "adding NEW user", fromClass, "requestForSetNewPassword");
                        /**create the user model array list,
                         * then create the users helper model and save it in the preferences*/
                        ArrayList<UserModel> userModelArrayList = new ArrayList<>();
                        userModelArrayList.add(0, userMain.getCustomerData());
                        //save the users helper model to the preferences
                        MultiAccountsHandler.setCustomerDataToPreferences(ForgotPasswordNewPasswordActivity.this, new UsersHelperModel(userModelArrayList));
                    }
                    PrefUtils.addCustomerData(ForgotPasswordNewPasswordActivity.this, userMain);
                    DataManager.getInstance().setCurrentUser(userMain.getCustomerData());

                    // Call For Add FCM Token
                    MyFirebaseMessagingService.addFCMTokenToServer(ForgotPasswordNewPasswordActivity.this, Constants.NotificationConfigration.FCM_NOTIFICATION_TONE, Constants.NotificationConfigration.FCM_NOTIFICATION_ON, Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_LOGIN);

                    // Start Home Activity
                    startNewActivityAndClear(ForgotPasswordNewPasswordActivity.this, HomeActivity.class);
                    AppEventLogs.applyAppEvent(AppEventLogValues.ForgotPaswordEvents.FORGOT_PASWRD_STEP_THREE,
                            AppEventLogValues.ForgotPaswordEvents.FORGOT_PASWRD_SUCCESS,
                            AppEventLogValues.ForgotPaswordEvents.FORGOT_PASWRD_STEP_THREE);

                } else {
                    Toast.makeText(ForgotPasswordNewPasswordActivity.this, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                if (response != null) {
                    BakcellPopUpDialog.showMessageDialog(ForgotPasswordNewPasswordActivity.this,
                            getString(R.string.bakcell_error_title), response.getDescription());
                }
                AppEventLogs.applyAppEvent(AppEventLogValues.ForgotPaswordEvents.FORGOT_PASWRD_STEP_THREE,
                        AppEventLogValues.ForgotPaswordEvents.FORGOT_PASWRD_FAILURE,
                        AppEventLogValues.ForgotPaswordEvents.FORGOT_PASWRD_STEP_THREE);
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                BakcellPopUpDialog.ApiFailureMessage(ForgotPasswordNewPasswordActivity.this);
            }
        });


    }


}
