package com.bakcell.activities.authentications;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivitySignupPinBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.pin.DataPin;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.ResendPinService;
import com.bakcell.webservices.SignupPinVerifyService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class SignupPinActivity extends BaseActivity implements View.OnClickListener {

    public static final String PIN_KEY = "otp.pin";
    public static final String NUMBER_KEY = "new.number";

    ActivitySignupPinBinding binding;

    private String pin = "";
    private String number = "";

    public final long BACK_BUTTON_DELAY_TIME = 25000;

    private final String fromClass = "SignupPinActivity";
    private boolean isFromManageAccountsFlow = false;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup_pin);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (bundle.containsKey(NUMBER_KEY)) {
                number = bundle.getString(NUMBER_KEY);
            }
            if (bundle.containsKey(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY)) {
                if (bundle.getString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY).
                        equalsIgnoreCase(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE)) {
                    isFromManageAccountsFlow = true;
                }
            }
        }

        initUiContent();

        initListeners();

        addBackButtonDelay();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void initUiContent() {
        binding.logo.setImageResource(Tools.getBakcellLocalizedLogo(this));
        Tools.updateTextAfterSec(this, binding.toChangeEnteredMobileNumberTxt);

    }


    private void addBackButtonDelay() {
        disableBackButton();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableBackButton();
            }
        }, BACK_BUTTON_DELAY_TIME);
    }


    private void enableNextButton() {
        binding.nextLayout.setEnabled(true);
        binding.nextLbl.setTextColor(ContextCompat.getColor(
                SignupPinActivity.this, R.color.white));
        binding.arrowIcon.setImageResource(R.drawable.arrow_right);
    }

    private void disableNextButton() {
        binding.nextLayout.setEnabled(false);
        binding.nextLbl.setTextColor(ContextCompat.getColor(
                SignupPinActivity.this, R.color.signup_status_line));
        binding.arrowIcon.setImageResource(R.drawable.forward_arrow_disabled);
    }


    private void enableBackButton() {
        binding.backLayout.setEnabled(true);
        binding.backLbl.setTextColor(ContextCompat.getColor(
                SignupPinActivity.this, R.color.white));
        binding.arrowLeftIcon.setImageResource(R.drawable.arrowleft);
    }

    private void disableBackButton() {
        binding.backLayout.setEnabled(false);
        binding.backLbl.setTextColor(ContextCompat.getColor(
                SignupPinActivity.this, R.color.signup_status_line));
        binding.arrowLeftIcon.setImageResource(R.drawable.backward_arrow_disabled);
    }


    private void initListeners() {
        binding.resendLayout.setOnClickListener(this);
        binding.nextLayout.setOnClickListener(this);
        binding.backLayout.setOnClickListener(this);
        binding.pinInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (Tools.isNumeric(String.valueOf(s)) && s.length() == 4) {
                    enableNextButton();
                } else {
                    disableNextButton();
                }

                //disable the pin input when user starts entering the pin number
                if (Tools.isNumeric(String.valueOf(s)) && s.length() > 0) {
                    disableResendButton();
                } else {
                    enableResendButton();
                }
            }
        });

        binding.pinInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    processOnNext();
                }
                return false;
            }
        });

    }

    private void enableResendButton() {
        binding.resendLayout.setEnabled(true);
        binding.resendLbl.setTextColor(ContextCompat.getColor(
                SignupPinActivity.this, R.color.white));
        binding.arrowLeftResendIcon.setImageResource(R.drawable.arrow_right);
    }//enableResendButton ends

    private void disableResendButton() {
        binding.resendLayout.setEnabled(false);
        binding.resendLbl.setTextColor(ContextCompat.getColor(
                SignupPinActivity.this, R.color.signup_status_line));
        binding.arrowLeftResendIcon.setImageResource(R.drawable.forward_arrow_disabled);
    }//enableResendButton ends

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.nextLayout:
                processOnNext();
                break;
            case R.id.resendLayout:

                requestForResendPin();

                break;
            case R.id.backLayout:
                finish();
                break;

        }
    }

    // Processing on NEXT pressed
    private void processOnNext() {
        String pin = binding.pinInput.getText().toString();
        if (FieldFormatter.isValidPin(pin)) {
            requestForPinVerify(pin);
        } else {
            BakcellPopUpDialog.showMessageDialog(SignupPinActivity.this, getString(R.string.bakcell_error_title), getString(R.string.error_msg_invalid_pin));
        }
    }

    /**
     * Request for Verify Pin
     */
    private void requestForPinVerify(final String pin) {

        SignupPinVerifyService.newInstance(SignupPinActivity.this, ServiceIDs.SIGNUP_PIN_VERIFY).execute(number, pin, EndPoints.Constants.CAUSE_SIGNUP,
                new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        Bundle bundle = new Bundle();
                        bundle.putString(SignupPinActivity.NUMBER_KEY, number);
                        bundle.putString(SignupPinActivity.PIN_KEY, pin);
                        if (isFromManageAccountsFlow) {
                            BakcellLogger.logE("fromKeyX", "from manage accounts", fromClass, "requestForPinVerify");
                            bundle.putString(MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_KEY,
                                    MultiAccountsHandler.MultiAccountConstants.ADD_NEW_ACCOUNT_FROM_MANAGE_ACCOUNTS_SCREEN_VALUE);
                        }
                        BakcellLogger.logE("fromKeyX", "NOT from manage accounts", fromClass, "requestForPinVerify");
                        startNewActivity(SignupPinActivity.this, SignupPasswordActivity.class, bundle);
                        AppEventLogs.applyAppEvent(AppEventLogValues.SignupEvents.SIGN_UP_STEP_TWO,
                                AppEventLogValues.SignupEvents.PIN_VERIFICATION_SUCCESS,
                                AppEventLogValues.SignupEvents.SIGN_UP_STEP_TWO);
                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        BakcellPopUpDialog.showMessageDialog(SignupPinActivity.this,
                                getString(R.string.bakcell_error_title), response.getDescription());
                        AppEventLogs.applyAppEvent(AppEventLogValues.SignupEvents.SIGN_UP_STEP_TWO,
                                AppEventLogValues.SignupEvents.PIN_VERIFICATION_FAILURE,
                                AppEventLogValues.SignupEvents.SIGN_UP_STEP_TWO);
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        BakcellPopUpDialog.ApiFailureMessage(SignupPinActivity.this);
                    }
                });

    }

    /**
     * Request for Resend Pin
     */
    private void requestForResendPin() {

        ResendPinService.newInstance(SignupPinActivity.this, ServiceIDs.RESEND_PIN).execute(number, EndPoints.Constants.CAUSE_SIGNUP, new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                String result = response.getData();

                DataPin dataPin = new Gson().fromJson(result, DataPin.class);

                Toast.makeText(SignupPinActivity.this, R.string.msg_pin_sent, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                BakcellPopUpDialog.showMessageDialog(SignupPinActivity.this,
                        getString(R.string.bakcell_error_title), response.getDescription());

            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                BakcellPopUpDialog.ApiFailureMessage(SignupPinActivity.this);
            }
        });

    }

}
