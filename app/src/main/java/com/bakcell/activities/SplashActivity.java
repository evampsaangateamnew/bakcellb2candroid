package com.bakcell.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bakcell.R;
import com.bakcell.activities.authentications.LoginActivity;
import com.bakcell.activities.landing.ManageAccountsActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.ActivitySplashBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.appversion.AppVersionMain;
import com.bakcell.models.user.UserMain;
import com.bakcell.notifications.MyFirebaseMessagingService;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.CacheConfig;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.AppVersionService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;
import allaudin.github.io.ease.EaseRootValues;

import static com.bakcell.globals.Constants.SSL_PINNING_SPLITTER_DELIMETER;


public class SplashActivity extends BaseActivity {

    private final String fromClass = "SplashActivity";
    private String screenNameToRedirect = "";
    private String screenTABName = "";
    private String offeringId = "";

    @Override
    protected void onResume() {
        super.onResume();
//        Crashlytics.getInstance().crash(); // Force a crash
        loadNDKValues();
    }//onResume ends

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private void loadNDKValues() {
        String collanSeparatedNDKValue = "";
        try {
            collanSeparatedNDKValue = getSSLPinningKey();

            //split the collon separated NDK Value
            //add it to the root values ndk values list
            if (Tools.hasValue(collanSeparatedNDKValue)) {
                EaseRootValues.getInstance().getNdkValues().clear();
                EaseRootValues.getInstance().getNdkValues().addAll(Arrays.asList(collanSeparatedNDKValue.split(SSL_PINNING_SPLITTER_DELIMETER)));
            }
        } catch (Exception exp) {
            BakcellLogger.logE("ndkX", "exception:::" + exp.toString(), fromClass, "loadNDKValues");
        }
        BakcellLogger.logE("ndkX", "collanSeparatedNDKValues:::" + EaseRootValues.getInstance().getNdkValues(), fromClass, "loadNDKValues");
    }//loadNDKValues ends

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String getNeededValue();

    /**a native method to get the ssl pining keys
     * separated by the collons :::*/
    public native String getSSLPinningKey();

    public final long SCREEN_DELAY_TIME = 1000;

    ActivitySplashBinding binding;

    //B@K^@eX*&~A%kZ@i
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppClass.initializeCustomFonts(getAssets());
        iniNeededValues();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        Tools.getScreenWidth(this);
        binding.titleSplash.setImageResource(Tools.getBakcellLocalizedLogo(SplashActivity.this));

        /// Test///

        /// Test///
        RootValues.getInstance().setPromoMessageDisplayed(false);
        DataManager.getInstance().setPromoMessage(null);
        AppEventLogs.applyAppEvent(AppEventLogValues.SplashEvents.SPLASH_SCREEN,
                AppEventLogValues.SplashEvents.APP_LAUNCHED,
                AppEventLogValues.SplashEvents.SPLASH_SCREEN);
        RootValues.getInstance().setLAST_SELECT_MENU(null);
        applyDynamicLinks();


        // Remove Cache update for Some API on new app session for some apis
        CacheConfig.updateCacheFlagsForAPIsCalls(this);

    }

    private void applyDynamicLinks() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            if (deepLink != null && Tools.hasValue(deepLink.toString()) && deepLink.getPathSegments() != null) {

                                List<String> segments = deepLink.getPathSegments();

                                String json = new Gson().toJson(segments);
                                BakcellLogger.logE("dynamic_links", " json = " + json, fromClass, "applyDynamicLinks");


                                if (segments != null && segments.size() > 0) {
                                  /*  if (segments.size() >= 4 ) {

                                    } else if () {

                                    }*/


                                    BakcellLogger.logE("dynamic_links", " segments = " + segments, fromClass, "applyDynamicLinks");
                                    if (segments.size() >= 4
                                            && segments.get(3) != null
                                            && segments.get(3).equalsIgnoreCase(Constants.DynamicLinksConstants.OFFERING_ID)) {

                                        if (Tools.hasValue(segments.get(4))) {
                                            offeringId = segments.get(4);
                                        }

                                        if (Tools.hasValue(segments.get(1))
                                                && segments.get(1).equalsIgnoreCase(Constants.DynamicLinksConstants.TAB_NAME)
                                                && Tools.hasValue(segments.get(2))) {
                                            screenTABName = segments.get(2);
                                            screenNameToRedirect = Constants.DynamicLinksScreenConstants.PACKAGES_SCREEN_CONSTANT;
                                        }

                                    } else if (segments.size() >= 2 && Tools.hasValue(segments.get(1))
                                            && segments.get(1).equalsIgnoreCase(Constants.DynamicLinksConstants.TAB_NAME)
                                            && Tools.hasValue(segments.get(2))) {
                                        screenTABName = segments.get(2);
                                        screenNameToRedirect = Constants.DynamicLinksScreenConstants.PACKAGES_SCREEN_CONSTANT;
                                    } else if (segments.size() >= 1 && Tools.hasValue(segments.get(0))
                                            && segments.get(0).equalsIgnoreCase(Constants.DynamicLinksConstants.TAB_NAME)
                                            && Tools.hasValue(segments.get(1))) {
                                        screenTABName = segments.get(1);
                                        screenNameToRedirect = Constants.DynamicLinksScreenConstants.PACKAGES_SCREEN_CONSTANT;
                                    } else {
                                        screenNameToRedirect = segments.get(segments.size() - 1);
                                    }
                                }

                                if (Tools.hasValue(screenNameToRedirect)) {
                                    RootValues.getInstance().setDynamicLinkRedirection(true);
                                }
                            }

                        }
                        appVersionRequest();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        appVersionRequest();
                    }
                });
    }


    private void iniNeededValues() {
        String neededValeus = getNeededValue();
        if (Tools.hasValue(neededValeus)) {
            String[] valuesArray = neededValeus.split(":::");
            if (valuesArray != null && valuesArray.length == 2) {
                RootValues.getInstance().setCLASS_NAME_M(valuesArray[0]);
                RootValues.getInstance().setCLASS_NAME_SE(valuesArray[1]);
            }
        }

    }

    private String getUserRedirectionScreenNameOnStartUp() {

        String redirectScreen = Constants.MenusKeyword.MENU_BOTTOM_DASHBOARD;

        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(MyFirebaseMessagingService.NOTIFICATION_RINGING_STATUS)) {
                redirectScreen = Constants.MenusKeyword.MENU_NOTIFICATIONS;
                AppEventLogs.applyAppEvent(AppEventLogValues.NotificationEvents.NOTIFICATIONS_SCREEN,
                        AppEventLogValues.NotificationEvents.APP_OPENED_VIA_NOTIFICATIONS,
                        AppEventLogValues.NotificationEvents.NOTIFICATIONS_SCREEN);
            }
        }

        return redirectScreen;
    }


    private void addSplashScreenDelay() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    try {
                        if (PrefUtils.getBoolean(SplashActivity.this, PrefUtils.PreKeywords.PREF_IS_FIRST_TIME, true)) {
                            BaseActivity.startNewActivityAndClear(SplashActivity.this, TutorialsActivity.class);
                            return;
                        }

                        checkIfUserAlreadyLoggenIn();
                    } catch (Exception e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                }
            }, SCREEN_DELAY_TIME);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }


    private void checkIfUserAlreadyLoggenIn() {

        try {
            UserMain userMain = PrefUtils.getCustomerData(SplashActivity.this);

            if (userMain != null && userMain.getCustomerData() != null && Tools.hasValue(userMain.getCustomerData().getMsisdn())) {
                if (PrefUtils.getBoolean(getApplicationContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false)) {
                    BaseActivity.startNewActivityAndClear(SplashActivity.this, LoginActivity.class);
                } else {
                    if (PrefUtils.getBoolean(SplashActivity.this, PrefUtils.PreKeywords.PREF_IS_MANAGE_ACCOUNTS_SCREEN_NEED_TO_SHOW, false)) {
                        /**redirect the user to the manage accounts screen*/
                        BaseActivity.startNewActivityAndClear(SplashActivity.this, ManageAccountsActivity.class);
                    } else {
                        // Check TRUE of app is open after closed
                        RootValues.getInstance().setIS_APP_OPENED_AFTER_CLOSED(true);
                        Bundle bundle = new Bundle();
                        bundle.putString(HomeActivity.KEY_HOME_FIRST_SCREEN, getUserRedirectionScreenNameOnStartUp());

                        bundle.putString(Constants.DynamicLinksConstants.DYNAMIC_LINK_SCREEN_NAME_DATA, screenNameToRedirect);
                        bundle.putString(Constants.DynamicLinksConstants.DYNAMIC_LINK_TAB_DATA, screenTABName);
                        bundle.putString(Constants.DynamicLinksConstants.DYNAMIC_LINK_OFFERING_DATA, offeringId);

                        RootValues.getInstance().setLAST_SELECT_MENU("");
                        BaseActivity.startNewActivityAndClear(SplashActivity.this, HomeActivity.class, bundle);
                    }
                }
            } else {
                BaseActivity.startNewActivityAndClear(SplashActivity.this, LoginActivity.class);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    private void appVersionRequest() {

        if (!Tools.isNetworkAvailable(SplashActivity.this)) {
            addSplashScreenDelay();
            return;
        }

        AppEventLogs.applyAppEvent(AppEventLogValues.SplashEvents.SPLASH_SCREEN,
                AppEventLogValues.SplashEvents.APP_VERSION_CHECK,
                AppEventLogValues.SplashEvents.SPLASH_SCREEN);


        final String AppVersion = Tools.getAppVersion(SplashActivity.this);
        AppVersionService.newInstance(SplashActivity.this, ServiceIDs.REQUEST_APP_VERSION).execute(AppVersion, new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                String result = response.getData();
                AppEventLogs.applyAppEvent(AppEventLogValues.SplashEvents.SPLASH_SCREEN,
                        AppEventLogValues.SplashEvents.APP_VERSION_UPDATED,
                        AppEventLogValues.SplashEvents.SPLASH_SCREEN);
                AppVersionMain appVersionMain = new Gson().fromJson(result, AppVersionMain.class);

                addSplashScreenDelay();


            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                // Check if user logout by server
                if (response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                    Toast.makeText(SplashActivity.this, "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                    HomeActivity.logoutUser(SplashActivity.this);
                    return;
                }


                if (response != null) {
                    if (response.getResultCode().equalsIgnoreCase("8")) {// Force Update
                        AppEventLogs.applyAppEvent(AppEventLogValues.SplashEvents.SPLASH_SCREEN,
                                AppEventLogValues.SplashEvents.NEW_APP_VERSION_AVAILABLE,
                                AppEventLogValues.SplashEvents.SPLASH_SCREEN);
                        showMessageDialogAppUpdate(SplashActivity.this, getString(R.string.splash_btn_lbl), response.getDescription(), 8);
                    } else if (response.getResultCode().equalsIgnoreCase("9")) {// Optional Update
                        String result = response.getData();
                        AppVersionMain appVersionMain = new Gson().fromJson(result, AppVersionMain.class);

                        showMessageDialogAppUpdate(SplashActivity.this, getString(R.string.splash_btn_lbl), response.getDescription(), 9);
                    } else if (response.getResultCode().equalsIgnoreCase("10")) {// Server Down
                        BakcellPopUpDialog.showMessageDialog(SplashActivity.this,
                                getString(R.string.bakcell_error_title), response.getDescription());
                    } else {
                        addSplashScreenDelay();
                    }
                } else {
                    addSplashScreenDelay();
                }
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                try {
                    addSplashScreenDelay();

                } catch (Exception e1) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e1.getMessage());
                }
            }

        });
    }


    public void showMessageDialogAppUpdate(final Context context, String title, String message, int code) {

        try {
            if (context == null || ((Activity) context).isFinishing()) return;

            if (((Activity) context).isFinishing()) return;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.dialog_bakcell_app_update, null);
            LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.root_layout);
            LinearLayout actionLayoutOptionUpdate = (LinearLayout) dialogView.findViewById(R.id.actionLayoutOptionUpdate);
            dialogBuilder.setView(dialogView);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);

            BakcellButtonNormal btn_cancel = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_cancel);
            BakcellButtonNormal btn_update = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_update);
            BakcellButtonNormal btn_update_force = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_update_force);
            BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
            BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
            tvTitle.setText(title);
            tvMessage.setText(message);
            btn_update_force.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {// Update

                    goToPlayStoreForUpdate(context);

                    alertDialog.dismiss();
                }
            });
            btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {// Update
                    goToPlayStoreForUpdate(context);
                    alertDialog.dismiss();
                    finish();
                }
            });
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {// Continue
                    addSplashScreenDelay();
                    alertDialog.dismiss();
                }
            });


            if (code == 8) {
                actionLayoutOptionUpdate.setVisibility(View.GONE);
                btn_update_force.setVisibility(View.VISIBLE);
            } else if (code == 9) {
                actionLayoutOptionUpdate.setVisibility(View.VISIBLE);
                btn_update_force.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    public void goToPlayStoreForUpdate(Context context) {
        if (context != null && !((Activity) context).isFinishing()) {
            final String appPackageName = context.getPackageName();
            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                } catch (Exception e) {
                }
            }
            try {
            } catch (Exception e) {
            }
        }
    }


}