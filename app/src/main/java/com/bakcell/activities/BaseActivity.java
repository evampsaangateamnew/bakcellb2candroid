package com.bakcell.activities;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bakcell.R;
import com.bakcell.dialogs.InAppThankYouDialog;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.InAppSurveyUploadListener;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.inappsurvey.UploadSurveyData;

import java.util.Locale;

import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 04-Jul-17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    public static final String BASE_EXTRA_TEXT_FOR_INTENT_INJECTION = "com.evampsaanga.bakcell";
    public static final String KEY_INJECTION_EXTRA_STRING = "injection_extra_string";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RootValues.getInstance().setCallingSurveyFromDialog(
                (uploadSurvey, onTransactionComplete, dialog, updateListOnSuccessfullySurvey) -> {
                    UploadSurveyData.requestForUploadInAppSurveyData(this, uploadSurvey, new InAppSurveyUploadListener() {
                        @Override
                        public void onInAppSurveyFailListener(EaseResponse<String> response) {

                            if (response != null && response.getDescription() != null) {
                                if (BaseActivity.this != null && !BaseActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.showMessageDialog(BaseActivity.this,
                                            getString(R.string.bakcell_error_title),
                                            response.getDescription()
                                    );
                                }
                            } else {
                                if (BaseActivity.this != null && !BaseActivity.this.isFinishing()) {
                                    BakcellPopUpDialog.showMessageDialog(BaseActivity.this,
                                            getString(R.string.bakcell_error_title),
                                            getString(R.string.server_stopped_responding_please_try_again)
                                    );
                                }
                            }
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onInAppSurveySuccessListener() {
                            if (updateListOnSuccessfullySurvey != null) {
                                updateListOnSuccessfullySurvey.updateListOnSuccessfullySurvey();
                            }
                            if (BaseActivity.this != null && !BaseActivity.this.isFinishing()) {
                                InAppThankYouDialog thankYouDialog = new InAppThankYouDialog(BaseActivity.this, onTransactionComplete);
                                thankYouDialog.show();
                            }
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    });

                });
        try {
            if (getIntent().getAction() != null && (getIntent().getAction().equalsIgnoreCase(Intent.ACTION_MAIN) || getIntent().getData().toString().contains("mybakcell.page.link"))) {
                return;
            }
            boolean isSafe = getIntent().hasExtra(KEY_INJECTION_EXTRA_STRING);
            if (isSafe) {
                String intentInjectionStr = getIntent().getStringExtra(KEY_INJECTION_EXTRA_STRING);
                if (intentInjectionStr == null || !intentInjectionStr
                        .contentEquals(BASE_EXTRA_TEXT_FOR_INTENT_INJECTION +
                                RootValues.getInstance().getTIME_STAMP_FOR_INTENT_INJECTION())) {
                    finish();
                }
            } else {
                finish();
            }
        } catch (Exception ex) {
            Logger.debugLog(getClass().getSimpleName(), ex.getMessage());
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.wrap(newBase, AppClass.getCurrentLanguageKey(newBase)));
    }


    /**
     * A locale helper class for updating app specific locale
     */
    public static class LocaleHelper extends ContextWrapper {

        public LocaleHelper(Context base) {
            super(base);
        }

        public static ContextWrapper wrap(Context context, String language) {
            try {
                if (TextUtils.isEmpty(language.trim())) {
                    return new LocaleHelper(context);
                }
                Configuration config = context.getResources().getConfiguration();
                Locale locale = new Locale(language);
                Locale.setDefault(locale);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    config.setLocale(locale);
                } else {
                    //noinspection deprecation
                    config.locale = locale;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    config.setLayoutDirection(locale);
                    context = context.createConfigurationContext(config);
                } else {
                    //noinspection deprecation
                    context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
                }
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            return new LocaleHelper(context);
        }

    } // LocaleHelper


    /**
     * Start new activity with having previous activity
     *
     * @param activity
     * @param clazz
     */
    public static void startNewActivity(Activity activity, Class clazz) {
        try {
            RootValues.getInstance().setTIME_STAMP_FOR_INTENT_INJECTION(String.valueOf(System.currentTimeMillis()));
            Intent intent = new Intent(activity, clazz);
            intent.putExtra(KEY_INJECTION_EXTRA_STRING,
                    BASE_EXTRA_TEXT_FOR_INTENT_INJECTION + RootValues.getInstance().getTIME_STAMP_FOR_INTENT_INJECTION());
            activity.startActivity(intent);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Start new activity and close previous activity
     *
     * @param activity
     * @param clazz
     */
    public static void startNewActivityAndClear(Activity activity, Class clazz) {
        try {
            RootValues.getInstance().setTIME_STAMP_FOR_INTENT_INJECTION(String.valueOf(System.currentTimeMillis()));
            Intent intent = new Intent(activity, clazz);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(KEY_INJECTION_EXTRA_STRING,
                    BASE_EXTRA_TEXT_FOR_INTENT_INJECTION + RootValues.getInstance().getTIME_STAMP_FOR_INTENT_INJECTION());
            activity.startActivity(intent);
            activity.finish();
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Start new activity and close previous activity with extra data
     *
     * @param activity
     * @param clazz
     * @param bundle
     */
    public static void startNewActivityAndClear(Activity activity, Class clazz, Bundle bundle) {
        try {
            RootValues.getInstance().setTIME_STAMP_FOR_INTENT_INJECTION(String.valueOf(System.currentTimeMillis()));
            Intent intent = new Intent(activity, clazz);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(KEY_INJECTION_EXTRA_STRING,
                    BASE_EXTRA_TEXT_FOR_INTENT_INJECTION + RootValues.getInstance().getTIME_STAMP_FOR_INTENT_INJECTION());
            intent.putExtras(bundle);
            activity.startActivity(intent);
            activity.finish();
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Start new Activity with adding bundle
     *
     * @param activity
     * @param clazz
     * @param bundle
     */
    public static void startNewActivity(Activity activity, Class clazz, Bundle bundle) {
        try {
            RootValues.getInstance().setTIME_STAMP_FOR_INTENT_INJECTION(String.valueOf(System.currentTimeMillis()));
            Intent intent = new Intent(activity, clazz);
            intent.putExtra(KEY_INJECTION_EXTRA_STRING,
                    BASE_EXTRA_TEXT_FOR_INTENT_INJECTION + RootValues.getInstance().getTIME_STAMP_FOR_INTENT_INJECTION());
            intent.putExtras(bundle);
            activity.startActivity(intent);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Start new Activity for Result
     *
     * @param activity
     * @param clazz
     * @param requestCode
     * @param bundle
     */
    public static void startNewActivityForResult(Activity activity, Class clazz,
                                                 int requestCode, Bundle bundle) {
        try {
            RootValues.getInstance().setTIME_STAMP_FOR_INTENT_INJECTION(String.valueOf(System.currentTimeMillis()));
            Intent intent = new Intent(activity, clazz);
            intent.putExtra(KEY_INJECTION_EXTRA_STRING,
                    BASE_EXTRA_TEXT_FOR_INTENT_INJECTION + RootValues.getInstance().getTIME_STAMP_FOR_INTENT_INJECTION());
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, requestCode);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (Tools.isApplicationBroughtToBackground(getApplicationContext())) {
                RootValues.getInstance().setIS_USAGE_DETAIL_OTP_VERIFY(false);
                if (RootValues.getInstance().getPauseInUsageDetailPageListener() != null) {
                    RootValues.getInstance().getPauseInUsageDetailPageListener().onPauseInUsageDetailListen();
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }


} // BaseActivity
