package com.bakcell.utilities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.adapters.CardAdapterSupplementaryOffers;
import com.bakcell.fragments.menus.TariffsFragment;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;


public class Tools {


    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\." +
                "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static void isGooglePlayServicesAvailable(Activity activity) {
        if (activity == null || activity.isFinishing()) return;
        try {
            GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
            int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
            if (status != ConnectionResult.SUCCESS) {
                googleApiAvailability.makeGooglePlayServicesAvailable(activity);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager
                cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean isNumeric(String s) {
        if (s == null) return false;
        return s.trim().matches("[-+]?\\d*\\.?\\d+");
    }

    public static String truncateString(String s, int length) {
        if (s.length() > length)
            return s.substring(0, length) + " ...";
        else
            return s;
    }


    public static String firstCapString(String input) {
        return Character.toUpperCase(input.charAt(0)) + input.substring(1);
    }

    public static void hideKeyboard(Activity activity) {
        try {
            if (activity == null) return;
            InputMethodManager
                    imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date firstOfMonth(int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);

        return cal.getTime();
    }

    public static String getDateStringForModel(Date date) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormatter.format(date);
    }

    public static String getDateString(Date date) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
        return dateFormatter.format(date);
    }


    public static Date getDateFromLiterals(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        return c.getTime();
    }


    public static Date dateFromString(String date) {
        Date convertedDate = null;
        SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
        try {
            convertedDate = format.parse(date);
        } catch (Exception e) {
        }

        return convertedDate;
    }


    public static int daysBetwenDates(Date date1, Date date2) {
        //milliseconds
        long different = date2.getTime() - date1.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;

        return (int) elapsedDays;
    }


    public static String convertDateFormat(String dateString) {
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
        fromFormat.setLenient(false);

        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        toFormat.setLenient(false);

        Date date = null;

        try {
            date = fromFormat.parse(dateString);
        } catch (Exception e) {
            return null;
        }

        assert date != null;
        return toFormat.format(date);
    }


    public static String newConvertDateFormat(String dateString) {
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
        fromFormat.setLenient(false);

        // SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat toFormat = new SimpleDateFormat("dd/MM/yyy");
        toFormat.setLenient(false);

        Date date = null;

        try {
            date = fromFormat.parse(dateString);
        } catch (Exception e) {
            return null;
        }

        return toFormat.format(date);

    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int) (dp * metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {
            return false;
        }
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            // test for connection
            if (cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable()
                    && cm.getActiveNetworkInfo().isConnected()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
        }
        return false;
    }

    public static String getAppVersion(Context context) {
        if (context == null) {
            return "";
        }
        String version = "";
        try {
            PackageInfo pInfo = null;
            try {
                pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            if (pInfo != null) {
                version = pInfo.versionName;
            }
        } catch (Exception ex) {
        }
        return version;
    }

    public static boolean isStrinBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(str.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }

    public static boolean hasValue(String value) {
        try {
            if (value != null && !value.equalsIgnoreCase("null") && !value.equalsIgnoreCase("")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }


    public static int getYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static int getDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }


    public static int boolToInt(Boolean b) {
        return b.compareTo(false);
    }


    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight/*, float angle*/) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        //matrix.postRotate(angle);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }


    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static Bitmap rotateImage(Bitmap bitmap, String filePath) {
        Bitmap resultBitmap = bitmap;

        try {
            ExifInterface exifInterface = new ExifInterface(filePath);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Matrix matrix = new Matrix();

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.postRotate(ExifInterface.ORIENTATION_ROTATE_90);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.postRotate(ExifInterface.ORIENTATION_ROTATE_180);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.postRotate(ExifInterface.ORIENTATION_ROTATE_270);
            }

            // Rotate the bitmap
            resultBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (Exception exception) {
            Log.d("TAG", "Could not rotate the image");
        }
        return resultBitmap;
    }

    public static void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static void getScreenWidth(Context context) {
        int width = 0;
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
            wm.getDefaultDisplay().getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;

            float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
            RootValues.getInstance().setTARIFFS_WIDTH((int) (dpWidth - Constants.TARIFF_ITEM_MARGIN));

            float circleWidth = dpWidth / 3;
            RootValues.getInstance().setDASHBOARD_CIRCLE_WIDTH(circleWidth);
            if (width <= 600) {
                RootValues.getInstance().setCIRCULAR_WIDTH(4);
                RootValues.getInstance().setBACK_CIRCULAR_WIDTH(2);
                RootValues.getInstance().setTARIFF_PAGGER_OFFSET(15);
            } else if (width > 600 && width <= 750) {
                RootValues.getInstance().setCIRCULAR_WIDTH(10);
                RootValues.getInstance().setBACK_CIRCULAR_WIDTH(3);
                RootValues.getInstance().setTARIFF_PAGGER_OFFSET(20);
            } else if (width > 750 && width <= 800) {
                RootValues.getInstance().setCIRCULAR_WIDTH(15);
                RootValues.getInstance().setBACK_CIRCULAR_WIDTH(4);
                RootValues.getInstance().setTARIFF_PAGGER_OFFSET(25);
            } else if (width > 800 && width <= 1080) {
                RootValues.getInstance().setCIRCULAR_WIDTH(15);
                RootValues.getInstance().setBACK_CIRCULAR_WIDTH(5);
                RootValues.getInstance().setTARIFF_PAGGER_OFFSET(40);
            } else {
                RootValues.getInstance().setCIRCULAR_WIDTH(15);
                RootValues.getInstance().setBACK_CIRCULAR_WIDTH(5);
                RootValues.getInstance().setTARIFF_PAGGER_OFFSET(50);
            }
        } catch (Exception e) {
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        try {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }
            int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                if (listItem != null) {
                    // This next line is needed before you call measure or else you won't getString measured height at all. The listitem needs to be drawn first to know the height.
                    listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception ex) {

        }
    }

    public static void setListViewHeightBasedOnChildren(ExpandableListView listView) {
        try {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }
            int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                if (listItem != null) {
                    // This next line is needed before you call measure or else you won't getString measured height at all. The listitem needs to be drawn first to know the height.
                    listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception ex) {

        }
    }

    public static String getDeviceID(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }


    public static String readTextFromFile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            if (input != null) {
                while ((line = input.readLine()) != null) {
                    returnString.append(line);
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (input != null)
                    input.close();
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }


    public static int getIntegerFromString(String string) {
        int val = -1;
        try {
            if (isNumeric(string)) {
                val = Integer.valueOf(string);
            }
        } catch (NumberFormatException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return val;
    }

    public static double getDoubleFromString(String string) {
        double val = -1;
        try {
            if (isNumeric(string)) {
                val = Double.valueOf(string);
            }
        } catch (NumberFormatException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return val;
    }

    public static float getFloatFromString(String string) {
        float val = -1;
        try {
            if (isNumeric(string)) {
                val = Float.valueOf(string);
            }
        } catch (NumberFormatException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return val;
    }

    public static long getMiliSecondsFromDate(String myDate) {
        long miliSec = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_TIME_FORMAT);
            Date date = null;
            try {
                date = sdf.parse(myDate);
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                return 0;
            }
            miliSec = date.getTime();
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return miliSec;
    }

    public static int getDaysDifferenceBetweenMiliseconds(long startDate, long endDate) {

        long duration = endDate - startDate;

        long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);

        int daysRemain = (int) diffInDays;

        return daysRemain;
    }

    public static int getHourssDifferenceBetweenMiliseconds(long startDate, long endDate) {

        long duration = endDate - startDate;

        long diffInDays = TimeUnit.MILLISECONDS.toHours(duration);

        int hoursRemain = (int) diffInDays;

        return hoursRemain;
    }


    public static long getCurrentMiliSeconds() {
        return System.currentTimeMillis();
    }


    public static String getLastDate(String dateString, int days) throws ParseException {

        Date date = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateString);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, -days);
        Date newDate = calendar.getTime();
        calendar.setTime(newDate);
//        String monthOfYear = new SimpleDateFormat(Constants.DateAndMonth.MONTH_FORMAT_NUM).format(calendar.getTime());//MMM for short Name(Jun)
//        String dayOfMonth = new SimpleDateFormat(Constants.DateAndMonth.DATE_OF_MONTH).format(calendar.getTime());//Date Number 01
//        String year = new SimpleDateFormat(Constants.DateAndMonth.YEAR).format(calendar.getTime());//year
        String date_string = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT).format(calendar.getTime());
        return date_string;
    }


    public static String getCurrentDate() throws ParseException {

        SimpleDateFormat date = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT);
        String currentDate = date.format(new Date());

        return currentDate;
    }


    public static String getMonthAndDateFormat(String date, String month) throws ParseException {
        String dateMonth = "";
        if (Tools.hasValue(date)) {
            Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            String monthName = new SimpleDateFormat(month).format(cal.getTime());
            String dateNumber = new SimpleDateFormat("dd").format(cal.getTime());
            dateMonth = dateNumber + "/" + monthName;
        }
        return dateMonth;
    }

    //1st,2nd,3rd,4th, Feb
    public static String getDateWithSuffixAndMonthNameFormat(String date, String month) throws ParseException {

        // Date Format "2017-10-01 00:00:00"
        Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String monthName = new SimpleDateFormat(month).format(cal.getTime());//MMM for short Name(Jun)
        String dateNumber = new SimpleDateFormat("dd").format(cal.getTime());//Date Number 01
        String dateMonth = getDayNumberSuffix(Integer.valueOf(dateNumber)) + " " + monthName;
        return dateMonth;//4th Feb
    }

    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return day + "th";
        }
        switch (day % 10) {
            case 1:
                return day + "st";
            case 2:
                return day + "nd";
            case 3:
                return day + "rd";
            case 31:
                return day + "st";
            default:
                return day + "th";
        }
    }


    public static String getLastMonthFirstDate() throws ParseException {

        String lastDate = "";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = 1;
        calendar.set(year, month, day);

        lastDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT).format(calendar.getTime());

        return lastDate;
    }


    public static String getLastMonthLastDate() throws ParseException {

        String lastDate = "";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(year, month, day);

        lastDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT).format(calendar.getTime());

        return lastDate;
    }


    //22-02-2017 to 22 Feb 2017
    public static String changeDateFormatForTextView(String dateString) throws ParseException {

        // parse the String "29-07-2013" to a java.util.Date object
        Date date = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT).parse(dateString);
        // format the java.util.Date object to the desired format
        String formattedDate = new SimpleDateFormat(Constants.DateAndMonth.NEW_SAMPLE_DATE_FORMAT).format(date);

        return formattedDate;
    }


    //22-02-2017 to 22/02/17
    public static String getDateAccordingToClientSaid(String dateString) throws ParseException {

        // parse the String "29-07-2013" to a java.util.Date object
        Date date = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT).parse(dateString);
        // format the java.util.Date object to the desired format
        String formattedDate = new SimpleDateFormat(Constants.DateAndMonth.NEW_SAMPLE_DATE_FORMAT).format(date);

        return formattedDate;
    }


    public static String changeDateFormatForMethods(String dateString) throws ParseException {

        // parse the String "29-07-2013" to a java.util.Date object //"dd MMM yyyy"
        Date date = new SimpleDateFormat(Constants.DateAndMonth.NEW_SAMPLE_DATE_FORMAT).parse(dateString);
        // format the java.util.Date object to the desired format
        String formattedDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT).format(date);

        return formattedDate;
    }


    public static Drawable getImageFromAssests(Context context, String imagePath) {
        if (context == null) return null;
        // load image
        Drawable d = null;
        if (context != null && Tools.hasValue(imagePath)) {
            try {
                if (context.getAssets() == null) return null;
                // get input stream
                InputStream ims = context.getAssets().open("country_flags/" + imagePath + ".png");
                // load image as Drawable
                d = Drawable.createFromStream(ims, null);
                // set image to ImageView
            } catch (IOException ex) {

                InputStream ims = null;
                try {
                    ims = context.getAssets().open("country_flags/" + "dummy_flag" + ".png");
                } catch (IOException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                // load image as Drawable
                d = Drawable.createFromStream(ims, null);

            }
        } else {
            InputStream ims = null;
            try {
                ims = context.getAssets().open("country_flags/" + "dummy_flag" + ".png");
            } catch (IOException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            // load image as Drawable
            d = Drawable.createFromStream(ims, null);

        }

        return d;
    }

    public static boolean isApplicationBroughtToBackground(Context context) {
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
            if (!tasks.isEmpty()) {
                ComponentName topActivity = tasks.get(0).topActivity;
                if (!topActivity.getPackageName().equals(context.getPackageName())) {
                    return true;
                }
            }
        } catch (SecurityException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return false;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }


    public static String getTimeFromDateAndTime(String dateString) throws ParseException {

        // parse the String "29-07-2013 00:00:00" to a java.util.Date object
        Date date = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT).parse(dateString);
        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
        String time = localDateFormat.format(date);

        return time;
    }

    //minutes:Hours:days
    public static String getTimeSecMinutesHoursDays(String dateString, Context context) throws ParseException {

        SimpleDateFormat endDateFormat = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_TIME_FORMAT);
        String currentDateAndTime = endDateFormat.format(new Date());
        Date endDate = endDateFormat.parse(currentDateAndTime);

        SimpleDateFormat startDateFormat = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_TIME_FORMAT);
        Date startDate = startDateFormat.parse(dateString);
        // format the java.util.Date object to the desired format

        long difference = endDate.getTime() - startDate.getTime();

        String minutesHoursDays = calculateTime(difference, context);

        return minutesHoursDays; //minutes:Hours:days
    }


    public static String calculateTime(long milis, Context context) {

        if (context == null) return "";

        String time = "";
        int day = (int) (milis / (1000 * 60 * 60 * 24));
        int hours = (int) ((milis - (1000 * 60 * 60 * 24 * day)) / (1000 * 60 * 60));
        int minute = (int) (milis - (1000 * 60 * 60 * 24 * day) - (1000 * 60 * 60 * hours)) / (1000 * 60);
        int second = (int) ((milis / 1000) % 60);

        if (day > 0) {
            if (day == 1) {
                time = day + " " + context.getResources().getString(R.string.lbl_day);
            } else {
                time = day + " " + context.getResources().getString(R.string.lbl_days);
            }
        } else if (day < 1 && hours > 0) {

            if (hours == 1) {
                time = hours + " " + context.getResources().getString(R.string.lbl_hr);
            } else {
                time = hours + " " + context.getResources().getString(R.string.lbl_hrs);
            }
        } else if (hours < 1 && minute > 0) {
            if (minute == 1) {
                time = minute + " " + context.getResources().getString(R.string.lbl_min);
            } else {
                time = minute + " " + context.getResources().getString(R.string.lbl_mins_);
            }
        } else if (minute < 1 && second > 0) {

            if (second == 1) {
                time = second + " " + context.getResources().getString(R.string.lbl_sec);
            } else {
                time = second + " " + context.getResources().getString(R.string.lbl_secs);
            }
        } else {

            if (second <= 0) {
                time = "0 " + context.getResources().getString(R.string.lbl_secs);
            }
        }

        return time;
    }

    public static float getDifferenceTimeInSec(Date start, Date end) {
        long totalCallTime = end.getTime() - start.getTime();
        float secs = TimeUnit.MILLISECONDS.toSeconds(totalCallTime);
        return ((int) secs) == 0 ? totalCallTime / 1000f : secs;
    }

    public static TrustManager[] buildTrustManagerFromCertificate(Context context) {
        try {
            // Load CAs from an InputStream
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            // From https://www.washington.edu/itconnect/security/ca/load-der.crt
//            InputStream is = context.getResources().getAssets().open("certificates/bakcell_certificate.crt");
//            InputStream caInput = new BufferedInputStream(is);
//            Certificate ca;
//            try {
//                ca = cf.generateCertificate(caInput);
//                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//            } finally {
//                caInput.close();
//            }

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            return tmf.getTrustManagers();

        } catch (NoSuchAlgorithmException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        } catch (KeyStoreException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        } catch (CertificateException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        } catch (IOException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return null;
    }

    public static float getRoundedDecimal(float unrounded) {//input=23455.89797979
        int precision = 2;
        BigDecimal bd = BigDecimal.valueOf(unrounded);
        BigDecimal rounded = bd.setScale(precision, BigDecimal.ROUND_HALF_UP);
        return rounded.floatValue(); //output =23455.89
    }


    public String extractPinFromMessage(String receviedMessage) {

        String pinNumber = "";

        try {
            if (Tools.hasValue(receviedMessage)) {
                String[] splitString = null;

                String regex = "\\s(\\d{4})";
                Pattern p = Pattern.compile(regex);
                Matcher m = p.matcher(receviedMessage);
                String subString = "";
                while (m.find()) {
                    subString = m.group(1);
                }

                try {
//                    if (splitString != null && splitString.length >= 2) {
//                        String pinString = subString.substring(0, subString.length());
//                        if (pinString.length() == 4) {
//                            String Spin1 = "", Spin2 = "", Spin3 = "", Spin4 = "";
//                            Spin1 = pinString.substring(0);
//                            Spin2 = pinString.substring(1);
//                            Spin3 = pinString.substring(2);
//                            Spin4 = pinString.substring(3);
//
//                        }
//                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        return receviedMessage;
    }


    public static int getBakcellLocalizedLogo(Context context) {


        if (context == null) return R.drawable.title_az;

        String currentLanguage = AppClass.getCurrentLanguageKey(context);

        switch (currentLanguage) {
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN:
                return R.drawable.title_en;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ:
                return R.drawable.title_az;
            case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU:
                return R.drawable.title_ru;
            default:
                return R.drawable.title_az;
        }

    }


    public static boolean checkIfCurrentDateIsExceedFromEndDate(String endDate) {
        boolean isDateExceed = false;
        try {
            // If you already have date objects then skip 1
            // Create 2 dates starts
            SimpleDateFormat currentDateFormat = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT);
            String currentDate = currentDateFormat.format(new Date());
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT);
            Date date1 = sdf.parse(currentDate);
            Date date2 = sdf.parse(endDate);
            // Date object is having 3 methods namely after,before and equals for comparing
            // after() will return true if and only if date1 is after date 2
            if (date1.after(date2)) {
//                System.out.println("currentDate is after endDate");
                isDateExceed = true;
            }
            // before() will return true if and only if date1 is before date2
            if (date1.before(date2)) {
//                System.out.println("currentDate is before endDate");
            }
            //equals() returns true if both the dates are equal
            if (date1.equals(date2)) {
//                System.out.println("currentDate is equal endDate");
            }
//            System.out.println();
        } catch (ParseException ex) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
        }
        return isDateExceed;
    }


    public static long getOneDayPlus(long endMili) {
        if (endMili > 0) {
            endMili = (long) (endMili + 8.64e+7);
        }
        return endMili;
    }

    public static long getOneDayMinus(long startMili) {
        if (startMili > 0) {
            startMili = (long) (startMili - 8.64e+7);
        }
        return startMili;
    }

    public static long getOneHourMinus(long startMili) {
        if (startMili > 0) {
            startMili = (long) (startMili - 3.6e+6);
        }
        return startMili;
    }

    public static int getNumberOfDaysInCurrentMonth() {
        Calendar c = Calendar.getInstance();
        int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        return monthMaxDays;
    }

    public static int getCurrentDayOfCurrentMonth() {
        int day = 0;
        Calendar now = Calendar.getInstance();
        day = now.get(Calendar.DAY_OF_MONTH);
        return day;
    }

    public static boolean isValueRed(String value) {
        if (value == null) return false;
        if (value.equalsIgnoreCase(TariffsFragment.VALUE_FREE_EN)) {
            return true;
        } else if (value.equalsIgnoreCase(TariffsFragment.VALUE_FREE_RU)) {
            return true;
        } else if (value.equalsIgnoreCase(TariffsFragment.VALUE_FREE_AZ)) {
            return true;
        } else if (value.equalsIgnoreCase(TariffsFragment.VALUE_UNLIMITED_EN)) {
            return true;
        } else if (value.equalsIgnoreCase(TariffsFragment.VALUE_UNLIMITED_RU)) {
            return true;
        } else if (value.equalsIgnoreCase(TariffsFragment.VALUE_UNLIMITED_AZ)) {
            return true;
        } else {
            return false;
        }
    }

    public static void updateTextAfterSec(final Context context, final BakcellTextViewNormal textViewNormal) {
        final int[] count = {25};
        new CountDownTimer(26000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                count[0]--;
                String text = context.getResources().getString(R.string.signup_pin_to_change_entered_mobile_lbl, "" + count[0]);
                textViewNormal.setText(text);
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    public static void setTariffsIcons(String iconMap, ImageView icon, Context context) {
        if (context == null || icon == null) return;
        if (!Tools.hasValue(iconMap)) {
            icon.setImageDrawable(null);
            return;
        }
        iconMap = iconMap.trim();
        if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_WHATSAPP_USING)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_whatsapp));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TS_WHATSAPP)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_whatsapp));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TSD_CALL)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_call));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_COUNTRY_WIDE_CALL)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_call));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_INTERNATION_CALLS)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_calls_international));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_ROAMING_CALLS)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_calls_roaming));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_ROAMING_INTERNET)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_internet));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TSD_INTERNET)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_internet));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TSD_COUNTRY_WIDE_INTERNET)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_internet));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TSD_SMS)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_sms));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_COUNTRY_WIDE_SMS)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_sms));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TSD_DISTINATION)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_destination));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TSD_ROUNDING)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_rounding_off));
        } else if (iconMap.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_YOUTUBE_ICON)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_youtube));
        } else if (iconMap.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_INSTAGRAM_ICON)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_insta));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_TS_MMS)) {
            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_mms));
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_DISCOUNT)) {
            icon.setImageDrawable(null);
        } else if (iconMap.equalsIgnoreCase(TariffsFragment.ICON_MAP_FREE)) {
            icon.setImageDrawable(null);
        } else {
            icon.setImageDrawable(null);
        }
    }

    public static void setOffersSubscriptionsIcons(Context context, String iconName, ImageView iconImg) {
        if (!Tools.hasValue(iconName)) {
            iconImg.setVisibility(View.INVISIBLE);
            return;
        }
        iconImg.setVisibility(View.VISIBLE);
        if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_CALL_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_call));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_INTERNET_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_internet));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_WHATSAPP_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_whatsapp));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_PHONE_ICON)) {//58475
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_call));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_INTERNATIONAL_CALLS_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_calls_international));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_ROAMING_CALLS_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_calls_roaming));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_FACEBOOK_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_facebook));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_MMS_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_mms));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_YOUTUBE_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_youtube));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_INSTAGRAM_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_insta));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_campaing));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_TM_REQUIRED_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_tmrequired));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_TM_PRESEENTED_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_tmpresented));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_DISTCOUNTED_CALLS_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_campaigndiscountedcall));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_DISTCOUNTED_INTERNET_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_campaigndiscountedinternet));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_CAMPAIGN_DISTCOUNTED_SMS_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_campaigndiscountedsms));
        } else if (iconName.equalsIgnoreCase(CardAdapterSupplementaryOffers.SUPPLEMENTARY_OFFER_HEADER_SMS_ICON)) {
            iconImg.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_sms));
        } else {
            iconImg.setVisibility(View.INVISIBLE);
        }

    }


}