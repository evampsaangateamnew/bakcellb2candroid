package com.bakcell.utilities;

import android.util.Log;

import com.bakcell.BuildConfig;

public class BakcellLogger {
    public static void logE(String key, String logValue, String fromClass, String fromMethod) {
        if (BuildConfig.LOG_ENABLED) {
            Log.e(key, "logValue:::" + logValue + " from Class:::" + fromClass + " from Method:::" + fromMethod);
        }
    }
}
