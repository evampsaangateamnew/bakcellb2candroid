package com.bakcell.utilities;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.globals.Constants;
import com.bakcell.widgets.BakcellEditText;
import com.bakcell.widgets.BakcellTextViewNormal;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FieldFormatter {

    public static final int PASSWORD_STRENGTH_STRONGE = 3;
    public static final int PASSWORD_STRENGTH_MEDUIM = 2;
    public static final int PASSWORD_STRENGTH_WEEK = 1;
    public static final int PASSWORD_STRENGTH_HIDE = 0;

    /**
     * Checkout password strength status
     *
     * @param password
     * @return
     */
    public static int checkPasswordStrength(Context context, String password) {
        if (Tools.hasValue(password) && password.length() >= 6 && password.length() <= 15) {
            password = password.trim();
            if (password.length() > 8 && getPasswordStrengthCount(context, password) == 2 && getPasswordStrengthCountAlphabets(password) == 2) {
                return PASSWORD_STRENGTH_STRONGE;
            } else if (password.length() > 8 && getPasswordStrengthCount(context, password) == 2 && getPasswordStrengthCountAlphabets(password) == 1) {
                return PASSWORD_STRENGTH_MEDUIM;
            } else if (password.length() > 8 && getPasswordStrengthCountAlphabets(password) == 2 && getPasswordStrengthCount(context, password) == 1) {
                return PASSWORD_STRENGTH_MEDUIM;
            } else if (password.length() >= 6 && getPasswordStrengthCount(context, password) == 2 && getPasswordStrengthCountAlphabets(password) == 2) {
                return PASSWORD_STRENGTH_MEDUIM;
            } else {
                return PASSWORD_STRENGTH_WEEK;
            }
        } else if (password != null && password.length() == 0) {
            return PASSWORD_STRENGTH_HIDE;
        }
        return PASSWORD_STRENGTH_HIDE;
    }

    public static int getPasswordStrengthCountAlphabets(String password) {
        int strength = 0;
        if (checkPasswordContainAlphebetsCapital(password)) {
            strength++;
        }
        if (checkPasswordContainAlphebetsSmall(password)) {
            strength++;
        }
        return strength;
    }

    public static int getPasswordStrengthCount(Context context, String password) {
        int strength = 0;
        if (checkPasswordContainDiggits(password)) {
            strength++;
        }
        if (checkPasswordSpecialCharacter(context, password)) {
            strength++;
        }
        return strength;
    }

    /**
     * Check password containing Capital char
     *
     * @param password
     * @return
     */
    public static boolean checkPasswordContainAlphebetsCapital(String password) {
        Pattern alphabetsCasePattern = Pattern.compile(".*[A-Z].*");
        Matcher matcher;

        if (Tools.hasValue(password) && password.length() >= 6 && password.length() <= 15) {
            matcher = alphabetsCasePattern.matcher(password);
            if (matcher.matches()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check password containing Small char
     *
     * @param password
     * @return
     */
    public static boolean checkPasswordContainAlphebetsSmall(String password) {
        Pattern alphabetsCasePattern = Pattern.compile(".*[a-z].*");
        Matcher matcher;

        if (Tools.hasValue(password) && password.length() >= 6 && password.length() <= 15) {
            matcher = alphabetsCasePattern.matcher(password);
            if (matcher.matches()) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkPasswordContainDiggits(String password) {
        Pattern digitPattern = Pattern.compile(".*[0-9].*");
        Matcher matcher;
        if (Tools.hasValue(password) && password.length() >= 6 && password.length() <= 15) {
            matcher = digitPattern.matcher(password);
            if (matcher.matches()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if password contains any character other than
     * letters, numbers.  Returns false otherwise.
     */
    public static boolean checkPasswordSpecialCharacter(Context context, String password) {
        if (context == null || !Tools.hasValue(password)) return false;
        boolean isSpacialCharContain = false;
        for (int i = 0; i < Constants.SPACIFIC_SPACIAL_CHARS.length(); i++) {
            if (password.contains(Constants.SPACIFIC_SPACIAL_CHARS.charAt(i) + "")) {
                isSpacialCharContain = true;
                break;
            }
        }
        return isSpacialCharContain;
    }


    /**
     * Checkout if paasword match complexity requirements
     *
     * @param context
     * @param passwordInputField
     * @param passwordStrengthStatudMainLayout
     * @param view1
     * @param view2
     * @param view3
     * @param statusText
     * @param isInVisible
     * @return
     */
    public static boolean setPasswordStrengthStatus(Context context,
                                                    BakcellEditText passwordInputField,
                                                    RelativeLayout passwordStrengthStatudMainLayout,
                                                    View view1,
                                                    View view2,
                                                    View view3,
                                                    BakcellTextViewNormal statusText,
                                                    boolean isInVisible) {

        boolean isMatchComplexity = false;

        if (passwordInputField == null || passwordStrengthStatudMainLayout == null
                || view1 == null || view2 == null || view3 == null || statusText == null) {
            return false;
        }

        int strengthValue = checkPasswordStrength(context, passwordInputField.getText().toString());

        switch (strengthValue) {
            case PASSWORD_STRENGTH_HIDE:
                if (isInVisible) {
                    passwordStrengthStatudMainLayout.setVisibility(View.INVISIBLE);
                } else {
                    passwordStrengthStatudMainLayout.setVisibility(View.GONE);
                }
                isMatchComplexity = false;
                break;
            case PASSWORD_STRENGTH_WEEK:
                if (isInVisible) {
                    if (passwordStrengthStatudMainLayout.getVisibility() == View.INVISIBLE) {
                        passwordStrengthStatudMainLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (passwordStrengthStatudMainLayout.getVisibility() == View.GONE) {
                        passwordStrengthStatudMainLayout.setVisibility(View.VISIBLE);
                    }
                }
                view1.setBackgroundColor(ContextCompat.getColor(context, R.color.password_strength_red));
                view2.setBackgroundColor(ContextCompat.getColor(context, R.color.light_gray));
                view3.setBackgroundColor(ContextCompat.getColor(context, R.color.light_gray));
                statusText.setText(R.string.lbl_password_weak);
                isMatchComplexity = true;
                break;
            case PASSWORD_STRENGTH_MEDUIM:
                if (isInVisible) {
                    if (passwordStrengthStatudMainLayout.getVisibility() == View.INVISIBLE) {
                        passwordStrengthStatudMainLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (passwordStrengthStatudMainLayout.getVisibility() == View.GONE) {
                        passwordStrengthStatudMainLayout.setVisibility(View.VISIBLE);
                    }
                }
                view1.setBackgroundColor(ContextCompat.getColor(context, R.color.password_strength_orange));
                view2.setBackgroundColor(ContextCompat.getColor(context, R.color.password_strength_orange));
                view3.setBackgroundColor(ContextCompat.getColor(context, R.color.light_gray));
                statusText.setText(R.string.lbl_password_normal);
                isMatchComplexity = true;
                break;
            case PASSWORD_STRENGTH_STRONGE:
                if (passwordStrengthStatudMainLayout.getVisibility() == View.GONE) {
                    passwordStrengthStatudMainLayout.setVisibility(View.VISIBLE);
                }
                view1.setBackgroundColor(ContextCompat.getColor(context, R.color.password_strength_green));
                view2.setBackgroundColor(ContextCompat.getColor(context, R.color.password_strength_green));
                view3.setBackgroundColor(ContextCompat.getColor(context, R.color.password_strength_green));
                statusText.setText(R.string.lbl_password_strong);
                isMatchComplexity = true;
                break;
        }
        return isMatchComplexity;
    }

    /**
     * Check if passwrod is valid accourding to Bakcell requirements
     *
     * @param password
     * @return
     */
    public static boolean isValidPassword(Context context, String password) {
        boolean isMatchComplexity = false;
        int strengthValue = checkPasswordStrength(context, password);
        switch (strengthValue) {
            case PASSWORD_STRENGTH_HIDE:
                isMatchComplexity = false;
                break;
            case PASSWORD_STRENGTH_WEEK:
                isMatchComplexity = true;
                break;
            case PASSWORD_STRENGTH_MEDUIM:
                isMatchComplexity = true;
                break;
            case PASSWORD_STRENGTH_STRONGE:
                isMatchComplexity = true;
                break;
        }
        return isMatchComplexity;
    }

    /**
     * Check if MSISDN valid accourding to Bakcell Requirements
     *
     * @param msisdn
     * @return
     */
    public static boolean isValidMsisdn(String msisdn) {
        boolean isValidPassword = false;
        if (Tools.hasValue(msisdn) && Tools.isNumeric(msisdn) && msisdn.length() == 9) {
            isValidPassword = true;
        }
        return isValidPassword;
    }

    /**
     * Check if pin is Valid
     *
     * @param pin
     * @return
     */
    public static boolean isValidPin(String pin) {
        boolean isValidPin = false;
        if (Tools.hasValue(pin)) {
            pin = pin.trim();
        }
        if (Tools.hasValue(pin) && Tools.isNumeric(pin) && pin.length() == 4) {
            isValidPin = true;
        }
        return isValidPin;
    }


    public static boolean isEmailValid(String email) {
        boolean isValidEmail = false;
        if (Tools.hasValue(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            isValidEmail = true;
        }
        return isValidEmail;
    }


}
