package com.bakcell.utilities;

import android.content.Context;

import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.webservices.core.EndPoints;

import allaudin.github.io.ease.EaseCacheManager;

/**
 * Created by Noman on 27-Dec-17.
 */

public class CacheConfig {

    /**
     * In this function we update flags through which API call or Cache data return
     * In Menus, Dashboard, Tariff and Supplementary offer APIs
     */
    public static void updateCacheFlagsForAPIsCalls(Context context) {
        if (context == null) return;

        RootValues.getInstance().setTariffApiCall(true);
        RootValues.getInstance().setSupplementaryOffersApiCall(true);
        RootValues.getInstance().setMySubscriptionsApiCall(true);
        RootValues.getInstance().setMyStoreLocatorApiCall(true);
        RootValues.getInstance().setContactUsApiCall(true);
        RootValues.getInstance().setDashboardApiCall(true);
        RootValues.getInstance().setFaqsApiCall(true);

    }

    /**
     * This function is for remove cache for some APIs data
     *
     * @param context
     */
    /*public static void removeCacheForSomeDataOnAppLaunch(Context context) {
        if (context == null) return;
        try {

            // Clear Tariff Cache in All Languages
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_TARIFF + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN);
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_TARIFF + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ);
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_TARIFF + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU);

            // Clear SO Cache in all languages
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_SUPPLEMENTARY_OFFERS + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN);
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_SUPPLEMENTARY_OFFERS + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ);
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_SUPPLEMENTARY_OFFERS + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

//        removeCacheDashboardData(context);

    }*/

    /**
     * This function remove cache for Dashboard data
     *
     * @param context
     */
    /*public static void removeCacheDashboardData(Context context) {
        if (context == null) return;
        try {

            // Clear Dashboard data cache in all languages
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_DAHSBOARD + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN);
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_DAHSBOARD + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ);
            EaseCacheManager.clear(context,
                    EndPoints.ApiCache.KEY_DAHSBOARD + Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }*/

    /**
     * This function remove cache for Menu data
     *
     * @param context
     */
    public static void removeCacheMenuData(Context context) {
        if (context == null) return;
        try {

            // Clear Dashboard data cache in all languages
            String cacheKey = MultiAccountsHandler.CacheKeys.getMenusCacheKey(context);
            EaseCacheManager.clear(context, cacheKey);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    /**
     * This function to remove cache for disclaimer dialog from sharedperferences while logout user
     *
     * @param context Context
     */
    public static void removeCacheDisclaimerDialog(Context context) {
        if (context == null) return;
        try {

            // Clear Cache for operational History
            PrefUtils.addBoolean(context,
                    MultiAccountsHandler.CacheKeys.getOperationalHistoryDisclaimerPopupCacheKey(context), true);

            // Clear Cache for Usage history detail
            PrefUtils.addBoolean(context,
                    MultiAccountsHandler.CacheKeys.getUsageHistoryDisclaimerPopupCacheKey(context), true);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

}
