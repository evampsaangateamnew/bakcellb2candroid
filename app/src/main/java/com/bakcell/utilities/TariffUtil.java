package com.bakcell.utilities;

import com.bakcell.models.DataManager;

/**
 * @author Junaid Hassan on 27, August, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class TariffUtil {
    public static String getTariffMigrationMessageWithPricing(String offeringId) {
        String messageTitle = "";

        if (DataManager.getInstance().getUserPredefinedData() != null &&
                DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices() != null &&
                !DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().isEmpty()) {
            for (int i = 0; i < DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().size(); i++) {
                if (Tools.hasValue(DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().get(i).getKey()) &&
                        DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().get(i).getKey().equals(offeringId)) {
                    messageTitle = DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().get(i).getValue();
                    break;
                }//list not null and empty
            }//for ends
        }//if ends

        if (!Tools.hasValue(messageTitle)) {
            if (DataManager.getInstance().getUserPredefinedData() != null &&
                    DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices() != null &&
                    !DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().isEmpty()) {
                for (int i = 0; i < DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().size(); i++) {
                    if (Tools.hasValue(DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().get(i).getKey()) &&
                            DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().get(i).getKey().equals("default")) {
                        messageTitle = DataManager.getInstance().getUserPredefinedData().getTariffMigrationPrices().get(i).getValue();
                        break;
                    }
                }//for ends
            }//list not null and empty
        }//message title has value check ends

        if (!Tools.hasValue(messageTitle)) {
            messageTitle = "Please Confirm (HC)";
        }

        return messageTitle;
    }//getTariffMigrationMessageWithPricing ends
}//TariffUtil ends
