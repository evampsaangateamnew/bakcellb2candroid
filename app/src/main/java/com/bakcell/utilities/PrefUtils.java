package com.bakcell.utilities;

import android.content.Context;

import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.manageaccounts.UsersHelperModel;
import com.bakcell.models.user.UserMain;
import com.google.gson.Gson;

/**
 * Utility class for saving and retrieving data from shared preferences
 */

public final class PrefUtils {

    private static final String PREF_NAME = "bakcell.prefs";

    public static final class PreKeywords {
        public static final String NOTIFICATION_COUNTER_POPUP_DATE_TIME="notification.counter.popup.date.time";
        public static final String FINGER_PRINT_SHOW = "key.pic.finger.print.show";
        public static final String PREF_APP_RATER_SHOWN = "key.app.rater.shown";
        public static final String USER_LOGIN_TIME = "key.user.login.time";
        public static final String MAY_BE_LATER_TIME = "key.may.be.later.time";
        public static final String PREF_KEY_CURRENT_LANGUAGE = "key.current.language";
        public static final String PREF_KEY_CUSTOMER_DATA = "key.pref.customer";
        public static final String PREF_KEY_IN_APP_SURVEY = "key.pref.in.app.survey";
        public static final String PREF_KEY_CUSTOMER_ACCOUNT_DATA = "key.pref.customers.account.data";
        public static final String PREF_IS_FIRST_TIME = "key.is.first.time";
        public static final String PREF_HOME_PAGE = "key.home.page";
        public static final String PREF_TOP_UP_PAGE = "key.top.up.page";
        public static final String PREF_BUNDLE_PAGE = "key.bundle.page";
        public static final String PREF_FNF_PAGE = "key.fnf.page";
        public static final String PREF_LOAN_PAGE = "key.loan.page";
        public static final String PREF_TRANSFER_MONEY = "key.transfer.money.page";
        public static final String PREF_TARIFF_PAGE = "key.tariff.page";
        public static final String PREF_IS_FIRST_TIME_LOGIN = "key.is.first.time.login";
        public static final String PREF_SETTINGS_NOTIFICATIONS = "key.settings.notifications";
        public static final String PREF_WRONG_ATTEMPS_COUNT = "key.wrong.attemps.count";
        public static final String PREF_IS_MANAGE_ACCOUNTS_SCREEN_NEED_TO_SHOW = "key.manage.accounts.screen.need.to.show";
    }

    private PrefUtils() {
    }

    /**
     * This function for only add user data to shared preferences
     *
     * @param context
     * @param userMain
     */
    public static void addCustomerData(Context context, UserMain userMain) {
        if (context == null) return;
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit().putString(PreKeywords.PREF_KEY_CUSTOMER_DATA, new Gson().toJson(userMain))
                .apply();
    }

    public static void addInAppSurvey(Context context, InAppSurvey inAppSurvey) {
        if (context == null) return;
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit().putString(PreKeywords.PREF_KEY_IN_APP_SURVEY, new Gson().toJson(inAppSurvey))
                .apply();
    }
    public static InAppSurvey getInAppSurvey(Context context) {
        if (context == null) return null;
        String json = getString(context, PreKeywords.PREF_KEY_IN_APP_SURVEY, "{}");
        return new Gson().fromJson(json, InAppSurvey.class);
    }

    public static void addCustomersAccountData(Context context, UsersHelperModel usersHelperModel) {
        if (context == null) return;
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit().putString(PreKeywords.PREF_KEY_CUSTOMER_ACCOUNT_DATA, new Gson().toJson(usersHelperModel))
                .apply();
    }

    public static UsersHelperModel getCustomersAccountData(Context context) {
        if (context == null) return null;
        String json = getString(context, PreKeywords.PREF_KEY_CUSTOMER_ACCOUNT_DATA, "{}");
        return new Gson().fromJson(json, UsersHelperModel.class);
    }

    /**
     * This function for getting only customer data from shared preferences
     *
     * @param context
     * @return
     */
    public static UserMain getCustomerData(Context context) {
        if (context == null) return null;
        String json = getString(context, PreKeywords.PREF_KEY_CUSTOMER_DATA, "{}");
        return new Gson().fromJson(json, UserMain.class);
    }

    /**
     * Add value to shared prefs
     *
     * @param context context for shared prefs
     * @param key     key
     * @param value   value for given key
     */
    public static void addString(Context context, String key, String value) {
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit().putString(key, value)
                .apply();
    } // add

    /**
     * Add int value to shared prefs
     *
     * @param context context for shared prefs
     * @param key     key
     * @param value   value for given key
     */
    public static void addInt(Context context, String key, int value) {
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit().putInt(key, value)
                .apply();
    } // addInt

    /**
     * Add float value to shared prefs
     *
     * @param context context for shared prefs
     * @param key     key
     * @param value   value for given key
     */
    public static void addFloat(Context context, String key, float value) {
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit().putFloat(key, value)
                .apply();
    } // addFloat


    /**
     * Add boolean value to shared prefs
     *
     * @param context
     * @param key
     * @param value
     */
    public static void addBoolean(Context context, String key, boolean value) {
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit().putBoolean(key, value)
                .apply();
    } // addFloat


    /**
     * Get boolean from prefs with given default value
     *
     * @param context context for shared prefs
     * @param key     key for this field
     * @param def     default value
     * @return value as String
     */
    public static boolean getBoolean(Context context, String key, boolean def) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .getBoolean(key, def);
    }


    /**
     * Get string from prefs with given default value
     *
     * @param context context for shared prefs
     * @param key     key for this field
     * @param def     default value
     * @return value as String
     */
    public static String getString(Context context, String key, String def) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .getString(key, def);
    }

    /**
     * Get String with given key and default to empty string
     *
     * @param context context for shared prefs
     * @param key     pref key
     * @return String against this key
     */
    public static String getString(Context context, String key) {
        return getString(context, key, "");
    }

    /**
     * Get value as given type
     *
     * @param context context for shared prefs
     * @param key     key for this field
     * @param def     default value
     * @param type    type to return
     * @param <T>     return type
     * @return T value parsed to given type
     */
    public static <T> T getAs(Context context, String key, String def, Class<T> type) {
        return type.cast(getString(context, key, def));
    }

    /**
     * @see PrefUtils#getAs(Context, String, String, Class)
     */
    public static int getAsInt(Context context, String key) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getInt(key, 0);
    }

    /**
     * @see PrefUtils#getAs(Context, String, String, Class)
     */
    public static float getAsFloat(Context context, String key) {
        return getAs(context, key, "0.0", float.class);
    }


    /**
     * Get JSON from prefs and convert it to given model
     *
     * @param context context for shared prefs
     * @param key     pref key
     * @param clazz   class of model
     * @param <T>     type of model
     * @return T modeled Json
     */
    public static <T> T getAsJson(Context context, String key, Class<T> clazz) {
        String json = getString(context, key, "{}");
        return new Gson().fromJson(json, clazz);
    }
} // PrefUtils
