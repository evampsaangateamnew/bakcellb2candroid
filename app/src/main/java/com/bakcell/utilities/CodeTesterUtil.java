package com.bakcell.utilities;

/**
 * @author Junaid Hassan on 21, August, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class CodeTesterUtil {
    public static void main(String[] args) {
        Computer comp = new Computer.Builder().setHDD("Yes HDD, available").build();
        System.out.println("Ram" + comp.getHDD());
    }
}//class ends

class Computer {

    //optional parameters
    private String HDD;
    private String RAM;
    private boolean isGraphicsCardEnabled;
    private boolean isBluetoothEnabled;


    public String getHDD() {
        return HDD;
    }

    public String getRAM() {
        return RAM;
    }

    public boolean isGraphicsCardEnabled() {
        return isGraphicsCardEnabled;
    }

    public boolean isBluetoothEnabled() {
        return isBluetoothEnabled;
    }

    private Computer(Builder builder) {
        this.HDD = builder.HDD;
        this.RAM = builder.RAM;
        this.isGraphicsCardEnabled = builder.isGraphicsCardEnabled;
        this.isBluetoothEnabled = builder.isBluetoothEnabled;
    }

    //Builder Class
    public static class Builder {
        // optional parameters
        private String HDD;
        private String RAM;
        private boolean isGraphicsCardEnabled;
        private boolean isBluetoothEnabled;

        public Builder() {
        }

        public Builder setRAM(String RAM) {
            this.RAM = RAM;
            return this;
        }

        public Builder setHDD(String HDD) {
            this.HDD = HDD;
            return this;
        }

        public Builder setGraphicsCardEnabled(boolean isGraphicsCardEnabled) {
            this.isGraphicsCardEnabled = isGraphicsCardEnabled;
            return this;
        }

        public Builder setBluetoothEnabled(boolean isBluetoothEnabled) {
            this.isBluetoothEnabled = isBluetoothEnabled;
            return this;
        }

        public Computer build() {
            return new Computer(this);
        }
    }//Builder class ends
}//computer class ends

/**please do not delete this method
 * this is for ssl pining as it was not working
 * so i created this new method for ssl pining
 * created by Junaid Hassan @ 27-12-2019*/
    /*@Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        boolean hasPublicKey = false;

        for (int i = 0; i < chain.length; i++) {
            if (chain[i].getPublicKey() != null) {
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-256");
                    md.update(chain[i].getPublicKey().getEncoded());
                    byte[] dig = md.digest();
                    String certForMatch = Base64.encodeToString(dig, Base64.DEFAULT);
                    String serverPublicKey = certForMatch.trim();
                    for (String localKey : keys) {
                        Log.e("asdhjkkh2j3", "public:::" + serverPublicKey + " local:::" + localKey);
                        if (localKey.equalsIgnoreCase(serverPublicKey)) {
                            hasPublicKey = true;
                            break;
                        }
                    }
                } catch (NoSuchAlgorithmException e) {
                    Log.d("Tag", String.format("%s", e.getMessage()));
                }
            }
        }

        if (!hasPublicKey) {
            throw new CertificateException("Client is not trusted.");
        }
    }*/