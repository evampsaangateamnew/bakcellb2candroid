package com.bakcell.utilities;

import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import androidx.annotation.RequiresApi;

import com.bakcell.globals.RootValues;

@RequiresApi(Build.VERSION_CODES.M)
public class BiometricHandler extends FingerprintManager.AuthenticationCallback {

    private CancellationSignal cancellationSignal = null;

    public BiometricHandler() {
        super();
    }

    public void starAuthentication(FingerprintManager manager) {
        cancellationSignal = new CancellationSignal();
        manager.authenticate(null, cancellationSignal, 0, this, null);
    }//starAuthentication ends

    public void stopAuthentication() {
        if (cancellationSignal != null) {
            cancellationSignal.cancel();
            cancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
                @Override
                public void onCancel() {
                    BakcellLogger.logE("cancellationSignal", "cancellation signal cancelled success", "BiometricHandler", "stopAuthentication");
                }
            });
        }
        BakcellLogger.logE("cancellationSignal", "stopAuthentication:::called", "BiometricHandler", "stopAuthentication");
    }//stopAuthentication ends

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
        BakcellLogger.logE("cancellationSignal", "onAuthenticationError:::" + (errString.toString()), "BiometricHandler", "onAuthenticationError");
        RootValues.getInstance().getFingerPrintScannerToLoginCallBack().onBiometricAuthenticationError(errorCode, errString.toString());
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);
        BakcellLogger.logE("cancellationSignal", "onAuthenticationHelp:::" + (helpString), "BiometricHandler", "onAuthenticationHelp");
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        BakcellLogger.logE("cancellationSignal", "onAuthenticationSucceeded:::", "BiometricHandler", "onAuthenticationSucceeded");
        RootValues.getInstance().getFingerPrintScannerToLoginCallBack().onBiometricAuthenticationSuccess();

    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();
        BakcellLogger.logE("cancellationSignal", "onAuthenticationFailed:::", "BiometricHandler", "onAuthenticationFailed");
        RootValues.getInstance().getFingerPrintScannerToLoginCallBack().onBiometricAuthenticationFailed();

    }
}
