package com.bakcell.utilities;

import android.annotation.SuppressLint;
import android.app.Activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.bakcell.globals.Constants.DateAndMonth.NOTIFICATION_COUNTER_POPUP_DATE_TIME_FORMAT;

/**
 * @author Junaid Hassan on 01, September, 2020
 * Senior Android Engineer. Islamabad Pakistan
 */
public class NotificationCounterPopupUtil {
    public static void setPopupShownDateTimeFromPrefs(Activity activity) {
        PrefUtils.addString(activity, PrefUtils.PreKeywords.NOTIFICATION_COUNTER_POPUP_DATE_TIME, getCurrentDateTime());
    }//setPopupShownDateTimeFromPrefs ends

    public static String getPopupShownDateTimeFromPrefs(Activity activity) {
        return PrefUtils.getString(activity, PrefUtils.PreKeywords.NOTIFICATION_COUNTER_POPUP_DATE_TIME, "");
    }//getPopupShownDateTimeFromPrefs ends

    public static String getCurrentDateTime() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateTime = new SimpleDateFormat(NOTIFICATION_COUNTER_POPUP_DATE_TIME_FORMAT);
        return dateTime.format(new Date());
    }//getCurrentDateTime ends

    public static long getHoursDifference(String date1, String date2) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(NOTIFICATION_COUNTER_POPUP_DATE_TIME_FORMAT);
            Date oldDate = format.parse(date2);
            Date currentDate = format.parse(date1);

            assert currentDate != null;
            assert oldDate != null;
            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            return hours;
        } catch (Exception exp) {
            BakcellLogger.logE("notifyCP", "Error:::" + exp.toString(), "NotificationCounterPopupUtil", "getHoursDifference");
            return 0;
        }
    }//getHoursDifference ends
}//NotificationCounterPopupUtil ends
