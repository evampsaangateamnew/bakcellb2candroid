package com.bakcell.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppRaterTool {

    private static final String SAMPLE_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String getCurrentDateTime() {
        SimpleDateFormat dateTime = new SimpleDateFormat(SAMPLE_DATE_TIME_FORMAT);
        return dateTime.format(new Date());
    }

    public static long getHoursDifference(Date date1, Date date2, String key) {
        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        if (key.equalsIgnoreCase("h")) {
            //give hours
            return hours;
        } else {
            //give days
            return days;
        }

    }

    public static long getDifference(String _startDate, String _endDate, String key) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(SAMPLE_DATE_TIME_FORMAT);
        Date startDate = format.parse(_startDate);
        Date endDate = format.parse(_endDate);

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = (different / daysInMilli);
        different %= daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different %= hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different %= minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        if (key.equalsIgnoreCase("h")) {
            return elapsedHours;
        } else {
            return elapsedDays;
        }

    }
}
