package com.bakcell.utilities;

import android.util.Log;

import com.bakcell.BuildConfig;


public class Logger {

    public static final String TAG_CATCH_LOGS = "catched_logs";

    /**
     * @param tag
     * @param message
     */
    public static void debugLog(String tag, String message) {
        if (BuildConfig.LOG_ENABLED) {
            Log.d(tag, message);
        }
    }

    /**
     * @param tag
     * @param message
     */
    public static void errorLog(String tag, String message) {
        if (BuildConfig.LOG_ENABLED) {
            Log.e(tag, message);
        }
    }

    /**
     * @param tag
     * @param message
     */
    public static void warnLog(String tag, String message) {
        if (BuildConfig.LOG_ENABLED) {
            Log.w(tag, message);
        }
    }
}
