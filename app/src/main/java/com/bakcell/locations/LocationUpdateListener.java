package com.bakcell.locations;


import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Noman on 30/9/2017.
 */

public interface LocationUpdateListener {

    void onLocationReceive(LatLng location);

}
