package com.bakcell.notifications;

import com.bakcell.models.notifications.SettingsNotifications;

/**
 * Created by Noman on 06/10/2017.
 */

public interface NotificationAppIddSetListenerInterface {

    void onAppIdSentSuccess(SettingsNotifications settingsNotifications, String message);

    void onAppIdSentError(int resultCode, String message);


}
