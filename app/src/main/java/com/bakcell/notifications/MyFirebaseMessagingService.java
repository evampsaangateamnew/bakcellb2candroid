package com.bakcell.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.SplashActivity;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.notifications.SettingsNotifications;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestAddFCMService;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String NOTIFICATION_RINGING_STATUS = "ringingAction";
    private static int count = 0;
    private static int notiId = 0;
    private static int taskCount = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage == null) return;

        String notTitle = "", notBody = "";
        if (remoteMessage != null && remoteMessage.getNotification() != null) {
            notTitle = remoteMessage.getNotification().getTitle();
            notBody = remoteMessage.getNotification().getBody();
        }

        Logger.debugLog("Notififcation", "notTitle");


        showNotification(notTitle, notBody, remoteMessage.getData());


    }


    public void showNotification(String notTitle, String notBody, Map<String, String> notDataMap) {

        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_notification)
//                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification_icon))
                        .setContentTitle(notTitle)
                        .setContentText(notBody)
                        .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(notBody)).setAutoCancel(true);


        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.notification_channel_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        Intent notificationIntent = new Intent(this, SplashActivity.class);

        if (notDataMap != null) {
            // Set Ringing status
            if (notDataMap.containsKey(NOTIFICATION_RINGING_STATUS)) {
                notificationIntent.putExtra(NOTIFICATION_RINGING_STATUS, notDataMap.get(NOTIFICATION_RINGING_STATUS));
                if (Constants.NotificationConfigration.FCM_NOTIFICATION_TONE.equalsIgnoreCase(notDataMap.get(NOTIFICATION_RINGING_STATUS))) {
                    mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                    mBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                } else if (Constants.NotificationConfigration.FCM_NOTIFICATION_VIBRATE.equalsIgnoreCase(notDataMap.get(NOTIFICATION_RINGING_STATUS))) {
                    mBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                } else if (Constants.NotificationConfigration.FCM_NOTIFICATION_MUTE.equalsIgnoreCase(notDataMap.get(NOTIFICATION_RINGING_STATUS))) {
                }
            }
        }

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(intent);


        mNotificationManager.notify(notiId++, mBuilder.build());


        setBadge(this, 0);

    }


    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        if (DataManager.getInstance() != null) {
            SettingsNotifications notificationSettings = DataManager.getInstance().getNotificationSettingsData(this);
            if (Tools.hasValue(s) && notificationSettings != null && Tools.hasValue(notificationSettings.getIsEnable()) && Tools.hasValue(notificationSettings.getRingingStatus())) {
                addFCMTokenToServer(this, s, notificationSettings.getRingingStatus(), notificationSettings.getIsEnable(), Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_LOGIN);
            }
        }
    }

    public static void addFCMTokenToServer(final Context context, final String ringingStatus, final String isEnable, final String cause) {
        if (context == null) return;
        try {
            if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser()
                    != null && Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(task -> {
                            if (!task.isSuccessful()) {
                                BakcellLogger.logE("fcm", "fcm token task not successful", "MyFirebaseMessagingService", "addFCMTokenToServer");
                                return;
                            }

                            if (task.getResult() != null) {
                                // Get new Instance ID token
                                String token = task.getResult().getToken();
                                requestingForSendFCMToken(true, context,
                                        DataManager.getInstance().getCurrentUser(), token, ringingStatus,
                                        isEnable, cause, new NotificationAppIddSetListenerInterface() {
                                            @Override
                                            public void onAppIdSentSuccess(SettingsNotifications settingsNotifications, String message) {
                                                BakcellLogger.logE("fcm", "fcm token sent successfully", "MyFirebaseMessagingService", "addFCMTokenToServer");
                                            }

                                            @Override
                                            public void onAppIdSentError(int resultCode, String message) {
                                                if (count < 3 && Tools.haveNetworkConnection(context)) {
                                                    addFCMTokenToServer(context, ringingStatus, isEnable, cause);
                                                    count++;
                                                } else {
                                                    count = 0;
                                                }
                                            }
                                        });
                            } else {
                                if (taskCount < 3 && Tools.haveNetworkConnection(context)) {
                                    addFCMTokenToServer(context, ringingStatus, isEnable, cause);
                                    taskCount++;
                                } else {
                                    taskCount = 0;
                                }
                            }
                        });
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    private void addFCMTokenToServer(final Context context, String token, final String ringingStatus, final String isEnable, final String cause) {
        if (context == null) return;
        try {
            if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser()
                    != null && Tools.hasValue(DataManager.getInstance().getCurrentUser().getMsisdn())) {
                requestingForSendFCMToken(true, context,
                        DataManager.getInstance().getCurrentUser(), token, ringingStatus,
                        isEnable, cause, new NotificationAppIddSetListenerInterface() {
                            @Override
                            public void onAppIdSentSuccess(SettingsNotifications settingsNotifications, String message) {
                                BakcellLogger.logE("fcm", "fcm token sent successfully", "MyFirebaseMessagingService", "addFCMTokenToServer");
                            }

                            @Override
                            public void onAppIdSentError(int resultCode, String message) {
                                if (count < 3 && Tools.haveNetworkConnection(context)) {
                                    addFCMTokenToServer(context, ringingStatus, isEnable, cause);
                                    count++;
                                } else {
                                    count = 0;
                                }
                            }
                        });
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    public static void requestingForSendFCMToken(boolean isBackground, final Context context, UserModel customerData, String fcmID, String ringingStatus, String isEnable, String cause, final NotificationAppIddSetListenerInterface anInterface) {
        if (context == null || customerData == null || anInterface == null) {
            return;
        }

        if (!Tools.hasValue(fcmID)) {
            Toast.makeText(context, "FCM key not fount.", Toast.LENGTH_SHORT).show();
            return;
        }

        RequestAddFCMService.newInstance(context, ServiceIDs.REQUEST_ADD_FCM).execute(customerData,
                fcmID, ringingStatus, isEnable, cause, isBackground, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        String result = response.getData();

                        SettingsNotifications settingsNotifications = null;
                        if (result != null) {
                            settingsNotifications = new Gson().fromJson(result, SettingsNotifications.class);
                        }

                        if (settingsNotifications != null) {
                            if (Tools.hasValue(settingsNotifications.getIsEnable()) && Tools.hasValue(settingsNotifications.getRingingStatus())) {
                                DataManager.getInstance().saveNotificationSettingsData(context, settingsNotifications);
                            }
                        }

                        anInterface.onAppIdSentSuccess(settingsNotifications, response.getDescription());
                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response != null && anInterface != null) {
                            anInterface.onAppIdSentError(1, response.getDescription());
                        }
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (context != null && anInterface != null) {
                            anInterface.onAppIdSentError(2, context.getString(R.string.server_stopped_responding_please_try_again));
                        }
                    }//end of onError
                });
    }

}
