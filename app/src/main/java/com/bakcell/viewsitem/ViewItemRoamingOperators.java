package com.bakcell.viewsitem;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

public class ViewItemRoamingOperators extends FrameLayout {

    private LinearLayout main;
    private LinearLayout separatorRow;
    private BakcellTextViewNormal serviceName;
    private BakcellTextViewNormal serviceValue;
    private BakcellTextViewNormal manatSymbol;
    private Context context;

    @SuppressLint("InflateParams")
    public ViewItemRoamingOperators(@NonNull Context context) {
        super(context);
        this.context = context;
        main = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.roaming_operators_item, null);
        serviceName = main.findViewById(R.id.serviceName);
        serviceValue = main.findViewById(R.id.serviceValue);
        separatorRow = main.findViewById(R.id.separatorRow);
        manatSymbol = main.findViewById(R.id.manatSymbol);
        this.addView(main);
    }//ViewItemRoamingOperators ends

    public BakcellTextViewNormal getServiceName() {
        return serviceName;
    }//getServiceName ends

    public void setServiceName(BakcellTextViewNormal serviceName) {
        this.serviceName = serviceName;
    }//setServiceName ends

    public BakcellTextViewNormal getServiceValue() {
        return serviceValue;
    }//getServiceValue ends

    public void setServiceValue(BakcellTextViewNormal serviceValue) {
        this.serviceValue = serviceValue;
    }//setServiceValue ends

    public void showContentView() {
        separatorRow.setVisibility(View.VISIBLE);
    }//showContentView ends

    public void hideContentView() {
        separatorRow.setVisibility(View.GONE);
    }//hideContentView ends

    public void showManatSymbol() {
        manatSymbol.setVisibility(View.VISIBLE);
        manatSymbol.setTextColor(context.getResources().getColor(R.color.colorPrimary));
    }//showManatSymbol ends

    public void hideManatSymbol() {
        manatSymbol.setVisibility(View.GONE);
        manatSymbol.setTextColor(context.getResources().getColor(R.color.text_gray));
    }//hideManatSymbol ends
}//class ends
