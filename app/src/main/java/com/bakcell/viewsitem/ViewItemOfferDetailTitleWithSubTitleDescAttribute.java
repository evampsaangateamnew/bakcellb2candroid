package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Noman on 29-May-17.
 */

public class ViewItemOfferDetailTitleWithSubTitleDescAttribute extends FrameLayout {

    private RelativeLayout main;

    private BakcellTextViewNormal subTitle, value, desc_txt,priceIcon;

    public ViewItemOfferDetailTitleWithSubTitleDescAttribute(@NonNull Context context) {

        super(context);
        main = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.offer_detail_title_subtitle_desc_attribute_item, null);

        subTitle = (BakcellTextViewNormal) main.findViewById(R.id.subTitle);
        value = (BakcellTextViewNormal) main.findViewById(R.id.value);
        priceIcon = (BakcellTextViewNormal) main.findViewById(R.id.priceIcon);
        desc_txt = (BakcellTextViewNormal) main.findViewById(R.id.desc_txt);

        this.addView(main);

    }

    public BakcellTextViewNormal getSubTitle() {
        return subTitle;
    }

    public BakcellTextViewNormal getValue() {
        return value;
    }

    public BakcellTextViewNormal getDesc_txt() {
        return desc_txt;
    }

    public BakcellTextViewNormal getPriceIcon() {
        return priceIcon;
    }
}
