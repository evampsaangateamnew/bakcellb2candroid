package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class ViewItemOfferDetailPriceAttribute extends FrameLayout {

    private RelativeLayout main;

    private BakcellTextViewNormal priceSubTitle, prceValue, priceIcon;

    public ViewItemOfferDetailPriceAttribute(@NonNull Context context) {

        super(context);
        main = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.offer_detail_price_attribute_item, null);

        priceSubTitle = (BakcellTextViewNormal) main.findViewById(R.id.priceSubTitle);
        prceValue = (BakcellTextViewNormal) main.findViewById(R.id.prceValue);
        priceIcon = (BakcellTextViewNormal) main.findViewById(R.id.priceIcon);
        priceSubTitle.setSelected(true);
        prceValue.setSelected(true);
        this.addView(main);

    }

    public BakcellTextViewNormal getPriceSubTitle() {
        return priceSubTitle;
    }

    public BakcellTextViewNormal getPrceValue() {
        return prceValue;
    }

    public BakcellTextViewNormal getPriceIcon() {
        return priceIcon;
    }

    public void hideAZN() {
        priceIcon.setText("");
    }

    public void showAZN() {
        priceIcon.setText(Html.fromHtml("&nbsp;&#x20bc;"));
    }
}
