package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Zeeshan Shabbir on 9/5/2017.
 */

public class ViewItemTariffsAttribute extends FrameLayout {
    private View seperator;
    private RelativeLayout main, rl_left;
    private BakcellTextViewNormal attribute_label_text;
    private BakcellTextViewNormal credit_value_txt_1, credit_value_txt_2, tv_tzn_2, tv_tzn_1;

    public ViewItemTariffsAttribute(@NonNull Context context) {
        super(context);
        main = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.item_view_tariffs_attribute, null);
        attribute_label_text = (BakcellTextViewNormal) main.findViewById(R.id.attribute_label_text);
        rl_left = (RelativeLayout) main.findViewById(R.id.rl_left);
        credit_value_txt_1 = (BakcellTextViewNormal) main.findViewById(R.id.credit_value_txt_1);
        credit_value_txt_1.setSelected(true);
        credit_value_txt_2 = (BakcellTextViewNormal) main.findViewById(R.id.credit_value_txt_2);
        credit_value_txt_2.setSelected(true);
        tv_tzn_1 = (BakcellTextViewNormal) main.findViewById(R.id.tv_tzn_1);
        tv_tzn_2 = (BakcellTextViewNormal) main.findViewById(R.id.tv_tzn_2);
        seperator = main.findViewById(R.id.seperator);
        credit_value_txt_1.setSelected(true);
        attribute_label_text.setSelected(true);

        this.addView(main);
    }

    public BakcellTextViewNormal getTv_tzn_2() {
        return tv_tzn_2;
    }

    public BakcellTextViewNormal getTv_tzn_1() {
        return tv_tzn_1;
    }

    public RelativeLayout getRl_left() {
        return rl_left;
    }

    public BakcellTextViewNormal getAttribute_label_text() {
        return attribute_label_text;
    }

    public BakcellTextViewNormal getCredit_value_txt_1() {
        return credit_value_txt_1;
    }

    public BakcellTextViewNormal getCredit_value_txt_2() {
        return credit_value_txt_2;
    }

    public void setRightValueTextColor(@ColorRes int color) {
        credit_value_txt_1.setTextColor(getResources().getColor(color));
    }

    public void setLeftValueTextColor(@ColorRes int color) {
        credit_value_txt_2.setTextColor(getResources().getColor(color));
    }

    public void hideRightAZN() {
        tv_tzn_1.setText("");
    }

    public void showRightAZN() {
        tv_tzn_1.setText(Html.fromHtml("&nbsp;&#x20bc;"));
    }

    public void hideLeftAZN() {
        tv_tzn_2.setText("");
    }

    public void showLeftAZN() {
        tv_tzn_2.setText(Html.fromHtml("&nbsp;&#x20bc;"));
    }

    public View getSeperator() {
        return seperator;
    }
}
