package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class ViewItemTariffsKlassAttribute extends FrameLayout {

    private RelativeLayout main;

    private BakcellTextViewNormal attribute_name, attribute_value;
    private ImageView icon;
    private View view;

    public ViewItemTariffsKlassAttribute(@NonNull Context context) {

        super(context);
        main = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.item_view_tariffs_klass_attribute, null);

        attribute_name = (BakcellTextViewNormal) main.findViewById(R.id.attribute_name);
        attribute_value = (BakcellTextViewNormal) main.findViewById(R.id.attribute_value);
        icon = (ImageView) main.findViewById(R.id.ic_img_call);
        view = main.findViewById(R.id.seperator);
        attribute_name.setSelected(true);
        attribute_value.setSelected(true);
        this.addView(main);

    }

    public BakcellTextViewNormal getAttribute_name() {
        return attribute_name;
    }

    public BakcellTextViewNormal getAttribute_value() {
        return attribute_value;
    }

    public ImageView getIcon() {
        return icon;
    }

    public View getView() {
        return view;
    }

    public void setValueTextColor(@ColorRes int color) {
        if (getResources() != null) {
            attribute_value.setTextColor(getResources().getColor(color));
        }
    }
}
