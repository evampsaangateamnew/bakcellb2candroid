package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Noman on 12/19/2017.
 */

public class StoreLocatorNearestStoreItem extends FrameLayout {
    LinearLayout main, ll_shortest_store;
    BakcellTextViewNormal nearStoreName, nearMilesTxt;

    public BakcellTextViewNormal getNearStoreName() {
        return nearStoreName;
    }

    public BakcellTextViewNormal getNearMilesTxt() {
        return nearMilesTxt;
    }

    public LinearLayout getLl_shortest_store() {
        return ll_shortest_store;
    }

    public StoreLocatorNearestStoreItem(@NonNull Context context) {
        super(context);
        main = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.store_locator_nearest_item, null);
        ll_shortest_store = main.findViewById(R.id.ll_shortest_store);
        nearStoreName = main.findViewById(R.id.nearStoreName);
        nearMilesTxt = main.findViewById(R.id.nearMilesTxt);
        nearStoreName.setSelected(true);
        this.addView(main);
    }
}
