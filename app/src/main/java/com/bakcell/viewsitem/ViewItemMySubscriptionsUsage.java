package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Noman on 9/25/2017.
 */

public class ViewItemMySubscriptionsUsage extends FrameLayout {

    RelativeLayout main, renew_progress_layout, remaining_progress_layout;

    private BakcellTextViewNormal remaining_title, remaining_time_title,
            renew_title, renew_time_title, renew_date, activation_date;

    private ProgressBar remaining_progress, renew_progress, remaining_progress_min, renew_progress_max;
    private ImageView ic_usage_icon;

    LinearLayout usageLayoutBG;
    View lineBelow;

    public ViewItemMySubscriptionsUsage(@NonNull Context context) {
        super(context);
        main = (RelativeLayout) LayoutInflater.from(context)
                .inflate(R.layout.my_subscriptions_item_view_usage, null);

        remaining_title = (BakcellTextViewNormal) main.findViewById(R.id.remaining_title);
        remaining_time_title = (BakcellTextViewNormal) main.findViewById(R.id.remaining_time_title);
        renew_title = (BakcellTextViewNormal) main.findViewById(R.id.renew_title);
        renew_time_title = (BakcellTextViewNormal) main.findViewById(R.id.renew_time_title);
        activation_date = (BakcellTextViewNormal) main.findViewById(R.id.activation_date);
        renew_date = (BakcellTextViewNormal) main.findViewById(R.id.renew_date);
        remaining_progress = (ProgressBar) main.findViewById(R.id.remaining_progress);
        renew_progress = (ProgressBar) main.findViewById(R.id.renew_progress);
        remaining_progress_min = (ProgressBar) main.findViewById(R.id.remaining_progress_min);
        renew_progress_max = (ProgressBar) main.findViewById(R.id.renew_progress_max);
        renew_progress_layout = (RelativeLayout) main.findViewById(R.id.renew_progress_layout);
        remaining_progress_layout = (RelativeLayout) main.findViewById(R.id.remaining_progress_layout);
        ic_usage_icon = (ImageView) main.findViewById(R.id.ic_usage_icon);
        usageLayoutBG = (LinearLayout) main.findViewById(R.id.usageLayoutBG);
        lineBelow = (View) main.findViewById(R.id.lineBelow);

        renew_date.setSelected(true);
        activation_date.setSelected(true);
        remaining_title.setSelected(true);

        this.addView(main);

    }

    public View getLineBelow() {
        return lineBelow;
    }

    public ProgressBar getRemaining_progress_min() {
        return remaining_progress_min;
    }

    public ProgressBar getRenew_progress_max() {
        return renew_progress_max;
    }

    public LinearLayout getUsageLayoutBG() {
        return usageLayoutBG;
    }

    public ImageView getIc_usage_icon() {
        return ic_usage_icon;
    }

    public RelativeLayout getRenew_progress_layout() {
        return renew_progress_layout;
    }

    public RelativeLayout getRemaining_progress_layout() {
        return remaining_progress_layout;
    }

    public BakcellTextViewNormal getRemaining_title() {
        return remaining_title;
    }

    public BakcellTextViewNormal getRemaining_time_title() {
        return remaining_time_title;
    }

    public BakcellTextViewNormal getRenew_title() {
        return renew_title;
    }

    public BakcellTextViewNormal getRenew_time_title() {
        return renew_time_title;
    }

    public BakcellTextViewNormal getRenew_date() {
        return renew_date;
    }

    public BakcellTextViewNormal getActivation_date() {
        return activation_date;
    }

    public ProgressBar getRemaining_progress() {
        return remaining_progress;
    }

    public ProgressBar getRenew_progress() {
        return renew_progress;
    }
}
