package com.bakcell.viewsitem;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bakcell.R;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestEnableDisableCoreService;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Zeeshan Shabbir on 10/10/2017.
 */

public class ViewItemCoreServices extends FrameLayout implements View.OnClickListener {
    protected LinearLayout main;
    public RelativeLayout rl_switch_layout, nextPayProgressLayout;
    private BakcellTextViewNormal tv_label, renewalTypeText;
    private BakcellTextViewNormal tv_desc;
    private SwitchCompat switch_category;
    private View seperator;
    private String offeringId;
    private Context context;
    private TextView nextPayTitle, nextPayDaysTitle, nextDateTxt, creditValueTxt, tvTzn;
    private ProgressBar nextProgress;

    private AlertDialog alertDialog;
    BakcellButtonNormal logoutBtn;
    BakcellButtonNormal cancelBtn;


    @SuppressLint("ClickableViewAccessibility")
    public ViewItemCoreServices(@NonNull Context context, String offeringId) {
        super(context);
        this.context = context;
        this.offeringId = offeringId;
        main = (LinearLayout) LayoutInflater.from(context)
                .inflate(R.layout.item_view_core_services, null);
        rl_switch_layout = (RelativeLayout) main.findViewById(R.id.rl_switch_layout);
        tv_desc = (BakcellTextViewNormal) main.findViewById(R.id.tv_desc);
        tv_label = (BakcellTextViewNormal) main.findViewById(R.id.tv_label);
        switch_category = (SwitchCompat) main.findViewById(R.id.switch_category);

        nextPayProgressLayout = (RelativeLayout) main.findViewById(R.id.next_pay_progress_layout);
        nextPayTitle = (BakcellTextViewNormal) main.findViewById(R.id.next_pay_title);
        nextPayDaysTitle = (BakcellTextViewNormal) main.findViewById(R.id.next_pay_days_title);
        nextDateTxt = (BakcellTextViewNormal) main.findViewById(R.id.next_date_txt);
        creditValueTxt = (BakcellTextViewNormal) main.findViewById(R.id.creditValueTxt);
        tvTzn = (BakcellTextViewNormal) main.findViewById(R.id.tv_tzn);
        renewalTypeText = (BakcellTextViewNormal) main.findViewById(R.id.renewalTypeText);
        nextProgress = (ProgressBar) main.findViewById(R.id.next_progress);


        seperator = main.findViewById(R.id.seperator);
        this.addView(main);

        switch_category.setOnTouchListener((v, event) -> event.getActionMasked() == MotionEvent.ACTION_MOVE);

        switch_category.setOnClickListener(v -> {
            if (switch_category.isChecked()) {
                handleCheckChange(false, "1");//deactive service
            } else {
                handleCheckChange(true, "3");//Active service
            }
        });
    }

    public BakcellTextViewNormal getRenewalTypeText() {
        return renewalTypeText;
    }

    public RelativeLayout getNextPayProgressLayout() {
        return nextPayProgressLayout;
    }

    public TextView getCreditValueTxt() {
        return creditValueTxt;
    }

    public TextView getTvTzn() {
        return tvTzn;
    }

    public TextView getNextPayTitle() {
        return nextPayTitle;
    }

    public TextView getNextPayDaysTitle() {
        return nextPayDaysTitle;
    }

    public TextView getNextDateTxt() {
        return nextDateTxt;
    }

    public ProgressBar getNextProgress() {
        return nextProgress;
    }

    public RelativeLayout getRl_switch_layout() {
        return rl_switch_layout;
    }

    public BakcellTextViewNormal getTv_label() {
        return tv_label;
    }

    public BakcellTextViewNormal getTv_desc() {
        return tv_desc;
    }

    public SwitchCompat getSwitchCategory() {
        return switch_category;
    }

    public View getSeperator() {
        return seperator;
    }

    public void handleCheckChange(final boolean isChecked, String actionType) {
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            final String finalActionType = actionType;
            RequestEnableDisableCoreService.newInstance(context,
                    ServiceIDs.REQUEST_ENABLE_DISABLE_CORE_SERVICE).execute(userModel, actionType,
                    offeringId, "", new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
//                            if (response.callStatus()) {
//                                switch_category.setChecked(isChecked);
//                            }
                            if (context != null && response != null && Tools.hasValue(response.getDescription())) {
                                BakcellPopUpDialog.showMessageDialog(context,
                                        context.getString(R.string.mesg_successful_title), response.getDescription());
                            }

                            if (isChecked) {
                                BakcellLogger.logE("manageNumber", "isChecked:::" + isChecked
                                        , "ViewItemCoreServices", "handleCheckChange");
                                RootValues.getInstance().getCoreServiceStatusChangeListener().updateStatus(offeringId, false);
                            } else {
                                BakcellLogger.logE("manageNumber", "isChecked:::" + isChecked
                                        , "ViewItemCoreServices", "handleCheckChange");
                                RootValues.getInstance().getCoreServiceStatusChangeListener().updateStatus(offeringId, true);
                            }

//                            if (context != null) {
//                                BakcellPopUpDialog.showMessageDialog(context,
//                                        context.getString(R.string.mesg_successful_title), response.getDescription());
//                            }

                            if (finalActionType.contentEquals("3")) {
                                AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                                        AppEventLogValues.ServiceEvents.CORE_SERVICES_DISABLE_SUCCESS,
                                        AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN);
                            } else {
                                AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                                        AppEventLogValues.ServiceEvents.CORE_SERVICES_ENABLE_SUCCESS,
                                        AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN);
                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {

                            Log.e("value_", response.callStatus() + "" + response);
                            if (isChecked) {
                                BakcellLogger.logE("manageNumber", "isChecked:::" + isChecked
                                        , "ViewItemCoreServices", "handleCheckChange");
                                RootValues.getInstance().getCoreServiceStatusChangeListener().updateStatus(offeringId, true);
                            } else {
                                BakcellLogger.logE("manageNumber", "isChecked:::" + isChecked
                                        , "ViewItemCoreServices", "handleCheckChange");
                                RootValues.getInstance().getCoreServiceStatusChangeListener().updateStatus(offeringId, false);
                            }

                            if (context != null && response != null) {
                                BakcellPopUpDialog.showMessageDialog(context,
                                        getLocalizedErrorTitle(), response.getDescription());
                            }
                            if (finalActionType.contentEquals("3")) {
                                AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                                        AppEventLogValues.ServiceEvents.CORE_SERVICES_DISABLE_FAILURE,
                                        AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN);
                            } else {
                                AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN,
                                        AppEventLogValues.ServiceEvents.CORE_SERVICES_ENABLE_FAILURE,
                                        AppEventLogValues.ServiceEvents.SERVICE_CORE_SERVICES_SCREEN);
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (context != null) {
                                BakcellPopUpDialog.ApiFailureMessage(context);
                            }
                            if (isChecked) {
                                BakcellLogger.logE("manageNumber", "isChecked:::" + isChecked
                                        , "ViewItemCoreServices", "handleCheckChange");
                                RootValues.getInstance().getCoreServiceStatusChangeListener().updateStatus(offeringId, false);
                            } else {
                                BakcellLogger.logE("manageNumber", "isChecked:::" + isChecked
                                        , "ViewItemCoreServices", "handleCheckChange");
                                RootValues.getInstance().getCoreServiceStatusChangeListener().updateStatus(offeringId, true);
                            }
                        }
                    });
        }

    }


    private void showOfferAlert(final boolean isChecked) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.diaog_logout_alert, null);
        dialogBuilder.setView(dialogView);

        alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        cancelBtn = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_cancel);
        logoutBtn = (BakcellButtonNormal) dialogView.findViewById(R.id.btn_logout);
        BakcellTextViewNormal tvMessage = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_pop_up_msg);
        BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView.findViewById(R.id.tv_dialog_title);
        if (isChecked) {
            tvTitle.setText(context.getString(R.string.dialog_confirmation_lbl));
            logoutBtn.setText(context.getString(R.string.suspend));
            tvMessage.setText(context.getString(R.string.msg_are_you_sure_you_want_to_suspend));
        } else {
            tvTitle.setText(context.getString(R.string.dialog_confirmation_lbl));
            logoutBtn.setText(context.getString(R.string.active_lbl));
            tvMessage.setText(context.getString(R.string.msg_are_you_sure_you_want_to_active));
        }
        logoutBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_logout:
//                alertDialog.dismiss();
//                if (switch_category.isChecked()) {
//                    handleCheckChange(false);//deactive service
//                } else {
//                    handleCheckChange(true);//Active service
//                }
                break;
            case R.id.btn_cancel:
                alertDialog.dismiss();
                break;
        }
    }

    private String getLocalizedErrorTitle() {
        String localizedErrorTitle;
        if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            localizedErrorTitle = "Error";
        } else if (AppClass.getCurrentLanguageKey(context).equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            localizedErrorTitle = "Xəta baş verdi";
        } else {
            localizedErrorTitle = "Ошибка";
        }

        return localizedErrorTitle;
    }

    public void enableCategorySwitch() {
        switch_category.setClickable(true);
        switch_category.setEnabled(true);
    }

    public void disableCategorySwitch() {
        switch_category.setClickable(false);
        switch_category.setEnabled(false);
    }
}
