package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Zeeshan Shabbir on 9/6/2017.
 */

public class ViewItemTariffsDetailDestination extends FrameLayout {
    private RelativeLayout main;
    private BakcellTextViewNormal tv_attribute_lbl;
    private BakcellTextViewNormal tv_attribute_value;
    private BakcellTextViewNormal tv_attribute_value_left;

    public ViewItemTariffsDetailDestination(@NonNull Context context) {
        super(context);
        main = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.item_view_tariffs_details_attribute, null);
        tv_attribute_lbl = (BakcellTextViewNormal) main.findViewById(R.id.tv_attribute_lbl);
        tv_attribute_value = (BakcellTextViewNormal) main.findViewById(R.id.tv_attribute_value);
        tv_attribute_value_left = (BakcellTextViewNormal) main.findViewById(R.id.tv_attribute_value_left);
        this.addView(main);
    }

    public BakcellTextViewNormal getTv_attribute_lbl() {
        return tv_attribute_lbl;
    }

    public BakcellTextViewNormal getTv_attribute_value() {
        return tv_attribute_value;
    }

    public BakcellTextViewNormal getTv_attribute_value_left() {
        return tv_attribute_value_left;
    }
}
