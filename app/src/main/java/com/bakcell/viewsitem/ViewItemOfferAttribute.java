package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class ViewItemOfferAttribute extends FrameLayout {

    private LinearLayout main;

    private ImageView icon_clock;
    private BakcellTextViewNormal attributeTitle, attributeValue, priceIcon, attribute_description;
    private View lineTopLine, lineTextAttribute;

    private LinearLayout onnetOffnetMainView;
    private RelativeLayout onnetView, offnetView;
    private BakcellTextViewNormal onnetLabel, onnetValue, offnetLabel, offnetValue;

    public ViewItemOfferAttribute(@NonNull Context context) {

        super(context);
        main = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.offer_attribute_layout_item, null);

        icon_clock = (ImageView) main.findViewById(R.id.icon_clock);
        attributeTitle = (BakcellTextViewNormal) main.findViewById(R.id.attributeTitle);
        attributeValue = (BakcellTextViewNormal) main.findViewById(R.id.attributeValue);
        priceIcon = (BakcellTextViewNormal) main.findViewById(R.id.priceIcon);
        attribute_description = (BakcellTextViewNormal) main.findViewById(R.id.attribute_description);
        lineTopLine = (View) main.findViewById(R.id.lineTopLine);
        lineTextAttribute = (View) main.findViewById(R.id.lineTextAttribute);

        onnetOffnetMainView = (LinearLayout) main.findViewById(R.id.onnetOffnetMainView);
        onnetView = (RelativeLayout) main.findViewById(R.id.onnetView);
        offnetView = (RelativeLayout) main.findViewById(R.id.offnetView);

        onnetLabel = (BakcellTextViewNormal) main.findViewById(R.id.onnetLabel);
        onnetValue = (BakcellTextViewNormal) main.findViewById(R.id.onnetValue);
        offnetLabel = (BakcellTextViewNormal) main.findViewById(R.id.offnetLabel);
        offnetValue = (BakcellTextViewNormal) main.findViewById(R.id.offnetValue);

        this.addView(main);

    }

    public void hideRightAZN() {
        this.priceIcon.setText("");
    }

    public void showRightAZN() {
        this.priceIcon.setText(Html.fromHtml("&nbsp; &#x20bc;"));
    }

    public ImageView getIcon_clock() {
        return icon_clock;
    }

    public BakcellTextViewNormal getAttributeTitle() {
        return attributeTitle;
    }

    public BakcellTextViewNormal getAttributeValue() {
        return attributeValue;
    }

    public BakcellTextViewNormal getPriceIcon() {
        return priceIcon;
    }

    public BakcellTextViewNormal getAttribute_description() {
        return attribute_description;
    }

    public View getLineTopLine() {
        return lineTopLine;
    }

    public View getLineTextAttribute() {
        return lineTextAttribute;
    }

    public LinearLayout getOnnetOffnetMainView() {
        return onnetOffnetMainView;
    }

    public RelativeLayout getOnnetView() {
        return onnetView;
    }

    public RelativeLayout getOffnetView() {
        return offnetView;
    }

    public BakcellTextViewNormal getOnnetLabel() {
        return onnetLabel;
    }

    public BakcellTextViewNormal getOnnetValue() {
        return onnetValue;
    }

    public BakcellTextViewNormal getOffnetLabel() {
        return offnetLabel;
    }

    public BakcellTextViewNormal getOffnetValue() {
        return offnetValue;
    }
}
