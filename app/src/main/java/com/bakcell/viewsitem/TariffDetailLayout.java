package com.bakcell.viewsitem;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bakcell.widgets.BakcellTextViewNormal;

public class TariffDetailLayout {

    // Price
    LinearLayout tariff_detail_parice_layout;

    // roundingSection
    RelativeLayout roundingSection, roundingTitleLayout;
    ImageView roundingTitleIcon;
    BakcellTextViewNormal roundingTitleText, roundingTitleValue,detailRoundingDescription;
    LinearLayout offerDetailroundingAttributeLayout;
    View rounding_below_line;

    // TextWithTitleSection
    RelativeLayout textWithTitleSection;
    BakcellTextViewNormal text_title, decription_text;
    View textWithTitle_below_line;

    //textWithOutTitleSection
    RelativeLayout textWithOutTitleSection;
    BakcellTextViewNormal textWithOutTitle_text;
    View textWithOutTitle_below_line;

    // textWithPointsSections
    RelativeLayout textWithPointsSections;
    LinearLayout textWithPointsLayout;
    View textWithPoints_below_line;

    //titleSubTitleValueDescSection
    RelativeLayout titleSubTitleValueDescSection, titleSubTitleValueDescTitleLayout;
    BakcellTextViewNormal titleSubTitleValueDescTitle;
    LinearLayout titleSubTitleValueDescAttributeLayout;
    View textSubTitleDesc_below_line;

    //dateSection
    RelativeLayout dateSection, dateLayout;
    BakcellTextViewNormal dateDescription, dateFromLabel, dateFromValue, dateToLabel, dateToValue;
    View date_below_line;

    //timeSection
    RelativeLayout timeSection, timeLayout;
    BakcellTextViewNormal timeDescription, timefromLabel, timefromValue, timetoLabel, timetoValue;
    View time_below_line;

    //roamingSection
    RelativeLayout roamingSection;
    BakcellTextViewNormal roamingDescriptionAbove, roamingDescriptionBelow;
    LinearLayout roamingCountryLayout;
    View roaming_below_line;

    // freeResourceValiditySection
    RelativeLayout freeResourceValiditySection, freeResourceTitleLayout, freeResourceOnnetLayout;
    BakcellTextViewNormal freeResourceTitle, freeResourceValue, onnetTitle, onnetValue, freeResourceDescription;


    public LinearLayout getTariff_detail_parice_layout() {
        return tariff_detail_parice_layout;
    }

    public void setTariff_detail_parice_layout(LinearLayout tariff_detail_parice_layout) {
        this.tariff_detail_parice_layout = tariff_detail_parice_layout;
    }

    public RelativeLayout getRoundingSection() {
        return roundingSection;
    }

    public void setRoundingSection(RelativeLayout roundingSection) {
        this.roundingSection = roundingSection;
    }

    public RelativeLayout getRoundingTitleLayout() {
        return roundingTitleLayout;
    }

    public void setRoundingTitleLayout(RelativeLayout roundingTitleLayout) {
        this.roundingTitleLayout = roundingTitleLayout;
    }

    public ImageView getRoundingTitleIcon() {
        return roundingTitleIcon;
    }

    public void setRoundingTitleIcon(ImageView roundingTitleIcon) {
        this.roundingTitleIcon = roundingTitleIcon;
    }

    public BakcellTextViewNormal getRoundingTitleText() {
        return roundingTitleText;
    }

    public void setRoundingTitleText(BakcellTextViewNormal roundingTitleText) {
        this.roundingTitleText = roundingTitleText;
    }

    public BakcellTextViewNormal getRoundingTitleValue() {
        return roundingTitleValue;
    }

    public void setRoundingTitleValue(BakcellTextViewNormal roundingTitleValue) {
        this.roundingTitleValue = roundingTitleValue;
    }

    public BakcellTextViewNormal getDetailRoundingDescription() {
        return detailRoundingDescription;
    }

    public void setDetailRoundingDescription(BakcellTextViewNormal detailRoundingDescription) {
        this.detailRoundingDescription = detailRoundingDescription;
    }

    public LinearLayout getOfferDetailroundingAttributeLayout() {
        return offerDetailroundingAttributeLayout;
    }

    public void setOfferDetailroundingAttributeLayout(LinearLayout offerDetailroundingAttributeLayout) {
        this.offerDetailroundingAttributeLayout = offerDetailroundingAttributeLayout;
    }

    public View getRounding_below_line() {
        return rounding_below_line;
    }

    public void setRounding_below_line(View rounding_below_line) {
        this.rounding_below_line = rounding_below_line;
    }

    public RelativeLayout getTextWithTitleSection() {
        return textWithTitleSection;
    }

    public void setTextWithTitleSection(RelativeLayout textWithTitleSection) {
        this.textWithTitleSection = textWithTitleSection;
    }

    public BakcellTextViewNormal getText_title() {
        return text_title;
    }

    public void setText_title(BakcellTextViewNormal text_title) {
        this.text_title = text_title;
    }

    public BakcellTextViewNormal getDecription_text() {
        return decription_text;
    }

    public void setDecription_text(BakcellTextViewNormal decription_text) {
        this.decription_text = decription_text;
    }

    public View getTextWithTitle_below_line() {
        return textWithTitle_below_line;
    }

    public void setTextWithTitle_below_line(View textWithTitle_below_line) {
        this.textWithTitle_below_line = textWithTitle_below_line;
    }

    public RelativeLayout getTextWithOutTitleSection() {
        return textWithOutTitleSection;
    }

    public void setTextWithOutTitleSection(RelativeLayout textWithOutTitleSection) {
        this.textWithOutTitleSection = textWithOutTitleSection;
    }

    public BakcellTextViewNormal getTextWithOutTitle_text() {
        return textWithOutTitle_text;
    }

    public void setTextWithOutTitle_text(BakcellTextViewNormal textWithOutTitle_text) {
        this.textWithOutTitle_text = textWithOutTitle_text;
    }

    public View getTextWithOutTitle_below_line() {
        return textWithOutTitle_below_line;
    }

    public void setTextWithOutTitle_below_line(View textWithOutTitle_below_line) {
        this.textWithOutTitle_below_line = textWithOutTitle_below_line;
    }

    public RelativeLayout getTextWithPointsSections() {
        return textWithPointsSections;
    }

    public void setTextWithPointsSections(RelativeLayout textWithPointsSections) {
        this.textWithPointsSections = textWithPointsSections;
    }

    public LinearLayout getTextWithPointsLayout() {
        return textWithPointsLayout;
    }

    public void setTextWithPointsLayout(LinearLayout textWithPointsLayout) {
        this.textWithPointsLayout = textWithPointsLayout;
    }

    public View getTextWithPoints_below_line() {
        return textWithPoints_below_line;
    }

    public void setTextWithPoints_below_line(View textWithPoints_below_line) {
        this.textWithPoints_below_line = textWithPoints_below_line;
    }

    public RelativeLayout getTitleSubTitleValueDescSection() {
        return titleSubTitleValueDescSection;
    }

    public void setTitleSubTitleValueDescSection(RelativeLayout titleSubTitleValueDescSection) {
        this.titleSubTitleValueDescSection = titleSubTitleValueDescSection;
    }

    public RelativeLayout getTitleSubTitleValueDescTitleLayout() {
        return titleSubTitleValueDescTitleLayout;
    }

    public void setTitleSubTitleValueDescTitleLayout(RelativeLayout titleSubTitleValueDescTitleLayout) {
        this.titleSubTitleValueDescTitleLayout = titleSubTitleValueDescTitleLayout;
    }

    public BakcellTextViewNormal getTitleSubTitleValueDescTitle() {
        return titleSubTitleValueDescTitle;
    }

    public void setTitleSubTitleValueDescTitle(BakcellTextViewNormal titleSubTitleValueDescTitle) {
        this.titleSubTitleValueDescTitle = titleSubTitleValueDescTitle;
    }

    public LinearLayout getTitleSubTitleValueDescAttributeLayout() {
        return titleSubTitleValueDescAttributeLayout;
    }

    public void setTitleSubTitleValueDescAttributeLayout(LinearLayout titleSubTitleValueDescAttributeLayout) {
        this.titleSubTitleValueDescAttributeLayout = titleSubTitleValueDescAttributeLayout;
    }

    public View getTextSubTitleDesc_below_line() {
        return textSubTitleDesc_below_line;
    }

    public void setTextSubTitleDesc_below_line(View textSubTitleDesc_below_line) {
        this.textSubTitleDesc_below_line = textSubTitleDesc_below_line;
    }

    public RelativeLayout getDateSection() {
        return dateSection;
    }

    public void setDateSection(RelativeLayout dateSection) {
        this.dateSection = dateSection;
    }

    public RelativeLayout getDateLayout() {
        return dateLayout;
    }

    public void setDateLayout(RelativeLayout dateLayout) {
        this.dateLayout = dateLayout;
    }

    public BakcellTextViewNormal getDateDescription() {
        return dateDescription;
    }

    public void setDateDescription(BakcellTextViewNormal dateDescription) {
        this.dateDescription = dateDescription;
    }

    public BakcellTextViewNormal getDateFromLabel() {
        return dateFromLabel;
    }

    public void setDateFromLabel(BakcellTextViewNormal dateFromLabel) {
        this.dateFromLabel = dateFromLabel;
    }

    public BakcellTextViewNormal getDateFromValue() {
        return dateFromValue;
    }

    public void setDateFromValue(BakcellTextViewNormal dateFromValue) {
        this.dateFromValue = dateFromValue;
    }

    public BakcellTextViewNormal getDateToLabel() {
        return dateToLabel;
    }

    public void setDateToLabel(BakcellTextViewNormal dateToLabel) {
        this.dateToLabel = dateToLabel;
    }

    public BakcellTextViewNormal getDateToValue() {
        return dateToValue;
    }

    public void setDateToValue(BakcellTextViewNormal dateToValue) {
        this.dateToValue = dateToValue;
    }

    public View getDate_below_line() {
        return date_below_line;
    }

    public void setDate_below_line(View date_below_line) {
        this.date_below_line = date_below_line;
    }

    public RelativeLayout getTimeSection() {
        return timeSection;
    }

    public void setTimeSection(RelativeLayout timeSection) {
        this.timeSection = timeSection;
    }

    public RelativeLayout getTimeLayout() {
        return timeLayout;
    }

    public void setTimeLayout(RelativeLayout timeLayout) {
        this.timeLayout = timeLayout;
    }

    public BakcellTextViewNormal getTimeDescription() {
        return timeDescription;
    }

    public void setTimeDescription(BakcellTextViewNormal timeDescription) {
        this.timeDescription = timeDescription;
    }

    public BakcellTextViewNormal getTimefromLabel() {
        return timefromLabel;
    }

    public void setTimefromLabel(BakcellTextViewNormal timefromLabel) {
        this.timefromLabel = timefromLabel;
    }

    public BakcellTextViewNormal getTimefromValue() {
        return timefromValue;
    }

    public void setTimefromValue(BakcellTextViewNormal timefromValue) {
        this.timefromValue = timefromValue;
    }

    public BakcellTextViewNormal getTimetoLabel() {
        return timetoLabel;
    }

    public void setTimetoLabel(BakcellTextViewNormal timetoLabel) {
        this.timetoLabel = timetoLabel;
    }

    public BakcellTextViewNormal getTimetoValue() {
        return timetoValue;
    }

    public void setTimetoValue(BakcellTextViewNormal timetoValue) {
        this.timetoValue = timetoValue;
    }

    public View getTime_below_line() {
        return time_below_line;
    }

    public void setTime_below_line(View time_below_line) {
        this.time_below_line = time_below_line;
    }

    public RelativeLayout getRoamingSection() {
        return roamingSection;
    }

    public void setRoamingSection(RelativeLayout roamingSection) {
        this.roamingSection = roamingSection;
    }

    public BakcellTextViewNormal getRoamingDescriptionAbove() {
        return roamingDescriptionAbove;
    }

    public void setRoamingDescriptionAbove(BakcellTextViewNormal roamingDescriptionAbove) {
        this.roamingDescriptionAbove = roamingDescriptionAbove;
    }

    public BakcellTextViewNormal getRoamingDescriptionBelow() {
        return roamingDescriptionBelow;
    }

    public void setRoamingDescriptionBelow(BakcellTextViewNormal roamingDescriptionBelow) {
        this.roamingDescriptionBelow = roamingDescriptionBelow;
    }

    public LinearLayout getRoamingCountryLayout() {
        return roamingCountryLayout;
    }

    public void setRoamingCountryLayout(LinearLayout roamingCountryLayout) {
        this.roamingCountryLayout = roamingCountryLayout;
    }

    public View getRoaming_below_line() {
        return roaming_below_line;
    }

    public void setRoaming_below_line(View roaming_below_line) {
        this.roaming_below_line = roaming_below_line;
    }

    public RelativeLayout getFreeResourceValiditySection() {
        return freeResourceValiditySection;
    }

    public void setFreeResourceValiditySection(RelativeLayout freeResourceValiditySection) {
        this.freeResourceValiditySection = freeResourceValiditySection;
    }

    public RelativeLayout getFreeResourceTitleLayout() {
        return freeResourceTitleLayout;
    }

    public void setFreeResourceTitleLayout(RelativeLayout freeResourceTitleLayout) {
        this.freeResourceTitleLayout = freeResourceTitleLayout;
    }

    public RelativeLayout getFreeResourceOnnetLayout() {
        return freeResourceOnnetLayout;
    }

    public void setFreeResourceOnnetLayout(RelativeLayout freeResourceOnnetLayout) {
        this.freeResourceOnnetLayout = freeResourceOnnetLayout;
    }

    public BakcellTextViewNormal getFreeResourceTitle() {
        return freeResourceTitle;
    }

    public void setFreeResourceTitle(BakcellTextViewNormal freeResourceTitle) {
        this.freeResourceTitle = freeResourceTitle;
    }

    public BakcellTextViewNormal getFreeResourceValue() {
        return freeResourceValue;
    }

    public void setFreeResourceValue(BakcellTextViewNormal freeResourceValue) {
        this.freeResourceValue = freeResourceValue;
    }

    public BakcellTextViewNormal getOnnetTitle() {
        return onnetTitle;
    }

    public void setOnnetTitle(BakcellTextViewNormal onnetTitle) {
        this.onnetTitle = onnetTitle;
    }

    public BakcellTextViewNormal getOnnetValue() {
        return onnetValue;
    }

    public void setOnnetValue(BakcellTextViewNormal onnetValue) {
        this.onnetValue = onnetValue;
    }

    public BakcellTextViewNormal getFreeResourceDescription() {
        return freeResourceDescription;
    }

    public void setFreeResourceDescription(BakcellTextViewNormal freeResourceDescription) {
        this.freeResourceDescription = freeResourceDescription;
    }
}
