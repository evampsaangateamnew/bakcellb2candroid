package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Noman on 29-May-17.
 */

public class ViewItemTariffsDetailPrice extends FrameLayout {

    LinearLayout main;

    RelativeLayout priceSection, priceTitleLayout;
    ImageView priceTitleIcon;
    BakcellTextViewNormal detailPriceTitle, detailPriceValue, detailPriceDescription;
    LinearLayout offerDetailAttributeLayout;
    View price_below_line;


    public ViewItemTariffsDetailPrice(@NonNull Context context) {

        super(context);
        main = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.item_view_tariff_detail_layout_price, null);

        priceSection = (RelativeLayout) main.findViewById(R.id.priceSection);
        priceTitleLayout = (RelativeLayout) main.findViewById(R.id.priceTitleLayout);
        priceTitleIcon = (ImageView) main.findViewById(R.id.priceTitleIcon);
        detailPriceTitle = (BakcellTextViewNormal) main.findViewById(R.id.detailPriceTitle);
        detailPriceValue = (BakcellTextViewNormal) main.findViewById(R.id.detailPriceValue);
        detailPriceDescription = (BakcellTextViewNormal) main.findViewById(R.id.detailPriceDescription);
        offerDetailAttributeLayout = (LinearLayout) main.findViewById(R.id.offerDetailAttributeLayout);
        price_below_line = (View) main.findViewById(R.id.price_below_line);

        this.addView(main);
    }

    public RelativeLayout getPriceSection() {
        return priceSection;
    }

    public RelativeLayout getPriceTitleLayout() {
        return priceTitleLayout;
    }

    public ImageView getPriceTitleIcon() {
        return priceTitleIcon;
    }

    public BakcellTextViewNormal getDetailPriceTitle() {
        return detailPriceTitle;
    }

    public BakcellTextViewNormal getDetailPriceValue() {
        return detailPriceValue;
    }

    public BakcellTextViewNormal getDetailPriceDescription() {
        return detailPriceDescription;
    }

    public LinearLayout getOfferDetailAttributeLayout() {
        return offerDetailAttributeLayout;
    }

    public View getPrice_below_line() {
        return price_below_line;
    }
}
