package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class ViewItemOfferDetailRoamingCountries extends FrameLayout {

    private RelativeLayout main;

    private BakcellTextViewNormal countryName;
    private ImageView countryFlag;
    private LinearLayout operatorLayout;

    public ViewItemOfferDetailRoamingCountries(@NonNull Context context) {

        super(context);
        main = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.offer_detail_roaming_countries_item, null);

        countryName = (BakcellTextViewNormal) main.findViewById(R.id.countryName);
        countryFlag = (ImageView) main.findViewById(R.id.countryFlag);
        operatorLayout = (LinearLayout) main.findViewById(R.id.operatorLayout);

        this.addView(main);

    }

    public BakcellTextViewNormal getCountryName() {
        return countryName;
    }

    public ImageView getCountryFlag() {
        return countryFlag;
    }

    public LinearLayout getOperatorLayout() {
        return operatorLayout;
    }
}
