package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Zeeshan Shabbir on 7/31/2017.
 */

public class StoreLocatorTimingItem extends FrameLayout {
    LinearLayout main;
    private BakcellTextViewNormal tvday, tvTimings;


    public StoreLocatorTimingItem(@NonNull Context context) {
        super(context);
        main = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.store_locator_timings, null);
        tvday = (BakcellTextViewNormal) main.findViewById(R.id.tv_day);
        tvTimings = (BakcellTextViewNormal) main.findViewById(R.id.tv_timing);
        this.addView(main);

    }


    public BakcellTextViewNormal getTvday() {
        return tvday;
    }

    public BakcellTextViewNormal getTvTimings() {
        return tvTimings;
    }
}
