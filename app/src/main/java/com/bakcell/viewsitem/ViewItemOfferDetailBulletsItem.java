package com.bakcell.viewsitem;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.bakcell.R;
import com.bakcell.widgets.BakcellTextViewNormal;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class ViewItemOfferDetailBulletsItem extends FrameLayout {

    private RelativeLayout main;

    private BakcellTextViewNormal bulletText;

    public ViewItemOfferDetailBulletsItem(@NonNull Context context) {

        super(context);
        main = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.offer_detail_bullets_item, null);

        bulletText = (BakcellTextViewNormal) main.findViewById(R.id.bulletText);

        this.addView(main);

    }

    public BakcellTextViewNormal getBulletText() {
        return bulletText;
    }
}
