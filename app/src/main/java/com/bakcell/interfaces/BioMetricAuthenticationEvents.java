package com.bakcell.interfaces;

public interface BioMetricAuthenticationEvents {
    void onBiometricAuthenticationError(int errorCode, String errorMessage);

    void onBiometricAuthenticationSuccess();

    void onBiometricAuthenticationFailed();
}
