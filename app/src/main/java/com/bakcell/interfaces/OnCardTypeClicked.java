package com.bakcell.interfaces;

public interface OnCardTypeClicked {
    void onCardClicked(long cardKey);
}
