package com.bakcell.interfaces;

/**
 * Created by Noman on 16-Oct-17.
 */

public interface ChangeTariffListener {
    void onChangeTariffListenerSuccess(String message);

    void onChangeTariffListenerFailure(String message);
}
