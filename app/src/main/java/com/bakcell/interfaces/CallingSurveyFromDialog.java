package com.bakcell.interfaces;

import androidx.annotation.Nullable;

import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.models.inappsurvey.UploadSurvey;

public interface CallingSurveyFromDialog {
    void onCallingSurveyFromDialog(UploadSurvey uploadSurvey,
                                   String onTransactionComplete,
                                   InAppFeedbackDialog inAppFeedbackDialog,
                                   @Nullable UpdateListOnSuccessfullySurvey updateListOnSuccessfullySurvey);
}
