package com.bakcell.interfaces;

public interface DisclaimerDialogListener {

    void onDisclaimerDialogOkListner(boolean checked);

}
