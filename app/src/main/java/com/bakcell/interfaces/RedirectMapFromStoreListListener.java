package com.bakcell.interfaces;

import com.bakcell.models.storelocator.StoreLocator;

/**
 * Created by Noman on 28-Sep-17.
 */

public interface RedirectMapFromStoreListListener {
    void onRedirectMapFromStoreListListen(StoreLocator storeLocator);
}
