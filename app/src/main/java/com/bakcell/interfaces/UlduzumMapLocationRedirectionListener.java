package com.bakcell.interfaces;

import com.bakcell.models.ulduzum.UlduzumLocationsServiceModel;

public interface UlduzumMapLocationRedirectionListener {
    void onMapLocationSelected(String categoryId, UlduzumLocationsServiceModel ulduzumLocationsServiceModel);
}
