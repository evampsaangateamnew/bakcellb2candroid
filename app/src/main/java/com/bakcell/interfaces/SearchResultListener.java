package com.bakcell.interfaces;

/**
 * Created by Zeeshan Shabbir on 8/30/2017.
 */

public interface SearchResultListener {
    void onSearchResult(boolean isResultFount);
}