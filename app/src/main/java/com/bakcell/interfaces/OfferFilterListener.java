package com.bakcell.interfaces;

import com.bakcell.models.supplementaryoffers.OfferFilter;

import java.util.ArrayList;

/**
 * Created by Noman on 28-Aug-17.
 */

public interface OfferFilterListener {
    void onFilterCall(boolean isSearch, String filter, ArrayList<OfferFilter> filteredIds, boolean isSearchedClosed, String offerAllGroup);
}
