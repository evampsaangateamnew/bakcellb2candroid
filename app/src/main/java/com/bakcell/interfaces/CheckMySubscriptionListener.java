package com.bakcell.interfaces;

/**
 * Created by Noman on 16-Oct-17.
 */

public interface CheckMySubscriptionListener {
    void onCheckMySubscriptionListener(boolean isLevelMatchd);
}
