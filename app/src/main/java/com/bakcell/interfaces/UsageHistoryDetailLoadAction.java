package com.bakcell.interfaces;

/**
 * Created by Freeware Sys on 23-May-17.
 */

public interface UsageHistoryDetailLoadAction {

    void onUsageHistoryDetailActionCall(int actionId);

}
