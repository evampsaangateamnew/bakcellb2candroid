package com.bakcell.interfaces;

import android.graphics.Bitmap;

/**
 * Created by Zeeshan Shabbir on 10/17/2017.
 */

public interface OnProfileImageUpdate {
    void onProfileImageUpdate(Bitmap bitmap);
}
