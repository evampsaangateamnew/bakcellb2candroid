package com.bakcell.interfaces;

import com.bakcell.models.topup.GetSavedCardsItems;

public interface SaveCardsItemClickListener {
    public void onItemClick(GetSavedCardsItems savedCardsItems, int position);
}
