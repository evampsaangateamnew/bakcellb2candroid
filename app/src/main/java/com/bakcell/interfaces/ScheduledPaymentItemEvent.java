package com.bakcell.interfaces;


/**
 * @author Umair Mustafa on 14/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
public interface ScheduledPaymentItemEvent {
    public void onItemClick(int position);

}
