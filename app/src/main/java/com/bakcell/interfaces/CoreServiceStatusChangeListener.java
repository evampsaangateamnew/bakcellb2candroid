package com.bakcell.interfaces;

public interface CoreServiceStatusChangeListener {
    void updateStatus(String offerId, boolean isActive);
}
