package com.bakcell.interfaces;

import com.bakcell.models.ulduzum.UlduzumLocationsServiceModel;

public interface UlduzumLocationsListFilterListener {
    void onListLocationSelected(String categoryId, UlduzumLocationsServiceModel ulduzumLocationsServiceModel);
}
