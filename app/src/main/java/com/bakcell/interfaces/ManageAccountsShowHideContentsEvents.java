package com.bakcell.interfaces;

/**
 * @author Junaid Hassan on 26, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public interface ManageAccountsShowHideContentsEvents {
    void onShowOrHideContents(boolean isShowContents);
}
