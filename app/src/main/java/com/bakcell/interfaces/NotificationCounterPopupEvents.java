package com.bakcell.interfaces;

/**
 * @author Junaid Hassan on 02, September, 2020
 * Senior Android Engineer. Islamabad Pakistan
 */
public interface NotificationCounterPopupEvents {
    void onViewNotificationsButtonClick();
}
