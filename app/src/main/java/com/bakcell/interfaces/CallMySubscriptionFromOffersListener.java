package com.bakcell.interfaces;

import com.bakcell.models.mysubscriptions.SubscriptionsMain;

/**
 * Created by Noman on 16-Oct-17.
 */

public interface CallMySubscriptionFromOffersListener {
    void onCheckMySubscriptionListener(SubscriptionsMain subscriptionsMain);
}
