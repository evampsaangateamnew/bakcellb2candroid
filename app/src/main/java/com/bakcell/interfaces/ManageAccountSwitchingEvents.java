package com.bakcell.interfaces;

import com.bakcell.models.user.UserModel;

/**
 * @author Junaid Hassan on 22, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public interface ManageAccountSwitchingEvents {
    void onAccountSwitchFromManageAccountsRecycler(UserModel switchedUserModel);
}
