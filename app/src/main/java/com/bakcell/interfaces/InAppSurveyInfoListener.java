package com.bakcell.interfaces;

import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Muhammad Ahmed on 8-Aug-21.
 */

public interface InAppSurveyInfoListener {
    void onInAppSurveyInfoListener(EaseResponse<String> response);
}
