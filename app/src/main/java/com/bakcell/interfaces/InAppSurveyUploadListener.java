package com.bakcell.interfaces;

import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Muhammad Ahmed on 8-Aug-21.
 */

public interface InAppSurveyUploadListener {
    void onInAppSurveyFailListener(EaseResponse<String> response);
    void onInAppSurveySuccessListener();
}
