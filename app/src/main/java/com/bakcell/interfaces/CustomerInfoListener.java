package com.bakcell.interfaces;

import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 16-Oct-17.
 */

public interface CustomerInfoListener {
    void onCustomerInfoListener(EaseResponse<String> response);
}
