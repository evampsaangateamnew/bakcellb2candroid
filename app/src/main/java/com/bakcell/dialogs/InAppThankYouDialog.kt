package com.bakcell.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.bakcell.R
import com.bakcell.databinding.DialogThankYouSurveyBinding

class InAppThankYouDialog(context: Context, private val thankYouMessage: String) : Dialog(context) {
    private lateinit var binding: DialogThankYouSurveyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setCancelable(false)
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_thank_you_survey,
            null,
            false
        )
        binding.apply {
            thankYou = thankYouMessage
            closeButton.setOnClickListener { dismiss() }
        }
        setContentView(binding.root)
    }


}