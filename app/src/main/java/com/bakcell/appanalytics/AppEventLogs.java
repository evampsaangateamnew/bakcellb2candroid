package com.bakcell.appanalytics;

import android.content.Context;
import android.os.Bundle;

import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Noman on 16-Nov-17.
 */

public class AppEventLogs {

    private static AppEventLogs appEventLogs;
    private static FirebaseAnalytics mFirebaseAnalytics;

    private AppEventLogs() {
    }


    public static AppEventLogs newInstance(Context context) {
        if (appEventLogs == null && context != null) {
            appEventLogs = new AppEventLogs();
            // Obtain the FirebaseAnalytics instance.
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }
        return appEventLogs;
    }


    /**
     * Logs Event from Apps
     *
     * @param eventType   Type of Event
     * @param screenName  Name of screen on which log event
     * @param screenEvent Actualy Event
     */
    public static void applyAppEvent(String screenName, String eventType, String screenEvent) {
        try {

            screenName = screenName.substring(0, Math.min(screenName.length(), 39));
            screenName = screenName.replaceAll(" ", "_");
            if (Tools.hasValue(screenName)) {
                screenName = screenName.toLowerCase();
            } else {
                return;
            }

            eventType = eventType.substring(0, Math.min(eventType.length(), 99));
            screenEvent = screenEvent.substring(0, Math.min(screenEvent.length(), 99));

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, eventType);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, screenEvent);

            mFirebaseAnalytics.logEvent(screenName, bundle);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }


}
