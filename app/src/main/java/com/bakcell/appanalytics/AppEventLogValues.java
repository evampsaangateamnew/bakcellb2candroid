package com.bakcell.appanalytics;

/**
 * Created by Zeeshan Shabbir on 11/16/2017.
 */

public class AppEventLogValues {

    public static class SplashEvents {
        public static final String SPLASH_SCREEN = "Splash Screen";
        public static final String APP_LAUNCHED = "App Launched";
        public static final String APP_VERSION_CHECK = "App version check api";
        public static final String APP_VERSION_UPDATED = "App Verison Updated";
        public static final String NEW_APP_VERSION_AVAILABLE = "New App Version Available";
    }

    public static class TutorialEvents {
        public static final String TUTORIAL_SCREEN = "Toturial Screens";
        public static final String NEXT_1 = "NEXT_1";
        public static final String NEXT_2 = "NEXT_2";
        public static final String NEXT_3 = "NEXT_3";
        public static final String NEXT_4 = "NEXT_4";
    }

    public static class LoginEvents {
        public static final String LOGIN_SCREEN = "Login Screen";
        public static final String USER_LOGGED_IN = "User LoggedIn";
        public static final String USER_FAILED_TO_LOGIN = "User failed to Login";
    }

    public static class SignupEvents {
        public static final String SIGN_UP_STEP_ONE = "Signup Screen Step1";
        public static final String NUMBER_VERIFICATION_SUCCESS = "Number Verification Success";
        public static final String NUMBER_VERIFICATION_FAILURE = "Number Verification Failure";

        public static final String SIGN_UP_STEP_TWO = "Signup Screen Step2";
        public static final String PIN_VERIFICATION_SUCCESS = "PIN Verification Success";
        public static final String PIN_VERIFICATION_FAILURE = "PIN Verification Failure";

        public static final String SIGN_UP_STEP_THREE = "Signup Screen Step3";
        public static final String SIGN_UP_SUCCESS = "Signup Success";
        public static final String SIGN_UP_FAILURE = "Signup Failure";
    }

    public static class ForgotPaswordEvents {
        public static final String FORGOT_PASWRD_STEP_ONE = "Forgotpassword Screen Step1";
        public static final String NUMBER_VERIFICATION_SUCCESS = "Number Verification Success";
        public static final String NUMBER_VERIFICATION_FAILURE = "Number Verification Failure";

        public static final String FORGOT_PASWRD_STEP_TWO = "Forgotpassword Screen Step2";
        public static final String PIN_VERIFICATION_SUCCESS = "PIN Verification Success";
        public static final String PIN_VERIFICATION_FAILURE = "PIN Verification Failure";

        public static final String FORGOT_PASWRD_STEP_THREE = "Forgotpassword Screen Step3";
        public static final String FORGOT_PASWRD_SUCCESS = "Forgotpassword Success";
        public static final String FORGOT_PASWRD_FAILURE = "Forgotpassword Failure";
    }

    public static class DashboardEvents {
        public static final String DASHBOARD_SCREEN = "Dashboard Screen";
        public static final String LOGGED_IN_USER_TYPE = "LogedIn User Type ";
        public static final String SWITCH_TO_ROAMING_USAGE = "Switch to Roaming Usage";
        public static final String SWITCH_TO_NORMAL_USAGE = "Switch to Normal Usage";
    }

    public static class TariffEvents {
        public static final String TARIFF_SCREEN = "Tariff Screen";
        public static final String TARIFF_CIN_SCREEN = "Tariff CIN Screen";
        public static final String TARIFF_KLASS_SCREEN = "Tariff Klass Screen";
        public static final String TARIFF_INDIVIDUAL_SCREEN = "Tariff Individual Screen";
        public static final String TARIFF_CORPORATE_SCREEN = "Tariff Corporate Screen";
        public static final String POSTPAID_KLASS_SCREEN = "Postpaid Klass Screen";
        public static final String TARIFF_MIGRATION_SUCCESS = "Tariff Migration Success";
        public static final String TARIFF_MIGRATION_FAILURE = "Tariff Migration Failure";
    }

    public static class ServiceEvents {
        public static final String SERVICE_SCREEN = "Services Screen";
        public static final String SERVICE_SUPPLEMENTARY_OFFERS_SCREEN = "Services Supplementary Offers Screen";
        public static final String SERVICE_SUPPLEMENTARY_ACTIVATION_SUCCESS = "Offers Activation Success";
        public static final String SERVICE_SUPPLEMENTARY_ACTIVATION_FAILURE = "Offers Activation Failure";
        public static final String SERVICE_QUICK_SERVICES_SCREEN = "Services Quik Services Screen";
        public static final String SERVICE_QUICK_SERVICES_MESSAGE_SEND_SUCCESS = "Message Send Success";
        public static final String SERVICE_QUICK_SERVICES_MESSAGE_SEND_FAILURE = "Message Send Failure";

        public static final String SERVICE_CORE_SERVICES_SCREEN = "Services Core Services Screen";
        public static final String CORE_SERVICES_ENABLE_SUCCESS = "Services Enable Success";
        public static final String CORE_SERVICES_ENABLE_FAILURE = "Services Enable Failure";
        public static final String CORE_SERVICES_DISABLE_SUCCESS = "Services Disable Success";
        public static final String CORE_SERVICES_DISABLE_FAILURE = "Services Disable Failure";

        public static final String FORWARD_NUMBER_SUCCESS = "Farword Number Success";
        public static final String FORWARD_NUMBER_FAILURE = "Farword Number Failure";

        public static final String SERVICES_FRIENDS_AND_FAMILY_SCREEN = "Services Friends & Family Screen";
        public static final String SERVICES_FRIENDS_AND_FAMILY_NUMBER_ADD_SUCCESS = "Friends & Family Number Add Success";
        public static final String SERVICES_FRIENDS_AND_FAMILY_NUMBER_ADD_FAILURE = "Friends & Family Number Add Failure";
        public static final String SERVICES_FRIENDS_AND_FAMILY_NUMBER_DELETE_SUCCESS = "Friends & Family Number Delete Success";
        public static final String SERVICES_FRIENDS_AND_FAMILY_NUMBER_DELETE_FAILURE = "Friends & Family Number Delete Failure";
    }

    public static class TopupEvents {
        public static final String TOP_UP_SCREEN = "Top up Screen";
        public static final String TOP_UP_SCRATCH_CARD_SUCCESS = "Top up Scratch Card Success";
        public static final String TOP_UP_SCRATCH_CARD_FAILURE = "Top up Scratch Card Failure";
        public static final String TOP_UP_PLASTIC_CARD_SUCCESS = "Top up Plastic Card Success";
        public static final String TOP_UP_PLASTIC_CARD_FAILURE = "Top up Plastic Card Failure";
    }

    public static class MoneyTransferEvents {
        public static final String MONEY_TRANSFER_SCREEN = "Money transfer Screen";
        public static final String MONEY_TRANSFER_SUCCESS = "Money transfer Success";
        public static final String MONEY_TRANSFER_FAILURE = "Money transfer Failure";
    }

    public static class LoanEvents {
        public static final String LOAN_SCREEN = "Loan Screen";
        public static final String LOAN_SUCCESS = "Loan Success";
        public static final String LOAN_FAILURE = "Loan Failure";
        public static final String LOAN_REQUEST_HISTORY_SCREEN = "Loan Request History Screen";
        public static final String LOAN_PAYMENT_HISTORY_SCREEN = "Loan Payment History Screen";
    }

    public static class AccountEvents {
        public static final String MY_ACCOUNT = "My Account";

        public static final String MY_SUBSCRIPTION_SCREEN = "My subscription Screen";
        public static final String MY_SUBSCRIPTION_RENEW_SUCCESS = "Renew Success";
        public static final String MY_SUBSCRIPTION_RENEW_FAILURE = "Renew Failure";
        public static final String MY_SUBSCRIPTION_DEACTIVATE_SUCCESS = "Deativate Success";
        public static final String MY_SUBSCRIPTION_DEACTIVATE_FAILURE = "Deativate Failure";


        public static final String USAGE_HISTORY_SCREEN = "Usage History Screen";
        public static final String USAGE_HISTORY_SUMMARY_SCREEN = "Usage History Summary Screen";

        public static final String USAGE_HISTORY_DETAIL_SCREEN = "Usage History Detail Screen";
        public static final String USAGE_HISTORY_DETAIL_PASSPORT_VERIFICATION_SUCCESS = "Usage Detail Passport Verification Success";
        public static final String USAGE_HISTORY_DETAIL_PASSPORT_VERIFICATION_FAILURE = "Usage Detail Passport Verification Failure";
        public static final String USAGE_HISTORY_DETAIL_PIN_VERIFICATION_SUCCESS = "Usage Detail PIN Verification Success";
        public static final String USAGE_HISTORY_DETAIL_PIN_VERIFICATION_FAILURE = "Usage Detail PIN Verification Failure";

        public static final String OPERATION_HISTORY_SCREEN = "Operation History Screen";
        public static final String MY_INSTALLMENT_SCREEN = "My Installments Screen";
    }

    public static class NotificationEvents {
        public static final String NOTIFICATIONS_SCREEN = "Notification Screen";
        public static final String APP_OPENED_VIA_NOTIFICATIONS = "App Opened Via Notificaiton";
    }

    public static class StoreLocatorEvents {
        public static final String STORE_LOCATOR_MAP = "Store Locator Map";
        public static final String STORE_LOCATOR_LIST = "Store Locator List";
    }

    public static class FAQEvents {
        public static final String FAQ_SCREEN = "FAQ Screen";
    }

    public static class LiveChatEvents {
        public static final String LIVE_CHAT_SCREEN = "Live Chat Screen";

    }

    public static class ContactUsEvents {
        public static final String CONTACT_US_SCREEN = "Contact Us Screen";

    }

    public static class TermsAndConditionEvents {
        public static final String TERMS_AND_CONDITIONS_SCREEN = "Terms And Conditions Screen";

    }

    public static class SettingsEvents {
        public static final String SETTINGS_SCREEN = "settings";
        public static final String APP_LANGUAGE_CHANGED = "App Languge Changed To ";
        public static final String NOTIFICATION_ENABLED = "Notification Enabled";
        public static final String NOTIFICATION_DISABLED = "Notification Disabled";

        public static final String CHANGE_PASWRD_SCREEN = "Change Password Screen";
        public static final String CHANGE_PASWRD_SUCCESS = "Change Password Success";
        public static final String CHANGE_PASWRD_FAILURE = "Change Password Failure";

    }

    public static class LogoutDialogEvents {
        public static final String USER_LOGGED_OUT = "User LoggedOut";
    }

    public static class ProfileScreen {
        public static final String PROFILE_SCREEN = "Profile Screen";
        public static final String ADD_IMAGE_SUCCESS = "Add Image Success";
        public static final String ADD_IMAGE_FAILURE = "Add Image Failure";
        public static final String REMOVE_IMAGE_SUCCESS = "Image Remove Success";
        public static final String REMOVE_IMAGE_FAILURE = "Image Remove Failure";

        public static final String CHANGE_EMAIL_SCREEN = "Change Email Screen";
        public static final String CHANGE_EMAIL_SUCCESS = "Email Changed Success";
        public static final String CHANGE_EMAIL_FAILURE = "Email Changed Failure";

        public static final String BILLING_LANGUAGE_CHANGED_SUCCESS = "Billing Language Changed To ";
        public static final String BILLING_LANGUAGE_CHANGED_FAILURE = "Billing Language Changed To ";

    }


    public static class RoamingScreen
    {
        public static final String ROAMING_SCREEN = "Roaming Screen";
        public static final String ROAMING_PACKAGES_SCREEN = "Roaming Packages Screen";
        public static final String ROAMING_PRICE_OPERATORS_SCREEN = "Roaming Price & Operator Screen";
        public static final String ROAMING_ACTIVATION_STATUS_SUCESS = "Roaming Activation Status Success";
        public static final String ROAMING_ACTIVATION_STATUS_FAILURE = "Roaming Activation Status Failure";
        public static final String ROAMING_ACTIVATION_SUCCESS = "Roaming Activation Success";
        public static final String ROAMING_ACTIVATION_FAILURE = "Roaming Activation Failure";
    }

    public static class UlduzumScreen
    {
        public static final String ULDUZUM_MERCHANT_DETAILS_SCREEN = "Ulduzum Merchant Detail Screen";
        public static final String ULDUZUM_MERCHANT_DETAILS_SUCESS = "Ulduzum Merchant Detail API Success";
        public static final String ULDUZUM_MERCHANT_DETAILS_FAILURE = "Ulduzum Merchant Detail API Failure";

    }

}
