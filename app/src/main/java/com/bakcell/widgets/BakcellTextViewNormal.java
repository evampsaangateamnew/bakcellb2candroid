package com.bakcell.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.bakcell.globals.RootValues;

/**
 * Created by Freeware Sys on 16-May-17.
 */

public class BakcellTextViewNormal extends androidx.appcompat.widget.AppCompatTextView {
    public BakcellTextViewNormal(Context context) {
        super(context);
    }

    public BakcellTextViewNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BakcellTextViewNormal(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(RootValues.getInstance().getFontArialRegular());
    }
}
