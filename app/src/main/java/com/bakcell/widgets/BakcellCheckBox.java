package com.bakcell.widgets;

import android.content.Context;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.util.AttributeSet;

/**
 * Created by Zeeshan Shabbir on 9/18/2017.
 */

/*public class BakcellCheckBox {
}*/
public class BakcellCheckBox extends AppCompatCheckBox {
    private OnCheckedChangeListener mListener;

    public BakcellCheckBox(final Context context) {
        super(context);
    }

    public BakcellCheckBox(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public BakcellCheckBox(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setOnCheckedChangeListener(final OnCheckedChangeListener listener) {
        mListener = listener;
        super.setOnCheckedChangeListener(listener);
    }

    public void setChecked(final boolean checked, final boolean alsoNotify) {
        if (!alsoNotify) {
            super.setOnCheckedChangeListener(null);
            super.setChecked(checked);
            super.setOnCheckedChangeListener(mListener);
            return;
        }
        super.setChecked(checked);
    }

    public void toggle(boolean alsoNotify) {
        if (!alsoNotify) {
            super.setOnCheckedChangeListener(null);
            super.toggle();
            super.setOnCheckedChangeListener(mListener);
        }
        super.toggle();
    }
}