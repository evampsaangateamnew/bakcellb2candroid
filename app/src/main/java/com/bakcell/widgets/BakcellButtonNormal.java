package com.bakcell.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.bakcell.globals.RootValues;

/**
 * Created by Zeeshan Shabbir on 8/7/2017.
 */

public class BakcellButtonNormal extends androidx.appcompat.widget.AppCompatButton {
    public BakcellButtonNormal(Context context) {
        super(context);
    }

    public BakcellButtonNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BakcellButtonNormal(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(RootValues.getInstance().getFontArialRegular());
    }
}
