package com.bakcell.widgets;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.bakcell.globals.RootValues;
import com.bakcell.utilities.Logger;

/**
 * Created by Zeeshan Shabbir on 7/26/2017.
 */

public class RecyclerItemDecorator extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        Logger.debugLog("TAG", RootValues.getInstance().getTARIFF_PAGGER_OFFSET() + "");
        outRect.left = (int) (-10 * view.getResources().getDisplayMetrics().density);
        outRect.right = (int) (-10 * view.getResources().getDisplayMetrics().density);
        outRect.top = 0;
        outRect.bottom = 0;
        //super.getItemOffsets(outRect, view, parent, state);
    }
}

