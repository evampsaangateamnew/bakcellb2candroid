package com.bakcell.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.bakcell.globals.RootValues;

/**
 * Created by Freeware Sys on 16-May-17.
 */

public class BakcellButtonBold extends androidx.appcompat.widget.AppCompatButton {


    public BakcellButtonBold(Context context) {
        super(context);
    }

    public BakcellButtonBold(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BakcellButtonBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(RootValues.getInstance().getFontArialBold());
    }
}
