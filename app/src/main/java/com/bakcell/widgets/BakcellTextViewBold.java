package com.bakcell.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.bakcell.globals.RootValues;

/**
 * Created by Freeware Sys on 16-May-17.
 */

public class BakcellTextViewBold extends androidx.appcompat.widget.AppCompatTextView {


    public BakcellTextViewBold(Context context) {
        super(context);
        setTextEncording();
    }

    public BakcellTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTextEncording();

    }

    public BakcellTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTextEncording();

    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(RootValues.getInstance().getFontArialBold());


    }


    public void setTextEncording() {
//        String txt = getText().toString();
//        String value = new String(txt.getBytes(), ISO_8859_1);
//        setText(value);
    }


}
