package com.bakcell.widgets;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.bakcell.globals.RootValues;

/**
 * Created by Zeeshan Shabbir on 7/10/2017.
 */

public class BakcellEditText extends AppCompatEditText {
    public BakcellEditText(Context context) {
        super(context);
        setLongClickable(false);
        setTextIsSelectable(false);
    }

    public BakcellEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLongClickable(false);
        setTextIsSelectable(false);
    }

    public BakcellEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLongClickable(false);
        setTextIsSelectable(false);
    }

    @Override
    public void selectAll() {

    }

    private int mPreviousCursorPosition;

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        CharSequence text = getText();
        if (text != null) {
            if (selStart != selEnd) {
                setSelection(mPreviousCursorPosition, mPreviousCursorPosition);
                return;
            }
        }
        mPreviousCursorPosition = selStart;
        super.onSelectionChanged(selStart, selEnd);
    }

    @Override
    public boolean isSuggestionsEnabled() {
        return false;
    }

    @Override
    public void setCustomSelectionActionModeCallback(ActionMode.Callback actionModeCallback) {
        ActionMode.Callback callback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        };
        super.setCustomSelectionActionModeCallback(callback);
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        if (id == android.R.id.paste) return false;
        return super.onTextContextMenuItem(id);

    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(RootValues.getInstance().getFontArialRegular());
    }
}
