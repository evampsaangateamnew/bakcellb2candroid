package com.bakcell.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

public class BakcellVideoView extends VideoView {
    public BakcellVideoView(Context context) {
        super(context);
    }

    public BakcellVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BakcellVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//		setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
        //Log.i("@@@@", "onMeasure");
        int width = getDefaultSize(this.getWidth(), widthMeasureSpec);
        int height = getDefaultSize(this.getHeight(), heightMeasureSpec);
        if (this.getWidth() > 0 && this.getHeight() > 0) {
            if (this.getWidth() * height > width * this.getHeight()) {
                //Log.i("@@@", "image too tall, correcting");
                height = width * this.getHeight() / this.getWidth();
            } else if (this.getWidth() * height < width * this.getHeight()) {
                //Log.i("@@@", "image too wide, correcting");
                width = height * this.getWidth() / this.getHeight();
            } else {
                //Log.i("@@@", "aspect ratio is correct: " +
                //width+"/"+height+"="+
                //mVideoWidth+"/"+mVideoHeight);
            }
        }
        //Log.i("@@@@@@@@@@", "setting size: " + width + 'x' + height);
        setMeasuredDimension(width, height);
    }
}