package com.bakcell.widgets;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Custom @ViewPager which gives us ability to enable/disable the scrolling of pages
 *
 * @author Zeeshan Shabbir
 */

public class BakcellViewPagger extends ViewPager {
    private Boolean disable = false;

    public BakcellViewPagger(Context context) {
        super(context);
    }

    public BakcellViewPagger(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return disable ? false : super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return disable ? false : super.onTouchEvent(event);
    }

    public void disableScroll(Boolean disable) {
        //When disable = true not work the scroll and when disble = false work the scroll
        this.disable = disable;
    }
}
