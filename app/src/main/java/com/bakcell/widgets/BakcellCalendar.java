package com.bakcell.widgets;

import android.app.DatePickerDialog;
import androidx.core.content.ContextCompat;
import android.widget.DatePicker;

import com.bakcell.R;
import com.bakcell.globals.Constants;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Zeeshan Shabbir on 8/16/2017.
 */

public class BakcellCalendar implements DatePickerDialog.OnDateSetListener {
    BakcellTextViewNormal mTextView;
    DatePickerDialog.OnDateSetListener onDate;


    public BakcellCalendar(BakcellTextViewNormal textViewNormal, DatePickerDialog.OnDateSetListener onDate) {
        mTextView = textViewNormal;
        this.onDate = onDate;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, dayOfMonth);

        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT);
        String formattedDate = sdf.format(c.getTime());
        try {
            mTextView.setText(Tools.changeDateFormatForTextView(formattedDate));
            mTextView.setTextColor(ContextCompat.getColor(view.getContext(), R.color.text_date_color));
            onDate.onDateSet(view, year, month, dayOfMonth);
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }
}

