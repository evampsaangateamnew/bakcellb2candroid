package com.bakcell.fragments.dialogs;

import android.app.Dialog;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;

import com.bakcell.R;
import com.bakcell.activities.landing.RoamingOfferActivity;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.adapters.AdapterFilterPackage;
import com.bakcell.adapters.CardAdapterSupplementaryOffers;
import com.bakcell.databinding.DialogFilterFragmentBinding;
import com.bakcell.globals.RootValues;
import com.bakcell.models.supplementaryoffers.OfferFilter;
import com.bakcell.models.supplementaryoffers.OfferFiltersMain;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 9/18/2017.
 */


public class BakcellFilterDialogFragment extends DialogFragment implements View.OnClickListener,
        AdapterFilterPackage.OnitemSelection {

    private static final String OFFERS_FILTER = "offers.filters";
    private static final String IS_FROM_ROAMING = "offers.from.raoming";
    DialogFilterFragmentBinding binding;

    private boolean isFromRoaming;

    private OfferFiltersMain offerFiltersMain;

    private AdapterFilterPackage appAdapter;
    private AdapterFilterPackage tabAdapter;
    private AdapterFilterPackage desktopAdapter;


    public static BakcellFilterDialogFragment newInstance(OfferFiltersMain offerFiltersMain,
                                                          boolean fromRoaming) {
        BakcellFilterDialogFragment frag = new BakcellFilterDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(OFFERS_FILTER, offerFiltersMain);
        args.putBoolean(IS_FROM_ROAMING, fromRoaming);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        offerFiltersMain = getArguments().getParcelable(OFFERS_FILTER);
        isFromRoaming = getArguments().getBoolean(IS_FROM_ROAMING);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_filter_fragment, container, false);
        initUiListeners();
        initUiViews();
        initSetUiTabs();
        return binding.getRoot();
    }

    private void initUiViews() {
        if (isFromRoaming) {
            binding.topLayout.setVisibility(View.GONE);
            binding.seperator1.setVisibility(View.INVISIBLE);
            binding.seperator2.setVisibility(View.GONE);
            binding.bottomSeperator.setVisibility(View.GONE);
            binding.dummyView.setVisibility(View.VISIBLE);
            binding.btnReset.setVisibility(View.GONE);
            binding.tvFilterText.setText(R.string.dialog_filter_fragment_roaming_lbl);
        } else {
            binding.topLayout.setVisibility(View.VISIBLE);
            binding.seperator1.setVisibility(View.VISIBLE);
            binding.seperator2.setVisibility(View.VISIBLE);
            binding.bottomSeperator.setVisibility(View.VISIBLE);
            binding.btnReset.setVisibility(View.VISIBLE);
            binding.dummyView.setVisibility(View.GONE);
            binding.tvFilterText.setText(R.string.lbl_package_type);
        }
    }

    private void initSetUiTabs() {

        if (offerFiltersMain != null) {
            if (offerFiltersMain.getApp() == null) {
                binding.layoutPhone.setVisibility(View.INVISIBLE);
            } else {
                preparePhone();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                appAdapter = new AdapterFilterPackage(getActivity(),
                        SupplementaryOffersActivity.LAYOUT_PHONE, offerFiltersMain.getApp(), this);
                binding.lvPackagePhone.setLayoutManager(layoutManager);
                binding.lvPackagePhone.setAdapter(appAdapter);
                if (RootValues.getInstance().getPhoneFilter() != null
                        && RootValues.getInstance().getPhoneFilter().size() > 0) {
                    appAdapter.alterSelectedFilter(RootValues.getInstance().getPhoneFilter());
                }
                if (RootValues.getInstance().getPhoneFilter() == null) {
                    appAdapter.selectAll();
                }
            }
            if (offerFiltersMain.getTab() == null || offerFiltersMain.getTab().size() == 0) {
                binding.layoutTab.setVisibility(View.INVISIBLE);
                binding.tabSeperator.setVisibility(View.INVISIBLE);
            } else {
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                binding.tabSeperator.setVisibility(View.VISIBLE);
                tabAdapter = new AdapterFilterPackage(getActivity(),
                        SupplementaryOffersActivity.LAYOUT_TAB, offerFiltersMain.getTab(), this);
                binding.lvPackageTab.setLayoutManager(layoutManager);
                binding.lvPackageTab.setAdapter(tabAdapter);
                if (RootValues.getInstance().getTabFilter() != null && RootValues.getInstance().getTabFilter().size() > 0) {
                    tabAdapter.alterSelectedFilter(RootValues.getInstance().getTabFilter());
                }
                if (RootValues.getInstance().getTabFilter() == null) {
                    tabAdapter.selectAll();
                }
            }
            if (offerFiltersMain.getDesktop() == null || offerFiltersMain.getDesktop().size() == 0) {
                binding.layoutTv.setVisibility(View.INVISIBLE);
            } else {
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                desktopAdapter = new AdapterFilterPackage(getActivity(),
                        SupplementaryOffersActivity.LAYOUT_DESKTOP, offerFiltersMain.getDesktop(), this);
                binding.lvPackageDesktop.setLayoutManager(layoutManager);
                binding.lvPackageDesktop.setAdapter(desktopAdapter);
                if (RootValues.getInstance().getTvFilter() != null && RootValues.getInstance().getTvFilter().size() > 0) {
                    desktopAdapter.alterSelectedFilter(RootValues.getInstance().getTvFilter());
                }

                if (RootValues.getInstance().getTvFilter() == null) {
                    desktopAdapter.selectAll();
                }

            }
        }

        if (Tools.hasValue(RootValues.getInstance().getFilterGroupAll())) {
            if (RootValues.getInstance().getFilterGroupAll().contains(CardAdapterSupplementaryOffers.OFFER_GROUP_MOBILE)) {
                binding.checkboxApp.setChecked(true);
            }
            if (RootValues.getInstance().getFilterGroupAll().contains(CardAdapterSupplementaryOffers.OFFER_GROUP_TAB)) {
                binding.checkboxTab.setChecked(true);
            }
            if (RootValues.getInstance().getFilterGroupAll().contains(CardAdapterSupplementaryOffers.OFFER_GROUP_DESKTOP)) {
                binding.checkboxDesktop.setChecked(true);
            }
        }

        if (RootValues.getInstance().getFilterGroupAll() == null) {
            binding.checkboxApp.setChecked(true);
            binding.checkboxTab.setChecked(true);
            binding.checkboxDesktop.setChecked(true);
        }

    }

    private void initUiListeners() {
        binding.layoutPhone.setOnClickListener(this);
        binding.layoutTab.setOnClickListener(this);
        binding.layoutTv.setOnClickListener(this);
        binding.applyFilter.setOnClickListener(this);
        binding.btnReset.setOnClickListener(this);

        //All check for app
        binding.checkboxApp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (appAdapter != null) {
                    itemSelected(SupplementaryOffersActivity.LAYOUT_PHONE, appAdapter.getOfferFilters());
                }
            }
        });
        //All check for tab
        binding.checkboxTab.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (tabAdapter != null) {
                    itemSelected(SupplementaryOffersActivity.LAYOUT_TAB, tabAdapter.getOfferFilters());
                }
            }
        });
        //All check for desktop
        binding.checkboxDesktop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (desktopAdapter != null) {
                    itemSelected(SupplementaryOffersActivity.LAYOUT_DESKTOP, desktopAdapter.getOfferFilters());
                }
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_phone: //Show phone layout
                preparePhone();
                break;
            case R.id.layout_tab: //Show tab layout
                prepareTab();
                break;
            case R.id.layout_tv: // Show desktop layout
                prepareDesktop();
                break;
            case R.id.applyFilter:
                sendFilterToActivity();
                break;
            case R.id.btn_reset:
                binding.checkboxApp.setChecked(true);
                binding.checkboxTab.setChecked(true);
                binding.checkboxDesktop.setChecked(true);
                if (appAdapter != null) {
                    appAdapter.selectAll();
                }
                if (tabAdapter != null) {
                    tabAdapter.selectAll();
                }
                if (desktopAdapter != null) {
                    desktopAdapter.selectAll();
                }
                break;
        }
    }

    private void sendFilterToActivity() {
        ArrayList<OfferFilter> filterArrayList = new ArrayList<>();
        if (appAdapter != null) {
            RootValues.getInstance().setPhoneFilter(appAdapter.getOfferFilters());
            if (!binding.checkboxApp.isChecked()) {
                filterArrayList.addAll(appAdapter.getOfferFilters());
            }
        }
        if (tabAdapter != null) {
            RootValues.getInstance().setTabFilter(tabAdapter.getOfferFilters());
            if (!binding.checkboxTab.isChecked()) {
                filterArrayList.addAll(tabAdapter.getOfferFilters());
            }
        }
        if (desktopAdapter != null) {
            RootValues.getInstance().setTvFilter(desktopAdapter.getOfferFilters());
            if (!binding.checkboxDesktop.isChecked()) {
                filterArrayList.addAll(desktopAdapter.getOfferFilters());
            }
        }

        String offerGroupFilterAll = "";
        if (binding.checkboxApp.isChecked()) {
            offerGroupFilterAll = offerGroupFilterAll + CardAdapterSupplementaryOffers.OFFER_GROUP_MOBILE + ",";
        }
        if (binding.checkboxTab.isChecked()) {
            offerGroupFilterAll = offerGroupFilterAll + CardAdapterSupplementaryOffers.OFFER_GROUP_TAB + ",";
        }
        if (binding.checkboxDesktop.isChecked()) {
            offerGroupFilterAll = offerGroupFilterAll + CardAdapterSupplementaryOffers.OFFER_GROUP_DESKTOP + ",";
        }
        RootValues.getInstance().setFilterGroupAll(offerGroupFilterAll);

        if (!isFromRoaming) {
            ((SupplementaryOffersActivity) getActivity()).applyFilter(filterArrayList, offerGroupFilterAll);
        } else {
            ((RoamingOfferActivity) getActivity()).performFiltering(filterArrayList, offerGroupFilterAll);
        }

    }

    private void prepareDesktop() {
        binding.phoneList.setVisibility(View.GONE);
        binding.tabList.setVisibility(View.GONE);
        binding.tvList.setVisibility(View.VISIBLE);
        binding.layoutPhone.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.white));
        binding.layoutTab.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.white));
        binding.layoutTv.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.light_gray));
    }

    private void prepareTab() {
        binding.phoneList.setVisibility(View.GONE);
        binding.tabList.setVisibility(View.VISIBLE);
        binding.tvList.setVisibility(View.GONE);
        binding.layoutPhone.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.white));
        binding.layoutTab.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.light_gray));
        binding.layoutTv.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.white));
    }

    private void preparePhone() {
        binding.phoneList.setVisibility(View.VISIBLE);
        binding.tabList.setVisibility(View.GONE);
        binding.tvList.setVisibility(View.GONE);
        binding.layoutPhone.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.light_gray));
        binding.layoutTab.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.white));
        binding.layoutTv.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.white));
    }

    @Override
    public void itemSelected(String type, ArrayList<OfferFilter> offerFilters) {
        switch (type) {
            case SupplementaryOffersActivity.LAYOUT_PHONE:
                if (binding.checkboxApp.isChecked()) {
                    binding.icPhone.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_phone_red));
                } else if (offerFilters != null && offerFilters.size() > 0) {
                    binding.icPhone.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_phone_yellow));
                } else {
                    binding.icPhone.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_phone));
                }
                break;
            case SupplementaryOffersActivity.LAYOUT_TAB:
                if (binding.checkboxTab.isChecked()) {
                    binding.icTab.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_tablet_red));
                } else if (offerFilters != null && offerFilters.size() > 0) {
                    binding.icTab.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_tablet_yellow));
                } else {
                    binding.icTab.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_tablet));
                }
                break;
            case SupplementaryOffersActivity.LAYOUT_DESKTOP:
                if (binding.checkboxDesktop.isChecked()) {
                    binding.icTv.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_tv_red));
                } else if (offerFilters != null && offerFilters.size() > 0) {
                    binding.icTv.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_tv_yellow));
                } else {
                    binding.icTv.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                            R.drawable.ic_filter_tv));
                }
                break;
        }
    }
}
