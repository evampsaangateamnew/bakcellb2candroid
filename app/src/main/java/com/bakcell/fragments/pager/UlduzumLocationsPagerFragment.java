package com.bakcell.fragments.pager;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.UlduzumLocationsActivity;
import com.bakcell.activities.landing.UlduzumMerchantDetailsActivity;
import com.bakcell.databinding.FragmentUlduzumLocationsBinding;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.UlduzumMapLocationRedirectionListener;
import com.bakcell.models.ulduzum.UlduzumLocationsServiceModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class UlduzumLocationsPagerFragment extends Fragment implements View.OnClickListener, GoogleMap.OnMarkerClickListener {
    int currentMarker = -1;
    private MapView mMapView = null;
    private final String fromClass = "UlduzumLocationsPagerFragment";
    private GoogleMap mGoogleMap = null;
    private UlduzumLocationsServiceModel serviceModel;
    private FragmentUlduzumLocationsBinding binding;
    private UlduzumMapLocationRedirectionListener ulduzumMapLocationRedirectionListener = null;

    public static UlduzumLocationsPagerFragment getInstance(UlduzumLocationsServiceModel serviceModel) {
        UlduzumLocationsPagerFragment fragment = new UlduzumLocationsPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(UlduzumLocationsActivity.ULDUZUM_LOCATIONS_BUNDLE_KEY, serviceModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(UlduzumLocationsActivity.ULDUZUM_LOCATIONS_BUNDLE_KEY)) {
            serviceModel = getArguments().getParcelable(UlduzumLocationsActivity.ULDUZUM_LOCATIONS_BUNDLE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ulduzum_locations, container, false);

        registerListener();
        initUI();
        initMap(savedInstanceState);
        return binding.getRoot();
    }

    private void registerListener() {
        ulduzumMapLocationRedirectionListener = new UlduzumMapLocationRedirectionListener() {
            @Override
            public void onMapLocationSelected(String categoryId, UlduzumLocationsServiceModel ulduzumLocationsServiceModel) {
                if (categoryId.equalsIgnoreCase(Constants.UlduzumConstants.ULDUZUM_CATEGORIES_FIRST_ITEM)) {
                    prepareLocationsOnMap(ulduzumLocationsServiceModel, true);
                } else {
                    //show the location on the map
                    filterLocationsOnMap(categoryId, ulduzumLocationsServiceModel);
                }
            }
        };
        binding.imgMoreDetail.setOnClickListener(this::onClick);
        binding.tfLocationName.setOnClickListener(this::onClick);
        binding.tfLocationAddress.setOnClickListener(this::onClick);
        RootValues.getInstance().setUlduzumMapLocationRedirectionListener(ulduzumMapLocationRedirectionListener);

    }//registerListener ends

    private void initUI() {

    }//initUI ends

    private void initMap(Bundle savedInstanceState) {
        if (binding.mapView != null) {
            binding.mapView.onCreate(savedInstanceState);
            binding.mapView.onResume();
        }//if (binding.mapView != null)  ends

        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception exp) {
            BakcellLogger.logE("ulduzumMaps", "error:::" + exp.toString(), fromClass, "initMap");
        }//catch ends

        binding.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                if (mGoogleMap != null) {
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            if (binding.markerLayout != null)
                                binding.markerLayout.setVisibility(View.GONE);

                        }
                    });
                    if (serviceModel != null) {
                        if (serviceModel.getMaindatalist() != null) {
                            if (serviceModel.getMaindatalist().size() > 0) {
                                prepareLocationsOnMap(serviceModel, false);
                                binding.contentLayout.setVisibility(View.VISIBLE);
                                binding.noDataFoundLayout.setVisibility(View.GONE);
                            } else {
                                //hide layout
                                binding.contentLayout.setVisibility(View.GONE);
                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                            }
                        } else {
                            //hide layout
                            binding.contentLayout.setVisibility(View.GONE);
                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        //hide layout
                        binding.contentLayout.setVisibility(View.GONE);
                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    //hide layout
                    binding.contentLayout.setVisibility(View.GONE);
                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                }
            }
        });//OnMapReadyCallback ends
    }//initMap ends

    private void filterLocationsOnMap(String categoryId, UlduzumLocationsServiceModel model) {
        if (mGoogleMap == null) {
            return;
        }//safe passage

        mGoogleMap.clear();
        LatLngBounds.Builder latLongBounds = new LatLngBounds.Builder();
        int filterCount = 0;
        String latitude = "";
        String longitutde = "";
        //match the id in the main datalist
        for (int j = 0; j < model.getMaindatalist().size(); j++) {
            if (categoryId.equalsIgnoreCase(model.getMaindatalist().get(j).getCategory_id())) {
                //skip the items without the lat long values
                if (Tools.hasValue(model.getMaindatalist().get(j).getCoord_lat()) && Tools.hasValue(model.getMaindatalist().get(j).getCoord_lng())) {
                    //increment the filter count by 1
                    filterCount += 1;
                    latitude = model.getMaindatalist().get(j).getCoord_lat();
                    longitutde = model.getMaindatalist().get(j).getCoord_lng();
                    prepareFilteredLocationsOnMap(
                            latLongBounds,
                            model.getMaindatalist().get(j).getCoord_lat(),
                            model.getMaindatalist().get(j).getCoord_lng(),
                            model.getMaindatalist().get(j).getName(),
                            "" + j
                    );

                    BakcellLogger.logE("mapDataX", "idkey:::" + categoryId + " catName:::" + model.getMaindatalist().get(j).getName() +
                                    "lat:::" + model.getMaindatalist().get(j).getCoord_lat() + " lon:::" + model.getMaindatalist().get(j).getCoord_lng() +
                                    " name:::" + "model.getMaindatalist().get(j).getAddress()",
                            fromClass, "filterLocationsOnMap");
                }//if Tools has Value
            }//if ends
        }//for ends

        boolean isOneMarker = false;
        if (filterCount < 2) {
            isOneMarker = true;
        }

        // Set Camera For Locations
        setGoogleMapZoomForFilteredLocations(mGoogleMap, latLongBounds, isOneMarker, latitude, longitutde, filterCount);
    }//filterLocationsOnMap ends

    private void setGoogleMapZoomForFilteredLocations(GoogleMap googleMap, LatLngBounds.Builder latLongBounds, boolean isOneMarker, String latitude, String longitude, int filterCount) {
        if (googleMap == null || latLongBounds == null) return;
        try {
            LatLngBounds bounds = latLongBounds.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

            CameraUpdate camZoom;

            if (isOneMarker && Tools.hasValue(latitude) && Tools.hasValue(longitude) && filterCount > 0) {
                float lat = Tools.getFloatFromString(latitude);
                float lng = Tools.getFloatFromString(longitude);
                LatLng latLng = new LatLng(lat, lng);

                camZoom = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            } else {
                camZoom = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            }

            googleMap.animateCamera(camZoom);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    private void prepareFilteredLocationsOnMap(LatLngBounds.Builder latLongBounds, String latitude, String longitude, String categoryName, String index) {
        //setup markers data
        LatLng location = new LatLng(Tools.getDoubleFromString(latitude), Tools.getDoubleFromString(longitude));

        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                .position(location)
//                .title(categoryName)
//                .snippet(address)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ulduzumlocationmarker)));
        marker.setTag("" + index);
        //include the location
        latLongBounds.include(location);
    }//prepareLocationsOnMap ends

    private void prepareLocationsOnMap(UlduzumLocationsServiceModel model, boolean isReset) {
        if (isReset) {
            if (mGoogleMap != null) {
                mGoogleMap.clear();//clear the map when user selects all from the spinner
                //else don't do anything
            }
        }
        //setup markers data
        LatLngBounds.Builder latLongBounds = new LatLngBounds.Builder();
        for (int i = 0; i < model.getMaindatalist().size(); i++) {
            //skip the items without the lat long values
            if (Tools.hasValue(model.getMaindatalist().get(i).getCoord_lat()) && Tools.hasValue(model.getMaindatalist().get(i).getCoord_lng())) {
                double latitude = Tools.getDoubleFromString(model.getMaindatalist().get(i).getCoord_lat());
                double longitude = Tools.getDoubleFromString(model.getMaindatalist().get(i).getCoord_lng());

                LatLng location = new LatLng(latitude, longitude);

                Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                        .position(location)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ulduzumlocationmarker)));
                marker.setTag("" + String.valueOf(i));
                //include the location
                latLongBounds.include(location);
            }//if Tools has Value
        }//for ends

        boolean isOneMarker = false;
        if (model.getMaindatalist().size() < 2) {
            isOneMarker = true;
        }
        //set ClickListener to Marker
        mGoogleMap.setOnMarkerClickListener(this);

        // Set Camera For Locations
        setGoogleMapZoom(mGoogleMap, latLongBounds, isOneMarker, model);
    }//prepareLocationsOnMap ends

    private void setGoogleMapZoom(GoogleMap googleMap, LatLngBounds.Builder latLongBounds, boolean isOneMarker, UlduzumLocationsServiceModel model) {
        if (googleMap == null || latLongBounds == null) return;

        try {
            LatLngBounds bounds = latLongBounds.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen
            CameraUpdate camZoom;

            if (isOneMarker && model != null && model.getMaindatalist().size() > 0) {
                float lat = Tools.getFloatFromString(model.getMaindatalist().get(0).getCoord_lat());
                float lng = Tools.getFloatFromString(model.getMaindatalist().get(0).getCoord_lat());
                LatLng latLng = new LatLng(lat, lng);

                camZoom = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            } else {
                camZoom = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            }

            googleMap.animateCamera(camZoom);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tf_locationAddress:
            case R.id.tf_locationName:
            case R.id.img_moreDetail:
                {
                if (currentMarker == -1) return; //safe passage
                if (serviceModel != null && serviceModel.getMaindatalist() != null && serviceModel.getMaindatalist().get(currentMarker) != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("merchantId", serviceModel.getMaindatalist().get(currentMarker).getId());
                    BaseActivity.startNewActivity(getActivity(), UlduzumMerchantDetailsActivity.class, bundle);
                }
            }
            break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {
            mMapView.onResume();
        }
    }//onResume ends

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null) {
            mMapView.onPause();
        }
    }//onPause ends

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }
    }//onDestroy ends

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }//onLowMemory ends

    /*
     * Onclick Listener on marker
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        int selectedIndex = 0;

        if (marker.getTag() != null) {
            selectedIndex = Integer.parseInt((String) marker.getTag());
            if (serviceModel.getMaindatalist() == null && serviceModel.getMaindatalist().get(selectedIndex) == null)
                return false;
            currentMarker = selectedIndex;
            binding.markerLayout.setVisibility(View.VISIBLE);
            binding.tfLocationName.setText(serviceModel.getMaindatalist().get(selectedIndex).getName());
            binding.tfLocationAddress.setText(getString(R.string.ulduzum_location_lbl_details));
            String loyality = serviceModel.getMaindatalist().get(selectedIndex).getDiscount();
            if (loyality != null) {
                if (loyality.matches("[0-9]+")) {
                    binding.tfPercentage.setText(loyality + "%");
                    binding.tfPercentage.setVisibility(View.VISIBLE);
                    binding.giftPackIcon.setVisibility(View.GONE);
                } else {
                    binding.tfPercentage.setVisibility(View.GONE);
                    binding.giftPackIcon.setVisibility(View.VISIBLE);
                }

            }
            return false;
        }
        return false;
    }// onMarkerClick ends

}
