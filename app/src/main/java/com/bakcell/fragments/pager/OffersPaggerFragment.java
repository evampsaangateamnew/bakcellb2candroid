package com.bakcell.fragments.pager;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.adapters.CardAdapterSupplementaryOffers;
import com.bakcell.databinding.FragmentPaggerOffersBinding;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.OfferFilterListener;
import com.bakcell.interfaces.SearchResultListener;
import com.bakcell.models.supplementaryoffers.OfferFilter;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.widgets.RecyclerItemDecorator;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

/**
 * Created by Noman on 22-May-17.
 */

public class OffersPaggerFragment extends Fragment implements SearchResultListener {

    public static final String TAB_TITLE = "tab.title";
    public static final String TAB_OFFERS = "tab.offers";
    public static final String FOUNDED_OFFERS_ITEM_POS = "founded.offers.item.position";

    FragmentPaggerOffersBinding binding;
    private RecyclerView mRecylerview;

    private CardAdapterSupplementaryOffers adapterSupplementaryOffers;
    LinearLayoutManager layoutManager;

    private int prevCenterPos;

    private String pageTitle = "";
    private int foundOfferItemPos = -1;
    private ArrayList<SupplementaryOffer> supplementaryOffersList;

    private ArrayList<SupplementaryOffer> filteredSupplementaryOffersList = new ArrayList<SupplementaryOffer>();

    private SupplementaryOffersActivity.SearchListener searchListener;


    public static OffersPaggerFragment getInstance(int foundOfferItemPos, String tabTitle, ArrayList<SupplementaryOffer>
            supplementaryOffers) {
        OffersPaggerFragment fragment = new OffersPaggerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TAB_TITLE, tabTitle);
        bundle.putInt(FOUNDED_OFFERS_ITEM_POS, foundOfferItemPos);
        bundle.putParcelableArrayList(TAB_OFFERS, supplementaryOffers);
        fragment.setArguments(bundle);
        return fragment;
    }

    OfferFilterListener offerFilterListener = new OfferFilterListener() {
        @Override
        public void onFilterCall(boolean isSearch, String filter, ArrayList<OfferFilter> filteredIds, boolean isSearchedClosed, String offerGroupFilterAll) {
            if (adapterSupplementaryOffers == null) return;
            if (isSearch) { //performing search
                if (adapterSupplementaryOffers.getFilter() != null && Tools.hasValue(filter)) {
                    adapterSupplementaryOffers.getFilter().filter(filter);
                }
                if (adapterSupplementaryOffers.getItemCount() > 0) {
                    binding.tvPagination.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.tvPagination.setText(1 + "/" + adapterSupplementaryOffers.getItemCount());
                        }
                    }, 10);
                } else {
                    binding.tvPagination.setText(0 + "/" + 0);
                }
            } else if (!isSearch && !isSearchedClosed) { // performing the filtering
                filteredSupplementaryOffersList = new ArrayList<>();
                for (int i = 0; i < supplementaryOffersList.size(); i++) {
                    if (supplementaryOffersList.get(i).getHeader() != null) {

                        boolean isFilterExist = false;
                        if (filteredIds != null) {
                            for (int j = 0; j < filteredIds.size(); j++) {
                                if (Tools.hasValue(filteredIds.get(j).getKey())
                                        && Tools.hasValue(supplementaryOffersList.get(i).getHeader().getAppOfferFilter())) {
                                    if (supplementaryOffersList.get(i).getHeader().getAppOfferFilter().trim().toLowerCase()
                                            .contains(filteredIds.get(j).getKey().trim().toLowerCase())) {
                                        isFilterExist = true;
                                        break;
                                    }
                                }
                            }
                        }

                        // Check here if selected all and offer filter apply in offer group value
                        if (!isFilterExist && Tools.hasValue(offerGroupFilterAll)) {
                            if (supplementaryOffersList.get(i).getHeader().getOfferGroup() != null
                                    && Tools.hasValue(supplementaryOffersList.get(i).getHeader().getOfferGroup().getGroupValue()))
                                if (offerGroupFilterAll.toLowerCase()
                                        .contains(supplementaryOffersList.get(i).getHeader().getOfferGroup().getGroupValue().trim().toLowerCase())) {
                                    isFilterExist = true;
                                }
                        }

                        if (isFilterExist) {
                            filteredSupplementaryOffersList.add(supplementaryOffersList.get(i));
                        }


                    }
                }
                if (filteredSupplementaryOffersList.size() > 0) {
                    resultFound(true, R.string.no_package_found_for_filtered);
                    adapterSupplementaryOffers.updateAdapter(filteredSupplementaryOffersList);
                    binding.tvPagination.setText(1 + "/" + adapterSupplementaryOffers.getItemCount());
                    adapterSupplementaryOffers.notifyDataSetChanged();
                } else {
                    //
                    resultFound(false, R.string.no_package_found_for_filtered);
                }
            } else if (!isSearch && filteredIds == null) { // reseting the filter
                resultFound(true, R.string.no_results_found);
                if (supplementaryOffersList != null) { //Fixed NPE
                    adapterSupplementaryOffers.updateAdapter(supplementaryOffersList);
                    binding.tvPagination.setText(1 + "/" + adapterSupplementaryOffers.getItemCount());
                    adapterSupplementaryOffers.notifyDataSetChanged();
                }
            } else if (filteredIds.size() == 0) {
                resultFound(true, R.string.no_package_found_for_filtered);
                adapterSupplementaryOffers.updateAdapter(supplementaryOffersList);

                if (binding.mDiscreteView.getVisibility() == View.VISIBLE) {
                    binding.mDiscreteView.scrollToPosition(0);
                }

                if (binding.mRecylerview.getVisibility() == View.VISIBLE) {
                    binding.mRecylerview.scrollToPosition(0);
                }

                binding.tvPagination.setText(1 + "/" + adapterSupplementaryOffers.getItemCount());
                adapterSupplementaryOffers.notifyDataSetChanged();
            } else if (isSearchedClosed) { // hiding the search and setting original adapter back
                resultFound(true, R.string.no_offer_found);
                adapterSupplementaryOffers.updateAdapter(supplementaryOffersList);
                if (adapterSupplementaryOffers.getItemCount() > 0) {
                    binding.tvPagination.setText(1 + "/" + adapterSupplementaryOffers.getItemCount());
                } else {
                    binding.tvPagination.setText(0 + "/" + adapterSupplementaryOffers.getItemCount());
                }

            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(TAB_TITLE)) {
            pageTitle = getArguments().getString(TAB_TITLE);
        }
        if (getArguments().containsKey(FOUNDED_OFFERS_ITEM_POS)) {
            foundOfferItemPos = getArguments().getInt(FOUNDED_OFFERS_ITEM_POS);
        }
        if (getArguments().containsKey(TAB_OFFERS)) {
            supplementaryOffersList = getArguments().getParcelableArrayList(TAB_OFFERS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_offers, container, false);

        if (supplementaryOffersList == null) supplementaryOffersList = new ArrayList<>();

        // Check for data availability
        if (supplementaryOffersList.size() == 0) {
            binding.mainView.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            binding.noDataLayout.noDataFound.setText(getString(R.string.no_package_found));
            binding.noSearchLayout.setVisibility(View.INVISIBLE);
        } else {
            binding.mainView.setVisibility(View.VISIBLE);
            binding.noSearchLayout.setVisibility(View.INVISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
        }

        initUIContents();

        initSearchListener();


        if (getString(R.string.supplementary_offer_tab_title_call).equalsIgnoreCase(pageTitle)) {
            RootValues.getInstance().setOfferFilterListenerCall(offerFilterListener);
        } else if (getString(R.string.supplementary_offer_tab_title_internet).equalsIgnoreCase(pageTitle)) {
            RootValues.getInstance().setOfferFilterListenerInternet(offerFilterListener);
        } else if (getString(R.string.supplementary_offer_tab_title_sms).equalsIgnoreCase(pageTitle)) {
            RootValues.getInstance().setOfferFilterListenerSms(offerFilterListener);
        }
        //TODO Temporary Commented by client
//        else if (getString(R.string.supplementary_offer_tab_title_campaign).equalsIgnoreCase(pageTitle)) {
//            RootValues.getInstance().setOfferFilterListenerCampaign(offerFilterListener);
//        } else if (getString(R.string.supplementary_offer_tab_title_tm).equalsIgnoreCase(pageTitle)) {
//            RootValues.getInstance().setOfferFilterListenerTm(offerFilterListener);
//        }
        else if (getString(R.string.supplementary_offer_tab_title_hybrid).equalsIgnoreCase(pageTitle)) {
            RootValues.getInstance().setOfferFilterListenerHybrid(offerFilterListener);
        }


        return binding.getRoot();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (getUserVisibleHint()) {
            Logger.debugLog("Title", pageTitle);
        }


    }


    private void initSearchListener() {
        searchListener = new SupplementaryOffersActivity.SearchListener() {
            @Override
            public void onSearchEnded() {
                adapterSupplementaryOffers.updateAdapter(supplementaryOffersList);
            }
        };
    }


    private void initUIContents() {

        mRecylerview = binding.mRecylerview;

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setAutoMeasureEnabled(true);
        layoutManager.setOrientation(RootValues.getInstance().getOffersListOrientation());
        mRecylerview.setLayoutManager(layoutManager);
        mRecylerview.setHasFixedSize(true);

        setupPaggerView();

        //prepareOffersList(supplementaryOffersList);

    }


    public void setUpPagination(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int center = recyclerView.getWidth() / 2;
                View centerView = recyclerView.findChildViewUnder(center, recyclerView.getTop());
                int centerPos = recyclerView.getChildAdapterPosition(centerView);

                if (prevCenterPos != centerPos) {
                    // dehighlight the previously highlighted view
                    prevCenterPos = centerPos;
                }
                Logger.debugLog("POSTION", prevCenterPos + "");
                binding.tvPagination.setText(prevCenterPos + 1 + "/" +
                        recyclerView.getAdapter().getItemCount());
            }
        });
    }

    private void setupPaggerView() {
        if (supplementaryOffersList == null || supplementaryOffersList.size() == 0) return;

        boolean isInternet = false;
        if (getString(R.string.supplementary_offer_tab_title_internet).equalsIgnoreCase(pageTitle)) {
            isInternet = true;
        }

        if (RootValues.getInstance().getOffersListOrientation() == LinearLayoutManager.VERTICAL) {
            //binding.leftRightIndicatorLayout.setVisibility(View.GONE);
            binding.mDiscreteView.setVisibility(View.INVISIBLE);
            binding.mRecylerview.setVisibility(View.VISIBLE);

            adapterSupplementaryOffers = new CardAdapterSupplementaryOffers(getActivity(), getActivity(),
                    supplementaryOffersList, this, false, isInternet, false, pageTitle);
            mRecylerview.setAdapter(adapterSupplementaryOffers);
            setUpPagination(mRecylerview);

            if (foundOfferItemPos != -1) {
                binding.mRecylerview.scrollToPosition(foundOfferItemPos);
            }

        } else {
//         //This code make recylerview same like pagger view one item at a time on screen
            //binding.leftRightIndicatorLayout.setVisibility(View.VISIBLE);
            binding.mDiscreteView.setVisibility(View.VISIBLE);
            binding.mRecylerview.setVisibility(View.INVISIBLE);
            binding.mDiscreteView.setOrientation(Orientation.HORIZONTAL);
            binding.mDiscreteView.setItemTransformer(new ScaleTransformer.Builder()
                    .build());
            binding.mDiscreteView.addItemDecoration(new RecyclerItemDecorator());
            adapterSupplementaryOffers = new CardAdapterSupplementaryOffers(getActivity(), getActivity(),
                    supplementaryOffersList, this, true, isInternet, false, pageTitle);
            binding.mDiscreteView.setAdapter(adapterSupplementaryOffers);
            setUpPagination(binding.mDiscreteView);

            if (foundOfferItemPos != -1) {
                binding.mDiscreteView.scrollToPosition(foundOfferItemPos);
            }

        }

    }

    @Override
    public void onSearchResult(final boolean isResultFount) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                resultFound(isResultFount, R.string.no_results_found);

            }
        });

    }

    private void resultFound(boolean isResultFount, @StringRes int id) {
        if (isResultFount) {
            binding.noSearchLayout.setVisibility(View.INVISIBLE);
            binding.mainView.setVisibility(View.VISIBLE);
        } else {
            binding.mainView.setVisibility(View.INVISIBLE);
            binding.noSearchLayout.setVisibility(View.VISIBLE);
            binding.tvNoResultFound.setText(id);
        }
    }
}
