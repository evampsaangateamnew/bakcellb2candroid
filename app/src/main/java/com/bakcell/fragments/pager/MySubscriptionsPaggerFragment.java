package com.bakcell.fragments.pager;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.adapters.CardAdapterMySubscriptionsItem;
import com.bakcell.databinding.FragmentPaggerSubscriptionsBinding;
import com.bakcell.models.mysubscriptions.Subscriptions;

import java.util.ArrayList;

/**
 * Created by Noman on 22-May-17.
 */

public class MySubscriptionsPaggerFragment extends Fragment implements View.OnClickListener {

    public static final String TAB_TITLE = "tab.title";
    public static final String TAB_OFFERS = "tab.offers";
    public static final String DASHBOARD_USAGE_TYPE = "dashboard.usage.type";

    FragmentPaggerSubscriptionsBinding binding;

    private CardAdapterMySubscriptionsItem adapterMySubscriptionsItem;
    LinearLayoutManager layoutManager;
    private int prevCenterPos;

    private String pageTitle = "";
    private ArrayList<Subscriptions> subscriptionsArrayList;
    private boolean isInternet = false;
    private boolean isRoaming = false;

    private String dashboardUsageType = "";

    public static MySubscriptionsPaggerFragment getInstance(String tabTitle, ArrayList<Subscriptions>
            subscriptionsesLis, String dashboardUsageType) {
        MySubscriptionsPaggerFragment fragment = new MySubscriptionsPaggerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TAB_TITLE, tabTitle);
        bundle.putString(DASHBOARD_USAGE_TYPE, dashboardUsageType);
        bundle.putParcelableArrayList(TAB_OFFERS, subscriptionsesLis);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(TAB_TITLE)) {
            pageTitle = getArguments().getString(TAB_TITLE);
            if (getString(R.string.supplementary_offer_tab_title_internet)
                    .equalsIgnoreCase(pageTitle)) {
                isInternet = true;
            }
            if (getString(R.string.supplementary_offer_tab_title_roaming).equalsIgnoreCase(pageTitle)) {
                isRoaming = true;
            }
        }
        if (getArguments().containsKey(TAB_OFFERS)) {
            subscriptionsArrayList = getArguments().getParcelableArrayList(TAB_OFFERS);
        }
        // Dashboard usage type
        if (getArguments().containsKey(DASHBOARD_USAGE_TYPE)) {
            dashboardUsageType = getArguments().getString(DASHBOARD_USAGE_TYPE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_subscriptions, container, false);

        initUIContents();

        initUiListeners();

        return binding.getRoot();
    }

    private void initUiListeners() {
        binding.btnOffers.setOnClickListener(this);
    }


    private void initUIContents() {


        if (subscriptionsArrayList == null || subscriptionsArrayList.size() == 0) {
            showNoOffer(true);
            return;
        } else {
            showNoOffer(false);
        }

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setAutoMeasureEnabled(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.mRecylerview.setLayoutManager(layoutManager);
        binding.mRecylerview.setHasFixedSize(true);
        adapterMySubscriptionsItem = new CardAdapterMySubscriptionsItem(getActivity(),
                subscriptionsArrayList, isRoaming, isInternet, dashboardUsageType);
        binding.mRecylerview.setAdapter(adapterMySubscriptionsItem);
        binding.mRecylerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int center = binding.mRecylerview.getWidth() / 2;
                View centerView = binding.mRecylerview.findChildViewUnder(center, binding.mRecylerview.getTop());
                int centerPos = binding.mRecylerview.getChildAdapterPosition(centerView);

                if (prevCenterPos != centerPos) {
                    prevCenterPos = centerPos;
                }

                binding.tvPagination.setText(prevCenterPos + 1 + "/" + recyclerView.getAdapter().getItemCount());

            }
        });

    }

    public void showNoOffer(final boolean show) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        if (show) {
                            binding.noOfferLayout.setVisibility(View.VISIBLE);
                            binding.offerLayout.setVisibility(View.GONE);
                        } else {
                            binding.offerLayout.setVisibility(View.VISIBLE);
                            binding.noOfferLayout.setVisibility(View.GONE);
                        }
                    }
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_offers:
                Bundle bundle = new Bundle();
                bundle.putString(SupplementaryOffersActivity.TAB_NAVIGATION_KEY, pageTitle);
                BaseActivity.startNewActivity(getActivity(), SupplementaryOffersActivity.class, bundle);
                break;
        }
    }
}
