package com.bakcell.fragments.pager;

import android.app.DatePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.DatePicker;

import com.bakcell.R;
import com.bakcell.adapters.CardAdapterPaymentRequest;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentPaggerPaymentHistoryBinding;
import com.bakcell.fragments.dialogs.DatePickerFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.loan.PaymentHistory;
import com.bakcell.models.loan.PaymentHistoryMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestPaymentHistoryService;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;


/**
 * Created by Noman on 6/9/2017.
 */

public class PaymentHistoryPaggerFragment extends Fragment implements View.OnClickListener {

    FragmentPaggerPaymentHistoryBinding binding;

    protected View view;

    private ArrayList<PaymentHistory> paymentHistoryArrayList;

    private CardAdapterPaymentRequest cardAdapterPaymentRequest;

    private String startDate = "", endDate = "", date = "";

    private boolean flag = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_payment_history, container, false);

        initUiContent();

        initUiListeners();

//        try {
//            date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_NINETY_DAY);
//            updateDates(date, Tools.getCurrentDate());
//        } catch (ParseException e) {
//            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
//        }
        ;

        AppEventLogs.applyAppEvent(AppEventLogValues.LoanEvents.LOAN_SCREEN,
                AppEventLogValues.LoanEvents.LOAN_PAYMENT_HISTORY_SCREEN,
                AppEventLogValues.LoanEvents.LOAN_SCREEN);

        return binding.getRoot();
    }

    private void updateDates(String newStartDate, String newEndDate) {

        startDate = newStartDate;
        endDate = newEndDate;
        requestForGetPaymentHistory(startDate, endDate);
    }


    private void initUiListeners() {
        binding.spinnerPeriodButton.setOnClickListener(this);
        binding.spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    try {
                        updateDates(Tools.getCurrentDate(), Tools.getCurrentDate());
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 1) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(),
                                Constants.DateAndMonth.LAST_SEVEN_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                } else if (position == 2) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(),
                                Constants.DateAndMonth.LAST_THIRTY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 3) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(),
                                Constants.DateAndMonth.LAST_NINETY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 5) {
                    showDatePicker();
                    binding.spinnerPeriod.setSelection(0);
                } else if (position == 6) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), (365 * 3));
                        updateDates(date, Tools.getCurrentDate());
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });


    }

    private void initUiContent() {

        binding.itemCounterText.setText("0/0");

        paymentHistoryArrayList = new ArrayList<>();
        cardAdapterPaymentRequest = new CardAdapterPaymentRequest(paymentHistoryArrayList,
                getActivity());
        binding.lvPaymentRequest.setAdapter(cardAdapterPaymentRequest);

        SpinnerAdapterOperationHistoryFilter spinnerAdapterPeriodFilter = new
                SpinnerAdapterOperationHistoryFilter(getActivity(),
                getResources().getStringArray(R.array.period_filter_rq_history));
        binding.spinnerPeriod.setAdapter(spinnerAdapterPeriodFilter);

        binding.lvPaymentRequest.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (cardAdapterPaymentRequest != null && cardAdapterPaymentRequest.getCount() > 0) {
                    binding.itemCounterText.setText((firstVisibleItem + visibleItemCount) + "/" + totalItemCount);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.spinnerPeriodButton:
                binding.spinnerPeriod.performClick();
                break;
        }
    }


    public void requestForGetPaymentHistory(String startDate, String endDate) {


        RequestPaymentHistoryService.newInstance(getActivity(), ServiceIDs.REQUEST_PAYMENT_HISTORY).execute(DataManager.getInstance().getCurrentUser(),
                startDate, endDate, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        PaymentHistoryMain historyMain = new Gson().fromJson(result, PaymentHistoryMain.class);

                        if (historyMain != null && historyMain.getPaymentHistory() != null) {
                            loadPaymentHistoryList(historyMain.getPaymentHistory());
                        }//end of if

                    }//end of onSuccess


                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }//end of if
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }//end of onError

                });

    }

    /**
     * load payment history list to UI
     *
     * @param paymentHistoryList
     */
    private void loadPaymentHistoryList(ArrayList<PaymentHistory> paymentHistoryList) {

        if (paymentHistoryList != null && paymentHistoryList.size() > 0) {
            binding.lvPaymentRequest.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            cardAdapterPaymentRequest = new CardAdapterPaymentRequest(paymentHistoryList, getActivity());
            binding.lvPaymentRequest.setAdapter(cardAdapterPaymentRequest);
        } else {
            binding.lvPaymentRequest.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            binding.itemCounterText.setText("0/0");
        }


    }


    private void showDatePicker() {

        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getActivity().getSupportFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            //it will return index of Month for june it will return 05 +1 will make it 06
            monthOfYear++;
            String dateNew = String.valueOf(year) + "-" + String.format("%02d", monthOfYear)
                    + "-" + String.format("%02d", dayOfMonth);

            updateDates(dateNew, dateNew);
        }
    };


}
