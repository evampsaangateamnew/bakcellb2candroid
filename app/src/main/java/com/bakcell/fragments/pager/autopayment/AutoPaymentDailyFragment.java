package com.bakcell.fragments.pager.autopayment;

import static com.bakcell.globals.BakcellPopUpDialog.showMessageDialog;
import static com.bakcell.globals.BakcellPopUpDialog.showMessageDialogWithOKCallback;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.AutoPaymentCardsAdapter;
import com.bakcell.databinding.FragmentAutoPaymentDailyBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.interfaces.OnOkClickListener;
import com.bakcell.interfaces.SaveCardsItemClickListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.topup.GetSavedCardsData;
import com.bakcell.models.topup.GetSavedCardsItems;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.DecimalDigitsInputFilter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.AddPaymentScheduler;
import com.bakcell.webservices.RequestGetSavedCards;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellCalendar;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class AutoPaymentDailyFragment extends Fragment {
    FragmentAutoPaymentDailyBinding binding;
    private List<GetSavedCardsItems> savedCards;
    private String savedCardId = "";
    private String dateForAPI = "";
    private int day, month, year;

    //create Listener to Date Picker
    DatePickerDialog.OnDateSetListener onDateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
        //create calendar instance and set date information which are picked by Date picker
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        //Months are indexed from 0 not 1 , so we add 1
        monthOfYear = monthOfYear + 1;
        if (String.valueOf(monthOfYear).length() == 1) {
            dateForAPI = year + "-0" + monthOfYear + "-" + dayOfMonth;
        } else {
            dateForAPI = year + "-" + monthOfYear + "-" + dayOfMonth;
        }


        //format date into require output format
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT_DAY_NAME, Locale.US);
        String formattedDate = sdf.format(c.getTime());
        //update textView with new selected date
        updateDateTextView(formattedDate);
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_auto_payment_daily, container, false);

        initUiContents();

        initUIListener();


        return binding.getRoot();

    } //onCreateView ends

    private void initUIListener() {
        binding.editTextAmountInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {

                String value = text.toString();
                String[] valueSplits = value.split("\\.");

                if (value.length() <= 2 && valueSplits.length >= 1 && Tools.hasValue(valueSplits[0]) && valueSplits[0].equals("0")) {
                    binding.editTextAmountInput.setText("");
                } else if (value.startsWith(".")) {
                    binding.editTextAmountInput.setText("");
                }

                binding.editTextAmountInput.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 2),});

            }
        });
        binding.llCalendar.setOnClickListener(view -> showCalendarDatePicker(year, month, day));
        binding.btnScheduleCard.setOnClickListener(view -> {
            String amount = binding.editTextAmountInput.getText().toString();
            String recurrence = binding.editTextRecurrence.getText().toString();
            try {
                if (!Tools.hasValue(recurrence) || Double.parseDouble(recurrence) <= 0) {
                    showMessageDialog(getActivity(),
                            getString(R.string.bakcell_attention_title), getString(R.string.auto_payment_invalid_recurrence_alert));
                    return;
                }
            } catch (Exception exception) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, exception.getMessage());
            }

            if (savedCards == null || savedCards.size() == 0) {
                showMessageDialog(getActivity(),
                        getString(R.string.bakcell_error_title), getString(R.string.auto_payment_no_saved_cards_alert));
                return;
            }
            if (savedCards != null && savedCards.size() > 0 && isValidAmount(amount) && Tools.hasValue(recurrence) && savedCardId != null && !savedCardId.isEmpty()) {
                requestSchedulePayment(amount, recurrence, dateForAPI, savedCardId, Constants.AutoPaymentConstants.BILLING_CYCLE_DAILY);
            }
        });
    } //initUIListener ends

    private void requestSchedulePayment(String amount, String recurrence, String startDate, String savedCardId, String billingCycle) {

        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            AddPaymentScheduler.newInstance(getActivity(), ServiceIDs.REQUEST_ADD_PAYMENT_SCHEDULER)
                    .execute(userModel, amount, billingCycle, startDate, savedCardId, recurrence, "1", new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            String responseDescription = "";
                            if (!getActivity().isFinishing() && response != null &&
                                    Tools.hasValue(response.getDescription())) {
                                responseDescription = response.getDescription();
                            }

                            if (!getActivity().isFinishing() && response != null) {
                                showMessageDialogWithOKCallback(getActivity(),
                                        getString(R.string.app_name), responseDescription, new OnOkClickListener() {
                                            @Override
                                            public void onOkClick() {
                                                if (requireActivity() != null) {
                                                    requireActivity().finish();
                                                }
                                            }
                                        });
                            }

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!getActivity().isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(getActivity());
                                return;
                            }

                            if (!getActivity().isFinishing() && response != null) {
                                showMessageDialog(getActivity(),
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!getActivity().isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(getActivity());
                            }

                        }
                    });

        }

    }

    private boolean isValidAmount(String amount) {
        boolean isValid = true;
        if ((!Tools.hasValue(amount) || !Tools.isNumeric(amount))) {
            BakcellPopUpDialog.showMessageDialogWithOKCallback(requireActivity(),
                    getString(R.string.bakcell_error_title),
                    getString(R.string.lbl_auto_payment_enter_amount), new OnOkClickListener() {
                        @Override
                        public void onOkClick() {
                        }
                    });

            return isValid = false;
        } else if (Double.parseDouble(amount) < 1) {
            BakcellPopUpDialog.showMessageDialogWithOKCallback(requireActivity(),
                    getString(R.string.bakcell_error_title),
                    getString(R.string.error_msg_invalid_amount), new OnOkClickListener() {
                        @Override
                        public void onOkClick() {
                        }
                    });

            isValid = false;
        }

        return isValid;
    }

    private void initUiContents() {
        updateDateTextView(getCurrentDate());
        //RequestSavedCards
        requestSavedCards();
    } // initUiContents ends

    private void setUpCurrentDate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT_DAY_NAME, Locale.US);
        String formattedDate = sdf.format(date.getTime());
        //update textView with new selected date
        updateDateTextView(formattedDate);
    }

    private void requestSavedCards() {
        // Check if user not NULL
        if (DataManager.getInstance().getCurrentUser() != null && getActivity() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();
            RequestGetSavedCards.newInstance(getActivity(), ServiceIDs.REQUEST_GET_SAVED_CARDS)
                    .execute(userModel, false, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                            String responseDescription = "";
                            if (getActivity() == null) {
                                return;
                            }
                            if (!getActivity().isFinishing() && response != null &&
                                    Tools.hasValue(response.getDescription())) {
                                responseDescription = response.getDescription();
                            }

                            if (!getActivity().isFinishing() && response != null) {
                                String data = response.getData();
                                Gson gson = new Gson();
                                GetSavedCardsData getSavedCardsData = gson.fromJson(data, GetSavedCardsData.class);

                                savedCards = getSavedCardsData.getData();
                                if (savedCards != null && savedCards.size() > 0) {
                                    setupRecentVisaCardRecyclerView();

                                } else {
                                    binding.recyclerviewRecentVisa.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                }
                            } else {
                                showMessageDialog(getActivity(),
                                        getString(R.string.bakcell_error_title), responseDescription);
                            }


                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (!getActivity().isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                HomeActivity.logoutUser(getActivity());
                                return;
                            }
                            if (!getActivity().isFinishing() && binding != null && binding.recyclerviewRecentVisa != null && binding.noDataFoundLayout != null) {
                                binding.recyclerviewRecentVisa.setVisibility(View.GONE);
                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                            }
                            if (!getActivity().isFinishing() && response != null) {
                                showMessageDialog(getActivity(),
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            if (!getActivity().isFinishing()) {
                                BakcellPopUpDialog.ApiFailureMessage(getActivity());
                            }
                            if (!getActivity().isFinishing() && binding != null && binding.recyclerviewRecentVisa != null && binding.noDataFoundLayout != null) {
                                binding.recyclerviewRecentVisa.setVisibility(View.GONE);
                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                            }

                        }
                    });

        }
    } // requestSavedCards ends

    //Setup recycleView for recent cards
    private void setupRecentVisaCardRecyclerView() {
        if (savedCards != null && savedCards.size() > 0) {

            AutoPaymentCardsAdapter autoPaymentCardsAdapter = new AutoPaymentCardsAdapter(savedCards, getActivity(), new SaveCardsItemClickListener() {
                @Override
                public void onItemClick(GetSavedCardsItems savedCardsItems, int position) {
                    if (savedCardsItems != null && Tools.hasValue(savedCardsItems.getPaymentKey())) {
                        savedCardId = savedCardsItems.getId();

                    }
                }
            });
            if (savedCards.get(0) != null && savedCards.get(0).getPaymentKey() != null) {
                savedCardId = savedCards.get(0).getId();
            }
            binding.recyclerviewRecentVisa.setAdapter(autoPaymentCardsAdapter);
            binding.recyclerviewRecentVisa.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

    } // end setupRecentVisaCardRecyclerView


    //show date picker dialog
    void showCalendarDatePicker(int year, int month, int day) {
        //return if activity is null or isFinishing true
        if (getActivity() == null || getActivity().isFinishing()) return;

        //get current date from Calendar
        final Calendar calendar = Calendar.getInstance();
        /*int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);*/

        // create custom Calendar class object & add OnDateSetListener to it.
        BakcellCalendar bakcellCalendar = new BakcellCalendar(binding.startDate, onDateSetListener);

        // create DatePickerDialog object
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), bakcellCalendar,
                year, month, day);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            datePickerDialog.setTitle("");//Prevent Date picker from creating extra Title.!
        }
        //restrict user to only select from current day onwards
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    } // showCalendar Ends

    //update start date text ui
    private void updateDateTextView(String dateNew) {
        if (getActivity() == null || getActivity().isFinishing()) return;
        binding.startDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_gray));
        binding.startDate.setText(dateNew);
    } // updateDates ends

    private String getCurrentDate() {
        final Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT_DAY_NAME, Locale.US);
        String formattedDate = sdf.format(calendar.getTime());
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        int monthForAPI = month + 1;
        if (String.valueOf(monthForAPI).length() == 1) {
            dateForAPI = year + "-0" + monthForAPI + "-" + day;
        } else {
            dateForAPI = year + "-" + monthForAPI + "-" + day;
        }
        return formattedDate;

    }
}