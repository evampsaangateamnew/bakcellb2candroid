package com.bakcell.fragments.pager;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.SelectAmountAdapter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentPaggerLoanBinding;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.topup.Topup;
import com.bakcell.models.user.UserPredefinedData;
import com.bakcell.models.user.userpredefinedobjects.PredefinedDataItem;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestLoanService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Zeeshan Shabbir on 6/9/2017.
 */

public class LoanPaggerFragment extends Fragment implements View.OnClickListener {

    FragmentPaggerLoanBinding binding;

    String[] spinnerAmout;

    private double loadAmount = -1;

    SelectAmountAdapter spinnerAdapaterLoan;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_loan, container, false);

        initUiContent();

        initListener();

        return binding.getRoot();
    }

    private void inAppFeedback(Topup topup) {
        int currentVisit = PrefUtils.getAsInt(requireContext(), PrefUtils.PreKeywords.PREF_LOAN_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(requireContext());
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.LOAN_PAGE);
            if (surveys != null) {
                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                        InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(requireContext());
                        if (surveys.getQuestions() != null) {
                            currentVisit = 0;
                            String message = getString(R.string.msg_get_load_success, loadAmount + "");
                            inAppFeedbackDialog.showMessageWithBalanceDialog(surveys, topup.getNewBalance(), message);
                        } else {
                            showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
                        }
                    } else {
                        showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
                    }
                } else {
                    showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
                }
            } else {
                showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
            }
        } else {
            showSuccessfulDialog(topup.getNewBalance(), topup.getNewCredit());
        }
        PrefUtils.addInt(requireContext(), PrefUtils.PreKeywords.PREF_LOAN_PAGE, currentVisit);
    }

    private void initUiContent() {

        binding.currentBalance.setText(RootValues.getInstance().getLoanCredit());

        populateArrayForSpinner();

        if (spinnerAmout != null) {
            spinnerAdapaterLoan = new SelectAmountAdapter(getActivity(), spinnerAmout);
            binding.spSelectAmount.setAdapter(spinnerAdapaterLoan);
        }

        if (spinnerAmout != null && spinnerAmout.length == 2) {
            binding.spSelectAmount.setSelection(1);
            binding.spSelectAmount.setEnabled(false);
        } else {
            binding.spSelectAmount.setSelection(0);
            binding.spSelectAmount.setEnabled(true);
        }

    }

    private void initListener() {
        binding.btnGetCredit.setOnClickListener(this);
        binding.btnGetCredit.setSelected(true);
        binding.spSelectAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerAdapaterLoan.setLastSelectedPos(position);
                spinnerAdapaterLoan.notifyDataSetChanged();
                spinnerAdapaterLoan.setOpened(false);

                loadAmount = -1;
                if (position != 0) selectAmount(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spSelectAmount.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                spinnerAdapaterLoan.setOpened(true);
                return false;
            }
        });
    }


    private void populateArrayForSpinner() {
        if (DataManager.getInstance().getUserPredefinedData() != null) {
            UserPredefinedData userPredefinedData = DataManager.getInstance().getUserPredefinedData();
            if (userPredefinedData.getTopup() != null &&
                    userPredefinedData.getTopup().getGetLoan() != null &&
                    userPredefinedData.getTopup().getGetLoan().getSelectAmount() != null) {

                ArrayList<PredefinedDataItem> selectAmountList = new ArrayList<>();

                if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null &&
                        DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
                    selectAmountList = userPredefinedData.getTopup().getGetLoan().getSelectAmount().getCin();
                } else {
                    selectAmountList = userPredefinedData.getTopup().getGetLoan().getSelectAmount().getKlass();
                }

                if (selectAmountList != null && selectAmountList.size() > 0) {
                    spinnerAmout = new String[selectAmountList.size() + 1];
                    spinnerAmout[0] = getString(R.string.spinner_label_select_all);
                    for (int i = 0; i < selectAmountList.size(); i++) {
                        spinnerAmout[i + 1] = selectAmountList.get(i).getAmount();
                    }
                }

            }
        }
    }


    private void selectAmount(int position) {

        if (position < 1 && spinnerAmout.length > position) {
            BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), getString(R.string.msg_select_amount));
            return;
        }

        String amountStr = spinnerAmout[position];

        if (Tools.hasValue(amountStr) && Tools.isNumeric(amountStr)) {
            loadAmount = Tools.getDoubleFromString(amountStr);
        }

    }


    public void confirmationGetCredit(final double loadAmount) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_top_up_confirmation, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button yesBtn = (Button) dialogView.findViewById(R.id.btn_yes);

        BakcellTextViewNormal currentBalance = (BakcellTextViewNormal) dialogView.findViewById(R.id.currentBalance);

        currentBalance.setText(DataManager.getInstance().getCurrentBalance() + "");

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loadAmount > -1) {
                    requestForGetLoad(loadAmount);
                }
                alertDialog.dismiss();
            }
        });
        TextView textView = (TextView) dialogView.findViewById(R.id.tv_top_confirmation_msg);

        String text = getResources().getString(R.string.msg_confirmation_getloan, this.loadAmount + "");

        textView.setText(text);

        Button noBtn = (Button) dialogView.findViewById(R.id.btn_no);
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }


    public void requestForGetLoad(double loanAmount) {


        RequestLoanService.newInstance(getActivity(), ServiceIDs.GET_LOAD).execute(DataManager.getInstance().getCurrentUser(), loanAmount + "", new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                String result = response.getData();

                Topup topup = new Gson().fromJson(result, Topup.class);

                if (topup != null) {
                    inAppFeedback(topup);
                }

                RootValues.getInstance().setDashboardApiCall(true);

                AppEventLogs.applyAppEvent(AppEventLogValues.LoanEvents.LOAN_SCREEN,
                        AppEventLogValues.LoanEvents.LOAN_SUCCESS,
                        AppEventLogValues.LoanEvents.LOAN_SCREEN);
            }


            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                // Check if user logout by server
                if (getActivity() != null && !getActivity().isFinishing() &&
                        response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                    HomeActivity.logoutUser(getActivity());
                    return;
                }

                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                            getString(R.string.bakcell_error_title), response.getDescription());
                }

                AppEventLogs.applyAppEvent(AppEventLogValues.LoanEvents.LOAN_SCREEN,
                        AppEventLogValues.LoanEvents.LOAN_FAILURE,
                        AppEventLogValues.LoanEvents.LOAN_SCREEN);
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                }
            }

        });

    }

    private void showSuccessfulDialog(String newBalance, String newCredit) {

        try {
            if (getActivity() != null && !getActivity().isFinishing()) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_top_up_successful, null);
                dialogBuilder.setView(dialogView);

                RootValues.getInstance().setLoanCredit(newCredit);
                binding.currentBalance.setText(RootValues.getInstance().getLoanCredit());

                DataManager.getInstance().setNewBalance(newBalance);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                if (alertDialog.getWindow() != null) {
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                Button okayBtn = (Button) dialogView.findViewById(R.id.btn_okay);
                okayBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                BakcellTextViewNormal textView = (BakcellTextViewNormal) dialogView.findViewById(R.id.tv_top_confirmation_msg);
                BakcellTextViewNormal tvBalnce = (BakcellTextViewNormal) dialogView.findViewById(R.id.tvBalnce);
                textView.setText(getString(R.string.msg_get_load_success, loadAmount + ""));

                if (Tools.hasValue(newBalance)) {
                    tvBalnce.setText(newBalance);
                } else {
                    tvBalnce.setText("");
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_get_credit:

                if (((int) loadAmount) == -1) {
                    BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), getString(R.string.msg_select_amount));
                    return;
                }

                confirmationGetCredit(loadAmount);

                break;
        }
    }

}
