package com.bakcell.fragments.pager;

import android.app.DatePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.ExpandableAdapterUsageHistorySummary;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentPaggerUsageHistorySummaryBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.usagehistory.summary.UsageHistorySummaryMain;
import com.bakcell.models.usagehistory.summary.UsageSummary;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestUsageHistorySummaryService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellCalendar;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 22-May-17.
 */

public class UsageHistorySummaryPaggerFragment extends Fragment implements View.OnClickListener {
    FragmentPaggerUsageHistorySummaryBinding binding;

    private ExpandableAdapterUsageHistorySummary historySummaryAdapter;

    private SpinnerAdapterOperationHistoryFilter spinnerAdapterPeriodFilter;

    private String startDate = "", endDate = "", date = "";
    String dateOne = "";
    String dateTwo = "";
    boolean flagDate = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_usage_history_summary, container, false);

        initUIContents();

        initUIListener();

        try {
            setDefaultDate();
            date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.CURRENT_DAY);
            updateDates(date, Tools.getCurrentDate());
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN,
                AppEventLogValues.AccountEvents.USAGE_HISTORY_SUMMARY_SCREEN,
                AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN);

        return binding.getRoot();
    }

    private void initUIContents() {


        spinnerAdapterPeriodFilter = new SpinnerAdapterOperationHistoryFilter(getActivity(), getResources().getStringArray(R.array.period_filter));
        binding.spinnerPeriod.setAdapter(spinnerAdapterPeriodFilter);

        ArrayList<UsageSummary> usageSummaryMainArrayList = new ArrayList<>();
        historySummaryAdapter = new ExpandableAdapterUsageHistorySummary(getActivity(), usageSummaryMainArrayList);
        binding.expandableListview.setAdapter(historySummaryAdapter);

    }

    private void initUIListener() {
        binding.spinnerPeriodButton.setOnClickListener(this);
        binding.tvDateOne.setOnClickListener(this);
        binding.tvDateTwo.setOnClickListener(this);
        binding.expandableListview.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                if (parent.isGroupExpanded(groupPosition)) {
                    ((ImageView) v.findViewById(R.id.plus_icon_img)).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.plus_gray_normal));
                } else {
                    ((ImageView) v.findViewById(R.id.plus_icon_img)).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.minus_gray_normal));
                }
                return false;
            }
        });
        binding.spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    if (flagDate) {
                        try {
                            binding.llCalendar.setVisibility(View.GONE);
                            updateDates(Tools.getCurrentDate(), Tools.getCurrentDate());

                        } catch (ParseException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                    } else {
                        binding.llCalendar.setVisibility(View.GONE);
                        flagDate = !flagDate;
                    }
                } else if (position == 1) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_SEVEN_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 2) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_THIRTY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 3) {
                    binding.llCalendar.setVisibility(View.GONE);
                    date = "";
                    String maxDate = "";
                    try {
                        date = Tools.getLastMonthFirstDate();
                        maxDate = Tools.getLastMonthLastDate();

                        updateDates(date, maxDate);

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                } else if (position == 4) {
                    try {
                        setDefaultDate();
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                    binding.llCalendar.setVisibility(View.VISIBLE);
                } else {
                    binding.llCalendar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void updateDates(String newStartDate, String newEndDate) {

        startDate = newStartDate;
        endDate = newEndDate;
        requestForGetUsageSummaryHistory(startDate, endDate);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.spinnerPeriodButton:
                binding.spinnerPeriod.performClick();
                break;
            case R.id.tv_date_one:
                try {
                    showCalendar(binding.tvDateOne, true);//false for max date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
            case R.id.tv_date_two:
                try {
                    showCalendar(binding.tvDateTwo, false);//true for min date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;

        }
    }


    public void requestForGetUsageSummaryHistory(String startDate, String endDate) {


        RequestUsageHistorySummaryService.newInstance(getActivity(), ServiceIDs.REQUEST_USAGE_HISTORY_SUMMARY)
                .execute(DataManager.getInstance().getCurrentUser(),
                        startDate, endDate, new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                String result = response.getData();

                                UsageHistorySummaryMain usageHistorySummaryMain = new Gson().fromJson(result, UsageHistorySummaryMain.class);

                                updateUIUsageSummary(usageHistorySummaryMain);

                            }//end of onSuccess


                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (getActivity() != null && !getActivity().isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                    return;
                                }

                                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                                    binding.expandableListview.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }//end of if
                            }//end of onFailure

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    binding.expandableListview.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                                }
                            }//end of onError
                        });
    }

    private void updateUIUsageSummary(UsageHistorySummaryMain usageHistorySummaryMain) {
        if (usageHistorySummaryMain == null) return;

        if (historySummaryAdapter != null && usageHistorySummaryMain.getSummaryList() != null) {

            historySummaryAdapter.updateList(usageHistorySummaryMain.getSummaryList());
            binding.expandableListview.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
        } else {
            binding.expandableListview.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }

    }

    public void showCalendar(BakcellTextViewNormal textViewNormal, boolean isStartDateSelected) throws ParseException {

        if (getActivity() == null) return;
        if (getActivity().isFinishing()) return;

        try {
            dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());

            Date minDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
            Date maxDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);

            BakcellCalendar bakcellCalendar;

            final Calendar calendar = Calendar.getInstance();
            if (isStartDateSelected) {
                calendar.setTime(minDate);
            } else {
                calendar.setTime(maxDate);
            }
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            bakcellCalendar = new BakcellCalendar(textViewNormal, ondate);


            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), bakcellCalendar,
                    year, month, day);
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {

                Calendar calndPre = Calendar.getInstance();
                calndPre.add(Calendar.MONTH, -3);
                calndPre.set(Calendar.DATE, 1);
                datePickerDialog.getDatePicker().setMinDate(calndPre.getTimeInMillis());

                Date dateMax = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);
                calendar.setTime(dateMax);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());

            } else {

                Date dateMin = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
                calendar.setTime(dateMin);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("");//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show();
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            //it will return index of Month for june it will return 05 +1 will make it 06
            //   monthOfYear++;
//            String dateNew = String.valueOf(year) + "-" + String.format("%02d", monthOfYear)
//                    + "-" + String.format("%02d", dayOfMonth);
            try {
                dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            try {
                dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            updateDates(dateOne, dateTwo);
        }
    };

    private void setDefaultDate() throws ParseException {
        binding.tvDateOne.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
        binding.tvDateTwo.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
    }
}

