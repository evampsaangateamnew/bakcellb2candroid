package com.bakcell.fragments.pager;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.bakcell.R;
import com.bakcell.adapters.ExpandableAdapterStoreLocator;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentStoreLocatorListBinding;
import com.bakcell.fragments.menus.StoreLocatorFragment;
import com.bakcell.models.storelocator.StoreLocator;
import com.bakcell.models.storelocator.StoreLocatorMain;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;

import static com.bakcell.fragments.pager.StoreLocatorMapPaggerFragment.FILTER_ALL_AZ;
import static com.bakcell.fragments.pager.StoreLocatorMapPaggerFragment.FILTER_ALL_EN;
import static com.bakcell.fragments.pager.StoreLocatorMapPaggerFragment.FILTER_ALL_RU;


/**
 * Created by Noman on 6/7/2017.
 */

public class StoreLocatorListPaggerFragment extends Fragment implements View.OnClickListener {

    FragmentStoreLocatorListBinding binding;

    private ExpandableAdapterStoreLocator expandableAdapterStoreLocator;

    SpinnerAdapterOperationHistoryFilter spinnerAdapterCities;
    SpinnerAdapterOperationHistoryFilter spinnerAdapterServiceCenters;

    private StoreLocatorMain storeLocatorMain;

    public static StoreLocatorListPaggerFragment getInstance(StoreLocatorMain storeLocatorMain) {
        StoreLocatorListPaggerFragment fragment = new StoreLocatorListPaggerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(StoreLocatorFragment.KEY_STORE_LOCATOR, storeLocatorMain);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(StoreLocatorFragment.KEY_STORE_LOCATOR)) {
            storeLocatorMain = getArguments().getParcelable(StoreLocatorFragment.KEY_STORE_LOCATOR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_store_locator_list, container, false);
        if (storeLocatorMain == null) return binding.getRoot();

        initUiContent();

        initUiListener();

        initStoreList();

        AppEventLogs.applyAppEvent(AppEventLogValues.StoreLocatorEvents.STORE_LOCATOR_LIST,
                AppEventLogValues.StoreLocatorEvents.STORE_LOCATOR_LIST,
                AppEventLogValues.StoreLocatorEvents.STORE_LOCATOR_LIST);

        return binding.getRoot();
    }


    private void initUiContent() {
        if (storeLocatorMain != null && storeLocatorMain.getCity() != null && storeLocatorMain.getCity().size() > 0) {
            spinnerAdapterCities = new SpinnerAdapterOperationHistoryFilter(getActivity(), storeLocatorMain.getCity().toArray(new String[0]));
            binding.spStoreLocatorCities.setAdapter(spinnerAdapterCities);
        }

        if (storeLocatorMain != null && storeLocatorMain.getType() != null && storeLocatorMain.getType().size() > 0) {
            spinnerAdapterServiceCenters = new SpinnerAdapterOperationHistoryFilter(getActivity(), storeLocatorMain.getType().toArray(new String[0]));
            binding.spStoreLocatorServices.setAdapter(spinnerAdapterServiceCenters);
        }

    }

    private void initStoreList() {

        if (storeLocatorMain.getStores() != null && storeLocatorMain.getStores().size() > 0) {
            binding.expandableListStoreLocator.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            expandableAdapterStoreLocator = new ExpandableAdapterStoreLocator(getActivity(), storeLocatorMain.getStores());
            binding.expandableListStoreLocator.setAdapter(expandableAdapterStoreLocator);
        } else {
            binding.expandableListStoreLocator.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }

    }


    private void initUiListener() {
        binding.ivCitiesDropDown.setOnClickListener(this);
        binding.ivServiceCenterDropDown.setOnClickListener(this);

        binding.spStoreLocatorCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String type = "";
                String city = "";
                if (!binding.spStoreLocatorServices.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorServices.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorServices.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    type = binding.spStoreLocatorServices.getSelectedItem().toString();
                }

                if (!binding.spStoreLocatorCities.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorCities.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorCities.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    city = binding.spStoreLocatorCities.getAdapter().getItem(position).toString();
                }
                filterLocationsOnService(type, city);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.spStoreLocatorServices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String city = "";
                String service = "";
                if (!binding.spStoreLocatorCities.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorCities.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorCities.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    city = binding.spStoreLocatorCities.getSelectedItem().toString();
                }
                if (!binding.spStoreLocatorServices.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorServices.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorServices.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    service = binding.spStoreLocatorServices.getAdapter().getItem(position).toString();
                }
                filterLocationsOnService(service, city);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void filterLocationsOnService(String service, String city) {
        ArrayList<StoreLocator> cityFilteredStores = new ArrayList<StoreLocator>();
        if (storeLocatorMain != null && storeLocatorMain.getStores() != null) {
            if (Tools.hasValue(city)) {
                for (int i = 0; i < storeLocatorMain.getStores().size(); i++) {
                    if (Tools.hasValue(storeLocatorMain.getStores().get(i).getCity())) {
                        if (storeLocatorMain.getStores().get(i).getCity().equalsIgnoreCase(city)) {
                            cityFilteredStores.add(storeLocatorMain.getStores().get(i));
                        }
                    }
                }
            } else {
                cityFilteredStores.addAll(storeLocatorMain.getStores());
            }
            ArrayList<StoreLocator> typeAndCityFilteredStores = new ArrayList<StoreLocator>();
            if (Tools.hasValue(service)) {
                if (cityFilteredStores.size() > 0) {
                    for (int i = 0; i < cityFilteredStores.size(); i++) {
                        if (Tools.hasValue(cityFilteredStores.get(i).getType())) {
                            if (cityFilteredStores.get(i).getType().equalsIgnoreCase(service)) {
                                typeAndCityFilteredStores.add(cityFilteredStores.get(i));
                            }
                        }
                    }
                }
            } else {
                typeAndCityFilteredStores.addAll(cityFilteredStores);
            }
            expandableAdapterStoreLocator.updataData(typeAndCityFilteredStores);
        }

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_cities_drop_down:
                binding.spStoreLocatorCities.performClick();
                break;
            case R.id.iv_service_center_drop_down:
                binding.spStoreLocatorServices.performClick();
                break;
        }
    }


}
