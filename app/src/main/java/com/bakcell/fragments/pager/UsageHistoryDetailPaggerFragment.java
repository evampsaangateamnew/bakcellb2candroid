package com.bakcell.fragments.pager;

import android.app.DatePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.ExpandableAdapterUsageHistoryDetail;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentPaggerUsageHistoryDetailBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.DisclaimerDialogListener;
import com.bakcell.interfaces.PauseInUsageDetailPageListener;
import com.bakcell.interfaces.UsageHistoryDetailLoadAction;
import com.bakcell.models.DataManager;
import com.bakcell.models.pin.DataOtp;
import com.bakcell.models.pin.DataPin;
import com.bakcell.models.usagehistory.details.UsageHistoryDetails;
import com.bakcell.models.usagehistory.details.UsageHistoryDetailsMain;
import com.bakcell.utilities.FieldFormatter;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestHistoryResendPinService;
import com.bakcell.webservices.RequestUsageHistoryDetailsService;
import com.bakcell.webservices.RequestVerifyAccountService;
import com.bakcell.webservices.RequestVerifyPinService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellCalendar;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 22-May-17.
 */

public class UsageHistoryDetailPaggerFragment extends Fragment implements View.OnClickListener {

    public static final String SERVICE_ALL = "all";
    public static final String SERVICE_DATA = "data";
    public static final String SERVICE_VOICE = "voice";
    public static final String SERVICE_SMS = "sms";
    public static final String SERVICE_OTHERS = "others";

    private final int USAGE_HISTORY_POASSPORT_SCREEN = 1;
    private final int USAGE_HISTORY_PIN_SCREEN = 2;
    private final int USAGE_HISTORY_DETAIL_SCREEN = 3;

    private int usageDetailCurrentState = -1;

    FragmentPaggerUsageHistoryDetailBinding binding;


    private ExpandableAdapterUsageHistoryDetail adapterUsageHistoryDetail;

    private SpinnerAdapterOperationHistoryFilter spinnerAdapterServiceTyprFilter;
    private SpinnerAdapterOperationHistoryFilter spinnerAdapterPeriodFilter;


    private String startDate = "", endDate = "", date = "";

    boolean flagDate = false;

    String dateOne = "";
    String dateTwo = "";
    String serviceType = null;
    private UsageHistoryDetailsMain usageHistoryDetailsMain;

    private String passportNumber;


    boolean isRed = false;

    Runnable runnable = null;

    int count = 0;
    String red;
    String grey;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_usage_history_detail, container, false);

        RootValues.getInstance().setUsageHistoryDetailLoadActionListener(usageHistoryDetailLoadActionListener);
        RootValues.getInstance().setPauseInUsageDetailPageListener(pauseInUsageDetailPageListener);

        onCreateViewProcess();

        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN,
                AppEventLogValues.AccountEvents.USAGE_HISTORY_DETAIL_SCREEN,
                AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        RootValues.getInstance().setUsageHistoryDetailLoadActionListener(null);
        RootValues.getInstance().setPauseInUsageDetailPageListener(null);

    }

    UsageHistoryDetailLoadAction usageHistoryDetailLoadActionListener = new UsageHistoryDetailLoadAction() {
        @Override
        public void onUsageHistoryDetailActionCall(int actionId) {

            if (RootValues.getInstance().isIS_USAGE_DETAIL_OTP_VERIFY()) {
                showViewAccordingNagivation(USAGE_HISTORY_DETAIL_SCREEN);
            } else {
                showViewAccordingNagivation(USAGE_HISTORY_POASSPORT_SCREEN);
            }

        }
    };

    PauseInUsageDetailPageListener pauseInUsageDetailPageListener = new PauseInUsageDetailPageListener() {
        @Override
        public void onPauseInUsageDetailListen() {
            try {
                if (usageDetailCurrentState != USAGE_HISTORY_PIN_SCREEN) {
                    showViewAccordingNagivation(USAGE_HISTORY_POASSPORT_SCREEN);
                }
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
        }
    };

    private void onCreateViewProcess() {
        initUIContents();

        initUiListerners();


        try {
            setDefaultDate();
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        binding.calendarView.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.spinnerPeriodButton:

                binding.spinnerPeriod.performClick();
                break;
            case R.id.spinnerServiceTypeButton:

                binding.spinnerServiceType.performClick();
                break;
            case R.id.buttonSubmitPassport:
                passportNumber = binding.passportNoInput.getText().toString();
                if (Tools.hasValue(passportNumber)) {
                    if (passportNumber.length() == 7) {
                        requestForVerifyAccount(passportNumber);
                    } else {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), getString(R.string.msg_invalid_passport_number));
                        }
                    }
                } else {
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), getString(R.string.msg_please_enter_passport_number));
                    }
                }
                break;
            case R.id.buttonSubscribePin:

                String pin = binding.pinInput.getText().toString();

                if (pin.length() == 0) {
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), getString(R.string.msg_please_enter_pin));
                    }
                    return;
                }

                if (!FieldFormatter.isValidPin(pin)) {
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), getString(R.string.error_msg_invalid_pin));
                    }
                    return;
                }

                requestForPinAccount(pin);

                break;
            case R.id.tvDateOne:
                try {
                    showCalendar(binding.tvDateOne, true);//false for max date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
            case R.id.tvDateTwo:
                try {
                    showCalendar(binding.tvDateTwo, false);//true for min date
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
            case R.id.resendLayout:

                requestForResendPin();

                break;

        }
    }

    private void updateDates(String newStartDate, String newEndDate) {

        startDate = newStartDate;
        endDate = newEndDate;
        requestForGetUsageDetailsHistory(startDate, endDate);
    }


    public void requestForGetUsageDetailsHistory(String startDate, String endDate) {


        RequestUsageHistoryDetailsService.newInstance(getActivity(), ServiceIDs.REQUEST_USAGE_HISTORY_DETAILS).execute(DataManager.getInstance().getCurrentUser(),
                startDate, endDate, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        usageHistoryDetailsMain = new Gson().fromJson(result, UsageHistoryDetailsMain.class);

                        updateUIUsageDetail(usageHistoryDetailsMain);

                        // Function to show user disclaier dialog
                        showDisclaimerDialog();

                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            binding.expandableListview.setVisibility(View.GONE);
                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());

                        }//end of if
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            binding.expandableListview.setVisibility(View.GONE);
                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }//end of onError
                });
    }

    /**
     * This function is called for to show disclaimer dialog to user to tell him/her that usage history is update after 15 min
     */
    private void showDisclaimerDialog() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (PrefUtils.getBoolean(getActivity(),
                    MultiAccountsHandler.CacheKeys.getUsageHistoryDisclaimerPopupCacheKey(getActivity()), true))
                BakcellPopUpDialog.showMessageDisclaimerDialog(getActivity(), getString(R.string.lbl_disclaimer), getString(R.string.message_disclaimer_message), new DisclaimerDialogListener() {
                    @Override
                    public void onDisclaimerDialogOkListner(boolean checked) {
                        if (checked) {
                            if (getActivity() != null && !getActivity().isFinishing()) {
                                PrefUtils.addBoolean(getActivity(),
                                        MultiAccountsHandler.CacheKeys.getUsageHistoryDisclaimerPopupCacheKey(getActivity()), false);
                            }
                        }
                    }
                });
        }
    }

    private void updateUIUsageDetail(UsageHistoryDetailsMain usageHistoryDetailsMain) {
        if (usageHistoryDetailsMain == null) return;

        if (adapterUsageHistoryDetail != null && usageHistoryDetailsMain.getRecords() != null) {

            binding.paggerNoTxt.setText("0/0");

            binding.expandableListview.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            adapterUsageHistoryDetail.updateList(usageHistoryDetailsMain.getRecords());
            if (serviceType != null) {
                filterUsageDetails(serviceType);
            }
        } else {
            binding.expandableListview.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }

    }


    private void initUiListerners() {
        binding.spinnerPeriodButton.setOnClickListener(this);
        binding.spinnerServiceTypeButton.setOnClickListener(this);
        binding.buttonSubmitPassport.setOnClickListener(this);
        binding.buttonSubscribePin.setOnClickListener(this);
        binding.tvDateOne.setOnClickListener(this);
        binding.tvDateTwo.setOnClickListener(this);
        binding.resendLayout.setOnClickListener(this);

        binding.spinnerServiceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (usageDetailCurrentState != USAGE_HISTORY_DETAIL_SCREEN) return;
                String[] flterArray = getResources().getStringArray(R.array.usage_history_service_type_filter_apply);
                serviceType = flterArray[position];
                filterUsageDetails(serviceType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (usageDetailCurrentState != USAGE_HISTORY_DETAIL_SCREEN) return;
                if (position == 0) {
                    if (flagDate) {
                        try {
                            binding.llCalendar.setVisibility(View.GONE);
                            updateDates(Tools.getCurrentDate(), Tools.getCurrentDate());

                        } catch (ParseException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                    } else {
                        flagDate = true;
                    }
                } else if (position == 1) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_SEVEN_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 2) {
                    try {
                        binding.llCalendar.setVisibility(View.GONE);
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_THIRTY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 3) {
                    binding.llCalendar.setVisibility(View.GONE);
                    date = "";
                    String maxDate = "";
                    try {
                        date = Tools.getLastMonthFirstDate();
                        maxDate = Tools.getLastMonthLastDate();

                        updateDates(date, maxDate);

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                } else if (position == 4) {
                    try {
                        setDefaultDate();
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                    binding.llCalendar.setVisibility(View.VISIBLE);
                } else {
                    binding.llCalendar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.pinInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

              /*  if (Tools.isNumeric(String.valueOf(s)) && s.length() == 4) {
                    enableNextButton();
                } else {
                    disableNextButton();
                }*/

                //disable the pin input when user starts entering the pin number
                if (Tools.isNumeric(String.valueOf(s)) && s.length() > 0) {
                    disableResendButton();
                } else {
                    enableResendButton();
                }
            }
        });
    }

    private void enableResendButton() {
        binding.resendLayout.setEnabled(true);
        binding.resendLabel.setTextColor(ContextCompat.getColor(
                getActivity(), R.color.black));
        binding.rightArrowImg.setImageResource(R.drawable.arrow_resend);
    }//enableResendButton ends

    private void disableResendButton() {
        binding.resendLayout.setEnabled(false);
        binding.resendLabel.setTextColor(ContextCompat.getColor(
                getActivity(), R.color.signup_status_line));
        binding.rightArrowImg.setImageResource(R.drawable.forward_arrow_disabled);
    }//enableResendButton ends

    ArrayList<UsageHistoryDetails> filterList = new ArrayList<>();

    private void filterUsageDetails(String item) {
        if (!Tools.hasValue(item)) return;
        switch (item.toLowerCase()) {
            case SERVICE_ALL:
                if (usageHistoryDetailsMain == null) return;
                adapterUsageHistoryDetail.updateList(usageHistoryDetailsMain.getRecords());
                break;
            case SERVICE_OTHERS:
                if (usageHistoryDetailsMain == null) return;
                filterList.clear();
                String filterStr = SERVICE_DATA + "," + SERVICE_SMS + "," + SERVICE_VOICE;
                if (usageHistoryDetailsMain != null) {
                    if (usageHistoryDetailsMain.getRecords() != null) {
                        for (int i = 0; i < usageHistoryDetailsMain.getRecords().size(); i++) {
                            if (Tools.hasValue(usageHistoryDetailsMain.getRecords().get(i).getType())) {
                                if (filterStr.contains(usageHistoryDetailsMain.getRecords().get(i).getType().trim().toLowerCase())) {

                                } else {
                                    filterList.add(usageHistoryDetailsMain.getRecords().get(i));
                                }
                            }
                        }
                    }
                    adapterUsageHistoryDetail.updateList(filterList);
                }
                break;
            default:
                filterList.clear();
                if (usageHistoryDetailsMain != null) {
                    if (usageHistoryDetailsMain.getRecords() != null) {
                        for (int i = 0; i < usageHistoryDetailsMain.getRecords().size(); i++) {
                            if (Tools.hasValue(usageHistoryDetailsMain.getRecords().get(i)
                                    .getType())) {
                                if (item.equalsIgnoreCase(usageHistoryDetailsMain.getRecords()
                                        .get(i).getType().trim())) {
                                    filterList.add(usageHistoryDetailsMain.getRecords().get(i));
                                }
                            }
                        }
                    }
                    adapterUsageHistoryDetail.updateList(filterList);
                }
                break;
        }
    }

    private void initUIContents() {


        binding.paggerNoTxt.setText("0/0");
        red = getString(R.string.usage_history_numbers_red);
        binding.tvText.setText(Html.fromHtml(red));


        spinnerAdapterServiceTyprFilter = new SpinnerAdapterOperationHistoryFilter(getActivity(), getResources().getStringArray(R.array.usage_history_service_type_filter));
        binding.spinnerServiceType.setAdapter(spinnerAdapterServiceTyprFilter);

        spinnerAdapterPeriodFilter = new SpinnerAdapterOperationHistoryFilter(getActivity(), getResources().getStringArray(R.array.period_filter));
        binding.spinnerPeriod.setAdapter(spinnerAdapterPeriodFilter);

        ArrayList<UsageHistoryDetails> usageDetailArrayList = new ArrayList<>();
        adapterUsageHistoryDetail = new ExpandableAdapterUsageHistoryDetail(getActivity(), usageDetailArrayList);
        binding.expandableListview.setAdapter(adapterUsageHistoryDetail);


        binding.expandableListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                binding.paggerNoTxt.setText((firstVisibleItem + visibleItemCount) + "/" + totalItemCount);

            }
        });

    }


    private void showViewAccordingNagivation(int viewName) {
        usageDetailCurrentState = viewName;
        flagDate = false;
        binding.spinnerPeriod.setSelection(0);
        binding.spinnerServiceType.setSelection(0);
        switch (viewName) {
            case USAGE_HISTORY_POASSPORT_SCREEN:
                binding.inputPassportView.setVisibility(View.VISIBLE);
                binding.inputPinView.setVisibility(View.GONE);
                binding.usageHistoryDetailView.setVisibility(View.GONE);
                binding.paggerNoTxt.setVisibility(View.GONE);
                if (adapterUsageHistoryDetail != null) {
                    adapterUsageHistoryDetail.updateList(new ArrayList<UsageHistoryDetails>());
                }
                binding.calendarView.setVisibility(View.GONE);
                binding.pageNoLayout.setVisibility(View.VISIBLE);
                break;
            case USAGE_HISTORY_PIN_SCREEN:
                binding.inputPassportView.setVisibility(View.GONE);
                binding.inputPinView.setVisibility(View.VISIBLE);
                binding.usageHistoryDetailView.setVisibility(View.GONE);
                binding.paggerNoTxt.setVisibility(View.GONE);
                if (adapterUsageHistoryDetail != null) {
                    adapterUsageHistoryDetail.updateList(new ArrayList<UsageHistoryDetails>());
                }
                binding.calendarView.setVisibility(View.GONE);
                binding.pageNoLayout.setVisibility(View.GONE);
                binding.pinInput.setText("");
                break;
            case USAGE_HISTORY_DETAIL_SCREEN:
                binding.inputPassportView.setVisibility(View.GONE);
                binding.inputPinView.setVisibility(View.GONE);
                binding.usageHistoryDetailView.setVisibility(View.VISIBLE);
                binding.paggerNoTxt.setVisibility(View.VISIBLE);
                binding.calendarView.setVisibility(View.VISIBLE);

                try {
                    date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.CURRENT_DAY);
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                try {
                    updateDates(date, Tools.getCurrentDate());
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
        }

    }


    public void showTextAnimation() {
        if (getUserVisibleHint()) {
            try {
                final Handler animationHandler = new Handler(Looper.getMainLooper());
                count = 0;
                red = getString(R.string.usage_history_numbers_red);
                grey = getString(R.string.usage_history_numbers_grey);

                runnable = new Runnable() {
                    @Override
                    public void run() {
                        count++;
                        if (isRed) {
                            binding.tvText.setText(Html.fromHtml(red));
                            isRed = false;
                        } else {
                            binding.tvText.setText(Html.fromHtml(grey));
                            isRed = true;
                        }
                        if (count > 7) {
                            animationHandler.removeCallbacks(runnable);
                            count = 0;
                        } else {
                            animationHandler.postDelayed(runnable, 500);
                        }

                    }
                };
                animationHandler.postDelayed(runnable, 500);
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
        }


    }

    private synchronized void changeText(final String[] texts, Handler handler1) {
        for (int i = 0; i < texts.length; i++) {
            final int finalI = i;
            final boolean isRed = false;
            handler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isRed) {
                        binding.tvText.setText(Html.fromHtml(texts[finalI]));
                    }
                }
            }, 5000);
        }


    }


    public void showCalendar(BakcellTextViewNormal textViewNormal, boolean isStartDateSelected) throws ParseException {
        if (getActivity() == null) return;
        if (getActivity().isFinishing()) return;

        try {
            dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());

            Date minDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
            Date maxDate = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);

            BakcellCalendar bakcellCalendar;

            final Calendar calendar = Calendar.getInstance();
            if (isStartDateSelected) {
                calendar.setTime(minDate);
            } else {
                calendar.setTime(maxDate);
            }
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            bakcellCalendar = new BakcellCalendar(textViewNormal, ondate);


            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), bakcellCalendar,
                    year, month, day);
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {
                Calendar calndPre = Calendar.getInstance();
                calndPre.add(Calendar.MONTH, -3);
                calndPre.set(Calendar.DATE, 1);
                datePickerDialog.getDatePicker().setMinDate(calndPre.getTimeInMillis());

                Date dateMax = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateTwo);
                calendar.setTime(dateMax);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());

            } else {

                Date dateMin = new SimpleDateFormat(Constants.DateAndMonth.SAMPLE_DATE_FORMAT, Locale.ENGLISH).parse(dateOne);
                calendar.setTime(dateMin);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("");//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show();
        } catch (ParseException e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            //it will return index of Month for june it will return 05 +1 will make it 06
            //   monthOfYear++;
//            String dateNew = String.valueOf(year) + "-" + String.format("%02d", monthOfYear)
//                    + "-" + String.format("%02d", dayOfMonth);
            try {
                dateOne = Tools.changeDateFormatForMethods(binding.tvDateOne.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
            try {
                dateTwo = Tools.changeDateFormatForMethods(binding.tvDateTwo.getText().toString());
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            updateDates(dateOne, dateTwo);
        }
    };


    private void setDefaultDate() throws ParseException {
        binding.tvDateOne.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
        binding.tvDateTwo.setText(Tools.changeDateFormatForTextView(Tools.getCurrentDate()));
    }


    public void requestForVerifyAccount(String passportNumber) {


        RequestVerifyAccountService.newInstance(getActivity(), ServiceIDs.REQUEST_HISTORY_ACCOUNT_VERIFY)
                .execute(DataManager.getInstance().getCurrentUser(), passportNumber, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        DataPin dataPin = new Gson().fromJson(result, DataPin.class);

//                        if (dataPin != null) {
//                            if (Tools.hasValue(dataPin.getPin())) {

                        showViewAccordingNagivation(USAGE_HISTORY_PIN_SCREEN);
                        binding.calendarView.setVisibility(View.GONE);

                        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_DETAIL_PASSPORT_VERIFICATION_SUCCESS,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN);

                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), response.getDescription());
                        }

                        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_DETAIL_PASSPORT_VERIFICATION_FAILURE,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN);

                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }

                });

    }


    public void requestForPinAccount(String pin) {


        RequestVerifyPinService.newInstance(getActivity(), ServiceIDs.REQUEST_HISTORY_PIN_VERIFY)
                .execute(DataManager.getInstance().getCurrentUser(), pin, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        DataOtp dataOtp = new Gson().fromJson(result, DataOtp.class);

                        showViewAccordingNagivation(USAGE_HISTORY_DETAIL_SCREEN);
                        binding.calendarView.setVisibility(View.VISIBLE);
                        RootValues.getInstance().setIS_USAGE_DETAIL_OTP_VERIFY(true);

                        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_DETAIL_PIN_VERIFICATION_SUCCESS,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN);

                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.bakcell_error_title), response.getDescription());
                        }

                        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_DETAIL_PIN_VERIFICATION_FAILURE,
                                AppEventLogValues.AccountEvents.USAGE_HISTORY_SCREEN);
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }

                });

    }


    /**
     * Request for Resend Pin
     */
    private void requestForResendPin() {

        if (DataManager.getInstance().getCurrentUser() == null) {
            Toast.makeText(getActivity(), R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestHistoryResendPinService.newInstance(getActivity(), ServiceIDs.REQUEST_HISTORY_RESEND).execute(DataManager.getInstance().getCurrentUser(), EndPoints.Constants.CAUSE_USAGEDETAIL, new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {


                String result = response.getData();

                DataPin dataPin = new Gson().fromJson(result, DataPin.class);

            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                // Check if user logout by server
                if (getActivity() != null && !getActivity().isFinishing() &&
                        response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                    HomeActivity.logoutUser(getActivity());
                    return;
                }

                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                            getString(R.string.bakcell_error_title), response.getDescription());
                }
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                }
            }
        });
    }

}
