package com.bakcell.fragments.pager;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.databinding.FragmentRoamingNewBinding;
import com.bakcell.models.roamingnewmodels.ServiceListHelperModel;
import com.bakcell.viewsitem.ViewItemRoamingOperators;

import java.util.ArrayList;

public class RoamingNewFragment extends Fragment implements View.OnClickListener {

    private FragmentRoamingNewBinding binding;
    private ArrayList<ServiceListHelperModel> serviceListHelperModel = new ArrayList<>();
    private static final String DATA_KEY = "roaming_model_data_key";

    public static RoamingNewFragment getInstance(ArrayList<ServiceListHelperModel> serviceListHelperModel) {
        RoamingNewFragment fragment = new RoamingNewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(DATA_KEY, serviceListHelperModel);
        fragment.setArguments(bundle);
        return fragment;
    }//getInstance ends

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(DATA_KEY)) {
            serviceListHelperModel = getArguments().getParcelableArrayList(DATA_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_roaming_new, container, false);
        binding.contentContainer.removeAllViews();

        for (int i = 0; i < serviceListHelperModel.size(); i++) {

            String service = serviceListHelperModel.get(i).service;
            String serviceUnit = serviceListHelperModel.get(i).serviceUnit;
            ViewItemRoamingOperators operators = new ViewItemRoamingOperators(getActivity());
            operators.getServiceName().setText(service);//set service name
            operators.getServiceName().setTextColor(getActivity().getResources().getColor(R.color.black));//in black color
            operators.getServiceValue().setText(serviceUnit);//set service unit
            operators.getServiceValue().setTextColor(getActivity().getResources().getColor(R.color.text_gray));//in gray color
            binding.contentContainer.addView(operators);
            for (int j = 0; j < serviceListHelperModel.get(i).serviceTypeListHelperModel.size(); j++) {
                String name = serviceListHelperModel.get(i).serviceTypeListHelperModel.get(j).type;
                String value = serviceListHelperModel.get(i).serviceTypeListHelperModel.get(j).value;
                String unit = serviceListHelperModel.get(i).serviceTypeListHelperModel.get(j).unit;

                ViewItemRoamingOperators operatorDetails = new ViewItemRoamingOperators(getActivity());

                operatorDetails.getServiceName().setText(name);
                operatorDetails.getServiceValue().setText(value);
                //show/hide manat symbol
                if (unit.trim().equalsIgnoreCase("manat") || unit.trim().equalsIgnoreCase("azn")) {
                    operatorDetails.showManatSymbol();
                    operatorDetails.getServiceValue().setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                } else {
                    operatorDetails.hideManatSymbol();
                    operatorDetails.getServiceValue().setTextColor(getActivity().getResources().getColor(R.color.text_gray));
                }

                if (value.trim().equalsIgnoreCase("PULSUZ") || value.trim().equalsIgnoreCase("FREE") || value.trim().equalsIgnoreCase("БЕСПЛАТНО")) {
                    operatorDetails.hideManatSymbol();
                    operatorDetails.getServiceValue().setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                }

                //show/hide the gray bottom view
                if (j == serviceListHelperModel.get(i).serviceTypeListHelperModel.size() - 1) {
                    operatorDetails.showContentView();
                } else {
                    operatorDetails.hideContentView();
                }
                //hide the view for the last block
                if (i == serviceListHelperModel.size() - 1) {
                    operatorDetails.hideContentView();
                }
                binding.contentContainer.addView(operatorDetails);
            }
        }
        return binding.getRoot();
    }//onCreateView ends

    @Override
    public void onClick(View view) {

    }
}
