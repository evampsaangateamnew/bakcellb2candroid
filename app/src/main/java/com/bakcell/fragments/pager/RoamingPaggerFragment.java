package com.bakcell.fragments.pager;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.RoamingOfferActivity;
import com.bakcell.adapters.RoamingCountryAdapter;
import com.bakcell.databinding.FragmentPaggerRoamingBinding;
import com.bakcell.models.supplementaryoffers.RoamingsOffer;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;


public class RoamingPaggerFragment extends Fragment implements View.OnClickListener {

    public static final String KEYWORD_ROAMING = "key.roamingsOffer";
    private FragmentPaggerRoamingBinding binding;

    private RoamingsOffer roamingsOffer;

    private String selectedCountry;
    private ArrayList<SupplementaryOffer> filteredRoamingOffers;
    private Toast toast;
    private String flag;

    public static RoamingPaggerFragment getInstance(RoamingsOffer roamingsOffer) {
        RoamingPaggerFragment fragment = new RoamingPaggerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEYWORD_ROAMING, roamingsOffer);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(KEYWORD_ROAMING)) {
            roamingsOffer = getArguments().getParcelable(KEYWORD_ROAMING);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_roaming, container, false);

        iniUIContents();

        initUiListeners();

        return binding.getRoot();
    }
    ArrayList<String> countryListSearch;

    private void iniUIContents() {

        if (roamingsOffer == null) return;
        if (roamingsOffer.getCountries() == null) return;
        countryListSearch=new ArrayList<>();
        for (int i = 0; i < roamingsOffer.getCountries().size(); i++) {
            countryListSearch.add(roamingsOffer.getCountries().get(i).getName());
        }

        RoamingCountryAdapter countryAdapter = new RoamingCountryAdapter(getActivity(), binding.allCountiesInout.getId(), countryListSearch);
        binding.allCountiesInout.setAdapter(countryAdapter);

        binding.allCountiesInout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.allCountiesInout.showDropDown();
            }
        });

        binding.allCountiesInout.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    prepareForRoamingActivity();
                    return true;
                }
                return false;
            }
        });

    }

    private void initUiListeners() {
        binding.btnRoaming.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_roaming:
                prepareForRoamingActivity();
                break;

        }
    }

    private void prepareForRoamingActivity() {
        selectedCountry = binding.allCountiesInout.getText().toString();
        if (selectedCountry.trim().length() == 0) {
            if (toast != null) {
                toast.cancel();
            }
            if (getActivity() != null && !getActivity().isFinishing()) {
                toast = Toast.makeText(getActivity(), R.string.activity_roaming_select_country_lbl,
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        } else {
            startRoamingActivity();
        }
    }

    private void startRoamingActivity() {
        flag = "";
        if (selectedCountry != null) {
            if (roamingsOffer.getOffers() != null) {
                if (roamingsOffer.getCountries() != null && roamingsOffer.getOffers().size() > 0) {
                    filteredRoamingOffers = new ArrayList<>();
                    for (int i = 0; i < roamingsOffer.getOffers().size(); i++) {
                        if (roamingsOffer.getOffers().get(i).getDetails() != null) {
                            if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails() != null) {
                                if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails()
                                        .getRoamingDetailsCountriesList() != null &&
                                        roamingsOffer.getOffers().get(i).getDetails()
                                                .getRoamingDetails().getRoamingDetailsCountriesList().size() > 0) {
                                    for (int j = 0; j < roamingsOffer.getOffers().get(i).getDetails()
                                            .getRoamingDetails().getRoamingDetailsCountriesList().size(); j++) {
                                        if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails()
                                                .getRoamingDetailsCountriesList().get(j).getCountryName().trim()
                                                .equalsIgnoreCase(selectedCountry.trim())) {

                                            if (!Tools.hasValue(flag))
                                                flag = roamingsOffer.getOffers().get(i).getDetails()
                                                        .getRoamingDetails()
                                                        .getRoamingDetailsCountriesList().get(j).getFlag();
                                            filteredRoamingOffers.add(roamingsOffer.getOffers().get(i));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (filteredRoamingOffers != null && filteredRoamingOffers.size() > 0) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(RoamingOfferActivity.KEY_ROAMING_OFFERS,
                        filteredRoamingOffers);
                if (roamingsOffer.getFilters() != null)
                    bundle.putParcelable(RoamingOfferActivity.KEY_ROAMING_FILTERS,
                            roamingsOffer.getFilters());
                bundle.putString(RoamingOfferActivity.KEY_COUNTRY_NAME, binding.allCountiesInout
                        .getText().toString());
                bundle.putString(RoamingOfferActivity.KEY_FLAG, flag);
                BaseActivity.startNewActivity(getActivity(), RoamingOfferActivity.class, bundle);
            } else {
                try {
                    if (toast != null) {
                        toast.cancel();
                    }
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        toast = Toast.makeText(getActivity(), R.string.activity_roaming_no_offers_found_lbl, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } catch (Exception ex) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                }
            }

        }
    }
}
