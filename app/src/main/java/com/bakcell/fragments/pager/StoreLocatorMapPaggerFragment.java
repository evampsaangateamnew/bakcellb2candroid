package com.bakcell.fragments.pager;

import android.Manifest;
import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.bakcell.R;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentStoreLocatorMapBinding;
import com.bakcell.fragments.menus.StoreLocatorFragment;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.LocationOnListener;
import com.bakcell.interfaces.RedirectMapListener;
import com.bakcell.locations.LocationUpdateListener;
import com.bakcell.locations.MyLocationListener;
import com.bakcell.models.storelocator.StoreLocator;
import com.bakcell.models.storelocator.StoreLocatorMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.StoreLocatorNearestStoreItem;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Created by Noman on 30/9/2017.
 */

public class StoreLocatorMapPaggerFragment extends Fragment implements View.OnClickListener {

    public static final int LOCATION_PERMISSION = 333;
    public static final int LOCATION_ON = 334;

    private final float METERS_IN_MILE = (float) 0.00062137119;

    FragmentStoreLocatorMapBinding binding;

    GoogleMap mGoogleMap;

    private StoreLocatorMain storeLocatorMain;
    public static final String SPINNER_ALL = "All";

    private StoreLocator storeLocatorHeadOffice = null;


    public static final String FILTER_ALL_EN = "All";
    public static final String FILTER_ALL_RU = "Все";
    public static final String FILTER_ALL_AZ = "Hamısı";

    public static StoreLocatorMapPaggerFragment getInstance(StoreLocatorMain storeLocatorMain, StoreLocator storeLocatorHeadOffice) {
        StoreLocatorMapPaggerFragment fragment = new StoreLocatorMapPaggerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(StoreLocatorFragment.KEY_STORE_LOCATOR, storeLocatorMain);
        bundle.putParcelable(StoreLocatorFragment.KEY_FROM_CONTACT_US, storeLocatorHeadOffice);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(StoreLocatorFragment.KEY_STORE_LOCATOR)) {
            storeLocatorMain = getArguments().getParcelable(StoreLocatorFragment.KEY_STORE_LOCATOR);
        }
        if (getArguments().containsKey(StoreLocatorFragment.KEY_FROM_CONTACT_US)) {
            storeLocatorHeadOffice = getArguments().getParcelable(StoreLocatorFragment.KEY_FROM_CONTACT_US);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_store_locator_map, container, false);
        if (storeLocatorMain == null) return binding.getRoot();
        if (binding.mapView != null) {
            binding.mapView.onCreate(savedInstanceState);
            binding.mapView.onResume();
        }

        initAndPrepareMap(storeLocatorMain.getStores());

        initUiContent();
        initUiListeners();

        processOnCurrentLocation(storeLocatorMain.getStores());

        RootValues.getInstance().setRedirectMapListener(redirectMapListener);

        RootValues.getInstance().setLocationOnListener(locationOnListener);

        AppEventLogs.applyAppEvent(AppEventLogValues.StoreLocatorEvents.STORE_LOCATOR_MAP,
                AppEventLogValues.StoreLocatorEvents.STORE_LOCATOR_LIST,
                AppEventLogValues.StoreLocatorEvents.STORE_LOCATOR_LIST);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RootValues.getInstance().setRedirectMapListener(null);
        RootValues.getInstance().setLocationOnListener(null);
    }


    private RedirectMapListener redirectMapListener = new RedirectMapListener() {
        @Override
        public void onRedirectMapListen(StoreLocator storeLocator) {
            if (storeLocator == null) return;

            ArrayList<StoreLocator> oneStoreLocatorsList = new ArrayList<>();
            oneStoreLocatorsList.add(storeLocator);
            initAndPrepareMap(oneStoreLocatorsList);
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (storeLocatorMain != null) {
                        processOnCurrentLocation(storeLocatorMain.getStores());
                    }
                }
                break;
        }
    }

    private void processOnCurrentLocation(final ArrayList<StoreLocator> storesList) {
        binding.nearStoreView.setVisibility(View.GONE);
        if (storesList == null || getActivity() == null || getActivity().isFinishing()) return;

        if (!Tools.isLocationEnabled(getActivity())) {
            showDialogForLocationSettings();
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission((Activity) getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION);
            return;
        } else {
            new MyLocationListener(getActivity(), new LocationUpdateListener() {
                @Override
                public void onLocationReceive(final LatLng myLagLong) {

                    if (getActivity() != null && !getActivity().isFinishing()) {
                    } else {
                        return;
                    }
                    if (myLagLong == null || storesList == null) return;
                    if (storesList != null && storesList.size() > 0) {
                        binding.nearStoreView.setVisibility(View.VISIBLE);
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<StoreLocator> nearestStores = sortListbyDistance(storesList, myLagLong);
                                if (nearestStores != null && nearestStores.size() > 3) {
                                    if (getActivity() != null && !getActivity().isDestroyed()
                                            && !getActivity().isFinishing()) {
                                        populateNearestStoreToContainer(nearestStores, 3, myLagLong);
                                    }
                                } else if (nearestStores != null && nearestStores.size() == 2) {
                                    if (getActivity() != null && !getActivity().isDestroyed()
                                            && !getActivity().isFinishing()) {
                                        populateNearestStoreToContainer(nearestStores, 2, myLagLong);
                                    }
                                } else if (nearestStores != null && nearestStores.size() == 1) {
                                    if (getActivity() != null && !getActivity().isDestroyed()
                                            && !getActivity().isFinishing()) {
                                        populateNearestStoreToContainer(nearestStores, 1, myLagLong);
                                    }
                                }
                            }
                        });
                    }

                }
            });
        }
    }

    private void populateNearestStoreToContainer(final ArrayList<StoreLocator> nearestStores, final int i, final LatLng myPostion) {
        binding.nearStoreView.removeAllViews();
        binding.nearStoreView.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    for (int j = 0; j < i; j++) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            StoreLocatorNearestStoreItem nearestStoreItem = new StoreLocatorNearestStoreItem(getActivity());
                            nearestStoreItem.getNearStoreName().setText("");
                            nearestStoreItem.getNearMilesTxt().setText("");
                            if (Tools.hasValue(nearestStores.get(j).getStore_name())) {
                                nearestStoreItem.getNearStoreName().setText(nearestStores.get(j).getStore_name());
                            }
                            if (Tools.hasValue(nearestStores.get(j).getLatitude()) && Tools.hasValue(nearestStores.get(j).getLongitude())) {
                                nearestStoreItem.getNearMilesTxt().setText(getString(R.string.nearrest_store_miles,
                                        Tools.getRoundedDecimal(
                                                calculateDistanceBetweenTwoLocation(
                                                        Tools.getDoubleFromString(nearestStores.get(j).getLatitude()),
                                                        Tools.getDoubleFromString(nearestStores.get(j).getLongitude()),
                                                        myPostion.latitude,
                                                        myPostion.longitude)
                                                        / 1000) + ""));
                            }
                            final int finalJ = j;
                            nearestStoreItem.getLl_shortest_store().setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ArrayList<StoreLocator> storeLocatorsHeadOffice = new ArrayList<StoreLocator>();
                                    storeLocatorsHeadOffice.add(nearestStores.get(finalJ));
                                    initAndPrepareMap(storeLocatorsHeadOffice);

                                }
                            });
                            binding.nearStoreView.addView(nearestStoreItem);
                        }
                    }
                }
            }
        }, 10);

    }

    LocationOnListener locationOnListener = new LocationOnListener() {
        @Override
        public void onLocationOnListener(int requestCode) {

            if (LOCATION_ON == requestCode && storeLocatorMain != null && Tools.isLocationEnabled(getActivity())) {
                processOnCurrentLocation(storeLocatorMain.getStores());
            }
        }
    };

    protected void showDialogForLocationSettings() {
        if (getActivity() == null || getActivity().isFinishing()) return;

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            if (status != null && getActivity() != null && !getActivity().isFinishing()) {
                                status.startResolutionForResult(getActivity(), StoreLocatorMapPaggerFragment.LOCATION_ON);
                            }
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });


    }


    private void initUiListeners() {
        binding.ivCitiesDropDown.setOnClickListener(this);
        binding.ivServiceCenterDropDown.setOnClickListener(this);

        binding.spStoreLocatorCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = "";
                String city = "";

                if (!binding.spStoreLocatorServices.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorServices.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorServices.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    type = binding.spStoreLocatorServices.getSelectedItem().toString();
                }
                if (!binding.spStoreLocatorCities.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorCities.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorCities.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    city = binding.spStoreLocatorCities.getAdapter().getItem(position).toString();
                }
                filterLocationsOnService(type, city);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.spStoreLocatorServices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String city = "";
                String service = "";

                if (!binding.spStoreLocatorCities.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorCities.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorCities.getSelectedItem().toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    city = binding.spStoreLocatorCities.getSelectedItem().toString();
                }
                if (!binding.spStoreLocatorServices.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_EN) &&
                        !binding.spStoreLocatorServices.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_AZ) &&
                        !binding.spStoreLocatorServices.getAdapter().getItem(position).toString().equalsIgnoreCase(FILTER_ALL_RU)) {
                    service = binding.spStoreLocatorServices.getAdapter().getItem(position).toString();
                }
                filterLocationsOnService(service, city);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void filterLocationsOnService(String service, String city) {
        ArrayList<StoreLocator> cityFilteredStores = new ArrayList<StoreLocator>();
        if (storeLocatorMain != null && storeLocatorMain.getStores() != null) {
            if (Tools.hasValue(city)) {
                for (int i = 0; i < storeLocatorMain.getStores().size(); i++) {
                    if (Tools.hasValue(storeLocatorMain.getStores().get(i).getCity())) {
                        if (storeLocatorMain.getStores().get(i).getCity().contentEquals(city)) {
                            cityFilteredStores.add(storeLocatorMain.getStores().get(i));
                        }
                    }
                }
            } else {
                cityFilteredStores.addAll(storeLocatorMain.getStores());
            }
            ArrayList<StoreLocator> typeAndCityFilteredStores = new ArrayList<StoreLocator>();

            if (Tools.hasValue(service)) {
                if (cityFilteredStores.size() > 0) {
                    for (int i = 0; i < cityFilteredStores.size(); i++) {
                        if (Tools.hasValue(cityFilteredStores.get(i).getType())) {
                            if (cityFilteredStores.get(i).getType().contentEquals(service)) {
                                typeAndCityFilteredStores.add(cityFilteredStores.get(i));
                            }
                        }
                    }
                }
            } else {
                typeAndCityFilteredStores.addAll(cityFilteredStores);
            }
            if (typeAndCityFilteredStores != null) {
                if (mGoogleMap == null) return;
                mGoogleMap.clear();
                if (typeAndCityFilteredStores.size() < 1) setGoogleMapZoomOut(mGoogleMap);
                prepareStoreLocationOnMap(mGoogleMap, typeAndCityFilteredStores);
            }
        }

    }


    private void initAndPrepareMap(final ArrayList<StoreLocator> storesList) {

        if (binding.mapView != null) {
            binding.mapView.setVisibility(View.INVISIBLE);

            binding.mapView.getMapAsync(new OnMapReadyCallback() {

                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mGoogleMap = googleMap;
                    if (getActivity() == null || getActivity().isFinishing()) return;
                    binding.mapView.setVisibility(View.VISIBLE);

                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

                    if (storesList != null && storesList.size() > 0) {
                        prepareStoreLocationOnMap(mGoogleMap, storesList);
                    }

                    if (storeLocatorHeadOffice != null) {
                        ArrayList<StoreLocator> storeLocatorsHeadOffice = new ArrayList<StoreLocator>();
                        storeLocatorsHeadOffice.add(storeLocatorHeadOffice);
                        storeLocatorHeadOffice = null;
                        initAndPrepareMap(storeLocatorsHeadOffice);
                    }


                    if (getActivity() != null && !getActivity().isFinishing() && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mGoogleMap.setMyLocationEnabled(true);

                }
            });
        }

    }


    private void prepareStoreLocationOnMap(GoogleMap googleMap, ArrayList<StoreLocator> storesList) {
        if (storesList == null || googleMap == null || storesList.size() < 1) return;


        if (storesList != null) {

            LatLngBounds.Builder latLongBounds = new LatLngBounds.Builder();

            for (int i = 0; i < storesList.size(); i++) {
                double latitude = Tools.getDoubleFromString(storesList.get(i).getLatitude());
                double longitude = Tools.getDoubleFromString(storesList.get(i).getLongitude());
                LatLng location = new LatLng(latitude, longitude);

                Marker marker = googleMap.addMarker(new MarkerOptions()
                        .position(location)
                        .title(storesList.get(i).getStore_name())
                        .snippet(storesList.get(i).getAddress())
                        .icon(BitmapDescriptorFactory.fromResource(StoreLocatorFragment.getMarkerIcon(storesList.get(i), true))));

                latLongBounds.include(location);

            }

            boolean isOneMarker = false;
            if (storesList != null && storesList.size() < 2) {
                isOneMarker = true;
            }

            // Set Camera For Locations
            setGoogleMapZoom(googleMap, latLongBounds, isOneMarker, storesList);


        }


    }


    private void setGoogleMapZoom(GoogleMap googleMap, LatLngBounds.Builder latLongBounds, boolean isOneMarker, ArrayList<StoreLocator> storesList) {
        if (googleMap == null || latLongBounds == null) return;

        try {
            LatLngBounds bounds = latLongBounds.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.30); // offset from edges of the map 10% of screen
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

            CameraUpdate camZoom;

            if (isOneMarker && storesList != null && storesList.size() > 0) {
                float lat = Tools.getFloatFromString(storesList.get(0).getLatitude());
                float lng = Tools.getFloatFromString(storesList.get(0).getLongitude());
                LatLng latLng = new LatLng(lat, lng);

                camZoom = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            } else {
                camZoom = CameraUpdateFactory.newLatLngBounds(bounds, 30);
            }

            googleMap.animateCamera(camZoom);

        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    private void setGoogleMapZoomOut(GoogleMap googleMap) {
        if (googleMap == null) return;

        googleMap.animateCamera(CameraUpdateFactory.zoomTo(1.0f));

    }

    private void initUiContent() {
        String currentLanguage = AppClass.getCurrentLanguageKey(getActivity());
        if (storeLocatorMain != null && storeLocatorMain.getCity() != null && storeLocatorMain.getCity().size() > 0) {

            switch (currentLanguage) {
                case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN:
                    storeLocatorMain.getCity().add(0, FILTER_ALL_EN);
                    break;
                case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ:
                    storeLocatorMain.getCity().add(0, FILTER_ALL_AZ);
                    break;
                case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU:
                    storeLocatorMain.getCity().add(0, FILTER_ALL_RU);
                    break;
            }
            //storeLocatorMain.getCity().add(0,SPINNER_ALL);
            SpinnerAdapterOperationHistoryFilter spinnerAdapterCities = new
                    SpinnerAdapterOperationHistoryFilter(getActivity(), storeLocatorMain.getCity().toArray(new String[0]));
            binding.spStoreLocatorCities.setAdapter(spinnerAdapterCities);
        }

        if (storeLocatorMain != null && storeLocatorMain.getType() != null && storeLocatorMain.getType().size() > 0) {
            switch (currentLanguage) {
                case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN:
                    storeLocatorMain.getType().add(0, FILTER_ALL_EN);
                    break;
                case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ:
                    storeLocatorMain.getType().add(0, FILTER_ALL_AZ);
                    break;
                case Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU:
                    storeLocatorMain.getType().add(0, FILTER_ALL_RU);
                    break;
            }
            //storeLocatorMain.getType().add(0,SPINNER_ALL);
            SpinnerAdapterOperationHistoryFilter spinnerAdapterServiceCenters = new
                    SpinnerAdapterOperationHistoryFilter(getActivity(), storeLocatorMain.getType().toArray(new String[0]));
            binding.spStoreLocatorServices.setAdapter(spinnerAdapterServiceCenters);
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_service_center_drop_down:
                binding.spStoreLocatorServices.performClick();
                break;
            case R.id.iv_cities_drop_down:
                binding.spStoreLocatorCities.performClick();
                break;
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            if (binding.mapView != null) {
                binding.mapView.onResume();
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (binding.mapView != null) {
                binding.mapView.onPause();
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        try {
            if (binding.mapView != null) {
                if (getActivity() != null && !getActivity().isFinishing() && binding.mapView.getVisibility() == View.VISIBLE) {
                    binding.mapView.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            if (binding.mapView != null) {
                binding.mapView.onLowMemory();
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    //ArrayList<StoreLocator> storesList
    public ArrayList<StoreLocator> sortListbyDistance(ArrayList<StoreLocator> storeLocators, final LatLng location) {
        Collections.sort(storeLocators, new Comparator<StoreLocator>() {
            @Override
            public int compare(StoreLocator o1, StoreLocator o2) {
                if (calculateDistanceBetweenTwoLocation(Tools.getDoubleFromString(o1.getLatitude())
                        , Tools.getDoubleFromString(o1.getLongitude()), location.latitude, location.longitude) >
                        calculateDistanceBetweenTwoLocation(Tools.getDoubleFromString(o2.getLatitude()),
                                Tools.getDoubleFromString(o2.getLongitude()), location.latitude, location.longitude)) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
        return storeLocators;
    }


    public float calculateDistanceBetweenTwoLocation(double firstLatitude,
                                                     double firstLongitude,
                                                     double secondLatitude,
                                                     double secondLongitude) {
        float[] results = new float[1];
        Location.distanceBetween(firstLatitude, firstLongitude, secondLatitude, secondLongitude, results);
        return results[0];
    }
}
