package com.bakcell.fragments.pager;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.activities.landing.UlduzumLocationsActivity;
import com.bakcell.adapters.UlduzumLocationsListAdapter;
import com.bakcell.databinding.FragmentUlduzumLocationsListBinding;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.UlduzumLocationsListFilterListener;
import com.bakcell.models.ulduzum.UlduzumLocationsListModel;
import com.bakcell.models.ulduzum.UlduzumLocationsServiceModel;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class UlduzumLocationsListPagerFragment extends Fragment implements View.OnClickListener {

    private FragmentUlduzumLocationsListBinding binding;
    private UlduzumLocationsServiceModel serviceModel = null;
    private UlduzumLocationsListAdapter ulduzumLocationsListAdapter = null;
    private ArrayList<UlduzumLocationsListModel> ulduzumLocationsListModel = null;
    private UlduzumLocationsListFilterListener ulduzumLocationsListFilterListener = null;
    private final String fromClass = "UlduzumLocationsListPagerFragment";

    public static UlduzumLocationsListPagerFragment getInstance(UlduzumLocationsServiceModel serviceModel) {
        UlduzumLocationsListPagerFragment fragment = new UlduzumLocationsListPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(UlduzumLocationsActivity.ULDUZUM_LOCATIONS_BUNDLE_KEY, serviceModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(UlduzumLocationsActivity.ULDUZUM_LOCATIONS_BUNDLE_KEY)) {
            serviceModel = getArguments().getParcelable(UlduzumLocationsActivity.ULDUZUM_LOCATIONS_BUNDLE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ulduzum_locations_list, container, false);

        //register listener
        registerListener();

        populateListData(serviceModel);

        return binding.getRoot();
    }//oncreate ends

    private void populateListData(UlduzumLocationsServiceModel serviceModel) {
        if (serviceModel != null) {
            if (serviceModel.getMaindatalist() != null) {
                if (serviceModel.getMaindatalist().size() > 0) {
                    ulduzumLocationsListModel = getModelList(serviceModel);
                    if (ulduzumLocationsListModel.size() > 0) {
                        //create adapter
                        ulduzumLocationsListAdapter = new UlduzumLocationsListAdapter(getActivity(),getActivity(), ulduzumLocationsListModel);
                        binding.locationsList.setAdapter(ulduzumLocationsListAdapter);
                        binding.locationsList.setAreHeadersSticky(false);
                        ulduzumLocationsListAdapter.notifyDataSetChanged();
                        binding.locationsList.setVisibility(View.VISIBLE);
                        binding.noDataFoundLayout.setVisibility(View.GONE);
                    } else {
                        binding.locationsList.setVisibility(View.GONE);
                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    binding.locationsList.setVisibility(View.GONE);
                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                }
            } else {
                binding.locationsList.setVisibility(View.GONE);
                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            }
        } else {
            binding.locationsList.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }
    }//populateListData ends

    private void populateListData(ArrayList<UlduzumLocationsListModel> filteredResults) {
        if (filteredResults != null) {
            if (filteredResults.size() > 0) {
                ulduzumLocationsListModel = filteredResults;
                //create adapter
                ulduzumLocationsListAdapter = new UlduzumLocationsListAdapter(getActivity(),getActivity(), ulduzumLocationsListModel);
                binding.locationsList.setAdapter(ulduzumLocationsListAdapter);
                binding.locationsList.setAreHeadersSticky(false);
                ulduzumLocationsListAdapter.notifyDataSetChanged();
                binding.locationsList.setVisibility(View.VISIBLE);
                binding.noDataFoundLayout.setVisibility(View.GONE);
            } else {
                binding.locationsList.setVisibility(View.GONE);
                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            }
        } else {
            binding.locationsList.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }
    }//populateListData ends

    private void registerListener() {
        ulduzumLocationsListFilterListener = new UlduzumLocationsListFilterListener() {
            @Override
            public void onListLocationSelected(String categoryId, UlduzumLocationsServiceModel ulduzumLocationsServiceModel) {
                //filter locations on list from here
                if (categoryId.equalsIgnoreCase(Constants.UlduzumConstants.ULDUZUM_CATEGORIES_FIRST_ITEM)) {
                    //populate all data inside the adapter
                    populateListData(ulduzumLocationsServiceModel);
                } else {
                    filterLocationsOnList(categoryId, ulduzumLocationsServiceModel);
                }//if ends
            }//onListLocationSelected ends
        };

        RootValues.getInstance().setUlduzumLocationsListFilterListener(ulduzumLocationsListFilterListener);//saving the filter listener object
    }//registerListener ends

    private void filterLocationsOnList(String categoryId, UlduzumLocationsServiceModel model) {
        ArrayList<UlduzumLocationsListModel> filteredResults = new ArrayList<>();
        //match the id in the main datalist
        for (int i = 0; i < model.getMaindatalist().size(); i++) {
            if (categoryId.equalsIgnoreCase(model.getMaindatalist().get(i).getCategory_id())) {
                //skip the items without the lat long values
                if (Tools.hasValue(model.getMaindatalist().get(i).getCoord_lng()) && Tools.hasValue(model.getMaindatalist().get(i).getCoord_lat())) {
                    filteredResults.add(new UlduzumLocationsListModel(
                            model.getMaindatalist().get(i).getName(),
                            model.getMaindatalist().get(i).getCategory_name(),
                            model.getMaindatalist().get(i).getCoord_lat(),
                            model.getMaindatalist().get(i).getCoord_lng(),
                            model.getMaindatalist().get(i).getCategory_id(),
                            model.getMaindatalist().get(i).getDiscount(),
                            model.getMaindatalist().get(i).getId()));

                    BakcellLogger.logE("filteredMap",
                            "idkey:::" + categoryId
                                    + " catName:::" + model.getMaindatalist().get(i).getCategory_name(), fromClass, "filterLocationsOnList");
                }//if tools has value
            }//if ends
        }//for ends

        //create the filtered adapter
        populateListData(filteredResults);
    }//filterLocationsOnList ends

    private ArrayList<UlduzumLocationsListModel> getModelList(UlduzumLocationsServiceModel model) {
        ArrayList<UlduzumLocationsListModel> result = new ArrayList<>();

        //create the model
        for (int i = 0; i < model.getMaindatalist().size(); i++) {
            //skip the items without the lat long values
            if (Tools.hasValue(model.getMaindatalist().get(i).getCoord_lat()) && Tools.hasValue(model.getMaindatalist().get(i).getCoord_lng())) {
                String name = "";
                if (Tools.hasValue(model.getMaindatalist().get(i).getName())) {
                    name = model.getMaindatalist().get(i).getName();
                }
                String categoryName = "";
                if (Tools.hasValue(model.getMaindatalist().get(i).getCategory_name())) {
                    categoryName = model.getMaindatalist().get(i).getCategory_name();
                }
                String latitude = "";
                if (Tools.hasValue(model.getMaindatalist().get(i).getCoord_lat())) {
                    latitude = model.getMaindatalist().get(i).getCoord_lat();
                }
                String longitude = "";
                if (Tools.hasValue(model.getMaindatalist().get(i).getCoord_lng())) {
                    longitude = model.getMaindatalist().get(i).getCoord_lng();
                }
                String categoryId = "";
                if (Tools.hasValue(model.getMaindatalist().get(i).getCategory_id())) {
                    categoryId = model.getMaindatalist().get(i).getCategory_id();
                }
                String loyalitySegment = "";
                if (Tools.hasValue(model.getMaindatalist().get(i).getDiscount())) {
                    loyalitySegment = model.getMaindatalist().get(i).getDiscount();
                }
                String merchantId = "";
                if (Tools.hasValue(model.getMaindatalist().get(i).getId())) {
                    merchantId = model.getMaindatalist().get(i).getId();
                }

                result.add(new UlduzumLocationsListModel(
                        name,
                        categoryName,
                        latitude,
                        longitude,
                        categoryId,
                        loyalitySegment,
                        merchantId
                ));
            }//if tools has value
        }//for ends

        Collections.sort(result, new Comparator<UlduzumLocationsListModel>() {
            @Override
            public int compare(UlduzumLocationsListModel o1, UlduzumLocationsListModel o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        return result;
    }//getModelList ends

    @Override
    public void onClick(View view) {

    }//onClick ends
}//class ends
