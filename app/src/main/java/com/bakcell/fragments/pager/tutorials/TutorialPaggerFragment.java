package com.bakcell.fragments.pager.tutorials;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.databinding.FragmentTutorialOneBinding;

/**
 * Created by Zeeshan Shabbir on 7/3/2017.
 */

public class TutorialPaggerFragment extends Fragment implements View.OnClickListener {

    FragmentTutorialOneBinding binding;

    private OnSplashPageChangeListener onSplashPageChangeListener;


    public static final String TUTORIAL_TITLE = "tutorial.title";
    public static final String TUTORIAL_MESSAGE = "tutorial.message";
    public static final String TUTORIAL_POSTION = "tutorial.position";
    public static final String TUTORIAL_DRAWABLE = "tutorial.drawable";

    public static Fragment newInstance(Context context,
                                       @DrawableRes int id,
                                       String title,
                                       String message,
                                       int position) {
        Bundle b = new Bundle();
        b.putInt(TUTORIAL_DRAWABLE, id);
        b.putString(TUTORIAL_TITLE, title);
        b.putString(TUTORIAL_MESSAGE, message);
        b.putInt(TUTORIAL_POSTION, position);


        return Fragment.instantiate(context, TutorialPaggerFragment.class.getName(), b);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tutorial_one, container, false);
        initUiContent();
        return binding.getRoot();
    }

    private void initUiContent() {
        binding.icRight.setOnClickListener(this);
        binding.icLeft.setOnClickListener(this);
        binding.tvHeading.setText(getArguments().getString(TUTORIAL_TITLE));
        binding.tvMessage.setText(getArguments().getString(TUTORIAL_MESSAGE));
        binding.ivPlaceholder.setImageResource(getArguments().getInt(TUTORIAL_DRAWABLE));
        if (getArguments().getInt(TUTORIAL_POSTION) == 0) {
            binding.icLeft.setVisibility(View.INVISIBLE);
        } else if (getArguments().getInt(TUTORIAL_POSTION) == 4) {
            binding.icRight.setVisibility(View.INVISIBLE);
        } else {
            binding.icRight.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ic_left:
                onSplashPageChangeListener.changePage(-1);
                break;
            case R.id.ic_right:
                onSplashPageChangeListener.changePage(1);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onSplashPageChangeListener = (OnSplashPageChangeListener) context;
    }

    public interface OnSplashPageChangeListener {
        void changePage(int i);
    }
}
