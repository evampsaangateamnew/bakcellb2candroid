package com.bakcell.fragments.pager;

import android.app.DatePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.ExpandableAdapterRequestHistory;
import com.bakcell.adapters.SpinnerAdapterOperationHistoryFilter;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentPaggerRequestHistoryBinding;
import com.bakcell.fragments.dialogs.DatePickerFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.models.loan.RequestHistory;
import com.bakcell.models.loan.RequestHistoryMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestLoanHistoryService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;


/**
 * Created by Noman on 6/9/2017.
 */

public class RequestHistoryPaggerFragment extends Fragment implements View.OnClickListener {
    FragmentPaggerRequestHistoryBinding binding;

    protected View view;

    private ArrayList<RequestHistory> requestHistoryArrayList;

    private ExpandableAdapterRequestHistory expandableAdapterRequestHistory;

    private String startDate = "", endDate = "", date = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagger_request_history, container, false);

        initUiContent();

        initUiListeners();

//        try {
//            date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_NINETY_DAY);
//            updateDates(date, Tools.getCurrentDate());
//        } catch (ParseException e) {
//            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
//        }

        AppEventLogs.applyAppEvent(AppEventLogValues.LoanEvents.LOAN_SCREEN,
                AppEventLogValues.LoanEvents.LOAN_REQUEST_HISTORY_SCREEN,
                AppEventLogValues.LoanEvents.LOAN_SCREEN);
        return binding.getRoot();
    }

    private void updateDates(String newStartDate, String newEndDate) {

        startDate = newStartDate;
        endDate = newEndDate;
        requestForGetLoadHistory(startDate, endDate);
    }


    private void initUiListeners() {

        binding.spinnerPeriodButton.setOnClickListener(this);

        binding.spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    try {
                        updateDates(Tools.getCurrentDate(), Tools.getCurrentDate());
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 1) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_SEVEN_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 2) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_THIRTY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 3) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), Constants.DateAndMonth.LAST_NINETY_DAY);
                        updateDates(date, Tools.getCurrentDate());

                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else if (position == 4) {
                    showDatePicker();
                    binding.spinnerPeriod.setSelection(0);
                } else if (position == 5) {
                    try {
                        date = "";
                        date = Tools.getLastDate(Tools.getCurrentDate(), (365 * 3));
                        updateDates(date, Tools.getCurrentDate());
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });

    }

    private void initUiContent() {

        binding.itemCounterText.setText("0/0");

        requestHistoryArrayList = new ArrayList<>();
        expandableAdapterRequestHistory = new ExpandableAdapterRequestHistory(requestHistoryArrayList,
                getActivity());
        binding.expandableRequestHistoryListview.setAdapter(expandableAdapterRequestHistory);

        SpinnerAdapterOperationHistoryFilter spinnerAdapterPeriodFilter = new
                SpinnerAdapterOperationHistoryFilter(getActivity(),
                getResources().getStringArray(R.array.period_filter_rq_history));
        binding.spinnerPeriod.setAdapter(spinnerAdapterPeriodFilter);

        binding.expandableRequestHistoryListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                binding.itemCounterText.setText((firstVisibleItem + visibleItemCount) + "/" + totalItemCount);

            }
        });

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.spinnerPeriodButton:
                binding.spinnerPeriod.performClick();
        }
    }


    public void requestForGetLoadHistory(String startDate, String endDate) {


        RequestLoanHistoryService.newInstance(getActivity(), ServiceIDs.REQUEST_LOAN_HISTORY).execute(DataManager.getInstance().getCurrentUser(),
                startDate, endDate, new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();

                        RequestHistoryMain historyMain = new Gson().fromJson(result, RequestHistoryMain.class);

                        if (historyMain != null && historyMain.getRequestHistoriesList() != null) {
                            loadRequestHistory(historyMain.getRequestHistoriesList());
                        }

                    }


                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }

                });

    }

    /**
     * Load request history to UI
     *
     * @param requestHistoriesList
     */
    private void loadRequestHistory(ArrayList<RequestHistory> requestHistoriesList) {

        if (requestHistoriesList != null && requestHistoriesList.size() > 0 && expandableAdapterRequestHistory != null) {
            binding.expandableRequestHistoryListview.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            expandableAdapterRequestHistory.updateData(requestHistoriesList);
        } else {
            binding.expandableRequestHistoryListview.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            binding.itemCounterText.setText("0/0");

        }

    }


    private void showDatePicker() {

        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */


        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);

        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getActivity().getSupportFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            monthOfYear++;
            String dateNew = String.valueOf(year) + "-" + String.format("%02d", monthOfYear)
                    + "-" + String.format("%02d", dayOfMonth);

            updateDates(dateNew, dateNew);
        }
    };


}
