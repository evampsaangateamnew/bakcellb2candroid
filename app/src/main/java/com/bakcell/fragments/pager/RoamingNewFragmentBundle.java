package com.bakcell.fragments.pager;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.adapters.CardAdapterSupplementaryOffers;
import com.bakcell.databinding.ActivityRoamingOfferBinding;
import com.bakcell.databinding.FragmentRoamingNewBundleBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.SearchResultListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.roamingnewmodels.ServiceListHelperModel;
import com.bakcell.models.supplementaryoffers.OfferFilter;
import com.bakcell.models.supplementaryoffers.OfferFiltersMain;
import com.bakcell.models.supplementaryoffers.RoamingsOffer;
import com.bakcell.models.supplementaryoffers.SupplementaryOfferMain;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.GetSupplementaryOffersService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.RecyclerItemDecorator;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class RoamingNewFragmentBundle extends Fragment implements SearchResultListener {

    FragmentRoamingNewBundleBinding binding;
    private CardAdapterSupplementaryOffers roamingAdapter;
    private ArrayList<SupplementaryOffer> offerList;
    public static final String KEY_ROAMING_OFFERS = "key.roaming.offers";
    public static final String KEY_ROAMING_FLAG = "key.roaming.flag";
    public static final String KEY_ROAMING_COUNTRY = "key.roaming.country";
    private SupplementaryOfferMain offerMain;


    public static RoamingNewFragmentBundle getInstance(String selectedCountry, String flag) {
        RoamingNewFragmentBundle fragment = new RoamingNewFragmentBundle();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_ROAMING_FLAG, flag);
        bundle.putString(KEY_ROAMING_COUNTRY, selectedCountry);
        fragment.setArguments(bundle);
        return fragment;
    }//getInstance ends


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(KEY_ROAMING_FLAG)) {
            flag = getArguments().getString(KEY_ROAMING_FLAG);
        }
        if (getArguments().containsKey(KEY_ROAMING_COUNTRY)) {
            selectedCountry = getArguments().getString(KEY_ROAMING_COUNTRY);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_roaming_new_bundle, container, false);
        offerMain = DataManager.getInstance().getSupplementaryOfferMainData();

        if (offerMain == null)
            requestForGetSupplementaryOffers();
        else
            loadUIAfterApi(offerMain);

        return binding.getRoot();
    }

    private void requestForGetSupplementaryOffers() {

        UserModel customerData = DataManager.getInstance().getCurrentUser();

        if (customerData == null) {
            Toast.makeText(getActivity(), R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        GetSupplementaryOffersService.newInstance(getActivity(),
                ServiceIDs.GET_SUPPLEMENTARY_OFFERS).execute(customerData,
                new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request,
                                          EaseResponse<String> response) {

                        RootValues.getInstance().setSupplementaryOffersApiCall(false);

                        try {
                            String result = response.getData();
                            offerMain = new Gson().fromJson(result, SupplementaryOfferMain.class);
                            if (offerMain != null) {
                                loadUIAfterApi(offerMain);

                            }
                        } catch (JsonSyntaxException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }

                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request,
                                          EaseResponse<String> response) {
                        // Check if user logout by server
                        if (response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getContext());
                        String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                if (offerMain != null) {
                                    loadUIAfterApi(offerMain);
                                }
                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        if (response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        // Load Cashed Data
                        String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getContext());
                        String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                        if (Tools.hasValue(cacheResponse)) {
                            try {
                                JSONObject jsonObject = new JSONObject(cacheResponse);
                                String cacheData = jsonObject.getJSONObject("data").toString();
                                offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                if (offerMain != null) {
                                    loadUIAfterApi(offerMain);
                                }
                            } catch (JSONException ex) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                            }
                        }

                        BakcellPopUpDialog.ApiFailureMessage(getActivity());
                    }
                });

    }

    private void loadUIAfterApi(SupplementaryOfferMain offerMain) {

        getRoamingCountryBundle(offerMain.getRoaming());

    }


    void setUpRecyclerView(ArrayList<SupplementaryOffer> offerMain) {
        offerList = offerMain;
        binding.mDiscreteView.setOrientation(Orientation.HORIZONTAL);
        binding.mDiscreteView.setItemTransformer(new ScaleTransformer.Builder()
                .build());
        binding.mDiscreteView.addItemDecoration(new RecyclerItemDecorator());
        binding.mDiscreteView.setNestedScrollingEnabled(false);
        binding.mDiscreteView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        binding.mDiscreteView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                int action = e.getAction();
                if (rv.canScrollHorizontally(RecyclerView.FOCUS_FORWARD)) {
                    switch (action) {
                        case MotionEvent.ACTION_MOVE:
                            rv.getParent()
                                    .requestDisallowInterceptTouchEvent(true);
                    }
                    return false;

                } else {
                    switch (action) {
                        case MotionEvent.ACTION_MOVE:
                            rv.getParent()
                                    .requestDisallowInterceptTouchEvent(false);
                    }
                    rv.removeOnItemTouchListener(this);
                    return true;

                }
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        roamingAdapter = new CardAdapterSupplementaryOffers(getContext(), getActivity(),
                offerList, this, true, true, true
                , getString(R.string.supplementary_offer_tab_title_roaming));
        binding.mDiscreteView.setAdapter(roamingAdapter);
        setUpPagination(binding.mDiscreteView);
        roamingAdapter.notifyDataSetChanged();
    }

    private int prevCenterPos;


    public void setUpPagination(RecyclerView recyclerView) {
        if (roamingAdapter == null) return;
        if (roamingAdapter.getItemCount() > 0) {
            binding.tvPagination.setText("1/" + roamingAdapter.getItemCount());
        } else {
            binding.tvPagination.setText("0/0");
        }
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int center = recyclerView.getWidth() / 2;
                View centerView = recyclerView.findChildViewUnder(center, recyclerView.getTop());
                int centerPos = recyclerView.getChildAdapterPosition(centerView);

                if (prevCenterPos != centerPos) {
                    // dehighlight the previously highlighted view
                    prevCenterPos = centerPos;
                }
                Logger.debugLog("POSTION", prevCenterPos + "");
                binding.tvPagination.setText(prevCenterPos + 1 + "/" +
                        recyclerView.getAdapter().getItemCount());
            }
        });
    }

    @Override
    public void onSearchResult(boolean isResultFount) {

    }

    private ArrayList<SupplementaryOffer> filteredRoamingOffers;

    String selectedCountry;
    String flag;

    void getRoamingCountryBundle(RoamingsOffer roamingsOffer) {
        flag = "";
        filteredRoamingOffers = new ArrayList<>();
        if (selectedCountry != null) {
            if (roamingsOffer.getOffers() != null) {
                if (roamingsOffer.getCountries() != null && roamingsOffer.getOffers().size() > 0) {

                    for (int i = 0; i < roamingsOffer.getOffers().size(); i++) {
                        if (roamingsOffer.getOffers().get(i).getDetails() != null) {
                            if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails() != null) {
                                if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails()
                                        .getRoamingDetailsCountriesList() != null &&
                                        roamingsOffer.getOffers().get(i).getDetails()
                                                .getRoamingDetails().getRoamingDetailsCountriesList().size() > 0) {
                                    for (int j = 0; j < roamingsOffer.getOffers().get(i).getDetails()
                                            .getRoamingDetails().getRoamingDetailsCountriesList().size(); j++) {
                                        if (roamingsOffer.getOffers().get(i).getDetails().getRoamingDetails()
                                                .getRoamingDetailsCountriesList().get(j).getCountryName().trim()
                                                .equalsIgnoreCase(selectedCountry.trim())) {

                                            if (!Tools.hasValue(flag))
                                                flag = roamingsOffer.getOffers().get(i).getDetails()
                                                        .getRoamingDetails()
                                                        .getRoamingDetailsCountriesList().get(j).getFlag();
                                            filteredRoamingOffers.add(roamingsOffer.getOffers().get(i));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (filteredRoamingOffers.size() > 0) {
                setUpRecyclerView(filteredRoamingOffers);
                DataManager.getInstance().setSwipeDisableCheckForOperators(true);
            } else
            {
                DataManager.getInstance().setSwipeDisableCheckForOperators(false);
                binding.tvPagination.setVisibility(View.GONE);
                binding.mDiscreteView.setVisibility(View.GONE);
                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            }
        }


    }
}