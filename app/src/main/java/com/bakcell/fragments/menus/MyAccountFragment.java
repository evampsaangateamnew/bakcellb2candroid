package com.bakcell.fragments.menus;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.MyInstallmentsActivity;
import com.bakcell.activities.landing.MySubscriptionsActivity;
import com.bakcell.activities.landing.OperationsHistoryActivity;
import com.bakcell.activities.landing.UsageHistoryActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentAccountBinding;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.dashboard.installments.InstallmentsMain;
import com.bakcell.utilities.PrefUtils;

/**
 * Created by Freeware Sys on 18-May-17.
 */

public class MyAccountFragment extends Fragment implements View.OnClickListener {

    FragmentAccountBinding binding;

    View rootView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false);
        initUiContent();
        initUiListeners();
        AppEventLogs.applyAppEvent(AppEventLogValues.AccountEvents.MY_ACCOUNT,
                AppEventLogValues.AccountEvents.MY_ACCOUNT,
                AppEventLogValues.AccountEvents.MY_ACCOUNT);
        return binding.getRoot();
    }

    private void initUiContent() {
        binding.tvUsageHistory.setSelected(true);
        binding.tvMyInstallments.setSelected(true);
        binding.tvMySubscription.setSelected(true);
        binding.tvOpeartionHistory.setSelected(true);
    }

    private void initUiListeners() {
        binding.tvUsageHistory.setSelected(true);
        binding.subscriptionLayout.setOnClickListener(this);
        binding.usageHistoryLayout.setOnClickListener(this);
        binding.operationHistoryLayout.setOnClickListener(this);
        binding.myinstallmentLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.subscription_layout:
                BaseActivity.startNewActivity(getActivity(), MySubscriptionsActivity.class);
                break;
            case R.id.usage_history_layout:
                BaseActivity.startNewActivity(getActivity(), UsageHistoryActivity.class);
                break;
            case R.id.operation_history_layout:
                BaseActivity.startNewActivity(getActivity(), OperationsHistoryActivity.class);
                break;
            case R.id.myinstallment_layout:

                // getString Installment data from Shared Preferences to show on My Installment page
                if (getActivity() != null && !getActivity().isFinishing()) {

                    InstallmentsMain installmentsMain = PrefUtils.getAsJson(getActivity(),
                            MultiAccountsHandler.CacheKeys.getUserInstallmentsCacheKey(getActivity()), InstallmentsMain.class);

                    if (installmentsMain != null && installmentsMain.getInstallmentes() != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString(MyInstallmentsActivity.KEY_ISNTALLMENTS_TITLE, installmentsMain.getInstallmentDescription());
                        bundle.putParcelableArrayList(MyInstallmentsActivity.KEY_ISNTALLMENTS_LIST, installmentsMain.getInstallmentes());
                        BaseActivity.startNewActivity(getActivity(), MyInstallmentsActivity.class, bundle);
                    }

                }


                break;
        }
    }
}
