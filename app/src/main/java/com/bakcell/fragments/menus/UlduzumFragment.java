package com.bakcell.fragments.menus;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.activities.landing.UlduzumLocationsActivity;
import com.bakcell.activities.landing.UlduzumUsageHistoryActivity;
import com.bakcell.adapters.UlduzumUnusedCodesAdapter;
import com.bakcell.databinding.UlduzumFragmentBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.ulduzum.UlduzumGetCodesModel;
import com.bakcell.models.ulduzum.UlduzumUnusedCodesModel;
import com.bakcell.models.ulduzum.UlduzumUsageDetails;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestUlduzumCodesWebService;
import com.bakcell.webservices.UlduzumRequestWebService;
import com.bakcell.webservices.UlduzumUnusedCodesWebService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class UlduzumFragment extends Fragment {

    private UlduzumFragmentBinding binding;
    private UlduzumUsageDetails ulduzumUsageDetails = null;
    private UlduzumUnusedCodesModel ulduzumUnusedCodesModel = null;
    private UlduzumUnusedCodesAdapter ulduzumUnusedCodesAdapter = null;
    private final int LOCATION_PERMISSION = 12;
    private UlduzumGetCodesModel ulduzumGetCodesModel;
    private String totalCodesLimit = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.ulduzum_fragment, container, false);

        initUiContent();
        initUiListeners();

        requestUlduzum();
        return binding.getRoot();
    }//onCreateView ends

    private void requestUlduzum() {
        //check if the current activity is not dead
        if (getActivity() == null || getActivity().isFinishing()) {
            return; //safe passage
        }

        if (DataManager.getInstance().getCurrentUser() == null) {
            Toast.makeText(getActivity(), R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        //set the ulduzumTotalDiscountsSubLabel
        if (DataManager.getInstance().getCurrentUser() != null) {
            if (DataManager.getInstance().getCurrentUser().getLoyaltySegment() != null) {
                if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLoyaltySegment())) {
                    binding.ulduzumTotalDiscountsSubLabel.setText(DataManager.getInstance().getCurrentUser().getLoyaltySegment());
                }
            }
        }

        UlduzumRequestWebService.newInstance(getActivity(),
                ServiceIDs.ULDUZUM_USAGE_DETAILS_ID).
                execute(DataManager.getInstance().getCurrentUser(), false/*this should not be in background*/,
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                                //check if the current activity is not dead yet!
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    String result = response.getData();
                                    ulduzumUsageDetails = new Gson().fromJson(result, UlduzumUsageDetails.class);
                                    if (ulduzumUsageDetails != null) {
                                        binding.noDataFoundLayout.setVisibility(View.GONE);
                                        binding.contentContainer.setVisibility(View.VISIBLE);
                                        initiateUI(ulduzumUsageDetails);
                                    } else {
                                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                        binding.contentContainer.setVisibility(View.GONE);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (getActivity() != null && !getActivity().isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                    return;
                                }
                                // commented as per client request , date 31/12/2020
                                 /*
                                // Load Cashed Data
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getUlduzumCacheKey(getActivity());
                                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            ulduzumUsageDetails = new Gson().fromJson(cacheData, UlduzumUsageDetails.class);
                                            if (ulduzumUsageDetails != null) {
                                                binding.noDataFoundLayout.setVisibility(View.GONE);
                                                binding.contentContainer.setVisibility(View.VISIBLE);
                                                initiateUI(ulduzumUsageDetails);
                                            } else {
                                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                binding.contentContainer.setVisibility(View.GONE);
                                            }
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                            binding.contentContainer.setVisibility(View.GONE);
                                        }
                                    }
                                }
*/

                                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                                }
                                // commented as per client request , date 31/12/2020
                                /*
                                // Load Cashed Data
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getUlduzumCacheKey(getActivity());
                                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            ulduzumUsageDetails = new Gson().fromJson(cacheData, UlduzumUsageDetails.class);
                                            if (ulduzumUsageDetails != null) {
                                                binding.noDataFoundLayout.setVisibility(View.GONE);
                                                binding.contentContainer.setVisibility(View.VISIBLE);
                                                initiateUI(ulduzumUsageDetails);
                                            } else {
                                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                binding.contentContainer.setVisibility(View.GONE);
                                            }
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                            binding.contentContainer.setVisibility(View.GONE);
                                        }
                                    }
                                }
*/

                            }
                        });
    }//requestUlduzum ends

    private void initiateUI(UlduzumUsageDetails ulduzum) {
        //check if the current activity is not dead
        if (getActivity() == null || getActivity().isFinishing()) {
            return; //safe passage
        }

        //set the total discounted value
        if (Tools.hasValue(ulduzum.getTotal_discounted())) {
            binding.ulduzumAmount.setText(ulduzum.getTotal_discount());
        }

        //daily_limit_left
        String dailyLimitLeft = "0";
        if (Tools.hasValue(ulduzum.getDaily_limit_left())) {
            dailyLimitLeft = ulduzum.getDaily_limit_left();
        }

        //total_limit
        String totalLimit = "0";
        if (Tools.hasValue(ulduzum.getDaily_limit_total())) {
            totalLimit = ulduzum.getDaily_limit_total();
        }

        binding.remainingCodesValue.setText(dailyLimitLeft + "/" + totalLimit);
        totalCodesLimit = totalLimit;
        try {
            //progress bar
            int min = Integer.parseInt(dailyLimitLeft);
            int max = Integer.parseInt(totalLimit);

            binding.ulduzumProgress.setMax(max);
            binding.ulduzumProgress.setProgress(min);
        } catch (Exception exp) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, exp.getMessage());
        }
    }//initiateUI ends

    private void initUiContent() {
        binding.ulduzumAmount.setSelected(true);
        binding.ulduzumUsageHistoryLabel.setSelected(true);
        binding.ulduzumLocationsLabel.setSelected(true);
        binding.ulduzumTotalDiscountsLabel.setSelected(true);
        binding.getCodesButton.setSelected(true);
        binding.unusedCodesButton.setSelected(true);
        binding.remainingCodesLabel.setSelected(true);
        binding.ulduzumTotalDiscountsSubLabel.setSelected(true);

        //set the ulduzumTotalDiscountsSubLabel
        if (DataManager.getInstance().getCurrentUser() != null) {
            if (DataManager.getInstance().getCurrentUser().getLoyaltySegment() != null) {
                if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLoyaltySegment())) {
                    binding.ulduzumTotalDiscountsSubLabel.setText(DataManager.getInstance().getCurrentUser().getLoyaltySegment());
                }
            }
        }
    }//initUiContent ends

    private void initUiListeners() {
        binding.getCodesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //handle get codes tap
                getUlduzumCodeClicked();
//                initiateUlduzumCode("1289"); this is just for testing you can test it
            }
        });

        binding.unusedCodesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //handle unused codes tap
                requestUnusedCodes();
            }
        });

        binding.usageHistoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //handle usage history tap
                BaseActivity.startNewActivity(getActivity(), UlduzumUsageHistoryActivity.class);
            }
        });

        binding.locationsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //permission
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION);
                    return;
                } else {
                    //handle locations tap
                    BaseActivity.startNewActivity(getActivity(), UlduzumLocationsActivity.class);
                }
            }
        });

    }//initUiListeners ends

    @SuppressLint("InflateParams")
    protected void getUlduzumCodeClicked() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_ulduzum_confirmation, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button yesBtn = (Button) dialogView.findViewById(R.id.btn_yes);

        BakcellTextViewNormal tvConfirmationMsg = (BakcellTextViewNormal) dialogView
                .findViewById(R.id.tv_top_confirmation_msg);
        BakcellTextViewBold tvTitle = (BakcellTextViewBold) dialogView
                .findViewById(R.id.dialog_title);

        String confirmationStr = getResources().getString(R.string.ulduzum_get_codes_confirmation);
        tvConfirmationMsg.setText(Html.fromHtml(confirmationStr));
        tvTitle.setSelected(true);

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestGetCodes();
                alertDialog.dismiss();
            }
        });

        Button noBtn = (Button) dialogView.findViewById(R.id.btn_no);
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    public void requestGetCodes() {
        //check if the current activity is not dead
        if (getActivity() == null || getActivity().isFinishing()) {
            return; //safe passage
        }

        RequestUlduzumCodesWebService.newInstance(getActivity(), ServiceIDs.ULDUZUM_GET_CODES_ID)
                .execute(DataManager.getInstance().getCurrentUser(), "",
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                String result = response.getData();
                                if (result == null) return;
                                ulduzumGetCodesModel = new Gson().fromJson(result, UlduzumGetCodesModel.class);
                                if (ulduzumGetCodesModel != null) {
                                    if (Tools.hasValue(ulduzumGetCodesModel.getCode())) {
                                        initiateUlduzumCode(ulduzumGetCodesModel.getCode());
                                    }
// popup removed which was showing when empty value return in codes.
//                                    else {
//                                        if (!getActivity().isFinishing() && response != null) {
//                                            BakcellPopUpDialog.showMessageDialog(getActivity(),
//                                                    getString(R.string.bakcell_error_title), response.getDescription());
//                                        }
//                                    }
                                }
//popup removed which was showing when Null return in codes.
//                                else {
//                                    if (!getActivity().isFinishing() && response != null) {
//                                        BakcellPopUpDialog.showMessageDialog(getActivity(),
//                                                getString(R.string.bakcell_error_title), response.getDescription());
//                                    }
//                                }
                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (getActivity() != null && !getActivity().isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                    return;
                                }

                                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                                }
                            }

                        });

    }//requestGetCodes ends

    private void initiateUlduzumCode(String code) {
        //check if the current activity is not dead
        if (getActivity() == null || getActivity().isFinishing()) {
            return; //safe passage
        }

        if (Tools.hasValue(code)) {
            //parse the code for empty strings
            String parsedCode = code.replace(" ", "");
            parsedCode = parsedCode.trim();
            //check the code and its length
            if (Tools.hasValue(parsedCode)) {
                //hide all aestriks
                //set the codes if length is 4
                if (parsedCode.length() == 4) {
                    //code 1
                    binding.ulduzumAestrik1.setVisibility(View.GONE);
                    binding.ulduzumCode1.setText("" + parsedCode.charAt(0));
                    binding.ulduzumCode1.setVisibility(View.VISIBLE);
                    //code 2
                    binding.ulduzumAestrik2.setVisibility(View.GONE);
                    binding.ulduzumCode2.setText("" + parsedCode.charAt(1));
                    binding.ulduzumCode2.setVisibility(View.VISIBLE);
                    //code 3
                    binding.ulduzumAestrik3.setVisibility(View.GONE);
                    binding.ulduzumCode3.setText("" + parsedCode.charAt(2));
                    binding.ulduzumCode3.setVisibility(View.VISIBLE);
                    //code 4
                    binding.ulduzumAestrik4.setVisibility(View.GONE);
                    binding.ulduzumCode4.setText("" + parsedCode.charAt(3));
                    binding.ulduzumCode4.setVisibility(View.VISIBLE);

                    /**
                     * update the progress after that the user has got the code
                     * suppose that user had got the code so total-1
                     * the redemptions left today will be left/total i.e. 9/10
                     * have been commented out since 04-03-2019*/
                    /*//update the progress
                    int existingProgress = 0;
                    String remaingCodesValue = "";
                    try {
                        existingProgress = binding.ulduzumProgress.getProgress();
                        if (existingProgress > 0 && Tools.hasValue(totalCodesLimit)) {
                            existingProgress -= 1;
                            remaingCodesValue = "" + existingProgress + "/" + totalCodesLimit;
                        }
                        //set the progress
                        binding.ulduzumProgress.setProgress(existingProgress);
                        binding.remainingCodesValue.setText(remaingCodesValue);
                    } catch (Exception exp) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, "error:::" + exp.toString());
                    }*/
                }
            }
        }

        /**after getting the ulduzum code recall the request ulduzum API
         * to refresh the UI
         * silently in the background*/
        requestUlduzumInBackground();
    }//initiateUlduzumCode ends

    private void requestUlduzumInBackground() {
        //check if the current activity is not dead
        if (getActivity() == null || getActivity().isFinishing()) {
            return; //safe passage
        }

        if (DataManager.getInstance().getCurrentUser() == null) {
            return;
        }

        //set the ulduzumTotalDiscountsSubLabel
        if (DataManager.getInstance().getCurrentUser() != null) {
            if (DataManager.getInstance().getCurrentUser().getLoyaltySegment() != null) {
                if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getLoyaltySegment())) {
                    binding.ulduzumTotalDiscountsSubLabel.setText(DataManager.getInstance().getCurrentUser().getLoyaltySegment());
                }
            }
        }

        /**This api should be called in the background
         * after getting an ulduzum code by the button*/

        UlduzumRequestWebService.newInstance(getActivity(),
                ServiceIDs.ULDUZUM_USAGE_DETAILS_ID).
                execute(DataManager.getInstance().getCurrentUser(), true/*this should be in background*/,
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                                //check if the current activity is not dead yet!
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    String result = response.getData();
                                    ulduzumUsageDetails = new Gson().fromJson(result, UlduzumUsageDetails.class);
                                    if (ulduzumUsageDetails != null) {
                                        binding.noDataFoundLayout.setVisibility(View.GONE);
                                        binding.contentContainer.setVisibility(View.VISIBLE);
                                        initiateUI(ulduzumUsageDetails);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (getActivity() != null && !getActivity().isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                // simply do nothing
                            }
                        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    BaseActivity.startNewActivity(getActivity(), UlduzumLocationsActivity.class);//start the activity here therefore
                }// if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)  ends
                break;
        }//switch ends
    }//onRequestPermissionsResult ends

    private void requestUnusedCodes() {
        //check if the current activity is not dead
        if (getActivity() == null || getActivity().isFinishing()) {
            return; //safe passage
        }

        if (DataManager.getInstance().getCurrentUser() == null) {
            Toast.makeText(getActivity(), R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }//if (DataManager.getInstance().getCurrentUser() == null) ends

        UlduzumUnusedCodesWebService.newInstance(getActivity(),
                ServiceIDs.ULDUZUM_UNUSED_CODES_ID).
                execute(DataManager.getInstance().getCurrentUser(),
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                                String result = response.getData();
                                ulduzumUnusedCodesModel = new Gson().fromJson(result, UlduzumUnusedCodesModel.class);
                                if (ulduzumUnusedCodesModel != null) {
                                    if (ulduzumUnusedCodesModel.getUnusedcodeslist() != null) {
                                        if (ulduzumUnusedCodesModel.getUnusedcodeslist().size() > 0) {
                                            initiateUlduzumUnusedCodesPopup(ulduzumUnusedCodesModel);
                                        } else {
                                            //no data found
                                            BakcellPopUpDialog.showMessageDialog(getActivity(), getActivity().getResources().getString(R.string.mesg_successful_title),
                                                    getActivity().getResources().getString(R.string.no_data_found));
                                        }
                                    } else {
                                        //no data found
                                        BakcellPopUpDialog.showMessageDialog(getActivity(), getActivity().getResources().getString(R.string.mesg_successful_title),
                                                getActivity().getResources().getString(R.string.no_data_found));
                                    }
                                } else {
                                    //response has no data
                                    BakcellPopUpDialog.showMessageDialog(getActivity(), getActivity().getResources().getString(R.string.mesg_successful_title),
                                            getActivity().getResources().getString(R.string.no_data_found));
                                }
                            }//onSuccess ends

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (!getActivity().isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                    return;
                                }

                                if (!getActivity().isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }
                            }//onFailure ends

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (!getActivity().isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                                }
                            }//onError ends
                        });
    }//requestUnusedCodes ends

    @SuppressLint("InflateParams")
    private void initiateUlduzumUnusedCodesPopup(UlduzumUnusedCodesModel model) {
        //check if the current activity is not dead
        if (getActivity() == null || getActivity().isFinishing()) {
            return; //safe passage
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.ulduzum_unused_codes_popup, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog unusedCodesDialog = dialogBuilder.create();
        unusedCodesDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        unusedCodesDialog.show();

        ListView unusedCodesList = dialogView.findViewById(R.id.unusedCodesList);
        BakcellButtonNormal buttonOk = dialogView.findViewById(R.id.okButton);
        TextView codeLabel = dialogView.findViewById(R.id.codeLabel);
        TextView codeDateLabel = dialogView.findViewById(R.id.codeDateLabel);
        codeLabel.setSelected(true);
        codeDateLabel.setSelected(true);

        //set the adapter
        ulduzumUnusedCodesAdapter = new UlduzumUnusedCodesAdapter(getActivity(), model);
        unusedCodesList.setAdapter(ulduzumUnusedCodesAdapter);

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unusedCodesDialog.dismiss();
            }
        });//buttonOk.setOnClickListener ends

    }//initiateUlduzumUnusedCodesPopup ends
}//class ends
