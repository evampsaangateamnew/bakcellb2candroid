package com.bakcell.fragments.menus;

import static com.bakcell.globals.Constants.RoamingBottomNavConstants.ROAMING_ACTIVATION_FLAG;
import static com.bakcell.globals.Constants.RoamingBottomNavConstants.ROAMING_ACTIVATION_OFFERING_ID;
import static com.bakcell.globals.Constants.RoamingBottomNavConstants.ROAMING_ACTIVATION_STATUS_PRODUCT_ID;
import static com.bakcell.globals.Constants.RoamingBottomNavConstants.ROAMING_DEACTIVATION_FLAG;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.activities.landing.RoamingCountryListActivity;
import com.bakcell.activities.landing.RoamingCountrySearchActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentBottomNavRoamingBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.roamingactivationstatus.RoamingActivationStatusModel;
import com.bakcell.models.supplementaryoffers.SupplementaryOfferMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.models.user.UserPredefinedData;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.GetSupplementaryOffersService;
import com.bakcell.webservices.RequestEnableDisableRoamingActivation;
import com.bakcell.webservices.RequestRoamingActivationStatus;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * @author Umair Mustafa
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */

public class RoamingBottomNavFragment extends Fragment implements View.OnClickListener {

    FragmentBottomNavRoamingBinding binding;
    SupplementaryOfferMain offerMain;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_nav_roaming, container, false);
        initUiContent();
        initUiListeners();
        //as directed by SA to fix flag issue We call this api and save it response in DataManager.
        requestForGetSupplementaryOffers();

        AppEventLogs.applyAppEvent(AppEventLogValues.RoamingScreen.ROAMING_SCREEN,
                AppEventLogValues.RoamingScreen.ROAMING_SCREEN,
                AppEventLogValues.RoamingScreen.ROAMING_SCREEN);

        return binding.getRoot();
    }

    private void initUiContent() {
        //if Roaming Activation visibility check in Predefined Data is true then show roaming activation Row else hide
        UserPredefinedData userPredefinedData = DataManager.getInstance().getUserPredefinedData();
        if (userPredefinedData != null) {
            if (userPredefinedData.getRoamingVisible()) {
                binding.mainRoamingLayout.setVisibility(View.VISIBLE);
            } else {
                binding.mainRoamingLayout.setVisibility(View.GONE);
            }
        }
    } // initUiContent ends

    private void initUiListeners() {
        binding.packagesLayout.setOnClickListener(this);
        binding.priceOperatorsLayout.setOnClickListener(this);
        binding.roamingActivationLayout.setOnClickListener(this);
    } //initUiListeners ends

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.roaming_activation_layout:
                callRoamingActivationApi();
                break;
            case R.id.price_operators_layout:
                BaseActivity.startNewActivity(getActivity(), RoamingCountryListActivity.class);
                break;
            case R.id.packages_layout:
                BaseActivity.startNewActivity(getActivity(), RoamingCountrySearchActivity.class);
                break;
        }
    } //onClick ends

    private void callRoamingActivationApi() {
        if (binding.switchRoamingActivation.isChecked()) {
            // call for deactivation with actionTYpe  1
            RequestRoamingActivation(ROAMING_ACTIVATION_OFFERING_ID, ROAMING_DEACTIVATION_FLAG);
        } else {
            //call for activiation with actionType 2
            RequestRoamingActivation(ROAMING_ACTIVATION_OFFERING_ID, ROAMING_ACTIVATION_FLAG);
        }
    } //callRoamingActivationApi ends

    //as directed by SA to fix flag issue We call this api and save it response in DataManager.
    private void requestForGetSupplementaryOffers() {
        //check if supplementary offers API is cached then call Roaming Activation Status Check API directly
        //otherwise if Supplementary offers API is not cached then call Supplementary Offer first then Roaming Activation Status API
        if (DataManager.getInstance().getSupplementaryOfferMainData() != null) {
            requestRoamingActivationStatus(ROAMING_ACTIVATION_STATUS_PRODUCT_ID);
        } else {
            UserModel customerData = DataManager.getInstance().getCurrentUser();

            if (customerData == null) {
                Toast.makeText(getActivity(), R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
                return;
            }

            GetSupplementaryOffersService.newInstance(getActivity(),
                    ServiceIDs.GET_SUPPLEMENTARY_OFFERS).execute(customerData,
                    new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {
                            RootValues.getInstance().setSupplementaryOffersApiCall(false);
                            try {
                                String result = response.getData();
                                SupplementaryOfferMain offerMain = new Gson().fromJson(result, SupplementaryOfferMain.class);
                                if (offerMain != null) {
                                    DataManager.getInstance().setSupplementaryOfferMainData(offerMain);
                                }
                            } catch (Exception e) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                            }
                            requestRoamingActivationStatus(ROAMING_ACTIVATION_STATUS_PRODUCT_ID);
                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {
                            // Check if user logout by server
                            if (getActivity() != null && !getActivity().isFinishing() && response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(getActivity());
                                return;
                            }

                            // Load Cashed Data
                            String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getContext());
                            String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                            if (Tools.hasValue(cacheResponse)) {
                                try {
                                    JSONObject jsonObject = new JSONObject(cacheResponse);
                                    String cacheData = jsonObject.getJSONObject("data").toString();
                                    offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                    if (offerMain != null) {
                                        DataManager.getInstance().setSupplementaryOfferMainData(offerMain);

                                    }
                                } catch (JSONException ex) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                }
                            }
                            requestRoamingActivationStatus(ROAMING_ACTIVATION_STATUS_PRODUCT_ID);
                            if (response != null) {
                                BakcellPopUpDialog.showMessageDialog(getActivity(),
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            // Load Cashed Data
                            if (getActivity() != null && !getActivity().isFinishing() && getContext() != null) {
                                String cacheKey = MultiAccountsHandler.CacheKeys.getSupplementaryOffersCacheKey(getContext());
                                String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                if (Tools.hasValue(cacheResponse)) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(cacheResponse);
                                        String cacheData = jsonObject.getJSONObject("data").toString();
                                        offerMain = new Gson().fromJson(cacheData, SupplementaryOfferMain.class);
                                        if (offerMain != null) {
                                            DataManager.getInstance().setSupplementaryOfferMainData(offerMain);
                                        }
                                    } catch (Exception ex) {
                                        Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                    }
                                }
                                requestRoamingActivationStatus(ROAMING_ACTIVATION_STATUS_PRODUCT_ID);
                                BakcellPopUpDialog.ApiFailureMessage(getActivity());
                            }
                        }
                    });

        }

    } //requestForGetSupplementaryOffers ends

    /*
    Request to change Roaming Activation Status
     */
    private void RequestRoamingActivation(String offerid, String actionType) {

        if (DataManager.getInstance().getCurrentUser() != null && getActivity() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();

            RequestEnableDisableRoamingActivation.newInstance(getActivity(), ServiceIDs.REQUEST_ENABLE_DISABLE_ROAMING_ACTIVATION)
                    .execute(userModel, actionType, offerid, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            if (getActivity() == null) {
                                return; //safe passage
                            }
                            if (getActivity().isFinishing()) {
                                return;
                            }
                            if (response != null && response.getDescription() != null) {
                                BakcellPopUpDialog.showMessageDialog(getContext(), getString(R.string.mesg_successful_title), response.getDescription());
                            }
                            binding.switchRoamingActivation.setChecked(!binding.switchRoamingActivation.isChecked());

                            AppEventLogs.applyAppEvent(AppEventLogValues.RoamingScreen.ROAMING_SCREEN,
                                    AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_SUCCESS,
                                    AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_SUCCESS);

                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {
                            if (getActivity() != null && !getActivity().isFinishing() && getContext() != null) {
                                // Check if user logout by server
                                if (response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                    return;
                                }
                                if (response != null && response.getDescription() != null) {
                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }


                                AppEventLogs.applyAppEvent(AppEventLogValues.RoamingScreen.ROAMING_SCREEN,
                                        AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_FAILURE,
                                        AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_FAILURE);

                            }
                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    });
        }
    } // RequestRoamingActivation ends

    /*
    Request to check roaming activation current status
     */
    private void requestRoamingActivationStatus(String offeringId) {
        if (getActivity() == null) return; //safe passege
        if (DataManager.getInstance().getCurrentUser() != null) {
            UserModel userModel = DataManager.getInstance().getCurrentUser();

            RequestRoamingActivationStatus.newInstance(getActivity(), ServiceIDs.REQUEST_ENABLE_DISABLE_ROAMING_ACTIVATION)
                    .execute(userModel, offeringId, new EaseCallbacks<String>() {
                        @Override
                        public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                            try {
                                String result = response.getData();
                                RoamingActivationStatusModel offerMain = new Gson().fromJson(result, RoamingActivationStatusModel.class);
                                if (getActivity() != null && offerMain != null) {
                                    binding.switchRoamingActivation.setChecked(offerMain.getActive());
                                }

                            } catch (JsonSyntaxException e) {
                                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                            }

                            AppEventLogs.applyAppEvent(AppEventLogValues.RoamingScreen.ROAMING_SCREEN,
                                    AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_STATUS_SUCESS,
                                    AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_STATUS_SUCESS);


                        }

                        @Override
                        public void onFailure(@NonNull EaseRequest<String> request,
                                              EaseResponse<String> response) {

                            // Check if user logout by server
                            if (response != null && response.getResultCode() != null &&
                                    response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                HomeActivity.logoutUser(getActivity());
                                return;
                            }
                            if (response != null && response.getDescription() != null) {
                                BakcellPopUpDialog.showMessageDialog(getActivity(),
                                        getString(R.string.bakcell_error_title), response.getDescription());
                            }


                            AppEventLogs.applyAppEvent(AppEventLogValues.RoamingScreen.ROAMING_SCREEN,
                                    AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_STATUS_FAILURE,
                                    AppEventLogValues.RoamingScreen.ROAMING_ACTIVATION_STATUS_FAILURE);

                        }

                        @Override
                        public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    });
        }
    } //requestRoamingActivationStatus ends
}
