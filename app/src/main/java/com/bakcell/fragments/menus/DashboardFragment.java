package com.bakcell.fragments.menus;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.activities.landing.LoanActivity;
import com.bakcell.activities.landing.MyInstallmentsActivity;
import com.bakcell.activities.landing.TopUpActivity;
import com.bakcell.adapters.DashboardUsageCircleAdapter;
import com.bakcell.animations.FlipAnimation;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentDashboardBinding;
import com.bakcell.dialogs.InAppFeedbackDialog;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.globals.RootViews;
import com.bakcell.models.DataManager;
import com.bakcell.models.dashboard.DashboardMain;
import com.bakcell.models.dashboard.balance.BalanceMain;
import com.bakcell.models.dashboard.freeresources.FreeResoures;
import com.bakcell.models.dashboard.installments.Installments;
import com.bakcell.models.inappsurvey.InAppSurvey;
import com.bakcell.models.inappsurvey.Surveys;
import com.bakcell.models.notifications.NotificationMain;
import com.bakcell.models.rateusapimodels.RateUsAPIModel;
import com.bakcell.utilities.AppRaterTool;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.NotificationCounterPopupUtil;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.GetDashboardService;
import com.bakcell.webservices.RateUsAPIBeforeRedirectionWebService;
import com.bakcell.webservices.RateUsWebService;
import com.bakcell.webservices.RequestGetNotificationCountService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellButtonNormal;
import com.bakcell.widgets.BakcellTextViewBold;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.google.gson.Gson;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

import static com.bakcell.globals.Constants.DateAndMonth.SAMPLE_DATE_TIME_FORMAT;


/**
 * Created by Noman on 16-May-17.
 */

public class DashboardFragment extends Fragment implements View.OnClickListener {

    private final String fromClass = "DashboardFragment";
    public static final int ANIMATION_DURATION = 500;

    public static final String CIRCLE_RESOURCE_TYPE_DATA = "DATA";
    public static final String CIRCLE_RESOURCE_TYPE_VOICE = "Voice";
    public static final String CIRCLE_RESOURCE_TYPE_SMS = "SMS";

    public static final String CIRCLE_RESOURCE_TYPE_ROAMING = "roaming";

    public static final String MRC_TYPE_MONTHLY = "monthly";
    public static final String MRC_TYPE_WEEKLY = "weekly";
    public static final String MRC_TYPE_DAILY = "daily";

    private final String CURRENCY_SIGN = "azn";

    FragmentDashboardBinding binding;

    View rootView;

    SwipeRefreshLayout swipe_refresh_layout;
    ProgressBar expiry_progress;
    ProgressBar mrc_expiry_progress;
    ProgressBar inst_fee_expiry_progress;
    ImageView roaming_enable_button;
    RelativeLayout flipRootView;
    RelativeLayout rl_idicator_layout;
    private boolean isRoaming;
    ViewStub balViewStub;
    View balanceLayout;

    private DashboardUsageCircleAdapter dashboardUsageCircleAdapter;

    private DashboardMain dashboardMain;

    private ArrayList<FreeResoures> freeResouresUsageList;
    private ArrayList<FreeResoures> freeResouresRoamingUsageList;

    int enableIcon = R.drawable.ic_roaming_normal;
    int disable = R.drawable.ic_roaming_enabled;

    private boolean isUserPayG = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);

        rootView = binding.getRoot();
        // View for Balance
        balViewStub = rootView.findViewById(R.id.balanceView);

        initCircles();

        iniUIContents();

        iniUIListeners();

        loadEmptyDataForBeforeAPIResponse();

        //API for getString Dashboard data

        requestForGetDashboardData(true);


        AppEventLogs.applyAppEvent(AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN,
                AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN,
                AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN);
        if (DataManager.getInstance().getCurrentUser() != null) {
            AppEventLogs.applyAppEvent(AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN,
                    AppEventLogValues.DashboardEvents.LOGGED_IN_USER_TYPE +
                            DataManager.getInstance().getCurrentUser().getSubscriberType()
                            + " " + DataManager.getInstance().getCurrentUser().getCustomerType(),
                    AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN);
        }
        inAppFeedback();
        return rootView;
    }

    private void inAppFeedback() {
        int currentVisit = PrefUtils.getAsInt(requireContext(), PrefUtils.PreKeywords.PREF_HOME_PAGE);
        currentVisit = currentVisit + 1;
        InAppSurvey inAppSurvey = PrefUtils.getInAppSurvey(getContext());
        /*
         *  in if condition first we check the InAppSurvey that is not null,
         *      if its null condition will return null to check with != null condition
         *      which will return false.
         *  if the InAppSurvey is not null it will return the inAppSurvey.getData(),
         *      which will compare with != null,  if inAppSurvey.getData() is null
         *      it will return false and inAppSurvey.getData() have data if condition
         *      will execute.
         */
        if ((inAppSurvey != null ? inAppSurvey.getData() : null) != null) {
            Surveys surveys = inAppSurvey.getData().findSurvey(Constants.InAppSurveyConstants.HOME_PAGE);
            if (surveys != null) {
                if (surveys.getSurveyLimit() > surveys.getSurveyCount()) {
                    if (surveys.getVisitLimit() <= currentVisit && surveys.getVisitLimit() > -1) {
                        InAppFeedbackDialog inAppFeedbackDialog = new InAppFeedbackDialog(getContext());
                        if (surveys.getQuestions() != null) {
                            currentVisit = 0;
                            inAppFeedbackDialog.showNormalDialog(surveys);
                        }
                    }
                }
            }
        }
        PrefUtils.addInt(requireContext(), PrefUtils.PreKeywords.PREF_HOME_PAGE, currentVisit);
    }

    private void requestNotificationCounter(boolean isProcessCounterPopup, String calledFrom) {
        if (getActivity() == null) return; //safe passage
        BakcellLogger.logE("notifyCP", "calledFrom:::" + calledFrom, "DashboardFragment", "requestNotificationCounter");
        RequestGetNotificationCountService.newInstance(getActivity(),
                ServiceIDs.REQUEST_GET_NOTIFICATION_COUNT, true)
                .execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response != null) {
                            NotificationMain notificationMain = new Gson().fromJson(response.getData(),
                                    NotificationMain.class);
//                            processNotificationCounterPopup();//temp
                            if (Tools.hasValue(notificationMain.getNotificationUnreadCount())) {
                                if (Tools.isNumeric(notificationMain.getNotificationUnreadCount())
                                        && Tools.getIntegerFromString(notificationMain.getNotificationUnreadCount()) > 0) {
                                    if (RootViews.getInstance().getNotificationCountText() != null) {
                                        RootViews.getInstance().getNotificationCountText().setVisibility(View.VISIBLE);
                                        RootViews.getInstance().getNotificationCountText()
                                                .setText(notificationMain.getNotificationUnreadCount());
                                    }
                                    //the notification unread count is greater than zero
                                    processNotificationCounterPopup();
                                } else {
                                    if (RootViews.getInstance().getNotificationCountText() != null) {
                                        RootViews.getInstance().getNotificationCountText().setVisibility(View.INVISIBLE);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {

                    }
                });
    }

    private void processNotificationCounterPopup() {
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            //check if the predefined data has hour and popup message
            String popupMessage = "";
            int popupHours = 0;
            if (DataManager.getInstance().getUserPredefinedData() != null &&
                    DataManager.getInstance().getUserPredefinedData().getNotificationPopupConfig() != null &&
                    Tools.hasValue(DataManager.getInstance().getUserPredefinedData().getNotificationPopupConfig().getHour()) &&
                    Tools.hasValue(DataManager.getInstance().getUserPredefinedData().getNotificationPopupConfig().getPopupMessage())) {

                popupHours = Tools.getIntegerFromString(DataManager.getInstance().getUserPredefinedData().getNotificationPopupConfig().getHour());
                popupMessage = DataManager.getInstance().getUserPredefinedData().getNotificationPopupConfig().getPopupMessage();

                BakcellLogger.logE("notifyCP", "hour:::" +
                        popupHours +
                        " popupMessage:::" +
                        popupMessage, "DashboardFragment", "processNotificationCounterPopup");

                if (popupHours < 0) {
                    BakcellLogger.logE("notifyCP", "popupHour < 0", "DashboardFragment", "processNotificationCounterPopup");
                    return;//safe passage go back!
                }
            } else {
                BakcellLogger.logE("notifyCP", "no popupMessage and popupHour", "DashboardFragment", "processNotificationCounterPopup");
                return;//safe passage, no popupMessage and popupHour
            }

            if (Tools.hasValue(NotificationCounterPopupUtil.getPopupShownDateTimeFromPrefs(getActivity()))) {
                //popup already being showed to the user
                //now check when to show the popup to user on time base
                String popupShownOlderTime = NotificationCounterPopupUtil.getPopupShownDateTimeFromPrefs(getActivity());
                String currentDateTime = NotificationCounterPopupUtil.getCurrentDateTime();

                //the difference of hours between the current date time and the last date time
                //when the popup was shown
                long differenceOfHours = NotificationCounterPopupUtil.getHoursDifference(currentDateTime, popupShownOlderTime);

                BakcellLogger.logE("notifyCP",
                        "\ndifferenceOfHours:::" + differenceOfHours +
                                "\npopupHours:::" + popupHours +
                                "\npopupShownOlderTime:::" + popupShownOlderTime +
                                "\ncurrentDateTime:::" + currentDateTime, "\nDashboardFragment",
                        "processNotificationCounterPopup");

                //check the difference of hours between current date time
                //and the hours from the predefined data
                if (differenceOfHours >= popupHours) {
                    showNotificationCounterPopup(popupMessage);
                }
            } else {
                //show the notification without the difference between
                //the current time and the hours from predefined data
                //because the user has just sign up/or we have not previous date time
                //when the popup was shown
                showNotificationCounterPopup(popupMessage);
            }
        }
    }//processNotificationCounterPopup ends

    private void showNotificationCounterPopup(String popupMessage) {
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            //save the date time on which popup is showing
            NotificationCounterPopupUtil.setPopupShownDateTimeFromPrefs(getActivity());
            BakcellLogger.logE("notifyCP", "showNotificationCounterPopup called", "DashboardFragment", "showNotificationCounterPopup");
            //show the popup
            try {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert inflater != null;
                @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.notification_count_popup, null);
                RelativeLayout rootLayout = dialogView.findViewById(R.id.root_layout);
                dialogBuilder.setView(dialogView);

                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    rootLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
                }

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                BakcellButtonNormal bakcellButtonNormal = dialogView.findViewById(R.id.btn_okay);
                bakcellButtonNormal.setSelected(true);
                BakcellTextViewBold tvTitle = dialogView.findViewById(R.id.tv_dialog_title);
                BakcellTextViewNormal tvMessage = dialogView.findViewById(R.id.tv_pop_up_msg);
                tvTitle.setVisibility(View.GONE);
                tvTitle.setText(getActivity().getResources().getString(R.string.dialog_title_successful));
                tvMessage.setText(popupMessage);
                BakcellButtonNormal closerButton = dialogView.findViewById(R.id.btnCancel);
                closerButton.setSelected(true);

                closerButton.setOnClickListener(v -> alertDialog.dismiss());

                if (AppClass.getCurrentLanguageKey(getContext()).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
                    bakcellButtonNormal.setText("Read");
                    closerButton.setText("Cancel");
                } else if (AppClass.getCurrentLanguageKey(getContext()).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                    bakcellButtonNormal.setText("Oxumaq");
                    closerButton.setText("İmtina");
                } else if (AppClass.getCurrentLanguageKey(getContext()).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
                    bakcellButtonNormal.setText("Прочитать");
                    closerButton.setText("Отмена");
                }

                bakcellButtonNormal.setOnClickListener(v -> {
                    alertDialog.dismiss();
                    if (RootValues.getInstance().getNotificationCounterPopupEvents() != null) {
                        RootValues.getInstance().getNotificationCounterPopupEvents().onViewNotificationsButtonClick();
                    }
                });
            } catch (Exception e) {
                BakcellLogger.logE("notifyCP", "error:::" + e.toString(), "DashBoardFragment", "showNotificationCounterPopup");
            }
        }//activity not null check ends
    }//showNotificationCounterPopup ends

    private void loadEmptyDataForBeforeAPIResponse() {

        loadEmptyValueCircles();

        binding.balanceAndProgressBarsLayout.setVisibility(View.INVISIBLE);

    }

    /**
     * Request for Get Dashboard data
     */
    private void requestForGetDashboardData(final boolean isCallRateUs) {
        GetDashboardService.newInstance(getActivity(), ServiceIDs.GET_DASHBOARD_PAGE)
                .execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (response == null || getActivity() == null || getActivity().isFinishing())
                            return;

                        RootValues.getInstance().setDashboardApiCall(false);
                        dashboardMain = new Gson().fromJson(response.getData(), DashboardMain.class);
                        loadDataToUi();
                        /**
                         * check if the promo message is there
                         * if there then show the promo message first
                         * and then show the rate us popup
                         * if there is no promo message simply show the rateus pop
                         * note that the promo message should be shown only for the one time just after the signup*/
                        if (DataManager.getInstance().getPromoMessage() != null &&
                                Tools.hasValue(DataManager.getInstance().getPromoMessage()) &&
                                !RootValues.getInstance().isPromoMessageDisplayed()) {
                            BakcellLogger.logE("promoMessageZ1", "message:::" + DataManager.getInstance().getPromoMessage(), fromClass, "requestForGetDashboardData");
                            //check if promo message is already not displayed
                            showPromoMessage(DataManager.getInstance().getPromoMessage(), isCallRateUs);
                        } else {
                            BakcellLogger.logE("promoMessageZ1", "null promo message:::", fromClass, "requestForGetDashboardData");
                            //promo message is null
                            if (isCallRateUs) {
                                requestForRateUs();
                            }
                        }//else ends
                    }//onSuccess ends

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        // Load Cashed Data
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            String cacheKey = MultiAccountsHandler.CacheKeys.getDashboardCacheKey(getActivity());
                            String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                            if (Tools.hasValue(cacheResponse)) {
                                try {
                                    JSONObject jsonObject = new JSONObject(cacheResponse);
                                    String cacheData = jsonObject.getJSONObject("data").toString();
                                    dashboardMain = new Gson().fromJson(cacheData, DashboardMain.class);
                                    loadDataToUi();
                                } catch (JSONException ex) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                }
                            }
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {

                        // Load Cashed Data
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            String cacheKey = MultiAccountsHandler.CacheKeys.getDashboardCacheKey(getActivity());
                            String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                            if (Tools.hasValue(cacheResponse)) {
                                try {
                                    JSONObject jsonObject = new JSONObject(cacheResponse);
                                    String cacheData = jsonObject.getJSONObject("data").toString();
                                    dashboardMain = new Gson().fromJson(cacheData, DashboardMain.class);
                                    loadDataToUi();
                                } catch (JSONException ex) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                }
                            }
                        }

                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }
                });


    }

    private void showPromoMessage(String promoMessage, final boolean isCallRateUs) {
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            try {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.promo_message_dialog, null);
                LinearLayout linearLayout = dialogView.findViewById(R.id.root_layout);
                dialogBuilder.setView(dialogView);

                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    linearLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
                }

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                BakcellButtonNormal bakcellButtonNormal = dialogView.findViewById(R.id.btn_okay);
                BakcellTextViewBold tvTitle = dialogView.findViewById(R.id.tv_dialog_title);
                BakcellTextViewNormal tvMessage = dialogView.findViewById(R.id.tv_pop_up_msg);
                tvTitle.setVisibility(View.GONE);
                tvTitle.setText(getActivity().getResources().getString(R.string.dialog_title_successful));
                tvMessage.setText(promoMessage);
                bakcellButtonNormal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        if (isCallRateUs) {
                            requestForRateUs();
                        }
                    }
                });
            } catch (Exception e) {
                BakcellLogger.logE("promoMessageZ1", "error:::" + e.toString(), "DashBoardFragment", "showPromoMessage");
            }
            /**the promo message is displayed
             * dont show it anymore
             * because it should be shown only once*/
            RootValues.getInstance().setPromoMessageDisplayed(true);
            DataManager.getInstance().setPromoMessage(null);
        }
    }//showPromoMessage ends

    private void requestForRateUs() {
        //run this service in the background
        RateUsWebService.newInstance(getActivity(), ServiceIDs.RATE_US_SERVICE_ID).execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                if (response == null) return;

                RateUsAPIModel model = new Gson().fromJson(response.getData(), RateUsAPIModel.class);
                if (model != null &&
                        Tools.hasValue(model.getRateus_android())) {
                    processRateUsDialog(model);
                } else {
                    requestNotificationCounter(true, "1231");
                }
            }//onSuccess ends

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                // Check if user logout by server
                if (getActivity() != null && !getActivity().isFinishing() &&
                        response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                    HomeActivity.logoutUser(getActivity());
                    return;
                }
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
            }
        });


    }

    /**
     * this method is used to show the rateus pop to the user
     * if the rateus api has rateus android =0, this means that the popup needs to be shown
     * if rateus api has rateus android=1 then dont show the popup to the user
     * and then check if the popup is already being shown to the user or not using the preferences
     * the rate us popup is being showed to the user based on some timed params
     */
    private void processRateUsDialog(RateUsAPIModel model) {
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            try {
                //check for app rater dialog
                if (DataManager.getInstance().getCurrentUser() != null &&
                        model.getRateus_android() != null &&
                        Tools.hasValue(model.getRateus_android())) {
                    //get the value of rateus_android
                    //if 0 then show the dialog
                    String rateus_android = model.getRateus_android();
                    if (rateus_android.equalsIgnoreCase("0")) {
                        //check if app rater dialog has been shown to the user or not
                        if (isAppRaterDialogShownToUser()) {
                            //user already have been shown the dialog
                            //get the current date
                            String currentDateTime = AppRaterTool.getCurrentDateTime();
                            BakcellLogger.logE("rateUs", "if showTime:::" + (currentDateTime), fromClass, "dialogTime");
                            //get the may be later date and time
                            String mayBeLaterDateTime = PrefUtils.getString(getActivity(), PrefUtils.PreKeywords.MAY_BE_LATER_TIME, "");
                            if (Tools.hasValue(currentDateTime) && Tools.hasValue(mayBeLaterDateTime)) {
                                /*long differenceInDays = AppRaterTool.getDifference(mayBeLaterDateTime, currentDateTime, "d");*/
                                SimpleDateFormat format = new SimpleDateFormat(SAMPLE_DATE_TIME_FORMAT);
                                Date loginDate = format.parse(mayBeLaterDateTime);
                                Date currentDate = format.parse(currentDateTime);
                                long differenceInDays = AppRaterTool.getHoursDifference(currentDate, loginDate, "d");
                                BakcellLogger.logE("rateUs", "if showDifference:::" + (differenceInDays), fromClass, "dialogTime");
                                //get latepopon
                                if (DataManager.getInstance().getCurrentUser().getLateOnPopup() != null &&
                                        Tools.hasValue(DataManager.getInstance().getCurrentUser().getLateOnPopup())) {
                                    String latePopupOn = (DataManager.getInstance().getCurrentUser().getLateOnPopup());
                                    BakcellLogger.logE("rateUs", "lateon popup:::" + (latePopupOn), fromClass, "dialogTime");
                                    if (differenceInDays >= Long.parseLong(latePopupOn)) {
                                        if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getPopupTitle()) &&
                                                Tools.hasValue(DataManager.getInstance().getCurrentUser().getPopupContent())) {
                                            String dialogTitle = DataManager.getInstance().getCurrentUser().getPopupTitle();
                                            String popupContent = DataManager.getInstance().getCurrentUser().getPopupContent();
                                            BakcellLogger.logE("popX123", "poptitle:::" + dialogTitle +
                                                    " content:::" + popupContent, fromClass, "processRateUsDialog");
                                            //please show the dialog
                                            showRateDialog(dialogTitle, popupContent);
                                        } else {
                                            requestNotificationCounter(true, "1653");
                                        }
                                    } else {
                                        requestNotificationCounter(true, "9721");
                                    }//if (differenceInDays >= latePopupOn) ends
                                } else {
                                    requestNotificationCounter(true, "2311");
                                }//if(DataManager.getInstance().getPredefinedData().lateOnPopup!=null) ends
                            } else {
                                requestNotificationCounter(true, "3408");
                            }//if (Tools.hasValue(currentDateTime) && Tools.hasValue(mayBeLaterDateTime)) ends
                        } else {
                            //get the current date
                            String currentDateTime = AppRaterTool.getCurrentDateTime();
                            BakcellLogger.logE("rateUs", "else showTime:::" + (currentDateTime), fromClass, "dialogTime");
                            //get the login date and time
                            String loginDateTime = PrefUtils.getString(getActivity(), PrefUtils.PreKeywords.USER_LOGIN_TIME, "");
                            BakcellLogger.logE("rateUs", "loginDateTime-new:::" + (loginDateTime), fromClass, "dialogTime");
                            if (Tools.hasValue(currentDateTime) && Tools.hasValue(loginDateTime)) {
                                /*long differenceInHrs = AppRaterTool.getDifference(loginDateTime, currentDateTime, "h");*/
                                SimpleDateFormat format = new SimpleDateFormat(SAMPLE_DATE_TIME_FORMAT);
                                Date loginDate = format.parse(loginDateTime);
                                Date currentDate = format.parse(currentDateTime);
                                long differenceInHrs = AppRaterTool.getHoursDifference(currentDate, loginDate, "h");
                                BakcellLogger.logE("rateUs", "else showDifference:::" + (differenceInHrs), fromClass, "dialogTime");
                                if (DataManager.getInstance().getCurrentUser().getFirstPopup() != null &&
                                        Tools.hasValue(DataManager.getInstance().getCurrentUser().getFirstPopup())) {
                                    //get the firstpop value
                                    String firstPopUpValue = (DataManager.getInstance().getCurrentUser().getFirstPopup());
                                    BakcellLogger.logE("rateUs", "else firstpopup:::" + (firstPopUpValue), fromClass, "dialogTime");
                                    if (differenceInHrs >= Long.parseLong(firstPopUpValue)) {
                                        if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getPopupTitle()) &&
                                                Tools.hasValue(DataManager.getInstance().getCurrentUser().getPopupContent())) {
                                            String dialogTitle = DataManager.getInstance().getCurrentUser().getPopupTitle();
                                            String popupContent = DataManager.getInstance().getCurrentUser().getPopupContent();
                                            BakcellLogger.logE("popX123", "poptitle111:::" + dialogTitle +
                                                    " content:::" + popupContent, fromClass, "processRateUsDialog");
                                            //please show the dialog
                                            showRateDialog(dialogTitle, popupContent);
                                        } else {
                                            requestNotificationCounter(true, "2474");
                                        }//if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getPopupTitle()) && Tools.hasValue(DataManager.getInstance().getCurrentUser().getPopupContent())) ends
                                    } else {
                                        requestNotificationCounter(true, "8654");
                                    }//if (differenceInHrs >= Long.parseLong(firstPopUpValue)) ends
                                } else {
                                    requestNotificationCounter(true, "5674");
                                }// if (DataManager.getInstance().getCurrentUser().getFirstPopup() != null) ends
                            } else {
                                requestNotificationCounter(true, "9120");
                            }// if (Tools.hasValue(currentDateTime) && Tools.hasValue(loginDateTime)) ends
                        }//if (isAppRaterDialogShownToUser()) ends
                    } else {
                        requestNotificationCounter(true, "5566");
                    }//if (rateus_android .equalsIgnoreCase("0")) ends
                } else {
                    requestNotificationCounter(true, "0875");
                }// if (DataManager.getInstance().getCurrentUser() != null) ends
            } catch (Exception exp) {
                requestNotificationCounter(true, "0022");
                BakcellLogger.logE("rateUs", "error:::" + (exp.toString()), fromClass, "processRateUsDialog");
            }
        }
    }//processRateUsDialog ends

    @SuppressLint("InflateParams")
    private void showRateDialog(String popupTitle, String popupContent) {
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            try {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.app_rater_dialog_layout, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();

                BakcellButtonNormal rateNowButton = dialogView.findViewById(R.id.rateNowButton);
                BakcellButtonNormal mayBeLaterButton = dialogView.findViewById(R.id.mayBeLaterButton);
                BakcellTextViewNormal rateUsTitle = dialogView.findViewById(R.id.rateUsTitle);
                BakcellTextViewNormal giveUsRating = dialogView.findViewById(R.id.giveUsRating);
                BakcellTextViewNormal doYouLikeText = dialogView.findViewById(R.id.doYouLikeText);

                doYouLikeText.setText(popupTitle);
                giveUsRating.setText(popupContent);
                rateNowButton.setSelected(true);
                mayBeLaterButton.setSelected(true);
                //rateNowButton & mayBeLaterButton translations
                if (AppClass.getCurrentLanguageKey(getContext()).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
                    rateNowButton.setText("RATE NOW");
                    mayBeLaterButton.setText("MAYBE LATER");
                    rateUsTitle.setText("Rate us");
                }

                if (AppClass.getCurrentLanguageKey(getContext()).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                    rateNowButton.setText("QİYMƏTLƏNDİRMƏK");
                    mayBeLaterButton.setText("SONRA");
                    rateUsTitle.setText("Bizi qiymətləndirin");
                }

                if (AppClass.getCurrentLanguageKey(getContext()).equals(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
                    rateNowButton.setText("ОЦЕНИТЬ");
                    mayBeLaterButton.setText("ПОЗЖЕ");
                    rateUsTitle.setText("Оцените нас");
                }

                mayBeLaterButton.setOnClickListener(view -> {
                    alertDialog.dismiss(); //dismiss the dialog
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        String currentDateTime = AppRaterTool.getCurrentDateTime();
                        PrefUtils.addString(getActivity(), PrefUtils.PreKeywords.MAY_BE_LATER_TIME, currentDateTime);
                        PrefUtils.addBoolean(getActivity(), PrefUtils.PreKeywords.PREF_APP_RATER_SHOWN, true);
                    }
                    requestNotificationCounter(true, "2000");
                });

                rateNowButton.setOnClickListener(view -> {
                    alertDialog.dismiss(); //dismiss the dialog
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        PrefUtils.addBoolean(getActivity(), PrefUtils.PreKeywords.PREF_APP_RATER_SHOWN, true);
                    }
                    //proceed user to the play store
                    callRateUsAPIBeforeRedirection();
                });
            } catch (Exception exp) {
                requestNotificationCounter(true, "1209");
                BakcellLogger.logE("rateUs", "error2:::" + (exp.toString()), fromClass, "showRateDialog");
            }//try-catch ends
        }
    }//showRateDialog ends

    private void callRateUsAPIBeforeRedirection() {
        RateUsAPIBeforeRedirectionWebService.newInstance(getActivity(), ServiceIDs.RATE_US_API_BEFORE_REDIRECTION_SERVICE_ID).execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                if (response == null) return;

                //success here
                redirectUserToPlayStore();
            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                // Check if user logout by server
                if (getActivity() != null && !getActivity().isFinishing() &&
                        response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                    HomeActivity.logoutUser(getActivity());
                    return;
                }

                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                            getString(R.string.bakcell_error_title), response.getDescription());
                }
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                }
            }
        });
    }

    private void redirectUserToPlayStore() {
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            try {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                startActivity(goToMarket);
            } catch (Exception exp) {
                BakcellLogger.logE("playE", "Error:::" + exp.toString(), "DashboardFragment", "redirectUserToPlayStore");
            }
        }
    }//redirectUserToPlayStore ends

    /**
     * this method is used to determine, if  the rate us popup is already shown to user or not
     * the process rateus dialog methods uses its flag
     */
    private Boolean isAppRaterDialogShownToUser() {
        boolean isAlreadyShown = false;
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            try {
                if (PrefUtils.getBoolean(getActivity(), PrefUtils.PreKeywords.PREF_APP_RATER_SHOWN, false)) {
                    isAlreadyShown = true;
                    BakcellLogger.logE("rateUs", "alreadyShow", fromClass, "isAppRaterDialogShownToUser");
                } else {
                    BakcellLogger.logE("rateUs", "NotShow", fromClass, "isAppRaterDialogShownToUser");
                }
            } catch (Exception exp) {
                BakcellLogger.logE("rateUs", "error:::" + (exp.toString()), fromClass, "isAppRaterDialogShownToUser");
                return isAlreadyShown;
            }
        }
        return isAlreadyShown;
    }

    private void loadDataToUi() {
        // Load UI
        updateUIWithData(dashboardMain, true);//true/response.isCached()

        // Add Balance data saperatly to Sharedpereferences
        if (getActivity() != null && dashboardMain != null) {
            String balanceData = new Gson().toJson(dashboardMain.getBalance());
            if (Tools.hasValue(balanceData)) {
                PrefUtils.addString(getActivity(),
                        MultiAccountsHandler.CacheKeys.getUserBalanceCacheKey(getActivity()), balanceData);
            }
        }


        getBalanceFromSharedPreferences();

        ////////////

        // Add Installment data saperatly to show on My Installment page
        if (getActivity() != null && dashboardMain != null) {
            String myInstallmentData = new Gson().toJson(dashboardMain.getInstallments());
            if (Tools.hasValue(myInstallmentData)) {
                PrefUtils.addString(getActivity(), MultiAccountsHandler.CacheKeys.getUserInstallmentsCacheKey(getActivity()), myInstallmentData);
            }
        }

        //set minimum value to be paid
        if (getActivity() != null && dashboardMain != null) {
            if (dashboardMain.getBalance() != null) {
                if (Tools.hasValue(dashboardMain.getBalance().getMinAmountTeBePaid())) {
                    BakcellLogger.logE("minAmount", "minPaid:::" + dashboardMain.getBalance().getMinAmountTeBePaid(), fromClass, "setting min amount to be paid");
                    if (Tools.hasValue(dashboardMain.getBalance().getMinAmountLabel())) {
                        binding.minimumAmountLabel.setText(dashboardMain.getBalance().getMinAmountLabel());
                        BakcellLogger.logE("minAmount", "minPaidlabel:::" + dashboardMain.getBalance().getMinAmountLabel(), fromClass, "setting min amount to be paid");
                    } else {
                        BakcellLogger.logE("minAmount", "minPaidlabel:::null", fromClass, "setting min amount to be paid");
                    }
                    binding.minimumAmountValue.setText(dashboardMain.getBalance().getMinAmountTeBePaid());
                } else {
                    BakcellLogger.logE("minAmount", "minPaid:::null", fromClass, "setting min amount to be paid");

                    binding.minimumAmountPaidHolder.setVisibility(View.GONE);
                }
            } else {
                BakcellLogger.logE("minAmount", "getBalance:::null", fromClass, "setting min amount to be paid");
                binding.minimumAmountPaidHolder.setVisibility(View.GONE);
            }
        } else {
            BakcellLogger.logE("minAmount", "dashboardMain:::null", fromClass, "setting min amount to be paid");
            binding.minimumAmountPaidHolder.setVisibility(View.GONE);
        }
    }

    private void getBalanceFromSharedPreferences() {

        if (getActivity() != null && !getActivity().isFinishing()) {
            BalanceMain balanceMain = PrefUtils.getAsJson(getActivity(),
                    MultiAccountsHandler.CacheKeys.getUserBalanceCacheKey(getActivity()), BalanceMain.class);
            if (balanceMain != null) {
                DataManager.getInstance().setBalanceMain(balanceMain);
            }
        }
    }//getBalanceFromSharedPereferences ends

    @SuppressLint("ClickableViewAccessibility")
    private void iniUIListeners() {

        binding.roamingEnableButton.setOnClickListener(this);
        swipe_refresh_layout.setOnRefreshListener(() -> {
            try {

                RootValues.getInstance().setDashboardApiCall(true);
                requestForGetDashboardData(false);

                swipe_refresh_layout.setRefreshing(false);
            } catch (Exception e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }
        });

        binding.myviewpager.setOnTouchListener((view, motionEvent) -> {

            if (swipe_refresh_layout != null) {
                try {
                    swipe_refresh_layout.setEnabled(false);
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        swipe_refresh_layout.setEnabled(true);
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }


            return false;
        });

        binding.creditPlusButton.setOnClickListener(v -> BaseActivity.startNewActivity(getActivity(), LoanActivity.class));

        binding.packageIconButton.setOnClickListener(v -> {
            RootValues.getInstance().setIsTariffFromDashBoard(true);
            RootValues.getInstance().setOfferingIdRedirectFromNotifications("");

            Bundle bundle = new Bundle();
            bundle.putBoolean(TariffsFragment.KEY_DATA_FROM_HOME_TO_TARIFF, isUserPayG);
            ((HomeActivity) getActivity()).selectMenu(Constants.MenusKeyword.MENU_BOTTOM_TARIFFS, bundle);
        });

        binding.installmentsProgressLayout.setOnClickListener(v -> {
            if (dashboardMain != null && dashboardMain.getInstallments() != null
                    && dashboardMain.getInstallments().getInstallmentes().size() > 0) {
                Bundle bundle = new Bundle();
                bundle.putString(MyInstallmentsActivity.KEY_ISNTALLMENTS_TITLE,
                        dashboardMain.getInstallments().getInstallmentDescription());
                bundle.putParcelableArrayList(MyInstallmentsActivity.KEY_ISNTALLMENTS_LIST,
                        dashboardMain.getInstallments().getInstallmentes());
                BaseActivity.startNewActivity(getActivity(), MyInstallmentsActivity.class, bundle);
            }
        });

    }

    private void animateProgressBarsOnSwipe() {
//        animateProgressBars();//animate progress bars
//
//        if (isRoaming) {
//            updateUsageCirclesData(freeResouresRoamingUsageList);
//        } else {
//            updateUsageCirclesData(freeResouresUsageList);
//        }
//
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//                swipe_refresh_layout.setRefreshing(false);
//            }
//        }, 700);
    }


    private void flipCard() {
        if (isRoaming) {
            binding.roamingEnableButton.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                    enableIcon));
            isRoaming = false;
            //juni1289
            if (freeResouresUsageList == null) {
                freeResouresUsageList = new ArrayList<>();
            }
            updateUsageCirclesData(freeResouresUsageList, false);
            AppEventLogs.applyAppEvent(AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN,
                    AppEventLogValues.DashboardEvents.SWITCH_TO_NORMAL_USAGE,
                    AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN);
        } else {
            binding.roamingEnableButton.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                    disable));
            isRoaming = true;
            //juni1289
            if (freeResouresUsageList == null) {
                freeResouresUsageList = new ArrayList<>();
            }
            updateUsageCirclesData(freeResouresRoamingUsageList, false);
            AppEventLogs.applyAppEvent(AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN,
                    AppEventLogValues.DashboardEvents.SWITCH_TO_ROAMING_USAGE,
                    AppEventLogValues.DashboardEvents.DASHBOARD_SCREEN);
        }

        FlipAnimation flipAnimation = new FlipAnimation(binding.myviewpager, binding.myviewpager);
        flipRootView.startAnimation(flipAnimation);
    }

    private void iniUIContents() {

        swipe_refresh_layout = binding.swipeRefreshLayout;
        expiry_progress = binding.expiryProgress;
        mrc_expiry_progress = binding.mrcExpiryProgress;
        inst_fee_expiry_progress = binding.instFeeExpiryProgress;
        roaming_enable_button = binding.roamingEnableButton;
        flipRootView = binding.flipRootView;
        rl_idicator_layout = binding.rlIndicatorLayout;

        binding.packageName.setSelected(true);
        // Set User package name
        if (DataManager.getInstance().getCurrentUser() != null
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getOfferingNameDisplay())) {
            binding.packageName.setText(DataManager.getInstance().getCurrentUser().getOfferingNameDisplay());
        } else {
            binding.packageName.setText("");
        }

        // Set User status
        if (DataManager.getInstance().getCurrentUser() != null &&
                Tools.hasValue(DataManager.getInstance().getCurrentUser().getStatus())) {
            if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getStatusDetails())
                    && DataManager.getInstance().getCurrentUser().getStatusDetails().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_CURRENT_STATUS_CODE)) {
                binding.packageStatusTxt.setTextColor(ContextCompat.getColor(getActivity(), R.color.green_light));
            } else {
                binding.packageStatusTxt.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            }
            binding.packageStatusTxt.setText(DataManager.getInstance().getCurrentUser().getStatus());
            binding.packageStatusTxt.setSelected(true);
        } else {
            binding.packageStatusTxt.setText("");
        }

        swipe_refresh_layout.setColorSchemeResources(R.color.colorPrimary);

        //Animate Proress bars
        animateProgressBars();
    }

    private void initCircles() {
        // setting width to arrows for cicles
        ViewGroup.LayoutParams params = binding.rlIndicatorLayout.getLayoutParams();
        params.width = (int) Tools.convertDpToPixel(RootValues.getInstance().getDASHBOARD_CIRCLE_WIDTH() + 45, getActivity());

        freeResouresUsageList = new ArrayList<>();
        dashboardUsageCircleAdapter = new DashboardUsageCircleAdapter(getActivity(), freeResouresUsageList, isRoaming, false);
        InfiniteScrollAdapter infiniteAdapter = InfiniteScrollAdapter.wrap(dashboardUsageCircleAdapter);
        binding.myviewpager.setOrientation(Orientation.HORIZONTAL);
        binding.myviewpager.setAdapter(infiniteAdapter);
        binding.myviewpager.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.7f)
                .build());

    }

    private void animateProgressBars() {


//        ObjectAnimator progressAnimator1 = ObjectAnimator.ofInt(expiry_progress, "progress", 0, 100);
//        progressAnimator1.setDuration(ANIMATION_DURATION);
//        progressAnimator1.setInterpolator(new LinearInterpolator());
//        progressAnimator1.start();
//
//        ObjectAnimator progressAnimator2 = ObjectAnimator.ofInt(mrc_expiry_progress, "progress", 0, 100);
//        progressAnimator2.setDuration(ANIMATION_DURATION);
//        progressAnimator2.setInterpolator(new LinearInterpolator());
//        progressAnimator2.start();
//
//        ObjectAnimator progressAnimator3 = ObjectAnimator.ofInt(inst_fee_expiry_progress, "progress", 0, 100);
//        progressAnimator3.setDuration(ANIMATION_DURATION);
//        progressAnimator3.setInterpolator(new LinearInterpolator());
//        progressAnimator3.start();
    }


    public class ProgressBarAnimation extends Animation {
        private ProgressBar progressBar;
        private float from;
        private float to;

        public ProgressBarAnimation(ProgressBar progressBar, float from, float to) {
            super();
            this.progressBar = progressBar;
            this.from = from;
            this.to = to;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            progressBar.setProgress((int) value);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.roaming_enable_button:
                flipCard();
                break;
            case R.id.swipe_refresh_layout:
                animateProgressBarsOnSwipe();
                break;
        }
    }


    private void updateUIWithData(DashboardMain dashboardMain, boolean isAnim) {
        if (dashboardMain == null || rootView == null) return;

        if (getActivity() != null && !getActivity().isFinishing()) {
        } else {
            return;
        }

        // Notificaiton Count Set
        /*if (RootViews.getInstance().getNotificationCountText() != null
                && Tools.hasValue(dashboardMain.getNotificationUnreadCount())
                && Tools.isNumeric(dashboardMain.getNotificationUnreadCount())
                && Tools.getIntegerFromString(dashboardMain.getNotificationUnreadCount()) > 0) {
            RootViews.getInstance().getNotificationCountText().setVisibility(View.VISIBLE);
            RootViews.getInstance().getNotificationCountText().setText(dashboardMain.getNotificationUnreadCount());
        } else {
            if (RootViews.getInstance().getNotificationCountText() != null) {
                RootViews.getInstance().getNotificationCountText().setVisibility(View.INVISIBLE);
            }
        }*/

        // Roaming Icon set
        if (dashboardMain.getFreeResources() != null &&
                dashboardMain.getFreeResources().getFreeResourcesRoaming() != null) {
            if (dashboardMain.getFreeResources().getFreeResourcesRoaming().size() > 0) {
                enableIcon = R.drawable.roamingdot;
                disable = R.drawable.ic_roamingoffer;
            } else {
                enableIcon = R.drawable.ic_roaming_normal;
                disable = R.drawable.ic_roaming_enabled;
            }
            binding.roamingEnableButton.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                    enableIcon));
        }

        binding.balanceAndProgressBarsLayout.setVisibility(View.VISIBLE);
        // Update Usage Circles
        if (dashboardMain.getFreeResources() != null) {
            if (dashboardMain.getFreeResources().getFreeResources() != null && dashboardMain.getFreeResources().getFreeResources().size() > 0) {
                freeResouresUsageList = dashboardMain.getFreeResources().getFreeResources();
                updateUsageCirclesData(freeResouresUsageList, isAnim);
            }
            if (dashboardMain.getFreeResources().getFreeResourcesRoaming() != null && dashboardMain.getFreeResources().getFreeResourcesRoaming().size() > 0) {
                freeResouresRoamingUsageList = dashboardMain.getFreeResources().getFreeResourcesRoaming();
            }
        }


        // Check Subscriber Type Type
        if (DataManager.getInstance().getCurrentUser() != null) {//Prepaid
            if (DataManager.getInstance().getCurrentUser().getSubscriberType()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)) {
                binding.creditProgressLayout.setVisibility(View.VISIBLE);

                balViewStub.setLayoutResource(R.layout.fragment_dashboard_balance_with_boxs);

                if (balViewStub.getParent() != null) {
                    balanceLayout = balViewStub.inflate();
                }

                setCreditMrcInstallmentsData(dashboardMain, true);

                setCustomerBalanceDataForBoxLayout(dashboardMain, balanceLayout, true);

                //Postpaid
            } else if (DataManager.getInstance().getCurrentUser().getSubscriberType()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_POSTPAID)) {
                binding.creditProgressLayout.setVisibility(View.GONE);
                binding.installmentsProgressLayout.setVisibility(View.VISIBLE);
                binding.mrcProgressLayout.setVisibility(View.VISIBLE);

                if (dashboardMain.getBalance() != null &&
                        dashboardMain.getBalance().getPostpaid() != null) {
                    if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getTemplate())) {

                        balViewStub.setLayoutResource(R.layout.fragment_dashboard_balance_with_list);

                        if (balViewStub.getParent() != null) {
                            balanceLayout = balViewStub.inflate();
                        }

                        setCustomerBalanceDataForListLayout(dashboardMain, balanceLayout);

                    }
                }

                setCreditMrcInstallmentsData(dashboardMain, false);

            } else {
                HomeActivity.logoutUser(getActivity());
            }
        } else {
            HomeActivity.logoutUser(getActivity());
        }


    }

    private void setCustomerBalanceDataForListLayout(DashboardMain dashboardMain, View balanceLayoutList) {
        if (dashboardMain == null || balanceLayoutList == null) return;
        if (getActivity() != null && !getActivity().isFinishing()) {
        } else {
            return;
        }

        BakcellTextViewNormal outstandingLabel = balanceLayoutList.findViewById(R.id.outstandingLabel);
        outstandingLabel.setSelected(true);
        BakcellTextViewNormal outstandingValue = balanceLayoutList.findViewById(R.id.outstandingValue);
        BakcellTextViewNormal outstandingValueSign = balanceLayoutList.findViewById(R.id.outstandingValueSign);

        BakcellTextViewNormal corporateLabel = balanceLayoutList.findViewById(R.id.corporateLabel);
        corporateLabel.setSelected(true);
        BakcellTextViewNormal individualLabel = balanceLayoutList.findViewById(R.id.individualLabel);
        individualLabel.setSelected(true);

        BakcellTextViewNormal balanceLabel = balanceLayoutList.findViewById(R.id.balanceLabel);
        balanceLabel.setSelected(true);
        BakcellTextViewNormal currentCreditLabel = balanceLayoutList.findViewById(R.id.currentCreditLabel);
        currentCreditLabel.setSelected(true);
        BakcellTextViewNormal availableCreditLabel = balanceLayoutList.findViewById(R.id.availableCreditLabel);
        availableCreditLabel.setSelected(true);

        BakcellTextViewNormal balanceCorporateValue = balanceLayoutList.findViewById(R.id.balanceCorporateValue);
        BakcellTextViewNormal balanceCorporateValueSign = balanceLayoutList.findViewById(R.id.balanceCorporateValueSign);

        BakcellTextViewNormal balanceIndividualValue = balanceLayoutList.findViewById(R.id.balanceIndividualValue);
        BakcellTextViewNormal balanceIndividualValueSign = balanceLayoutList.findViewById(R.id.balanceIndividualValueSign);

        BakcellTextViewNormal currentCreditCorporateValue = balanceLayoutList.findViewById(R.id.currentCreditCorporateValue);
        BakcellTextViewNormal currentCreditCorporateValueSign = balanceLayoutList.findViewById(R.id.currentCreditCorporateValueSign);

        BakcellTextViewNormal currentCreditIndividualValue = balanceLayoutList.findViewById(R.id.currentCreditIndividualValue);
        BakcellTextViewNormal currentCreditIndividualValueSign = balanceLayoutList.findViewById(R.id.currentCreditIndividualValueSign);

        BakcellTextViewNormal availableCreditCorporateValue = balanceLayoutList.findViewById(R.id.availableCreditCorporateValue);
        BakcellTextViewNormal availableCreditCorporateValueSign = balanceLayoutList.findViewById(R.id.availableCreditCorporateValueSign);

        BakcellTextViewNormal availableCreditIndividualValue = balanceLayoutList.findViewById(R.id.availableCreditIndividualValue);
        BakcellTextViewNormal availableCreditIndividualValueSign = balanceLayoutList.findViewById(R.id.availableCreditIndividualValueSign);

        RelativeLayout outstatndingLayout = balanceLayoutList.findViewById(R.id.outstatndingLayout);
        LinearLayout corporateIndividualLayout = balanceLayoutList.findViewById(R.id.corporateIndividualLayout);
        LinearLayout balanceListLayout = balanceLayoutList.findViewById(R.id.balanceListLayout);
        LinearLayout currentCreditLayout = balanceLayoutList.findViewById(R.id.currentCreditLayout);

        outstandingLabel.setText("");
        outstandingValue.setText("");
        corporateLabel.setText("");
        individualLabel.setText("");
        balanceLabel.setText("");
        currentCreditLabel.setText("");
        availableCreditLabel.setText("");
        balanceCorporateValue.setText("");
        balanceIndividualValue.setText("");
        currentCreditCorporateValue.setText("");
        currentCreditIndividualValue.setText("");
        availableCreditCorporateValue.setText("");
        availableCreditIndividualValue.setText("");

        if (dashboardMain.getBalance() != null && dashboardMain.getBalance().getPostpaid() != null) {

            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getOutstandingIndividualDeptLabel())) {
                outstandingLabel.setText(dashboardMain.getBalance().getPostpaid()
                        .getOutstandingIndividualDeptLabel());
            }
            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getOutstandingIndividualDept())) {
                outstandingValue.setText(dashboardMain.getBalance().getPostpaid()
                        .getOutstandingIndividualDept());
            }

            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getCorporateLabel())) {
                corporateLabel.setText(dashboardMain.getBalance().getPostpaid()
                        .getCorporateLabel());
            }
            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getIndividualLabel())) {
                individualLabel.setText(dashboardMain.getBalance().getPostpaid().getIndividualLabel());
            }

            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getBalanceLabel())) {
                balanceLabel.setText(dashboardMain.getBalance().getPostpaid().getBalanceLabel());
            }
            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getCurrentCreditLabel())) {
                currentCreditLabel.setText(dashboardMain.getBalance().getPostpaid()
                        .getCurrentCreditLabel());
            }

            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getAvailableCreditLabel())) {
                availableCreditLabel.setText(dashboardMain.getBalance().getPostpaid()
                        .getAvailableCreditLabel());
            }
            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getBalanceCorporateValue())) {
                balanceCorporateValue.setText(dashboardMain.getBalance().getPostpaid()
                        .getBalanceCorporateValue());
            }

            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getBalanceIndividualValue())) {
                balanceIndividualValue.setText(dashboardMain.getBalance().getPostpaid()
                        .getBalanceIndividualValue());
            }
            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getCurrentCreditCorporateValue())) {
                currentCreditCorporateValue.setText(dashboardMain.getBalance().getPostpaid()
                        .getCurrentCreditCorporateValue());
            }
            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getCurrentCreditIndividualValue())) {
                currentCreditIndividualValue.setText(dashboardMain.getBalance().getPostpaid()
                        .getCurrentCreditIndividualValue());
            }

            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getAvailableBalanceCorporateValue())) {
                availableCreditCorporateValue.setText(dashboardMain.getBalance().getPostpaid()
                        .getAvailableBalanceCorporateValue());
            }
            if (Tools.hasValue(dashboardMain.getBalance().getPostpaid()
                    .getAvailableBalanceIndividualValue())) {
                availableCreditIndividualValue.setText(dashboardMain.getBalance().getPostpaid()
                        .getAvailableBalanceIndividualValue());
            }

        }

        //Check templates
        if (dashboardMain.getBalance() != null
                && dashboardMain.getBalance().getPostpaid() != null
                && Tools.hasValue(dashboardMain.getBalance().getPostpaid().getTemplate())) {
            if (dashboardMain.getBalance().getPostpaid().getTemplate()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword
                            .POSTPAID_TEMPLATE_INDIVIDUAL)) {

                //Postpaid Individual

                currentCreditLayout.setVisibility(View.VISIBLE);

                outstatndingLayout.setVisibility(View.VISIBLE);
                corporateIndividualLayout.setVisibility(View.GONE);
                balanceListLayout.setVisibility(View.VISIBLE);

                balanceCorporateValue.setVisibility(View.INVISIBLE);
                currentCreditCorporateValue.setVisibility(View.INVISIBLE);
                availableCreditCorporateValue.setVisibility(View.INVISIBLE);
                balanceCorporateValueSign.setVisibility(View.INVISIBLE);
                currentCreditCorporateValueSign.setVisibility(View.INVISIBLE);
                availableCreditCorporateValueSign.setVisibility(View.INVISIBLE);

            } else if (dashboardMain.getBalance().getPostpaid().getTemplate()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword
                            .POSTPAID_TEMPLATE_CORPORATE_FULL_PAY)) {

                //Corporate Full Pay

                currentCreditLayout.setVisibility(View.GONE);

                outstatndingLayout.setVisibility(View.VISIBLE);
                corporateIndividualLayout.setVisibility(View.VISIBLE);
                balanceListLayout.setVisibility(View.VISIBLE);

                balanceCorporateValue.setVisibility(View.VISIBLE);
                currentCreditCorporateValue.setVisibility(View.VISIBLE);
                availableCreditCorporateValue.setVisibility(View.VISIBLE);
                balanceCorporateValueSign.setVisibility(View.VISIBLE);
                currentCreditCorporateValueSign.setVisibility(View.VISIBLE);
                availableCreditCorporateValueSign.setVisibility(View.VISIBLE);

            } else if (dashboardMain.getBalance().getPostpaid().getTemplate()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword
                            .POSTPAID_TEMPLATE_CORPORATE_INDIVIDUAL)) {

                //Corporate Individual

                currentCreditLayout.setVisibility(View.VISIBLE);

                outstatndingLayout.setVisibility(View.VISIBLE);
                corporateIndividualLayout.setVisibility(View.VISIBLE);
                balanceListLayout.setVisibility(View.VISIBLE);

                balanceCorporateValue.setVisibility(View.VISIBLE);
                currentCreditCorporateValue.setVisibility(View.VISIBLE);
                availableCreditCorporateValue.setVisibility(View.VISIBLE);
                balanceCorporateValueSign.setVisibility(View.VISIBLE);
                currentCreditCorporateValueSign.setVisibility(View.VISIBLE);
                availableCreditCorporateValueSign.setVisibility(View.VISIBLE);

            }
        }

    }

    private void setCustomerBalanceDataForBoxLayout(DashboardMain dashboardMain,
                                                    View balanceLayoutBox, boolean isPrepaid) {
        if (dashboardMain == null || balanceLayoutBox == null) return;

        if (getActivity() != null && !getActivity().isFinishing()) {
        } else {
            return;
        }

        BakcellTextViewNormal main_wallet_title = balanceLayoutBox.findViewById(R.id.main_wallet_title);
        main_wallet_title.setSelected(true);
        BakcellTextViewNormal main_wallet_value = balanceLayoutBox.findViewById(R.id.main_wallet_value);
        BakcellTextViewBold priceSignMainWallet = balanceLayoutBox.findViewById(R.id.priceSignMainWallet);
        ImageView main_wallet_icon = balanceLayoutBox.findViewById(R.id.main_wallet_icon);

        BakcellTextViewNormal bonus_title = balanceLayoutBox.findViewById(R.id.bonus_title);
        bonus_title.setSelected(true);
        BakcellTextViewNormal bonus_value = balanceLayoutBox.findViewById(R.id.bonus_value);
        BakcellTextViewBold priceSignBonusWallet = balanceLayoutBox.findViewById(R.id.priceSignBonusWallet);
        ImageView bonus_icon = balanceLayoutBox.findViewById(R.id.bonus_icon);

        BakcellTextViewNormal countrywide_title = balanceLayoutBox.findViewById(R.id.countrywide_title);
        countrywide_title.setSelected(true);
        BakcellTextViewNormal countrywide_value = balanceLayoutBox.findViewById(R.id.countrywide_value);
        BakcellTextViewBold priceSignCountrywide = balanceLayoutBox.findViewById(R.id.priceSignCountrywide);
        ImageView countrywide_icon = balanceLayoutBox.findViewById(R.id.countrywide_icon);

        RelativeLayout mainWalletView = balanceLayoutBox.findViewById(R.id.mainWalletView);
        RelativeLayout bonusWalletView = balanceLayoutBox.findViewById(R.id.bonusWalletView);
        RelativeLayout countryWalletView = balanceLayoutBox.findViewById(R.id.countryWalletView);

        mainWalletView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.startNewActivity(getActivity(), TopUpActivity.class);
            }
        });

        main_wallet_title.setText("");
        main_wallet_value.setText("");

        bonus_title.setText("");
        bonus_value.setText("");

        countrywide_title.setText("");
        countrywide_value.setText("");

        if (isPrepaid) {
            main_wallet_icon.setImageResource(R.drawable.main);
            bonus_icon.setImageResource(R.drawable.bonus);
            countrywide_icon.setImageResource(R.drawable.country);

            if (dashboardMain.getBalance() != null && dashboardMain.getBalance().getPrepaid() != null) {
                priceSignBonusWallet.setVisibility(View.VISIBLE);
                // Main Wallet
                if (dashboardMain.getBalance().getPrepaid().getMainWallet() != null && Tools.hasValue(dashboardMain.getBalance().getPrepaid().getMainWallet().getAmount())) {
                    mainWalletView.setVisibility(View.VISIBLE);
                    main_wallet_title.setText(dashboardMain.getBalance().getPrepaid()
                            .getMainWallet().getBalanceTypeName());
                    main_wallet_value.setText(dashboardMain.getBalance().getPrepaid()
                            .getMainWallet().getAmount());

                    if (Tools.hasValue(dashboardMain.getBalance().getPrepaid().getMainWallet()
                            .getLowerLimit()) && Tools.isNumeric(dashboardMain.getBalance()
                            .getPrepaid().getMainWallet().getLowerLimit())) {
                        double limit = Tools.getDoubleFromString(dashboardMain.getBalance()
                                .getPrepaid().getMainWallet().getLowerLimit());
                        double amount = Tools.getDoubleFromString(dashboardMain.getBalance()
                                .getPrepaid().getMainWallet().getAmount());

                        if (amount < limit) {
                            main_wallet_value.setTextColor(ContextCompat.getColor(getActivity(),
                                    R.color.colorPrimary));
                            priceSignMainWallet.setTextColor(ContextCompat.getColor(getActivity(),
                                    R.color.colorPrimary));
                        } else {
                            main_wallet_value.setTextColor(ContextCompat.getColor(getActivity(),
                                    R.color.black));
                            priceSignMainWallet.setTextColor(ContextCompat.getColor(getActivity(),
                                    R.color.black));
                        }
                    }
                } else {
                    mainWalletView.setVisibility(View.INVISIBLE);
                }
                // Bounus Wallet
                if (dashboardMain.getBalance().getPrepaid().getBounusWallet() != null && Tools.hasValue(dashboardMain.getBalance().getPrepaid().getBounusWallet().getAmount())) {
                    bonusWalletView.setVisibility(View.VISIBLE);
                    bonus_title.setText(dashboardMain.getBalance().getPrepaid().getBounusWallet()
                            .getBalanceTypeName());
                    bonus_value.setText(dashboardMain.getBalance().getPrepaid().getBounusWallet()
                            .getAmount());

                    if (Tools.hasValue(dashboardMain.getBalance().getPrepaid().getBounusWallet()
                            .getLowerLimit()) && Tools.isNumeric(dashboardMain.getBalance()
                            .getPrepaid().getBounusWallet().getLowerLimit())) {
                        int limit = Tools.getIntegerFromString(dashboardMain.getBalance()
                                .getPrepaid().getBounusWallet().getLowerLimit());
                        int amount = Tools.getIntegerFromString(dashboardMain.getBalance()
                                .getPrepaid().getBounusWallet().getAmount());

//                        if (amount < limit) {
//                            bonus_value.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
//                        } else {
//                            bonus_value.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
//                        }
                    }
                } else {
                    bonusWalletView.setVisibility(View.INVISIBLE);
                }
                // Country Wide Wallet
                if (dashboardMain.getBalance().getPrepaid().getCountryWideWallet() != null && Tools.hasValue(dashboardMain.getBalance().getPrepaid().getCountryWideWallet().getAmount())) {
                    countryWalletView.setVisibility(View.VISIBLE);
                    countrywide_title.setText(dashboardMain.getBalance().getPrepaid()
                            .getCountryWideWallet().getBalanceTypeName());
                    countrywide_value.setText(dashboardMain.getBalance().getPrepaid()
                            .getCountryWideWallet().getAmount());

                    if (Tools.hasValue(dashboardMain.getBalance().getPrepaid()
                            .getCountryWideWallet().getLowerLimit())
                            && Tools.isNumeric(dashboardMain.getBalance().getPrepaid()
                            .getCountryWideWallet().getLowerLimit())) {
                        int limit = Tools.getIntegerFromString(dashboardMain.getBalance()
                                .getPrepaid().getCountryWideWallet().getLowerLimit());
                        int amount = Tools.getIntegerFromString(dashboardMain.getBalance()
                                .getPrepaid().getCountryWideWallet().getAmount());

//                        if (amount < limit) {
//                            countrywide_value.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
//                            priceSignCountrywide.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
//                        } else {
//                            countrywide_value.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
//                            priceSignCountrywide.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
//                        }
                    }
                } else {
                    countryWalletView.setVisibility(View.INVISIBLE);
                }
            }
        } else {
            main_wallet_icon.setImageResource(R.drawable.main);
            bonus_icon.setImageResource(R.drawable.currentcreditpospaid);
            countrywide_icon.setImageResource(R.drawable.avcreditpostpaid);

            if (dashboardMain.getBalance() != null && dashboardMain.getBalance().getPostpaid() != null) {
                priceSignBonusWallet.setVisibility(View.VISIBLE);

                if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getBalanceIndividualValue())) {
                    mainWalletView.setVisibility(View.VISIBLE);
                    main_wallet_title.setText(dashboardMain.getBalance().getPostpaid()
                            .getBalanceLabel());
                    main_wallet_value.setText(dashboardMain.getBalance().getPostpaid()
                            .getBalanceIndividualValue());
                } else {
                    mainWalletView.setVisibility(View.INVISIBLE);
                }

                if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getCurrentCreditIndividualValue())) {
                    bonusWalletView.setVisibility(View.VISIBLE);
                    bonus_title.setText(dashboardMain.getBalance().getPostpaid()
                            .getCurrentCreditLabel());
                    bonus_value.setText(dashboardMain.getBalance().getPostpaid()
                            .getCurrentCreditIndividualValue());
                } else {
                    bonusWalletView.setVisibility(View.INVISIBLE);
                }

                if (Tools.hasValue(dashboardMain.getBalance().getPostpaid().getAvailableBalanceIndividualValue())) {
                    countryWalletView.setVisibility(View.VISIBLE);
                    countrywide_title.setText(dashboardMain.getBalance().getPostpaid()
                            .getAvailableCreditLabel());
                    countrywide_value.setText(dashboardMain.getBalance().getPostpaid()
                            .getAvailableBalanceIndividualValue());
                } else {
                    countryWalletView.setVisibility(View.INVISIBLE);
                }


            }

        }

    }

    private void setCreditMrcInstallmentsData(DashboardMain dashboardMain, boolean isPrepaid) {
        if (dashboardMain == null) return;
        if (getActivity() != null && !getActivity().isFinishing()) {
        } else {
            return;
        }

        // Credit
        if (dashboardMain.getCredit() != null && isPrepaid) {
            binding.creditProgressLayout.setVisibility(View.VISIBLE);
            String startDate = dashboardMain.getCredit().getCreditInitialDate();
            String endDate = dashboardMain.getCredit().getCreditDate();

//now we are calculating these variable in the backend.
//            long startMili = Tools.getMiliSecondsFromDate(startDate);
//            long endMili = Tools.getMiliSecondsFromDate(endDate);
//            long currentMili = Tools.getCurrentMiliSeconds();
//            int daysDiffCurrent = Tools.getDaysDifferenceBetweenMiliseconds(startMili, currentMili);
//            int daysDiffTotal = Tools.getDaysDifferenceBetweenMiliseconds(startMili, endMili);
            int daysDiffCurrent = -1;
            int daysDiffTotal = -1;

            if (Tools.hasValue(dashboardMain.getCredit().getCreditDays()) && Tools.isNumeric(dashboardMain.getCredit().getCreditDays())) {
                daysDiffTotal = Tools.getIntegerFromString(dashboardMain.getCredit().getCreditDays());
            }
            if (Tools.hasValue(dashboardMain.getCredit().getProgressDays()) && Tools.isNumeric(dashboardMain.getCredit().getProgressDays())) {
                daysDiffCurrent = Tools.getIntegerFromString(dashboardMain.getCredit().getProgressDays());
            }

            if (Tools.hasValue(dashboardMain.getCredit().getCreditTitleLabel())) {
                binding.creditTitle.setSelected(true);
                binding.creditTitle.setText(dashboardMain.getCredit().getCreditTitleLabel());
            } else {
                binding.creditTitle.setText("");
            }

            if (Tools.hasValue(dashboardMain.getCredit().getCreditTitleValue())) {
                binding.creditValueTxt.setText(dashboardMain.getCredit().getCreditTitleValue());
                RootValues.getInstance().setLoanCredit(dashboardMain.getCredit().getCreditTitleValue());
            } else {
                binding.creditValueTxt.setText("");
                RootValues.getInstance().setLoanCredit("0.0");
                binding.creditProgressLayout.setVisibility(View.GONE);
            }

            if (Tools.hasValue(dashboardMain.getCredit().getCreditDateLabel())) {
                binding.expiryTitle.setSelected(true);
                binding.expiryTitle.setText(dashboardMain.getCredit().getCreditDateLabel());
            } else {
                binding.expiryTitle.setText("");
            }

            if (Tools.hasValue(dashboardMain.getCredit().getCreditDate())) {
                try {
                    binding.expiryDate.setText(Tools.getDateAccordingToClientSaid(dashboardMain.getCredit().getCreditDate()));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }

            } else {
                binding.expiryDate.setText("");
            }

            if (daysDiffTotal > -1) {
                if (getActivity() != null) {
                    int days = -1;

                    if (Tools.hasValue(dashboardMain.getCredit().getRemainingCreditDays()) && Tools.isNumeric(dashboardMain.getCredit().getRemainingCreditDays())) {
                        days = Tools.getIntegerFromString(dashboardMain.getCredit().getRemainingCreditDays());
                    }

                    if (days <= -1) {
                        days = 0;
                    }
                    if (getResources() != null) {
                        binding.daysTxt.setText(getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, days, days));
                    }
                    if (Tools.isNumeric(dashboardMain.getCredit().getCreditLimit()) &&
                            days < Tools.getDoubleFromString(dashboardMain
                                    .getCredit().getCreditLimit())) {
                        binding.daysTxt.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.colorPrimary));
                    } else {
                        binding.daysTxt.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.black));
                    }
                }
            } else {
                binding.daysTxt.setText("");
            }
            binding.expiryProgress.setMax(daysDiffTotal);
            binding.expiryProgress.setProgress(daysDiffCurrent);
            //////

            try {
                binding.expiryDate.setText(Tools.getDateAccordingToClientSaid(endDate));
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            if (Tools.hasValue(dashboardMain.getCredit().getCreditDate())) {
                binding.creditInnerProgressView.setVisibility(View.VISIBLE);
            } else {
                binding.creditInnerProgressView.setVisibility(View.INVISIBLE);
            }

        } else {
            binding.creditProgressLayout.setVisibility(View.GONE);
        }

        // MRC
        boolean isMrcShow = true;
        if ((DataManager.getInstance().getCurrentUser() != null
                && Tools.hasValue(DataManager.getInstance().getCurrentUser().getBrandName())
                && DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN))) {
          /*  if (!DataManager.getInstance().getCurrentUser().getOfferingId().equalsIgnoreCase(Constants.UserCurrentKeyword.DRC_USER_CIN_TARIFF_OFFERING_ID)) {
                isMrcShow = false;
            }*/

            if (DataManager.getInstance().getCurrentUser().getCinTariff() != null &&
                    Tools.hasValue(DataManager.getInstance().getCurrentUser().getCinTariff()) &&
                    DataManager.getInstance().getCurrentUser().getCinTariff().equalsIgnoreCase(Constants.TariffsConstants.CIN_TARIFF_SHOW)) {
                BakcellLogger.logE("mrCShoX", "mrc needs  to show", fromClass, "setCreditMrcInstallmentsData");
                isMrcShow = true;
            } else {
                BakcellLogger.logE("mrCShoX", "mrc needs NOT to show", fromClass, "setCreditMrcInstallmentsData");
                isMrcShow = false;
            }
        }
        if (dashboardMain.getMrc() != null && isMrcShow) {

            binding.mrcProgressLayout.setVisibility(View.VISIBLE);

            if (Tools.hasValue(dashboardMain.getMrc().getMrcTitleLabel())) {
                binding.mrcTitle.setSelected(true);
                binding.mrcTitle.setText(dashboardMain.getMrc().getMrcTitleLabel());
            } else {
                binding.mrcTitle.setText("");
            }
            if (Tools.hasValue(dashboardMain.getMrc().getMrcTitleValue())) {
                binding.mrcValueTxt.setText(dashboardMain.getMrc().getMrcTitleValue());
            } else {
                binding.mrcValueTxt.setText("");
                binding.aznSignMrc.setVisibility(View.GONE);
            }
            if (Tools.hasValue(dashboardMain.getMrc().getMrcDateLabel())) {
                binding.mrcExpiryTitle.setSelected(true);
                binding.mrcExpiryTitle.setText(dashboardMain.getMrc().getMrcDateLabel());
            } else {
                binding.mrcExpiryTitle.setText("");
            }

            if (Tools.hasValue(dashboardMain.getMrc().getMrcCurrency()) && dashboardMain.getMrc().getMrcCurrency().equalsIgnoreCase(CURRENCY_SIGN)) {
                binding.aznSignMrc.setVisibility(View.VISIBLE);
            } else {
                binding.aznSignMrc.setVisibility(View.GONE);
            }

            String startDate = dashboardMain.getMrc().getMrcInitialDate();
            String endDate = dashboardMain.getMrc().getMrcDate();
            if (Tools.hasValue(startDate) && Tools.hasValue(endDate)) {
                // Check if MRC end Date Excced from Current date
                if (Tools.hasValue(dashboardMain.getMrc().getMrcStatus())
                        && DataManager.getInstance().getCurrentUser().getSubscriberType().equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)
                        && DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_KLASS)
                        || (DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)
                        && DataManager.getInstance().getCurrentUser().getOfferingId().equalsIgnoreCase(Constants.UserCurrentKeyword.DRC_USER_CIN_TARIFF_OFFERING_ID))) {
                    binding.packageTitle.setVisibility(View.VISIBLE);

                    if (dashboardMain.getMrc().getMrcStatus().equalsIgnoreCase(Constants.UserCurrentKeyword.DASHBOARD_MRC_UPPAID)) {
                        binding.packageTitle.setText(R.string.lbl_upaid);// Upaid
                        binding.packageTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.password_strength_orange));
                        isUserPayG = true;
                    } else {
                        binding.packageTitle.setText(R.string.lbl_paid);// Paid
                        binding.packageTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_gray));
                        isUserPayG = false;
                    }

                } else {
                    binding.packageTitle.setVisibility(View.GONE);
                }

                long startMili = Tools.getMiliSecondsFromDate(startDate);
                long endMili = Tools.getMiliSecondsFromDate(endDate);
                if (Tools.hasValue(dashboardMain.getMrc().getMrcType())
                        && dashboardMain.getMrc().getMrcType().equalsIgnoreCase(MRC_TYPE_DAILY)) {
                    endMili = Tools.getOneHourMinus(endMili);
                } else {
                    endMili = Tools.getOneDayMinus(endMili);
                }
                long currentMili = Tools.getCurrentMiliSeconds();

                int daysDiffCurrent = Tools.getDaysDifferenceBetweenMiliseconds(startMili, currentMili);
                int daysDiffTotal = Tools.getDaysDifferenceBetweenMiliseconds(startMili, endMili);

                int hoursDiffCurrent = Tools.getHourssDifferenceBetweenMiliseconds(startMili, currentMili);
                int hoursDiffTotel = Tools.getHourssDifferenceBetweenMiliseconds(startMili, endMili);

                if (Tools.hasValue(dashboardMain.getMrc().getMrcDate())) {
                    try {
                        binding.mrcExpiryDate.setText(Tools.getDateAccordingToClientSaid(dashboardMain.getMrc().getMrcDate()));
                    } catch (ParseException e) {
                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                    }
                } else {
                    binding.mrcExpiryDate.setText("");
                }

                if (daysDiffTotal > -1) {

                    int days = daysDiffTotal - daysDiffCurrent;
                    if (isUserPayG || days < 0) {
                        days = 0;
                    }
                    if (binding.mrcDaysTxt != null && getResources() != null) {
                        binding.mrcDaysTxt.setText(getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, days, days));
                    }
                    changeColorOfMrcRemainingDays(binding.mrcDaysTxt, dashboardMain, days);
                } else {
                    binding.mrcDaysTxt.setText("");
                }

                if (Tools.hasValue(dashboardMain.getMrc().getMrcType())
                        && dashboardMain.getMrc().getMrcType().equalsIgnoreCase(MRC_TYPE_DAILY)) {

                    binding.mrcExpiryProgress.setMax(hoursDiffTotel);
                    binding.mrcExpiryProgress.setProgress(hoursDiffCurrent);
                    int remaingHours = hoursDiffTotel - hoursDiffCurrent;
                    // Repeated for Hours
                    if (isUserPayG || remaingHours < 0) {
                        remaingHours = 0;
                    }
                    changeColorOfMrcRemainingDays(binding.mrcDaysTxt, dashboardMain, remaingHours);
                    binding.mrcDaysTxt.setText(getResources().getQuantityString(R.plurals.dashboard_credit_remaining_hours
                            , remaingHours, remaingHours));
                    //
                } else {
                    binding.mrcExpiryProgress.setMax(daysDiffTotal);
                    binding.mrcExpiryProgress.setProgress(daysDiffCurrent);
                }

                if (isUserPayG) {
                    binding.mrcExpiryProgress.setMax(100);
                    binding.mrcExpiryProgress.setProgress(100);
                }

                //////
                try {
                    binding.mrcExpiryDate.setText(Tools.getDateAccordingToClientSaid(endDate));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            } else {// Incase if no start date and end date (null/empty)

                if (Tools.hasValue(dashboardMain.getMrc().getMrcStatus())
                        && DataManager.getInstance().getCurrentUser().getSubscriberType().equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)
                        && DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_KLASS)
                        || (DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)
                        && DataManager.getInstance().getCurrentUser().getOfferingId().equalsIgnoreCase(Constants.UserCurrentKeyword.DRC_USER_CIN_TARIFF_OFFERING_ID))) {
                    binding.packageTitle.setVisibility(View.VISIBLE);
                    binding.packageTitle.setText(R.string.lbl_upaid);// Upaid
                    binding.packageTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.password_strength_orange));
                    isUserPayG = true;
                } else {
                    binding.packageTitle.setVisibility(View.GONE);
                }

                if (Tools.hasValue(dashboardMain.getMrc().getMrcType())
                        && dashboardMain.getMrc().getMrcType().equalsIgnoreCase(MRC_TYPE_DAILY)) {
                    binding.mrcDaysTxt.setText(getResources().getQuantityString(R.plurals.dashboard_credit_remaining_hours
                            , 0, 0));
                    binding.mrcDaysTxt.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                } else {
                    binding.mrcDaysTxt.setText(getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, 0, 0));
                    binding.mrcDaysTxt.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                }

                binding.mrcExpiryDate.setText(R.string.lbl_expired);

                binding.mrcExpiryProgress.setMax(100);
                binding.mrcExpiryProgress.setProgress(100);

            }

        } else {
            binding.mrcProgressLayout.setVisibility(View.GONE);
        }

        // Installments
        if (dashboardMain.getInstallments() != null && dashboardMain.getInstallments()
                .getInstallmentes() != null &&
                dashboardMain.getInstallments().getInstallmentes().size() > 0) {
            binding.installmentsProgressLayout.setVisibility(View.VISIBLE);

            Installments firstInstallment = dashboardMain.getInstallments().getInstallmentes().get(0);

            if (firstInstallment == null) return;

            String startDate = firstInstallment.getNextPaymentInitialDate();
            String endDate = firstInstallment.getNextPaymentValue();

            long startMili = Tools.getMiliSecondsFromDate(startDate);
            //startMili = Tools.getOneDayMinus(startMili);
            long endMili = Tools.getMiliSecondsFromDate(endDate);
            long currentMili = Tools.getCurrentMiliSeconds();

            int daysDiffCurrent = Tools.getDaysDifferenceBetweenMiliseconds(startMili, currentMili);
            int daysDiffTotal = Tools.getDaysDifferenceBetweenMiliseconds(startMili, endMili);

            int daysDiffCurrentEnd = Tools.getDaysDifferenceBetweenMiliseconds(currentMili, endMili);

            if (Tools.hasValue(dashboardMain.getInstallments().getInstallmentTitle())) {
                binding.instFeeTitle.setSelected(true);
                binding.instFeeTitle.setText(dashboardMain.getInstallments().getInstallmentTitle());
            } else {
                binding.instFeeTitle.setText("");
            }

            if (Tools.hasValue(firstInstallment.getAmountValue())) {
                binding.instFeeValueTxt.setText(firstInstallment.getAmountValue());
            } else {
                binding.instFeeValueTxt.setText("");
            }

            if (Tools.hasValue(firstInstallment.getNextPaymentLabel())) {
                binding.instFeeExpiryTitle.setSelected(true);
                binding.instFeeExpiryTitle.setText(firstInstallment.getNextPaymentLabel());
            } else {
                binding.instFeeExpiryTitle.setText("");
            }

            if (Tools.hasValue(firstInstallment.getNextPaymentValue())) {
                try {
                    binding.instFeeExpiryDate.setText(Tools.getDateAccordingToClientSaid(firstInstallment.getNextPaymentValue()));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            } else {
                binding.instFeeExpiryDate.setText("");
            }

            //
            if (daysDiffTotal > -1) {

                int days = Tools.getNumberOfDaysInCurrentMonth() - Tools.getCurrentDayOfCurrentMonth();
                if (days < 0) {
                    days = 0;
                }
                if (getResources() != null) {
                    binding.instFeeDaysTxt.setText(getResources().getQuantityString(R.plurals.dashboard_credit_remaining_days, days, days));
                }
                if (Tools.isNumeric(firstInstallment.getInstallmentFeeLimit()) &&
                        days < Tools.getDoubleFromString(firstInstallment
                                .getInstallmentFeeLimit())) {
                    binding.instFeeDaysTxt.setTextColor(ContextCompat.getColor(getActivity(),
                            R.color.colorPrimary));
                } else {
                    binding.instFeeDaysTxt.setTextColor(ContextCompat.getColor(getActivity(),
                            R.color.black));
                }
            } else {
                binding.instFeeDaysTxt.setText("");
            }

            binding.instFeeExpiryProgress.setMax(Tools.getNumberOfDaysInCurrentMonth());
            binding.instFeeExpiryProgress.setProgress((Tools.getCurrentDayOfCurrentMonth() - 1));

            try {
                binding.instFeeExpiryDate.setText(Tools.getDateAccordingToClientSaid(endDate));
            } catch (ParseException e) {
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

        } else {
            binding.installmentsProgressLayout.setVisibility(View.GONE);
        }


    }

    private void changeColorOfMrcRemainingDays(BakcellTextViewNormal bakcellTextViewNormal, DashboardMain dashboardMain, int days) {
        if (dashboardMain != null && dashboardMain.getMrc() != null) {
            if (Tools.isNumeric(dashboardMain.getMrc().getMrcLimit()) &&
                    days < Tools.getDoubleFromString(dashboardMain.getMrc().getMrcLimit())) {
                bakcellTextViewNormal.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            } else {
                bakcellTextViewNormal.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
            }
        }
    }

    private int getCurrentHourNoFromCurrentDay() {
        int hour = 0;

        Calendar rightNow = Calendar.getInstance();
        hour = rightNow.get(Calendar.HOUR_OF_DAY);

        return hour;
    }


    private void updateUsageCirclesData(ArrayList<FreeResoures> freeResouresList, boolean isAnim) {

        dashboardUsageCircleAdapter = new DashboardUsageCircleAdapter(getActivity(),
                updateCirclesListArrangePosition(freeResouresList), isRoaming, isAnim);
        InfiniteScrollAdapter infiniteAdapter = InfiniteScrollAdapter.wrap(dashboardUsageCircleAdapter);
        binding.myviewpager.setOrientation(Orientation.HORIZONTAL);
        binding.myviewpager.setAdapter(infiniteAdapter);
        binding.myviewpager.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.7f)
                .build());


    }

    private void loadEmptyValueCircles() {

        ArrayList<FreeResoures> freeResouresList = new ArrayList<>();
        FreeResoures resoures1 = new FreeResoures();
        FreeResoures resoures2 = new FreeResoures();
        FreeResoures resoures3 = new FreeResoures();

        resoures1.setResourceDiscountedText("");
        resoures1.setResourceInitialUnits("");
        resoures1.setResourceRemainingUnits("");
        resoures1.setResourceType(CIRCLE_RESOURCE_TYPE_DATA);
        resoures1.setResourcesTitleLabel(getString(R.string.lbl_internet));
        resoures1.setResourceUnitName(getString(R.string.lbl_mbs));

        resoures2.setResourceDiscountedText("");
        resoures2.setResourceInitialUnits("");
        resoures2.setResourceRemainingUnits("");
        resoures2.setResourceType(CIRCLE_RESOURCE_TYPE_SMS);
        resoures2.setResourcesTitleLabel(getString(R.string.lbl_sms));
        resoures2.setResourceUnitName("");

        resoures3.setResourceDiscountedText("");
        resoures3.setResourceInitialUnits("");
        resoures3.setResourceRemainingUnits("");
        resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_VOICE);
        resoures3.setResourcesTitleLabel(getString(R.string.lbl_calls));
        resoures3.setResourceUnitName(getString(R.string.lbl_mins));

        freeResouresList.add(resoures1);
        freeResouresList.add(resoures2);
        freeResouresList.add(resoures3);


        updateUsageCirclesData(freeResouresList, false);
    }

    private ArrayList<FreeResoures> updateCirclesListArrangePosition(ArrayList<FreeResoures> resouresList) {

        if (getActivity() != null && !getActivity().isFinishing()) {
        } else {
            return resouresList;
        }

        if (resouresList != null && resouresList.size() > 0) {
            if (resouresList.size() == 1) {

                FreeResoures resoures1 = new FreeResoures();

                FreeResoures resoures2 = new FreeResoures();
                resoures2.setResourceDiscountedText("");
                resoures2.setResourceInitialUnits("");
                resoures2.setResourceRemainingUnits("");

                FreeResoures resoures3 = new FreeResoures();
                resoures3.setResourceDiscountedText("");
                resoures3.setResourceInitialUnits("");
                resoures3.setResourceRemainingUnits("");

                if (resouresList.get(0).getResourceType()
                        .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_DATA)) {

                    resoures2.setResourceType(CIRCLE_RESOURCE_TYPE_SMS);
                    resoures2.setResourcesTitleLabel(getString(R.string.lbl_sms));
                    resoures2.setResourceUnitName("");

                    resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_VOICE);
                    resoures3.setResourcesTitleLabel(getString(R.string.lbl_calls));
                    resoures3.setResourceUnitName(getString(R.string.lbl_mins));

                } else if (resouresList.get(0).getResourceType()
                        .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_SMS)) {

                    resoures2.setResourceType(CIRCLE_RESOURCE_TYPE_DATA);
                    resoures2.setResourcesTitleLabel(getString(R.string.lbl_internet));
                    resoures2.setResourceUnitName(getString(R.string.lbl_mbs));

                    resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_VOICE);
                    resoures3.setResourcesTitleLabel(getString(R.string.lbl_calls));
                    resoures3.setResourceUnitName(getString(R.string.lbl_mins));

                } else if (resouresList.get(0).getResourceType()
                        .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_VOICE)) {

                    resoures2.setResourceType(CIRCLE_RESOURCE_TYPE_SMS);
                    resoures2.setResourcesTitleLabel(getString(R.string.lbl_sms));
                    resoures2.setResourceUnitName("");

                    resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_DATA);
                    resoures3.setResourcesTitleLabel(getString(R.string.lbl_internet));
                    resoures3.setResourceUnitName(getString(R.string.lbl_mbs));

                }

                resoures1 = resouresList.get(0);

                resouresList = new ArrayList<>();
                resouresList.add(resoures1);
                resouresList.add(resoures2);
                resouresList.add(resoures3);

            } else if (resouresList.size() == 2) {

                FreeResoures resoures1 = new FreeResoures();
                FreeResoures resoures2 = new FreeResoures();
                FreeResoures resoures3 = new FreeResoures();

                for (int i = 0; i < resouresList.size(); i++) {
                    if (resouresList.get(i) != null && resouresList.get(i)
                            .getResourceType() != null) {

                        if (resouresList.get(i).getResourceType()
                                .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_SMS)) {

                            if (Tools.hasValue(resouresList.get(i).getResourcesTitleLabel()) && Tools.hasValue(resouresList.get(i).getRemainingFormatted()) &&
                                    Tools.hasValue(resouresList.get(i).getResourceUnitName())) {
                                resoures2 = resouresList.get(i);
                            } else {
                                resoures2.setResourceType(CIRCLE_RESOURCE_TYPE_SMS);
                                resoures2.setResourcesTitleLabel(getString(R.string.lbl_sms));
                                resoures2.setResourceUnitName("");
                            }

                        } else {
                            resoures2.setResourceType(CIRCLE_RESOURCE_TYPE_SMS);
                            resoures2.setResourcesTitleLabel(getString(R.string.lbl_sms));
                            resoures2.setResourceUnitName("");
                        }

                        if (resouresList.get(i).getResourceType()
                                .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_DATA)) {

                            if (Tools.hasValue(resouresList.get(i).getResourcesTitleLabel()) && Tools.hasValue(resouresList.get(i).getRemainingFormatted()) &&
                                    Tools.hasValue(resouresList.get(i).getResourceUnitName())) {
                                resoures1 = resouresList.get(i);
                            } else {
                                resoures1.setResourceType(CIRCLE_RESOURCE_TYPE_DATA);
                                resoures1.setResourcesTitleLabel(getString(R.string.lbl_internet));
                                resoures1.setResourceUnitName(getString(R.string.lbl_mbs));
                            }

                        } else {
                            resoures1.setResourceType(CIRCLE_RESOURCE_TYPE_DATA);
                            resoures1.setResourcesTitleLabel(getString(R.string.lbl_internet));
                            resoures1.setResourceUnitName(getString(R.string.lbl_mbs));
                        }

                        if (resouresList.get(i).getResourceType()
                                .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_VOICE)) {
                            if (Tools.hasValue(resouresList.get(i).getResourcesTitleLabel()) && Tools.hasValue(resouresList.get(i).getRemainingFormatted()) &&
                                    Tools.hasValue(resouresList.get(i).getResourceUnitName())) {
                                resoures3 = resouresList.get(i);
                            } else {
                                resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_VOICE);
                                resoures3.setResourcesTitleLabel(getString(R.string.lbl_calls));
                                resoures3.setResourceUnitName(getString(R.string.lbl_mins));
                            }
                        } else {
                            resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_VOICE);
                            resoures3.setResourcesTitleLabel(getString(R.string.lbl_calls));
                            resoures3.setResourceUnitName(getString(R.string.lbl_mins));
                        }
                    }
                }

                resouresList = new ArrayList<>();
                resouresList.add(resoures1);
                resouresList.add(resoures2);
                resouresList.add(resoures3);

                /*FreeResoures resoures3 = new FreeResoures();
                resoures3.setResourceDiscountedText("");
                resoures3.setResourceInitialUnits("");
                resoures3.setResourceRemainingUnits("");

                if ((resouresList.get(0).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_DATA)
                        && resouresList.get(1).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_SMS))
                        || (resouresList.get(1).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_DATA)
                        && resouresList.get(0).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_SMS))) {

                    resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_VOICE);
                    resoures3.setResourcesTitleLabel(getString(R.string.lbl_calls));
                    resoures3.setResourceUnitName(getString(R.string.lbl_mins));

                } else if ((resouresList.get(0).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_DATA)
                        && resouresList.get(1).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_VOICE))
                        || (resouresList.get(1).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_DATA)
                        && resouresList.get(0).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_VOICE))) {

                    resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_SMS);
                    resoures3.setResourcesTitleLabel(getString(R.string.lbl_sms));
                    resoures3.setResourceUnitName("");

                } else if ((resouresList.get(0).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_SMS)
                        && resouresList.get(1).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_VOICE))
                        || (resouresList.get(1).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_SMS)
                        && resouresList.get(0).getResourceType().equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_VOICE))) {

                    resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_DATA);
                    resoures3.setResourcesTitleLabel(getString(R.string.lbl_internet));
                    resoures3.setResourceUnitName(getString(R.string.lbl_mbs));

                }

                resouresList.add(resoures3);*/

            } else if (resouresList.size() == 3) {
                FreeResoures resoures1 = new FreeResoures();
                FreeResoures resoures2 = new FreeResoures();
                FreeResoures resoures3 = new FreeResoures();
                for (int i = 0; i < resouresList.size(); i++) {
                    if (resouresList.get(i) != null && resouresList.get(i)
                            .getResourceType() != null) {
                        if (resouresList.get(i).getResourceType()
                                .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_DATA)) {
                            resoures1 = resouresList.get(i);
                        } else if (resouresList.get(i).getResourceType()
                                .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_SMS)) {
                            resoures2 = resouresList.get(i);
                        } else if (resouresList.get(i).getResourceType()
                                .equalsIgnoreCase(CIRCLE_RESOURCE_TYPE_VOICE)) {
                            resoures3 = resouresList.get(i);
                        }
                    }
                }

                resouresList = new ArrayList<>();
                resouresList.add(resoures1);
                resouresList.add(resoures2);
                resouresList.add(resoures3);
            }
        } else {
            resouresList = new ArrayList<>();

            FreeResoures resoures1 = new FreeResoures();
            FreeResoures resoures2 = new FreeResoures();
            FreeResoures resoures3 = new FreeResoures();

            resoures1.setResourceType(CIRCLE_RESOURCE_TYPE_DATA);
            resoures1.setResourcesTitleLabel(getString(R.string.lbl_internet));
            resoures1.setResourceUnitName(getString(R.string.lbl_mbs));

            resoures2.setResourceType(CIRCLE_RESOURCE_TYPE_SMS);
            resoures2.setResourcesTitleLabel(getString(R.string.lbl_sms));
            resoures2.setResourceUnitName("");

            resoures3.setResourceType(CIRCLE_RESOURCE_TYPE_VOICE);
            resoures3.setResourcesTitleLabel(getString(R.string.lbl_calls));
            resoures3.setResourceUnitName(getString(R.string.lbl_mins));

            resouresList.add(resoures1);
            resouresList.add(resoures2);
            resouresList.add(resoures3);

        }

        if (resouresList != null && resouresList.size() > 0) {
            FreeResoures freeResoures = resouresList.get(0);
            if (!Tools.hasValue(freeResoures.getResourceRemainingUnits())
                    || !Tools.hasValue(freeResoures.getResourceInitialUnits())) {
                for (int i = 0; i < resouresList.size(); i++) {
                    FreeResoures resource = resouresList.get(i);
                    if (Tools.hasValue(resource.getResourceInitialUnits()) ||
                            Tools.hasValue(freeResoures.getResourceRemainingUnits())) {
                        resouresList.remove(resource);
                        resouresList.add(0, resource);
                        break;
                    }
                }
            }
        }

        return resouresList;
    }


}
