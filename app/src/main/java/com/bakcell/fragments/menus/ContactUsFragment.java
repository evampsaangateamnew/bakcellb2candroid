package com.bakcell.fragments.menus;


import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentContactUsBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.contactus.ContactUs;
import com.bakcell.models.storelocator.StoreLocator;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestContactUsService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Freeware Sys on 18-May-17.
 */

public class ContactUsFragment extends Fragment implements View.OnClickListener {
    FragmentContactUsBinding binding;

    ContactUs contactUs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false);

        initUiListeners();

        requestContactUs();

        AppEventLogs.applyAppEvent(AppEventLogValues.ContactUsEvents.CONTACT_US_SCREEN,
                AppEventLogValues.ContactUsEvents.CONTACT_US_SCREEN,
                AppEventLogValues.ContactUsEvents.CONTACT_US_SCREEN);

        return binding.getRoot();
    }

    private void initUiListeners() {
        binding.HeadOfficeLocationIcon.setOnClickListener(this);
        binding.customerCareNoLayout.setOnClickListener(this);
        binding.phoneLayout.setOnClickListener(this);
        binding.faxLayout.setOnClickListener(this);
        binding.emailLayout.setOnClickListener(this);
        binding.websiteLayout.setOnClickListener(this);
        binding.officesLayout.setOnClickListener(this);
        binding.btnSend.setOnClickListener(this);

        binding.facebookLayout.setOnClickListener(this);
        binding.twitterLayout.setOnClickListener(this);
        binding.googleLayout.setOnClickListener(this);
        binding.youtubeLayout.setOnClickListener(this);
        binding.linkedinLayout.setOnClickListener(this);
        binding.instaLayout.setOnClickListener(this);

        binding.addressTxt.setText("");
        binding.customerCareNoTxt.setText("");
        binding.phoneNotxt.setText("");
        binding.faxNotxt.setText("");
        binding.emailTxt.setText("");
        binding.websiteTxt.setText("");
        binding.btnSend.setSelected(true);
    }

//    public RedirectMapFromStoreListListener redirectMapFromStoreListListener = new RedirectMapFromStoreListListener() {
//        @Override
//        public void onRedirectMapFromStoreListListen(final StoreLocator storeLocator) {
//
//            ((HomeActivity) getActivity()).selectMenu(Constants.MenusKeyword.MENU_STORE_LOCATOR,null);
//
//            final Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (AppClass.redirectMapListener != null) {
//                        AppClass.redirectMapListener.onRedirectMapListen(storeLocator, 1);
//                    }
//                }
//            }, 100);
//
//        }
//    };


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.HeadOfficeLocationIcon:
                if (contactUs != null) {
                    StoreLocator storeLocator = new StoreLocator();
                    storeLocator.setStore_name(binding.headOfferTitle.getText().toString());
                    storeLocator.setAddress(contactUs.getAddress());
                    storeLocator.setLatitude(contactUs.getAddressLat());
                    storeLocator.setLongitude(contactUs.getAddressLong());

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(StoreLocatorFragment.KEY_FROM_CONTACT_US, storeLocator);
                    ((HomeActivity) getActivity()).selectMenu(Constants.MenusKeyword.MENU_STORE_LOCATOR, bundle);

                }
                break;
            case R.id.customerCareNoLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getCustomerCareNo())) { //Fix NPE
                    openDailer(Uri.parse("tel:" + contactUs.getCustomerCareNo()));
                }
                break;
            case R.id.phoneLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getPhone())) {
                    openDailer(Uri.parse("tel:" + contactUs.getPhone()));
                }
                break;
            case R.id.faxLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getFax())) {
                    openDailer(Uri.parse("tel:" + contactUs.getFax()));
                }
                break;
            case R.id.emailLayout:
                try {
                    if (contactUs != null) {
                        if (Tools.hasValue(contactUs.getEmail())) {
                            composeEmail(new String[]{contactUs.getEmail()}, "");
                        } else {
                            BakcellPopUpDialog.showMessageDialog(getActivity(), getString(R.string.warning),
                                    getString(R.string.no_email_setup));
                        }
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
            case R.id.websiteLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getWebsite())) {
                    openBrowser(Uri.parse("https://" + contactUs.getWebsite().trim()));
                }
                break;
            case R.id.officesLayout:
                Bundle bundle = new Bundle();
                bundle.putParcelable(StoreLocatorFragment.KEY_FROM_CONTACT_US, null);
                ((HomeActivity) getActivity()).selectMenu(Constants.MenusKeyword.MENU_STORE_LOCATOR, bundle);
                break;
            case R.id.btn_send:
                try {
                    if (contactUs != null) {
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, R.string.lbl_invite_friends);
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, contactUs.getInviteFriendText());
                        startActivity(Intent.createChooser(sharingIntent, getString(R.string.lbl_share_using)));
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
                break;
            case R.id.facebookLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getFacebookLink())) {
                    openBrowser(Uri.parse(contactUs.getFacebookLink()));
                }
                break;
            case R.id.twitterLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getTwitterLink())) {
                    openBrowser(Uri.parse(contactUs.getTwitterLink().trim()));
                }
                break;
            case R.id.googleLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getGoogleLink())) {
                    openBrowser(Uri.parse(contactUs.getGoogleLink()));
                }
                break;
            case R.id.youtubeLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getYoutubeLink())) {
                    openBrowser(Uri.parse(contactUs.getYoutubeLink().trim()));
                }
                break;
            case R.id.linkedinLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getYoutubeLink())) {
                    openBrowser(Uri.parse(contactUs.getLinkedinLink().trim()));
                }
                break;
            case R.id.instaLayout:
                if (contactUs != null && Tools.hasValue(contactUs.getInstagram())) {
                    openBrowser(Uri.parse(contactUs.getInstagram().trim()));
                }
                break;
        }
    }

    private void openBrowser(Uri parse) {
        if (parse == null) return;
        try {
            Intent i = new Intent(Intent.ACTION_VIEW, parse);
            startActivity(i);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    public void composeEmail(String[] addresses, String subject) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, addresses);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            startActivity(Intent.createChooser(intent, getString(R.string.lbl_send_email)));
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    private void openDailer(Uri parse) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, parse);
            startActivity(intent);
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }


    public void requestContactUs() {


        RequestContactUsService.newInstance(getActivity(), ServiceIDs.REQUEST_CONTACT_US).execute(DataManager.getInstance().getCurrentUser(),
                "", "", new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        RootValues.getInstance().setContactUsApiCall(false);

                        String result = response.getData();
                        if (result == null) return;

                        contactUs = new Gson().fromJson(result, ContactUs.class);

                        if (contactUs != null) {
                            setDataToUI(contactUs);
                        }


                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        // Load Cashed Data
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            String cacheKey = MultiAccountsHandler.CacheKeys.getContactUsCacheKey(getActivity());
                            String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                            if (Tools.hasValue(cacheResponse)) {
                                try {
                                    JSONObject jsonObject = new JSONObject(cacheResponse);
                                    String cacheData = jsonObject.getJSONObject("data").toString();
                                    contactUs = new Gson().fromJson(cacheData, ContactUs.class);
                                    if (contactUs != null) {
                                        setDataToUI(contactUs);
                                    }
                                } catch (JSONException ex) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                }
                            }
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            // Load Cashed Data
                            String cacheKey = MultiAccountsHandler.CacheKeys.getContactUsCacheKey(getActivity());
                            String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                            if (Tools.hasValue(cacheResponse)) {
                                try {
                                    JSONObject jsonObject = new JSONObject(cacheResponse);
                                    String cacheData = jsonObject.getJSONObject("data").toString();
                                    contactUs = new Gson().fromJson(cacheData, ContactUs.class);
                                    if (contactUs != null) {
                                        setDataToUI(contactUs);
                                    }
                                } catch (JSONException ex) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                }
                            }

                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }//end of onError
                });

    }

    private void setDataToUI(ContactUs contactUs) {
        if (contactUs == null) return;

        if (Tools.hasValue(contactUs.getAddress())) {
            binding.addressTxt.setText(contactUs.getAddress());
        }
        if (Tools.hasValue(contactUs.getCustomerCareNo())) {
            binding.customerCareNoTxt.setText(contactUs.getCustomerCareNo());
        }
        if (Tools.hasValue(contactUs.getPhone())) {
            binding.phoneNotxt.setText(contactUs.getPhone());
        }
        if (Tools.hasValue(contactUs.getFax())) {
            binding.faxNotxt.setText(contactUs.getFax());
        }
        if (Tools.hasValue(contactUs.getEmail())) {
            binding.emailTxt.setText(contactUs.getEmail());
        }
        if (Tools.hasValue(contactUs.getWebsite())) {
            binding.websiteTxt.setText(contactUs.getWebsite());
        }

    }


}
