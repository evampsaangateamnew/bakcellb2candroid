package com.bakcell.fragments.menus;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.databinding.FragmentStoreLocatorBinding;
import com.bakcell.fragments.pager.StoreLocatorListPaggerFragment;
import com.bakcell.fragments.pager.StoreLocatorMapPaggerFragment;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.interfaces.RedirectMapFromStoreListListener;
import com.bakcell.models.DataManager;
import com.bakcell.models.storelocator.StoreLocator;
import com.bakcell.models.storelocator.StoreLocatorMain;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RequestStoreLocatorService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;


/**
 * Created by Noman on 18-May-17.
 */

public class StoreLocatorFragment extends Fragment {

    public static final String KEY_STORE_LOCATOR = "key.store.locator";
    public static final String KEY_FROM_CONTACT_US = "key.from.contact.us";

    public static final String EN_STORE_TYPE_MAP_HEAD_OFFICE = "Head Office";
    public static final String EN_STORE_TYPE_MAP_CUSTOMER_CARE_OFFICE = "Customer care offices";
    public static final String EN_STORE_TYPE_MAP_BAKCEL_IM = "Bakcell-IM";
    public static final String EN_STORE_TYPE_MAP_DEALER_STORES = "Dealer stores";

    public static final String AZ_STORE_TYPE_MAP_HEAD_OFFICE = "Mərkəzi Ofis";
    public static final String AZ_STORE_TYPE_MAP_CUSTOMER_CARE_OFFICE = "Müştəri xidmətləri mərkəzləri";
    public static final String AZ_STORE_TYPE_MAP_BAKCEL_IM = "Bakcell-IM";
    public static final String AZ_STORE_TYPE_MAP_DEALER_STORES = "Diler mağazaları";

    public static final String RU_STORE_TYPE_MAP_HEAD_OFFICE = "Центральный Офис";
    public static final String RU_STORE_TYPE_MAP_CUSTOMER_CARE_OFFICE = "Офисы обслуживания";
    public static final String RU_STORE_TYPE_MAP_BAKCEL_IM = "Bakcell-IM";
    public static final String RU_STORE_TYPE_MAP_DEALER_STORES = "Дилерские магазины";

    FragmentStoreLocatorBinding binding;

    private StoreLocatorMain storeLocatorMain;

    private StoreLocator storeLocatorHeadOffice = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(StoreLocatorFragment.KEY_FROM_CONTACT_US)) {
            storeLocatorHeadOffice = getArguments().getParcelable(StoreLocatorFragment.KEY_FROM_CONTACT_US);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_store_locator, container, false);

        initUITabsAndPagger();

        RootValues.getInstance().setRedirectMapFromStoreListListener(redirectMapFromStoreListListener);

        requestStoreLocator();

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RootValues.getInstance().setRedirectMapFromStoreListListener(null);
    }

    private RedirectMapFromStoreListListener redirectMapFromStoreListListener = new RedirectMapFromStoreListListener() {
        @Override
        public void onRedirectMapFromStoreListListen(StoreLocator storeLocator) {
            binding.viewPager.setCurrentItem(0);
            RootValues.getInstance().getRedirectMapListener().onRedirectMapListen(storeLocator);
        }
    };

    private void prepareDataToUI(StoreLocatorMain storeLocatorMain) {
        if (storeLocatorMain == null) return;

        iniPaggerView(storeLocatorMain);

    }

    private void initUITabsAndPagger() {
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.store_locator_tab_lbl));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.store_locator_list_tab_lbl));

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            int margin = (int) (10 * getResources().getDisplayMetrics().density);
            p.setMargins(margin, 0, margin, 0);
            tab.requestLayout();
        }

        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void iniPaggerView(StoreLocatorMain storeLocatorMain) {
        if (storeLocatorMain == null) return;

        final ArrayList<Fragment> fragmentsList = new ArrayList<>();
        fragmentsList.add(StoreLocatorMapPaggerFragment.getInstance(storeLocatorMain, storeLocatorHeadOffice));
        fragmentsList.add(StoreLocatorListPaggerFragment.getInstance(storeLocatorMain));

        binding.viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (fragmentsList != null) {
                        binding.viewPager.setAdapter(new PagerAdapterOffers(getChildFragmentManager(), fragmentsList));
                    }
                } catch (Exception e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            }
        }, 300);

    }


    public void requestStoreLocator() {


        RequestStoreLocatorService.newInstance(getActivity(), ServiceIDs.REQUEST_STORE_LOCATOR)
                .execute(DataManager.getInstance().getCurrentUser(), "",
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                RootValues.getInstance().setMyStoreLocatorApiCall(false);

                                String result = response.getData();
                                if (result == null) return;

                                storeLocatorMain = new Gson().fromJson(result, StoreLocatorMain.class);

                                prepareDataToUI(storeLocatorMain);

                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (getActivity() != null && !getActivity().isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                    return;
                                }

                                // Load Cashed Data
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getStoreLocatorCacheKey(getActivity());
                                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            storeLocatorMain = new Gson().fromJson(cacheData, StoreLocatorMain.class);
                                            prepareDataToUI(storeLocatorMain);
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                        }
                                    }
                                }

                                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    // Load Cashed Data
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getStoreLocatorCacheKey(getActivity());
                                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            storeLocatorMain = new Gson().fromJson(cacheData, StoreLocatorMain.class);
                                            prepareDataToUI(storeLocatorMain);
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                        }
                                    }

                                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                                }
                            }

                        });

    }


    public static int getMarkerIcon(StoreLocator storeLocator, boolean isFromMap) {

        int iconMarker = R.drawable.marker4;

        if (storeLocator != null && Tools.hasValue(storeLocator.getType())) {
            String storeType = storeLocator.getType().trim();

            if (EN_STORE_TYPE_MAP_HEAD_OFFICE.equalsIgnoreCase(storeType)
                    || AZ_STORE_TYPE_MAP_HEAD_OFFICE.equalsIgnoreCase(storeType)
                    || RU_STORE_TYPE_MAP_HEAD_OFFICE.equalsIgnoreCase(storeType)) {
                if (isFromMap) {
                    iconMarker = R.drawable.marker3;
                } else {
                    iconMarker = R.drawable.listmarker3;
                }
            } else if (EN_STORE_TYPE_MAP_CUSTOMER_CARE_OFFICE.equalsIgnoreCase(storeType)
                    || AZ_STORE_TYPE_MAP_CUSTOMER_CARE_OFFICE.equalsIgnoreCase(storeType)
                    || RU_STORE_TYPE_MAP_CUSTOMER_CARE_OFFICE.equalsIgnoreCase(storeType)) {
                if (isFromMap) {
                    iconMarker = R.drawable.marker1;
                } else {
                    iconMarker = R.drawable.listmarker1;
                }
            } else if (EN_STORE_TYPE_MAP_BAKCEL_IM.equalsIgnoreCase(storeType)
                    || AZ_STORE_TYPE_MAP_BAKCEL_IM.equalsIgnoreCase(storeType)
                    || RU_STORE_TYPE_MAP_BAKCEL_IM.equalsIgnoreCase(storeType)) {
                if (isFromMap) {
                    iconMarker = R.drawable.marker2;
                } else {
                    iconMarker = R.drawable.listmarker2;
                }
            } else if (EN_STORE_TYPE_MAP_DEALER_STORES.equalsIgnoreCase(storeType)
                    || AZ_STORE_TYPE_MAP_DEALER_STORES.equalsIgnoreCase(storeType)
                    || RU_STORE_TYPE_MAP_DEALER_STORES.equalsIgnoreCase(storeType)) {
                if (isFromMap) {
                    iconMarker = R.drawable.marker4;
                } else {
                    iconMarker = R.drawable.listmarker4;
                }
            }

        }

        return iconMarker;

    }


}
