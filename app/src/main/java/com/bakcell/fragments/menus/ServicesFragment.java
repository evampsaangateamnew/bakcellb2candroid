package com.bakcell.fragments.menus;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.landing.AutoPaymentActivity;
import com.bakcell.activities.landing.CoreServicesActivity;
import com.bakcell.activities.landing.FNFActivity;
import com.bakcell.activities.landing.FastTopUpActivity;
import com.bakcell.activities.landing.LoanActivity;
import com.bakcell.activities.landing.MoneyTransferActivity;
import com.bakcell.activities.landing.QuickServicesActivity;
import com.bakcell.activities.landing.SupplementaryOffersActivity;
import com.bakcell.activities.landing.TopUpActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentServicesBinding;
import com.bakcell.globals.Constants;
import com.bakcell.models.DataManager;
import com.bakcell.utilities.Tools;

import static com.bakcell.R.layout.fragment_services;

/**
 * Created by Zeeshan Shabbir on 6/20/2017.
 */

public class ServicesFragment extends Fragment implements View.OnClickListener {

    FragmentServicesBinding binding;

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, fragment_services, container, false);
        initUiContent();
        initUiListeners();

        checkCustomerInfoForHidingManageNumber();

        AppEventLogs.applyAppEvent(AppEventLogValues.ServiceEvents.SERVICE_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_SCREEN,
                AppEventLogValues.ServiceEvents.SERVICE_SCREEN);
        return binding.getRoot();
    }

    private void checkCustomerInfoForHidingManageNumber() {
        if (DataManager.getInstance().getCurrentUser() != null) {
            if (DataManager.getInstance().getCurrentUser().getHideNumberTariffIds() != null) {
                //get the current user logged in offering id
                if (DataManager.getInstance().getCurrentUser().getOfferingId() != null) {
                    if (Tools.hasValue(DataManager.getInstance().getCurrentUser().getOfferingId())) {
                        //now check if the array list contains this offering id
                        if (DataManager.getInstance().getCurrentUser().getHideNumberTariffIds()
                                .contains(DataManager.getInstance().getCurrentUser().getOfferingId())) {
                            binding.coreServicesLayout.setVisibility(View.GONE);
                        } else {
                            binding.coreServicesLayout.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
    }

    private void initUiContent() {
        binding.tvFNF.setSelected(true);
        binding.tvFreeSms.setSelected(true);
        binding.tvManageNumber.setSelected(true);
        binding.tvPackages.setSelected(true);

        //Top-up portion
        binding.tvLoan.setSelected(true);
        binding.tvMoneyTransfer.setSelected(true);
        binding.tvTopup.setSelected(true);


    }

    private void initUiListeners() {

        if (DataManager.getInstance().getUserPredefinedData() != null) {
            if (DataManager.getInstance().getUserPredefinedData().getFnf() != null && Tools.hasValue(DataManager.getInstance().getUserPredefinedData().getFnf().isFnFAllowed())) {
                if (DataManager.getInstance().getUserPredefinedData().getFnf()
                        .isFnFAllowed().equalsIgnoreCase("true")) {
                    binding.fAndFLayout.setVisibility(View.VISIBLE);
                    binding.fAndFSeparator.setVisibility(View.VISIBLE);
                } else {
                    binding.fAndFLayout.setVisibility(View.GONE);
                    binding.fAndFSeparator.setVisibility(View.GONE);
                }
            } else {
                binding.fAndFLayout.setVisibility(View.GONE);
                binding.fAndFSeparator.setVisibility(View.GONE);
            }
        }

        binding.supplementaryLayout.setOnClickListener(this);
        binding.fAndFLayout.setOnClickListener(this);
        binding.coreServicesLayout.setOnClickListener(this);
        binding.quickServiceLayout.setOnClickListener(this);



        //topup portion
        binding.topupLayout.setOnClickListener(this);
        binding.topupAutoLayout.setOnClickListener(this);
        binding.topupFastLayout.setOnClickListener(this);
        binding.loanLayout.setOnClickListener(this);
        binding.moneyTransferLayout.setOnClickListener(this);

        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null && DataManager.getInstance().getCurrentUser().getSubscriberType().equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_POSTPAID)) {
            binding.moneyTransferLayout.setVisibility(View.GONE);
            binding.loanLayout.setVisibility(View.GONE);
            binding.moneyBelowLine.setVisibility(View.GONE);
            binding.loanBelowLine.setVisibility(View.GONE);
        } else {
            binding.moneyTransferLayout.setVisibility(View.VISIBLE);
            binding.loanLayout.setVisibility(View.VISIBLE);
            binding.moneyBelowLine.setVisibility(View.VISIBLE);
            binding.loanBelowLine.setVisibility(View.VISIBLE);
        }

       /* if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null && DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
            binding.tvLoan.setText(R.string.activity_loan_cin_lbl);
        } else*/
        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null && DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_KLASS)) {
            binding.tvLoan.setText(R.string.activity_loan_klass_lbl);
        } else {
            binding.tvLoan.setText(R.string.activity_loan_cin_lbl);
        }

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.supplementary_layout:
                BaseActivity.startNewActivity(getActivity(), SupplementaryOffersActivity.class);
                break;
            case R.id.f_and_f_layout:
                BaseActivity.startNewActivity(getActivity(), FNFActivity.class);
                break;
            case R.id.core_services_layout:
                BaseActivity.startNewActivity(getActivity(), CoreServicesActivity.class);
                break;
            case R.id.quick_service_layout:
                BaseActivity.startNewActivity(getActivity(), QuickServicesActivity.class);
                break;
            case R.id.topup_layout:
                BaseActivity.startNewActivity(getActivity(), TopUpActivity.class);
                break;
            case R.id.loan_layout:
                BaseActivity.startNewActivity(getActivity(), LoanActivity.class);
                break;
            case R.id.money_transfer_layout:
                BaseActivity.startNewActivity(getActivity(), MoneyTransferActivity.class);
                break;
            case R.id.topup_auto_layout:
                BaseActivity.startNewActivity(getActivity(), AutoPaymentActivity.class);
                break;
            case R.id.topup_fast_layout:
                BaseActivity.startNewActivity(getActivity(), FastTopUpActivity.class);
                break;

        }

    }
}
