package com.bakcell.fragments.menus;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentTermsAndConditionsBinding;

/**
 * Created by Freeware Sys on 18-May-17.
 */

public class TermsAndConditionsFragment extends Fragment {

    FragmentTermsAndConditionsBinding binding;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms_and_conditions, container, false);

        AppEventLogs.applyAppEvent(AppEventLogValues.TermsAndConditionEvents.TERMS_AND_CONDITIONS_SCREEN,
                AppEventLogValues.TermsAndConditionEvents.TERMS_AND_CONDITIONS_SCREEN,
                AppEventLogValues.TermsAndConditionEvents.TERMS_AND_CONDITIONS_SCREEN);

        return binding.getRoot();
    }


}
