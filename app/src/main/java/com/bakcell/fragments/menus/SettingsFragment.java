package com.bakcell.fragments.menus;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import com.bakcell.R;
import com.bakcell.activities.BaseActivity;
import com.bakcell.activities.HomeActivity;
import com.bakcell.activities.landing.ChangePasswordActivity;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentSettingsBinding;
import com.bakcell.globals.AppClass;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.Constants;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.notifications.SettingsNotifications;
import com.bakcell.notifications.MyFirebaseMessagingService;
import com.bakcell.notifications.NotificationAppIddSetListenerInterface;
import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

/**
 * Created by Noman on 18-May-17.
 */

public class SettingsFragment extends Fragment implements View.OnClickListener {

    FragmentSettingsBinding binding;
    private final String fromClass = "SettingsFragment";
    private String selectedLanguage;

    private SettingsNotifications settingsNotifications;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);

        initFingerPrint();

        initUiListeners();

        settingsNotifications = DataManager.getInstance().getNotificationSettingsData(getActivity());
        processOnFCMCallResponse(settingsNotifications);

        addFCMTokenToServer(settingsNotifications.getRingingStatus(), Constants.NotificationConfigration.FCM_NOTIFICATION_ON, Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_LOGIN, true, true);
        return binding.getRoot();
    }

    private void initFingerPrint() {
        binding.fingerPrintSwitch.setChecked(PrefUtils.getBoolean(getContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false));
        try {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                // Initializing Fingerprint Manager
                FingerprintManager fingerprintManager = (FingerprintManager) getActivity().getSystemService(Context.FINGERPRINT_SERVICE);
                if (fingerprintManager != null) {
                    if (fingerprintManager.isHardwareDetected()) {
                        binding.fingerPrintScannerLayout.setVisibility(View.VISIBLE);
                    } else {
                        binding.fingerPrintScannerLayout.setVisibility(View.GONE);
                    }
                } else {
                    binding.fingerPrintScannerLayout.setVisibility(View.GONE);
                }
            } else {
                binding.fingerPrintScannerLayout.setVisibility(View.GONE);
            }
        } catch (Exception exp) {
            binding.fingerPrintScannerLayout.setVisibility(View.GONE);
        }

        binding.fingerPrintSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (getContext() == null || getActivity() == null) {
                    return;//to be on the safe side, this is safe passage
                }
                if (b) {
                    if (android.os.Build.VERSION.SDK_INT >= 23) {
                        // Initializing Fingerprint Manager
                        FingerprintManager fingerprintManager = (FingerprintManager) getActivity().getSystemService(Context.FINGERPRINT_SERVICE);
                        if (fingerprintManager != null) {
                            if (fingerprintManager.isHardwareDetected()) {

                                if (fingerprintManager.hasEnrolledFingerprints()) {
                                    binding.fingerPrintSwitch.setChecked(true);
                                    PrefUtils.addBoolean(getContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, true);
                                } else {
                                    BakcellPopUpDialog.showMessageDialog(getActivity(), getActivity().getResources().getString(R.string.bakcell_error_title), getActivity().getResources().getString(R.string.biometric_no_finger_prints_enrolled_error));
                                    binding.fingerPrintSwitch.setChecked(false);
                                    PrefUtils.addBoolean(getContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false);
                                }
                            } else {
                                binding.fingerPrintSwitch.setChecked(false);
                                PrefUtils.addBoolean(getContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false);
                                BakcellPopUpDialog.showMessageDialog(getActivity(), getActivity().getResources().getString(R.string.bakcell_error_title), getActivity().getResources().getString(R.string.biometric_not_available_error_label));
                            }
                        } else {
                            binding.fingerPrintSwitch.setChecked(false);
                            PrefUtils.addBoolean(getContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false);
                            BakcellPopUpDialog.showMessageDialog(getActivity(), getActivity().getResources().getString(R.string.bakcell_error_title), getActivity().getResources().getString(R.string.biometric_not_available_error_label));
                        }
                    } else {
                        binding.fingerPrintSwitch.setChecked(false);
                        PrefUtils.addBoolean(getContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false);
                        BakcellPopUpDialog.showMessageDialog(getActivity(), getActivity().getResources().getString(R.string.bakcell_error_title), getActivity().getResources().getString(R.string.biometric_not_available_error_label));
                    }
                } else {
                    binding.fingerPrintSwitch.setChecked(false);
                    PrefUtils.addBoolean(getContext(), PrefUtils.PreKeywords.FINGER_PRINT_SHOW, false);
                }
            }
        });
    }

    private void initUiListeners() {

        selectedLanguage = AppClass.getCurrentLanguageKey(getActivity());

        // Update check boxes according to selected languaeg
        if (selectedLanguage.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
            binding.currentLanguageText.setText(getString(R.string.lbl_en_english));
        } else if (selectedLanguage.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
            binding.currentLanguageText.setText(getString(R.string.lbl_az_azarbaycan));
        } else if (selectedLanguage.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
            binding.currentLanguageText.setText(getString(R.string.lbl_ru_rusian));
        } else {
            binding.currentLanguageText.setText(getString(R.string.lbl_en_english));
        }


        binding.appVerisonText.setText(Tools.getAppVersion(getActivity()));


        binding.llChangeLanguage.setOnClickListener(this);
        binding.llChangePassword.setOnClickListener(this);

        binding.notificationsSwitchLayout.setOnClickListener(this);

        binding.radioToneView.setOnClickListener(this);
        binding.radioVibrateView.setOnClickListener(this);
        binding.radioMuteView.setOnClickListener(this);


    }

    protected void changePasswordClicked() {
        BaseActivity.startNewActivity(getActivity(), ChangePasswordActivity.class);
    }

    protected void changeLanguageClicked() {

        if (getActivity() == null) return;
        if (getActivity().isFinishing()) return;

        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.dialog_select_langue, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            Button yesBtn = dialogView.findViewById(R.id.btn_done);

            RadioGroup radioGroup = dialogView.findViewById(R.id.languageGroup);

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    switch (checkedId) {
                        case R.id.azeri:
                            selectedLanguage = Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ;
                            break;
                        case R.id.russian:
                            selectedLanguage = Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU;
                            break;
                        case R.id.english:
                            selectedLanguage = Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN;
                            break;
                    }
                }
            });

            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!selectedLanguage.equalsIgnoreCase(AppClass.getCurrentLanguageKey(getActivity()))) {
                        alertDialog.dismiss();
                        /**remove all the caches related to the apis, promo message and app rater popup*/
                        DataManager.getInstance().setSupplementaryOfferMainData(null);
                        MultiAccountsHandler.resetAllUserDataAndAPICallerCaches(getActivity());
                        AppClass.setCurrentLanguageKey(getActivity(), selectedLanguage);
                        RootValues.getInstance().setLAST_SELECT_MENU("");
                        RootValues.getInstance().setIS_APP_OPENED_AFTER_CLOSED(true);
                        Bundle bundle = new Bundle();
                        bundle.putString(HomeActivity.KEY_HOME_FIRST_SCREEN, Constants.MenusKeyword.MENU_SETTINGS);
                        BaseActivity.startNewActivityAndClear(getActivity(), HomeActivity.class, bundle);

                        AppEventLogs.applyAppEvent(AppEventLogValues.SettingsEvents.SETTINGS_SCREEN,
                                AppEventLogValues.SettingsEvents.APP_LANGUAGE_CHANGED + " " + selectedLanguage,
                                AppEventLogValues.SettingsEvents.SETTINGS_SCREEN);
                    }//if (!selectedLanguage.equalsIgnoreCase(AppClass.getCurrentLanguageKey(getActivity()))) ends
                }//onClick ends
            });

            AppCompatRadioButton azeri = dialogView.findViewById(R.id.azeri);
            AppCompatRadioButton russian = dialogView.findViewById(R.id.russian);
            AppCompatRadioButton english = dialogView.findViewById(R.id.english);

            // Update check boxes according to selected languaeg
            if (!TextUtils.isEmpty(selectedLanguage)) {
                if (selectedLanguage.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_EN)) {
                    english.setChecked(true);
                } else if (selectedLanguage.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_AZ)) {
                    azeri.setChecked(true);
                } else if (selectedLanguage.equalsIgnoreCase(Constants.AppLanguageKeyword.APP_LANGUAGE_KEY_RU)) {
                    russian.setChecked(true);
                } else {
                    english.setChecked(true);
                }
            } else {
                english.setChecked(true);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_change_language:
                changeLanguageClicked();
                break;
            case R.id.ll_change_password:
                changePasswordClicked();
                break;
            case R.id.notificationsSwitchLayout:

                notificationSwithOnOff();

                break;
            case R.id.radioToneView:
                if (!binding.radioTone.isChecked()) {
                    addFCMTokenToServer(Constants.NotificationConfigration.FCM_NOTIFICATION_TONE, settingsNotifications.getIsEnable(), Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_SETTINGS, false, false);
                } else {
                    return;
                }
                break;
            case R.id.radioVibrateView:
                if (!binding.radioVibrate.isChecked()) {
                    addFCMTokenToServer(Constants.NotificationConfigration.FCM_NOTIFICATION_VIBRATE, settingsNotifications.getIsEnable(), Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_SETTINGS, false, false);
                } else {
                    return;
                }
                break;
            case R.id.radioMuteView:
                if (!binding.radioMute.isChecked()) {
                    addFCMTokenToServer(Constants.NotificationConfigration.FCM_NOTIFICATION_MUTE, settingsNotifications.getIsEnable(), Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_SETTINGS, false, false);
                } else {
                    return;
                }
                break;
        }
    }


    private void notificationSwithOnOff() {

        if (binding.notificationsSwitch.isChecked()) {
            addFCMTokenToServer(settingsNotifications.getRingingStatus(), Constants.NotificationConfigration.FCM_NOTIFICATION_OFF, Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_SETTINGS, false, false);
            AppEventLogs.applyAppEvent(AppEventLogValues.SettingsEvents.SETTINGS_SCREEN,
                    AppEventLogValues.SettingsEvents.NOTIFICATION_DISABLED,
                    AppEventLogValues.SettingsEvents.SETTINGS_SCREEN);
        } else {
            addFCMTokenToServer(settingsNotifications.getRingingStatus(), Constants.NotificationConfigration.FCM_NOTIFICATION_ON, Constants.NotificationConfigration.FCM_NOTIFICATION_API_CAUSE_SETTINGS, false, false);
            AppEventLogs.applyAppEvent(AppEventLogValues.SettingsEvents.SETTINGS_SCREEN,
                    AppEventLogValues.SettingsEvents.NOTIFICATION_ENABLED,
                    AppEventLogValues.SettingsEvents.SETTINGS_SCREEN);
        }

    }


    private void addFCMTokenToServer(final String ringingStatus, final String isEnable, String cause, boolean isBackground, final boolean isFirstTime) {
        if (getActivity() == null || getActivity().isFinishing()) return;
        try {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            BakcellLogger.logE("fcm", "fcm token task not successful", fromClass, "addFCMTokenToServer");
                            return;
                        }

                        if (task.getResult() != null) {
                            String token = task.getResult().getToken();
                            MyFirebaseMessagingService.requestingForSendFCMToken(isBackground, getActivity(),
                                    DataManager.getInstance().getCurrentUser(),
                                    token,
                                    ringingStatus, isEnable, cause, new NotificationAppIddSetListenerInterface() {
                                        @Override
                                        public void onAppIdSentSuccess(SettingsNotifications settingsNotifi, String message) {
                                            if (getActivity() == null || getActivity().isFinishing()) return;
                                            try {
                                                if (settingsNotifi != null && Tools.hasValue(settingsNotifi.getRingingStatus()) && Tools.hasValue(settingsNotifi.getIsEnable())) {
                                                    settingsNotifications = new SettingsNotifications();
                                                    settingsNotifications.setRingingStatus(settingsNotifi.getRingingStatus());
                                                    settingsNotifications.setIsEnable(settingsNotifi.getIsEnable());
                                                    processOnFCMCallResponse(settingsNotifications);
                                                    BakcellLogger.logE("fcm", "fcm token sent successfully", fromClass, "addFCMTokenToServer");
                                                }

                                                if (!isFirstTime) {
                                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                                            getString(R.string.mesg_successful_title), message);
                                                }
                                            } catch (Exception e) {
                                                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onAppIdSentError(int resultCode, String message) {
                                            if (getActivity() == null || getActivity().isFinishing()) return;

                                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                                    getString(R.string.bakcell_error_title), message);

                                        }
                                    });
                        }
                    });
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

    }


    public void processOnFCMCallResponse(SettingsNotifications settingsNotifi) {
        if (settingsNotifi == null) return;

        if (Tools.hasValue(settingsNotifi.getIsEnable())) {
            if (settingsNotifi.getIsEnable().equalsIgnoreCase(Constants.NotificationConfigration.FCM_NOTIFICATION_ON)) {
                binding.notificationsSwitch.setChecked(true);

                binding.radioToneView.setEnabled(true);
                binding.radioVibrateView.setEnabled(true);
                binding.radioMuteView.setEnabled(true);

            } else if (settingsNotifi.getIsEnable().equalsIgnoreCase(Constants.NotificationConfigration.FCM_NOTIFICATION_OFF)) {
                binding.notificationsSwitch.setChecked(false);

                binding.radioToneView.setEnabled(false);
                binding.radioVibrateView.setEnabled(false);
                binding.radioMuteView.setEnabled(false);
            }
        }

        if (Tools.hasValue(settingsNotifi.getRingingStatus())) {
            if (Constants.NotificationConfigration.FCM_NOTIFICATION_TONE.equalsIgnoreCase(settingsNotifi.getRingingStatus())) {
                binding.radioTone.setChecked(true);
                binding.radioVibrate.setChecked(false);
                binding.radioMute.setChecked(false);
            } else if (Constants.NotificationConfigration.FCM_NOTIFICATION_VIBRATE.equalsIgnoreCase(settingsNotifi.getRingingStatus())) {
                binding.radioTone.setChecked(false);
                binding.radioVibrate.setChecked(true);
                binding.radioMute.setChecked(false);
            } else if (Constants.NotificationConfigration.FCM_NOTIFICATION_MUTE.equalsIgnoreCase(settingsNotifi.getRingingStatus())) {
                binding.radioTone.setChecked(false);
                binding.radioVibrate.setChecked(false);
                binding.radioMute.setChecked(true);
            }
        }

    }


}
