package com.bakcell.fragments.menus;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bakcell.R;
import com.bakcell.adapters.AdapterIndividualNew;
import com.bakcell.adapters.AdapterTariffsCINNew;
import com.bakcell.adapters.AdapterTraiffsCorporate;
import com.bakcell.adapters.AdapterTraiffsKlass;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentTariffsBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.Constants;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.globals.TariffsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.tariffs.HeaderAttributes;
import com.bakcell.models.tariffs.MainTariffs;
import com.bakcell.models.tariffs.TariffCinNew;
import com.bakcell.models.tariffs.corporateobject.TariffCorporate;
import com.bakcell.models.tariffs.helper.TariffsHelperModel;
import com.bakcell.models.tariffs.individualobject.TariffIndividual;
import com.bakcell.models.tariffs.tariffcinobject.TariffCin;
import com.bakcell.models.tariffs.tariffdetails.DetailsAttributes;
import com.bakcell.models.tariffs.tariffdetails.Price;
import com.bakcell.models.tariffs.tariffdetails.RoamingDetailsCountries;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.TariffKlass;
import com.bakcell.models.user.UserModel;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.viewsitem.TariffDetailLayout;
import com.bakcell.viewsitem.ViewItemOfferDetailBulletsItem;
import com.bakcell.viewsitem.ViewItemOfferDetailPriceAttribute;
import com.bakcell.viewsitem.ViewItemOfferDetailRoamingCountries;
import com.bakcell.viewsitem.ViewItemOfferDetailRoundingAttribute;
import com.bakcell.viewsitem.ViewItemOfferDetailTitleWithSubTitleDescAttribute;
import com.bakcell.viewsitem.ViewItemTariffsDetailPrice;
import com.bakcell.webservices.GetTariffsListsService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.bakcell.widgets.BakcellTextViewNormal;
import com.bakcell.widgets.RecyclerItemDecorator;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Objects;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Noman on 18-May-17.
 */

public class TariffsFragment extends Fragment {

    private final String fromClass = "TariffsFragment";

    public static final String KEY_DATA_FROM_NOTIFICATION = "key.data.notification";
    public static final String KEY_DATA_FROM_HOME_TO_TARIFF = "key.data.home.to.tariff";

    public static final String CAN_NOT_SUBSCRIBE = "0";
    public static final String CAN_SUBSCRIBE = "1";
    public static final String ALREADY_SUBSCRIBED = "2";
    public static final String RENEWABLE = "3";

    public static final String TARIFF_SUBSCRIBE = "Subscribe";
    public static final String TARIFF_RENEW = "Renew";

    public static final String CALL_STRING = "tsDCall";
    public static final String INTERNET_STRING = "tsDInternet";
    public static final String SMS_STRING = "tsDSMS";
    public static final String COUNTRY_WIDE_STRING = "tsDDistination";
    public static final String WHATSAPP_STRING = "tsDWhatsapp";

    public static final String VALUE_FREE_EN = "FREE";
    public static final String VALUE_FREE_RU = "БЕСПЛАТНО";
    public static final String VALUE_FREE_AZ = "PULSUZ";

    public static final String VALUE_UNLIMITED_EN = "Unlimited";
    public static final String VALUE_UNLIMITED_RU = "Безлимитный";
    public static final String VALUE_UNLIMITED_AZ = "Limitsiz";

    //ICONS MAP FOR TARIFFS
    public static final String ICON_MAP_WHATSAPP_USING = "whatsappUsing";
    public static final String ICON_MAP_TSD_CALL = "tsDCall";
    public static final String ICON_MAP_ROAMING_CALLS = "roamingCalls";
    public static final String ICON_MAP_COUNTRY_WIDE_CALL = "countrywideCall";
    public static final String ICON_MAP_INTERNATION_CALLS = "internationalCalls";
    public static final String ICON_MAP_TSD_SMS = "tsDSMS";
    public static final String ICON_MAP_COUNTRY_WIDE_SMS = "countrywideSMS";
    public static final String ICON_MAP_TSD_INTERNET = "tsDInternet";
    public static final String ICON_MAP_TSD_COUNTRY_WIDE_INTERNET = "countrywideInternet";
    public static final String ICON_MAP_ROAMING_INTERNET = "roamingInternet";
    public static final String ICON_MAP_TSD_DISTINATION = "tsDDistination";
    public static final String ICON_MAP_TSD_ROUNDING = "tsDRounding";
    public static final String ICON_MAP_DISCOUNT = "discount";
    public static final String ICON_MAP_FREE = "free";
    public static final String ICON_MAP_TS_WHATSAPP = "tsDWhatsapp";
    public static final String ICON_MAP_TS_MMS = "tsDMMS";
    private MainTariffs mainTariffs = null;

    FragmentTariffsBinding binding;

    private boolean isKlassPayG = false;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(TariffsFragment.KEY_DATA_FROM_HOME_TO_TARIFF)) {
            isKlassPayG = getArguments().getBoolean(TariffsFragment.KEY_DATA_FROM_HOME_TO_TARIFF);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tariffs, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(Objects.requireNonNull(getContext()).getResources().getString(R.string.loading));
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(true);

        // Tariff API Call
        requestForGetTariffsLists();

        AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                AppEventLogValues.TariffEvents.TARIFF_SCREEN);


        return binding.getRoot();
    }


    /**
     * Setting data to UI elements
     *
     * @param isPrepaidCustomer
     * @param mainTariffs
     */
    private void initUITabs(final boolean isPrepaidCustomer, final MainTariffs mainTariffs) {
        if (mainTariffs != null && getActivity() != null && !getActivity().isFinishing()) {
        } else {
            return;
        }

        try {
            setNoDataFound(View.GONE);
            binding.tabLayout.removeAllTabs();
            // Adding Tabs here according to customer type
            if (isPrepaidCustomer) {
                // Tabs for prepaid users KLASS and CIN
                binding.tabLayout.setVisibility(View.VISIBLE);
                binding.tabLayout.addTab(binding.tabLayout.newTab()
                        .setText(R.string.tarif_tab_title_klass));
                binding.tabLayout.addTab(binding.tabLayout.newTab()
                        .setText(R.string.tarif_tab_title_cin));
            } else {
                // Tabs for postpaid users CORPORATE and INDIVIDUAL
                binding.tabLayout.setVisibility(View.GONE);
                binding.tabLayout.addTab(binding.tabLayout.newTab()
                        .setText(R.string.tarif_tab_title_corporate));
                binding.tabLayout.addTab(binding.tabLayout.newTab()
                        .setText(R.string.tarif_tab_individual));
            }


            try {
                setNoDataFound(View.GONE);
                if (getActivity() != null && !getActivity().isFinishing()) {
                    for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
                        View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
                        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
                        int marign = (int) (20 * getActivity().getResources().getDisplayMetrics().density);
                        p.setMargins(marign, 0, marign, 0);
                        tab.requestLayout();
                    }
                }
            } catch (Exception e) {
                setNoDataFound(View.VISIBLE);
                Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
            }

            // UI Tab change listener
            binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (tab.getPosition() == 0) {
                        if (isPrepaidCustomer) {
                            if (mainTariffs.getTariffPrepaid().getTariffsKlasses() != null) {
                                prepareKlassTabData(mainTariffs.getTariffPrepaid().getTariffsKlasses());
                            }
                        }
                    } else {
                        if (isPrepaidCustomer) {
                            prepareCinTabData(mainTariffs);
                        }
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            if (isPrepaidCustomer) {
                if (mainTariffs.getTariffPrepaid().getTariffsKlasses() != null) {
                    setNoDataFound(View.GONE);
                    prepareKlassTabData(mainTariffs.getTariffPrepaid().getTariffsKlasses());//individual
                } else {
                    setNoDataFound(View.VISIBLE);
                }
            } else {
                UserModel customerData = DataManager.getInstance().getCurrentUser();
                if (Tools.hasValue(customerData.getCustomerType())) {
                    setNoDataFound(View.GONE);
                    if (customerData.getBrandName().toLowerCase().contains(Constants.UserCurrentKeyword.USER_BRAND_KLASS)) {
                        if (mainTariffs.getTariffPostpaid() != null &&
                                mainTariffs.getTariffPostpaid().getKlassPostpaid() != null) {
                            setNoDataFound(View.GONE);
                            prepareKlassTabData(mainTariffs.getTariffPostpaid().getKlassPostpaid());
                            AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                                    AppEventLogValues.TariffEvents.POSTPAID_KLASS_SCREEN,
                                    AppEventLogValues.TariffEvents.TARIFF_SCREEN);
                        } else {
                            setNoDataFound(View.VISIBLE);
                        }
                    } else {
                        if (Tools.hasValue(customerData.getCustomerType())
                                && customerData.getCustomerType().toLowerCase().contains(Constants.UserCurrentKeyword
                                .CUSTOMER_TYPE_POSTPAID_INDIVIDUAL_TEM)) {
                            if (mainTariffs.getTariffPostpaid() != null &&
                                    mainTariffs.getTariffPostpaid().getIndividual() != null) {
                                setNoDataFound(View.GONE);
                                prepareIndividualTabData(mainTariffs.getTariffPostpaid().getIndividual());
                            } else {
                                setNoDataFound(View.VISIBLE);
                            }
                        } else if (Tools.hasValue(customerData.getCustomerType())
                                && customerData.getCustomerType().toLowerCase().contains(Constants.UserCurrentKeyword
                                .CUSTOMER_TYPE_POSTPAID_CORPORATE)) {
                            setNoDataFound(View.GONE);
                            if (mainTariffs.getTariffPostpaid() != null &&
                                    mainTariffs.getTariffPostpaid().getCorporate() != null) {
                                setNoDataFound(View.GONE);
                                prepareCorporateTabData(mainTariffs.getTariffPostpaid().getCorporate());
                                AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                                        AppEventLogValues.TariffEvents.TARIFF_CORPORATE_SCREEN,
                                        AppEventLogValues.TariffEvents.TARIFF_SCREEN);
                            } else {
                                setNoDataFound(View.VISIBLE);
                            }
                        } else {
                            setNoDataFound(View.VISIBLE);
                        }
                    }
                } else {
                    setNoDataFound(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            setNoDataFound(View.VISIBLE);
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    boolean isSecondPageLoaded; //Prevents setting adapter twice

    private void prepareCinTabData(MainTariffs mainTariffs) {
        AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                AppEventLogValues.TariffEvents.TARIFF_CIN_SCREEN,
                AppEventLogValues.TariffEvents.TARIFF_SCREEN);
        if (isSecondPageLoaded) {
            binding.myviewpager.setVisibility(View.INVISIBLE);
            binding.myviewpager1.setVisibility(View.VISIBLE);
        } else {
            binding.myviewpager.setVisibility(View.INVISIBLE);
            binding.myviewpager1.setVisibility(View.VISIBLE);
            binding.myviewpager1.setOrientation(Orientation.HORIZONTAL);
            binding.myviewpager1.setItemTransformer(new ScaleTransformer.Builder().build());
            binding.myviewpager1.addItemDecoration(new RecyclerItemDecorator());

            if (getActivity() != null &&
                    !getActivity().isFinishing() &&
                    progressDialog != null &&
                    !progressDialog.isShowing()) {
                progressDialog.show();
            }

            new Handler().postDelayed(() -> {
                /**get the combined list of CIN & CIN_NEW tariffs*/
                ArrayList<TariffsHelperModel> combinedCINList = getCombinedCINNewTariffsList(
                        mainTariffs.getTariffPrepaid().getTariffCins(),
                        mainTariffs.getTariffPrepaid().getTariffCinNew());
                if (combinedCINList != null && !combinedCINList.isEmpty()) {
                    //safe passage
                } else {
                    isSecondPageLoaded = false;
                    progressDialog.dismiss();
                    return;
                }

                binding.myviewpager1.setAdapter(new AdapterTariffsCINNew(getActivity(),
                        getContext(), combinedCINList));
                binding.myviewpager1.scrollToPosition(0);

                isSecondPageLoaded = true;
            }, 1000);

            new Handler().postDelayed(() -> {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }, 1500);
        }

    }

    boolean isKlassLoaded; //Prevents setting adapter twice

    private void prepareKlassTabData(ArrayList<TariffKlass> klassArrayList) {
        if (getActivity() != null &&
                !getActivity().isFinishing()) {
            AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                    AppEventLogValues.TariffEvents.TARIFF_KLASS_SCREEN,
                    AppEventLogValues.TariffEvents.TARIFF_SCREEN);
            if (isKlassLoaded) {
                binding.myviewpager1.setVisibility(View.INVISIBLE);
                binding.myviewpager.setVisibility(View.VISIBLE);
            } else {

                if (getActivity() != null &&
                        !getActivity().isFinishing()) {
                    if (progressDialog != null &&
                            !progressDialog.isShowing()) {
                        progressDialog.show();
                    }
                }

                new Handler().postDelayed(() -> {
                    binding.myviewpager1.setVisibility(View.INVISIBLE);
                    binding.myviewpager.setVisibility(View.VISIBLE);
                    binding.myviewpager.setOrientation(Orientation.HORIZONTAL);
                    binding.myviewpager.setItemTransformer(new ScaleTransformer.Builder()
                            .build());
                    binding.myviewpager.addItemDecoration(new RecyclerItemDecorator());
                    if (klassArrayList == null) {
                        isKlassLoaded = false;
                        progressDialog.dismiss();
                        return;
                    }
                    binding.myviewpager.setAdapter(new AdapterTraiffsKlass(getActivity(), klassArrayList,
                            false, false, false));
                    isKlassLoaded = true;
                }, 1000);

                new Handler().postDelayed(() -> {
                    if (getActivity() != null && !getActivity().isFinishing()
                            && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }, 2500);
            }
        }
    }

    private ArrayList<TariffsHelperModel> getCombinedCINNewTariffsList(ArrayList<TariffCin> tariffCins, ArrayList<TariffCinNew> tariffCinNew) {
        return TariffsHandler.getListOfCINAndCIN_NEWTariffs(tariffCins, tariffCinNew);
    }//getCombinedCINNewTariffsList ends

    boolean isCorporateLoaded;

    private void prepareCorporateTabData(ArrayList<TariffCorporate> mainTariffs) {
        if (isCorporateLoaded) {
            binding.myviewpager1.setVisibility(View.INVISIBLE);
            binding.myviewpager.setVisibility(View.VISIBLE);
        } else {
            binding.myviewpager1.setVisibility(View.INVISIBLE);
            binding.myviewpager.setVisibility(View.VISIBLE);
            binding.myviewpager.setOrientation(Orientation.HORIZONTAL);
            binding.myviewpager.setItemTransformer(new ScaleTransformer.Builder()
                    .build());
            binding.myviewpager.addItemDecoration(new RecyclerItemDecorator());
            binding.myviewpager.setAdapter(new AdapterTraiffsCorporate(getActivity(),
                    mainTariffs, false, false, false));
            isCorporateLoaded = true;
        }
    }

    boolean isIndivialualLoaded;

    private void prepareIndividualTabData(ArrayList<TariffIndividual> mainTariffs) {
        AppEventLogs.applyAppEvent(AppEventLogValues.TariffEvents.TARIFF_SCREEN,
                AppEventLogValues.TariffEvents.TARIFF_INDIVIDUAL_SCREEN,
                AppEventLogValues.TariffEvents.TARIFF_SCREEN);
        if (mainTariffs != null && mainTariffs.size() > 0) {
            setNoDataFound(View.GONE);

            if (isIndivialualLoaded) {
                binding.myviewpager1.setVisibility(View.VISIBLE);
                binding.myviewpager.setVisibility(View.INVISIBLE);
            } else {
                binding.myviewpager1.setVisibility(View.VISIBLE);
                binding.myviewpager.setVisibility(View.INVISIBLE);
                binding.myviewpager1.setOrientation(Orientation.HORIZONTAL);
                binding.myviewpager1.setItemTransformer(new ScaleTransformer.Builder()
                        .build());
                binding.myviewpager1.addItemDecoration(new RecyclerItemDecorator());
                binding.myviewpager1.setAdapter(new AdapterIndividualNew(getActivity(),
                        mainTariffs, false, false, false));
                isIndivialualLoaded = true;
            }
        } else {
            setNoDataFound(View.VISIBLE);
        }
    }

    private void requestForGetTariffsLists() {

        UserModel customerData = DataManager.getInstance().getCurrentUser();

        if (customerData == null) {
            Toast.makeText(getActivity(), R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show();
            return;
        }

        GetTariffsListsService.newInstance(getActivity(), ServiceIDs.GET_TARIFFS_LISTS).execute(customerData,
                new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        if (getActivity() == null || getActivity().isFinishing()) return;

                        RootValues.getInstance().setTariffApiCall(false);

                        String result = response.getData();

                        try {
                            mainTariffs = new Gson().fromJson(result, MainTariffs.class);
                        } catch (JsonSyntaxException e) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                        }
                        if (mainTariffs != null) {
                            setNoDataFound(View.GONE);
                            prepareDataToUI(mainTariffs);
                           /* if (mainTariffs.getTariffPrepaid() != null &&
                                    mainTariffs.getTariffPrepaid().getTariffCinNew() != null &&
                                    mainTariffs.getTariffPrepaid().getTariffCins() != null &&
                                    mainTariffs.getTariffPrepaid().getTariffsKlasses() != null) {
                                BakcellLogger.logE("abcX2", "headerName:::" +
                                                mainTariffs.getTariffPrepaid().getTariffCinNew().get(0).getHeader().getTariffType(),
                                        fromClass, "requestForGetTariffsLists");
                            }//not null check ends*/
                        } else { //main tariffs not null
                            setNoDataFound(View.VISIBLE);
                        }

                    }//onSuccess ends

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
//                            HomeActivity.logoutUser(getActivity());
//                            return;
                        }

                        // Load Cashed Data
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            String cacheKey = MultiAccountsHandler.CacheKeys.getTariffsCacheKey(getActivity());
                            String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                            if (Tools.hasValue(cacheResponse)) {
                                try {
                                    JSONObject jsonObject = new JSONObject(cacheResponse);
                                    String cacheData = jsonObject.getJSONObject("data").toString();
                                    try {
                                        mainTariffs = new Gson().fromJson(cacheData, MainTariffs.class);
                                    } catch (JsonSyntaxException e) {
                                        Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                                    }
                                    if (mainTariffs != null) {
                                        setNoDataFound(View.GONE);
                                        prepareDataToUI(mainTariffs);
                                    } else {
                                        setNoDataFound(View.VISIBLE);
                                    }
                                } catch (JSONException ex) {
                                    Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                }
                            }
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {

                            // Load Cashed Data
                            if (getActivity() != null && !getActivity().isFinishing()) {
                                String cacheKey = MultiAccountsHandler.CacheKeys.getTariffsCacheKey(getActivity());
                                String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                if (Tools.hasValue(cacheResponse)) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(cacheResponse);
                                        String cacheData = jsonObject.getJSONObject("data").toString();
                                        try {
                                            mainTariffs = new Gson().fromJson(cacheData, MainTariffs.class);
                                        } catch (JsonSyntaxException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                        }
                                        if (mainTariffs != null) {
                                            setNoDataFound(View.GONE);
                                            prepareDataToUI(mainTariffs);
                                        } else {
                                            setNoDataFound(View.VISIBLE);
                                        }
                                    } catch (JSONException ex) {
                                        Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                    }
                                }
                            }

                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }
                });

    }

    /**
     * Prepares UI according to customer type
     *
     * @param mainTariffs
     */
    private void prepareDataToUI(final MainTariffs mainTariffs) {
        try {
            if (getActivity() == null || getActivity().isFinishing()) return;
            if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null
                    && DataManager.getInstance().getCurrentUser().getSubscriberType()
                    .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)) {
                initUITabs(true, mainTariffs);
            } else {
                initUITabs(false, mainTariffs);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }

        // Check if Tariff from Dashboard And/OR from Notifications
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (RootValues.getInstance().isIsTariffFromDashBoard()) {
                goToSpacificTariffItem(mainTariffs);
                RootValues.getInstance().setIsTariffFromDashBoard(false);
                RootValues.getInstance().setOfferingIdRedirectFromNotifications("");
            }
        }, 1000);
    }

    private void goToSpacificTariffItem(MainTariffs mainTariffs) {
        if (mainTariffs == null || getActivity() == null || getActivity().isFinishing()) return;

        if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null && DataManager.getInstance().getCurrentUser().getSubscriberType()
                .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)) {
            //Prepaid
            if (DataManager.getInstance() != null && DataManager.getInstance().getCurrentUser() != null &&
                    DataManager.getInstance().getCurrentUser().getBrandName().equalsIgnoreCase(Constants.UserCurrentKeyword.USER_BRAND_CIN)) {
                //Cin

                TabLayout.Tab tab = binding.tabLayout.getTabAt(1);
                tab.select();

                prepareCinTabData(mainTariffs);

                if (mainTariffs.getTariffPrepaid() != null) {
                    goToSubscribeTariffPrepaidCin(mainTariffs);
                }

            } else {
                //Klass

                if (mainTariffs.getTariffPrepaid() != null) {
                    goToSubscribeTariffPrepaidKlass(mainTariffs);
                }

            }
        } else {
            //Postpaid
            if (DataManager.getInstance() != null &&
                    DataManager.getInstance().getCurrentUser() != null &&
                    Tools.hasValue(DataManager.getInstance().getCurrentUser().getCustomerType()) &&
                    DataManager.getInstance().getCurrentUser().getCustomerType().toLowerCase().contains(Constants.UserCurrentKeyword.CUSTOMER_TYPE_POSTPAID_INDIVIDUAL)) {
                //Individual

                if (mainTariffs.getTariffPostpaid() != null) {
                    goToSubscribeTariffPostpaidIndividual(mainTariffs);
                }

            } else {
                //Corporate
                if (mainTariffs.getTariffPostpaid() != null) {
                    goToSubscribeTariffPostpaidCorporate(mainTariffs);
                }
            }
        }

    }

    private void goToSubscribeTariffPrepaidCin(MainTariffs mainTariffs) {
        if (mainTariffs == null || mainTariffs.getTariffPrepaid() == null || mainTariffs.getTariffPrepaid().getTariffCins() == null || mainTariffs.getTariffPrepaid().getTariffCins().size() < 1)
            return;
        ArrayList<TariffsHelperModel> cinsList = getCombinedCINNewTariffsList(
                mainTariffs.getTariffPrepaid().getTariffCins(),
                mainTariffs.getTariffPrepaid().getTariffCinNew());

        if (!Tools.hasValue(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
            // From Dashboard
            int matchedIndex = -1;
            for (int i = 0; i < cinsList.size(); i++) {
                if (cinsList.get(i) != null &&
                        cinsList.get(i).getAnyTariffObject() != null &&
                        cinsList.get(i).getAnyTariffObject() instanceof TariffCin) {

                    //instance of tariff cin, now find the offering id
                    if (Tools.hasValue(((TariffCin) cinsList.get(i).getAnyTariffObject()).getHeader().getSubscribable()) &&
                            (((TariffCin) cinsList.get(i).getAnyTariffObject()).getHeader().getSubscribable().equalsIgnoreCase(ALREADY_SUBSCRIBED)
                                    || ((TariffCin) cinsList.get(i).getAnyTariffObject()).getHeader().getSubscribable().equalsIgnoreCase(RENEWABLE))) {
                        matchedIndex = i;
                        break;
                    }

                } else if (cinsList.get(i).getAnyTariffObject() != null &&
                        cinsList.get(i).getAnyTariffObject() instanceof TariffCinNew) {

                    //instance of tariff cin new, now find the offering id
                    if (Tools.hasValue(((TariffCinNew) cinsList.get(i).getAnyTariffObject()).getHeader().getSubscribable()) &&
                            (((TariffCinNew) cinsList.get(i).getAnyTariffObject()).getHeader().getSubscribable().equalsIgnoreCase(ALREADY_SUBSCRIBED)
                                    || ((TariffCinNew) cinsList.get(i).getAnyTariffObject()).getHeader().getSubscribable().equalsIgnoreCase(RENEWABLE))) {
                        matchedIndex = i;
                        break;
                    }

                }


            }
            if (matchedIndex != -1) {

                binding.myviewpager1.setAdapter(new AdapterTariffsCINNew(getActivity(),
                        getContext(), cinsList));
                binding.myviewpager1.scrollToPosition(matchedIndex);

            }
        } else {
            // From Notifications
            int matchedIndex = -1;
            for (int i = 0; i < cinsList.size(); i++) {
                if (cinsList.get(i) != null &&
                        cinsList.get(i).getAnyTariffObject() != null &&
                        cinsList.get(i).getAnyTariffObject() instanceof TariffCin) {

                    if (Tools.hasValue(((TariffCin) cinsList.get(i).getAnyTariffObject()).getHeader().getOfferingId()) &&
                            ((TariffCin) cinsList.get(i).getAnyTariffObject()).getHeader().getOfferingId().equalsIgnoreCase(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
                        matchedIndex = i;
                        break;

                    }
                } else if (cinsList.get(i) != null &&
                        cinsList.get(i).getAnyTariffObject() != null &&
                        cinsList.get(i).getAnyTariffObject() instanceof TariffCinNew) {

                    if (Tools.hasValue(((TariffCinNew) cinsList.get(i).getAnyTariffObject()).getHeader().getOfferingId()) &&
                            ((TariffCinNew) cinsList.get(i).getAnyTariffObject()).getHeader().getOfferingId().equalsIgnoreCase(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
                        matchedIndex = i;
                        break;

                    }
                }
            }
            if (matchedIndex != -1) {

                binding.myviewpager1.setAdapter(new AdapterTariffsCINNew(getActivity(),
                        getContext(), cinsList));

                binding.myviewpager1.scrollToPosition(matchedIndex);

            }
        }
    }

    private void goToSubscribeTariffPrepaidKlass(MainTariffs mainTariffs) {
        if (mainTariffs == null || mainTariffs.getTariffPrepaid() == null || mainTariffs.getTariffPrepaid().getTariffsKlasses() == null || mainTariffs.getTariffPrepaid().getTariffsKlasses().size() < 1)
            return;
        ArrayList<TariffKlass> klassList = mainTariffs.getTariffPrepaid().getTariffsKlasses();

        if (!Tools.hasValue(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
            // From Dashboard
            int matchedIndex = -1;
            for (int i = 0; i < klassList.size(); i++) {
                if (klassList.get(i) != null && klassList.get(i).getHeader() != null &&
                        Tools.hasValue(klassList.get(i).getHeader().getSubscribable()) && (
                        klassList.get(i).getHeader().getSubscribable().equalsIgnoreCase(ALREADY_SUBSCRIBED)
                                || klassList.get(i).getHeader().getSubscribable().equalsIgnoreCase(RENEWABLE))) {
                    matchedIndex = i;
                    break;
                }
            }
            if (matchedIndex != -1) {
                binding.myviewpager.setAdapter(new AdapterTraiffsKlass(getActivity(), mainTariffs
                        .getTariffPrepaid().getTariffsKlasses(), !isKlassPayG, isKlassPayG, false));

                binding.myviewpager.scrollToPosition(matchedIndex);

            }
        } else {
            // From Notifications
            int matchedIndex = -1;
            for (int i = 0; i < klassList.size(); i++) {
                if (klassList.get(i) != null && klassList.get(i).getHeader() != null &&
                        Tools.hasValue(klassList.get(i).getHeader().getOfferingId()) &&
                        klassList.get(i).getHeader().getOfferingId().equalsIgnoreCase(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
                    matchedIndex = i;
                    break;
                }
            }
            if (matchedIndex != -1) {

                binding.myviewpager.setAdapter(new AdapterTraiffsKlass(getActivity(), mainTariffs
                        .getTariffPrepaid().getTariffsKlasses(), false, false, false));

                binding.myviewpager.scrollToPosition(matchedIndex);

            }
        }

    }

    private void goToSubscribeTariffPostpaidIndividual(MainTariffs mainTariffs) {
        if (mainTariffs == null || mainTariffs.getTariffPostpaid() == null || mainTariffs.getTariffPostpaid().getIndividual() == null || mainTariffs.getTariffPostpaid().getIndividual().size() < 1)
            return;
        ArrayList<TariffIndividual> individualList = mainTariffs.getTariffPostpaid().getIndividual();

        if (!Tools.hasValue(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
            // From Dashboard
            int matchedIndex = -1;
            for (int i = 0; i < individualList.size(); i++) {
                if (individualList.get(i) != null && individualList.get(i).getHeader() != null &&
                        Tools.hasValue(individualList.get(i).getHeader().getSubscribable()) && (
                        individualList.get(i).getHeader().getSubscribable().equalsIgnoreCase(ALREADY_SUBSCRIBED)
                                || individualList.get(i).getHeader().getSubscribable().equalsIgnoreCase(RENEWABLE))) {
                    matchedIndex = i;
                    break;
                }
            }
            if (matchedIndex != -1) {

                binding.myviewpager.setAdapter(new AdapterIndividualNew(getActivity(),
                        mainTariffs.getTariffPostpaid().getIndividual(), false, false, false));

                binding.myviewpager.scrollToPosition(matchedIndex);

            }
        } else {
            // From Notifications
            int matchedIndex = -1;
            for (int i = 0; i < individualList.size(); i++) {
                if (individualList.get(i) != null && individualList.get(i).getHeader() != null &&
                        Tools.hasValue(individualList.get(i).getHeader().getOfferingId()) &&
                        individualList.get(i).getHeader().getOfferingId().equalsIgnoreCase(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
                    matchedIndex = i;
                    break;
                }
            }
            if (matchedIndex != -1) {

                binding.myviewpager.setAdapter(new AdapterIndividualNew(getActivity(),
                        mainTariffs.getTariffPostpaid().getIndividual(), false, false, false));

                binding.myviewpager.scrollToPosition(matchedIndex);

            }
        }

    }

    private void goToSubscribeTariffPostpaidCorporate(MainTariffs mainTariffs) {
        if (mainTariffs == null || mainTariffs.getTariffPostpaid() == null || mainTariffs.getTariffPostpaid().getCorporate() == null || mainTariffs.getTariffPostpaid().getCorporate().size() < 1)
            return;
        ArrayList<TariffCorporate> corporateList = mainTariffs.getTariffPostpaid().getCorporate();

        if (!Tools.hasValue(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
            // From Dashboard
            int matchedIndex = -1;
            for (int i = 0; i < corporateList.size(); i++) {
                if (corporateList.get(i) != null && corporateList.get(i).getHeader() != null &&
                        Tools.hasValue(corporateList.get(i).getHeader().getSubscribable()) && (
                        corporateList.get(i).getHeader().getSubscribable().equalsIgnoreCase(ALREADY_SUBSCRIBED)
                                || corporateList.get(i).getHeader().getSubscribable().equalsIgnoreCase(RENEWABLE))) {
                    matchedIndex = i;
                    break;
                }
            }
            if (matchedIndex != -1) {

                binding.myviewpager.setAdapter(new AdapterTraiffsCorporate(getActivity(),
                        mainTariffs.getTariffPostpaid().getCorporate(), false, false, false));

                binding.myviewpager.scrollToPosition(matchedIndex);

            }
        } else {
            // From Notifications
            int matchedIndex = -1;
            for (int i = 0; i < corporateList.size(); i++) {
                if (corporateList.get(i) != null && corporateList.get(i).getHeader() != null &&
                        Tools.hasValue(corporateList.get(i).getHeader().getOfferingId()) &&
                        corporateList.get(i).getHeader().getOfferingId().equalsIgnoreCase(RootValues.getInstance().getOfferingIdRedirectFromNotifications())) {
                    matchedIndex = i;
                    break;
                }
            }
            if (matchedIndex != -1) {

                binding.myviewpager.setAdapter(new AdapterTraiffsCorporate(getActivity(),
                        mainTariffs.getTariffPostpaid().getCorporate(), false, false, false));
                binding.myviewpager.scrollToPosition(matchedIndex);

            }

        }

    }


    // Tariff Detail Unitfied Layout initialization
    public static TariffDetailLayout loadDetailUI_Ids(View itemView) {

        TariffDetailLayout detailLayout = new TariffDetailLayout();

        // Price
        detailLayout.setTariff_detail_parice_layout((LinearLayout) itemView.findViewById(R.id.tariff_detail_parice_layout));

        // roundingSection
        detailLayout.setRoundingSection((RelativeLayout) itemView.findViewById(R.id.roundingSection));
        detailLayout.setRoundingTitleLayout((RelativeLayout) itemView.findViewById(R.id.roundingTitleLayout));
        detailLayout.setRoundingTitleIcon((ImageView) itemView.findViewById(R.id.roundingTitleIcon));
        detailLayout.setRoundingTitleText((BakcellTextViewNormal) itemView.findViewById(R.id.roundingTitleText));
        detailLayout.setRoundingTitleValue((BakcellTextViewNormal) itemView.findViewById(R.id.roundingTitleValue));
        detailLayout.setDetailRoundingDescription((BakcellTextViewNormal) itemView.findViewById(R.id.detailRoundingDescription));
        detailLayout.setOfferDetailroundingAttributeLayout((LinearLayout) itemView.findViewById(R.id.offerDetailroundingAttributeLayout));
        detailLayout.setRounding_below_line(itemView.findViewById(R.id.rounding_below_line));

        // TextWithTitleSection
        detailLayout.setTextWithTitleSection((RelativeLayout) itemView.findViewById(R.id.textWithTitleSection));
        detailLayout.setText_title((BakcellTextViewNormal) itemView.findViewById(R.id.text_title));
        detailLayout.setDecription_text((BakcellTextViewNormal) itemView.findViewById(R.id.decription_text));
        detailLayout.setTextWithTitle_below_line(itemView.findViewById(R.id.textWithTitle_below_line));

        //textWithOutTitleSection
        detailLayout.setTextWithOutTitleSection((RelativeLayout) itemView.findViewById(R.id.textWithOutTitleSection));
        detailLayout.setTextWithOutTitle_text((BakcellTextViewNormal) itemView.findViewById(R.id.textWithOutTitle_text));
        detailLayout.setTextWithOutTitle_below_line(itemView.findViewById(R.id.textWithOutTitle_below_line));

        // textWithPointsSections
        detailLayout.setTextWithPointsSections((RelativeLayout) itemView.findViewById(R.id.textWithPointsSections));
        detailLayout.setTextWithPointsLayout((LinearLayout) itemView.findViewById(R.id.textWithPointsLayout));
        detailLayout.setTextWithPoints_below_line(itemView.findViewById(R.id.textWithPoints_below_line));

        //titleSubTitleValueDescSection
        detailLayout.setTitleSubTitleValueDescSection((RelativeLayout) itemView.findViewById(R.id.titleSubTitleValueDescSection));
        detailLayout.setTitleSubTitleValueDescTitleLayout((RelativeLayout) itemView.findViewById(R.id.titleSubTitleValueDescTitleLayout));
        detailLayout.setTitleSubTitleValueDescTitle((BakcellTextViewNormal) itemView.findViewById(R.id.titleSubTitleValueDescTitle));
        detailLayout.setTitleSubTitleValueDescAttributeLayout((LinearLayout) itemView.findViewById(R.id.titleSubTitleValueDescAttributeLayout));
        detailLayout.setTextSubTitleDesc_below_line(itemView.findViewById(R.id.textSubTitleDesc_below_line));

        //dateSection
        detailLayout.setDateSection((RelativeLayout) itemView.findViewById(R.id.dateSection));
        detailLayout.setDateLayout((RelativeLayout) itemView.findViewById(R.id.dateLayout));
        detailLayout.setDateDescription((BakcellTextViewNormal) itemView.findViewById(R.id.dateDescription));
        detailLayout.setDateFromLabel((BakcellTextViewNormal) itemView.findViewById(R.id.dateFromLabel));
        detailLayout.setDateFromValue((BakcellTextViewNormal) itemView.findViewById(R.id.dateFromValue));
        detailLayout.setDateToLabel((BakcellTextViewNormal) itemView.findViewById(R.id.dateToLabel));
        detailLayout.setDateToValue((BakcellTextViewNormal) itemView.findViewById(R.id.dateToValue));
        detailLayout.setDate_below_line(itemView.findViewById(R.id.date_below_line));

        //timeSection
        detailLayout.setTimeSection((RelativeLayout) itemView.findViewById(R.id.timeSection));
        detailLayout.setTimeLayout((RelativeLayout) itemView.findViewById(R.id.timeLayout));
        detailLayout.setTimeDescription((BakcellTextViewNormal) itemView.findViewById(R.id.timeDescription));
        detailLayout.setTimefromLabel((BakcellTextViewNormal) itemView.findViewById(R.id.timefromLabel));
        detailLayout.setTimefromValue((BakcellTextViewNormal) itemView.findViewById(R.id.timefromValue));
        detailLayout.setTimetoLabel((BakcellTextViewNormal) itemView.findViewById(R.id.timetoLabel));
        detailLayout.setTimetoValue((BakcellTextViewNormal) itemView.findViewById(R.id.timetoValue));
        detailLayout.setTime_below_line(itemView.findViewById(R.id.time_below_line));

        //roamingSection
        detailLayout.setRoamingSection((RelativeLayout) itemView.findViewById(R.id.roamingSection));
        detailLayout.setRoamingDescriptionAbove((BakcellTextViewNormal) itemView.findViewById(R.id.roamingDescriptionAbove));
        detailLayout.setRoamingDescriptionBelow((BakcellTextViewNormal) itemView.findViewById(R.id.roamingDescriptionBelow));
        detailLayout.setRoamingCountryLayout((LinearLayout) itemView.findViewById(R.id.roamingCountryLayout));
        detailLayout.setRoaming_below_line(itemView.findViewById(R.id.roaming_below_line));

        // freeResourceValiditySection
        detailLayout.setFreeResourceValiditySection((RelativeLayout) itemView.findViewById(R.id.freeResourceValiditySection));
        detailLayout.setFreeResourceTitleLayout((RelativeLayout) itemView.findViewById(R.id.freeResourceTitleLayout));
        detailLayout.setFreeResourceOnnetLayout((RelativeLayout) itemView.findViewById(R.id.freeResourceOnnetLayout));
        detailLayout.setFreeResourceTitle((BakcellTextViewNormal) itemView.findViewById(R.id.freeResourceTitle));
        detailLayout.setFreeResourceValue((BakcellTextViewNormal) itemView.findViewById(R.id.freeResourceValue));
        detailLayout.setOnnetTitle((BakcellTextViewNormal) itemView.findViewById(R.id.onnetTitle));
        detailLayout.setOnnetValue((BakcellTextViewNormal) itemView.findViewById(R.id.onnetValue));
        detailLayout.setFreeResourceDescription((BakcellTextViewNormal) itemView.findViewById(R.id.freeResourceDescription));


        return detailLayout;
    }

    // Tariff Detail Unitfied Layout Showing data
    public static void populateDetailSectionContents(TariffDetails offerDetail, TariffDetailLayout detailLayout, Context context) {
        // priceSection
        if (offerDetail.getPrice() != null && offerDetail.getPrice().size() > 0) {
            detailLayout.getTariff_detail_parice_layout().setVisibility(View.VISIBLE);
            detailLayout.getTariff_detail_parice_layout().removeAllViews();
            for (int i = 0; i < offerDetail.getPrice().size(); i++) {
                ViewItemTariffsDetailPrice viewItemTariffsDetailPrice = populatePriceContents(offerDetail.getPrice().get(i), context);
                if (viewItemTariffsDetailPrice != null) {
                    detailLayout.getTariff_detail_parice_layout().addView(viewItemTariffsDetailPrice);
                }
            }
        } else {
            detailLayout.getTariff_detail_parice_layout().setVisibility(View.GONE);
        }

        // roundingSection
        if (offerDetail.getRounding() != null) {
            detailLayout.getRoundingSection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getRounding().getTitle())) {
                detailLayout.getRoundingTitleText().setText(offerDetail.getRounding().getTitle());
                detailLayout.getRoundingTitleText().setSelected(true);
            } else {
                detailLayout.getRoundingTitleText().setText("");
            }
            if (Tools.hasValue(offerDetail.getRounding().getValue())) {
                detailLayout.getRoundingTitleValue().setText(offerDetail.getRounding().getValue());
            } else {
                detailLayout.getRoundingTitleValue().setText("");
            }
            if (Tools.hasValue(offerDetail.getRounding().getDescription())) {
                detailLayout.getDetailRoundingDescription().setText(Html.fromHtml(offerDetail.getRounding().getDescription()));
            } else {
                detailLayout.getDetailRoundingDescription().setText("");
            }

            if (Tools.hasValue(offerDetail.getRounding().getIconName())) {
                Tools.setTariffsIcons(offerDetail.getRounding().getIconName().trim(), detailLayout.getRoundingTitleIcon(), context);
            }

            // Rounding Attributes
            if (offerDetail.getRounding().getAttributeList() != null && offerDetail.getRounding().getAttributeList().size() > 0) {
                detailLayout.getOfferDetailroundingAttributeLayout().setVisibility(View.VISIBLE);
                detailLayout.getOfferDetailroundingAttributeLayout().removeAllViews();
                ArrayList<DetailsAttributes> roundingAttributeslist = offerDetail.getRounding().getAttributeList();
                for (int i = 0; i < roundingAttributeslist.size(); i++) {
                    ViewItemOfferDetailRoundingAttribute detailRoundingAttribute = new ViewItemOfferDetailRoundingAttribute(context);
                    detailRoundingAttribute.hideAZN();
                    if (Tools.hasValue(roundingAttributeslist.get(i).getTitle())) {
                        detailRoundingAttribute.getPriceSubTitle().setText(roundingAttributeslist.get(i).getTitle());
                    } else {
                        detailRoundingAttribute.getPriceSubTitle().setText("");
                    }
                    if (Tools.hasValue(roundingAttributeslist.get(i).getValue())) {
                        detailRoundingAttribute.getPrceValue().setText(roundingAttributeslist.get(i).getValue());
                    } else {
                        detailRoundingAttribute.getPrceValue().setText("");
                    }
                    detailLayout.getOfferDetailroundingAttributeLayout().addView(detailRoundingAttribute);
                }
            } else {
                detailLayout.getOfferDetailroundingAttributeLayout().setVisibility(View.GONE);
            }
        } else {
            detailLayout.getRoundingSection().setVisibility(View.GONE);
        }

        // TextWithTitleSection
        if (offerDetail.getTextWithTitle() != null) {
            detailLayout.getTextWithTitleSection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getTextWithTitle().getTitle())) {
                detailLayout.getText_title().setVisibility(View.VISIBLE);
                detailLayout.getText_title().setText(offerDetail.getTextWithTitle().getTitle());
            } else {
                detailLayout.getText_title().setVisibility(View.GONE);
            }
            if (Tools.hasValue(offerDetail.getTextWithTitle().getText())) {
                detailLayout.getDecription_text().setVisibility(View.VISIBLE);
                detailLayout.getDecription_text().setText(Html.fromHtml(offerDetail.getTextWithTitle().getText()));
            } else {
                detailLayout.getDecription_text().setVisibility(View.GONE);
            }
        } else {
            detailLayout.getTextWithTitleSection().setVisibility(View.GONE);
        }

        //textWithOutTitleSection
        if (offerDetail.getTextWithOutTitle() != null) {
            detailLayout.getTextWithOutTitleSection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getTextWithOutTitle().getDescription())) {
                detailLayout.getTextWithOutTitle_text().setText(Html.fromHtml(offerDetail.getTextWithOutTitle().getDescription()));
            } else {
                detailLayout.getTextWithOutTitle_text().setText("");
            }
        } else {
            detailLayout.getTextWithOutTitleSection().setVisibility(View.GONE);
        }

        // textWithPointsSections
        if (offerDetail.getTextWithPoints() != null && offerDetail.getTextWithPoints().getPointsList() != null && offerDetail.getTextWithPoints().getPointsList().size() > 0) {
            detailLayout.getTextWithPointsSections().setVisibility(View.VISIBLE);
            detailLayout.getTextWithPointsLayout().removeAllViews();
            ArrayList<String> pointsList = offerDetail.getTextWithPoints().getPointsList();
            for (int i = 0; i < pointsList.size(); i++) {
                ViewItemOfferDetailBulletsItem detailBulletsItem = new ViewItemOfferDetailBulletsItem(context);
                if (Tools.hasValue(pointsList.get(i))) {
                    detailBulletsItem.getBulletText().setText(Html.fromHtml(pointsList.get(i)));
                    detailLayout.getTextWithPointsLayout().addView(detailBulletsItem);
                } else {
                    detailBulletsItem.getBulletText().setText("");
                }
            }
        } else {
            detailLayout.getTextWithPointsSections().setVisibility(View.GONE);
        }


        //titleSubTitleValueDescSection
        if (offerDetail.getTitleSubTitleValueAndDesc() != null) {
            detailLayout.getTitleSubTitleValueDescSection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getTitleSubTitleValueAndDesc().getTitle())) {
                detailLayout.getTitleSubTitleValueDescTitle().setVisibility(View.VISIBLE);
                detailLayout.getTitleSubTitleValueDescTitle().setText(offerDetail.getTitleSubTitleValueAndDesc().getTitle());
                detailLayout.getTitleSubTitleValueDescTitle().setSelected(true);
            } else {
                detailLayout.getTitleSubTitleValueDescTitle().setVisibility(View.GONE);
            }
            // Sub Title Attributes
            if (offerDetail.getTitleSubTitleValueAndDesc().getAttributeList() != null && offerDetail.getTitleSubTitleValueAndDesc().getAttributeList().size() > 0) {
                detailLayout.getTitleSubTitleValueDescAttributeLayout().setVisibility(View.VISIBLE);
                detailLayout.getTitleSubTitleValueDescAttributeLayout().removeAllViews();
                ArrayList<HeaderAttributes> titleSubTitleList = offerDetail.getTitleSubTitleValueAndDesc().getAttributeList();
                for (int i = 0; i < titleSubTitleList.size(); i++) {
                    ViewItemOfferDetailTitleWithSubTitleDescAttribute subTitleDescAttribute = new ViewItemOfferDetailTitleWithSubTitleDescAttribute(context);
                    if (Tools.hasValue(titleSubTitleList.get(i).getTitle())) {
                        subTitleDescAttribute.getSubTitle().setText(titleSubTitleList.get(i).getTitle());
                    } else {
                        subTitleDescAttribute.getSubTitle().setText("");
                    }


                    //Attribute Value + Unit
                    String attributeValue = "";
                    if (Tools.hasValue(titleSubTitleList.get(i).getValue())) {
                        attributeValue = titleSubTitleList.get(i).getValue();
                    }
                    if (Tools.hasValue(titleSubTitleList.get(i).getUnit())) {
                        if (titleSubTitleList.get(i).getUnit()
                                .equalsIgnoreCase(Constants.UserCurrentKeyword.CURRENCY_AZN)) {
                            subTitleDescAttribute.getPriceIcon().setVisibility(View.VISIBLE);
                        } else {
                            subTitleDescAttribute.getPriceIcon().setVisibility(View.GONE);
                            attributeValue += " " + titleSubTitleList.get(i).getUnit();
                        }
                    } else {
                        subTitleDescAttribute.getPriceIcon().setVisibility(View.GONE);
                    }
                    subTitleDescAttribute.getValue().setText(attributeValue);


                    if (Tools.hasValue(titleSubTitleList.get(i).getDescription())) {
                        subTitleDescAttribute.getDesc_txt().setVisibility(View.VISIBLE);
                        subTitleDescAttribute.getDesc_txt().setText(Html.fromHtml(titleSubTitleList.get(i).getDescription()));
                    } else {
                        subTitleDescAttribute.getDesc_txt().setVisibility(View.GONE);
                    }
                    detailLayout.getTitleSubTitleValueDescAttributeLayout().addView(subTitleDescAttribute);
                }
            } else {
                detailLayout.getTitleSubTitleValueDescAttributeLayout().setVisibility(View.GONE);
            }
        } else {
            detailLayout.getTitleSubTitleValueDescSection().setVisibility(View.GONE);
        }

        //dateSection
        if (offerDetail.getDate() != null) {
            detailLayout.getDateSection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getDate().getDescription())) {
                detailLayout.getDateDescription().setText(Html.fromHtml(offerDetail.getDate().getDescription()));
            } else {
                detailLayout.getDateDescription().setText("");
            }
            if (Tools.hasValue(offerDetail.getDate().getFromTitle())) {
                detailLayout.getDateFromLabel().setText(offerDetail.getDate().getFromTitle());
            } else {
                detailLayout.getDateFromLabel().setText("");
            }
            if (Tools.hasValue(offerDetail.getDate().getFromValue())) {
                try {
                    detailLayout.getDateFromValue().setText(
                            Tools.getDateAccordingToClientSaid(
                                    offerDetail.getDate().getFromValue()));
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            } else {
                detailLayout.getDateFromValue().setText("");
            }
            if (Tools.hasValue(offerDetail.getDate().getToTitle())) {
                detailLayout.getDateToLabel().setText(offerDetail.getDate().getToTitle());
            } else {
                detailLayout.getDateToLabel().setText("");
            }
            if (Tools.hasValue(offerDetail.getDate().getToValue())) {
                try {
                    detailLayout.getDateToValue().setText(
                            Tools.getDateAccordingToClientSaid(offerDetail.getDate().getToValue())
                    );
                } catch (ParseException e) {
                    Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
                }
            } else {
                detailLayout.getDateToValue().setText("");
            }
        } else {
            detailLayout.getDateSection().setVisibility(View.GONE);
        }

        //timeSection
        if (offerDetail.getTime() != null) {
            detailLayout.getTimeSection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getTime().getDescription())) {
                detailLayout.getTimeDescription().setText(Html.fromHtml(offerDetail.getTime().getDescription()));
            } else {
                detailLayout.getTimeDescription().setText("");
            }
            if (Tools.hasValue(offerDetail.getTime().getFromTitle())) {
                detailLayout.getTimefromLabel().setText(offerDetail.getTime().getFromTitle());
            } else {
                detailLayout.getTimefromLabel().setText("");
            }
            if (Tools.hasValue(offerDetail.getTime().getFromValue())) {
                detailLayout.getTimefromValue().setText(offerDetail.getTime().getFromValue());
            } else {
                detailLayout.getTimefromValue().setText("");
            }
            if (Tools.hasValue(offerDetail.getTime().getToTitle())) {
                detailLayout.getTimetoLabel().setText(offerDetail.getTime().getToTitle());
            } else {
                detailLayout.getTimetoLabel().setText("");
            }
            if (Tools.hasValue(offerDetail.getTime().getToValue())) {
                detailLayout.getTimetoValue().setText(offerDetail.getTime().getToValue());
            } else {
                detailLayout.getTimetoValue().setText("");
            }
        } else {
            detailLayout.getTimeSection().setVisibility(View.GONE);
        }


        //roamingSection
        if (offerDetail.getRoamingDetails() != null) {
            detailLayout.getRoamingSection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getRoamingDetails().getDescriptionAbove())) {
                detailLayout.getRoamingDescriptionAbove().setVisibility(View.VISIBLE);
                detailLayout.getRoamingDescriptionAbove().setText(Html.fromHtml(offerDetail.getRoamingDetails().getDescriptionAbove()));
            } else {
                detailLayout.getRoamingDescriptionAbove().setVisibility(View.GONE);
            }
            if (Tools.hasValue(offerDetail.getRoamingDetails().getDescriptionBelow())) {
                detailLayout.getRoamingDescriptionBelow().setVisibility(View.VISIBLE);
                detailLayout.getRoamingDescriptionBelow().setText(Html.fromHtml(offerDetail.getRoamingDetails().getDescriptionBelow()));
            } else {
                detailLayout.getRoamingDescriptionBelow().setVisibility(View.GONE);
            }

            // RoamingsOffer Coutries list
            if (offerDetail.getRoamingDetails().getRoamingDetailsCountriesList() != null && offerDetail.getRoamingDetails().getRoamingDetailsCountriesList().size() > 0) {
                detailLayout.getRoamingCountryLayout().setVisibility(View.VISIBLE);
                detailLayout.getRoamingCountryLayout().removeAllViews();
                ArrayList<RoamingDetailsCountries> roamingCountriesList = offerDetail.getRoamingDetails().getRoamingDetailsCountriesList();
                for (int i = 0; i < roamingCountriesList.size(); i++) {
                    ViewItemOfferDetailRoamingCountries detailRoamingCountries = new ViewItemOfferDetailRoamingCountries(context);
                    if (Tools.hasValue(roamingCountriesList.get(i).getCountryName())) {
                        detailRoamingCountries.getCountryName().setText(roamingCountriesList.get(i).getCountryName());
                    } else {
                        detailRoamingCountries.getCountryName().setText("");
                    }
                    if (Tools.hasValue(roamingCountriesList.get(i).getFlag())) {
                        detailRoamingCountries.getCountryFlag().setImageDrawable(Tools.getImageFromAssests(context, roamingCountriesList.get(i).getFlag()));
                    } else {
                        detailRoamingCountries.getCountryFlag().setImageResource(R.drawable.dummy_flag);
                    }
                    // Country Operators list
                    if (roamingCountriesList.get(i).getOperatorList() != null && roamingCountriesList.get(i).getOperatorList().size() > 0) {
                        detailRoamingCountries.getOperatorLayout().setVisibility(View.VISIBLE);
                        detailRoamingCountries.getOperatorLayout().removeAllViews();
                        ArrayList<String> countryOperatorsList = roamingCountriesList.get(i).getOperatorList();
                        for (int j = 0; j < countryOperatorsList.size(); j++) {
                            if (Tools.hasValue(countryOperatorsList.get(j))) {
                                BakcellTextViewNormal textViewOperator = new BakcellTextViewNormal(context);
                                textViewOperator.setText(countryOperatorsList.get(j));
                                textViewOperator.setGravity(Gravity.RIGHT);
                                detailRoamingCountries.getOperatorLayout().addView(textViewOperator);
                            }
                        }
                    } else {
                        detailRoamingCountries.getOperatorLayout().setVisibility(View.GONE);
                    }
                    detailLayout.getRoamingCountryLayout().addView(detailRoamingCountries);
                }
            } else {
                detailLayout.getRoamingCountryLayout().setVisibility(View.GONE);
            }
        } else {
            detailLayout.getRoamingSection().setVisibility(View.GONE);
        }

        // freeResourceValiditySection
        if (offerDetail.getFreeResourceValidity() != null) {
            detailLayout.getFreeResourceValiditySection().setVisibility(View.VISIBLE);
            if (Tools.hasValue(offerDetail.getFreeResourceValidity().getTitle())) {
                detailLayout.getFreeResourceTitle().setText(offerDetail.getFreeResourceValidity().getTitle());
            } else {
                detailLayout.getFreeResourceTitle().setText("");
            }
            if (Tools.hasValue(offerDetail.getFreeResourceValidity().getTitleValue())) {
                detailLayout.getFreeResourceValue().setText(offerDetail.getFreeResourceValidity().getTitleValue());
            } else {
                detailLayout.getFreeResourceValue().setText("");
            }
            if (Tools.hasValue(offerDetail.getFreeResourceValidity().getSubTitle())) {
                detailLayout.getOnnetTitle().setText(offerDetail.getFreeResourceValidity().getSubTitle());
            } else {
                detailLayout.getOnnetTitle().setText("");
            }
            if (Tools.hasValue(offerDetail.getFreeResourceValidity().getSubTitleValue())) {
                detailLayout.getOnnetValue().setText(offerDetail.getFreeResourceValidity().getSubTitleValue());
            } else {
                detailLayout.getOnnetValue().setText("");
            }
            if (Tools.hasValue(offerDetail.getFreeResourceValidity().getDescription())) {
                detailLayout.getFreeResourceDescription().setVisibility(View.VISIBLE);
                detailLayout.getFreeResourceDescription().setText(Html.fromHtml(offerDetail.getFreeResourceValidity().getDescription()));
            } else {
                detailLayout.getFreeResourceDescription().setVisibility(View.GONE);
            }
        } else {
            detailLayout.getFreeResourceValiditySection().setVisibility(View.GONE);
        }
    }


    private static ViewItemTariffsDetailPrice populatePriceContents(Price price, Context context) {
        if (price == null || context == null) return null;

        ViewItemTariffsDetailPrice viewItemTariffsDetailPrice = new ViewItemTariffsDetailPrice(context);

        if (Tools.hasValue(price.getTitle())) {
            viewItemTariffsDetailPrice.getDetailPriceTitle().setText(price.getTitle());
            viewItemTariffsDetailPrice.getDetailPriceTitle().setSelected(true);
        } else {
            viewItemTariffsDetailPrice.getDetailPriceTitle().setText("");
        }
        if (Tools.hasValue(price.getValue())) {
            viewItemTariffsDetailPrice.getDetailPriceValue().setText(price.getValue());
        } else {
            viewItemTariffsDetailPrice.getDetailPriceValue().setText("");
        }
        if (Tools.hasValue(price.getDescription())) {
            viewItemTariffsDetailPrice.getDetailPriceDescription().setText(Html.fromHtml(price.getDescription()));
        } else {
            viewItemTariffsDetailPrice.getDetailPriceDescription().setVisibility(View.GONE);
        }


        if (Tools.hasValue(price.getIconName())) {
            Tools.setTariffsIcons(price.getIconName().trim(), viewItemTariffsDetailPrice.getPriceTitleIcon(), context);
        }
        // Price Attributes
        if (price.getAttributeList() != null && price.getAttributeList().size() > 0) {
            viewItemTariffsDetailPrice.getOfferDetailAttributeLayout().setVisibility(View.VISIBLE);
            viewItemTariffsDetailPrice.getOfferDetailAttributeLayout().removeAllViews();
            ArrayList<DetailsAttributes> detailPriceAttributesList = price.getAttributeList();
            for (int i = 0; i < detailPriceAttributesList.size(); i++) {
                ViewItemOfferDetailPriceAttribute detailPriceAttribute = new ViewItemOfferDetailPriceAttribute(context);
                if (Tools.hasValue(detailPriceAttributesList.get(i).getTitle())) {
                    detailPriceAttribute.getPriceSubTitle().setText(detailPriceAttributesList.get(i).getTitle());
                } else {
                    detailPriceAttribute.getPriceSubTitle().setText("");
                }
                if (Tools.hasValue(detailPriceAttributesList.get(i).getValue())) {
                    detailPriceAttribute.getPrceValue().setText(detailPriceAttributesList.get(i).getValue());
                    if (Tools.isNumeric(detailPriceAttributesList.get(i).getValue())) {
                        detailPriceAttribute.showAZN();
                    } else {
                        detailPriceAttribute.hideAZN();
                    }
                } else {
                    detailPriceAttribute.hideAZN();
                    detailPriceAttribute.getPrceValue().setText("");
                }
                viewItemTariffsDetailPrice.getOfferDetailAttributeLayout().addView(detailPriceAttribute);
            }
        } else {
            viewItemTariffsDetailPrice.getOfferDetailAttributeLayout().setVisibility(View.GONE);
        }
        return viewItemTariffsDetailPrice;
    }


    private void setNoDataFound(int visibility) {
        binding.noDataFoundLayout.setVisibility(visibility);
    }
}