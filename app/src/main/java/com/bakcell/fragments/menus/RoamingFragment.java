package com.bakcell.fragments.menus;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.adapters.PagerAdapterOffers;
import com.bakcell.databinding.FragmentRoamingBinding;
import com.bakcell.fragments.pager.OffersPaggerFragment;
import com.bakcell.fragments.pager.RoamingPaggerFragment;

import java.util.ArrayList;

/**
 * Created by Freeware Sys on 18-May-17.
 */

public class RoamingFragment extends Fragment {

    FragmentRoamingBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_roaming, container, false);

        initUITabsAndPagger();

        iniPaggerView();

        binding.viewPager.setCurrentItem(6);

        return binding.getRoot();
    }


    private void initUITabsAndPagger() {

        // Adding Tabs here
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_call));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_internet));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_sms));
        //TODO Temporary Commented by client
//        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_campaign));
//        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_tm));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_hybrid));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.supplementary_offer_tab_title_roaming));


        iniPaggerView();

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 70, 0);
            tab.requestLayout();
        }

//        tabLayout.setupWithViewPager(viewPager);// Problem with this need to check in detail

//        if (SupplementaryOffersFragment.OFFERS_LIST_ORIENTATION == LinearLayoutManager.VERTICAL) {
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
//        }else {
//
//            viewPager.setOnTouchListener(new View.OnTouchListener() {
//
//                public boolean onTouch(View arg0, MotionEvent arg1) {
//                    return true;
//                }
//            });
//        }

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void iniPaggerView() {
        ArrayList<Fragment> paggerFragmentsList = new ArrayList<>();
        if (binding.tabLayout != null && binding.tabLayout.getTabCount() > 0) {
            for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
                if (i == 6) {
                    paggerFragmentsList.add(new RoamingPaggerFragment());
                } else {
                    paggerFragmentsList.add(new OffersPaggerFragment());
                }

            }
        }
        binding.viewPager.setAdapter(new PagerAdapterOffers(getChildFragmentManager(), paggerFragmentsList));
    }
}
