package com.bakcell.fragments.menus;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.AdapterNotificationsItem;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentNotificationsBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.models.DataManager;
import com.bakcell.models.notifications.Notification;
import com.bakcell.models.notifications.NotificationMain;
import com.bakcell.webservices.RequestGetNotificationService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Muhammad Atif Arif on 10/6/2017.
 */

public class NotificationsFragment extends Fragment {

    FragmentNotificationsBinding binding;

    private AdapterNotificationsItem adapterNotificationsItem;
    LinearLayoutManager layoutManager;
    private NotificationMain notifications;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false);

        initUIContents();


//        String notificationsData = Tools.readTextFromFile("NotificationData.txt", getActivity());
//        notifications = new Gson().fromJson(notificationsData, NotificationMain.class);
//
//        if (notifications != null) {
//            loadNotificationList(notifications.getNotificationList());
//        }

        AppEventLogs.applyAppEvent(AppEventLogValues.NotificationEvents.NOTIFICATIONS_SCREEN,
                AppEventLogValues.NotificationEvents.NOTIFICATIONS_SCREEN,
                AppEventLogValues.NotificationEvents.NOTIFICATIONS_SCREEN);

        requestGetNotification();

        return binding.getRoot();
    }

    private void initUIContents() {

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setAutoMeasureEnabled(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.mRecylerview.setLayoutManager(layoutManager);
        binding.mRecylerview.setHasFixedSize(true);
       /* ArrayList<Notification> notificationArrayList = new ArrayList<>();
        adapterNotificationsItem = new AdapterNotificationsItem(getActivity(), notificationArrayList);
        binding.mRecylerview.setAdapter(adapterNotificationsItem);*/

    }

    private void requestGetNotification() {

        RequestGetNotificationService.newInstance(getActivity(), ServiceIDs.REQUEST_NOTIFICATION).execute(DataManager.getInstance().getCurrentUser(),
                "", "", new EaseCallbacks<String>() {
                    @Override
                    public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        String result = response.getData();
                        if (result == null) return;

                        notifications = new Gson().fromJson(result, NotificationMain.class);

                        if (notifications != null) {
                            loadNotificationList(notifications.getNotificationList());
                        }


                    }//end of onSuccess

                    @Override
                    public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                        // Check if user logout by server
                        if (getActivity() != null && !getActivity().isFinishing() &&
                                response != null && response.getResultCode() != null &&
                                response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                            Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                            HomeActivity.logoutUser(getActivity());
                            return;
                        }

                        if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                            BakcellPopUpDialog.showMessageDialog(getActivity(),
                                    getString(R.string.bakcell_error_title), response.getDescription());
                        }
                    }//end of onFailure

                    @Override
                    public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            BakcellPopUpDialog.ApiFailureMessage(getActivity());
                        }
                    }//end of onError
                });

    }

    private void loadNotificationList(ArrayList<Notification> notificationList) {

        if (notificationList != null && notificationList.size() > 0) {
            binding.notificationListView.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            adapterNotificationsItem = new AdapterNotificationsItem(getActivity(), notificationList);
            binding.mRecylerview.setAdapter(adapterNotificationsItem);
        } else {
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            binding.notificationListView.setVisibility(View.GONE);
        }
    }


    public void onSearch(String s) {
        adapterNotificationsItem.getFilter().filter(s);
    }

    public void onSearchClosed() {
        if (notifications.getNotificationList() != null)
            adapterNotificationsItem.updateAdapter(notifications.getNotificationList());
    }
}
