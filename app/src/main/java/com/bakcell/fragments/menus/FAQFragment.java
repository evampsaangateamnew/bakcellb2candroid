package com.bakcell.fragments.menus;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.ExpandableAdapterFAQ;
import com.bakcell.adapters.SpinnerAdapterFAQ;
import com.bakcell.appanalytics.AppEventLogValues;
import com.bakcell.appanalytics.AppEventLogs;
import com.bakcell.databinding.FragmentFaqBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.globals.RootValues;
import com.bakcell.models.DataManager;
import com.bakcell.models.faq.Data;
import com.bakcell.models.faq.FaqCategory;
import com.bakcell.models.faq.Faqs;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.FaqsService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

/**
 * Created by Zeeshan Shabbir on 6/20/2017.
 */

public class FAQFragment extends Fragment {

    FragmentFaqBinding binding;

    SpinnerAdapterFAQ spinnerAdapterFAQ;
    ExpandableAdapterFAQ expandableAdapterFAQ;

    private ArrayList<FaqCategory> faqCategoryArrayList;
    private ArrayList<Faqs> faqsArrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_faq, container, false);

        initUiContents();

        initUIListener();

        reguestForGetFaqsList();

        AppEventLogs.applyAppEvent(AppEventLogValues.FAQEvents.FAQ_SCREEN,
                AppEventLogValues.FAQEvents.FAQ_SCREEN,
                AppEventLogValues.FAQEvents.FAQ_SCREEN);

        return binding.getRoot();
    }

    private void reguestForGetFaqsList() {


        FaqsService.newInstance(getActivity(), ServiceIDs.FAQS).execute(DataManager.getInstance().getCurrentUser(), new EaseCallbacks<String>() {
            @Override
            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                RootValues.getInstance().setFaqsApiCall(false);

                if (response == null) return;

                Data data = new Gson().fromJson(response.getData(), Data.class);
                if (data != null) {
                    prepareListData(data);
                }
            }

            @Override
            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {
                // Check if user logout by server
                if (getActivity() != null && !getActivity().isFinishing() &&
                        response != null && response.getResultCode() != null &&
                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                    HomeActivity.logoutUser(getActivity());
                    return;
                }

                // Load Cashed Data
                if (getActivity() != null && !getActivity().isFinishing()) {
                    String cacheKey = MultiAccountsHandler.CacheKeys.getFAQsCacheKey(getActivity());
                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                    if (Tools.hasValue(cacheResponse)) {
                        try {
                            JSONObject jsonObject = new JSONObject(cacheResponse);
                            String cacheData = jsonObject.getJSONObject("data").toString();
                            Data data = new Gson().fromJson(cacheData, Data.class);
                            if (data != null) {
                                prepareListData(data);
                            }
                        } catch (JSONException ex) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                        }
                    }
                }

                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                            getString(R.string.bakcell_error_title), response.getDescription());
                }
            }

            @Override
            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    String cacheKey = MultiAccountsHandler.CacheKeys.getFAQsCacheKey(getActivity());
                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                    if (Tools.hasValue(cacheResponse)) {
                        try {
                            JSONObject jsonObject = new JSONObject(cacheResponse);
                            String cacheData = jsonObject.getJSONObject("data").toString();
                            Data data = new Gson().fromJson(cacheData, Data.class);
                            if (data != null) {
                                prepareListData(data);
                            }
                        } catch (JSONException ex) {
                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                        }
                    }

                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                }
            }
        });


    }

    private void prepareListData(Data data) {

        if (data == null)
            return;

        faqCategoryArrayList = data.getFaqlist();
        spinnerAdapterFAQ.refreshList(faqCategoryArrayList);

        if (faqCategoryArrayList != null && faqCategoryArrayList.size() > 0) {
            binding.expandableListviewFaq.setVisibility(View.VISIBLE);
            binding.noDataFoundLayout.setVisibility(View.GONE);
            faqsArrayList = faqCategoryArrayList.get(0).getFaqlist();
            if (faqsArrayList != null) {
                expandableAdapterFAQ.refreshList(faqsArrayList);
            }
        } else {
            binding.expandableListviewFaq.setVisibility(View.GONE);
            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
        }


    }

    private void initUiContents() {


        faqCategoryArrayList = new ArrayList<>();
        spinnerAdapterFAQ = new SpinnerAdapterFAQ(getActivity(), faqCategoryArrayList);
        binding.spFaq.setAdapter(spinnerAdapterFAQ);


        faqsArrayList = new ArrayList<>();
        expandableAdapterFAQ = new ExpandableAdapterFAQ(getActivity(), faqsArrayList);
        binding.expandableListviewFaq.setAdapter(expandableAdapterFAQ);

    }

    private void initUIListener() {

        binding.spFaq.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((HomeActivity) getActivity()).clearSearchView();
                spinnerAdapterFAQ.setLastSelectedPos(position);
                spinnerAdapterFAQ.notifyDataSetChanged();
                spinnerAdapterFAQ.setOpened(false);

                if (faqCategoryArrayList != null && faqCategoryArrayList.size() > position) {
                    faqsArrayList = faqCategoryArrayList.get(position).getFaqlist();
                    expandableAdapterFAQ.refreshList(faqsArrayList);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spFaq.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                spinnerAdapterFAQ.setOpened(true);
                return false;
            }
        });

    }


    public void onSearch(String keyword) {
        expandableAdapterFAQ.getFilter().filter(keyword);
    }

    public void onSearchClosed() {
        expandableAdapterFAQ.refreshList(faqsArrayList);
    }

}
