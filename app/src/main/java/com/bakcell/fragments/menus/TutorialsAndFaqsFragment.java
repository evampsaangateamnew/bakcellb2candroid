package com.bakcell.fragments.menus;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bakcell.R;
import com.bakcell.databinding.FragmentTutorialsAndFaqsBinding;

/**
 * Created by Noman on 18-May-17.
 */

public class TutorialsAndFaqsFragment extends Fragment {


    FragmentTutorialsAndFaqsBinding binding;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tutorials_and_faqs, container, false);
        return binding.getRoot();
    }
}
