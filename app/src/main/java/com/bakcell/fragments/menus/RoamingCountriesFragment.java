package com.bakcell.fragments.menus;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bakcell.R;
import com.bakcell.activities.HomeActivity;
import com.bakcell.adapters.RoamingCountriesAdapter;
import com.bakcell.databinding.FragmentRoamingCountriesBinding;
import com.bakcell.globals.BakcellPopUpDialog;
import com.bakcell.globals.MultiAccountsHandler;
import com.bakcell.models.DataManager;
import com.bakcell.models.roamingnewmodels.RoamingCountriesHelperModel;
import com.bakcell.models.roamingnewmodels.RoamingModelNew;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.Tools;
import com.bakcell.webservices.RoamingCountriesWebService;
import com.bakcell.webservices.core.EndPoints;
import com.bakcell.webservices.core.ServiceIDs;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import allaudin.github.io.ease.EaseCacheManager;
import allaudin.github.io.ease.EaseCallbacks;
import allaudin.github.io.ease.EaseException;
import allaudin.github.io.ease.EaseRequest;
import allaudin.github.io.ease.EaseResponse;

public class RoamingCountriesFragment extends Fragment implements View.OnClickListener {

    private RoamingCountriesAdapter roamingCountriesAdapter = null;
    private FragmentRoamingCountriesBinding binding;
    private RoamingModelNew roamingModelNew = null;
    private ArrayList<RoamingCountriesHelperModel> roamingCountriesHelperModel = null;
    public static final String ROAMING_COUNTRIES_DETAILS_DATA_KEY = "roaming_country_details_key";
    public static final String ROAMING_COUNTRIES_SELECTED_KEY = "roaming_country_selected_key";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_roaming_countries, container, false);

        initUIListeners();
        requestRoamingCountriesData();
        return binding.getRoot();
    }//onCreateView ends

    public void onSearch(String keyword) {
        if (roamingCountriesAdapter != null) {
            roamingCountriesAdapter.getFilter().filter(keyword);
        }
    }

    public void onSearchClosed() {
        if (roamingModelNew != null) {
            roamingCountriesAdapter.refreshList(getModelList(roamingModelNew));
        }
    }

    private void initUIListeners() {

    }//initUIListeners ends

    public void requestRoamingCountriesData() {
        RoamingCountriesWebService.newInstance(getActivity(), ServiceIDs.ROAMING_COUNTRIES_DATA_ID)
                .execute(DataManager.getInstance().getCurrentUser(),
                        new EaseCallbacks<String>() {
                            @Override
                            public void onSuccess(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                String result = response.getData();
                                if (result == null) return;
                                roamingModelNew = new Gson().fromJson(result, RoamingModelNew.class);
                                if (roamingModelNew != null) {
                                    if (roamingModelNew.getCountryList() != null) {
                                        if (roamingModelNew.getCountryList().size() > 0) {
                                            //initiate the ui
                                            initiateUI(roamingModelNew);
                                        } else {
                                            binding.roamingCountriesList.setVisibility(View.GONE);
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        binding.roamingCountriesList.setVisibility(View.GONE);
                                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    binding.roamingCountriesList.setVisibility(View.GONE);
                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onFailure(@NonNull EaseRequest<String> request, EaseResponse<String> response) {

                                // Check if user logout by server
                                if (getActivity() != null && !getActivity().isFinishing() &&
                                        response != null && response.getResultCode() != null &&
                                        response.getResultCode().equalsIgnoreCase(EndPoints.Constants.KEY_CODE_LOGOUT)) {
                                    Toast.makeText(getActivity(), "" + response.getDescription(), Toast.LENGTH_SHORT).show();
                                    HomeActivity.logoutUser(getActivity());
                                    return;
                                }

                                // Load Cashed Data
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getRoamingCountriesCacheKey(getActivity());
                                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            roamingModelNew = new Gson().fromJson(cacheData, RoamingModelNew.class);
                                            if (roamingModelNew != null) {
                                                if (roamingModelNew.getCountryList() != null) {
                                                    if (roamingModelNew.getCountryList().size() > 0) {
                                                        //initiate the ui
                                                        initiateUI(roamingModelNew);
                                                    } else {
                                                        binding.roamingCountriesList.setVisibility(View.GONE);
                                                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                    }
                                                } else {
                                                    binding.roamingCountriesList.setVisibility(View.GONE);
                                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                }
                                            } else {
                                                binding.roamingCountriesList.setVisibility(View.GONE);
                                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                            }
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                            binding.roamingCountriesList.setVisibility(View.GONE);
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }

                                if (getActivity() != null && !getActivity().isFinishing() && response != null) {
                                    BakcellPopUpDialog.showMessageDialog(getActivity(),
                                            getString(R.string.bakcell_error_title), response.getDescription());
                                }
                            }

                            @Override
                            public void onError(@NonNull EaseRequest<String> request, @NonNull EaseException e) {
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    BakcellPopUpDialog.ApiFailureMessage(getActivity());
                                }

                                // Load Cashed Data
                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    String cacheKey = MultiAccountsHandler.CacheKeys.getRoamingCountriesCacheKey(getActivity());
                                    String cacheResponse = EaseCacheManager.get(getActivity(), cacheKey, null);
                                    if (Tools.hasValue(cacheResponse)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(cacheResponse);
                                            String cacheData = jsonObject.getJSONObject("data").toString();
                                            roamingModelNew = new Gson().fromJson(cacheData, RoamingModelNew.class);
                                            if (roamingModelNew != null) {
                                                if (roamingModelNew.getCountryList() != null) {
                                                    if (roamingModelNew.getCountryList().size() > 0) {
                                                        //initiate the ui
                                                        initiateUI(roamingModelNew);
                                                    } else {
                                                        binding.roamingCountriesList.setVisibility(View.GONE);
                                                        binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                    }
                                                } else {
                                                    binding.roamingCountriesList.setVisibility(View.GONE);
                                                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                                }
                                            } else {
                                                binding.roamingCountriesList.setVisibility(View.GONE);
                                                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                            }
                                        } catch (JSONException ex) {
                                            Logger.debugLog(Logger.TAG_CATCH_LOGS, ex.getMessage());
                                            binding.roamingCountriesList.setVisibility(View.GONE);
                                            binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            }

                        });

    }//requestRoamingCountriesData ends

    private void initiateUI(RoamingModelNew roamingModel) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            roamingCountriesHelperModel = getModelList(roamingModel);
            if (roamingCountriesHelperModel != null) {
                if (roamingCountriesHelperModel.size() > 0) {
                    roamingCountriesAdapter = new RoamingCountriesAdapter(getContext(), getActivity(), roamingCountriesHelperModel, roamingModel);
                    binding.roamingCountriesList.setAdapter(roamingCountriesAdapter);
                    binding.roamingCountriesList.setVisibility(View.VISIBLE);
                    binding.noDataFoundLayout.setVisibility(View.GONE);
                } else {
                    binding.roamingCountriesList.setVisibility(View.GONE);
                    binding.noDataFoundLayout.setVisibility(View.VISIBLE);
                }
            } else {
                binding.roamingCountriesList.setVisibility(View.GONE);
                binding.noDataFoundLayout.setVisibility(View.VISIBLE);
            }
        }
    }//initiateUI ends

    private ArrayList<RoamingCountriesHelperModel> getModelList(RoamingModelNew countries) {
        ArrayList<RoamingCountriesHelperModel> model = new ArrayList<>();

        for (int i = 0; i < countries.getCountryList().size(); i++) {
            if (Tools.hasValue(countries.getCountryList().get(i).getCountry()) &&
                    Tools.hasValue(countries.getCountryList().get(i).getCountry_id())) {
                model.add(new RoamingCountriesHelperModel(countries.getCountryList().get(i).getCountry(),
                        countries.getCountryList().get(i).getCountry_id()));
            }
        }//for ends

        /*Collections.sort(model, new Comparator<RoamingCountriesHelperModel>() {
            @Override
            public int compare(RoamingCountriesHelperModel o1, RoamingCountriesHelperModel o2) {
                return o1.country.compareTo(o2.country);
            }
        });*/
        return model;
    }//getModelList ends

    @Override
    public void onClick(View v) {
//        int id = v.getId();
//        switch (id) {
//            case R.id.testButton:
//                BaseActivity.startNewActivity(getActivity(), RoamingCountryTabsActivity.class);
//                break;
//        }//switch ends
    }//onClick ends
}//class ends
