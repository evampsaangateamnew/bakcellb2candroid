package com.bakcell.models.friendfamily;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 02-Oct-17.
 */

public class FriendFamily implements Parcelable {

    @SerializedName("createdDate")
    private String createdDate;
    @SerializedName("msisdn")
    private String msisdn;

    protected FriendFamily(Parcel in) {
        createdDate = in.readString();
        msisdn = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(createdDate);
        dest.writeString(msisdn);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FriendFamily> CREATOR = new Creator<FriendFamily>() {
        @Override
        public FriendFamily createFromParcel(Parcel in) {
            return new FriendFamily(in);
        }

        @Override
        public FriendFamily[] newArray(int size) {
            return new FriendFamily[size];
        }
    };

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
