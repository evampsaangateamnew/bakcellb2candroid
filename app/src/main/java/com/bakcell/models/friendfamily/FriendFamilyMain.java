package com.bakcell.models.friendfamily;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 02-Oct-17.
 */

public class FriendFamilyMain implements Parcelable {

    @SerializedName("fnfLimit")
    private String fnfLimit;
    @SerializedName("fnfList")
    private ArrayList<FriendFamily> fnfList;


    protected FriendFamilyMain(Parcel in) {
        fnfLimit = in.readString();
        fnfList = in.createTypedArrayList(FriendFamily.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fnfLimit);
        dest.writeTypedList(fnfList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FriendFamilyMain> CREATOR = new Creator<FriendFamilyMain>() {
        @Override
        public FriendFamilyMain createFromParcel(Parcel in) {
            return new FriendFamilyMain(in);
        }

        @Override
        public FriendFamilyMain[] newArray(int size) {
            return new FriendFamilyMain[size];
        }
    };

    public String getFnfLimit() {
        return fnfLimit;
    }

    public void setFnfLimit(String fnfLimit) {
        this.fnfLimit = fnfLimit;
    }

    public ArrayList<FriendFamily> getFnfList() {
        return fnfList;
    }

    public void setFnfList(ArrayList<FriendFamily> fnfList) {
        this.fnfList = fnfList;
    }
}
