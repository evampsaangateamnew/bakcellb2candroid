package com.bakcell.models.Email;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Freeware Sys on 04-Aug-17.
 */

public class DataEmail implements Parcelable {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected DataEmail(Parcel in) {
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DataEmail> CREATOR = new Creator<DataEmail>() {
        @Override
        public DataEmail createFromParcel(Parcel in) {
            return new DataEmail(in);
        }

        @Override
        public DataEmail[] newArray(int size) {
            return new DataEmail[size];
        }
    };
}
