package com.bakcell.models.rateusapimodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RateUsAPIModel implements Parcelable {
    @SerializedName("pic_tnc")
    private String pic_tnc;
    @SerializedName("rateus_ios")
    private String rateus_ios;
    @SerializedName("rateus_android")
    private String rateus_android;

    protected RateUsAPIModel(Parcel in) {
        pic_tnc = in.readString();
        rateus_ios = in.readString();
        rateus_android = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pic_tnc);
        dest.writeString(rateus_ios);
        dest.writeString(rateus_android);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RateUsAPIModel> CREATOR = new Creator<RateUsAPIModel>() {
        @Override
        public RateUsAPIModel createFromParcel(Parcel in) {
            return new RateUsAPIModel(in);
        }

        @Override
        public RateUsAPIModel[] newArray(int size) {
            return new RateUsAPIModel[size];
        }
    };

    public String getPic_tnc() {
        return pic_tnc;
    }

    public void setPic_tnc(String pic_tnc) {
        this.pic_tnc = pic_tnc;
    }

    public String getRateus_ios() {
        return rateus_ios;
    }

    public void setRateus_ios(String rateus_ios) {
        this.rateus_ios = rateus_ios;
    }

    public String getRateus_android() {
        return rateus_android;
    }

    public void setRateus_android(String rateus_android) {
        this.rateus_android = rateus_android;
    }
}
