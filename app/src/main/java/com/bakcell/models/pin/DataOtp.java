package com.bakcell.models.pin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Freeware Sys on 04-Aug-17.
 */

public class DataOtp implements Parcelable {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected DataOtp(Parcel in) {
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DataOtp> CREATOR = new Creator<DataOtp>() {
        @Override
        public DataOtp createFromParcel(Parcel in) {
            return new DataOtp(in);
        }

        @Override
        public DataOtp[] newArray(int size) {
            return new DataOtp[size];
        }
    };
}
