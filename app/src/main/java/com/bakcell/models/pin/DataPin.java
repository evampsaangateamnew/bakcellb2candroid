package com.bakcell.models.pin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Freeware Sys on 04-Aug-17.
 */

public class DataPin implements Parcelable {

    @SerializedName("pin")
    private String pin;

    protected DataPin(Parcel in) {
        pin = in.readString();
    }

    public static final Creator<DataPin> CREATOR = new Creator<DataPin>() {
        @Override
        public DataPin createFromParcel(Parcel in) {
            return new DataPin(in);
        }

        @Override
        public DataPin[] newArray(int size) {
            return new DataPin[size];
        }
    };

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pin);
    }
}
