package com.bakcell.models.topup;

import com.google.gson.annotations.SerializedName;

public class InitiatePaymentData {

    @SerializedName("paymentKey")
    private String paymentKey;

    public String getPaymentKey() {
        return paymentKey;
    }

    public void setPaymentKey(String paymentKey) {
        this.paymentKey = paymentKey;
    }

}