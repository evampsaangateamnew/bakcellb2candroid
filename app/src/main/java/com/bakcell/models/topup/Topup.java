package com.bakcell.models.topup;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 18-Sep-17.
 */

public class Topup implements Parcelable {

    @SerializedName("oldBalance")
    private String oldBalance;
    @SerializedName("newBalance")
    private String newBalance;
    @SerializedName("newCredit")
    private String newCredit;

    protected Topup(Parcel in) {
        oldBalance = in.readString();
        newBalance = in.readString();
        newCredit = in.readString();
    }

    public static final Creator<Topup> CREATOR = new Creator<Topup>() {
        @Override
        public Topup createFromParcel(Parcel in) {
            return new Topup(in);
        }

        @Override
        public Topup[] newArray(int size) {
            return new Topup[size];
        }
    };

    public String getNewCredit() {
        return newCredit;
    }

    public void setNewCredit(String newCredit) {
        this.newCredit = newCredit;
    }

    public String getOldBalance() {
        return oldBalance;
    }

    public void setOldBalance(String oldBalance) {
        this.oldBalance = oldBalance;
    }

    public String getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(String newBalance) {
        this.newBalance = newBalance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(oldBalance);
        dest.writeString(newBalance);
        dest.writeString(newCredit);
    }
}
