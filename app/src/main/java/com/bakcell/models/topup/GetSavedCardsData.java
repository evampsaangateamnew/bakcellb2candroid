package com.bakcell.models.topup;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSavedCardsData implements Parcelable {
    @SerializedName("cardDetails")
    private List<GetSavedCardsItems> data;
    @SerializedName("lastAmount")
    private double lastAmount;


    protected GetSavedCardsData(Parcel in) {
        data = in.createTypedArrayList(GetSavedCardsItems.CREATOR);
        lastAmount = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
        dest.writeDouble(lastAmount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetSavedCardsData> CREATOR = new Creator<GetSavedCardsData>() {
        @Override
        public GetSavedCardsData createFromParcel(Parcel in) {
            return new GetSavedCardsData(in);
        }

        @Override
        public GetSavedCardsData[] newArray(int size) {
            return new GetSavedCardsData[size];
        }
    };

    public List<GetSavedCardsItems> getData() {
        return data;
    }

    public void setData(List<GetSavedCardsItems> data) {
        this.data = data;
    }

    public double getLastAmount() {
        return lastAmount;
    }

    public void setLastAmount(double lastAmount) {
        this.lastAmount = lastAmount;
    }
}
