package com.bakcell.models.topup;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GetSavedCardsItems implements Parcelable {
    @SerializedName("cardType")
    private String cardType;
    @SerializedName("paymentKey")
    private String paymentKey;
    @SerializedName("cardMaskNumber")
    private String topupNumber;
    @SerializedName("id")
    private String id;


    protected GetSavedCardsItems(Parcel in) {
        cardType = in.readString();
        paymentKey = in.readString();
        topupNumber = in.readString();
        id = in.readString();
    }

    public GetSavedCardsItems() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cardType);
        dest.writeString(paymentKey);
        dest.writeString(topupNumber);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetSavedCardsItems> CREATOR = new Creator<GetSavedCardsItems>() {
        @Override
        public GetSavedCardsItems createFromParcel(Parcel in) {
            return new GetSavedCardsItems(in);
        }

        @Override
        public GetSavedCardsItems[] newArray(int size) {
            return new GetSavedCardsItems[size];
        }
    };

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getPaymentKey() {
        return paymentKey;
    }

    public void setPaymentKey(String paymentKey) {
        this.paymentKey = paymentKey;
    }

    public String getTopupNumber() {
        return topupNumber;
    }

    public void setTopupNumber(String topupNumber) {
        this.topupNumber = topupNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
