package com.bakcell.models.moneytransfer;

/**
 * Created by Zeeshan Shabbir on 9/19/2017.
 */

public class MoneyTransfer {
    private String number;
    private String text;
    private String amount;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
