package com.bakcell.models.ulduzumdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UlduzumDetailsModel {
    @SerializedName("merchantDetail")
    private MerchantModel merchantModel;

    public UlduzumDetailsModel(MerchantModel merchantModel) {
        this.merchantModel = merchantModel;
    }

    public MerchantModel getMerchantModel() {
        return merchantModel;
    }

    public void setMerchantModel(MerchantModel merchantModel) {
        this.merchantModel = merchantModel;
    }
}
