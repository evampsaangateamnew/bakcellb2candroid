package com.bakcell.models.coreservices;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 10/10/2017.
 */

public class CoreServicesList implements Parcelable {

    @SerializedName("categoryId")
    int categoryId;
    @SerializedName("id")
    int id;
    @SerializedName("sortOrder")
    int sortOrder;
    @SerializedName("description")
    String description;
    @SerializedName("effectiveDate")
    String effectiveDate;
    @SerializedName("expireDate")
    String expireDate;
    @SerializedName("forwardNumber")
    String forwardNumber;
    @SerializedName("freeFor")
    String freeFor;
    @SerializedName("name")
    String name;
    @SerializedName("offeringId")
    String offeringId;
    @SerializedName("price")
    String price;
    @SerializedName("renewable")
    String renewable;
    @SerializedName("status")
    String status;
    @SerializedName("storeId")
    String storeId;
    @SerializedName("validity")
    String validity;

    @SerializedName("validityLabel")
    String validityLabel;
    @SerializedName("progressDateLabel")
    String progressDateLabel;
    @SerializedName("progressTitle")
    String progressTitle;

    protected CoreServicesList(Parcel in) {
        categoryId = in.readInt();
        id = in.readInt();
        sortOrder = in.readInt();
        description = in.readString();
        effectiveDate = in.readString();
        expireDate = in.readString();
        forwardNumber = in.readString();
        freeFor = in.readString();
        name = in.readString();
        offeringId = in.readString();
        price = in.readString();
        renewable = in.readString();
        status = in.readString();
        storeId = in.readString();
        validity = in.readString();
        validityLabel = in.readString();
        progressDateLabel = in.readString();
        progressTitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeInt(id);
        dest.writeInt(sortOrder);
        dest.writeString(description);
        dest.writeString(effectiveDate);
        dest.writeString(expireDate);
        dest.writeString(forwardNumber);
        dest.writeString(freeFor);
        dest.writeString(name);
        dest.writeString(offeringId);
        dest.writeString(price);
        dest.writeString(renewable);
        dest.writeString(status);
        dest.writeString(storeId);
        dest.writeString(validity);
        dest.writeString(validityLabel);
        dest.writeString(progressDateLabel);
        dest.writeString(progressTitle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CoreServicesList> CREATOR = new Creator<CoreServicesList>() {
        @Override
        public CoreServicesList createFromParcel(Parcel in) {
            return new CoreServicesList(in);
        }

        @Override
        public CoreServicesList[] newArray(int size) {
            return new CoreServicesList[size];
        }
    };

    public String getValidityLabel() {
        return validityLabel;
    }

    public void setValidityLabel(String validityLabel) {
        this.validityLabel = validityLabel;
    }

    public String getProgressDateLabel() {
        return progressDateLabel;
    }

    public void setProgressDateLabel(String progressDateLabel) {
        this.progressDateLabel = progressDateLabel;
    }

    public String getProgressTitle() {
        return progressTitle;
    }

    public void setProgressTitle(String progressTitle) {
        this.progressTitle = progressTitle;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getForwardNumber() {
        return forwardNumber;
    }

    public void setForwardNumber(String forwardNumber) {
        this.forwardNumber = forwardNumber;
    }

    public String getFreeFor() {
        return freeFor;
    }

    public void setFreeFor(String freeFor) {
        this.freeFor = freeFor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRenewable() {
        return renewable;
    }

    public void setRenewable(String renewable) {
        this.renewable = renewable;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }


}
