package com.bakcell.models.coreservices;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 10/10/2017.
 */

public class CoreServicesMain implements Parcelable{
    @SerializedName("coreServiceCategory")
    String coreServiceCategory;
    @SerializedName("coreServicesList")
    ArrayList<CoreServicesList> coreServicesList;

    public String getCoreServiceCategory() {
        return coreServiceCategory;
    }

    public void setCoreServiceCategory(String coreServiceCategory) {
        this.coreServiceCategory = coreServiceCategory;
    }

    public ArrayList<CoreServicesList> getCoreServicesList() {
        return coreServicesList;
    }

    public void setCoreServicesList(ArrayList<CoreServicesList> coreServicesList) {
        this.coreServicesList = coreServicesList;
    }

    protected CoreServicesMain(Parcel in) {
        coreServiceCategory = in.readString();
        coreServicesList = in.createTypedArrayList(CoreServicesList.CREATOR);
    }

    public static final Creator<CoreServicesMain> CREATOR = new Creator<CoreServicesMain>() {
        @Override
        public CoreServicesMain createFromParcel(Parcel in) {
            return new CoreServicesMain(in);
        }

        @Override
        public CoreServicesMain[] newArray(int size) {
            return new CoreServicesMain[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(coreServiceCategory);
        dest.writeTypedList(coreServicesList);
    }
}
