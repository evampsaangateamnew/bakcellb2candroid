package com.bakcell.models.coreservices;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 10/10/2017.
 */

public class CoreServices implements Parcelable {

    @SerializedName("message")
    String message;

    @SerializedName("coreServices")
    private ArrayList<CoreServicesMain> coreServices;

    protected CoreServices(Parcel in) {
        message = in.readString();
        coreServices = in.createTypedArrayList(CoreServicesMain.CREATOR);
    }

    public static final Creator<CoreServices> CREATOR = new Creator<CoreServices>() {
        @Override
        public CoreServices createFromParcel(Parcel in) {
            return new CoreServices(in);
        }

        @Override
        public CoreServices[] newArray(int size) {
            return new CoreServices[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CoreServicesMain> getCoreServices() {
        return coreServices;
    }

    public void setCoreServices(ArrayList<CoreServicesMain> coreServices) {
        this.coreServices = coreServices;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeTypedList(coreServices);
    }
}
