package com.bakcell.models.contactus;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 03-Oct-17.
 */

public class ContactUs implements Parcelable {

    @SerializedName("address")
    private String address;
    @SerializedName("addressLat")
    private String addressLat;
    @SerializedName("addressLong")
    private String addressLong;
    @SerializedName("customerCareNo")
    private String customerCareNo;
    @SerializedName("email")
    private String email;
    @SerializedName("facebookLink")
    private String facebookLink;
    @SerializedName("fax")
    private String fax;
    @SerializedName("googleLink")
    private String googleLink;
    @SerializedName("inviteFriendText")
    private String inviteFriendText;
    @SerializedName("linkedinLink")
    private String linkedinLink;
    @SerializedName("phone")
    private String phone;
    @SerializedName("twitterLink")
    private String twitterLink;
    @SerializedName("website")
    private String website;
    @SerializedName("youtubeLink")
    private String youtubeLink;
    @SerializedName("instagram")
    private String instagram;

    protected ContactUs(Parcel in) {
        address = in.readString();
        addressLat = in.readString();
        addressLong = in.readString();
        customerCareNo = in.readString();
        email = in.readString();
        facebookLink = in.readString();
        fax = in.readString();
        googleLink = in.readString();
        inviteFriendText = in.readString();
        linkedinLink = in.readString();
        phone = in.readString();
        twitterLink = in.readString();
        website = in.readString();
        youtubeLink = in.readString();
        instagram = in.readString();
    }

    public static final Creator<ContactUs> CREATOR = new Creator<ContactUs>() {
        @Override
        public ContactUs createFromParcel(Parcel in) {
            return new ContactUs(in);
        }

        @Override
        public ContactUs[] newArray(int size) {
            return new ContactUs[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressLat() {
        return addressLat;
    }

    public void setAddressLat(String addressLat) {
        this.addressLat = addressLat;
    }

    public String getAddressLong() {
        return addressLong;
    }

    public void setAddressLong(String addressLong) {
        this.addressLong = addressLong;
    }

    public String getCustomerCareNo() {
        return customerCareNo;
    }

    public void setCustomerCareNo(String customerCareNo) {
        this.customerCareNo = customerCareNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getGoogleLink() {
        return googleLink;
    }

    public void setGoogleLink(String googleLink) {
        this.googleLink = googleLink;
    }

    public String getInviteFriendText() {
        return inviteFriendText;
    }

    public void setInviteFriendText(String inviteFriendText) {
        this.inviteFriendText = inviteFriendText;
    }

    public String getLinkedinLink() {
        return linkedinLink;
    }

    public void setLinkedinLink(String linkedinLink) {
        this.linkedinLink = linkedinLink;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(address);
        parcel.writeString(addressLat);
        parcel.writeString(addressLong);
        parcel.writeString(customerCareNo);
        parcel.writeString(email);
        parcel.writeString(facebookLink);
        parcel.writeString(fax);
        parcel.writeString(googleLink);
        parcel.writeString(inviteFriendText);
        parcel.writeString(linkedinLink);
        parcel.writeString(phone);
        parcel.writeString(twitterLink);
        parcel.writeString(website);
        parcel.writeString(youtubeLink);
        parcel.writeString(instagram);
    }
}
