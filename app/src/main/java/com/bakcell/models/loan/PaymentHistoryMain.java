package com.bakcell.models.loan;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Noman on 6/9/2017.
 */

public class PaymentHistoryMain implements Parcelable {

    private ArrayList<PaymentHistory> paymentHistory;

    public ArrayList<PaymentHistory> getPaymentHistory() {
        return paymentHistory;
    }

    public void setPaymentHistory(ArrayList<PaymentHistory> paymentHistory) {
        this.paymentHistory = paymentHistory;
    }

    protected PaymentHistoryMain(Parcel in) {
        paymentHistory = in.createTypedArrayList(PaymentHistory.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(paymentHistory);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaymentHistoryMain> CREATOR = new Creator<PaymentHistoryMain>() {
        @Override
        public PaymentHistoryMain createFromParcel(Parcel in) {
            return new PaymentHistoryMain(in);
        }

        @Override
        public PaymentHistoryMain[] newArray(int size) {
            return new PaymentHistoryMain[size];
        }
    };
}
