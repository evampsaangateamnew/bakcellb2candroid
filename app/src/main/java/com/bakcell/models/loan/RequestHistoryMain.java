package com.bakcell.models.loan;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 18-Sep-17.
 */

public class RequestHistoryMain implements Parcelable {

    @SerializedName("loanHistory")
    private ArrayList<RequestHistory> loanHistory;

    protected RequestHistoryMain(Parcel in) {
        loanHistory = in.createTypedArrayList(RequestHistory.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(loanHistory);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestHistoryMain> CREATOR = new Creator<RequestHistoryMain>() {
        @Override
        public RequestHistoryMain createFromParcel(Parcel in) {
            return new RequestHistoryMain(in);
        }

        @Override
        public RequestHistoryMain[] newArray(int size) {
            return new RequestHistoryMain[size];
        }
    };

    public ArrayList<RequestHistory> getRequestHistoriesList() {
        return loanHistory;
    }

    public void setRequestHistoriesList(ArrayList<RequestHistory> requestHistoriesList) {
        this.loanHistory = requestHistoriesList;
    }
}
