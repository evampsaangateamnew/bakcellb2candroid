package com.bakcell.models.loan;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 6/9/2017.
 */

public class PaymentHistory implements Parcelable{

    @SerializedName("amount")
    private String amount;
    @SerializedName("dateTime")
    private String dateTime;
    @SerializedName("loanID")
    private String loanID;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getLoanID() {
        return loanID;
    }

    public void setLoanID(String loanID) {
        this.loanID = loanID;
    }

    protected PaymentHistory(Parcel in) {
        amount = in.readString();
        dateTime = in.readString();
        loanID = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
        dest.writeString(dateTime);
        dest.writeString(loanID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaymentHistory> CREATOR = new Creator<PaymentHistory>() {
        @Override
        public PaymentHistory createFromParcel(Parcel in) {
            return new PaymentHistory(in);
        }

        @Override
        public PaymentHistory[] newArray(int size) {
            return new PaymentHistory[size];
        }
    };
}
