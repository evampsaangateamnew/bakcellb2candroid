package com.bakcell.models.loan;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 18/10/2017.
 */

public class RequestHistory implements Parcelable {

    @SerializedName("amount")
    private String amount;
    @SerializedName("dateTime")
    private String dateTime;
    @SerializedName("loanID")
    private String loanID;
    @SerializedName("paid")
    private String paid;
    @SerializedName("remaining")
    private String remaining;
    @SerializedName("status")
    private String status;


    protected RequestHistory(Parcel in) {
        amount = in.readString();
        dateTime = in.readString();
        loanID = in.readString();
        paid = in.readString();
        remaining = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
        dest.writeString(dateTime);
        dest.writeString(loanID);
        dest.writeString(paid);
        dest.writeString(remaining);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestHistory> CREATOR = new Creator<RequestHistory>() {
        @Override
        public RequestHistory createFromParcel(Parcel in) {
            return new RequestHistory(in);
        }

        @Override
        public RequestHistory[] newArray(int size) {
            return new RequestHistory[size];
        }
    };

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getLoanID() {
        return loanID;
    }

    public void setLoanID(String loanID) {
        this.loanID = loanID;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
