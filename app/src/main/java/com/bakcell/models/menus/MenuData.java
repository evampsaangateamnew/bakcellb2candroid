package com.bakcell.models.menus;

import com.google.gson.annotations.SerializedName;

public class MenuData {

    @SerializedName("MenuEN")
    private MenuEN menuEN;

    @SerializedName("MenuAZ")
    private MenuAZ menuAZ;

    @SerializedName("MenuRU")
    private MenuRU menuRU;

    public void setMenuEN(MenuEN menuEN) {
        this.menuEN = menuEN;
    }

    public MenuEN getMenuEN() {
        return menuEN;
    }

    public void setMenuAZ(MenuAZ menuAZ) {
        this.menuAZ = menuAZ;
    }

    public MenuAZ getMenuAZ() {
        return menuAZ;
    }

    public void setMenuRU(MenuRU menuRU) {
        this.menuRU = menuRU;
    }

    public MenuRU getMenuRU() {
        return menuRU;
    }

    @Override
    public String toString() {
        return
                "Data{" +
                        "menuEN = '" + menuEN + '\'' +
                        ",menuAZ = '" + menuAZ + '\'' +
                        ",menuRU = '" + menuRU + '\'' +
                        "}";
    }
}