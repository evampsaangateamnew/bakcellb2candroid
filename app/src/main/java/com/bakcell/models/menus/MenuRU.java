package com.bakcell.models.menus;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MenuRU {

    @SerializedName("menuHorizontal")
    private ArrayList<MenuHorizontalItem> menuHorizontal;

    @SerializedName("menuVertical")
    private ArrayList<MenuVerticalItem> menuVertical;

    public void setMenuHorizontal(ArrayList<MenuHorizontalItem> menuHorizontal) {
        this.menuHorizontal = menuHorizontal;
    }

    public ArrayList<MenuHorizontalItem> getMenuHorizontal() {
        return menuHorizontal;
    }

    public void setMenuVertical(ArrayList<MenuVerticalItem> menuVertical) {
        this.menuVertical = menuVertical;
    }

    public ArrayList<MenuVerticalItem> getMenuVertical() {
        return menuVertical;
    }

    @Override
    public String toString() {
        return
                "MenuRU{" +
                        "menuHorizontal = '" + menuHorizontal + '\'' +
                        ",menuVertical = '" + menuVertical + '\'' +
                        "}";
    }
}