package com.bakcell.models.menus;

import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Tools;
import com.google.gson.annotations.SerializedName;

public class MenuVerticalItem {

    @SerializedName("identifier")
    private String identifier;

    @SerializedName("sortOrder")
    private String sortOrder;

    @SerializedName("title")
    private String title;

    private int menuItemId;

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public int getSortOrderInt() {
        int order = -1;
        try {
            if (Tools.isNumeric(sortOrder)) {
                order = Integer.valueOf(sortOrder);
            }
        } catch (Exception e) {
            BakcellLogger.logE("SortOrderX12", "sortOrderError:::" + e.toString(), "MenuVerticalItem", "getSortOrderInt");
        }
        return order;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return
                "MenuVerticalItem{" +
                        "identifier = '" + identifier + '\'' +
                        ",sortOrder = '" + sortOrder + '\'' +
                        ",title = '" + title + '\'' +
                        "}";
    }
}