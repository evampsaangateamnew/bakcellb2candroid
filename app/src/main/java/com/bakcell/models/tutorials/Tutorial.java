package com.bakcell.models.tutorials;

import androidx.annotation.DrawableRes;

/**
 * Created by Zeeshan Shabbir on 8/3/2017.
 */

public class Tutorial {
    @DrawableRes
    private int res;
    private String title;
    private String message;
    private int position;

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
