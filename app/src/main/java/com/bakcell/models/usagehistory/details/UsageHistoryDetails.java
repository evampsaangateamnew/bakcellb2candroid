package com.bakcell.models.usagehistory.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 23-Sep-17.
 */

public class UsageHistoryDetails implements Parcelable {

    @SerializedName("destination")
    private String destination;
    @SerializedName("endDateTime")
    private String endDateTime;
    @SerializedName("number")
    private String number;
    @SerializedName("period")
    private String period;
    @SerializedName("service")
    private String service;
    @SerializedName("startDateTime")
    private String startDateTime;
    @SerializedName("type")
    private String type;
    @SerializedName("usage")
    private String usage;
    @SerializedName("chargedAmount")
    private String chargedAmount;
    @SerializedName("unit")
    private String unit;
    @SerializedName("zone")
    private String zone;


    protected UsageHistoryDetails(Parcel in) {
        destination = in.readString();
        endDateTime = in.readString();
        number = in.readString();
        period = in.readString();
        service = in.readString();
        startDateTime = in.readString();
        type = in.readString();
        usage = in.readString();
        chargedAmount = in.readString();
        unit = in.readString();
        zone = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(destination);
        dest.writeString(endDateTime);
        dest.writeString(number);
        dest.writeString(period);
        dest.writeString(service);
        dest.writeString(startDateTime);
        dest.writeString(type);
        dest.writeString(usage);
        dest.writeString(chargedAmount);
        dest.writeString(unit);
        dest.writeString(zone);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsageHistoryDetails> CREATOR = new Creator<UsageHistoryDetails>() {
        @Override
        public UsageHistoryDetails createFromParcel(Parcel in) {
            return new UsageHistoryDetails(in);
        }

        @Override
        public UsageHistoryDetails[] newArray(int size) {
            return new UsageHistoryDetails[size];
        }
    };

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getChargedAmount() {
        return chargedAmount;
    }

    public void setChargedAmount(String chargedAmount) {
        this.chargedAmount = chargedAmount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
