package com.bakcell.models.usagehistory.summary;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 23-Sep-17.
 */

public class UsageHistorySummaryMain implements Parcelable {

    @SerializedName("summaryList")
    private ArrayList<UsageSummary> summaryList;

    protected UsageHistorySummaryMain(Parcel in) {
        summaryList = in.createTypedArrayList(UsageSummary.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(summaryList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsageHistorySummaryMain> CREATOR = new Creator<UsageHistorySummaryMain>() {
        @Override
        public UsageHistorySummaryMain createFromParcel(Parcel in) {
            return new UsageHistorySummaryMain(in);
        }

        @Override
        public UsageHistorySummaryMain[] newArray(int size) {
            return new UsageHistorySummaryMain[size];
        }
    };

    public ArrayList<UsageSummary> getSummaryList() {
        return summaryList;
    }

    public void setSummaryList(ArrayList<UsageSummary> summaryList) {
        this.summaryList = summaryList;
    }
}
