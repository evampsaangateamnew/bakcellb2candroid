package com.bakcell.models.usagehistory.summary;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 23-Sep-17.
 */

public class UsageSummaryRecords implements Parcelable {

    @SerializedName("chargeable_amount")
    private String chargeable_amount;
    @SerializedName("count")
    private String count;
    @SerializedName("service_type")
    private String service_type;
    @SerializedName("total_usage")
    private String total_usage;
    @SerializedName("unit")
    private String unit;


    protected UsageSummaryRecords(Parcel in) {
        chargeable_amount = in.readString();
        count = in.readString();
        service_type = in.readString();
        total_usage = in.readString();
        unit = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(chargeable_amount);
        dest.writeString(count);
        dest.writeString(service_type);
        dest.writeString(total_usage);
        dest.writeString(unit);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsageSummaryRecords> CREATOR = new Creator<UsageSummaryRecords>() {
        @Override
        public UsageSummaryRecords createFromParcel(Parcel in) {
            return new UsageSummaryRecords(in);
        }

        @Override
        public UsageSummaryRecords[] newArray(int size) {
            return new UsageSummaryRecords[size];
        }
    };

    public String getChargeable_amount() {
        return chargeable_amount;
    }

    public void setChargeable_amount(String chargeable_amount) {
        this.chargeable_amount = chargeable_amount;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getTotal_usage() {
        return total_usage;
    }

    public void setTotal_usage(String total_usage) {
        this.total_usage = total_usage;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
