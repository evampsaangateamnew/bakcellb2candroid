package com.bakcell.models.usagehistory.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 23-Sep-17.
 */

public class UsageHistoryDetailsMain implements Parcelable {

    @SerializedName("records")
    private ArrayList<UsageHistoryDetails> records;

    protected UsageHistoryDetailsMain(Parcel in) {
        records = in.createTypedArrayList(UsageHistoryDetails.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(records);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsageHistoryDetailsMain> CREATOR = new Creator<UsageHistoryDetailsMain>() {
        @Override
        public UsageHistoryDetailsMain createFromParcel(Parcel in) {
            return new UsageHistoryDetailsMain(in);
        }

        @Override
        public UsageHistoryDetailsMain[] newArray(int size) {
            return new UsageHistoryDetailsMain[size];
        }
    };

    public ArrayList<UsageHistoryDetails> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<UsageHistoryDetails> records) {
        this.records = records;
    }
}
