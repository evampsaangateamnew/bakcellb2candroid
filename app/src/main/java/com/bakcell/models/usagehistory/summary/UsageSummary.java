package com.bakcell.models.usagehistory.summary;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 23-Sep-17.
 */

public class UsageSummary implements Parcelable{

    @SerializedName("name")
    private String name;
    @SerializedName("totalCharge")
    private String totalCharge;
    @SerializedName("totalUsage")
    private String totalUsage;

    @SerializedName("records")
    private ArrayList<UsageSummaryRecords> records;

    protected UsageSummary(Parcel in) {
        name = in.readString();
        totalCharge = in.readString();
        totalUsage = in.readString();
        records = in.createTypedArrayList(UsageSummaryRecords.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(totalCharge);
        dest.writeString(totalUsage);
        dest.writeTypedList(records);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsageSummary> CREATOR = new Creator<UsageSummary>() {
        @Override
        public UsageSummary createFromParcel(Parcel in) {
            return new UsageSummary(in);
        }

        @Override
        public UsageSummary[] newArray(int size) {
            return new UsageSummary[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(String totalCharge) {
        this.totalCharge = totalCharge;
    }

    public String getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(String totalUsage) {
        this.totalUsage = totalUsage;
    }

    public ArrayList<UsageSummaryRecords> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<UsageSummaryRecords> records) {
        this.records = records;
    }
}
