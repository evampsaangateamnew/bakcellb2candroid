package com.bakcell.models.changepassword;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Noman on 16-Aug-17.
 */

public class ChangePasswordData implements Parcelable {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected ChangePasswordData(Parcel in) {
        message = in.readString();
    }

    public static final Creator<ChangePasswordData> CREATOR = new Creator<ChangePasswordData>() {
        @Override
        public ChangePasswordData createFromParcel(Parcel in) {
            return new ChangePasswordData(in);
        }

        @Override
        public ChangePasswordData[] newArray(int size) {
            return new ChangePasswordData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }
}
