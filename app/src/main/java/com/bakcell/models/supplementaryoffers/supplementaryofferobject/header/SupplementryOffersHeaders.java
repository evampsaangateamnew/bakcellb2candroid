/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * @author Noman
 */
public class SupplementryOffersHeaders implements Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("offeringId")
    private String offeringId;
    @SerializedName("offerLevel")
    private String offerLevel;
    @SerializedName("btnRenew")
    private String btnRenew;
    @SerializedName("stickerColorCode")
    private String stickerColorCode;
    @SerializedName("offerName")
    private String offerName;
    @SerializedName("price")
    private String price;
    @SerializedName("stickerLabel")
    private String stickerLabel;
    @SerializedName("validityTitle")
    private String validityTitle;
    @SerializedName("validityValue")
    private String validityValue;
    @SerializedName("validityInformation")
    private String validityInformation;
    @SerializedName("type")
    private String type;
    @SerializedName("appOfferFilter")
    private String appOfferFilter;
    @SerializedName("isTopUp")
    private String isTopUp;
    @SerializedName("offerGroup")
    private GroupedOfferAttributes offerGroup;
    @SerializedName("attributeList")
    private ArrayList<HeaderAttributes> attributeList;


    protected SupplementryOffersHeaders(Parcel in) {
        id = in.readString();
        offeringId = in.readString();
        offerLevel = in.readString();
        btnRenew = in.readString();
        stickerColorCode = in.readString();
        offerName = in.readString();
        price = in.readString();
        stickerLabel = in.readString();
        validityTitle = in.readString();
        validityValue = in.readString();
        validityInformation = in.readString();
        type = in.readString();
        appOfferFilter = in.readString();
        isTopUp = in.readString();
        offerGroup = in.readParcelable(GroupedOfferAttributes.class.getClassLoader());
        attributeList = in.createTypedArrayList(HeaderAttributes.CREATOR);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getOfferLevel() {
        return offerLevel;
    }

    public void setOfferLevel(String offerLevel) {
        this.offerLevel = offerLevel;
    }

    public String getBtnRenew() {
        return btnRenew;
    }

    public void setBtnRenew(String btnRenew) {
        this.btnRenew = btnRenew;
    }

    public String getStickerColorCode() {
        return stickerColorCode;
    }

    public void setStickerColorCode(String stickerColorCode) {
        this.stickerColorCode = stickerColorCode;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStickerLabel() {
        return stickerLabel;
    }

    public void setStickerLabel(String stickerLabel) {
        this.stickerLabel = stickerLabel;
    }

    public String getValidityTitle() {
        return validityTitle;
    }

    public void setValidityTitle(String validityTitle) {
        this.validityTitle = validityTitle;
    }

    public String getValidityValue() {
        return validityValue;
    }

    public void setValidityValue(String validityValue) {
        this.validityValue = validityValue;
    }

    public String getValidityInformation() {
        return validityInformation;
    }

    public void setValidityInformation(String validityInformation) {
        this.validityInformation = validityInformation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAppOfferFilter() {
        return appOfferFilter;
    }

    public void setAppOfferFilter(String appOfferFilter) {
        this.appOfferFilter = appOfferFilter;
    }

    public String getIsTopUp() {
        return isTopUp;
    }

    public void setIsTopUp(String isTopUp) {
        this.isTopUp = isTopUp;
    }

    public GroupedOfferAttributes getOfferGroup() {
        return offerGroup;
    }

    public void setOfferGroup(GroupedOfferAttributes offerGroup) {
        this.offerGroup = offerGroup;
    }

    public ArrayList<HeaderAttributes> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(ArrayList<HeaderAttributes> attributeList) {
        this.attributeList = attributeList;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(offeringId);
        dest.writeString(offerLevel);
        dest.writeString(btnRenew);
        dest.writeString(stickerColorCode);
        dest.writeString(offerName);
        dest.writeString(price);
        dest.writeString(stickerLabel);
        dest.writeString(validityTitle);
        dest.writeString(validityValue);
        dest.writeString(validityInformation);
        dest.writeString(type);
        dest.writeString(appOfferFilter);
        dest.writeString(isTopUp);
        dest.writeParcelable(offerGroup, flags);
        dest.writeTypedList(attributeList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SupplementryOffersHeaders> CREATOR = new Creator<SupplementryOffersHeaders>() {
        @Override
        public SupplementryOffersHeaders createFromParcel(Parcel in) {
            return new SupplementryOffersHeaders(in);
        }

        @Override
        public SupplementryOffersHeaders[] newArray(int size) {
            return new SupplementryOffersHeaders[size];
        }
    };
}