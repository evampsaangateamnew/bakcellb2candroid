/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class DetailsAttributes implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private String value;
    @SerializedName("unit")
    private String unit;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    protected DetailsAttributes(Parcel in) {
        title = in.readString();
        value = in.readString();
        unit = in.readString();
    }

    public static final Creator<DetailsAttributes> CREATOR = new Creator<DetailsAttributes>() {
        @Override
        public DetailsAttributes createFromParcel(Parcel in) {
            return new DetailsAttributes(in);
        }

        @Override
        public DetailsAttributes[] newArray(int size) {
            return new DetailsAttributes[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
        dest.writeString(unit);
    }
}
