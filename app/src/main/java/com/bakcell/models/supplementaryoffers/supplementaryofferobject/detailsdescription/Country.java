package com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.header.SupplementryOffersHeaders;
import com.google.gson.annotations.SerializedName;

public class Country implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("flag")
    private String flag;

    public Country(String name, String flag) {
        this.name = name;
        this.flag = flag;
    }

    protected Country(Parcel in) {
        name = in.readString();
        flag = in.readString();

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(flag);
    }


    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

}
