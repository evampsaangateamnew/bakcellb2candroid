package com.bakcell.models.supplementaryoffers.supplementaryofferobject;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription.SupplementryOffersDetailsAndDescription;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.header.SupplementryOffersHeaders;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Aug-17.
 */

public class SupplementaryOffer implements Parcelable {

    @SerializedName("header")
    private SupplementryOffersHeaders header;
    @SerializedName("details")
    private SupplementryOffersDetailsAndDescription details;
    @SerializedName("description")
    private SupplementryOffersDetailsAndDescription description;

    public SupplementryOffersHeaders getHeader() {
        return header;
    }

    public void setHeader(SupplementryOffersHeaders header) {
        this.header = header;
    }

    public SupplementryOffersDetailsAndDescription getDetails() {
        return details;
    }

    public void setDetails(SupplementryOffersDetailsAndDescription details) {
        this.details = details;
    }

    public SupplementryOffersDetailsAndDescription getDescription() {
        return description;
    }

    public void setDescription(SupplementryOffersDetailsAndDescription description) {
        this.description = description;
    }

    protected SupplementaryOffer(Parcel in) {
        header = in.readParcelable(SupplementryOffersHeaders.class.getClassLoader());
        details = in.readParcelable(SupplementryOffersDetailsAndDescription.class.getClassLoader());
        description = in.readParcelable(SupplementryOffersDetailsAndDescription.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(header, flags);
        dest.writeParcelable(details, flags);
        dest.writeParcelable(description, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SupplementaryOffer> CREATOR = new Creator<SupplementaryOffer>() {
        @Override
        public SupplementaryOffer createFromParcel(Parcel in) {
            return new SupplementaryOffer(in);
        }

        @Override
        public SupplementaryOffer[] newArray(int size) {
            return new SupplementaryOffer[size];
        }
    };
}
