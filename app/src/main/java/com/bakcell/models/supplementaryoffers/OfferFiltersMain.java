package com.bakcell.models.supplementaryoffers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 28-Aug-17.
 */

public class OfferFiltersMain implements Parcelable {

    @SerializedName("app")
    private ArrayList<OfferFilter> app;
    @SerializedName("tab")
    private ArrayList<OfferFilter> tab;
    @SerializedName("desktop")
    private ArrayList<OfferFilter> desktop;

    public ArrayList<OfferFilter> getApp() {
        return app;
    }

    public void setApp(ArrayList<OfferFilter> app) {
        this.app = app;
    }

    public ArrayList<OfferFilter> getDesktop() {
        return desktop;
    }

    public void setDesktop(ArrayList<OfferFilter> desktop) {
        this.desktop = desktop;
    }

    public ArrayList<OfferFilter> getTab() {
        return tab;
    }

    public void setTab(ArrayList<OfferFilter> tab) {
        this.tab = tab;
    }

    protected OfferFiltersMain(Parcel in) {
        app = in.createTypedArrayList(OfferFilter.CREATOR);
        desktop = in.createTypedArrayList(OfferFilter.CREATOR);
        tab = in.createTypedArrayList(OfferFilter.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(app);
        dest.writeTypedList(desktop);
        dest.writeTypedList(tab);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OfferFiltersMain> CREATOR = new Creator<OfferFiltersMain>() {
        @Override
        public OfferFiltersMain createFromParcel(Parcel in) {
            return new OfferFiltersMain(in);
        }

        @Override
        public OfferFiltersMain[] newArray(int size) {
            return new OfferFiltersMain[size];
        }
    };
}
