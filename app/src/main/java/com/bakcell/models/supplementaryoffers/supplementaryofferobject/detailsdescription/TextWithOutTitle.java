/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class TextWithOutTitle implements Parcelable {

    @SerializedName("description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    protected TextWithOutTitle(Parcel in) {
        description = in.readString();
    }

    public static final Creator<TextWithOutTitle> CREATOR = new Creator<TextWithOutTitle>() {
        @Override
        public TextWithOutTitle createFromParcel(Parcel in) {
            return new TextWithOutTitle(in);
        }

        @Override
        public TextWithOutTitle[] newArray(int size) {
            return new TextWithOutTitle[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
    }
}
