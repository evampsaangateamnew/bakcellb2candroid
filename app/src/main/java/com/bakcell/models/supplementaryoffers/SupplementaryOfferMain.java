package com.bakcell.models.supplementaryoffers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Aug-17.
 */

public class SupplementaryOfferMain implements Parcelable {

    @SerializedName("call")
    private OfferCategory call;
    @SerializedName("campaign")
    private OfferCategory campaign;
    @SerializedName("hybrid")
    private OfferCategory hybrid;
    @SerializedName("internet")
    private OfferCategory internet;
    @SerializedName("sms")
    private OfferCategory sms;
    @SerializedName("tm")
    private OfferCategory tm;
    @SerializedName("roaming")
    private RoamingsOffer roaming;

    public OfferCategory getCall() {
        return call;
    }

    public void setCall(OfferCategory call) {
        this.call = call;
    }

    public OfferCategory getCampaign() {
        return campaign;
    }

    public void setCampaign(OfferCategory campaign) {
        this.campaign = campaign;
    }

    public OfferCategory getHybrid() {
        return hybrid;
    }

    public void setHybrid(OfferCategory hybrid) {
        this.hybrid = hybrid;
    }

    public OfferCategory getInternet() {
        return internet;
    }

    public void setInternet(OfferCategory internet) {
        this.internet = internet;
    }

    public OfferCategory getSms() {
        return sms;
    }

    public void setSms(OfferCategory sms) {
        this.sms = sms;
    }

    public OfferCategory getTm() {
        return tm;
    }

    public void setTm(OfferCategory tm) {
        this.tm = tm;
    }

    public RoamingsOffer getRoaming() {
        return roaming;
    }

    public void setRoaming(RoamingsOffer roaming) {
        this.roaming = roaming;
    }

    protected SupplementaryOfferMain(Parcel in) {
        call = in.readParcelable(OfferCategory.class.getClassLoader());
        campaign = in.readParcelable(OfferCategory.class.getClassLoader());
        hybrid = in.readParcelable(OfferCategory.class.getClassLoader());
        internet = in.readParcelable(OfferCategory.class.getClassLoader());
        sms = in.readParcelable(OfferCategory.class.getClassLoader());
        tm = in.readParcelable(OfferCategory.class.getClassLoader());
        roaming = in.readParcelable(RoamingsOffer.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(call, flags);
        dest.writeParcelable(campaign, flags);
        dest.writeParcelable(hybrid, flags);
        dest.writeParcelable(internet, flags);
        dest.writeParcelable(sms, flags);
        dest.writeParcelable(tm, flags);
        dest.writeParcelable(roaming, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SupplementaryOfferMain> CREATOR = new Creator<SupplementaryOfferMain>() {
        @Override
        public SupplementaryOfferMain createFromParcel(Parcel in) {
            return new SupplementaryOfferMain(in);
        }

        @Override
        public SupplementaryOfferMain[] newArray(int size) {
            return new SupplementaryOfferMain[size];
        }
    };
}
