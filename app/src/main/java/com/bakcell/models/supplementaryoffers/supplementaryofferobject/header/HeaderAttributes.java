/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class HeaderAttributes implements Parcelable {
    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private String value;
    @SerializedName("description")
    private String description;
    @SerializedName("iconMap")
    private String iconMap;
    @SerializedName("unit")
    private String unit;
    @SerializedName("onnetLabel")
    private String onnetLabel;
    @SerializedName("onnetValue")
    private String onnetValue;
    @SerializedName("offnetLabel")
    private String offnetLabel;
    @SerializedName("offnetValue")
    private String offnetValue;

    protected HeaderAttributes(Parcel in) {
        title = in.readString();
        value = in.readString();
        description = in.readString();
        iconMap = in.readString();
        unit = in.readString();
        onnetLabel = in.readString();
        onnetValue = in.readString();
        offnetLabel = in.readString();
        offnetValue = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
        dest.writeString(description);
        dest.writeString(iconMap);
        dest.writeString(unit);
        dest.writeString(onnetLabel);
        dest.writeString(onnetValue);
        dest.writeString(offnetLabel);
        dest.writeString(offnetValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HeaderAttributes> CREATOR = new Creator<HeaderAttributes>() {
        @Override
        public HeaderAttributes createFromParcel(Parcel in) {
            return new HeaderAttributes(in);
        }

        @Override
        public HeaderAttributes[] newArray(int size) {
            return new HeaderAttributes[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconMap() {
        return iconMap;
    }

    public void setIconMap(String iconMap) {
        this.iconMap = iconMap;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    public String getOnnetLabel() {
        return onnetLabel;
    }

    public void setOnnetLabel(String onnetLabel) {
        this.onnetLabel = onnetLabel;
    }

    public String getOnnetValue() {
        return onnetValue;
    }

    public void setOnnetValue(String onnetValue) {
        this.onnetValue = onnetValue;
    }

    public String getOffnetLabel() {
        return offnetLabel;
    }

    public void setOffnetLabel(String offnetLabel) {
        this.offnetLabel = offnetLabel;
    }

    public String getOffnetValue() {
        return offnetValue;
    }

    public void setOffnetValue(String offnetValue) {
        this.offnetValue = offnetValue;
    }


}
