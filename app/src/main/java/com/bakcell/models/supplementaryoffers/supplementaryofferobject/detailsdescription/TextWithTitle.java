/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class TextWithTitle implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("text")
    private String text;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    protected TextWithTitle(Parcel in) {
        title = in.readString();
        text = in.readString();
    }

    public static final Creator<TextWithTitle> CREATOR = new Creator<TextWithTitle>() {
        @Override
        public TextWithTitle createFromParcel(Parcel in) {
            return new TextWithTitle(in);
        }

        @Override
        public TextWithTitle[] newArray(int size) {
            return new TextWithTitle[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(text);
    }
}
