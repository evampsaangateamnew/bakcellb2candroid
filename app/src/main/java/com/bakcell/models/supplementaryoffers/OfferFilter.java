package com.bakcell.models.supplementaryoffers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 28-Aug-17.
 */

public class OfferFilter implements Parcelable {

    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;

    private boolean isCheck;

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public OfferFilter() {

    }

    public OfferFilter(Parcel in) {
        key = in.readString();
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OfferFilter> CREATOR = new Creator<OfferFilter>() {
        @Override
        public OfferFilter createFromParcel(Parcel in) {
            return new OfferFilter(in);
        }

        @Override
        public OfferFilter[] newArray(int size) {
            return new OfferFilter[size];
        }
    };
}
