package com.bakcell.models.supplementaryoffers;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription.Country;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 24-Aug-17.
 */

public class RoamingsOffer implements Parcelable {

    @SerializedName("countries")
    private ArrayList<Country> countries;
    @SerializedName("offers")
    private ArrayList<SupplementaryOffer> offers;
    @SerializedName("filters")
    private OfferFiltersMain filters;

    public ArrayList<Country> getCountries() {
        return countries;
    }

    public void setCountries(ArrayList<Country> countries) {
        this.countries = countries;
    }

    public ArrayList<SupplementaryOffer> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<SupplementaryOffer> offers) {
        this.offers = offers;
    }

    public OfferFiltersMain getFilters() {
        return filters;
    }

    public void setFilters(OfferFiltersMain filters) {
        this.filters = filters;
    }

    public RoamingsOffer(){}

    protected RoamingsOffer(Parcel in) {
        countries = in.createTypedArrayList(Country.CREATOR);
        offers = in.createTypedArrayList(SupplementaryOffer.CREATOR);
        filters = in.readParcelable(OfferFiltersMain.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(countries);
        dest.writeTypedList(offers);
        dest.writeParcelable(filters, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoamingsOffer> CREATOR = new Creator<RoamingsOffer>() {
        @Override
        public RoamingsOffer createFromParcel(Parcel in) {
            return new RoamingsOffer(in);
        }

        @Override
        public RoamingsOffer[] newArray(int size) {
            return new RoamingsOffer[size];
        }
    };
}


