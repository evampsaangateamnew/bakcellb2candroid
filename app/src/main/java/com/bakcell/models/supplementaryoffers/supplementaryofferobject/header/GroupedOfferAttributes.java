/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class GroupedOfferAttributes implements Parcelable{
    @SerializedName("groupName")
    private String groupName;
    @SerializedName("groupValue")
    private String groupValue;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupValue() {
        return groupValue;
    }

    public void setGroupValue(String groupValue) {
        this.groupValue = groupValue;
    }

    protected GroupedOfferAttributes(Parcel in) {
        groupName = in.readString();
        groupValue = in.readString();
    }

    public static final Creator<GroupedOfferAttributes> CREATOR = new Creator<GroupedOfferAttributes>() {
        @Override
        public GroupedOfferAttributes createFromParcel(Parcel in) {
            return new GroupedOfferAttributes(in);
        }

        @Override
        public GroupedOfferAttributes[] newArray(int size) {
            return new GroupedOfferAttributes[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(groupName);
        dest.writeString(groupValue);
    }
}
