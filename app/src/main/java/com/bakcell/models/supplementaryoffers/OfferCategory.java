package com.bakcell.models.supplementaryoffers;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.supplementaryoffers.supplementaryofferobject.SupplementaryOffer;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 24-Aug-17.
 */

public class OfferCategory implements Parcelable {

    @SerializedName("filters")
    private OfferFiltersMain filters;
    @SerializedName("offers")
    private ArrayList<SupplementaryOffer> offers;

    public OfferCategory(){}

    public OfferFiltersMain getFilters() {
        return filters;
    }

    public void setFilters(OfferFiltersMain filters) {
        this.filters = filters;
    }

    public ArrayList<SupplementaryOffer> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<SupplementaryOffer> offers) {
        this.offers = offers;
    }

    protected OfferCategory(Parcel in) {
        filters = in.readParcelable(OfferFiltersMain.class.getClassLoader());
        offers = in.createTypedArrayList(SupplementaryOffer.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(filters, flags);
        dest.writeTypedList(offers);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OfferCategory> CREATOR = new Creator<OfferCategory>() {
        @Override
        public OfferCategory createFromParcel(Parcel in) {
            return new OfferCategory(in);
        }

        @Override
        public OfferCategory[] newArray(int size) {
            return new OfferCategory[size];
        }
    };
}


