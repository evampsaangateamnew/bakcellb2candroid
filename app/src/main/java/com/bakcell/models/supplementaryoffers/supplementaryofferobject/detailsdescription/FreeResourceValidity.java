/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class FreeResourceValidity implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("titleValue")
    private String titleValue;
    @SerializedName("subTitle")
    private String subTitle;
    @SerializedName("subTitleValue")
    private String subTitleValue;
    @SerializedName("description")
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleValue() {
        return titleValue;
    }

    public void setTitleValue(String titleValue) {
        this.titleValue = titleValue;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSubTitleValue() {
        return subTitleValue;
    }

    public void setSubTitleValue(String subTitleValue) {
        this.subTitleValue = subTitleValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    protected FreeResourceValidity(Parcel in) {
        title = in.readString();
        titleValue = in.readString();
        subTitle = in.readString();
        subTitleValue = in.readString();
        description = in.readString();
    }

    public static final Creator<FreeResourceValidity> CREATOR = new Creator<FreeResourceValidity>() {
        @Override
        public FreeResourceValidity createFromParcel(Parcel in) {
            return new FreeResourceValidity(in);
        }

        @Override
        public FreeResourceValidity[] newArray(int size) {
            return new FreeResourceValidity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(titleValue);
        dest.writeString(subTitle);
        dest.writeString(subTitleValue);
        dest.writeString(description);
    }
}
