/**
 *
 */
package com.bakcell.models.supplementaryoffers.supplementaryofferobject.detailsdescription;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Noman
 */
public class Price implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private String value;
    @SerializedName("iconName")
    private String iconName;
    @SerializedName("offersCurrency")
    private String offersCurrency;
    @SerializedName("description")
    private String description;
    @SerializedName("attributeList")
    private ArrayList<DetailsAttributes> attributeList;

    protected Price(Parcel in) {
        title = in.readString();
        value = in.readString();
        iconName = in.readString();
        offersCurrency = in.readString();
        description = in.readString();
        attributeList = in.createTypedArrayList(DetailsAttributes.CREATOR);
    }

    public static final Creator<Price> CREATOR = new Creator<Price>() {
        @Override
        public Price createFromParcel(Parcel in) {
            return new Price(in);
        }

        @Override
        public Price[] newArray(int size) {
            return new Price[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOffersCurrency() {
        return offersCurrency;
    }

    public void setOffersCurrency(String offersCurrency) {
        this.offersCurrency = offersCurrency;
    }

    public ArrayList<DetailsAttributes> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(ArrayList<DetailsAttributes> attributeList) {
        this.attributeList = attributeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
        dest.writeString(iconName);
        dest.writeString(offersCurrency);
        dest.writeString(description);
        dest.writeTypedList(attributeList);
    }
}
