package com.bakcell.models.getscheduledpayments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScheduledPaymentData implements Parcelable {

    public static final Creator<ScheduledPaymentData> CREATOR = new Creator<ScheduledPaymentData>() {
        @Override
        public ScheduledPaymentData createFromParcel(Parcel in) {
            return new ScheduledPaymentData(in);
        }

        @Override
        public ScheduledPaymentData[] newArray(int size) {
            return new ScheduledPaymentData[size];
        }
    };
    @SerializedName("data")
    private List<ScheduledPaymentDataItem> data;

    protected ScheduledPaymentData(Parcel in) {
        data = in.createTypedArrayList(ScheduledPaymentDataItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<ScheduledPaymentDataItem> getData() {
        return data;
    }
}