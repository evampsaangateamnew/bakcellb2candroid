package com.bakcell.models.getscheduledpayments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ScheduledPaymentDataItem implements Parcelable {

    public static final Creator<ScheduledPaymentDataItem> CREATOR = new Creator<ScheduledPaymentDataItem>() {
        @Override
        public ScheduledPaymentDataItem createFromParcel(Parcel in) {
            return new ScheduledPaymentDataItem(in);
        }

        @Override
        public ScheduledPaymentDataItem[] newArray(int size) {
            return new ScheduledPaymentDataItem[size];
        }
    };
    @SerializedName("savedCardId")
    private String savedCardId;
    @SerializedName("recurrentDay")
    private String recurrentDay;
    @SerializedName("cardType")
    private String cardType;
    @SerializedName("recurrenceNumber")
    private String recurrenceNumber;
    @SerializedName("amount")
    private String amount;
    @SerializedName("id")
    private int id;
    @SerializedName("msisdn")
    private String msisdn;
    @SerializedName("nextScheduledDate")
    private String nextScheduledDate;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("billingCycle")
    private String billingCycle;

    protected ScheduledPaymentDataItem(Parcel in) {
        savedCardId = in.readString();
        recurrenceNumber = in.readString();
        amount = in.readString();
        id = in.readInt();
        msisdn = in.readString();
        nextScheduledDate = in.readString();
        startDate = in.readString();
        billingCycle = in.readString();
        recurrentDay = in.readString();
        cardType = in.readString();
    }

    public ScheduledPaymentDataItem() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(savedCardId);
        dest.writeString(recurrenceNumber);
        dest.writeString(amount);
        dest.writeInt(id);
        dest.writeString(msisdn);
        dest.writeString(nextScheduledDate);
        dest.writeString(startDate);
        dest.writeString(billingCycle);
        dest.writeString(recurrentDay);
        dest.writeString(cardType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getSavedCardId() {
        return savedCardId;
    }

    public String getRecurrenceNumber() {
        return recurrenceNumber;
    }

    public String getAmount() {
        return amount;
    }

    public int getId() {
        return id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getNextScheduledDate() {
        return nextScheduledDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getBillingCycle() {
        return billingCycle;
    }

    public String getRecurrentDay() {
        return recurrentDay;
    }

    public void setRecurrentDay(String recurrentDay) {
        this.recurrentDay = recurrentDay;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}