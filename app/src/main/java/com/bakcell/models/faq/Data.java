package com.bakcell.models.faq;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Freeware Sys on 25-Jul-17.
 */

public class Data implements Parcelable {

    @SerializedName("faqlist")
    ArrayList<FaqCategory> faqlist;

    public ArrayList<FaqCategory> getFaqlist() {
        return faqlist;
    }

    public void setFaqlist(ArrayList<FaqCategory> faqlist) {
        this.faqlist = faqlist;
    }

    protected Data(Parcel in) {
        faqlist = in.createTypedArrayList(FaqCategory.CREATOR);
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(faqlist);
    }
}
