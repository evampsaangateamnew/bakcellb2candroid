package com.bakcell.models.faq;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Freeware Sys on 25-Jul-17.
 */

public class FaqCategory implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("qalist")
    private ArrayList<Faqs> qalist;

    public FaqCategory() {

    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Faqs> getFaqlist() {
        return qalist;
    }

    public void setFaqlist(ArrayList<Faqs> faqlist) {
        this.qalist = faqlist;
    }

    protected FaqCategory(Parcel in) {
        title = in.readString();
        qalist = in.createTypedArrayList(Faqs.CREATOR);
    }

    public static final Creator<FaqCategory> CREATOR = new Creator<FaqCategory>() {
        @Override
        public FaqCategory createFromParcel(Parcel in) {
            return new FaqCategory(in);
        }

        @Override
        public FaqCategory[] newArray(int size) {
            return new FaqCategory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeTypedList(qalist);
    }
}
