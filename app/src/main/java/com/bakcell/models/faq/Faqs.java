package com.bakcell.models.faq;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Freeware Sys on 25-Jul-17.
 */

public class Faqs implements Parcelable {

    @SerializedName("question")
    private String question;
    @SerializedName("answer")
    private String answer;
    @SerializedName("sort_order")
    private String sort_order;


    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    protected Faqs(Parcel in) {
        question = in.readString();
        answer = in.readString();
        sort_order = in.readString();
    }

    public static final Creator<Faqs> CREATOR = new Creator<Faqs>() {
        @Override
        public Faqs createFromParcel(Parcel in) {
            return new Faqs(in);
        }

        @Override
        public Faqs[] newArray(int size) {
            return new Faqs[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeString(answer);
        dest.writeString(sort_order);
    }
}
