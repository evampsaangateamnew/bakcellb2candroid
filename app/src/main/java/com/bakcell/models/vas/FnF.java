package com.bakcell.models.vas;

/**
 * Created by Zeeshan Shabbir on 6/21/2017.
 */

public class FnF {
    String number;
    String date;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
