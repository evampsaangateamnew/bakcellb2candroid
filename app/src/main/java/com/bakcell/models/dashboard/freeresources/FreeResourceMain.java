package com.bakcell.models.dashboard.freeresources;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 29-Aug-17.
 */

public class FreeResourceMain implements Parcelable {

    @SerializedName("freeResources")
    private ArrayList<FreeResoures> freeResources;
    @SerializedName("freeResourcesRoaming")
    private ArrayList<FreeResoures> freeResourcesRoaming;

    public ArrayList<FreeResoures> getFreeResources() {
        return freeResources;
    }

    public void setFreeResources(ArrayList<FreeResoures> freeResources) {
        this.freeResources = freeResources;
    }

    public ArrayList<FreeResoures> getFreeResourcesRoaming() {
        return freeResourcesRoaming;
    }

    public void setFreeResourcesRoaming(ArrayList<FreeResoures> freeResourcesRoaming) {
        this.freeResourcesRoaming = freeResourcesRoaming;
    }

    protected FreeResourceMain(Parcel in) {
        freeResources = in.createTypedArrayList(FreeResoures.CREATOR);
        freeResourcesRoaming = in.createTypedArrayList(FreeResoures.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(freeResources);
        dest.writeTypedList(freeResourcesRoaming);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FreeResourceMain> CREATOR = new Creator<FreeResourceMain>() {
        @Override
        public FreeResourceMain createFromParcel(Parcel in) {
            return new FreeResourceMain(in);
        }

        @Override
        public FreeResourceMain[] newArray(int size) {
            return new FreeResourceMain[size];
        }
    };
}
