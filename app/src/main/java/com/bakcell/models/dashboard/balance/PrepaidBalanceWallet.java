package com.bakcell.models.dashboard.balance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class PrepaidBalanceWallet implements Parcelable {

    @SerializedName("amount")
    private String amount;
    @SerializedName("balanceTypeName")
    private String balanceTypeName;
    @SerializedName("currency")
    private String currency;
    @SerializedName("effectiveTime")
    private String effectiveTime;
    @SerializedName("expireTime")
    private String expireTime;
    @SerializedName("lowerLimit")
    private String lowerLimit;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalanceTypeName() {
        return balanceTypeName;
    }

    public void setBalanceTypeName(String balanceTypeName) {
        this.balanceTypeName = balanceTypeName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(String effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    protected PrepaidBalanceWallet(Parcel in) {
        amount = in.readString();
        balanceTypeName = in.readString();
        currency = in.readString();
        effectiveTime = in.readString();
        expireTime = in.readString();
        lowerLimit = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
        dest.writeString(balanceTypeName);
        dest.writeString(currency);
        dest.writeString(effectiveTime);
        dest.writeString(expireTime);
        dest.writeString(lowerLimit);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PrepaidBalanceWallet> CREATOR = new Creator<PrepaidBalanceWallet>() {
        @Override
        public PrepaidBalanceWallet createFromParcel(Parcel in) {
            return new PrepaidBalanceWallet(in);
        }

        @Override
        public PrepaidBalanceWallet[] newArray(int size) {
            return new PrepaidBalanceWallet[size];
        }
    };
}
