package com.bakcell.models.dashboard.balance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class BalancePrepaid implements Parcelable {

    @SerializedName("bounusWallet")
    private PrepaidBalanceWallet bounusWallet;
    @SerializedName("countryWideWallet")
    private PrepaidBalanceWallet countryWideWallet;
    @SerializedName("mainWallet")
    private PrepaidBalanceWallet mainWallet;

    public PrepaidBalanceWallet getBounusWallet() {
        return bounusWallet;
    }

    public void setBounusWallet(PrepaidBalanceWallet bounusWallet) {
        this.bounusWallet = bounusWallet;
    }

    public PrepaidBalanceWallet getCountryWideWallet() {
        return countryWideWallet;
    }

    public void setCountryWideWallet(PrepaidBalanceWallet countryWideWallet) {
        this.countryWideWallet = countryWideWallet;
    }

    public PrepaidBalanceWallet getMainWallet() {
        return mainWallet;
    }

    public void setMainWallet(PrepaidBalanceWallet mainWallet) {
        this.mainWallet = mainWallet;
    }

    protected BalancePrepaid(Parcel in) {
        bounusWallet = in.readParcelable(PrepaidBalanceWallet.class.getClassLoader());
        countryWideWallet = in.readParcelable(PrepaidBalanceWallet.class.getClassLoader());
        mainWallet = in.readParcelable(PrepaidBalanceWallet.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(bounusWallet, flags);
        dest.writeParcelable(countryWideWallet, flags);
        dest.writeParcelable(mainWallet, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BalancePrepaid> CREATOR = new Creator<BalancePrepaid>() {
        @Override
        public BalancePrepaid createFromParcel(Parcel in) {
            return new BalancePrepaid(in);
        }

        @Override
        public BalancePrepaid[] newArray(int size) {
            return new BalancePrepaid[size];
        }
    };
}
