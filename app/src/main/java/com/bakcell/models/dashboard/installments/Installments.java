package com.bakcell.models.dashboard.installments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class Installments implements Parcelable {

    @SerializedName("amountLabel")
    private String amountLabel;
    @SerializedName("amountValue")
    private String amountValue;
    @SerializedName("currency")
    private String currency;
    @SerializedName("installmentFeeLimit")
    private String installmentFeeLimit;
    @SerializedName("installmentStatus")
    private String installmentStatus;
    @SerializedName("name")
    private String name;
    @SerializedName("nextPaymentLabel")
    private String nextPaymentLabel;
    @SerializedName("nextPaymentValue")
    private String nextPaymentValue;
    @SerializedName("nextPaymentInitialDate")
    private String nextPaymentInitialDate;
    @SerializedName("purchaseDateLabel")
    private String purchaseDateLabel;
    @SerializedName("purchaseDateValue")
    private String purchaseDateValue;
    @SerializedName("remainingAmountCurrentValue")
    private String remainingAmountCurrentValue;
    @SerializedName("remainingAmountLabel")
    private String remainingAmountLabel;
    @SerializedName("remainingAmountTotalValue")
    private String remainingAmountTotalValue;
    @SerializedName("remainingCurrentPeriod")
    private String remainingCurrentPeriod;
    @SerializedName("remainingPeriodBeginDateLabel")
    private String remainingPeriodBeginDateLabel;
    @SerializedName("remainingPeriodBeginDateValue")
    private String remainingPeriodBeginDateValue;
    @SerializedName("remainingPeriodEndDateLabel")
    private String remainingPeriodEndDateLabel;
    @SerializedName("remainingPeriodEndDateValue")
    private String remainingPeriodEndDateValue;
    @SerializedName("remainingPeriodLabel")
    private String remainingPeriodLabel;
    @SerializedName("remainingTotalPeriod")
    private String remainingTotalPeriod;

    public String getAmountLabel() {
        return amountLabel;
    }

    public void setAmountLabel(String amountLabel) {
        this.amountLabel = amountLabel;
    }

    public String getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(String amountValue) {
        this.amountValue = amountValue;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInstallmentFeeLimit() {
        return installmentFeeLimit;
    }

    public void setInstallmentFeeLimit(String installmentFeeLimit) {
        this.installmentFeeLimit = installmentFeeLimit;
    }

    public String getInstallmentStatus() {
        return installmentStatus;
    }

    public void setInstallmentStatus(String installmentStatus) {
        this.installmentStatus = installmentStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNextPaymentLabel() {
        return nextPaymentLabel;
    }

    public void setNextPaymentLabel(String nextPaymentLabel) {
        this.nextPaymentLabel = nextPaymentLabel;
    }

    public String getNextPaymentValue() {
        return nextPaymentValue;
    }

    public void setNextPaymentValue(String nextPaymentValue) {
        this.nextPaymentValue = nextPaymentValue;
    }

    public String getNextPaymentInitialDate() {
        return nextPaymentInitialDate;
    }

    public void setNextPaymentInitialDate(String nextPaymentInitialDate) {
        this.nextPaymentInitialDate = nextPaymentInitialDate;
    }

    public String getPurchaseDateLabel() {
        return purchaseDateLabel;
    }

    public void setPurchaseDateLabel(String purchaseDateLabel) {
        this.purchaseDateLabel = purchaseDateLabel;
    }

    public String getPurchaseDateValue() {
        return purchaseDateValue;
    }

    public void setPurchaseDateValue(String purchaseDateValue) {
        this.purchaseDateValue = purchaseDateValue;
    }

    public String getRemainingAmountCurrentValue() {
        return remainingAmountCurrentValue;
    }

    public void setRemainingAmountCurrentValue(String remainingAmountCurrentValue) {
        this.remainingAmountCurrentValue = remainingAmountCurrentValue;
    }

    public String getRemainingAmountLabel() {
        return remainingAmountLabel;
    }

    public void setRemainingAmountLabel(String remainingAmountLabel) {
        this.remainingAmountLabel = remainingAmountLabel;
    }

    public String getRemainingAmountTotalValue() {
        return remainingAmountTotalValue;
    }

    public void setRemainingAmountTotalValue(String remainingAmountTotalValue) {
        this.remainingAmountTotalValue = remainingAmountTotalValue;
    }

    public String getRemainingCurrentPeriod() {
        return remainingCurrentPeriod;
    }

    public void setRemainingCurrentPeriod(String remainingCurrentPeriod) {
        this.remainingCurrentPeriod = remainingCurrentPeriod;
    }

    public String getRemainingPeriodBeginDateLabel() {
        return remainingPeriodBeginDateLabel;
    }

    public void setRemainingPeriodBeginDateLabel(String remainingPeriodBeginDateLabel) {
        this.remainingPeriodBeginDateLabel = remainingPeriodBeginDateLabel;
    }

    public String getRemainingPeriodBeginDateValue() {
        return remainingPeriodBeginDateValue;
    }

    public void setRemainingPeriodBeginDateValue(String remainingPeriodBeginDateValue) {
        this.remainingPeriodBeginDateValue = remainingPeriodBeginDateValue;
    }

    public String getRemainingPeriodEndDateLabel() {
        return remainingPeriodEndDateLabel;
    }

    public void setRemainingPeriodEndDateLabel(String remainingPeriodEndDateLabel) {
        this.remainingPeriodEndDateLabel = remainingPeriodEndDateLabel;
    }

    public String getRemainingPeriodEndDateValue() {
        return remainingPeriodEndDateValue;
    }

    public void setRemainingPeriodEndDateValue(String remainingPeriodEndDateValue) {
        this.remainingPeriodEndDateValue = remainingPeriodEndDateValue;
    }

    public String getRemainingPeriodLabel() {
        return remainingPeriodLabel;
    }

    public void setRemainingPeriodLabel(String remainingPeriodLabel) {
        this.remainingPeriodLabel = remainingPeriodLabel;
    }

    public String getRemainingTotalPeriod() {
        return remainingTotalPeriod;
    }

    public void setRemainingTotalPeriod(String remainingTotalPeriod) {
        this.remainingTotalPeriod = remainingTotalPeriod;
    }

    protected Installments(Parcel in) {
        amountLabel = in.readString();
        amountValue = in.readString();
        currency = in.readString();
        installmentFeeLimit = in.readString();
        installmentStatus = in.readString();
        name = in.readString();
        nextPaymentLabel = in.readString();
        nextPaymentValue = in.readString();
        nextPaymentInitialDate = in.readString();
        purchaseDateLabel = in.readString();
        purchaseDateValue = in.readString();
        remainingAmountCurrentValue = in.readString();
        remainingAmountLabel = in.readString();
        remainingAmountTotalValue = in.readString();
        remainingCurrentPeriod = in.readString();
        remainingPeriodBeginDateLabel = in.readString();
        remainingPeriodBeginDateValue = in.readString();
        remainingPeriodEndDateLabel = in.readString();
        remainingPeriodEndDateValue = in.readString();
        remainingPeriodLabel = in.readString();
        remainingTotalPeriod = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amountLabel);
        dest.writeString(amountValue);
        dest.writeString(currency);
        dest.writeString(installmentFeeLimit);
        dest.writeString(installmentStatus);
        dest.writeString(name);
        dest.writeString(nextPaymentLabel);
        dest.writeString(nextPaymentValue);
        dest.writeString(nextPaymentInitialDate);
        dest.writeString(purchaseDateLabel);
        dest.writeString(purchaseDateValue);
        dest.writeString(remainingAmountCurrentValue);
        dest.writeString(remainingAmountLabel);
        dest.writeString(remainingAmountTotalValue);
        dest.writeString(remainingCurrentPeriod);
        dest.writeString(remainingPeriodBeginDateLabel);
        dest.writeString(remainingPeriodBeginDateValue);
        dest.writeString(remainingPeriodEndDateLabel);
        dest.writeString(remainingPeriodEndDateValue);
        dest.writeString(remainingPeriodLabel);
        dest.writeString(remainingTotalPeriod);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Installments> CREATOR = new Creator<Installments>() {
        @Override
        public Installments createFromParcel(Parcel in) {
            return new Installments(in);
        }

        @Override
        public Installments[] newArray(int size) {
            return new Installments[size];
        }
    };
}
