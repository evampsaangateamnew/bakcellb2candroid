package com.bakcell.models.dashboard.balance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class BalancePostpaid implements Parcelable {

    @SerializedName("availableBalanceCorporateValue")
    private String availableBalanceCorporateValue;
    @SerializedName("availableBalanceIndividualValue")
    private String availableBalanceIndividualValue;
    @SerializedName("availableCreditLabel")
    private String availableCreditLabel;
    @SerializedName("balanceCorporateValue")
    private String balanceCorporateValue;
    @SerializedName("balanceIndividualValue")
    private String balanceIndividualValue;
    @SerializedName("balanceLabel")
    private String balanceLabel;
    @SerializedName("corporateLabel")
    private String corporateLabel;
    @SerializedName("currentCreditCorporateValue")
    private String currentCreditCorporateValue;
    @SerializedName("currentCreditIndividualValue")
    private String currentCreditIndividualValue;
    @SerializedName("currentCreditLabel")
    private String currentCreditLabel;
    @SerializedName("individualLabel")
    private String individualLabel;
    @SerializedName("outstandingIndividualDebt")
    private String outstandingIndividualDept;
    @SerializedName("outstandingIndividualDebtLabel")
    private String outstandingIndividualDeptLabel;
    @SerializedName("template")
    private String template;

    public String getAvailableBalanceCorporateValue() {
        return availableBalanceCorporateValue;
    }

    public void setAvailableBalanceCorporateValue(String availableBalanceCorporateValue) {
        this.availableBalanceCorporateValue = availableBalanceCorporateValue;
    }

    public String getAvailableBalanceIndividualValue() {
        return availableBalanceIndividualValue;
    }

    public void setAvailableBalanceIndividualValue(String availableBalanceIndividualValue) {
        this.availableBalanceIndividualValue = availableBalanceIndividualValue;
    }

    public String getAvailableCreditLabel() {
        return availableCreditLabel;
    }

    public void setAvailableCreditLabel(String availableCreditLabel) {
        this.availableCreditLabel = availableCreditLabel;
    }

    public String getBalanceCorporateValue() {
        return balanceCorporateValue;
    }

    public void setBalanceCorporateValue(String balanceCorporateValue) {
        this.balanceCorporateValue = balanceCorporateValue;
    }

    public String getBalanceIndividualValue() {
        return balanceIndividualValue;
    }

    public void setBalanceIndividualValue(String balanceIndividualValue) {
        this.balanceIndividualValue = balanceIndividualValue;
    }

    public String getBalanceLabel() {
        return balanceLabel;
    }

    public void setBalanceLabel(String balanceLabel) {
        this.balanceLabel = balanceLabel;
    }

    public String getCorporateLabel() {
        return corporateLabel;
    }

    public void setCorporateLabel(String corporateLabel) {
        this.corporateLabel = corporateLabel;
    }

    public String getCurrentCreditCorporateValue() {
        return currentCreditCorporateValue;
    }

    public void setCurrentCreditCorporateValue(String currentCreditCorporateValue) {
        this.currentCreditCorporateValue = currentCreditCorporateValue;
    }

    public String getCurrentCreditIndividualValue() {
        return currentCreditIndividualValue;
    }

    public void setCurrentCreditIndividualValue(String currentCreditIndividualValue) {
        this.currentCreditIndividualValue = currentCreditIndividualValue;
    }

    public String getCurrentCreditLabel() {
        return currentCreditLabel;
    }

    public void setCurrentCreditLabel(String currentCreditLabel) {
        this.currentCreditLabel = currentCreditLabel;
    }

    public String getIndividualLabel() {
        return individualLabel;
    }

    public void setIndividualLabel(String individualLabel) {
        this.individualLabel = individualLabel;
    }

    public String getOutstandingIndividualDept() {
        return outstandingIndividualDept;
    }

    public void setOutstandingIndividualDept(String outstandingIndividualDept) {
        this.outstandingIndividualDept = outstandingIndividualDept;
    }

    public String getOutstandingIndividualDeptLabel() {
        return outstandingIndividualDeptLabel;
    }

    public void setOutstandingIndividualDeptLabel(String outstandingIndividualDeptLabel) {
        this.outstandingIndividualDeptLabel = outstandingIndividualDeptLabel;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    protected BalancePostpaid(Parcel in) {
        availableBalanceCorporateValue = in.readString();
        availableBalanceIndividualValue = in.readString();
        availableCreditLabel = in.readString();
        balanceCorporateValue = in.readString();
        balanceIndividualValue = in.readString();
        balanceLabel = in.readString();
        corporateLabel = in.readString();
        currentCreditCorporateValue = in.readString();
        currentCreditIndividualValue = in.readString();
        currentCreditLabel = in.readString();
        individualLabel = in.readString();
        outstandingIndividualDept = in.readString();
        outstandingIndividualDeptLabel = in.readString();
        template = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(availableBalanceCorporateValue);
        dest.writeString(availableBalanceIndividualValue);
        dest.writeString(availableCreditLabel);
        dest.writeString(balanceCorporateValue);
        dest.writeString(balanceIndividualValue);
        dest.writeString(balanceLabel);
        dest.writeString(corporateLabel);
        dest.writeString(currentCreditCorporateValue);
        dest.writeString(currentCreditIndividualValue);
        dest.writeString(currentCreditLabel);
        dest.writeString(individualLabel);
        dest.writeString(outstandingIndividualDept);
        dest.writeString(outstandingIndividualDeptLabel);
        dest.writeString(template);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BalancePostpaid> CREATOR = new Creator<BalancePostpaid>() {
        @Override
        public BalancePostpaid createFromParcel(Parcel in) {
            return new BalancePostpaid(in);
        }

        @Override
        public BalancePostpaid[] newArray(int size) {
            return new BalancePostpaid[size];
        }
    };
}
