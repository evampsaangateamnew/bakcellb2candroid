package com.bakcell.models.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.dashboard.balance.BalanceMain;
import com.bakcell.models.dashboard.freeresources.FreeResourceMain;
import com.bakcell.models.dashboard.installments.InstallmentsMain;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class DashboardMain implements Parcelable {

    @SerializedName("balance")
    private BalanceMain balance;
    @SerializedName("installments")
    private InstallmentsMain installments;
    @SerializedName("mrc")
    private Mrc mrc;
    @SerializedName("credit")
    private Credit credit;
    @SerializedName("freeResources")
    private FreeResourceMain freeResources;

    public BalanceMain getBalance() {
        return balance;
    }

    public void setBalance(BalanceMain balance) {
        this.balance = balance;
    }

    public InstallmentsMain getInstallments() {
        return installments;
    }

    public void setInstallments(InstallmentsMain installments) {
        this.installments = installments;
    }

    public Mrc getMrc() {
        return mrc;
    }

    public void setMrc(Mrc mrc) {
        this.mrc = mrc;
    }

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    public FreeResourceMain getFreeResources() {
        return freeResources;
    }

    public void setFreeResources(FreeResourceMain freeResources) {
        this.freeResources = freeResources;
    }

    protected DashboardMain(Parcel in) {
        balance = in.readParcelable(BalanceMain.class.getClassLoader());
        installments = in.readParcelable(InstallmentsMain.class.getClassLoader());
        mrc = in.readParcelable(Mrc.class.getClassLoader());
        credit = in.readParcelable(Credit.class.getClassLoader());
        freeResources = in.readParcelable(FreeResourceMain.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(balance, flags);
        dest.writeParcelable(installments, flags);
        dest.writeParcelable(mrc, flags);
        dest.writeParcelable(credit, flags);
        dest.writeParcelable(freeResources, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DashboardMain> CREATOR = new Creator<DashboardMain>() {
        @Override
        public DashboardMain createFromParcel(Parcel in) {
            return new DashboardMain(in);
        }

        @Override
        public DashboardMain[] newArray(int size) {
            return new DashboardMain[size];
        }
    };
}
