package com.bakcell.models.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class Mrc implements Parcelable {

    @SerializedName("mrcCurrency")
    private String mrcCurrency;
    @SerializedName("mrcDate")
    private String mrcDate;
    @SerializedName("mrcDateLabel")
    private String mrcDateLabel;
    @SerializedName("mrcInitialDate")
    private String mrcInitialDate;
    @SerializedName("mrcLimit")
    private String mrcLimit;
    @SerializedName("mrcTitleLabel")
    private String mrcTitleLabel;
    @SerializedName("mrcTitleValue")
    private String mrcTitleValue;
    @SerializedName("mrcType")
    private String mrcType;
    @SerializedName("mrcStatus")
    private String mrcStatus;

    protected Mrc(Parcel in) {
        mrcCurrency = in.readString();
        mrcDate = in.readString();
        mrcDateLabel = in.readString();
        mrcInitialDate = in.readString();
        mrcLimit = in.readString();
        mrcTitleLabel = in.readString();
        mrcTitleValue = in.readString();
        mrcType = in.readString();
        mrcStatus = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mrcCurrency);
        dest.writeString(mrcDate);
        dest.writeString(mrcDateLabel);
        dest.writeString(mrcInitialDate);
        dest.writeString(mrcLimit);
        dest.writeString(mrcTitleLabel);
        dest.writeString(mrcTitleValue);
        dest.writeString(mrcType);
        dest.writeString(mrcStatus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Mrc> CREATOR = new Creator<Mrc>() {
        @Override
        public Mrc createFromParcel(Parcel in) {
            return new Mrc(in);
        }

        @Override
        public Mrc[] newArray(int size) {
            return new Mrc[size];
        }
    };

    public String getMrcStatus() {
        return mrcStatus;
    }

    public void setMrcStatus(String mrcStatus) {
        this.mrcStatus = mrcStatus;
    }

    public String getMrcCurrency() {
        return mrcCurrency;
    }

    public void setMrcCurrency(String mrcCurrency) {
        this.mrcCurrency = mrcCurrency;
    }

    public String getMrcDate() {
        return mrcDate;
    }

    public void setMrcDate(String mrcDate) {
        this.mrcDate = mrcDate;
    }

    public String getMrcDateLabel() {
        return mrcDateLabel;
    }

    public void setMrcDateLabel(String mrcDateLabel) {
        this.mrcDateLabel = mrcDateLabel;
    }

    public String getMrcInitialDate() {
        return mrcInitialDate;
    }

    public void setMrcInitialDate(String mrcInitialDate) {
        this.mrcInitialDate = mrcInitialDate;
    }

    public String getMrcLimit() {
        return mrcLimit;
    }

    public void setMrcLimit(String mrcLimit) {
        this.mrcLimit = mrcLimit;
    }

    public String getMrcTitleLabel() {
        return mrcTitleLabel;
    }

    public void setMrcTitleLabel(String mrcTitleLabel) {
        this.mrcTitleLabel = mrcTitleLabel;
    }

    public String getMrcTitleValue() {
        return mrcTitleValue;
    }

    public void setMrcTitleValue(String mrcTitleValue) {
        this.mrcTitleValue = mrcTitleValue;
    }


    public String getMrcType() {
        return mrcType;
    }

    public void setMrcType(String mrcType) {
        this.mrcType = mrcType;
    }
}
