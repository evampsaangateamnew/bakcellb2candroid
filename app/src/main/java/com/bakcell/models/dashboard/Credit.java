package com.bakcell.models.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class Credit implements Parcelable {

    @SerializedName("creditCurrency")
    private String creditCurrency;
    @SerializedName("creditDate")
    private String creditDate;
    @SerializedName("creditInitialDate")
    private String creditInitialDate;
    @SerializedName("creditDateLabel")
    private String creditDateLabel;
    @SerializedName("creditLimit")
    private String creditLimit;
    @SerializedName("creditTitleLabel")
    private String creditTitleLabel;
    @SerializedName("creditTitleValue")
    private String creditTitleValue;
    @SerializedName("creditDays")
    private String creditDays;
    @SerializedName("remainingCreditDays")
    private String remainingCreditDays;
    @SerializedName("progressDays")
    private String progressDays;


    public String getRemainingCreditDays() {
        return remainingCreditDays;
    }

    public void setRemainingCreditDays(String remainingCreditDays) {
        this.remainingCreditDays = remainingCreditDays;
    }

    public String getProgressDays() {
        return progressDays;
    }

    public void setProgressDays(String progressDays) {
        this.progressDays = progressDays;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditCurrency() {
        return creditCurrency;
    }

    public void setCreditCurrency(String creditCurrency) {
        this.creditCurrency = creditCurrency;
    }

    public String getCreditDate() {
        return creditDate;
    }

    public void setCreditDate(String creditDate) {
        this.creditDate = creditDate;
    }

    public String getCreditInitialDate() {
        return creditInitialDate;
    }

    public void setCreditInitialDate(String creditInitialDate) {
        this.creditInitialDate = creditInitialDate;
    }

    public String getCreditDateLabel() {
        return creditDateLabel;
    }

    public void setCreditDateLabel(String creditDateLabel) {
        this.creditDateLabel = creditDateLabel;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCreditTitleLabel() {
        return creditTitleLabel;
    }

    public void setCreditTitleLabel(String creditTitleLabel) {
        this.creditTitleLabel = creditTitleLabel;
    }

    public String getCreditTitleValue() {
        return creditTitleValue;
    }

    public void setCreditTitleValue(String creditTitleValue) {
        this.creditTitleValue = creditTitleValue;
    }

    protected Credit(Parcel in) {
        creditCurrency = in.readString();
        creditDate = in.readString();
        creditInitialDate = in.readString();
        creditDateLabel = in.readString();
        creditLimit = in.readString();
        creditTitleLabel = in.readString();
        creditTitleValue = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(creditCurrency);
        dest.writeString(creditDate);
        dest.writeString(creditInitialDate);
        dest.writeString(creditDateLabel);
        dest.writeString(creditLimit);
        dest.writeString(creditTitleLabel);
        dest.writeString(creditTitleValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Credit> CREATOR = new Creator<Credit>() {
        @Override
        public Credit createFromParcel(Parcel in) {
            return new Credit(in);
        }

        @Override
        public Credit[] newArray(int size) {
            return new Credit[size];
        }
    };
}
