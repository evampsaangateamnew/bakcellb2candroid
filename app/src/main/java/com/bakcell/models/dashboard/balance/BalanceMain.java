package com.bakcell.models.dashboard.balance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class BalanceMain implements Parcelable {

    @SerializedName("prepaid")
    private BalancePrepaid prepaid;
    @SerializedName("postpaid")
    private BalancePostpaid postpaid;
    @SerializedName("minAmountTeBePaid")
    private String minAmountTeBePaid;
    @SerializedName("minAmountLabel")
    private String minAmountLabel;

    public String getMinAmountTeBePaid() {
        return minAmountTeBePaid;
    }

    public void setMinAmountTeBePaid(String minAmountTeBePaid) {
        this.minAmountTeBePaid = minAmountTeBePaid;
    }

    public String getMinAmountLabel() {
        return minAmountLabel;
    }

    public void setMinAmountLabel(String minAmountLabel) {
        this.minAmountLabel = minAmountLabel;
    }

    public BalancePrepaid getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(BalancePrepaid prepaid) {
        this.prepaid = prepaid;
    }

    public BalancePostpaid getPostpaid() {
        return postpaid;
    }

    public void setPostpaid(BalancePostpaid postpaid) {
        this.postpaid = postpaid;
    }

    protected BalanceMain(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BalanceMain> CREATOR = new Creator<BalanceMain>() {
        @Override
        public BalanceMain createFromParcel(Parcel in) {
            return new BalanceMain(in);
        }

        @Override
        public BalanceMain[] newArray(int size) {
            return new BalanceMain[size];
        }
    };
}
