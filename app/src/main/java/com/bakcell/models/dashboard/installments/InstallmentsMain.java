package com.bakcell.models.dashboard.installments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 29-Aug-17.
 */

public class InstallmentsMain implements Parcelable {

    @SerializedName("installmentDescription")
    private String installmentDescription;
    @SerializedName("installmentTitle")
    private String installmentTitle;
    @SerializedName("installments")
    private ArrayList<Installments> installments;

    public String getInstallmentDescription() {
        return installmentDescription;
    }

    public void setInstallmentDescription(String installmentDescription) {
        this.installmentDescription = installmentDescription;
    }

    public String getInstallmentTitle() {
        return installmentTitle;
    }

    public void setInstallmentTitle(String installmentTitle) {
        this.installmentTitle = installmentTitle;
    }

    public ArrayList<Installments> getInstallmentes() {
        return installments;
    }

    public void setInstallmentes(ArrayList<Installments> installmentes) {
        this.installments = installmentes;
    }

    protected InstallmentsMain(Parcel in) {
        installmentDescription = in.readString();
        installmentTitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(installmentDescription);
        dest.writeString(installmentTitle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InstallmentsMain> CREATOR = new Creator<InstallmentsMain>() {
        @Override
        public InstallmentsMain createFromParcel(Parcel in) {
            return new InstallmentsMain(in);
        }

        @Override
        public InstallmentsMain[] newArray(int size) {
            return new InstallmentsMain[size];
        }
    };
}
