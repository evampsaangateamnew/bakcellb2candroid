package com.bakcell.models.dashboard.freeresources;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Aug-17.
 */

public class FreeResoures implements Parcelable {

    @SerializedName("resourceDiscountedText")
    private String resourceDiscountedText;
    @SerializedName("resourceInitialUnits")
    private String resourceInitialUnits;
    @SerializedName("resourceRemainingUnits")
    private String resourceRemainingUnits;
    @SerializedName("resourcesTitleLabel")
    private String resourcesTitleLabel;
    @SerializedName("resourceType")
    private String resourceType;
    @SerializedName("resourceUnitName")
    private String resourceUnitName;
    @SerializedName("remainingFormatted")
    private String remainingFormatted;

    public FreeResoures() {
    }


    protected FreeResoures(Parcel in) {
        resourceDiscountedText = in.readString();
        resourceInitialUnits = in.readString();
        resourceRemainingUnits = in.readString();
        resourcesTitleLabel = in.readString();
        resourceType = in.readString();
        resourceUnitName = in.readString();
        remainingFormatted = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(resourceDiscountedText);
        dest.writeString(resourceInitialUnits);
        dest.writeString(resourceRemainingUnits);
        dest.writeString(resourcesTitleLabel);
        dest.writeString(resourceType);
        dest.writeString(resourceUnitName);
        dest.writeString(remainingFormatted);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FreeResoures> CREATOR = new Creator<FreeResoures>() {
        @Override
        public FreeResoures createFromParcel(Parcel in) {
            return new FreeResoures(in);
        }

        @Override
        public FreeResoures[] newArray(int size) {
            return new FreeResoures[size];
        }
    };

    public String getResourceDiscountedText() {
        return resourceDiscountedText;
    }

    public void setResourceDiscountedText(String resourceDiscountedText) {
        this.resourceDiscountedText = resourceDiscountedText;
    }

    public String getResourceInitialUnits() {
        return resourceInitialUnits;
    }

    public void setResourceInitialUnits(String resourceInitialUnits) {
        this.resourceInitialUnits = resourceInitialUnits;
    }

    public String getResourceRemainingUnits() {
        return resourceRemainingUnits;
    }

    public void setResourceRemainingUnits(String resourceRemainingUnits) {
        this.resourceRemainingUnits = resourceRemainingUnits;
    }

    public String getResourcesTitleLabel() {
        return resourcesTitleLabel;
    }

    public void setResourcesTitleLabel(String resourcesTitleLabel) {
        this.resourcesTitleLabel = resourcesTitleLabel;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceUnitName() {
        return resourceUnitName;
    }

    public void setResourceUnitName(String resourceUnitName) {
        this.resourceUnitName = resourceUnitName;
    }

    public String getRemainingFormatted() {
        return remainingFormatted;
    }

    public void setRemainingFormatted(String remainingFormatted) {
        this.remainingFormatted = remainingFormatted;
    }
}
