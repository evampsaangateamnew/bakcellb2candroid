package com.bakcell.models.quickservices;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 10/6/2017.
 */

public class FreeSms implements Parcelable {
    @SerializedName("onNetSMS")
    String onNetSMS;
    @SerializedName("offNetSMS")
    String offNetSMS;

    @SerializedName("smsSent")
    boolean smsSent;

    protected FreeSms(Parcel in) {
        onNetSMS = in.readString();
        offNetSMS = in.readString();
        smsSent = in.readByte() != 0;
    }

    public static final Creator<FreeSms> CREATOR = new Creator<FreeSms>() {
        @Override
        public FreeSms createFromParcel(Parcel in) {
            return new FreeSms(in);
        }

        @Override
        public FreeSms[] newArray(int size) {
            return new FreeSms[size];
        }
    };

    public String getOnNetSMS() {
        return onNetSMS;
    }

    public void setOnNetSMS(String onNetSMS) {
        this.onNetSMS = onNetSMS;
    }

    public String getOffNetSMS() {
        return offNetSMS;
    }

    public void setOffNetSMS(String offNetSMS) {
        this.offNetSMS = offNetSMS;
    }

    public boolean isSmsSent() {
        return smsSent;
    }

    public void setSmsSent(boolean smsSent) {
        this.smsSent = smsSent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(onNetSMS);
        dest.writeString(offNetSMS);
        dest.writeByte((byte) (smsSent ? 1 : 0));
    }
}
