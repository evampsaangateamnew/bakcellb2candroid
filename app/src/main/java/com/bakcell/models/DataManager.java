package com.bakcell.models;

import android.content.Context;

import com.bakcell.globals.Constants;
import com.bakcell.models.dashboard.balance.BalanceMain;
import com.bakcell.models.notifications.SettingsNotifications;
import com.bakcell.models.supplementaryoffers.SupplementaryOfferMain;
import com.bakcell.models.user.UserMain;
import com.bakcell.models.user.UserModel;
import com.bakcell.models.user.UserPredefinedData;
import com.bakcell.models.user.UserPrimaryOffering;
import com.bakcell.models.user.UserSupplementaryOffering;
import com.bakcell.utilities.Logger;
import com.bakcell.utilities.PrefUtils;
import com.bakcell.utilities.Tools;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Noman on 7/16/2017.
 */
public class DataManager {

    private static DataManager instance = null;

    // Customer Data fields
    private UserModel currentUser = UserModel.getInstance();
    private UserPrimaryOffering primaryOfferings = UserPrimaryOffering.getInstance();
    private ArrayList<UserSupplementaryOffering> userSupplementaryOfferingsList = new ArrayList<>();
    private UserPredefinedData userPredefinedData = UserPredefinedData.getInstance();
    private SupplementaryOfferMain supplementaryOfferMain=null;
    private String promoMessage;

    // Balance Data Fields
    private BalanceMain balanceMain;

    private DataManager() {
    }

    public static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    public String getPromoMessage() {
        return promoMessage;
    }

    public void setPromoMessage(String promoMessage) {
        this.promoMessage = promoMessage;
    }

    /**
     * Set Current User Data
     *
     * @param currentUser
     */
    public void setCurrentUser(UserModel currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * Get Current User Data
     *
     * @return
     */
    public UserModel getCurrentUser() {
        return this.currentUser;
    }

    /**
     * save supplementaryOfferMain for flag usecase
     *
     * @param supplementaryOfferMain
     */
    public void setSupplementaryOfferMainData(SupplementaryOfferMain supplementaryOfferMain)
    {
    this.supplementaryOfferMain = supplementaryOfferMain;
    }

    public SupplementaryOfferMain  getSupplementaryOfferMainData()
    {
        return supplementaryOfferMain;
    }



    /**
     * getString current primary offering (Activated Package) for user
     *
     * @return
     */
    public UserPrimaryOffering getUserPrimaryOffering() {
        return primaryOfferings;
    }

    /**
     * set current primary offering (Activated Package) for user
     *
     * @param primaryOfferings
     */
    public void setUserPrimaryOffering(UserPrimaryOffering primaryOfferings) {
        this.primaryOfferings = primaryOfferings;
    }


    /**
     * getString Currently subscribed offer's list for currrent User
     *
     * @return
     */
    public ArrayList<UserSupplementaryOffering> getUserSupplementaryOfferingsList() {
        return userSupplementaryOfferingsList;
    }


    /**
     * set Currently subscribed offer's list for currrent User
     *
     * @return
     */
    public void setUserSupplementaryOfferingsList(ArrayList<UserSupplementaryOffering> userSupplementaryOfferingsList) {
        this.userSupplementaryOfferingsList = userSupplementaryOfferingsList;
    }


    /**
     * Get User Balance
     *
     * @return
     */
    public BalanceMain getBalanceMain() {
        return balanceMain;
    }

    /**
     * Set User Balance
     *
     * @param balanceMain
     */
    public void setBalanceMain(BalanceMain balanceMain) {
        this.balanceMain = balanceMain;
    }

    /**
     * getString Prepaid, Postpaid Current Balance
     */
    public double getCurrentBalance() {
        double balance = 0;
        try {
            if (DataManager.getInstance().getCurrentUser() != null && DataManager.getInstance().getCurrentUser().getSubscriberType().equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)) {
                if (balanceMain != null && balanceMain.getPrepaid() != null &&
                        balanceMain.getPrepaid().getMainWallet() != null) {
                    if (Tools.isNumeric(balanceMain.getPrepaid().getMainWallet().getAmount())) {
                        balance = Tools.getDoubleFromString(balanceMain.getPrepaid().getMainWallet().getAmount());
                    }
                }
            } else {
                if (balanceMain != null && balanceMain.getPostpaid() != null &&
                        balanceMain.getPostpaid().getBalanceIndividualValue() != null) {
                    if (Tools.isNumeric(balanceMain.getPostpaid().getBalanceIndividualValue())) {
                        balance = Tools.getDoubleFromString(balanceMain.getPostpaid().getBalanceIndividualValue());
                    }
                }
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return balance;
    }

    /**
     * update balance of prepaid customer
     *
     * @param balance
     */
    public void setNewBalance(String balance) {
        // Note: We are updating cache for balance just update session data
        if (DataManager.getInstance().getCurrentUser() != null &&
                DataManager.getInstance().getCurrentUser().getSubscriberType()
                        .equalsIgnoreCase(Constants.UserCurrentKeyword.SUBSCRIBER_TYPE_PREPAID)) {
            if (balanceMain != null && balanceMain.getPrepaid() != null &&
                    balanceMain.getPrepaid().getMainWallet() != null) {
                balanceMain.getPrepaid().getMainWallet().setAmount(balance);
            }
        }
    }


    /**
     * getString user's predefined data
     *
     * @return
     */
    public UserPredefinedData getUserPredefinedData() {
        return userPredefinedData;
    }

    /**
     * set user's predeined data
     *
     * @param userPredefinedData
     */
    public void setUserPredefinedData(UserPredefinedData userPredefinedData) {
        this.userPredefinedData = userPredefinedData;
    }


    /**
     * Update Email in customer info
     *
     * @param email
     */
    public void updateCurrentUserEmail(Context context, String email) {
        if (!Tools.hasValue(email)) return;
        try {
            UserMain userMain = PrefUtils.getCustomerData(context);
            if (userMain != null && userMain.getCustomerData() != null) {
                userMain.getCustomerData().setEmail(email);
                DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
                PrefUtils.addCustomerData(context, userMain);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Update Profile Image in customer info
     *
     * @param profileImage
     */
    public void updateCurrentUserProfileImage(Context context, String profileImage) {
        if (!Tools.hasValue(profileImage)) return;
        try {
            UserMain userMain = PrefUtils.getCustomerData(context);
            if (userMain != null && userMain.getCustomerData() != null) {
                userMain.getCustomerData().setImageURL(profileImage);
                DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
                PrefUtils.addCustomerData(context, userMain);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Update billing Language  in customer info
     *
     * @param billingLanguage
     */
    public void updateCurrentUserBillingLanguage(Context context, String billingLanguage) {
        if (!Tools.hasValue(billingLanguage)) return;
        try {
            UserMain userMain = PrefUtils.getCustomerData(context);
            if (userMain != null && userMain.getCustomerData() != null) {
                userMain.getCustomerData().setLanguage(billingLanguage);
                DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
                PrefUtils.addCustomerData(context, userMain);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Update Token  in customer info
     *
     * @param newToken
     */
    public void updateCurrentUserToken(Context context, String newToken) {
        if (!Tools.hasValue(newToken)) return;
        try {
            UserMain userMain = PrefUtils.getCustomerData(context);
            if (userMain != null && userMain.getCustomerData() != null) {
                userMain.getCustomerData().setToken(newToken);
                DataManager.getInstance().setCurrentUser(userMain.getCustomerData());
                PrefUtils.addCustomerData(context, userMain);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * Save Notification settings data to shared preferences after Add FCM token added
     *
     * @param context
     * @param settingsNotifications
     */
    public void saveNotificationSettingsData(Context context, SettingsNotifications settingsNotifications) {
        try {
            if (context != null && settingsNotifications != null) {
                String notiData = new Gson().toJson(settingsNotifications);
                PrefUtils.addString(context, PrefUtils.PreKeywords.PREF_SETTINGS_NOTIFICATIONS, notiData);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
    }

    /**
     * get Notification settings data form shared preferences
     *
     * @param context
     */
    public SettingsNotifications getNotificationSettingsData(Context context) {
        try {
            if (context != null) {
                return PrefUtils.getAsJson(context, PrefUtils.PreKeywords.PREF_SETTINGS_NOTIFICATIONS, SettingsNotifications.class);
            }
        } catch (Exception e) {
            Logger.debugLog(Logger.TAG_CATCH_LOGS, e.getMessage());
        }
        return null;
    }

    private Boolean swipeDisableCheck=false;
    public void setSwipeDisableCheckForOperators(boolean enable) {
        swipeDisableCheck = enable;
    }

    public Boolean getSwipeDisableCheckForOperators() {
        return swipeDisableCheck;
    }
}
