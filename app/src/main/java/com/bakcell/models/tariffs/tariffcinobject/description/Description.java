package com.bakcell.models.tariffs.tariffcinobject.description;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class Description implements Parcelable {
    @SerializedName("advantages")
    DescriptionItem advantages;
    @SerializedName("classification")
    DescriptionItem classification;
    @SerializedName("descLabel")
    String descLabel;

    protected Description(Parcel in) {
        advantages = in.readParcelable(DescriptionItem.class.getClassLoader());
        classification = in.readParcelable(DescriptionItem.class.getClassLoader());
        descLabel = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(advantages, flags);
        dest.writeParcelable(classification, flags);
        dest.writeString(descLabel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Description> CREATOR = new Creator<Description>() {
        @Override
        public Description createFromParcel(Parcel in) {
            return new Description(in);
        }

        @Override
        public Description[] newArray(int size) {
            return new Description[size];
        }
    };

    public DescriptionItem getAdvantages() {
        return advantages;
    }

    public void setAdvantages(DescriptionItem advantages) {
        this.advantages = advantages;
    }

    public DescriptionItem getClassification() {
        return classification;
    }

    public void setClassification(DescriptionItem classification) {
        this.classification = classification;
    }

    public String getDescLabel() {
        return descLabel;
    }

    public void setDescLabel(String descLabel) {
        this.descLabel = descLabel;
    }
}
