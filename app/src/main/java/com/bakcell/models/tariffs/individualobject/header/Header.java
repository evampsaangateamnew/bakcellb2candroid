package com.bakcell.models.tariffs.individualobject.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePriceItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/4/2017.
 */

public class Header implements Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("offeringId")
    private String offeringId;
    @SerializedName("name")
    String name;
    @SerializedName("priceLabel")
    String priceLabel;
    @SerializedName("priceValue")
    String priceValue;
    @SerializedName("subscribable")
    String subscribable;
    @SerializedName("call")
    private PackagePriceItem call;
    @SerializedName("sms")
    private PackagePriceItem sms;
    @SerializedName("internet")
    private PackagePriceItem internet;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceLabel() {
        return priceLabel;
    }

    public void setPriceLabel(String priceLabel) {
        this.priceLabel = priceLabel;
    }

    public String getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(String priceValue) {
        this.priceValue = priceValue;
    }

    public String getSubscribable() {
        return subscribable;
    }

    public void setSubscribable(String subscribable) {
        this.subscribable = subscribable;
    }

    public PackagePriceItem getCall() {
        return call;
    }

    public void setCall(PackagePriceItem call) {
        this.call = call;
    }

    public PackagePriceItem getSms() {
        return sms;
    }

    public void setSms(PackagePriceItem sms) {
        this.sms = sms;
    }

    public PackagePriceItem getInternet() {
        return internet;
    }

    public void setInternet(PackagePriceItem internet) {
        this.internet = internet;
    }

    protected Header(Parcel in) {
        id = in.readString();
        offeringId = in.readString();
        name = in.readString();
        priceLabel = in.readString();
        priceValue = in.readString();
        subscribable = in.readString();
        call = in.readParcelable(PackagePriceItem.class.getClassLoader());
        sms = in.readParcelable(PackagePriceItem.class.getClassLoader());
        internet = in.readParcelable(PackagePriceItem.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(offeringId);
        dest.writeString(name);
        dest.writeString(priceLabel);
        dest.writeString(priceValue);
        dest.writeString(subscribable);
        dest.writeParcelable(call, flags);
        dest.writeParcelable(sms, flags);
        dest.writeParcelable(internet, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Header> CREATOR = new Creator<Header>() {
        @Override
        public Header createFromParcel(Parcel in) {
            return new Header(in);
        }

        @Override
        public Header[] newArray(int size) {
            return new Header[size];
        }
    };
}
