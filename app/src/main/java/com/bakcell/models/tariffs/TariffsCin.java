package com.bakcell.models.tariffs;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class TariffsCin implements Parcelable {


    private String name_title;
    private String detail_title;
    private String desc_title;

    public TariffsCin(Parcel in) {
        name_title = in.readString();
        detail_title = in.readString();
        desc_title = in.readString();
    }

    public static final Creator<TariffsCin> CREATOR = new Creator<TariffsCin>() {
        @Override
        public TariffsCin createFromParcel(Parcel in) {
            return new TariffsCin(in);
        }

        @Override
        public TariffsCin[] newArray(int size) {
            return new TariffsCin[size];
        }
    };

    public TariffsCin() {

    }

    public String getName_title() {
        return name_title;
    }

    public void setName_title(String name_title) {
        this.name_title = name_title;
    }

    public String getDetail_title() {
        return detail_title;
    }

    public void setDetail_title(String detail_title) {
        this.detail_title = detail_title;
    }

    public String getDesc_title() {
        return desc_title;
    }

    public void setDesc_title(String desc_title) {
        this.desc_title = desc_title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name_title);
        dest.writeString(detail_title);
        dest.writeString(desc_title);
    }
}
