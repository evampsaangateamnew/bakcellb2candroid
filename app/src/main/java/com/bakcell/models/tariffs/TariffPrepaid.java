package com.bakcell.models.tariffs;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffcinobject.TariffCin;
import com.bakcell.models.tariffs.tariffklassobject.TariffKlass;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class TariffPrepaid implements Parcelable {
    @SerializedName("klass")
    ArrayList<TariffKlass> tariffsKlasses;
    @SerializedName("cin")
    ArrayList<TariffCin> tariffCins;
    @SerializedName("cin_new")
    ArrayList<TariffCinNew> tariffCinNew;

    public ArrayList<TariffCinNew> getTariffCinNew() {
        return tariffCinNew;
    }

    public void setTariffCinNew(ArrayList<TariffCinNew> tariffCinNew) {
        this.tariffCinNew = tariffCinNew;
    }

    public ArrayList<TariffKlass> getTariffsKlasses() {
        return tariffsKlasses;
    }

    public void setTariffsKlasses(ArrayList<TariffKlass> tariffsKlasses) {
        this.tariffsKlasses = tariffsKlasses;
    }

    public ArrayList<TariffCin> getTariffCins() {
        return tariffCins;
    }

    public void setTariffCins(ArrayList<TariffCin> tariffCins) {
        this.tariffCins = tariffCins;
    }

    protected TariffPrepaid(Parcel in) {
        tariffsKlasses = in.createTypedArrayList(TariffKlass.CREATOR);
        tariffCins = in.createTypedArrayList(TariffCin.CREATOR);
        tariffCinNew = in.createTypedArrayList(TariffCinNew.CREATOR);
    }

    public static final Creator<TariffPrepaid> CREATOR = new Creator<TariffPrepaid>() {
        @Override
        public TariffPrepaid createFromParcel(Parcel in) {
            return new TariffPrepaid(in);
        }

        @Override
        public TariffPrepaid[] newArray(int size) {
            return new TariffPrepaid[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(tariffsKlasses);
        dest.writeTypedList(tariffCins);
        dest.writeTypedList(tariffCinNew);
    }
}
