package com.bakcell.models.tariffs.individualobject;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.bakcell.models.tariffs.corporateobject.description.Description;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePrice;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/4/2017.
 */

public class TariffIndividual implements Parcelable {
    @SerializedName("header")
    Header header;
    @SerializedName("price")
    PackagePrice price;
    @SerializedName("details")
    TariffDetails details;
    @SerializedName("description")
    Description description;

    protected TariffIndividual(Parcel in) {
        header = in.readParcelable(Header.class.getClassLoader());
        price = in.readParcelable(PackagePrice.class.getClassLoader());
        details = in.readParcelable(TariffDetails.class.getClassLoader());
        description = in.readParcelable(Description.class.getClassLoader());
    }

    public static final Creator<TariffIndividual> CREATOR = new Creator<TariffIndividual>() {
        @Override
        public TariffIndividual createFromParcel(Parcel in) {
            return new TariffIndividual(in);
        }

        @Override
        public TariffIndividual[] newArray(int size) {
            return new TariffIndividual[size];
        }
    };

    public PackagePrice getPrice() {
        return price;
    }

    public void setPrice(PackagePrice price) {
        this.price = price;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public TariffDetails getDetails() {
        return details;
    }

    public void setDetails(TariffDetails details) {
        this.details = details;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(header, flags);
        dest.writeParcelable(price, flags);
        dest.writeParcelable(details, flags);
        dest.writeParcelable(description, flags);
    }
}
