package com.bakcell.models.tariffs.tariffklassobject.packageprice;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.Attributes;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class PackagePriceItem implements Parcelable {
    @SerializedName("iconName")
    String iconName;
    @SerializedName("title")
    String title;
    @SerializedName("titleValue")
    String titleValue;
    @SerializedName("titleValueLeft")
    String titleValueLeft;
    @SerializedName("titleValueRight")
    String titleValueRight;
    @SerializedName("priceTemplate")
    String priceTemplate;
    @SerializedName("attributes")
    ArrayList<Attributes> attributes;
    @SerializedName("subTitle")
    String subTitle;
    @SerializedName("subTitleValue")
    String subTitleValue;

    protected PackagePriceItem(Parcel in) {
        iconName = in.readString();
        title = in.readString();
        titleValue = in.readString();
        titleValueLeft = in.readString();
        titleValueRight = in.readString();
        priceTemplate = in.readString();
        attributes = in.createTypedArrayList(Attributes.CREATOR);
        subTitle = in.readString();
        subTitleValue = in.readString();
    }

    public static final Creator<PackagePriceItem> CREATOR = new Creator<PackagePriceItem>() {
        @Override
        public PackagePriceItem createFromParcel(Parcel in) {
            return new PackagePriceItem(in);
        }

        @Override
        public PackagePriceItem[] newArray(int size) {
            return new PackagePriceItem[size];
        }
    };

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleValue() {
        return titleValue;
    }

    public void setTitleValue(String titleValue) {
        this.titleValue = titleValue;
    }

    public String getTitleValueLeft() {
        return titleValueLeft;
    }

    public void setTitleValueLeft(String titleValueLeft) {
        this.titleValueLeft = titleValueLeft;
    }

    public String getTitleValueRight() {
        return titleValueRight;
    }

    public void setTitleValueRight(String titleValueRight) {
        this.titleValueRight = titleValueRight;
    }

    public String getPriceTemplate() {
        return priceTemplate;
    }

    public void setPriceTemplate(String priceTemplate) {
        this.priceTemplate = priceTemplate;
    }

    public ArrayList<Attributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attributes> attributes) {
        this.attributes = attributes;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSubTitleValue() {
        return subTitleValue;
    }

    public void setSubTitleValue(String subTitleValue) {
        this.subTitleValue = subTitleValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(iconName);
        dest.writeString(title);
        dest.writeString(titleValue);
        dest.writeString(titleValueLeft);
        dest.writeString(titleValueRight);
        dest.writeString(priceTemplate);
        dest.writeTypedList(attributes);
        dest.writeString(subTitle);
        dest.writeString(subTitleValue);
    }
}
