package com.bakcell.models.tariffs;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Aug-17.
 */

public class Tariffs implements Parcelable {

    @SerializedName("advantages")
    private String advantages;
    @SerializedName("bonus_note")
    private String bonus_note;
    @SerializedName("call_price_title")
    private String call_price_title;
    @SerializedName("call_price_value")
    private String call_price_value;
    @SerializedName("charging_interval_for_internet")
    private String charging_interval_for_internet;
    @SerializedName("chg_interval_countrywide_call")
    private String chg_interval_countrywide_call;
    @SerializedName("chg_interval_intnl_calls")
    private String chg_interval_intnl_calls;
    @SerializedName("clarification")
    private String clarification;
    @SerializedName("countrywide")
    private String countrywide;
    @SerializedName("countrywide_calls")
    private String countrywide_calls;
    @SerializedName("countrywide_internet")
    private String countrywide_internet;
    @SerializedName("countrywide_sms")
    private String countrywide_sms;
    @SerializedName("international_sms")
    private String international_sms;
    @SerializedName("internet_package")
    private String internet_package;
    @SerializedName("internet_payg")
    private String internet_payg;
    @SerializedName("internet_price_title")
    private String internet_price_title;
    @SerializedName("internet_price_value")
    private String internet_price_value;
    @SerializedName("mms")
    private String mms;
    @SerializedName("mms_title")
    private String mms_title;
    @SerializedName("mms_value")
    private String mms_value;
    @SerializedName("monthly_fee")
    private String monthly_fee;
    @SerializedName("name")
    private String name;
    @SerializedName("non_working_days_details")
    private String non_working_days_details;
    @SerializedName("official_wrkng_dz")
    private String official_wrkng_dz;
    @SerializedName("off_net_calls")
    private String off_net_calls;
    @SerializedName("on_net_calls")
    private String on_net_calls;
    @SerializedName("price")
    private String price;
    @SerializedName("short_description")
    private String short_description;
    @SerializedName("sms_price_title")
    private String sms_price_title;
    @SerializedName("sms_price_value")
    private String sms_price_value;
    @SerializedName("subscribable")
    private String subscribable;
    @SerializedName("tag")
    private String tag;
    @SerializedName("whatsapp_using")
    private String whatsapp_using;

    public String getAdvantages() {
        return advantages;
    }

    public void setAdvantages(String advantages) {
        this.advantages = advantages;
    }

    public String getBonus_note() {
        return bonus_note;
    }

    public void setBonus_note(String bonus_note) {
        this.bonus_note = bonus_note;
    }

    public String getCall_price_title() {
        return call_price_title;
    }

    public void setCall_price_title(String call_price_title) {
        this.call_price_title = call_price_title;
    }

    public String getCall_price_value() {
        return call_price_value;
    }

    public void setCall_price_value(String call_price_value) {
        this.call_price_value = call_price_value;
    }

    public String getCharging_interval_for_internet() {
        return charging_interval_for_internet;
    }

    public void setCharging_interval_for_internet(String charging_interval_for_internet) {
        this.charging_interval_for_internet = charging_interval_for_internet;
    }

    public String getChg_interval_countrywide_call() {
        return chg_interval_countrywide_call;
    }

    public void setChg_interval_countrywide_call(String chg_interval_countrywide_call) {
        this.chg_interval_countrywide_call = chg_interval_countrywide_call;
    }

    public String getChg_interval_intnl_calls() {
        return chg_interval_intnl_calls;
    }

    public void setChg_interval_intnl_calls(String chg_interval_intnl_calls) {
        this.chg_interval_intnl_calls = chg_interval_intnl_calls;
    }

    public String getClarification() {
        return clarification;
    }

    public void setClarification(String clarification) {
        this.clarification = clarification;
    }

    public String getCountrywide() {
        return countrywide;
    }

    public void setCountrywide(String countrywide) {
        this.countrywide = countrywide;
    }

    public String getCountrywide_calls() {
        return countrywide_calls;
    }

    public void setCountrywide_calls(String countrywide_calls) {
        this.countrywide_calls = countrywide_calls;
    }

    public String getCountrywide_internet() {
        return countrywide_internet;
    }

    public void setCountrywide_internet(String countrywide_internet) {
        this.countrywide_internet = countrywide_internet;
    }

    public String getCountrywide_sms() {
        return countrywide_sms;
    }

    public void setCountrywide_sms(String countrywide_sms) {
        this.countrywide_sms = countrywide_sms;
    }

    public String getInternational_sms() {
        return international_sms;
    }

    public void setInternational_sms(String international_sms) {
        this.international_sms = international_sms;
    }

    public String getInternet_package() {
        return internet_package;
    }

    public void setInternet_package(String internet_package) {
        this.internet_package = internet_package;
    }

    public String getInternet_payg() {
        return internet_payg;
    }

    public void setInternet_payg(String internet_payg) {
        this.internet_payg = internet_payg;
    }

    public String getInternet_price_title() {
        return internet_price_title;
    }

    public void setInternet_price_title(String internet_price_title) {
        this.internet_price_title = internet_price_title;
    }

    public String getInternet_price_value() {
        return internet_price_value;
    }

    public void setInternet_price_value(String internet_price_value) {
        this.internet_price_value = internet_price_value;
    }

    public String getMms() {
        return mms;
    }

    public void setMms(String mms) {
        this.mms = mms;
    }

    public String getMms_title() {
        return mms_title;
    }

    public void setMms_title(String mms_title) {
        this.mms_title = mms_title;
    }

    public String getMms_value() {
        return mms_value;
    }

    public void setMms_value(String mms_value) {
        this.mms_value = mms_value;
    }

    public String getMonthly_fee() {
        return monthly_fee;
    }

    public void setMonthly_fee(String monthly_fee) {
        this.monthly_fee = monthly_fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNon_working_days_details() {
        return non_working_days_details;
    }

    public void setNon_working_days_details(String non_working_days_details) {
        this.non_working_days_details = non_working_days_details;
    }

    public String getOfficial_wrkng_dz() {
        return official_wrkng_dz;
    }

    public void setOfficial_wrkng_dz(String official_wrkng_dz) {
        this.official_wrkng_dz = official_wrkng_dz;
    }

    public String getOff_net_calls() {
        return off_net_calls;
    }

    public void setOff_net_calls(String off_net_calls) {
        this.off_net_calls = off_net_calls;
    }

    public String getOn_net_calls() {
        return on_net_calls;
    }

    public void setOn_net_calls(String on_net_calls) {
        this.on_net_calls = on_net_calls;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getSms_price_title() {
        return sms_price_title;
    }

    public void setSms_price_title(String sms_price_title) {
        this.sms_price_title = sms_price_title;
    }

    public String getSms_price_value() {
        return sms_price_value;
    }

    public void setSms_price_value(String sms_price_value) {
        this.sms_price_value = sms_price_value;
    }

    public String getSubscribable() {
        return subscribable;
    }

    public void setSubscribable(String subscribable) {
        this.subscribable = subscribable;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getWhatsapp_using() {
        return whatsapp_using;
    }

    public void setWhatsapp_using(String whatsapp_using) {
        this.whatsapp_using = whatsapp_using;
    }

    protected Tariffs(Parcel in) {
        advantages = in.readString();
        bonus_note = in.readString();
        call_price_title = in.readString();
        call_price_value = in.readString();
        charging_interval_for_internet = in.readString();
        chg_interval_countrywide_call = in.readString();
        chg_interval_intnl_calls = in.readString();
        clarification = in.readString();
        countrywide = in.readString();
        countrywide_calls = in.readString();
        countrywide_internet = in.readString();
        countrywide_sms = in.readString();
        international_sms = in.readString();
        internet_package = in.readString();
        internet_payg = in.readString();
        internet_price_title = in.readString();
        internet_price_value = in.readString();
        mms = in.readString();
        mms_title = in.readString();
        mms_value = in.readString();
        monthly_fee = in.readString();
        name = in.readString();
        non_working_days_details = in.readString();
        official_wrkng_dz = in.readString();
        off_net_calls = in.readString();
        on_net_calls = in.readString();
        price = in.readString();
        short_description = in.readString();
        sms_price_title = in.readString();
        sms_price_value = in.readString();
        subscribable = in.readString();
        tag = in.readString();
        whatsapp_using = in.readString();
    }

    public static final Creator<Tariffs> CREATOR = new Creator<Tariffs>() {
        @Override
        public Tariffs createFromParcel(Parcel in) {
            return new Tariffs(in);
        }

        @Override
        public Tariffs[] newArray(int size) {
            return new Tariffs[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(advantages);
        dest.writeString(bonus_note);
        dest.writeString(call_price_title);
        dest.writeString(call_price_value);
        dest.writeString(charging_interval_for_internet);
        dest.writeString(chg_interval_countrywide_call);
        dest.writeString(chg_interval_intnl_calls);
        dest.writeString(clarification);
        dest.writeString(countrywide);
        dest.writeString(countrywide_calls);
        dest.writeString(countrywide_internet);
        dest.writeString(countrywide_sms);
        dest.writeString(international_sms);
        dest.writeString(internet_package);
        dest.writeString(internet_payg);
        dest.writeString(internet_price_title);
        dest.writeString(internet_price_value);
        dest.writeString(mms);
        dest.writeString(mms_title);
        dest.writeString(mms_value);
        dest.writeString(monthly_fee);
        dest.writeString(name);
        dest.writeString(non_working_days_details);
        dest.writeString(official_wrkng_dz);
        dest.writeString(off_net_calls);
        dest.writeString(on_net_calls);
        dest.writeString(price);
        dest.writeString(short_description);
        dest.writeString(sms_price_title);
        dest.writeString(sms_price_value);
        dest.writeString(subscribable);
        dest.writeString(tag);
        dest.writeString(whatsapp_using);
    }
}
