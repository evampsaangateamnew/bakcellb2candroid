package com.bakcell.models.tariffs;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Aug-17.
 */

public class MainTariffs implements Parcelable {

    @SerializedName("prepaid")
    private TariffPrepaid tariffPrepaid;
    @SerializedName("postpaid")
    private TariffPostpaid tariffPostpaid;

    public TariffPrepaid getTariffPrepaid() {
        return tariffPrepaid;
    }

    public void setTariffPrepaid(TariffPrepaid tariffPrepaid) {
        this.tariffPrepaid = tariffPrepaid;
    }

    public TariffPostpaid getTariffPostpaid() {
        return tariffPostpaid;
    }

    public void setTariffPostpaid(TariffPostpaid tariffPostpaid) {
        this.tariffPostpaid = tariffPostpaid;
    }

    protected MainTariffs(Parcel in) {
        tariffPrepaid = in.readParcelable(TariffPrepaid.class.getClassLoader());
        tariffPostpaid = in.readParcelable(TariffPostpaid.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(tariffPrepaid, flags);
        dest.writeParcelable(tariffPostpaid, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MainTariffs> CREATOR = new Creator<MainTariffs>() {
        @Override
        public MainTariffs createFromParcel(Parcel in) {
            return new MainTariffs(in);
        }

        @Override
        public MainTariffs[] newArray(int size) {
            return new MainTariffs[size];
        }
    };
}
