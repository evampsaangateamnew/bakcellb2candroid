/**
 *
 */
package com.bakcell.models.tariffs.tariffdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Noman
 */
public class TextWithPoints implements Parcelable {

    @SerializedName("pointsList")
    ArrayList<String> pointsList;

    public ArrayList<String> getPointsList() {
        return pointsList;
    }

    public void setPointsList(ArrayList<String> pointsList) {
        this.pointsList = pointsList;
    }

    protected TextWithPoints(Parcel in) {
        pointsList = in.createStringArrayList();
    }

    public static final Creator<TextWithPoints> CREATOR = new Creator<TextWithPoints>() {
        @Override
        public TextWithPoints createFromParcel(Parcel in) {
            return new TextWithPoints(in);
        }

        @Override
        public TextWithPoints[] newArray(int size) {
            return new TextWithPoints[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(pointsList);
    }
}
