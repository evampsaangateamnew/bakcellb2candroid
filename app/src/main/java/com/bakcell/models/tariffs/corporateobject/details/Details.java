package com.bakcell.models.tariffs.corporateobject.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.details.DetailItem;
import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePriceItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/4/2017.
 */

public class Details implements Parcelable {
    @SerializedName("destination")
    PackagePriceItem destination;
    @SerializedName("internationalOnPeak")
    DetailItem internationalOnPeak;
    @SerializedName("internationalOffPeak")
    DetailItem internationalOffPeak;



    public PackagePriceItem getDestination() {
        return destination;
    }

    public void setDestination(PackagePriceItem destination) {
        this.destination = destination;
    }

    public DetailItem getInternationalOnPeak() {
        return internationalOnPeak;
    }

    public void setInternationalOnPeak(DetailItem internationalOnPeak) {
        this.internationalOnPeak = internationalOnPeak;
    }

    public DetailItem getInternationalOffPeak() {
        return internationalOffPeak;
    }

    public void setInternationalOffPeak(DetailItem internationalOffPeak) {
        this.internationalOffPeak = internationalOffPeak;
    }

    protected Details(Parcel in) {
        destination = in.readParcelable(PackagePriceItem.class.getClassLoader());
        internationalOnPeak = in.readParcelable(DetailItem.class.getClassLoader());
        internationalOffPeak = in.readParcelable(DetailItem.class.getClassLoader());
    }

    public static final Creator<Details> CREATOR = new Creator<Details>() {
        @Override
        public Details createFromParcel(Parcel in) {
            return new Details(in);
        }

        @Override
        public Details[] newArray(int size) {
            return new Details[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(destination, flags);
        dest.writeParcelable(internationalOnPeak, flags);
        dest.writeParcelable(internationalOffPeak, flags);
    }
}
