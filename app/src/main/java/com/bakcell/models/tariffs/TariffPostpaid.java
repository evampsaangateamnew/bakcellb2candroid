package com.bakcell.models.tariffs;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.corporateobject.TariffCorporate;
import com.bakcell.models.tariffs.individualobject.TariffIndividual;
import com.bakcell.models.tariffs.tariffklassobject.TariffKlass;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class TariffPostpaid implements Parcelable {
    @SerializedName("corporate")
    ArrayList<TariffCorporate> corporate;
    @SerializedName("klassPostpaid")
    ArrayList<TariffKlass> klassPostpaid;
    @SerializedName("individual")
    ArrayList<TariffIndividual> individual;

    protected TariffPostpaid(Parcel in) {
        corporate = in.createTypedArrayList(TariffCorporate.CREATOR);
        klassPostpaid = in.createTypedArrayList(TariffKlass.CREATOR);
        individual = in.createTypedArrayList(TariffIndividual.CREATOR);
    }

    public static final Creator<TariffPostpaid> CREATOR = new Creator<TariffPostpaid>() {
        @Override
        public TariffPostpaid createFromParcel(Parcel in) {
            return new TariffPostpaid(in);
        }

        @Override
        public TariffPostpaid[] newArray(int size) {
            return new TariffPostpaid[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public ArrayList<TariffCorporate> getCorporate() {
        return corporate;
    }

    public void setCorporate(ArrayList<TariffCorporate> corporate) {
        this.corporate = corporate;
    }

    public ArrayList<TariffKlass> getKlassPostpaid() {
        return klassPostpaid;
    }

    public void setKlassPostpaid(ArrayList<TariffKlass> klassPostpaid) {
        this.klassPostpaid = klassPostpaid;
    }

    public ArrayList<TariffIndividual> getIndividual() {
        return individual;
    }

    public void setIndividual(ArrayList<TariffIndividual> individual) {
        this.individual = individual;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(corporate);
        dest.writeTypedList(klassPostpaid);
        dest.writeTypedList(individual);
    }
}
