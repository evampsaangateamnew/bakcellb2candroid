package com.bakcell.models.tariffs.tariffcinobject.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePriceItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class Details implements Parcelable{
    @SerializedName("mms")
    PackagePriceItem mms;
    @SerializedName("call")
    PackagePriceItem call;
    @SerializedName("destination")
    PackagePriceItem destination;

    public PackagePriceItem getMms() {
        return mms;
    }

    public void setMms(PackagePriceItem mms) {
        this.mms = mms;
    }

    public PackagePriceItem getCall() {
        return call;
    }

    public void setCall(PackagePriceItem call) {
        this.call = call;
    }

    public PackagePriceItem getDestination() {
        return destination;
    }

    public void setDestination(PackagePriceItem destination) {
        this.destination = destination;
    }

    protected Details(Parcel in) {
        mms = in.readParcelable(PackagePriceItem.class.getClassLoader());
        call = in.readParcelable(PackagePriceItem.class.getClassLoader());
        destination = in.readParcelable(PackagePriceItem.class.getClassLoader());
    }

    public static final Creator<Details> CREATOR = new Creator<Details>() {
        @Override
        public Details createFromParcel(Parcel in) {
            return new Details(in);
        }

        @Override
        public Details[] newArray(int size) {
            return new Details[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mms, flags);
        dest.writeParcelable(call, flags);
        dest.writeParcelable(destination, flags);
    }
}
