package com.bakcell.models.tariffs.tariffklassobject.packageprice;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class PackagePrice implements Parcelable {
    @SerializedName("call")
    private PackagePriceItem call;
    @SerializedName("sms")
    private PackagePriceItem sms;
    @SerializedName("internet")
    private PackagePriceItem interent;
    @SerializedName("packagePriceLabel")
    private String packagePriceLabel;
    @SerializedName("priceLabel")
    private String priceLabel;

    protected PackagePrice(Parcel in) {
        call = in.readParcelable(PackagePriceItem.class.getClassLoader());
        sms = in.readParcelable(PackagePriceItem.class.getClassLoader());
        interent = in.readParcelable(PackagePriceItem.class.getClassLoader());
        packagePriceLabel = in.readString();
        priceLabel = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(call, flags);
        dest.writeParcelable(sms, flags);
        dest.writeParcelable(interent, flags);
        dest.writeString(packagePriceLabel);
        dest.writeString(priceLabel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PackagePrice> CREATOR = new Creator<PackagePrice>() {
        @Override
        public PackagePrice createFromParcel(Parcel in) {
            return new PackagePrice(in);
        }

        @Override
        public PackagePrice[] newArray(int size) {
            return new PackagePrice[size];
        }
    };

    public String getPackagePriceLabel() {
        return packagePriceLabel;
    }

    public void setPackagePriceLabel(String packagePriceLabel) {
        this.packagePriceLabel = packagePriceLabel;
    }

    public String getPriceLabel() {
        return priceLabel;
    }

    public void setPriceLabel(String priceLabel) {
        this.priceLabel = priceLabel;
    }

    public PackagePriceItem getCall() {
        return call;
    }

    public void setCall(PackagePriceItem call) {
        this.call = call;
    }

    public PackagePriceItem getSms() {
        return sms;
    }

    public void setSms(PackagePriceItem sms) {
        this.sms = sms;
    }

    public PackagePriceItem getInterent() {
        return interent;
    }

    public void setInterent(PackagePriceItem interent) {
        this.interent = interent;
    }


}
