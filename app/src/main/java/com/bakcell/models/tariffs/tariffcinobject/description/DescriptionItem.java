package com.bakcell.models.tariffs.tariffcinobject.description;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class DescriptionItem implements Parcelable{
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    protected DescriptionItem(Parcel in) {
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<DescriptionItem> CREATOR = new Creator<DescriptionItem>() {
        @Override
        public DescriptionItem createFromParcel(Parcel in) {
            return new DescriptionItem(in);
        }

        @Override
        public DescriptionItem[] newArray(int size) {
            return new DescriptionItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
    }
}
