package com.bakcell.models.tariffs.corporateobject.description;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.details.DetailItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/4/2017.
 */

public class Description implements Parcelable {
    @SerializedName("advantages")
    DetailItem advantages;
    @SerializedName("classification")
    DetailItem classification;
    @SerializedName("descLabel")
    String descLabel;
    @SerializedName("descriptionTitle")
    String descriptionTitle;

    protected Description(Parcel in) {
        advantages = in.readParcelable(DetailItem.class.getClassLoader());
        classification = in.readParcelable(DetailItem.class.getClassLoader());
        descLabel = in.readString();
        descriptionTitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(advantages, flags);
        dest.writeParcelable(classification, flags);
        dest.writeString(descLabel);
        dest.writeString(descriptionTitle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Description> CREATOR = new Creator<Description>() {
        @Override
        public Description createFromParcel(Parcel in) {
            return new Description(in);
        }

        @Override
        public Description[] newArray(int size) {
            return new Description[size];
        }
    };

    public String getDescriptionTitle() {
        return descriptionTitle;
    }

    public void setDescriptionTitle(String descriptionTitle) {
        this.descriptionTitle = descriptionTitle;
    }

    public DetailItem getAdvantages() {
        return advantages;
    }

    public void setAdvantages(DetailItem advantages) {
        this.advantages = advantages;
    }

    public DetailItem getClassification() {
        return classification;
    }

    public void setClassification(DetailItem classification) {
        this.classification = classification;
    }

    public String getDescLabel() {
        return descLabel;
    }

    public void setDescLabel(String descLabel) {
        this.descLabel = descLabel;
    }
}
