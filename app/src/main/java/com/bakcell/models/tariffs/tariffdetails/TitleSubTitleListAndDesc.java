/**
 *
 */
package com.bakcell.models.tariffs.tariffdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.HeaderAttributes;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * @author Noman
 */
public class TitleSubTitleListAndDesc implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("attributeList")
    private ArrayList<HeaderAttributes> attributeList;


    protected TitleSubTitleListAndDesc(Parcel in) {
        title = in.readString();
        attributeList = in.createTypedArrayList(HeaderAttributes.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeTypedList(attributeList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TitleSubTitleListAndDesc> CREATOR = new Creator<TitleSubTitleListAndDesc>() {
        @Override
        public TitleSubTitleListAndDesc createFromParcel(Parcel in) {
            return new TitleSubTitleListAndDesc(in);
        }

        @Override
        public TitleSubTitleListAndDesc[] newArray(int size) {
            return new TitleSubTitleListAndDesc[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<HeaderAttributes> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(ArrayList<HeaderAttributes> attributeList) {
        this.attributeList = attributeList;
    }


}