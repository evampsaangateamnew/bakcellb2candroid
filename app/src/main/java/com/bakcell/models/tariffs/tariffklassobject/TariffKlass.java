package com.bakcell.models.tariffs.tariffklassobject;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffcinobject.description.Description;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePrice;
import com.bakcell.models.tariffs.tariffklassobject.paygprice.PaygPrice;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class TariffKlass implements Parcelable {
    @SerializedName("header")
    Header header;
    @SerializedName("packagePrice")
    PackagePrice packagePrice;
    @SerializedName("paygPrice")
    PaygPrice paygPrice;
    @SerializedName("details")
    TariffDetails details;
    @SerializedName("description")
    Description description;

    protected TariffKlass(Parcel in) {
        header = in.readParcelable(Header.class.getClassLoader());
        packagePrice = in.readParcelable(PackagePrice.class.getClassLoader());
        paygPrice = in.readParcelable(PaygPrice.class.getClassLoader());
        details = in.readParcelable(TariffDetails.class.getClassLoader());
        description = in.readParcelable(Description.class.getClassLoader());
    }

    public static final Creator<TariffKlass> CREATOR = new Creator<TariffKlass>() {
        @Override
        public TariffKlass createFromParcel(Parcel in) {
            return new TariffKlass(in);
        }

        @Override
        public TariffKlass[] newArray(int size) {
            return new TariffKlass[size];
        }
    };

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public PackagePrice getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(PackagePrice packagePrice) {
        this.packagePrice = packagePrice;
    }

    public PaygPrice getPaygPrice() {
        return paygPrice;
    }

    public void setPaygPrice(PaygPrice paygPrice) {
        this.paygPrice = paygPrice;
    }

    public TariffDetails getDetails() {
        return details;
    }

    public void setDetails(TariffDetails details) {
        this.details = details;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(header, flags);
        dest.writeParcelable(packagePrice, flags);
        dest.writeParcelable(paygPrice, flags);
        dest.writeParcelable(details, flags);
        dest.writeParcelable(description, flags);
    }
}
