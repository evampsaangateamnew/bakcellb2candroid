/**
 *
 */
package com.bakcell.models.tariffs.tariffdetails;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Noman
 */
public class TariffDetails implements Parcelable {

    @SerializedName("price")
    private ArrayList<Price> price;
    @SerializedName("rounding")
    private Rounding rounding;
    @SerializedName("textWithTitle")
    private TextWithTitle textWithTitle;
    @SerializedName("textWithOutTitle")
    private TextWithOutTitle textWithOutTitle;
    @SerializedName("textWithPoints")
    private TextWithPoints textWithPoints;
    @SerializedName("titleSubTitleValueAndDesc")
    private TitleSubTitleListAndDesc titleSubTitleValueAndDesc;
    @SerializedName("date")
    private Date date;
    @SerializedName("time")
    private Time time;
    @SerializedName("roamingDetails")
    private RoamingDetails roamingDetails;
    @SerializedName("freeResourceValidity")
    private FreeResourceValidity freeResourceValidity;
    @SerializedName("detailLabel")
    private String detailLabel;

    protected TariffDetails(Parcel in) {
        price = in.createTypedArrayList(Price.CREATOR);
        rounding = in.readParcelable(Rounding.class.getClassLoader());
        textWithTitle = in.readParcelable(TextWithTitle.class.getClassLoader());
        textWithOutTitle = in.readParcelable(TextWithOutTitle.class.getClassLoader());
        textWithPoints = in.readParcelable(TextWithPoints.class.getClassLoader());
        titleSubTitleValueAndDesc = in.readParcelable(TitleSubTitleListAndDesc.class.getClassLoader());
        date = in.readParcelable(Date.class.getClassLoader());
        time = in.readParcelable(Time.class.getClassLoader());
        roamingDetails = in.readParcelable(RoamingDetails.class.getClassLoader());
        freeResourceValidity = in.readParcelable(FreeResourceValidity.class.getClassLoader());
        detailLabel = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(price);
        dest.writeParcelable(rounding, flags);
        dest.writeParcelable(textWithTitle, flags);
        dest.writeParcelable(textWithOutTitle, flags);
        dest.writeParcelable(textWithPoints, flags);
        dest.writeParcelable(titleSubTitleValueAndDesc, flags);
        dest.writeParcelable(date, flags);
        dest.writeParcelable(time, flags);
        dest.writeParcelable(roamingDetails, flags);
        dest.writeParcelable(freeResourceValidity, flags);
        dest.writeString(detailLabel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TariffDetails> CREATOR = new Creator<TariffDetails>() {
        @Override
        public TariffDetails createFromParcel(Parcel in) {
            return new TariffDetails(in);
        }

        @Override
        public TariffDetails[] newArray(int size) {
            return new TariffDetails[size];
        }
    };

    public String getDetailLabel() {
        return detailLabel;
    }

    public void setDetailLabel(String detailLabel) {
        this.detailLabel = detailLabel;
    }

    public ArrayList<Price> getPrice() {
        return price;
    }

    public void setPrice(ArrayList<Price> price) {
        this.price = price;
    }

    public Rounding getRounding() {
        return rounding;
    }

    public void setRounding(Rounding rounding) {
        this.rounding = rounding;
    }

    public TextWithTitle getTextWithTitle() {
        return textWithTitle;
    }

    public void setTextWithTitle(TextWithTitle textWithTitle) {
        this.textWithTitle = textWithTitle;
    }

    public TextWithOutTitle getTextWithOutTitle() {
        return textWithOutTitle;
    }

    public void setTextWithOutTitle(TextWithOutTitle textWithOutTitle) {
        this.textWithOutTitle = textWithOutTitle;
    }

    public TextWithPoints getTextWithPoints() {
        return textWithPoints;
    }

    public void setTextWithPoints(TextWithPoints textWithPoints) {
        this.textWithPoints = textWithPoints;
    }

    public TitleSubTitleListAndDesc getTitleSubTitleValueAndDesc() {
        return titleSubTitleValueAndDesc;
    }

    public void setTitleSubTitleValueAndDesc(TitleSubTitleListAndDesc titleSubTitleValueAndDesc) {
        this.titleSubTitleValueAndDesc = titleSubTitleValueAndDesc;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public RoamingDetails getRoamingDetails() {
        return roamingDetails;
    }

    public void setRoamingDetails(RoamingDetails roamingDetails) {
        this.roamingDetails = roamingDetails;
    }

    public FreeResourceValidity getFreeResourceValidity() {
        return freeResourceValidity;
    }

    public void setFreeResourceValidity(FreeResourceValidity freeResourceValidity) {
        this.freeResourceValidity = freeResourceValidity;
    }


}
