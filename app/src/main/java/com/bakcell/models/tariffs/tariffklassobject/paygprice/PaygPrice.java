package com.bakcell.models.tariffs.tariffklassobject.paygprice;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePriceItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class PaygPrice implements Parcelable {
    @SerializedName("call")
    private PackagePriceItem call;
    @SerializedName("sms")
    private PackagePriceItem sms;
    @SerializedName("internet")
    private PackagePriceItem interent;
    @SerializedName("paygPriceLabel")
    private String paygPriceLabel;

    protected PaygPrice(Parcel in) {
        call = in.readParcelable(PackagePriceItem.class.getClassLoader());
        sms = in.readParcelable(PackagePriceItem.class.getClassLoader());
        interent = in.readParcelable(PackagePriceItem.class.getClassLoader());
        paygPriceLabel = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(call, flags);
        dest.writeParcelable(sms, flags);
        dest.writeParcelable(interent, flags);
        dest.writeString(paygPriceLabel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaygPrice> CREATOR = new Creator<PaygPrice>() {
        @Override
        public PaygPrice createFromParcel(Parcel in) {
            return new PaygPrice(in);
        }

        @Override
        public PaygPrice[] newArray(int size) {
            return new PaygPrice[size];
        }
    };

    public PackagePriceItem getCall() {
        return call;
    }

    public void setCall(PackagePriceItem call) {
        this.call = call;
    }

    public PackagePriceItem getSms() {
        return sms;
    }

    public void setSms(PackagePriceItem sms) {
        this.sms = sms;
    }

    public PackagePriceItem getInterent() {
        return interent;
    }

    public void setInterent(PackagePriceItem interent) {
        this.interent = interent;
    }

    public String getPaygPriceLabel() {
        return paygPriceLabel;
    }

    public void setPaygPriceLabel(String paygPriceLabel) {
        this.paygPriceLabel = paygPriceLabel;
    }
}
