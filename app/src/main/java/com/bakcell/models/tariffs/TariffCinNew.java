package com.bakcell.models.tariffs;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffcinobject.description.Description;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.bakcell.models.tariffs.tariffklassobject.packageprice.PackagePrice;
import com.bakcell.models.tariffs.tariffklassobject.paygprice.PaygPrice;
import com.google.gson.annotations.SerializedName;

/**
 * @author Junaid Hassan on 29, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class TariffCinNew implements Parcelable {
    @SerializedName("header")
    Header header;
    @SerializedName("packagePrice")
    PackagePrice packagePrice;
    @SerializedName("paygPrice")
    PaygPrice paygPrice;
    @SerializedName("details")
    TariffDetails details;
    @SerializedName("description")
    Description description;

    protected TariffCinNew(Parcel in) {
        header = in.readParcelable(Header.class.getClassLoader());
        packagePrice = in.readParcelable(PackagePrice.class.getClassLoader());
        paygPrice = in.readParcelable(PaygPrice.class.getClassLoader());
        details = in.readParcelable(TariffDetails.class.getClassLoader());
        description = in.readParcelable(Description.class.getClassLoader());
    }

    public static final Creator<TariffCinNew> CREATOR = new Creator<TariffCinNew>() {
        @Override
        public TariffCinNew createFromParcel(Parcel in) {
            return new TariffCinNew(in);
        }

        @Override
        public TariffCinNew[] newArray(int size) {
            return new TariffCinNew[size];
        }
    };

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public PackagePrice getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(PackagePrice packagePrice) {
        this.packagePrice = packagePrice;
    }

    public PaygPrice getPaygPrice() {
        return paygPrice;
    }

    public void setPaygPrice(PaygPrice paygPrice) {
        this.paygPrice = paygPrice;
    }

    public TariffDetails getDetails() {
        return details;
    }

    public void setDetails(TariffDetails details) {
        this.details = details;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(header, flags);
        dest.writeParcelable(packagePrice, flags);
        dest.writeParcelable(paygPrice, flags);
        dest.writeParcelable(details, flags);
        dest.writeParcelable(description, flags);
    }
}
