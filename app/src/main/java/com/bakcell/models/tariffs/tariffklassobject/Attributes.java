package com.bakcell.models.tariffs.tariffklassobject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class Attributes implements Parcelable {
    @SerializedName("iconName")
    private
    String iconName;
    @SerializedName("title")
    private
    String title;
    @SerializedName("value")
    private
    String value;
    @SerializedName("valueLeft")
    private
    String valueLeft;
    @SerializedName("valueRight")
    private
    String valueRight;
    @SerializedName("metrics")
    String metrics;


    protected Attributes(Parcel in) {
        iconName = in.readString();
        title = in.readString();
        value = in.readString();
        valueLeft = in.readString();
        valueRight = in.readString();
        metrics = in.readString();
    }

    public static final Creator<Attributes> CREATOR = new Creator<Attributes>() {
        @Override
        public Attributes createFromParcel(Parcel in) {
            return new Attributes(in);
        }

        @Override
        public Attributes[] newArray(int size) {
            return new Attributes[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueLeft() {
        return valueLeft;
    }

    public void setValueLeft(String valueLeft) {
        this.valueLeft = valueLeft;
    }

    public String getValueRight() {
        return valueRight;
    }

    public void setValueRight(String valueRight) {
        this.valueRight = valueRight;
    }

    public String getMetrics() {
        return metrics;
    }

    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(iconName);
        dest.writeString(title);
        dest.writeString(value);
        dest.writeString(valueLeft);
        dest.writeString(valueRight);
        dest.writeString(metrics);
    }
}
