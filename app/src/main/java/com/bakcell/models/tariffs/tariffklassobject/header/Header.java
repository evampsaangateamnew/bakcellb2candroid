package com.bakcell.models.tariffs.tariffklassobject.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffklassobject.Attributes;
import com.bakcell.models.tariffs.tariffklassobject.details.DetailItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class Header implements Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("offeringId")
    private String offeringId;
    @SerializedName("currency")
    private String currency;
    @SerializedName("name")
    private String name;
    @SerializedName("priceLabel")
    private String priceLabel;
    @SerializedName("priceValue")
    private String priceValue;
    @SerializedName("subscribable")
    private String subscribable;
    @SerializedName("attributes")
    private ArrayList<Attributes> attributes;
    @SerializedName("bonusDescription")
    private String bonusDescription;
    @SerializedName("bonusIconName")
    private String bonusIconName;
    @SerializedName("bonusTitle")
    private String bonusTitle;
    @SerializedName("call")
    private DetailItem call;
    @SerializedName("sms")
    private DetailItem sms;
    @SerializedName("internet")
    private DetailItem internet;
    @SerializedName("tariffType")
    private String tariffType;
    @SerializedName("sortOrder")
    private String sortOrder;

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getTariffType() {
        return tariffType;
    }

    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceLabel() {
        return priceLabel;
    }

    public void setPriceLabel(String priceLabel) {
        this.priceLabel = priceLabel;
    }

    public String getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(String priceValue) {
        this.priceValue = priceValue;
    }

    public String getSubscribable() {
        return subscribable;
    }

    public void setSubscribable(String subscribable) {
        this.subscribable = subscribable;
    }

    public ArrayList<Attributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attributes> attributes) {
        this.attributes = attributes;
    }

    public String getBonusDescription() {
        return bonusDescription;
    }

    public void setBonusDescription(String bonusDescription) {
        this.bonusDescription = bonusDescription;
    }

    public String getBonusIconName() {
        return bonusIconName;
    }

    public void setBonusIconName(String bonusIconName) {
        this.bonusIconName = bonusIconName;
    }

    public String getBonusTitle() {
        return bonusTitle;
    }

    public void setBonusTitle(String bonusTitle) {
        this.bonusTitle = bonusTitle;
    }

    public DetailItem getCall() {
        return call;
    }

    public void setCall(DetailItem call) {
        this.call = call;
    }

    public DetailItem getSms() {
        return sms;
    }

    public void setSms(DetailItem sms) {
        this.sms = sms;
    }

    public DetailItem getInternet() {
        return internet;
    }

    public void setInternet(DetailItem internet) {
        this.internet = internet;
    }

    protected Header(Parcel in) {
        id = in.readString();
        offeringId = in.readString();
        currency = in.readString();
        name = in.readString();
        priceLabel = in.readString();
        priceValue = in.readString();
        subscribable = in.readString();
        attributes = in.createTypedArrayList(Attributes.CREATOR);
        bonusDescription = in.readString();
        bonusIconName = in.readString();
        bonusTitle = in.readString();
        call = in.readParcelable(DetailItem.class.getClassLoader());
        sms = in.readParcelable(DetailItem.class.getClassLoader());
        internet = in.readParcelable(DetailItem.class.getClassLoader());
        tariffType = in.readString();
        sortOrder = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(offeringId);
        dest.writeString(currency);
        dest.writeString(name);
        dest.writeString(priceLabel);
        dest.writeString(priceValue);
        dest.writeString(subscribable);
        dest.writeTypedList(attributes);
        dest.writeString(bonusDescription);
        dest.writeString(bonusIconName);
        dest.writeString(bonusTitle);
        dest.writeParcelable(call, flags);
        dest.writeParcelable(sms, flags);
        dest.writeParcelable(internet, flags);
        dest.writeString(tariffType);
        dest.writeString(sortOrder);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Header> CREATOR = new Creator<Header>() {
        @Override
        public Header createFromParcel(Parcel in) {
            return new Header(in);
        }

        @Override
        public Header[] newArray(int size) {
            return new Header[size];
        }
    };
}
