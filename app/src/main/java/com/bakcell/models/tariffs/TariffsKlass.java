package com.bakcell.models.tariffs;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class TariffsKlass implements Parcelable {


    private String name_title;
    private String price_title;
    private String package_price_title;
    private String payg_price_title;
    private String detail_title;


    private ArrayList<TariffsKlassAttr> tariffsKlassAttrsList;

    public TariffsKlass(Parcel in) {
        name_title = in.readString();
        price_title = in.readString();
        package_price_title = in.readString();
        payg_price_title = in.readString();
        detail_title = in.readString();
    }

    public static final Creator<TariffsKlass> CREATOR = new Creator<TariffsKlass>() {
        @Override
        public TariffsKlass createFromParcel(Parcel in) {
            return new TariffsKlass(in);
        }

        @Override
        public TariffsKlass[] newArray(int size) {
            return new TariffsKlass[size];
        }
    };

    public TariffsKlass() {

    }

    public ArrayList<TariffsKlassAttr> getTariffsKlassAttrsList() {

        if (tariffsKlassAttrsList == null)
            tariffsKlassAttrsList = new ArrayList<>();

        return tariffsKlassAttrsList;
    }

    public void setTariffsKlassAttrsList(ArrayList<TariffsKlassAttr> tariffsKlassAttrsList) {
        this.tariffsKlassAttrsList = tariffsKlassAttrsList;
    }

    public String getName_title() {
        return name_title;
    }

    public void setName_title(String name_title) {
        this.name_title = name_title;
    }

    public String getPrice_title() {
        return price_title;
    }

    public void setPrice_title(String price_title) {
        this.price_title = price_title;
    }

    public String getPackage_price_title() {
        return package_price_title;
    }

    public void setPackage_price_title(String package_price_title) {
        this.package_price_title = package_price_title;
    }

    public String getPayg_price_title() {
        return payg_price_title;
    }

    public void setPayg_price_title(String payg_price_title) {
        this.payg_price_title = payg_price_title;
    }

    public String getDetail_title() {
        return detail_title;
    }

    public void setDetail_title(String detail_title) {
        this.detail_title = detail_title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name_title);
        dest.writeString(price_title);
        dest.writeString(package_price_title);
        dest.writeString(payg_price_title);
        dest.writeString(detail_title);
    }
}
