package com.bakcell.models.tariffs.tariffcinobject;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.tariffs.tariffcinobject.description.Description;
import com.bakcell.models.tariffs.tariffdetails.TariffDetails;
import com.bakcell.models.tariffs.tariffklassobject.header.Header;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 8/31/2017.
 */

public class TariffCin implements Parcelable {
    @SerializedName("header")
    Header header;
    @SerializedName("details")
    TariffDetails details;
    @SerializedName("description")
    Description description;

    protected TariffCin(Parcel in) {
        header = in.readParcelable(Header.class.getClassLoader());
        details = in.readParcelable(TariffDetails.class.getClassLoader());
        description = in.readParcelable(Description.class.getClassLoader());
    }

    public static final Creator<TariffCin> CREATOR = new Creator<TariffCin>() {
        @Override
        public TariffCin createFromParcel(Parcel in) {
            return new TariffCin(in);
        }

        @Override
        public TariffCin[] newArray(int size) {
            return new TariffCin[size];
        }
    };

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public TariffDetails getDetails() {
        return details;
    }

    public void setDetails(TariffDetails details) {
        this.details = details;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(header, flags);
        dest.writeParcelable(details, flags);
        dest.writeParcelable(description, flags);
    }
}
