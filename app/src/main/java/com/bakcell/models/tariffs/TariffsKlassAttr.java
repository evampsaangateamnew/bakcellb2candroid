package com.bakcell.models.tariffs;

/**
 * Created by Freeware Sys on 29-May-17.
 */

public class TariffsKlassAttr {

    private String attr_name;
    private String attr_value;
    private String attr_type;

    public String getAttr_name() {
        return attr_name;
    }

    public void setAttr_name(String attr_name) {
        this.attr_name = attr_name;
    }

    public String getAttr_value() {
        return attr_value;
    }

    public void setAttr_value(String attr_value) {
        this.attr_value = attr_value;
    }

    public String getAttr_type() {
        return attr_type;
    }

    public void setAttr_type(String attr_type) {
        this.attr_type = attr_type;
    }
}
