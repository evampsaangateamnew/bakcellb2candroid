package com.bakcell.models.tariffs.helper;

import com.bakcell.utilities.BakcellLogger;
import com.bakcell.utilities.Tools;

/**
 * @author Junaid Hassan on 29, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class TariffsHelperModel {
    private Object anyTariffObject;
    private String sortOrder;

    public TariffsHelperModel(Object anyTariffObject, String sortOrder) {
        this.anyTariffObject = anyTariffObject;
        this.sortOrder = sortOrder;
    }

    public Object getAnyTariffObject() {
        return anyTariffObject;
    }

    public void setAnyTariffObject(Object anyTariffObject) {
        this.anyTariffObject = anyTariffObject;
    }

    public int getSortOrderInt() {
        int order = -1;
        try {
            if (Tools.isNumeric(sortOrder)) {
                order = Integer.valueOf(sortOrder);
            }
        } catch (Exception e) {
            BakcellLogger.logE("SortOrderX12", "sortOrderError:::" + e.toString(), "MenuHorizontalItem", "getSortOrderInt");
        }
        return order;
    }
}