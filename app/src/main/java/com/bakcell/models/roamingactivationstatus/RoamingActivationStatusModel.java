package com.bakcell.models.roamingactivationstatus;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RoamingActivationStatusModel implements Parcelable {
    @SerializedName("active")
    private boolean active;

    protected RoamingActivationStatusModel(Parcel in) {
        active = in.readInt() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(active ? 1 : 0);;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoamingActivationStatusModel> CREATOR = new Creator<RoamingActivationStatusModel>() {
        @Override
        public RoamingActivationStatusModel createFromParcel(Parcel in) {
            return new RoamingActivationStatusModel(in);
        }

        @Override
        public RoamingActivationStatusModel[] newArray(int size) {
            return new RoamingActivationStatusModel[size];
        }
    };

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
