package com.bakcell.models.storelocator;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 29-Sep-17.
 */

public class StoreLocator implements Parcelable {

    @SerializedName("address")
    private String address;
    @SerializedName("city")
    private String city;
    @SerializedName("id")
    private String id;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("type")
    private String type;
    @SerializedName("store_name")
    private String store_name;
    @SerializedName("contactNumbers")
    private ArrayList<String> contactNumbers;
    @SerializedName("timing")
    private ArrayList<StoreTime> timings;

    public StoreLocator() {
    }

    protected StoreLocator(Parcel in) {
        address = in.readString();
        city = in.readString();
        id = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        type = in.readString();
        store_name = in.readString();
        contactNumbers = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(id);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(type);
        dest.writeString(store_name);
        dest.writeStringList(contactNumbers);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StoreLocator> CREATOR = new Creator<StoreLocator>() {
        @Override
        public StoreLocator createFromParcel(Parcel in) {
            return new StoreLocator(in);
        }

        @Override
        public StoreLocator[] newArray(int size) {
            return new StoreLocator[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public ArrayList<String> getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(ArrayList<String> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public ArrayList<StoreTime> getTimings() {
        return timings;
    }

    public void setTimings(ArrayList<StoreTime> timings) {
        this.timings = timings;
    }
}
