package com.bakcell.models.storelocator;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 29-Sep-17.
 */

public class StoreTime implements Parcelable {

    @SerializedName("day")
    private String day;
    @SerializedName("timings")
    private String timings;

    protected StoreTime(Parcel in) {
        day = in.readString();
        timings = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(day);
        dest.writeString(timings);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StoreTime> CREATOR = new Creator<StoreTime>() {
        @Override
        public StoreTime createFromParcel(Parcel in) {
            return new StoreTime(in);
        }

        @Override
        public StoreTime[] newArray(int size) {
            return new StoreTime[size];
        }
    };

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }
}
