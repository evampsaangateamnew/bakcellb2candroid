package com.bakcell.models.storelocator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeeshan Shabbir on 7/31/2017.
 */

public class TempStoreLocator {
    List<TempStoreTimings> tempStoreTimingsList = new ArrayList<>();
    List<TempStoreContact> tempStoreContactList = new ArrayList<>();

    public List<TempStoreTimings> getTempStoreTimingsList() {
        return tempStoreTimingsList;
    }

    public void setTempStoreTimingsList(List<TempStoreTimings> tempStoreTimingsList) {
        this.tempStoreTimingsList = tempStoreTimingsList;
    }

    public List<TempStoreContact> getTempStoreContactList() {
        return tempStoreContactList;
    }

    public void setTempStoreContactList(List<TempStoreContact> tempStoreContactList) {
        this.tempStoreContactList = tempStoreContactList;
    }
}
