package com.bakcell.models.storelocator;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 29-Sep-17.
 */

public class StoreLocatorMain implements Parcelable {
    @SerializedName("type")
    private ArrayList<String> type;
    @SerializedName("city")
    private ArrayList<String> city;
    @SerializedName("stores")
    private ArrayList<StoreLocator> stores;

    protected StoreLocatorMain(Parcel in) {
        type = in.createStringArrayList();
        city = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(type);
        dest.writeStringList(city);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StoreLocatorMain> CREATOR = new Creator<StoreLocatorMain>() {
        @Override
        public StoreLocatorMain createFromParcel(Parcel in) {
            return new StoreLocatorMain(in);
        }

        @Override
        public StoreLocatorMain[] newArray(int size) {
            return new StoreLocatorMain[size];
        }
    };

    public ArrayList<String> getType() {
        return type;
    }

    public void setType(ArrayList<String> type) {
        this.type = type;
    }

    public ArrayList<String> getCity() {
        return city;
    }

    public void setCity(ArrayList<String> city) {
        this.city = city;
    }

    public ArrayList<StoreLocator> getStores() {
        return stores;
    }

    public void setStores(ArrayList<StoreLocator> stores) {
        this.stores = stores;
    }
}
