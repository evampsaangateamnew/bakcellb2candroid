package com.bakcell.models.storelocator;

/**
 * Created by Zeeshan Shabbir on 7/31/2017.
 */

public class TempStoreTimings {

    String days = "Sundays";
    String timing = "10:00 - 11:00";

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }
}
