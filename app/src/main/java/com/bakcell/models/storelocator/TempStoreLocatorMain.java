package com.bakcell.models.storelocator;

import java.util.List;

/**
 * Created by Zeeshan Shabbir on 7/31/2017.
 */

public class TempStoreLocatorMain {
    String storeName;
    String Location;
    List<TempStoreLocator> tempStoreLocatorList;

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public List<TempStoreLocator> getTempStoreLocatorList() {
        return tempStoreLocatorList;
    }

    public void setTempStoreLocatorList(List<TempStoreLocator> tempStoreLocatorList) {
        this.tempStoreLocatorList = tempStoreLocatorList;
    }
}
