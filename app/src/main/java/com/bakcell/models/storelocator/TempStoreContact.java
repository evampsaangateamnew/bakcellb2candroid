package com.bakcell.models.storelocator;

/**
 * Created by Zeeshan Shabbir on 7/31/2017.
 */

public class TempStoreContact {
    String contactNumber;

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}
