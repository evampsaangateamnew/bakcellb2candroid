package com.bakcell.models.fastpayments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FastTopUpData implements Parcelable {
    @SerializedName("fastPaymentDetails")
    private List<FastTopUpDetail> fastPaymentDetails = null;

    protected FastTopUpData(Parcel in) {
        fastPaymentDetails = in.createTypedArrayList(FastTopUpDetail.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(fastPaymentDetails);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FastTopUpData> CREATOR = new Creator<FastTopUpData>() {
        @Override
        public FastTopUpData createFromParcel(Parcel in) {
            return new FastTopUpData(in);
        }

        @Override
        public FastTopUpData[] newArray(int size) {
            return new FastTopUpData[size];
        }
    };

    public List<FastTopUpDetail> getFastPaymentDetails() {
        return fastPaymentDetails;
    }

    public void setFastPaymentDetails(List<FastTopUpDetail> fastPaymentDetails) {
        this.fastPaymentDetails = fastPaymentDetails;
    }
}
