package com.bakcell.models.fastpayments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class FastTopUpDetail implements Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("cardType")
    private String cardType;
    @SerializedName("amount")
    private String amount;
    @SerializedName("topupNumber")
    private String topupNumber;
    @SerializedName("paymentKey")
    private String paymentKey;


    protected FastTopUpDetail(Parcel in) {
        id = in.readString();
        cardType = in.readString();
        amount = in.readString();
        topupNumber = in.readString();
        paymentKey = in.readString();
    }

    public FastTopUpDetail(){}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(cardType);
        dest.writeString(amount);
        dest.writeString(topupNumber);
        dest.writeString(paymentKey);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FastTopUpDetail> CREATOR = new Creator<FastTopUpDetail>() {
        @Override
        public FastTopUpDetail createFromParcel(Parcel in) {
            return new FastTopUpDetail(in);
        }

        @Override
        public FastTopUpDetail[] newArray(int size) {
            return new FastTopUpDetail[size];
        }
    };

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTopupNumber() {
        return topupNumber;
    }

    public void setTopupNumber(String topupNumber) {
        this.topupNumber = topupNumber;
    }

    public String getPaymentKey() {
        return paymentKey;
    }

    public void setPaymentKey(String paymentKey) {
        this.paymentKey = paymentKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
