package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DetailList implements Parcelable {
    @SerializedName("country_id")
    private String country_id;
    @SerializedName("operator_list")
    private ArrayList<Operator_list> operator_list;
    @SerializedName("country")
    private String country;

    protected DetailList(Parcel in) {
        country_id = in.readString();
        country = in.readString();
        operator_list = in.createTypedArrayList(Operator_list.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(country_id);
        dest.writeString(country);
        dest.writeTypedList(operator_list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetailList> CREATOR = new Creator<DetailList>() {
        @Override
        public DetailList createFromParcel(Parcel in) {
            return new DetailList(in);
        }

        @Override
        public DetailList[] newArray(int size) {
            return new DetailList[size];
        }
    };

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public ArrayList<Operator_list> getOperator_list() {
        return operator_list;
    }

    public void setOperator_list(ArrayList<Operator_list> operator_list) {
        this.operator_list = operator_list;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
