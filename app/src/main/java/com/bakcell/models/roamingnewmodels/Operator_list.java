package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Operator_list implements Parcelable {
    @SerializedName("service_list")
    private ArrayList<Service_list> service_list;
    @SerializedName("operator")
    private String operator;

    protected Operator_list(Parcel in) {
        operator = in.readString();
        service_list = in.createTypedArrayList(Service_list.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(operator);
        dest.writeTypedList(service_list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Operator_list> CREATOR = new Creator<Operator_list>() {
        @Override
        public Operator_list createFromParcel(Parcel in) {
            return new Operator_list(in);
        }

        @Override
        public Operator_list[] newArray(int size) {
            return new Operator_list[size];
        }
    };

    public ArrayList<Service_list> getService_list() {
        return service_list;
    }

    public void setService_list(ArrayList<Service_list> service_list) {
        this.service_list = service_list;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
