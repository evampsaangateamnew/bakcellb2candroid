package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ServiceListHelperModel implements Parcelable {
    public String service;
    public String serviceUnit;
    public ArrayList<ServiceTypeListHelperModel> serviceTypeListHelperModel;

    public ServiceListHelperModel(String service, String serviceUnit, ArrayList<ServiceTypeListHelperModel> serviceTypeListHelperModel) {
        this.service = service;
        this.serviceUnit = serviceUnit;
        this.serviceTypeListHelperModel = serviceTypeListHelperModel;
    }

    protected ServiceListHelperModel(Parcel in) {
        service = in.readString();
        serviceUnit = in.readString();
        serviceTypeListHelperModel = in.createTypedArrayList(ServiceTypeListHelperModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(service);
        dest.writeString(serviceUnit);
        dest.writeTypedList(serviceTypeListHelperModel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceListHelperModel> CREATOR = new Creator<ServiceListHelperModel>() {
        @Override
        public ServiceListHelperModel createFromParcel(Parcel in) {
            return new ServiceListHelperModel(in);
        }

        @Override
        public ServiceListHelperModel[] newArray(int size) {
            return new ServiceListHelperModel[size];
        }
    };
}
