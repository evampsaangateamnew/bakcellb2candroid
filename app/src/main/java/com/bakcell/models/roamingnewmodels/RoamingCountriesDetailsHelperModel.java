package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RoamingCountriesDetailsHelperModel implements Parcelable {
    public String operator;
    public ArrayList<ServiceListHelperModel> serviceListHelperModel;


    public RoamingCountriesDetailsHelperModel(String operator, ArrayList<ServiceListHelperModel> serviceListHelperModel) {
        this.operator = operator;
        this.serviceListHelperModel = serviceListHelperModel;

    }

    protected RoamingCountriesDetailsHelperModel(Parcel in) {
        operator = in.readString();
        serviceListHelperModel = in.createTypedArrayList(ServiceListHelperModel.CREATOR);

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(operator);
        dest.writeTypedList(serviceListHelperModel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoamingCountriesDetailsHelperModel> CREATOR = new Creator<RoamingCountriesDetailsHelperModel>() {
        @Override
        public RoamingCountriesDetailsHelperModel createFromParcel(Parcel in) {
            return new RoamingCountriesDetailsHelperModel(in);
        }

        @Override
        public RoamingCountriesDetailsHelperModel[] newArray(int size) {
            return new RoamingCountriesDetailsHelperModel[size];
        }
    };
}
