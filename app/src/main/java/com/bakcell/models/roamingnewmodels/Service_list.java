package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Service_list implements Parcelable {
    @SerializedName("service_unit")
    private String service_unit;
    @SerializedName("service")
    private String service;
    @SerializedName("service_type_list")
    private ArrayList<Service_type_list> service_type_list;

    protected Service_list(Parcel in) {
        service_unit = in.readString();
        service = in.readString();
        service_type_list = in.createTypedArrayList(Service_type_list.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(service_unit);
        dest.writeString(service);
        dest.writeTypedList(service_type_list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Service_list> CREATOR = new Creator<Service_list>() {
        @Override
        public Service_list createFromParcel(Parcel in) {
            return new Service_list(in);
        }

        @Override
        public Service_list[] newArray(int size) {
            return new Service_list[size];
        }
    };

    public String getService_unit() {
        return service_unit;
    }

    public void setService_unit(String service_unit) {
        this.service_unit = service_unit;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public ArrayList<Service_type_list> getService_type_list() {
        return service_type_list;
    }

    public void setService_type_list(ArrayList<Service_type_list> service_type_list) {
        this.service_type_list = service_type_list;
    }
}
