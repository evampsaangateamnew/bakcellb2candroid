package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Service_type_list implements Parcelable {
    @SerializedName("unit")
    private String unit;
    @SerializedName("service_type")
    private String service_type;
    @SerializedName("value")
    private String value;

    protected Service_type_list(Parcel in) {
        unit = in.readString();
        service_type = in.readString();
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(unit);
        dest.writeString(service_type);
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Service_type_list> CREATOR = new Creator<Service_type_list>() {
        @Override
        public Service_type_list createFromParcel(Parcel in) {
            return new Service_type_list(in);
        }

        @Override
        public Service_type_list[] newArray(int size) {
            return new Service_type_list[size];
        }
    };

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
