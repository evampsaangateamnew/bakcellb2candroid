package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceTypeListHelperModel implements Parcelable {
    public String unit;
    public String value;
    public String type;

    public ServiceTypeListHelperModel(String unit, String value, String type) {
        this.unit = unit;
        this.value = value;
        this.type = type;
    }

    protected ServiceTypeListHelperModel(Parcel in) {
        unit = in.readString();
        value = in.readString();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(unit);
        dest.writeString(value);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceTypeListHelperModel> CREATOR = new Creator<ServiceTypeListHelperModel>() {
        @Override
        public ServiceTypeListHelperModel createFromParcel(Parcel in) {
            return new ServiceTypeListHelperModel(in);
        }

        @Override
        public ServiceTypeListHelperModel[] newArray(int size) {
            return new ServiceTypeListHelperModel[size];
        }
    };
}
