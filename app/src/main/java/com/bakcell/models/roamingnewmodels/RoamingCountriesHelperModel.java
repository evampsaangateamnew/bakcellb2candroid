package com.bakcell.models.roamingnewmodels;

public class RoamingCountriesHelperModel {
    public String country;
    public String countryId;

    public RoamingCountriesHelperModel(String country, String countryId) {
        this.country = country;
        this.countryId = countryId;
    }
}
