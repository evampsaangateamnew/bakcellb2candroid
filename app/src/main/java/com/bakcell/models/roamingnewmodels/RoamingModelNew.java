package com.bakcell.models.roamingnewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
//using this model since 15-Jan-2018 due to ios preference change in response structure
public class RoamingModelNew implements Parcelable {
    @SerializedName("countryList")
    private ArrayList<CountryList> countryList;
    @SerializedName("detailList")
    private ArrayList<DetailList> detailList;

    protected RoamingModelNew(Parcel in) {
        countryList = in.createTypedArrayList(CountryList.CREATOR);
        detailList = in.createTypedArrayList(DetailList.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(countryList);
        dest.writeTypedList(detailList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoamingModelNew> CREATOR = new Creator<RoamingModelNew>() {
        @Override
        public RoamingModelNew createFromParcel(Parcel in) {
            return new RoamingModelNew(in);
        }

        @Override
        public RoamingModelNew[] newArray(int size) {
            return new RoamingModelNew[size];
        }
    };

    public ArrayList<CountryList> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<CountryList> countryList) {
        this.countryList = countryList;
    }

    public ArrayList<DetailList> getDetailList() {
        return detailList;
    }

    public void setDetailList(ArrayList<DetailList> detailList) {
        this.detailList = detailList;
    }
}
