package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan on 1/18/2018.
 */

public class RedirectionLinks implements Parcelable {
    @SerializedName("onlinePaymentGatewayURL_AZ")
    String onlinePaymentGatewayURL_AZ;
    @SerializedName("onlinePaymentGatewayURL_EN")
    String onlinePaymentGatewayURL_EN;
    @SerializedName("onlinePaymentGatewayURL_RU")
    String onlinePaymentGatewayURL_RU;

    protected RedirectionLinks(Parcel in) {
        onlinePaymentGatewayURL_AZ = in.readString();
        onlinePaymentGatewayURL_EN = in.readString();
        onlinePaymentGatewayURL_RU = in.readString();
    }

    public static final Creator<RedirectionLinks> CREATOR = new Creator<RedirectionLinks>() {
        @Override
        public RedirectionLinks createFromParcel(Parcel in) {
            return new RedirectionLinks(in);
        }

        @Override
        public RedirectionLinks[] newArray(int size) {
            return new RedirectionLinks[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(onlinePaymentGatewayURL_AZ);
        parcel.writeString(onlinePaymentGatewayURL_EN);
        parcel.writeString(onlinePaymentGatewayURL_RU);
    }

    public String getOnlinePaymentGatewayURL_AZ() {
        return onlinePaymentGatewayURL_AZ;
    }

    public void setOnlinePaymentGatewayURL_AZ(String onlinePaymentGatewayURL_AZ) {
        this.onlinePaymentGatewayURL_AZ = onlinePaymentGatewayURL_AZ;
    }

    public String getOnlinePaymentGatewayURL_EN() {
        return onlinePaymentGatewayURL_EN;
    }

    public void setOnlinePaymentGatewayURL_EN(String onlinePaymentGatewayURL_EN) {
        this.onlinePaymentGatewayURL_EN = onlinePaymentGatewayURL_EN;
    }

    public String getOnlinePaymentGatewayURL_RU() {
        return onlinePaymentGatewayURL_RU;
    }

    public void setOnlinePaymentGatewayURL_RU(String onlinePaymentGatewayURL_RU) {
        this.onlinePaymentGatewayURL_RU = onlinePaymentGatewayURL_RU;
    }
}
