package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/20/2017.
 */

public class PredefinedDataItem implements Parcelable {

    @SerializedName("text")
    private String text;
    @SerializedName("amount")
    private String amount;
    @SerializedName("currency")
    private String currency;


    protected PredefinedDataItem(Parcel in) {
        text = in.readString();
        amount = in.readString();
        currency = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeString(amount);
        dest.writeString(currency);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PredefinedDataItem> CREATOR = new Creator<PredefinedDataItem>() {
        @Override
        public PredefinedDataItem createFromParcel(Parcel in) {
            return new PredefinedDataItem(in);
        }

        @Override
        public PredefinedDataItem[] newArray(int size) {
            return new PredefinedDataItem[size];
        }
    };

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
