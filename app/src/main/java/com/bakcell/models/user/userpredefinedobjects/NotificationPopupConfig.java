package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Junaid Hassan on 01, September, 2020
 * Senior Android Engineer. Islamabad Pakistan
 */
public class NotificationPopupConfig implements Parcelable {

    @SerializedName("hour")
    private String hour;
    @SerializedName("popupMessage")
    private String popupMessage;

    protected NotificationPopupConfig(Parcel in) {
        hour = in.readString();
        popupMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(hour);
        dest.writeString(popupMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NotificationPopupConfig> CREATOR = new Creator<NotificationPopupConfig>() {
        @Override
        public NotificationPopupConfig createFromParcel(Parcel in) {
            return new NotificationPopupConfig(in);
        }

        @Override
        public NotificationPopupConfig[] newArray(int size) {
            return new NotificationPopupConfig[size];
        }
    };

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPopupMessage() {
        return popupMessage;
    }

    public void setPopupMessage(String popupMessage) {
        this.popupMessage = popupMessage;
    }
}
