package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CardType implements Parcelable {
    @SerializedName("key")
    private long key;
    @SerializedName("value")
    private String value;

    protected CardType(Parcel in) {
        key = in.readLong();
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(key);
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CardType> CREATOR = new Creator<CardType>() {
        @Override
        public CardType createFromParcel(Parcel in) {
            return new CardType(in);
        }

        @Override
        public CardType[] newArray(int size) {
            return new CardType[size];
        }
    };

    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
