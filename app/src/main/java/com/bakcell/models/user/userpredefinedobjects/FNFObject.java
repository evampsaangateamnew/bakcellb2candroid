package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 03-Oct-17.
 */

public class FNFObject implements Parcelable {

    @SerializedName("maxFNFCount")
    private String maxFNFCount;

    @SerializedName("fnFAllowed")
    private String fnFAllowed;

    protected FNFObject(Parcel in) {
        maxFNFCount = in.readString();
    }

    public static final Creator<FNFObject> CREATOR = new Creator<FNFObject>() {
        @Override
        public FNFObject createFromParcel(Parcel in) {
            return new FNFObject(in);
        }

        @Override
        public FNFObject[] newArray(int size) {
            return new FNFObject[size];
        }
    };

    public String getMaxFNFCount() {
        return maxFNFCount;
    }

    public void setMaxFNFCount(String maxFNFCount) {
        this.maxFNFCount = maxFNFCount;
    }

    public String isFnFAllowed() {
        return fnFAllowed;
    }

    public void setFnFAllowed(String fnFAllowed) {
        this.fnFAllowed = fnFAllowed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(maxFNFCount);
        dest.writeString(fnFAllowed);
    }
}
