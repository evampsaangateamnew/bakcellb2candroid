package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/20/2017.
 */

public class GetLoan implements Parcelable {

    @SerializedName("selectAmount")
    private ObjectSelectAmount selectAmount;


    protected GetLoan(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetLoan> CREATOR = new Creator<GetLoan>() {
        @Override
        public GetLoan createFromParcel(Parcel in) {
            return new GetLoan(in);
        }

        @Override
        public GetLoan[] newArray(int size) {
            return new GetLoan[size];
        }
    };

    public ObjectSelectAmount getSelectAmount() {
        return selectAmount;
    }

    public void setSelectAmount(ObjectSelectAmount selectAmount) {
        this.selectAmount = selectAmount;
    }
}
