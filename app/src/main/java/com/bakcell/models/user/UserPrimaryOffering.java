package com.bakcell.models.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 21-Aug-17.
 */

public class UserPrimaryOffering implements Parcelable {

    @SerializedName("OfferingId")
    private String OfferingId;
    @SerializedName("OfferingName")
    private String OfferingName;
    @SerializedName("OfferingCode")
    private String OfferingCode;
    @SerializedName("OfferingShortName")
    private String OfferingShortName;
    @SerializedName("Status")
    private String Status;
    @SerializedName("NetworkType")
    private String NetworkType;
    @SerializedName("EffectiveTime")
    private String EffectiveTime;
    @SerializedName("ExpiredTime")
    private String ExpiredTime;

    public String getOfferingId() {
        return OfferingId;
    }

    public void setOfferingId(String offeringId) {
        OfferingId = offeringId;
    }

    public String getOfferingName() {
        return OfferingName;
    }

    public void setOfferingName(String offeringName) {
        OfferingName = offeringName;
    }

    public String getOfferingCode() {
        return OfferingCode;
    }

    public void setOfferingCode(String offeringCode) {
        OfferingCode = offeringCode;
    }

    public String getOfferingShortName() {
        return OfferingShortName;
    }

    public void setOfferingShortName(String offeringShortName) {
        OfferingShortName = offeringShortName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getNetworkType() {
        return NetworkType;
    }

    public void setNetworkType(String networkType) {
        NetworkType = networkType;
    }

    public String getEffectiveTime() {
        return EffectiveTime;
    }

    public void setEffectiveTime(String effectiveTime) {
        EffectiveTime = effectiveTime;
    }

    public String getExpiredTime() {
        return ExpiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        ExpiredTime = expiredTime;
    }


    private UserPrimaryOffering() {
    }

    private static UserPrimaryOffering newInstance = null;

    public static UserPrimaryOffering getInstance() {
        if (newInstance == null) newInstance = new UserPrimaryOffering();
        return newInstance;
    }


    protected UserPrimaryOffering(Parcel in) {
        OfferingId = in.readString();
        OfferingName = in.readString();
        OfferingCode = in.readString();
        OfferingShortName = in.readString();
        Status = in.readString();
        NetworkType = in.readString();
        EffectiveTime = in.readString();
        ExpiredTime = in.readString();
    }

    public static final Creator<UserPrimaryOffering> CREATOR = new Creator<UserPrimaryOffering>() {
        @Override
        public UserPrimaryOffering createFromParcel(Parcel in) {
            return new UserPrimaryOffering(in);
        }

        @Override
        public UserPrimaryOffering[] newArray(int size) {
            return new UserPrimaryOffering[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(OfferingId);
        dest.writeString(OfferingName);
        dest.writeString(OfferingCode);
        dest.writeString(OfferingShortName);
        dest.writeString(Status);
        dest.writeString(NetworkType);
        dest.writeString(EffectiveTime);
        dest.writeString(ExpiredTime);
    }
}
