package com.bakcell.models.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Noman
 */

public class UserModel implements Parcelable {

    @SerializedName("entityId")
    private String entityId;
    @SerializedName("customerId")
    private String customerId;
    @SerializedName("accountId")
    private String accountId;
    @SerializedName("brandId")
    private String brandId;
    @SerializedName("customerType")
    private String customerType;
    @SerializedName("dob")
    private String dob;
    @SerializedName("effectiveDate")
    private String effectiveDate;
    @SerializedName("expiryDate")
    private String expiryDate;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("gender")
    private String gender;
    @SerializedName("billingLanguage")
    private String language;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("loyaltySegment")
    private String loyaltySegment;
    @SerializedName("middleName")
    private String middleName;
    @SerializedName("msisdn")
    private String msisdn;
    @SerializedName("offeringId")
    private String offeringId;
    @SerializedName("status")
    private String status;
    @SerializedName("statusDetails")
    private String statusDetails;
    @SerializedName("subscriberType")
    private String subscriberType;
    @SerializedName("title")
    private String title;
    @SerializedName("token")
    private String token;
    @SerializedName("brandName")
    private String brandName;
    @SerializedName("offeringName")
    private String offeringName;
    @SerializedName("email")
    private String email;
    @SerializedName("puk")
    private String puk;
    @SerializedName("pin")
    private String pin;
    @SerializedName("sim")
    private String sim;
    @SerializedName("imageURL")
    private String imageURL;
    @SerializedName("offeringNameDisplay")
    private String offeringNameDisplay;
    @SerializedName("groupType")
    private String groupType;
    @SerializedName("firstPopup")
    private String firstPopup;
    @SerializedName("lateOnPopup")
    private String lateOnPopup;
    @SerializedName("popupTitle")
    private String popupTitle;
    @SerializedName("popupContent")
    private String popupContent;
    @SerializedName("hideNumberTariffIds")
    private ArrayList<String> hideNumberTariffIds;
    @SerializedName("cinTariff")
    private String cinTariff;

    private UserModel() {
    }

    private static UserModel userModelInstance = null;

    public static UserModel getInstance() {
        if (userModelInstance == null) userModelInstance = new UserModel();
        return userModelInstance;
    }

    protected UserModel(Parcel in) {
        entityId = in.readString();
        hideNumberTariffIds = in.createStringArrayList();
        customerId = in.readString();
        accountId = in.readString();
        brandId = in.readString();
        customerType = in.readString();
        dob = in.readString();
        effectiveDate = in.readString();
        expiryDate = in.readString();
        firstName = in.readString();
        gender = in.readString();
        language = in.readString();
        lastName = in.readString();
        loyaltySegment = in.readString();
        middleName = in.readString();
        msisdn = in.readString();
        offeringId = in.readString();
        status = in.readString();
        statusDetails = in.readString();
        subscriberType = in.readString();
        title = in.readString();
        token = in.readString();
        brandName = in.readString();
        offeringName = in.readString();
        email = in.readString();
        puk = in.readString();
        pin = in.readString();
        sim = in.readString();
        imageURL = in.readString();
        offeringNameDisplay = in.readString();
        groupType = in.readString();
        firstPopup = in.readString();
        lateOnPopup = in.readString();
        popupTitle = in.readString();
        popupContent = in.readString();
        cinTariff = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(entityId);
        dest.writeString(customerId);
        dest.writeList(hideNumberTariffIds);
        dest.writeString(accountId);
        dest.writeString(brandId);
        dest.writeString(customerType);
        dest.writeString(dob);
        dest.writeString(effectiveDate);
        dest.writeString(expiryDate);
        dest.writeString(firstName);
        dest.writeString(gender);
        dest.writeString(language);
        dest.writeString(lastName);
        dest.writeString(loyaltySegment);
        dest.writeString(middleName);
        dest.writeString(msisdn);
        dest.writeString(offeringId);
        dest.writeString(status);
        dest.writeString(statusDetails);
        dest.writeString(subscriberType);
        dest.writeString(title);
        dest.writeString(token);
        dest.writeString(brandName);
        dest.writeString(offeringName);
        dest.writeString(email);
        dest.writeString(puk);
        dest.writeString(pin);
        dest.writeString(sim);
        dest.writeString(imageURL);
        dest.writeString(offeringNameDisplay);
        dest.writeString(groupType);
        dest.writeString(firstPopup);
        dest.writeString(lateOnPopup);
        dest.writeString(popupTitle);
        dest.writeString(popupContent);
        dest.writeString(cinTariff);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getCinTariff() {
        return cinTariff;
    }

    public void setCinTariff(String cinTariff) {
        this.cinTariff = cinTariff;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFirstPopup() {
        return firstPopup;
    }

    public void setFirstPopup(String firstPopup) {
        this.firstPopup = firstPopup;
    }

    public String getLateOnPopup() {
        return lateOnPopup;
    }

    public void setLateOnPopup(String lateOnPopup) {
        this.lateOnPopup = lateOnPopup;
    }

    public String getPopupTitle() {
        return popupTitle;
    }

    public void setPopupTitle(String popupTitle) {
        this.popupTitle = popupTitle;
    }

    public String getPopupContent() {
        return popupContent;
    }

    public void setPopupContent(String popupContent) {
        this.popupContent = popupContent;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoyaltySegment() {
        return loyaltySegment;
    }

    public void setLoyaltySegment(String loyaltySegment) {
        this.loyaltySegment = loyaltySegment;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public String getSubscriberType() {
        return subscriberType;
    }

    public void setSubscriberType(String subscriberType) {
        this.subscriberType = subscriberType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getOfferingName() {
        return offeringName;
    }

    public void setOfferingName(String offeringName) {
        this.offeringName = offeringName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPuk() {
        return puk;
    }

    public void setPuk(String puk) {
        this.puk = puk;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getOfferingNameDisplay() {
        return offeringNameDisplay;
    }

    public void setOfferingNameDisplay(String offeringNameDisplay) {
        this.offeringNameDisplay = offeringNameDisplay;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public static UserModel getUserModelInstance() {
        return userModelInstance;
    }

    public static void setUserModelInstance(UserModel userModelInstance) {
        UserModel.userModelInstance = userModelInstance;
    }

    public ArrayList<String> getHideNumberTariffIds() {
        return hideNumberTariffIds;
    }

    public void setHideNumberTariffIds(ArrayList<String> hideNumberTariffIds) {
        this.hideNumberTariffIds = hideNumberTariffIds;
    }
}
