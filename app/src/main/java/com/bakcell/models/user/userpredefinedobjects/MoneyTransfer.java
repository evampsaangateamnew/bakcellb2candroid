package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/20/2017.
 */

public class MoneyTransfer implements Parcelable {

    @SerializedName("selectAmount")
    private ObjectSelectAmount selectAmount;
    @SerializedName("termsAndCondition")
    private ObjectSelectAmount termsAndCondition;

    public ObjectSelectAmount getSelectAmount() {
        return selectAmount;
    }

    public void setSelectAmount(ObjectSelectAmount selectAmount) {
        this.selectAmount = selectAmount;
    }

    public ObjectSelectAmount getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(ObjectSelectAmount termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }

    protected MoneyTransfer(Parcel in) {
        selectAmount = in.readParcelable(ObjectSelectAmount.class.getClassLoader());
        termsAndCondition = in.readParcelable(ObjectSelectAmount.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(selectAmount, flags);
        dest.writeParcelable(termsAndCondition, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MoneyTransfer> CREATOR = new Creator<MoneyTransfer>() {
        @Override
        public MoneyTransfer createFromParcel(Parcel in) {
            return new MoneyTransfer(in);
        }

        @Override
        public MoneyTransfer[] newArray(int size) {
            return new MoneyTransfer[size];
        }
    };
}
