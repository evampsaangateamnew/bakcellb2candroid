package com.bakcell.models.user.userpredefinedobjects;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan Shabbir on 9/20/2017.
 */

public class Topup implements Parcelable {

    @SerializedName("moneyTransfer")
    private MoneyTransfer moneyTransfer;
    @SerializedName("getLoan")
    private GetLoan getLoan;
    @SerializedName("plasticCard")
    private PlasticCard plasticCard;


    protected Topup(Parcel in) {
        moneyTransfer = in.readParcelable(MoneyTransfer.class.getClassLoader());
        getLoan = in.readParcelable(GetLoan.class.getClassLoader());
        plasticCard = in.readParcelable(PlasticCard.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(moneyTransfer, flags);
        dest.writeParcelable(getLoan, flags);
        dest.writeParcelable(plasticCard, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Topup> CREATOR = new Creator<Topup>() {
        @Override
        public Topup createFromParcel(Parcel in) {
            return new Topup(in);
        }

        @Override
        public Topup[] newArray(int size) {
            return new Topup[size];
        }
    };

    public MoneyTransfer getMoneyTransfer() {
        return moneyTransfer;
    }

    public void setMoneyTransfer(MoneyTransfer moneyTransfer) {
        this.moneyTransfer = moneyTransfer;
    }

    public GetLoan getGetLoan() {
        return getLoan;
    }

    public void setGetLoan(GetLoan getLoan) {
        this.getLoan = getLoan;
    }

    public PlasticCard getPlasticCard() {
        return plasticCard;
    }

    public void setPlasticCard(PlasticCard plasticCard) {
        this.plasticCard = plasticCard;
    }
}
