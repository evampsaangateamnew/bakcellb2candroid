package com.bakcell.models.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.user.userpredefinedobjects.FNFObject;
import com.bakcell.models.user.userpredefinedobjects.NotificationPopupConfig;
import com.bakcell.models.user.userpredefinedobjects.RedirectionLinks;
import com.bakcell.models.user.userpredefinedobjects.TariffMigrationPrices;
import com.bakcell.models.user.userpredefinedobjects.Topup;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Zeeshan Shabbir on 9/20/2017.
 */

public class UserPredefinedData implements Parcelable {
    @SerializedName("notificationPopupConfig")
    private NotificationPopupConfig notificationPopupConfig;
    @SerializedName("topup")
    private Topup topup;
    @SerializedName("fnf")
    private FNFObject fnf;
    @SerializedName("tariffMigrationPrices")
    ArrayList<TariffMigrationPrices> tariffMigrationPrices;
    @SerializedName("redirectionLinks")
    RedirectionLinks redirectionLinks;
    @SerializedName("liveChat")
    String liveChat;
    private static UserPredefinedData newInstance = null;
    @SerializedName("roamingVisible")
    boolean roamingVisible;
    @SerializedName("goldenPayMessages")
    HashMap<String,String> goldenPayMessages;

    public HashMap<String, String> getGoldenPayMessages() {
        return goldenPayMessages;
    }

    public void setGoldenPayMessages(HashMap<String, String> goldenPayMessages) {
        this.goldenPayMessages = goldenPayMessages;
    }

    public static UserPredefinedData getInstance() {
        if (newInstance == null) newInstance = new UserPredefinedData();
        return newInstance;
    }

    UserPredefinedData() {

    }


    protected UserPredefinedData(Parcel in) {
        notificationPopupConfig= in.readParcelable(NotificationPopupConfig.class.getClassLoader());
        topup = in.readParcelable(Topup.class.getClassLoader());
        fnf = in.readParcelable(FNFObject.class.getClassLoader());
        liveChat = in.readString();
        tariffMigrationPrices = in.createTypedArrayList(TariffMigrationPrices.CREATOR);
        redirectionLinks = in.readParcelable(RedirectionLinks.class.getClassLoader());
        roamingVisible =in.readInt() != 0; // 0 to false, 1 = true
//      goldenPayMessages = in.readHashMap(GoldenPayMessages.class.getClassLoader()); // as told by Junaid bhai

    }

    public static final Creator<UserPredefinedData> CREATOR = new Creator<UserPredefinedData>() {
        @Override
        public UserPredefinedData createFromParcel(Parcel in) {
            return new UserPredefinedData(in);
        }

        @Override
        public UserPredefinedData[] newArray(int size) {
            return new UserPredefinedData[size];
        }
    };

    public static UserPredefinedData getNewInstance() {
        return newInstance;
    }

    public static void setNewInstance(UserPredefinedData newInstance) {
        UserPredefinedData.newInstance = newInstance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Topup getTopup() {
        return topup;
    }

    public void setTopup(Topup topup) {
        this.topup = topup;
    }

    public void setLiveChat(String liveChat) {
        this.liveChat = liveChat;
    }

    public String getLiveChat() {
        return liveChat;
    }

    public FNFObject getFnf() {
        return fnf;
    }

    public void setFnf(FNFObject fnf) {
        this.fnf = fnf;
    }

    public ArrayList<TariffMigrationPrices> getTariffMigrationPrices() {
        return tariffMigrationPrices;
    }

    public void setTariffMigrationPrices(ArrayList<TariffMigrationPrices> tariffMigrationPrices) {
        this.tariffMigrationPrices = tariffMigrationPrices;
    }

    public RedirectionLinks getRedirectionLinks() {
        return redirectionLinks;
    }

    public void setRedirectionLinks(RedirectionLinks redirectionLinks) {
        this.redirectionLinks = redirectionLinks;
    }

    public NotificationPopupConfig getNotificationPopupConfig() {
        return notificationPopupConfig;
    }

    public void setNotificationPopupConfig(NotificationPopupConfig notificationPopupConfig) {
        this.notificationPopupConfig = notificationPopupConfig;
    }


    public boolean getRoamingVisible() {
        return roamingVisible;
    }

    public void setRoamingVisible(boolean roamingVisible) {
        this.roamingVisible = roamingVisible;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(notificationPopupConfig, i);
        parcel.writeParcelable(topup, i);
        parcel.writeParcelable(fnf, i);
        parcel.writeString(liveChat);
        parcel.writeTypedList(tariffMigrationPrices);
        parcel.writeParcelable(redirectionLinks, i);
        parcel.writeInt(roamingVisible ? 1 : 0); // 1 = true, 0 = false
//      parcel.writeMap(goldenPayMessages); // as told by junaid bhai
    }
}
