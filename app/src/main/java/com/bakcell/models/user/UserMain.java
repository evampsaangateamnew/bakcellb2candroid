package com.bakcell.models.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Freeware Sys on 26-Jul-17.
 */

public class UserMain implements Parcelable {

    @SerializedName("customerData")
    private UserModel customerData;

    @SerializedName("primaryOffering")
    private UserPrimaryOffering primaryOffering;

    @SerializedName("supplementaryOfferingList")
    private ArrayList<UserSupplementaryOffering> supplementaryOfferingList;

    @SerializedName("predefinedData")
    UserPredefinedData predefinedData;

    @SerializedName("promoMessage")
    private String promoMessage;

    protected UserMain(Parcel in) {
        customerData = in.readParcelable(UserModel.class.getClassLoader());
        primaryOffering = in.readParcelable(UserPrimaryOffering.class.getClassLoader());
        supplementaryOfferingList = in.createTypedArrayList(UserSupplementaryOffering.CREATOR);
        predefinedData = in.readParcelable(UserPredefinedData.class.getClassLoader());
        promoMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(customerData, flags);
        dest.writeString(promoMessage);
        dest.writeParcelable(primaryOffering, flags);
        dest.writeTypedList(supplementaryOfferingList);
        dest.writeParcelable(predefinedData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserMain> CREATOR = new Creator<UserMain>() {
        @Override
        public UserMain createFromParcel(Parcel in) {
            return new UserMain(in);
        }

        @Override
        public UserMain[] newArray(int size) {
            return new UserMain[size];
        }
    };

    public UserModel getCustomerData() {
        return customerData;
    }

    public void setCustomerData(UserModel customerData) {
        this.customerData = customerData;
    }

    public UserPrimaryOffering getPrimaryOffering() {
        return primaryOffering;
    }

    public void setPrimaryOffering(UserPrimaryOffering primaryOfferings) {
        this.primaryOffering = primaryOfferings;
    }

    public ArrayList<UserSupplementaryOffering> getSupplementaryOfferingList() {
        return supplementaryOfferingList;
    }

    public void setSupplementaryOfferingList(ArrayList<UserSupplementaryOffering> supplementaryOfferingList) {
        this.supplementaryOfferingList = supplementaryOfferingList;
    }

    public UserPredefinedData getPredefinedData() {
        return predefinedData;
    }

    public void setPredefinedData(UserPredefinedData predefinedData) {
        this.predefinedData = predefinedData;
    }

    public String getPromoMessage() {
        return promoMessage;
    }

    public void setPromoMessage(String promoMessage) {
        this.promoMessage = promoMessage;
    }
}
