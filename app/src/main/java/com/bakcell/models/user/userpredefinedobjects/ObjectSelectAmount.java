package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 08-Nov-17.
 */

public class ObjectSelectAmount implements Parcelable {

    @SerializedName("cin")
    private ArrayList<PredefinedDataItem> cin;
    @SerializedName("klass")
    private ArrayList<PredefinedDataItem> klass;


    protected ObjectSelectAmount(Parcel in) {
        cin = in.createTypedArrayList(PredefinedDataItem.CREATOR);
        klass = in.createTypedArrayList(PredefinedDataItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(cin);
        dest.writeTypedList(klass);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObjectSelectAmount> CREATOR = new Creator<ObjectSelectAmount>() {
        @Override
        public ObjectSelectAmount createFromParcel(Parcel in) {
            return new ObjectSelectAmount(in);
        }

        @Override
        public ObjectSelectAmount[] newArray(int size) {
            return new ObjectSelectAmount[size];
        }
    };

    public ArrayList<PredefinedDataItem> getCin() {
        return cin;
    }

    public void setCin(ArrayList<PredefinedDataItem> cin) {
        this.cin = cin;
    }

    public ArrayList<PredefinedDataItem> getKlass() {
        return klass;
    }

    public void setKlass(ArrayList<PredefinedDataItem> klass) {
        this.klass = klass;
    }
}
