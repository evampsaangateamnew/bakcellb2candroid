package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zeeshan on 1/17/2018.
 */

public class TariffMigrationPrices implements Parcelable {
    @SerializedName("key")
    String key;
    @SerializedName("value")
    String value;

    protected TariffMigrationPrices(Parcel in) {
        key = in.readString();
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TariffMigrationPrices> CREATOR = new Creator<TariffMigrationPrices>() {
        @Override
        public TariffMigrationPrices createFromParcel(Parcel in) {
            return new TariffMigrationPrices(in);
        }

        @Override
        public TariffMigrationPrices[] newArray(int size) {
            return new TariffMigrationPrices[size];
        }
    };

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
