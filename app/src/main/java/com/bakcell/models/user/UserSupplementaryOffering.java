package com.bakcell.models.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 21-Aug-17.
 */

public class UserSupplementaryOffering implements Parcelable {

    @SerializedName("offeringId")
    private String offeringId;
    @SerializedName("offeringName")
    private String offeringName;
    @SerializedName("offeringCode")
    private String offeringCode;
    @SerializedName("offeringShortName")
    private String offeringShortName;
    @SerializedName("status")
    private String status;
    @SerializedName("networkType")
    private String networkType;
    @SerializedName("effectiveTime")
    private String effectiveTime;
    @SerializedName("expiredTime")
    private String expiredTime;


    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getOfferingName() {
        return offeringName;
    }

    public void setOfferingName(String offeringName) {
        this.offeringName = offeringName;
    }

    public String getOfferingCode() {
        return offeringCode;
    }

    public void setOfferingCode(String offeringCode) {
        this.offeringCode = offeringCode;
    }

    public String getOfferingShortName() {
        return offeringShortName;
    }

    public void setOfferingShortName(String offeringShortName) {
        this.offeringShortName = offeringShortName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(String effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public String getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }

    protected UserSupplementaryOffering(Parcel in) {
        offeringId = in.readString();
        offeringName = in.readString();
        offeringCode = in.readString();
        offeringShortName = in.readString();
        status = in.readString();
        networkType = in.readString();
        effectiveTime = in.readString();
        expiredTime = in.readString();
    }

    public static final Creator<UserSupplementaryOffering> CREATOR = new Creator<UserSupplementaryOffering>() {
        @Override
        public UserSupplementaryOffering createFromParcel(Parcel in) {
            return new UserSupplementaryOffering(in);
        }

        @Override
        public UserSupplementaryOffering[] newArray(int size) {
            return new UserSupplementaryOffering[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(offeringId);
        dest.writeString(offeringName);
        dest.writeString(offeringCode);
        dest.writeString(offeringShortName);
        dest.writeString(status);
        dest.writeString(networkType);
        dest.writeString(effectiveTime);
        dest.writeString(expiredTime);
    }
}
