package com.bakcell.models.user.userpredefinedobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlasticCard implements Parcelable {
    @SerializedName("cardTypes")
    private List<CardType> cardTypes = null;

    protected PlasticCard(Parcel in) {
        cardTypes = in.createTypedArrayList(CardType.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(cardTypes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlasticCard> CREATOR = new Creator<PlasticCard>() {
        @Override
        public PlasticCard createFromParcel(Parcel in) {
            return new PlasticCard(in);
        }

        @Override
        public PlasticCard[] newArray(int size) {
            return new PlasticCard[size];
        }
    };

    public List<CardType> getCardTypes() {
        return cardTypes;
    }

    public void setCardTypes(List<CardType> cardTypes) {
        this.cardTypes = cardTypes;
    }
}
