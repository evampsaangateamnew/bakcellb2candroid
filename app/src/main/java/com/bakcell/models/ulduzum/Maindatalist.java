package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Maindatalist implements Parcelable {
    @SerializedName("logo_small")
    private Logo_small logo_small;
    @SerializedName("segment_b_discount")
    private String segment_b_discount;
    @SerializedName("segment_a_discount")
    private String segment_a_discount;
    @SerializedName("short_code")
    private String short_code;
    @SerializedName("coor_lng")
    private String coor_lng;
    @SerializedName("app_redmsharename")
    private String app_redmsharename;
    @SerializedName("app_sharename")
    private String app_sharename;
    @SerializedName("category_id")
    private String category_id;
    @SerializedName("contact_person")
    private String contact_person;
    @SerializedName("max_discount_amount")
    private String max_discount_amount;
    @SerializedName("segment_c_discount")
    private String segment_c_discount;
    @SerializedName("id")
    private String id;
    @SerializedName("category_name")
    private String category_name;
    @SerializedName("address")
    private String address;
    @SerializedName("coor_lat")
    private String coor_lat;
    @SerializedName("description")
    private String description;
    @SerializedName("name")
    private String name;
    @SerializedName("min_discount_amount")
    private String min_discount_amount;
    @SerializedName("telephone")
    private String telephone;
    @SerializedName("parent_id")
    private String parent_id;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("loyality_segment")
    private String loyality_segment;

    protected Maindatalist(Parcel in) {
        segment_b_discount = in.readString();
        segment_a_discount = in.readString();
        short_code = in.readString();
        loyality_segment = in.readString();
        coor_lng = in.readString();
        app_redmsharename = in.readString();
        app_sharename = in.readString();
        category_id = in.readString();
        contact_person = in.readString();
        max_discount_amount = in.readString();
        segment_c_discount = in.readString();
        id = in.readString();
        category_name = in.readString();
        address = in.readString();
        coor_lat = in.readString();
        description = in.readString();
        name = in.readString();
        min_discount_amount = in.readString();
        telephone = in.readString();
        parent_id = in.readString();
        mobile = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(segment_b_discount);
        dest.writeString(segment_a_discount);
        dest.writeString(short_code);
        dest.writeString(coor_lng);
        dest.writeString(loyality_segment);
        dest.writeString(app_redmsharename);
        dest.writeString(app_sharename);
        dest.writeString(category_id);
        dest.writeString(contact_person);
        dest.writeString(max_discount_amount);
        dest.writeString(segment_c_discount);
        dest.writeString(id);
        dest.writeString(category_name);
        dest.writeString(address);
        dest.writeString(coor_lat);
        dest.writeString(description);
        dest.writeString(name);
        dest.writeString(min_discount_amount);
        dest.writeString(telephone);
        dest.writeString(parent_id);
        dest.writeString(mobile);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Maindatalist> CREATOR = new Creator<Maindatalist>() {
        @Override
        public Maindatalist createFromParcel(Parcel in) {
            return new Maindatalist(in);
        }

        @Override
        public Maindatalist[] newArray(int size) {
            return new Maindatalist[size];
        }
    };

    public String getLoyality_segment() {
        return loyality_segment;
    }

    public void setLoyality_segment(String loyality_segment) {
        this.loyality_segment = loyality_segment;
    }

    public Logo_small getLogo_small() {
        return logo_small;
    }

    public void setLogo_small(Logo_small logo_small) {
        this.logo_small = logo_small;
    }

    public String getSegment_b_discount() {
        return segment_b_discount;
    }

    public void setSegment_b_discount(String segment_b_discount) {
        this.segment_b_discount = segment_b_discount;
    }

    public String getSegment_a_discount() {
        return segment_a_discount;
    }

    public void setSegment_a_discount(String segment_a_discount) {
        this.segment_a_discount = segment_a_discount;
    }

    public String getShort_code() {
        return short_code;
    }

    public void setShort_code(String short_code) {
        this.short_code = short_code;
    }

    public String getCoor_lng() {
        return coor_lng;
    }

    public void setCoor_lng(String coor_lng) {
        this.coor_lng = coor_lng;
    }

    public String getApp_redmsharename() {
        return app_redmsharename;
    }

    public void setApp_redmsharename(String app_redmsharename) {
        this.app_redmsharename = app_redmsharename;
    }

    public String getApp_sharename() {
        return app_sharename;
    }

    public void setApp_sharename(String app_sharename) {
        this.app_sharename = app_sharename;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getMax_discount_amount() {
        return max_discount_amount;
    }

    public void setMax_discount_amount(String max_discount_amount) {
        this.max_discount_amount = max_discount_amount;
    }

    public String getSegment_c_discount() {
        return segment_c_discount;
    }

    public void setSegment_c_discount(String segment_c_discount) {
        this.segment_c_discount = segment_c_discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCoor_lat() {
        return coor_lat;
    }

    public void setCoor_lat(String coor_lat) {
        this.coor_lat = coor_lat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMin_discount_amount() {
        return min_discount_amount;
    }

    public void setMin_discount_amount(String min_discount_amount) {
        this.min_discount_amount = min_discount_amount;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
