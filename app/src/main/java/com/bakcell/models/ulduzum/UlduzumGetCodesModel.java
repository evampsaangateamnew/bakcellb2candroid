package com.bakcell.models.ulduzum;

import com.google.gson.annotations.SerializedName;

public class UlduzumGetCodesModel {
    @SerializedName("code")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
