package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UlduzumUsageDetails implements Parcelable {
    @SerializedName("total_amount")
    private String total_amount;
    @SerializedName("total_redemptions")
    private String total_redemptions;
    @SerializedName("daily_limit_left")
    private String daily_limit_left;
    @SerializedName("daily_limit_total")
    private String daily_limit_total;
    @SerializedName("total_discount")
    private String total_discount;
    @SerializedName("last_unused_code")
    private String last_unused_code;
    @SerializedName("total_discounted")
    private String total_discounted;
    @SerializedName("segment_type")
    private String segment_type;

    protected UlduzumUsageDetails(Parcel in) {
        total_amount = in.readString();
        total_redemptions = in.readString();
        daily_limit_left = in.readString();
        total_discount = in.readString();
        last_unused_code = in.readString();
        total_discounted = in.readString();
        segment_type = in.readString();
        daily_limit_total=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(total_amount);
        dest.writeString(total_redemptions);
        dest.writeString(daily_limit_left);
        dest.writeString(total_discount);
        dest.writeString(last_unused_code);
        dest.writeString(total_discounted);
        dest.writeString(segment_type);
        dest.writeString(daily_limit_total);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UlduzumUsageDetails> CREATOR = new Creator<UlduzumUsageDetails>() {
        @Override
        public UlduzumUsageDetails createFromParcel(Parcel in) {
            return new UlduzumUsageDetails(in);
        }

        @Override
        public UlduzumUsageDetails[] newArray(int size) {
            return new UlduzumUsageDetails[size];
        }
    };

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getTotal_redemptions() {
        return total_redemptions;
    }

    public void setTotal_redemptions(String total_redemptions) {
        this.total_redemptions = total_redemptions;
    }

    public String getDaily_limit_left() {
        return daily_limit_left;
    }

    public void setDaily_limit_left(String daily_limit_left) {
        this.daily_limit_left = daily_limit_left;
    }

    public String getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(String total_discount) {
        this.total_discount = total_discount;
    }

    public String getLast_unused_code() {
        return last_unused_code;
    }

    public void setLast_unused_code(String last_unused_code) {
        this.last_unused_code = last_unused_code;
    }

    public String getTotal_discounted() {
        return total_discounted;
    }

    public void setTotal_discounted(String total_discounted) {
        this.total_discounted = total_discounted;
    }

    public String getSegment_type() {
        return segment_type;
    }

    public void setSegment_type(String segment_type) {
        this.segment_type = segment_type;
    }

    public void setDaily_limit_total(String daily_limit_total) {
        this.daily_limit_total = daily_limit_total;
    }

    public String getDaily_limit_total() {
        return daily_limit_total;
    }
}
