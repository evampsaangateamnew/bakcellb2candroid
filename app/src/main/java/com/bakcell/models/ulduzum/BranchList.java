package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class BranchList implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("coord_lat")
    private String coord_lat;
    @SerializedName("coord_lng")
    private String coord_lng;

    public BranchList(Parcel in) {

        name = in.readString();
        coord_lat = in.readString();
        coord_lng = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(coord_lat);
        dest.writeString(coord_lng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BranchList> CREATOR = new Creator<BranchList>() {
        @Override
        public BranchList createFromParcel(Parcel in) {
            return new BranchList(in);
        }

        @Override
        public BranchList[] newArray(int size) {
            return new BranchList[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoord_lat() {
        return coord_lat;
    }

    public void setCoord_lat(String coord_lat) {
        this.coord_lat = coord_lat;
    }

    public String getCoord_lng() {
        return coord_lng;
    }

    public void setCoord_lng(String coord_lng) {
        this.coord_lng = coord_lng;
    }
}
