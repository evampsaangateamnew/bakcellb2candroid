package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MerchantModel implements Parcelable {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("category_id")
    private String category_id;
    @SerializedName("category_name")
    private String category_name;
    @SerializedName("logo")
    private String logo;
    @SerializedName("coord_lat")
    private String coord_lat;
    @SerializedName("coord_lng")
    private String coord_lng;
    @SerializedName("isspecialdisc")
    private String isspecialdisc;
    @SerializedName("discount")
    private String discount;
    @SerializedName("branches")
    private ArrayList<BranchList> branches;

    protected MerchantModel(Parcel in)
    {
        id = in.readString();
        name = in.readString();
        category_id = in.readString();
        category_name = in.readString();
        logo = in.readString();
        coord_lat = in.readString();
        coord_lng = in.readString();
        isspecialdisc = in.readString();
        discount = in.readString();
        branches = in.createTypedArrayList(BranchList.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(logo);
        dest.writeString(coord_lat);
        dest.writeString(coord_lng);
        dest.writeString(isspecialdisc);
        dest.writeString(discount);
        dest.writeTypedList(branches);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MerchantModel> CREATOR = new Creator<MerchantModel>() {
        @Override
        public MerchantModel createFromParcel(Parcel in) {
            return new MerchantModel(in);
        }

        @Override
        public MerchantModel[] newArray(int size) {
            return new MerchantModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCoord_lat() {
        return coord_lat;
    }

    public void setCoord_lat(String coord_lat) {
        this.coord_lat = coord_lat;
    }

    public String getCoord_lng() {
        return coord_lng;
    }

    public void setCoord_lng(String coord_lng) {
        this.coord_lng = coord_lng;
    }

    public String getIsspecialdisc() {
        return isspecialdisc;
    }

    public void setIsspecialdisc(String isspecialdisc) {
        this.isspecialdisc = isspecialdisc;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public ArrayList<BranchList> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<BranchList> branches) {
        this.branches = branches;
    }
}
