package com.bakcell.models.ulduzum;

public class UlduzumLocationsListModel {
    public String name;
    public String categoryName;
    public String latitude;
    public String longitude;
    public String categoryId;
    public String loyalitySegment;
    public String merchantId;

    public UlduzumLocationsListModel(String categoryType, String categoryName, String latitude, String longitude,  String categoryId, String loyalitySegment,String merchantId) {
        this.name = categoryType;
        this.categoryName = categoryName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.categoryId = categoryId;
        this.loyalitySegment = loyalitySegment;
        this.merchantId = merchantId;
    }
}
