package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UsageHistoryList implements Parcelable {
    @SerializedName("identical_code")
    private String identical_code;
    @SerializedName("merchant_name")
    private String merchant_name;
    @SerializedName("redemption_time")
    private String redemption_time;
    @SerializedName("category_id")
    private String category_id;
    @SerializedName("discount_amount")
    private String discount_amount;
    @SerializedName("discount_percent")
    private String discount_percent;
    @SerializedName("id")
    private String id;
    @SerializedName("amount")
    private String amount;
    @SerializedName("category_name")
    private String category_name;
    @SerializedName("redemption_date")
    private String redemption_date;
    @SerializedName("merchant_id")
    private String merchant_id;
    @SerializedName("payment_type")
    private String payment_type;
    @SerializedName("discounted_amount")
    private String discounted_amount;

    protected UsageHistoryList(Parcel in) {
        identical_code = in.readString();
        merchant_name = in.readString();
        redemption_time = in.readString();
        category_id = in.readString();
        discount_amount = in.readString();
        discount_percent = in.readString();
        id = in.readString();
        amount = in.readString();
        category_name = in.readString();
        redemption_date = in.readString();
        merchant_id = in.readString();
        payment_type = in.readString();
        discounted_amount = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identical_code);
        dest.writeString(merchant_name);
        dest.writeString(redemption_time);
        dest.writeString(category_id);
        dest.writeString(discount_amount);
        dest.writeString(discount_percent);
        dest.writeString(id);
        dest.writeString(amount);
        dest.writeString(category_name);
        dest.writeString(redemption_date);
        dest.writeString(merchant_id);
        dest.writeString(payment_type);
        dest.writeString(discounted_amount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsageHistoryList> CREATOR = new Creator<UsageHistoryList>() {
        @Override
        public UsageHistoryList createFromParcel(Parcel in) {
            return new UsageHistoryList(in);
        }

        @Override
        public UsageHistoryList[] newArray(int size) {
            return new UsageHistoryList[size];
        }
    };

    public String getIdentical_code() {
        return identical_code;
    }

    public void setIdentical_code(String identical_code) {
        this.identical_code = identical_code;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getRedemption_time() {
        return redemption_time;
    }

    public void setRedemption_time(String redemption_time) {
        this.redemption_time = redemption_time;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(String discount_percent) {
        this.discount_percent = discount_percent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getRedemption_date() {
        return redemption_date;
    }

    public void setRedemption_date(String redemption_date) {
        this.redemption_date = redemption_date;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getDiscounted_amount() {
        return discounted_amount;
    }

    public void setDiscounted_amount(String discounted_amount) {
        this.discounted_amount = discounted_amount;
    }
}
