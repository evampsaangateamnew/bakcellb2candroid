package com.bakcell.models.ulduzum;

public class UlduzumCategoriesSpinnerModel {
    public String category;
    public String categoryId;

    public UlduzumCategoriesSpinnerModel(String category,String categoryId) {
        this.category = category;
        this.categoryId = categoryId;
    }
}
