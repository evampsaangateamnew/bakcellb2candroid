package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Logo_small implements Parcelable {
    @SerializedName("height")
    private String height;
    @SerializedName("width")
    private String width;
    @SerializedName("type")
    private String type;
    @SerializedName("url")
    private String url;

    protected Logo_small(Parcel in) {
        height = in.readString();
        width = in.readString();
        type = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(height);
        dest.writeString(width);
        dest.writeString(type);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Logo_small> CREATOR = new Creator<Logo_small>() {
        @Override
        public Logo_small createFromParcel(Parcel in) {
            return new Logo_small(in);
        }

        @Override
        public Logo_small[] newArray(int size) {
            return new Logo_small[size];
        }
    };

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
