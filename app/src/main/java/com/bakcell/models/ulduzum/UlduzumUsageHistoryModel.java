package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UlduzumUsageHistoryModel implements Parcelable {
    @SerializedName("usageHistoryList")
    private ArrayList<UsageHistoryList> usageHistoryList;

    protected UlduzumUsageHistoryModel(Parcel in) {
        usageHistoryList = in.createTypedArrayList(UsageHistoryList.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(usageHistoryList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UlduzumUsageHistoryModel> CREATOR = new Creator<UlduzumUsageHistoryModel>() {
        @Override
        public UlduzumUsageHistoryModel createFromParcel(Parcel in) {
            return new UlduzumUsageHistoryModel(in);
        }

        @Override
        public UlduzumUsageHistoryModel[] newArray(int size) {
            return new UlduzumUsageHistoryModel[size];
        }
    };

    public ArrayList<UsageHistoryList> getUsageHistoryList() {
        return usageHistoryList;
    }

    public void setUsageHistoryList(ArrayList<UsageHistoryList> usageHistoryList) {
        this.usageHistoryList = usageHistoryList;
    }
}
