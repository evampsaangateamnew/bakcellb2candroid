package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UlduzumUnusedCodesModel implements Parcelable {
    @SerializedName("unusedcodeslist")
    private ArrayList<Unusedcodeslist> unusedcodeslist;

    protected UlduzumUnusedCodesModel(Parcel in) {
        unusedcodeslist = in.createTypedArrayList(Unusedcodeslist.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(unusedcodeslist);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UlduzumUnusedCodesModel> CREATOR = new Creator<UlduzumUnusedCodesModel>() {
        @Override
        public UlduzumUnusedCodesModel createFromParcel(Parcel in) {
            return new UlduzumUnusedCodesModel(in);
        }

        @Override
        public UlduzumUnusedCodesModel[] newArray(int size) {
            return new UlduzumUnusedCodesModel[size];
        }
    };

    public ArrayList<Unusedcodeslist> getUnusedcodeslist() {
        return unusedcodeslist;
    }

    public void setUnusedcodeslist(ArrayList<Unusedcodeslist> unusedcodeslist) {
        this.unusedcodeslist = unusedcodeslist;
    }
}
