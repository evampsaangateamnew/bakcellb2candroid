package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UlduzumLocationsServiceModel implements Parcelable {

    @SerializedName("categories")
    private ArrayList<CategoryList> categoryList;
    @SerializedName("merchants")
    private ArrayList<MerchantModel> maindatalist;

    protected UlduzumLocationsServiceModel(Parcel in) {
        categoryList = in.createTypedArrayList(CategoryList.CREATOR);
        maindatalist = in.createTypedArrayList(MerchantModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(categoryList);
        dest.writeTypedList(maindatalist);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UlduzumLocationsServiceModel> CREATOR = new Creator<UlduzumLocationsServiceModel>() {
        @Override
        public UlduzumLocationsServiceModel createFromParcel(Parcel in) {
            return new UlduzumLocationsServiceModel(in);
        }

        @Override
        public UlduzumLocationsServiceModel[] newArray(int size) {
            return new UlduzumLocationsServiceModel[size];
        }
    };


    public ArrayList<CategoryList> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<CategoryList> categoryList) {
        this.categoryList = categoryList;
    }

    public ArrayList<MerchantModel> getMaindatalist() {
        return maindatalist;
    }

    public void setMaindatalist(ArrayList<MerchantModel> maindatalist) {
        this.maindatalist = maindatalist;
    }
}
