package com.bakcell.models.ulduzum;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Unusedcodeslist implements Parcelable {
    @SerializedName("identical_code")
    private String identical_code;
    @SerializedName("insert_date")
    private String insert_date;

    protected Unusedcodeslist(Parcel in) {
        identical_code = in.readString();
        insert_date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identical_code);
        dest.writeString(insert_date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Unusedcodeslist> CREATOR = new Creator<Unusedcodeslist>() {
        @Override
        public Unusedcodeslist createFromParcel(Parcel in) {
            return new Unusedcodeslist(in);
        }

        @Override
        public Unusedcodeslist[] newArray(int size) {
            return new Unusedcodeslist[size];
        }
    };

    public String getIdentical_code() {
        return identical_code;
    }

    public void setIdentical_code(String identical_code) {
        this.identical_code = identical_code;
    }

    public String getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(String insert_date) {
        this.insert_date = insert_date;
    }
}
