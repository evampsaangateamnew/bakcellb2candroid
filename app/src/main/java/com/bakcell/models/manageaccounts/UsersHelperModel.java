package com.bakcell.models.manageaccounts;

import com.bakcell.models.user.UserModel;

import java.util.ArrayList;

/**
 * @author Junaid Hassan on 19, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
public class UsersHelperModel {
    public ArrayList<UserModel> userModelArrayList;

    public UsersHelperModel(ArrayList<UserModel> userModelArrayList) {
        this.userModelArrayList = userModelArrayList;
    }//constructor ends
}//class ends
