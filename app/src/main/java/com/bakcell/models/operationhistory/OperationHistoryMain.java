package com.bakcell.models.operationhistory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 23-Sep-17.
 */

public class OperationHistoryMain implements Parcelable {

    @SerializedName("records")
    private ArrayList<OperationHistoryRecord> records;

    protected OperationHistoryMain(Parcel in) {
        records = in.createTypedArrayList(OperationHistoryRecord.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(records);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OperationHistoryMain> CREATOR = new Creator<OperationHistoryMain>() {
        @Override
        public OperationHistoryMain createFromParcel(Parcel in) {
            return new OperationHistoryMain(in);
        }

        @Override
        public OperationHistoryMain[] newArray(int size) {
            return new OperationHistoryMain[size];
        }
    };

    public ArrayList<OperationHistoryRecord> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<OperationHistoryRecord> records) {
        this.records = records;
    }
}
