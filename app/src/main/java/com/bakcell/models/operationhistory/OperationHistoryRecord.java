package com.bakcell.models.operationhistory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 23-Sep-17.
 */

public class OperationHistoryRecord implements Parcelable {

    @SerializedName("amount")
    private String amount;
    @SerializedName("clarification")
    private String clarification;
    @SerializedName("date")
    private String date;
    @SerializedName("description")
    private String description;
    @SerializedName("endingBalance")
    private String endingBalance;
    @SerializedName("primaryIdentity")
    private String primaryIdentity;
    @SerializedName("transactionType")
    private String transactionType;
    @SerializedName("currency")
    String currency;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getClarification() {
        return clarification;
    }

    public void setClarification(String clarification) {
        this.clarification = clarification;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndingBalance() {
        return endingBalance;
    }

    public void setEndingBalance(String endingBalance) {
        this.endingBalance = endingBalance;
    }

    public String getPrimaryIdentity() {
        return primaryIdentity;
    }

    public void setPrimaryIdentity(String primaryIdentity) {
        this.primaryIdentity = primaryIdentity;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    protected OperationHistoryRecord(Parcel in) {
        amount = in.readString();
        clarification = in.readString();
        date = in.readString();
        description = in.readString();
        endingBalance = in.readString();
        primaryIdentity = in.readString();
        transactionType = in.readString();
        currency = in.readString();
    }

    public static final Creator<OperationHistoryRecord> CREATOR = new Creator<OperationHistoryRecord>() {
        @Override
        public OperationHistoryRecord createFromParcel(Parcel in) {
            return new OperationHistoryRecord(in);
        }

        @Override
        public OperationHistoryRecord[] newArray(int size) {
            return new OperationHistoryRecord[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(amount);
        parcel.writeString(clarification);
        parcel.writeString(date);
        parcel.writeString(description);
        parcel.writeString(endingBalance);
        parcel.writeString(primaryIdentity);
        parcel.writeString(transactionType);
        parcel.writeString(currency);
    }
}
