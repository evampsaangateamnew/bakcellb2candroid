package com.bakcell.models.mysubscriptions;

import android.os.Parcel;
import android.os.Parcelable;

import com.bakcell.models.mysubscriptions.details.SubscriptionsDetailsMain;
import com.bakcell.models.mysubscriptions.header.HeaderMain;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Sep-17.
 */

public class Subscriptions implements Parcelable{

    @SerializedName("header")
    private HeaderMain header;
    @SerializedName("details")
    private SubscriptionsDetailsMain details;

    protected Subscriptions(Parcel in) {
        details = in.readParcelable(SubscriptionsDetailsMain.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(details, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Subscriptions> CREATOR = new Creator<Subscriptions>() {
        @Override
        public Subscriptions createFromParcel(Parcel in) {
            return new Subscriptions(in);
        }

        @Override
        public Subscriptions[] newArray(int size) {
            return new Subscriptions[size];
        }
    };

    public HeaderMain getHeader() {
        return header;
    }

    public void setHeader(HeaderMain header) {
        this.header = header;
    }

    public SubscriptionsDetailsMain getDetails() {
        return details;
    }

    public void setDetails(SubscriptionsDetailsMain details) {
        this.details = details;
    }
}
