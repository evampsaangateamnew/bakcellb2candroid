/**
 *
 */
package com.bakcell.models.mysubscriptions.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Noman
 */
public class RoamingDetails implements Parcelable {

    @SerializedName("descriptionAbove")
    private String descriptionAbove;
    @SerializedName("descriptionBelow")
    private String descriptionBelow;
    @SerializedName("roamingDetailsCountriesList")
    private ArrayList<RoamingDetailsCountries> roamingDetailsCountriesList;

    public String getDescriptionAbove() {
        return descriptionAbove;
    }

    public void setDescriptionAbove(String descriptionAbove) {
        this.descriptionAbove = descriptionAbove;
    }

    public String getDescriptionBelow() {
        return descriptionBelow;
    }

    public void setDescriptionBelow(String descriptionBelow) {
        this.descriptionBelow = descriptionBelow;
    }

    public ArrayList<RoamingDetailsCountries> getRoamingDetailsCountriesList() {
        return roamingDetailsCountriesList;
    }

    public void setRoamingDetailsCountriesList(ArrayList<RoamingDetailsCountries> roamingDetailsCountriesList) {
        this.roamingDetailsCountriesList = roamingDetailsCountriesList;
    }

    protected RoamingDetails(Parcel in) {
        descriptionAbove = in.readString();
        descriptionBelow = in.readString();
    }

    public static final Creator<RoamingDetails> CREATOR = new Creator<RoamingDetails>() {
        @Override
        public RoamingDetails createFromParcel(Parcel in) {
            return new RoamingDetails(in);
        }

        @Override
        public RoamingDetails[] newArray(int size) {
            return new RoamingDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(descriptionAbove);
        dest.writeString(descriptionBelow);
    }
}
