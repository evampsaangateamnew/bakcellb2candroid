package com.bakcell.models.mysubscriptions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Sep-17.
 */

public class SubscriptionsMain implements Parcelable {

    @SerializedName("call")
    private SubscriptionCategory call;
    @SerializedName("campaign")
    private SubscriptionCategory campaign;
    @SerializedName("hybrid")
    private SubscriptionCategory hybrid;
    @SerializedName("internet")
    private SubscriptionCategory internet;
    @SerializedName("sms")
    private SubscriptionCategory sms;
    @SerializedName("tm")
    private SubscriptionCategory tm;
    @SerializedName("roaming")
    private SubscriptionCategory roaming;

    @SerializedName("voiceInclusiveOffers")
    private SubscriptionCategory voiceInclusiveOffers;
    @SerializedName("internetInclusiveOffers")
    private SubscriptionCategory internetInclusiveOffers;
    @SerializedName("smsInclusiveOffers")
    private SubscriptionCategory smsInclusiveOffers;

    public SubscriptionsMain() {
    }


    protected SubscriptionsMain(Parcel in) {
        call = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        campaign = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        hybrid = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        internet = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        sms = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        tm = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        roaming = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        voiceInclusiveOffers = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        internetInclusiveOffers = in.readParcelable(SubscriptionCategory.class.getClassLoader());
        smsInclusiveOffers = in.readParcelable(SubscriptionCategory.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(call, flags);
        dest.writeParcelable(campaign, flags);
        dest.writeParcelable(hybrid, flags);
        dest.writeParcelable(internet, flags);
        dest.writeParcelable(sms, flags);
        dest.writeParcelable(tm, flags);
        dest.writeParcelable(roaming, flags);
        dest.writeParcelable(voiceInclusiveOffers, flags);
        dest.writeParcelable(internetInclusiveOffers, flags);
        dest.writeParcelable(smsInclusiveOffers, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubscriptionsMain> CREATOR = new Creator<SubscriptionsMain>() {
        @Override
        public SubscriptionsMain createFromParcel(Parcel in) {
            return new SubscriptionsMain(in);
        }

        @Override
        public SubscriptionsMain[] newArray(int size) {
            return new SubscriptionsMain[size];
        }
    };

    public SubscriptionCategory getVoiceInclusiveOffers() {
        return voiceInclusiveOffers;
    }

    public void setVoiceInclusiveOffers(SubscriptionCategory voiceInclusiveOffers) {
        this.voiceInclusiveOffers = voiceInclusiveOffers;
    }

    public SubscriptionCategory getInternetInclusiveOffers() {
        return internetInclusiveOffers;
    }

    public void setInternetInclusiveOffers(SubscriptionCategory internetInclusiveOffers) {
        this.internetInclusiveOffers = internetInclusiveOffers;
    }

    public SubscriptionCategory getSmsInclusiveOffers() {
        return smsInclusiveOffers;
    }

    public void setSmsInclusiveOffers(SubscriptionCategory smsInclusiveOffers) {
        this.smsInclusiveOffers = smsInclusiveOffers;
    }

    public SubscriptionCategory getCall() {
        return call;
    }

    public void setCall(SubscriptionCategory call) {
        this.call = call;
    }

    public SubscriptionCategory getCampaign() {
        return campaign;
    }

    public void setCampaign(SubscriptionCategory campaign) {
        this.campaign = campaign;
    }

    public SubscriptionCategory getHybrid() {
        return hybrid;
    }

    public void setHybrid(SubscriptionCategory hybrid) {
        this.hybrid = hybrid;
    }

    public SubscriptionCategory getInternet() {
        return internet;
    }

    public void setInternet(SubscriptionCategory internet) {
        this.internet = internet;
    }

    public SubscriptionCategory getSms() {
        return sms;
    }

    public void setSms(SubscriptionCategory sms) {
        this.sms = sms;
    }

    public SubscriptionCategory getTm() {
        return tm;
    }

    public void setTm(SubscriptionCategory tm) {
        this.tm = tm;
    }

    public SubscriptionCategory getRoaming() {
        return roaming;
    }

    public void setRoaming(SubscriptionCategory roaming) {
        this.roaming = roaming;
    }


}
