/**
 *
 */
package com.bakcell.models.mysubscriptions.details;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class SubscriptionsDetailsMain implements Parcelable {

    @SerializedName("price")
    private Price price;
    @SerializedName("rounding")
    private Rounding rounding;
    @SerializedName("textWithTitle")
    private TextWithTitle textWithTitle;
    @SerializedName("textWithOutTitle")
    private TextWithOutTitle textWithOutTitle;
    @SerializedName("textWithPoints")
    private TextWithPoints textWithPoints;
    @SerializedName("titleSubTitleValueAndDesc")
    private TitleSubTitleListAndDesc titleSubTitleValueAndDesc;
    @SerializedName("date")
    private Date date;
    @SerializedName("time")
    private Time time;
    @SerializedName("roamingDetails")
    private RoamingDetails roamingDetails;
    @SerializedName("freeResourceValidity")
    private FreeResourceValidity freeResourceValidity;

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Rounding getRounding() {
        return rounding;
    }

    public void setRounding(Rounding rounding) {
        this.rounding = rounding;
    }

    public TextWithTitle getTextWithTitle() {
        return textWithTitle;
    }

    public void setTextWithTitle(TextWithTitle textWithTitle) {
        this.textWithTitle = textWithTitle;
    }

    public TextWithOutTitle getTextWithOutTitle() {
        return textWithOutTitle;
    }

    public void setTextWithOutTitle(TextWithOutTitle textWithOutTitle) {
        this.textWithOutTitle = textWithOutTitle;
    }

    public TextWithPoints getTextWithPoints() {
        return textWithPoints;
    }

    public void setTextWithPoints(TextWithPoints textWithPoints) {
        this.textWithPoints = textWithPoints;
    }

    public TitleSubTitleListAndDesc getTitleSubTitleValueAndDesc() {
        return titleSubTitleValueAndDesc;
    }

    public void setTitleSubTitleValueAndDesc(TitleSubTitleListAndDesc titleSubTitleValueAndDesc) {
        this.titleSubTitleValueAndDesc = titleSubTitleValueAndDesc;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public RoamingDetails getRoamingDetails() {
        return roamingDetails;
    }

    public void setRoamingDetails(RoamingDetails roamingDetails) {
        this.roamingDetails = roamingDetails;
    }

    public FreeResourceValidity getFreeResourceValidity() {
        return freeResourceValidity;
    }

    public void setFreeResourceValidity(FreeResourceValidity freeResourceValidity) {
        this.freeResourceValidity = freeResourceValidity;
    }

    protected SubscriptionsDetailsMain(Parcel in) {
        price = in.readParcelable(Price.class.getClassLoader());
        rounding = in.readParcelable(Rounding.class.getClassLoader());
        date = in.readParcelable(Date.class.getClassLoader());
        roamingDetails = in.readParcelable(RoamingDetails.class.getClassLoader());
        freeResourceValidity = in.readParcelable(FreeResourceValidity.class.getClassLoader());
    }

    public static final Creator<SubscriptionsDetailsMain> CREATOR = new Creator<SubscriptionsDetailsMain>() {
        @Override
        public SubscriptionsDetailsMain createFromParcel(Parcel in) {
            return new SubscriptionsDetailsMain(in);
        }

        @Override
        public SubscriptionsDetailsMain[] newArray(int size) {
            return new SubscriptionsDetailsMain[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(price, flags);
        dest.writeParcelable(rounding, flags);
        dest.writeParcelable(date, flags);
        dest.writeParcelable(roamingDetails, flags);
        dest.writeParcelable(freeResourceValidity, flags);
    }
}
