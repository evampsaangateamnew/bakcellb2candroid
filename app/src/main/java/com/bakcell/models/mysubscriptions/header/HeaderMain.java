package com.bakcell.models.mysubscriptions.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 24-Sep-17.
 */

public class HeaderMain implements Parcelable {

    @SerializedName("currency")
    private String currency;
    @SerializedName("id")
    private String id;
    @SerializedName("offerName")
    private String name;
    @SerializedName("price")
    private String price;
    @SerializedName("status")
    private String status;
    @SerializedName("type")
    private String type;
    @SerializedName("offerGroup")
    private OfferGroup offerGroup;
    @SerializedName("usage")
    private ArrayList<OfferUsage> usage;
    @SerializedName("offeringId")
    private String offeringId;
    @SerializedName("offerLevel")
    private String offerLevel;
    @SerializedName("btnDeactivate")
    private String btnDeactivate;
    @SerializedName("btnRenew")
    private String btnRenew;
    @SerializedName("isFreeResource")
    private String isFreeResource;
    @SerializedName("attributeList")
    private ArrayList<OfferAction> attributeList;

    protected HeaderMain(Parcel in) {
        currency = in.readString();
        id = in.readString();
        name = in.readString();
        price = in.readString();
        status = in.readString();
        type = in.readString();
        offerGroup = in.readParcelable(OfferGroup.class.getClassLoader());
        usage = in.createTypedArrayList(OfferUsage.CREATOR);
        offeringId = in.readString();
        offerLevel = in.readString();
        btnDeactivate = in.readString();
        btnRenew = in.readString();
        isFreeResource = in.readString();
        attributeList = in.createTypedArrayList(OfferAction.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currency);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(price);
        dest.writeString(status);
        dest.writeString(type);
        dest.writeParcelable(offerGroup, flags);
        dest.writeTypedList(usage);
        dest.writeString(offeringId);
        dest.writeString(offerLevel);
        dest.writeString(btnDeactivate);
        dest.writeString(btnRenew);
        dest.writeString(isFreeResource);
        dest.writeTypedList(attributeList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HeaderMain> CREATOR = new Creator<HeaderMain>() {
        @Override
        public HeaderMain createFromParcel(Parcel in) {
            return new HeaderMain(in);
        }

        @Override
        public HeaderMain[] newArray(int size) {
            return new HeaderMain[size];
        }
    };

    public String getIsFreeResource() {
        return isFreeResource;
    }

    public void setIsFreeResource(String isFreeResource) {
        this.isFreeResource = isFreeResource;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public OfferGroup getOfferGroup() {
        return offerGroup;
    }

    public void setOfferGroup(OfferGroup offerGroup) {
        this.offerGroup = offerGroup;
    }

    public ArrayList<OfferUsage> getUsage() {
        return usage;
    }

    public void setUsage(ArrayList<OfferUsage> usage) {
        this.usage = usage;
    }

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getOfferLevel() {
        return offerLevel;
    }

    public void setOfferLevel(String offerLevel) {
        this.offerLevel = offerLevel;
    }

    public String getBtnDeactivate() {
        return btnDeactivate;
    }

    public void setBtnDeactivate(String btnDeactivate) {
        this.btnDeactivate = btnDeactivate;
    }

    public String getBtnRenew() {
        return btnRenew;
    }

    public void setBtnRenew(String btnRenew) {
        this.btnRenew = btnRenew;
    }

    public ArrayList<OfferAction> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(ArrayList<OfferAction> attributeList) {
        this.attributeList = attributeList;
    }
}
