package com.bakcell.models.mysubscriptions.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Sep-17.
 */

public class OfferAction implements Parcelable {

    @SerializedName("desc")
    private String desc;
    @SerializedName("iconMap")
    private String iconMap;
    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private String value;
    @SerializedName("unit")
    private String unit;

    protected OfferAction(Parcel in) {
        desc = in.readString();
        iconMap = in.readString();
        title = in.readString();
        value = in.readString();
        unit = in.readString();
    }

    public static final Creator<OfferAction> CREATOR = new Creator<OfferAction>() {
        @Override
        public OfferAction createFromParcel(Parcel in) {
            return new OfferAction(in);
        }

        @Override
        public OfferAction[] newArray(int size) {
            return new OfferAction[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(desc);
        dest.writeString(iconMap);
        dest.writeString(title);
        dest.writeString(value);
        dest.writeString(unit);
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIconMap() {
        return iconMap;
    }

    public void setIconMap(String iconMap) {
        this.iconMap = iconMap;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
