package com.bakcell.models.mysubscriptions.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Sep-17.
 */

public class OfferGroup implements Parcelable{

    @SerializedName("groupName")
    private String groupName;
    @SerializedName("groupValue")
    private String groupValue;

    protected OfferGroup(Parcel in) {
        groupName = in.readString();
        groupValue = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(groupName);
        dest.writeString(groupValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OfferGroup> CREATOR = new Creator<OfferGroup>() {
        @Override
        public OfferGroup createFromParcel(Parcel in) {
            return new OfferGroup(in);
        }

        @Override
        public OfferGroup[] newArray(int size) {
            return new OfferGroup[size];
        }
    };

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupValue() {
        return groupValue;
    }

    public void setGroupValue(String groupValue) {
        this.groupValue = groupValue;
    }
}
