package com.bakcell.models.mysubscriptions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 24-Sep-17.
 */

public class SubscriptionCategory implements Parcelable {

    @SerializedName("offers")
    private ArrayList<Subscriptions> offers;

    public SubscriptionCategory() {
    }

    protected SubscriptionCategory(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubscriptionCategory> CREATOR = new Creator<SubscriptionCategory>() {
        @Override
        public SubscriptionCategory createFromParcel(Parcel in) {
            return new SubscriptionCategory(in);
        }

        @Override
        public SubscriptionCategory[] newArray(int size) {
            return new SubscriptionCategory[size];
        }
    };

    public ArrayList<Subscriptions> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<Subscriptions> offers) {
        this.offers = offers;
    }
}
