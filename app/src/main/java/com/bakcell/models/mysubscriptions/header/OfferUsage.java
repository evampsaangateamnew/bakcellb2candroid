package com.bakcell.models.mysubscriptions.header;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 24-Sep-17.
 */

public class OfferUsage implements Parcelable {

    @SerializedName("activationDate")
    private String activationDate;
    @SerializedName("iconName")
    private String iconName;
    @SerializedName("remainingTitle")
    private String remainingTitle;
    @SerializedName("remainingUsage")
    private String remainingUsage;
    @SerializedName("renewalDate")
    private String renewalDate;
    @SerializedName("renewalTitle")
    private String renewalTitle;
    @SerializedName("totalUsage")
    private String totalUsage;
    @SerializedName("unit")
    private String unit;
    @SerializedName("type")
    private String type;


    protected OfferUsage(Parcel in) {
        activationDate = in.readString();
        iconName = in.readString();
        remainingTitle = in.readString();
        remainingUsage = in.readString();
        renewalDate = in.readString();
        renewalTitle = in.readString();
        totalUsage = in.readString();
        unit = in.readString();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(activationDate);
        dest.writeString(iconName);
        dest.writeString(remainingTitle);
        dest.writeString(remainingUsage);
        dest.writeString(renewalDate);
        dest.writeString(renewalTitle);
        dest.writeString(totalUsage);
        dest.writeString(unit);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OfferUsage> CREATOR = new Creator<OfferUsage>() {
        @Override
        public OfferUsage createFromParcel(Parcel in) {
            return new OfferUsage(in);
        }

        @Override
        public OfferUsage[] newArray(int size) {
            return new OfferUsage[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getRemainingTitle() {
        return remainingTitle;
    }

    public void setRemainingTitle(String remainingTitle) {
        this.remainingTitle = remainingTitle;
    }

    public String getRemainingUsage() {
        return remainingUsage;
    }

    public void setRemainingUsage(String remainingUsage) {
        this.remainingUsage = remainingUsage;
    }

    public String getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(String renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewalTitle() {
        return renewalTitle;
    }

    public void setRenewalTitle(String renewalTitle) {
        this.renewalTitle = renewalTitle;
    }

    public String getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(String totalUsage) {
        this.totalUsage = totalUsage;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
