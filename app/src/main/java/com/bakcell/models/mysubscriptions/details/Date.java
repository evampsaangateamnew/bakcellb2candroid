/**
 *
 */
package com.bakcell.models.mysubscriptions.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class Date implements Parcelable {

    @SerializedName("fromTitle")
    private String fromTitle;
    @SerializedName("fromValue")
    private String fromValue;
    @SerializedName("toTitle")
    private String toTitle;
    @SerializedName("toValue")
    private String toValue;
    @SerializedName("description")
    private String description;

    public String getFromTitle() {
        return fromTitle;
    }

    public void setFromTitle(String fromTitle) {
        this.fromTitle = fromTitle;
    }

    public String getFromValue() {
        return fromValue;
    }

    public void setFromValue(String fromValue) {
        this.fromValue = fromValue;
    }

    public String getToTitle() {
        return toTitle;
    }

    public void setToTitle(String toTitle) {
        this.toTitle = toTitle;
    }

    public String getToValue() {
        return toValue;
    }

    public void setToValue(String toValue) {
        this.toValue = toValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    protected Date(Parcel in) {
        fromTitle = in.readString();
        fromValue = in.readString();
        toTitle = in.readString();
        toValue = in.readString();
        description = in.readString();
    }

    public static final Creator<Date> CREATOR = new Creator<Date>() {
        @Override
        public Date createFromParcel(Parcel in) {
            return new Date(in);
        }

        @Override
        public Date[] newArray(int size) {
            return new Date[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fromTitle);
        dest.writeString(fromValue);
        dest.writeString(toTitle);
        dest.writeString(toValue);
        dest.writeString(description);
    }
}
