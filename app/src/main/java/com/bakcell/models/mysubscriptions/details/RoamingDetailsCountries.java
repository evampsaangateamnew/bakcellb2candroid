/**
 *
 */
package com.bakcell.models.mysubscriptions.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Muneeb Rehman
 */
public class RoamingDetailsCountries implements Parcelable {

    @SerializedName("countryName")
    private String countryName;
    @SerializedName("flag")
    private String flag;
    @SerializedName("operatorList")
    ArrayList<String> operatorList;


    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public ArrayList<String> getOperatorList() {
        return operatorList;
    }

    public void setOperatorList(ArrayList<String> operatorList) {
        this.operatorList = operatorList;
    }

    protected RoamingDetailsCountries(Parcel in) {
        countryName = in.readString();
        flag = in.readString();
        operatorList = in.createStringArrayList();
    }

    public static final Creator<RoamingDetailsCountries> CREATOR = new Creator<RoamingDetailsCountries>() {
        @Override
        public RoamingDetailsCountries createFromParcel(Parcel in) {
            return new RoamingDetailsCountries(in);
        }

        @Override
        public RoamingDetailsCountries[] newArray(int size) {
            return new RoamingDetailsCountries[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(countryName);
        dest.writeString(flag);
        dest.writeStringList(operatorList);
    }
}
