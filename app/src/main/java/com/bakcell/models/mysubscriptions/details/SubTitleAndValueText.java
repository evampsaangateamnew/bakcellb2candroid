/**
 *
 */
package com.bakcell.models.mysubscriptions.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class SubTitleAndValueText implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private String value;
    @SerializedName("iconName")
    private String iconName;
    @SerializedName("description")
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    protected SubTitleAndValueText(Parcel in) {
        title = in.readString();
        value = in.readString();
        iconName = in.readString();
        description = in.readString();
    }

    public static final Creator<SubTitleAndValueText> CREATOR = new Creator<SubTitleAndValueText>() {
        @Override
        public SubTitleAndValueText createFromParcel(Parcel in) {
            return new SubTitleAndValueText(in);
        }

        @Override
        public SubTitleAndValueText[] newArray(int size) {
            return new SubTitleAndValueText[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
        dest.writeString(iconName);
        dest.writeString(description);
    }
}
