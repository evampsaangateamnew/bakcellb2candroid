/**
 *
 */
package com.bakcell.models.mysubscriptions.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Noman
 */
public class SubTitleAndValue implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private String value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    protected SubTitleAndValue(Parcel in) {
        title = in.readString();
        value = in.readString();
    }

    public static final Creator<SubTitleAndValue> CREATOR = new Creator<SubTitleAndValue>() {
        @Override
        public SubTitleAndValue createFromParcel(Parcel in) {
            return new SubTitleAndValue(in);
        }

        @Override
        public SubTitleAndValue[] newArray(int size) {
            return new SubTitleAndValue[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
    }
}
