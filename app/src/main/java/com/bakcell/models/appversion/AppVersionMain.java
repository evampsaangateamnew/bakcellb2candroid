package com.bakcell.models.appversion;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Noman on 19-Oct-17.
 */

public class AppVersionMain implements Parcelable {

    @SerializedName("versionConfig")
    private String versionConfig;
    @SerializedName("timeStamps")
    private ArrayList<TimeStamps> timeStamps;


    public String getVersionConfig() {
        return versionConfig;
    }

    public void setVersionConfig(String versionConfig) {
        this.versionConfig = versionConfig;
    }

    public ArrayList<TimeStamps> getTimeStamps() {
        return timeStamps;
    }

    public void setTimeStamps(ArrayList<TimeStamps> timeStamps) {
        this.timeStamps = timeStamps;
    }

    protected AppVersionMain(Parcel in) {
        versionConfig = in.readString();
        timeStamps = in.createTypedArrayList(TimeStamps.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(versionConfig);
        dest.writeTypedList(timeStamps);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AppVersionMain> CREATOR = new Creator<AppVersionMain>() {
        @Override
        public AppVersionMain createFromParcel(Parcel in) {
            return new AppVersionMain(in);
        }

        @Override
        public AppVersionMain[] newArray(int size) {
            return new AppVersionMain[size];
        }
    };
}
