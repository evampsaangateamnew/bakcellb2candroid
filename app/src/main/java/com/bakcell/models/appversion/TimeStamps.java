package com.bakcell.models.appversion;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 19-Oct-17.
 */

public class TimeStamps implements Parcelable {

    @SerializedName("cacheType")
    private String cacheType;
    @SerializedName("timeStamp")
    private String timeStamp;

    public String getCacheType() {
        return cacheType;
    }

    public void setCacheType(String cacheType) {
        this.cacheType = cacheType;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    protected TimeStamps(Parcel in) {
        cacheType = in.readString();
        timeStamp = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cacheType);
        dest.writeString(timeStamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TimeStamps> CREATOR = new Creator<TimeStamps>() {
        @Override
        public TimeStamps createFromParcel(Parcel in) {
            return new TimeStamps(in);
        }

        @Override
        public TimeStamps[] newArray(int size) {
            return new TimeStamps[size];
        }
    };
}
