package com.bakcell.models.filter;

/**
 * Created by Zeeshan Shabbir on 8/21/2017.
 */

public class PackageName {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
