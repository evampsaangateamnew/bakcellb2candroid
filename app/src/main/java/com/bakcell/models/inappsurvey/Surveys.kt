package com.bakcell.models.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 * Created by Muhammad Ahmed on 8/12/2021.
 */

data class Surveys(

    @SerializedName("id") val id: Int,
    @SerializedName("screenName") val screenName: String?,
    @SerializedName("timeLimit") val timeLimit: Int,
    @SerializedName("visitLimit") val visitLimit: Int,
    @SerializedName("surveyLimit") val surveyLimit: Int,
    @SerializedName("surveyCount") val surveyCount: Int,
    @SerializedName("onTransactionCompleteEn") val onTransactionCompleteEn: String?,
    @SerializedName("onTransactionCompleteAz") val onTransactionCompleteAz: String?,
    @SerializedName("onTransactionCompleteRu") val onTransactionCompleteRu: String?,
    @SerializedName("questions") val questions: List<Questions>?,
    @SerializedName("commentEnable") val commentEnable: String?,
    @SerializedName("commentsTitleEn") val commentsTitleEn: String?,
    @SerializedName("commentsTitleAz") val commentsTitleAz: String?,
    @SerializedName("commentsTitleRu") val commentsTitleRu: String?,
    @SerializedName("titleEn") val titleEn: String?,
    @SerializedName("customerType") val customerType: String?

) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createTypedArrayList(Questions),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    fun findQuestion(questionId: Int): Questions? {
        questions?.forEach {
            if (it.id == questionId) {
                return it
            }
        }
        return null
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(screenName)
        parcel.writeInt(timeLimit)
        parcel.writeInt(visitLimit)
        parcel.writeInt(surveyLimit)
        parcel.writeInt(surveyCount)
        parcel.writeString(onTransactionCompleteEn)
        parcel.writeString(onTransactionCompleteAz)
        parcel.writeString(onTransactionCompleteRu)
        parcel.writeTypedList(questions)
        parcel.writeString(commentEnable)
        parcel.writeString(commentsTitleEn)
        parcel.writeString(commentsTitleAz)
        parcel.writeString(commentsTitleRu)
        parcel.writeString(titleEn)
        parcel.writeString(customerType)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Surveys> {
        override fun createFromParcel(parcel: Parcel): Surveys {
            return Surveys(parcel)
        }

        override fun newArray(size: Int): Array<Surveys?> {
            return arrayOfNulls(size)
        }
    }


}