package com.bakcell.models.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Muhammad Ahmed on 8/12/2021.
 */


data class Questions(

    @SerializedName("id") val id: Int,
    @SerializedName("questionTextEN") val questionTextEN: String?,
    @SerializedName("questionTextAZ") val questionTextAZ: String?,
    @SerializedName("questionTextRU") val questionTextRU: String?,
    @SerializedName("answerType") val answerType: Int,
    @SerializedName("answers") val answers: List<Answers>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.createTypedArrayList(Answers.CREATOR)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(questionTextEN)
        parcel.writeString(questionTextAZ)
        parcel.writeString(questionTextRU)
        parcel.writeInt(answerType)
        parcel.writeTypedList(answers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Questions> {
        override fun createFromParcel(parcel: Parcel): Questions {
            return Questions(parcel)
        }

        override fun newArray(size: Int): Array<Questions?> {
            return arrayOfNulls(size)
        }
    }
}