package com.bakcell.models.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 * Created by Muhammad Ahmed on 8/12/2021.
 */


data class Data(
    @SerializedName("surveys") val surveys: List<Surveys>?,
    @SerializedName("userSurveys") val userSurveys : List<UserSurveys>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Surveys),
        parcel.createTypedArrayList(UserSurveys)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(surveys)
        parcel.writeTypedList(userSurveys)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Data> {
        override fun createFromParcel(parcel: Parcel): Data {
            return Data(parcel)
        }

        override fun newArray(size: Int): Array<Data?> {
            return arrayOfNulls(size)
        }
    }
    fun findSurvey(pageName: String): Surveys? {
        surveys?.forEach {
            if (it.screenName == pageName) {
                return it
            }
        }
        return null
    }
    fun findSurveyById(surveyId: Int): Surveys? {
        surveys?.forEach {
            if (it.id == surveyId) {
                return it
            }
        }
        return null
    }
}