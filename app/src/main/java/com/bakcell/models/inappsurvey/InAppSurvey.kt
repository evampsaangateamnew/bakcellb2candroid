package com.bakcell.models.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Muhammad Ahmed on 8/12/2021.
 */


data class InAppSurvey(

    @SerializedName("callStatus") val callStatus: Boolean,
    @SerializedName("resultCode") val resultCode: String?,
    @SerializedName("resultDesc") val resultDesc: String?,
    @SerializedName("data") val data: Data?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Data::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (callStatus) 1 else 0)
        parcel.writeString(resultCode)
        parcel.writeString(resultDesc)
        parcel.writeParcelable(data, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<InAppSurvey> {
        override fun createFromParcel(parcel: Parcel): InAppSurvey {
            return InAppSurvey(parcel)
        }

        override fun newArray(size: Int): Array<InAppSurvey?> {
            return arrayOfNulls(size)
        }
    }
}