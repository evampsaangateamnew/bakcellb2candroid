package com.bakcell.models.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Muhammad Ahmed on 8/12/2021.
 */


data class Answers(

    @SerializedName("id") val id: Int,
    @SerializedName("answerTextEN") val answerTextEN: String?,
    @SerializedName("answerTextAZ") val answerTextAZ: String?,
    @SerializedName("answerTextRU") val answerTextRU: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(answerTextEN)
        parcel.writeString(answerTextAZ)
        parcel.writeString(answerTextRU)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Answers> {
        override fun createFromParcel(parcel: Parcel): Answers {
            return Answers(parcel)
        }

        override fun newArray(size: Int): Array<Answers?> {
            return arrayOfNulls(size)
        }
    }
}