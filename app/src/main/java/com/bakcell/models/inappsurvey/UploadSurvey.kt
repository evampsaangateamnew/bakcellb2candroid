package com.bakcell.models.inappsurvey

data class UploadSurvey(
    val comment: String = "",
    val answerId: String = "",
    val questionId: String = "",
    val offeringId: String = "",
    val offeringType:String = "",
    val surveyId: String = ""
)
