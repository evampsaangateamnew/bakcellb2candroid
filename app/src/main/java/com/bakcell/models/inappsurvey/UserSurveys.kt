package com.bakcell.models.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by Muhammad Ahmed on 8/12/2021.
 */


data class UserSurveys (

    @SerializedName("answer") val answer : String?,
    @SerializedName("offeringId") val offeringId : String?,
    @SerializedName("offeringType") val offeringType : String?,
    @SerializedName("comment") val comment : String?,
    @SerializedName("surveyId") val surveyId : String?,
    @SerializedName("answerId") val answerId : String?,
    @SerializedName("questionId") val questionId : String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(answer)
        parcel.writeString(offeringId)
        parcel.writeString(offeringType)
        parcel.writeString(comment)
        parcel.writeString(surveyId)
        parcel.writeString(answerId)
        parcel.writeString(questionId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserSurveys> {
        override fun createFromParcel(parcel: Parcel): UserSurveys {
            return UserSurveys(parcel)
        }

        override fun newArray(size: Int): Array<UserSurveys?> {
            return arrayOfNulls(size)
        }
    }
}
