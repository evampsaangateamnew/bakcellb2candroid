package com.bakcell.models.notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Noman on 08-Oct-17.
 */

public class SettingsNotifications implements Parcelable {

    @SerializedName("isEnable")
    private String isEnable;
    @SerializedName("ringingStatus")
    private String ringingStatus;

    public SettingsNotifications() {
    }

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    public String getRingingStatus() {
        return ringingStatus;
    }

    public void setRingingStatus(String ringingStatus) {
        this.ringingStatus = ringingStatus;
    }

    protected SettingsNotifications(Parcel in) {
        isEnable = in.readString();
        ringingStatus = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(isEnable);
        dest.writeString(ringingStatus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SettingsNotifications> CREATOR = new Creator<SettingsNotifications>() {
        @Override
        public SettingsNotifications createFromParcel(Parcel in) {
            return new SettingsNotifications(in);
        }

        @Override
        public SettingsNotifications[] newArray(int size) {
            return new SettingsNotifications[size];
        }
    };
}
