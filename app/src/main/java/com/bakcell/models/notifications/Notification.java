package com.bakcell.models.notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Muhammad Atif Arif on 10/6/2017.
 */

public class Notification implements Parcelable {

    @SerializedName("message")
    private String textMessage;
    @SerializedName("landingPage")
    private String landingPage;
    @SerializedName("datetime")
    private String dateTime;
    @SerializedName("icon")
    private String icon;
    @SerializedName("actionType")
    private String actionType;
    @SerializedName("actionID")
    private String actionId;
    @SerializedName("btnTxt")
    private String buttonText;


    public String getTextMessage() {return textMessage;}

    public void setTextMessage(String textMessage) {this.textMessage = textMessage;}

    public String getLandingPage() {return landingPage;}

    public void setLandingPage(String landingPage) {this.landingPage = landingPage;}

    public String getDateTime() {return dateTime;}

    public void setDateTime(String dateTime) {this.dateTime = dateTime;}

    public String getIcon() {return icon;}

    public void setIcon(String icon) {this.icon = icon;}

    public String getActionType() {return actionType;}

    public void setActionType(String actionType) {this.actionType = actionType;}

    public String getActionId() {return actionId;}

    public void setActionId(String actionId) {this.actionId = actionId;}

    public String getButtonText() {return buttonText;}

    public void setButtonText(String buttonText) {this.buttonText = buttonText;}



    protected Notification(Parcel in) {
        textMessage = in.readString();
        landingPage = in.readString();
        dateTime = in.readString();
        icon = in.readString();
        actionType = in.readString();
        actionId = in.readString();
        buttonText = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(textMessage);
        dest.writeString(landingPage);
        dest.writeString(dateTime);
        dest.writeString(icon);
        dest.writeString(actionType);
        dest.writeString(actionId);
        dest.writeString(buttonText);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

}
