package com.bakcell.models.notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Muhamamd Atif on 10-06-17.
 */

public class NotificationMain implements Parcelable {

    @SerializedName("notificationUnreadCount")
    String notificationUnreadCount;

    @SerializedName("notificationsList")
    private ArrayList<Notification> notificationList;

    protected NotificationMain(Parcel in) {
        notificationList = in.createTypedArrayList(Notification.CREATOR);
    }

    public static final Creator<NotificationMain> CREATOR = new Creator<NotificationMain>() {
        @Override
        public NotificationMain createFromParcel(Parcel in) {
            return new NotificationMain(in);
        }

        @Override
        public NotificationMain[] newArray(int size) {
            return new NotificationMain[size];
        }
    };

    public ArrayList<Notification> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(ArrayList<Notification> notificationList) {
        this.notificationList = notificationList;
    }

    public String getNotificationUnreadCount() {
        return notificationUnreadCount;
    }

    public void setNotificationUnreadCount(String notificationUnreadCount) {
        this.notificationUnreadCount = notificationUnreadCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(notificationList);
    }
}
